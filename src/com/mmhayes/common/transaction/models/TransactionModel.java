package com.mmhayes.common.transaction.models;

//MMHayes dependencies

import com.mmhayes.common.api.PosApi30.PosAPIModelCache;
import com.mmhayes.common.api.errorhandling.exceptions.*;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.product.collections.ProductCollectionCalculation;
import com.mmhayes.common.terminal.models.TerminalModel;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.MMHProperties;
import com.mmhayes.common.product.models.*;
import com.mmhayes.common.transaction.collections.*;
import com.mmhayes.common.account.models.*;
import com.mmhayes.common.loyalty.models.*;
import com.mmhayes.common.loyalty.collections.*;
import com.mmhayes.common.donation.models.*;
import com.mmhayes.common.voucher.collections.DiscountInfoCollection;
import com.mmhayes.common.voucher.models.DiscountInfoModel;

//API dependencies
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.api.PosApi30.TransactionLineBuilder;
import com.mmhayes.common.api.errorhandling.exceptions.TransactionNotFoundException;
import com.fasterxml.jackson.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionDeserializer;
import org.apache.commons.lang3.StringUtils;

//other dependencies
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.*;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2021-08-30 14:16:40 -0400 (Mon, 30 Aug 2021) $: Date of last commit
 $Rev: 15226 $: Revision of last commit
*/
@ApiModel(value = "TransactionModel", description = "TransactionModel is the basis for all activity done for transactions.")
@JsonPropertyOrder({"id", "timeStamp", "ticketOpenedDTM", "drawerNum", "name", "address", "city", "state", "zip", "email", "phone",
        "transactionTypeId", "transactionType", "orderType", "orderTypeId", "originalTransactionId",
        "offlineTransactionId", "directVoidQuantity", "directVoidAmount", "indirectVoidQuantity", "indirectVoidAmount",
        "priceOverrideQuantity", "transPriceLevel", "transReferenceNumber", "transComment", "qcImportStatus", "userName", "recordedOffline",
        "pickUpDeliveryNote", "clientIdentifier",
        "orderNumber", "transactionLabel", "transactionStatus", "transactionStatusId", "transStatusLastUpdated", "linkedToPaTransactionId", "linkedFromPaTransactionIds", "useMealPlanDiscount",
        "estimatedOrderTime", "endShiftUserId", "qcImportStatus", "posKioskTerminalId", "loyaltyAdjustmentTypeId",
        "loyaltyAccount", "products", "tenders", "invalidTenders", "taxes", "discounts", "paidOuts",
        "receivedOnAccounts", "rewards", "serviceCharges", "gratuities", "surcharges", "combos", "donations", "loyaltyPointsEarned", "loyaltyPointsRedeemed", "terminal"})

public class TransactionModel {

    //region private properties

    private String timeStamp = null;
    private String orderType = "";
    private String transComment = "";
    private String pickUpDeliveryNote = "";
    private String name = "";
    private String address = "";
    private String city = "";
    private String state = "";
    private String zip = "";

    private String email = "";
    private String phone = "";
    private String transactionType;
    private String transReferenceNumber = "";
    private String userName = "";
    private String estimatedOrderTime = "";
    private String clientIdentifier = "";
    private String orderNumber = null;
    private String transactionLabel = "";
    private String transactionStatus = ""; // Parked Order
    private String transStatusLastUpdated = "";
    private String ticketOpenedDTM = null;
    private String linkedFromPaTransactionIdsCsv = "";
    private ArrayList<Object> linkedFromPaTransactionIds = null; //This will come in from the client (QCPOS)

    private Integer id = null;
    private Integer terminalId = null;
    private Integer userId = null;
    private Integer qcImportStatus = 1; //default this value to Imported so the background processor doesn't write it multiple times to the QC_Transactions file. 0 - BP imports it, 1 - API writes it, 2 - non QC Transaction
    private Integer drawerNum = null;
    private Integer originalTransactionId = null;
    private Integer offlineTransactionId = null;
    private Integer directVoidQuantity = null;
    private Integer indirectVoidQuantity = null;
    private Integer priceOverrideQuantity = null;
    private Integer transPriceLevel = null;
    private Integer transactionTypeId;
    private Integer endShiftUserId = null;
    private Integer linkedToPaTransactionId = null; //Parked Order
    private Integer transactionStatusId = null; //Parked Order
    private Integer posKioskTerminalId = null;
    private Integer orderTypeId = null;
    private Integer loyaltyAdjustmentTypeId = null;
    private Integer voucherCount = 0;

    private BigDecimal directVoidAmount = null;
    private BigDecimal indirectVoidAmount = null;
    private BigDecimal qcTenderBalance = BigDecimal.ZERO;
    private BigDecimal totalQcDiscountTotal = null;

    private boolean isOfflineMode = false;
    private boolean hasQuickChargeTender = false;
    private boolean hasCashTender = false;
    private boolean hasCreditCardTender = false;
    private boolean hasCheckTender = false;
    private boolean isCancelTransactionType = false;
    private boolean isTrainingTransactionType = false;
    private boolean isInquiry = false;
    private boolean hasValidatedLoyaltyAccount = false;
    private boolean hasItemProfileMappings = false;
    private boolean recordedOffline = false;
    private boolean isCreateOpen = false;
    private boolean useMealPlanDiscount = true;
    private boolean paymentGatewayInquireTransactionType = false;
    private boolean recordPrinterQueueRecordsLocally = false; //Only used for QC POS Kiosk
    private boolean isGrabAndGoTransaction = false; //Only used for MyQC
    private boolean hasDonation = false;
    private boolean autoApplyQcDiscounts = true;
    private boolean voucherCalcInProgress = false;

    private PosAPIHelper.TransactionType transactionTypeEnum;
    private PosAPIHelper.ApiActionType apiActionTypeEnum;
    private PosAPIHelper.PosType posType;

    private TerminalModel terminal = null;
    private TerminalModel originalTerminal = null;

    private LoyaltyAccountModel loyaltyAccount = null;
    private TransactionModel originalTransaction = null;
    private AccountQueryParams accountQueryParams = null;

    private List<TaxLineItemModel> taxes = new ArrayList<>();
    private List<TenderLineItemModel> tenders = new ArrayList<>();
    private List<ProductLineItemModel> products = new ArrayList<>();
    private List<PaidOutLineItemModel> paidOuts = new ArrayList<>();
    private List<ReceivedOnAccountLineItemModel> receivedOnAccounts = new ArrayList<>();
    private List<DiscountLineItemModel> discounts = new ArrayList<>();
    private List<LoyaltyRewardLineItemModel> rewards = new ArrayList<>();
    private List<ServiceChargeLineItemModel> serviceCharges = new ArrayList<>();
    private List<GratuityLineItemModel> gratuities = new ArrayList<>();
    private List<SurchargeLineItemModel> surcharges = new ArrayList<>();
    private List<LoyaltyPointModel> loyaltyPointsEarned = new ArrayList<>(); //will return the loyalty points that were earned on this transaction
    private List<LoyaltyPointModel> loyaltyPointsRedeemed = new ArrayList<>(); //will return the loyalty points that were redeemed on this transaction
    private List<LoyaltyPointModel> loyaltyPointsAll = new ArrayList<>(); //will return the loyalty points that were earned on this transaction
    private List<LoyaltyAdjustmentModel> loyaltyAdjustments = new ArrayList<>();
    private List<DonationLineItemModel> donations = new ArrayList<>();
    private List<ComboLineItemModel> combos = new ArrayList<>(); //will return the product combos on this transaction
    private List<TenderLineItemModel> invalidTenders = new ArrayList<>();
    private DataManager dm = new DataManager();

    private DiscountInfoCollection discountInfoCollection = new DiscountInfoCollection();

    //endregion

    //default constructor
    public TransactionModel() {
    }

    /**
     * Get One PATransaction from the database.  This gets all associated line items as well
     * Filter by the current revenue center.  You can't fetch transactions from outside of your Revenue Center
     *
     * @param id
     * @param terminalModel
     * @return
     * @throws Exception
     */
    public TransactionModel getOneTransactionById(Integer id, TerminalModel terminalModel, TransactionQueryParams transactionQueryParams) throws Exception {
        HashMap transactionHM = getOneTransactionFromDB(id, terminalModel, transactionQueryParams);

        CommonAPI.checkIsNullOrEmptyObject(transactionHM.get("ID"), "Transaction Id cannot be found.", terminalId);
        this.setModelProperties(transactionHM, terminalModel);
        this.setTerminal(this.getOriginalTerminal()); //set the Terminal back to the original terminal from the transaction
        this.getTerminal().logTransactionTime();
        return this;
    }

    /**
     * Fetch the Original Transaction for the refund by ID and void by ID endpoints
     * Filter by the current revenue center.  You can't fetch transactions from outside of your Revenue Center
     *
     * @param id            : transaction ID
     * @param terminalModel : current Terminal Model
     * @return
     * @throws Exception
     */
    public TransactionModel getOneTransactionByIdAndSetOriginalTransaction(Integer id, TerminalModel terminalModel, TransactionQueryParams transactionQueryParams) throws Exception {
        HashMap transactionHM = getOneTransactionFromDB(id, terminalModel, transactionQueryParams);
        CommonAPI.checkIsNullOrEmptyObject(transactionHM.get("ID"), "Transaction Id cannot be found.", terminalModel.getId());
        this.setModelProperties(transactionHM, terminalModel);
        this.prepareTransactionForVoidOrRefund(transactionHM, terminalModel);
        return this;
    }

    /**
     * Fetch the Original Transaction for the refund and void endpoints
     * Filter by the current revenue center.  You can't fetch transactions from outside of your Revenue Center
     *
     * @param terminalModel : current Terminal Model
     * @return
     * @throws Exception
     */
    public TransactionModel getOneTransactionByIdAndSetOriginalTransaction(TerminalModel terminalModel) throws Exception {
        DataManager dm = new DataManager();
        HashMap transactionHM = getOneTransactionFromDB(this.getId(), terminalModel, new TransactionQueryParams());
        CommonAPI.checkIsNullOrEmptyObject(transactionHM.get("ID"), "Transaction Id cannot be found.", terminalModel.getId());
        this.prepareTransactionForVoidOrRefund(transactionHM, terminalModel);
        return this;
    }

    /**
     * Get One PATransaction from the database.  This gets all associated line items as well
     * Filter by the current revenue center.  You can't fetch transactions from outside of your Revenue Center
     *
     * @param transIdObj
     * @param terminalModel
     * @return
     * @throws Exception
     */
    public TransactionModel getOneTransactionById(Object transIdObj, TerminalModel terminalModel, TransactionQueryParams transactionQueryParams) throws Exception {
        HashMap transactionHM = null;

        if (StringUtils.isNumeric(transIdObj.toString())){
            transactionHM = getOneTransactionFromDB(Integer.parseInt(transIdObj.toString()), terminalModel, transactionQueryParams);
        } else {
            HashMap<String, Integer> offlinePaTransIdParts = DataFunctions.parseOfflineTransIdFormat(transIdObj.toString(), "T");
            Integer localPaTransId = offlinePaTransIdParts.get("localPaTransId");// Integer.parseInt(transParts[0]);
            Integer localTerminalId = offlinePaTransIdParts.get("localTerminalId");
            transactionHM = getOneTransactionFromDB(localPaTransId, localTerminalId, terminalModel, transactionQueryParams);
        }

        CommonAPI.checkIsNullOrEmptyObject(transactionHM.get("ID"), "Transaction Id cannot be found.", terminalId);
        this.setModelProperties(transactionHM, terminalModel);
        this.setTerminal(this.getOriginalTerminal()); //set the Terminal back to the original terminal from the transaction
        this.getTerminal().logTransactionTime();
        return this;
    }

    /**
     * This is the common way to fetch a Transaction by Transaction ID.
     * If openOnly is true, then pull
     * Transactions of type Open, with any status EXCEPT Edit Completed
     * Transactions of type Cancel, with the status Expired
     *
     * @param transactionId
     * @param terminalModel
     * @param transactionQueryParams
     * @return
     * @throws Exception
     */
    private HashMap getOneTransactionFromDB(Integer transactionId, TerminalModel terminalModel, TransactionQueryParams transactionQueryParams) throws Exception {
        DataManager dm = new DataManager();

        //check for sandbox mode
        if (terminalModel.isSandBoxMode()) {
            sandboxModeCheck(transactionId);
        }

        //get all models in an array list
        ArrayList<HashMap> transactionList = dm.parameterizedExecuteQuery("data.posapi30.getOneQCPosTransactionById",
                new Object[]{
                        transactionId, terminalModel.getRevenueCenterId(),
                        (transactionQueryParams.isOpenOnly()) ? 1 : 0
                },
                PosAPIHelper.getLogFileName(terminalModel.getId()),
                true
        );

        HashMap transactionHM = null;

        if (transactionList.size() > 0) {
            transactionHM = transactionList.get(0);
        } else {
            throw new TransactionNotFoundException(terminalModel.getId());
        }

        if (transactionHM != null
                && CommonAPI.convertModelDetailToInteger(transactionHM.get("CANACCESSREVENUECENTER")) != null
                && !CommonAPI.convertModelDetailToInteger(transactionHM.get("CANACCESSREVENUECENTER")).toString().isEmpty()
                && CommonAPI.convertModelDetailToInteger(transactionHM.get("CANACCESSREVENUECENTER")) == 0) {

            throw new TransactionNotFoundException(terminalModel.getId());

        }

        return transactionHM;
    }

    //private HashMap getOneTransactionFromDB(Integer localPaTransId, Integer localTerminalId, String offlineTransId, TerminalModel terminalModel, TransactionQueryParams transactionQueryParams) throws Exception {
    private HashMap getOneTransactionFromDB(Integer localPaTransId, Integer localTerminalId, TerminalModel terminalModel, TransactionQueryParams transactionQueryParams) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> transactionList = dm.parameterizedExecuteQuery("data.posapi30.getOneQCPosTransactionByPaTransIdOffline",
                new Object[]{
                        localPaTransId, terminalModel.getRevenueCenterId(),
                        (transactionQueryParams.isOpenOnly()) ? 1 : 0,
                        localTerminalId
                },
                PosAPIHelper.getLogFileName(terminalModel.getId()),
                true
        );

        HashMap transactionHM = null;

        if (transactionList.size() > 0) {
            transactionHM = transactionList.get(0);
        } else {
            throw new TransactionNotFoundException(terminalModel.getId());
        }

        if (transactionHM != null
                && CommonAPI.convertModelDetailToInteger(transactionHM.get("CANACCESSREVENUECENTER")) != null
                && !CommonAPI.convertModelDetailToInteger(transactionHM.get("CANACCESSREVENUECENTER")).toString().isEmpty()
                && CommonAPI.convertModelDetailToInteger(transactionHM.get("CANACCESSREVENUECENTER")) == 0) {

            throw new TransactionNotFoundException(terminalModel.getId());

        }

        return transactionHM;
    }

    /**
     * Used by the Cashier Shift End to fetch a transaction
     *
     * @param id            - Transaction ID
     * @param terminalModel - Terminal ID
     * @param transTypeId   - Trnasaction Type
     * @return
     * @throws Exception
     */
    public TransactionModel getOneTransactionByIdAndTransTypeId(Integer id, TerminalModel terminalModel, Integer transTypeId) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> transactionList = dm.parameterizedExecuteQuery("data.posapi30.getOneQCPosTransactionByIdAndTransTypeId",
                new Object[]{
                        id, transTypeId
                },
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        HashMap transactionHM = null;
        //transaction list is null or empty, return empty list eglundin - 07-26-2016
        if (transactionList.size() > 0) {
            transactionHM = transactionList.get(0);
        } else {
            return null;
        }

        this.setModelProperties(transactionHM, terminalModel);
        return this;
    }


    /**
     * Get One PATransaction from the database by the ThirdPartyTransReferenceNumber.  This gets all associated line items as well
     * Filter by the current revenue center.  You can't fetch transactions from outside of your Revenue Center
     *
     * @param transReferenceNumber
     * @param terminalModel
     * @return
     * @throws Exception
     */
    public TransactionModel getOneTransactionByTransReferenceNumber(String transReferenceNumber, TerminalModel terminalModel) throws Exception {
        HashMap transactionHM = getOneTransactionFromDB(transReferenceNumber, terminalModel);

        CommonAPI.checkIsNullOrEmptyObject(transactionHM.get("ID"), "Transaction Id cannot be found.", terminalId);
        this.setModelProperties(transactionHM, terminalModel);
        this.setTerminal(this.getOriginalTerminal()); //set the Terminal back to the original terminal from the transaction

        return this;
    }

    /**
     * Fetch the Original Transaction for the refund by ID and void by ID endpoints by ThirdPartyTransReferenceNumber
     * Filter by the current revenue center.  You can't fetch transactions from outside of your Revenue Center
     *
     * @param transReferenceNumber : transaction ID
     * @param terminalModel        : current Terminal Model
     * @return
     * @throws Exception
     */
    public TransactionModel getOneTransactionByTransReferenceNumberAndSetOriginalTransaction(String transReferenceNumber, TerminalModel terminalModel) throws Exception {
        HashMap transactionHM = getOneTransactionFromDB(transReferenceNumber, terminalModel);
        CommonAPI.checkIsNullOrEmptyObject(transactionHM.get("ID"), "Transaction Id cannot be found.", terminalId);
        this.setModelProperties(transactionHM, terminalModel);
        this.prepareTransactionForVoidOrRefund(transactionHM, terminalModel);
        return this;
    }

    /**
     * This is the common way to fetch a Transaction by ThirdPartTransReferenceNumber
     *
     * @param transReferenceNumber
     * @param terminalModel
     * @return
     * @throws Exception
     */
    private HashMap getOneTransactionFromDB(String transReferenceNumber, TerminalModel terminalModel) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> transactionList = dm.parameterizedExecuteQuery("data.posapi30.getOneQCPosTransactionByTransReferenceNumber",
                new Object[]{
                        transReferenceNumber, terminalModel.getRevenueCenterId()
                },
                PosAPIHelper.getLogFileName(terminalModel.getId()),
                true
        );

        HashMap transactionHM = null;
        //transaction list is null or empty, return empty list eglundin - 07-26-2016
        //transaction list is > 1, error out. eglundin - 10/30/2017

        if (transactionList.size() > 1) {
            throw new DuplicateTransactionException("Multiple transactions with the provided Transaction Reference Number were found.  Unable to Complete Request");
        } else if (transactionList.size() == 1) {
            transactionHM = transactionList.get(0);
        } else {
            throw new TransactionNotFoundException(terminalId);
        }

        if (transactionHM != null
                && CommonAPI.convertModelDetailToInteger(transactionHM.get("CANACCESSREVENUECENTER")) != null
                && !CommonAPI.convertModelDetailToInteger(transactionHM.get("CANACCESSREVENUECENTER")).toString().isEmpty()
                && CommonAPI.convertModelDetailToInteger(transactionHM.get("CANACCESSREVENUECENTER")) == 0) {

            throw new TransactionNotFoundException(terminalId);

        }

        return transactionHM;
    }

    public TransactionModel setModelProperties(HashMap modelDetailHM, TerminalModel terminalModel) throws Exception {
        this.setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));
        this.setTransactionTypeId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("TRANSTYPEID")));
        this.setTimeStamp(CommonAPI.convertModelDetailToString(modelDetailHM.get("TRANSACTIONDATE")));
        this.setTerminalId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("TERMINALID")));
        this.setUserId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("USERID")));
        this.setUserName(CommonAPI.convertModelDetailToString(modelDetailHM.get("USERNAME")));

        this.setName(CommonAPI.convertModelDetailToString(modelDetailHM.get("PERSONNAME")));
        this.setAddress(CommonAPI.convertModelDetailToString(modelDetailHM.get("ADDRESS")));
        this.setCity(CommonAPI.convertModelDetailToString(modelDetailHM.get("CITY")));
        this.setEmail(CommonAPI.convertModelDetailToString(modelDetailHM.get("EMAIL")));
        this.setPhone(CommonAPI.convertModelDetailToString(modelDetailHM.get("PHONE")));

        this.setOrginalTransactionID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PATRANSIDORIGINAL")));
        this.setOfflineTransactionID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PATRANSIDOFFLINE")));
        this.setDirectVoidQuantity(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("DIRECTVOIDQTY")));
        this.setDirectVoidAmount(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("DIRECTVOIDAMT")));
        this.setIndirectVoidQuantity(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("INDIRECTVOIDQTY")));
        this.setIndirectVoidAmount(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("INDIRECTVOIDAMT")));

        this.setPriceOverrideQuantity(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PRICEOVERRIDEQTY")));
        this.setTransPriceLevel(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("TRANSPRICELVL")));
        this.setDrawerNum(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("DRAWERNUM")));
        this.setQcImportStatus(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("QCIMPORTSTATUS")));

        this.setTransComment(CommonAPI.convertModelDetailToString(modelDetailHM.get("TRANSCOMMENT")));
        this.setTransReferenceNumber(CommonAPI.convertModelDetailToString(modelDetailHM.get("THIRDPARTYTRANSREFNUMBER")));
        this.setEstimatedOrderTime(CommonAPI.convertModelDetailToString(modelDetailHM.get("ESTIMATEDORDERTIME")));

        this.setOrderNumber(CommonAPI.convertModelDetailToString(modelDetailHM.get("ORDERNUM")));
        this.setTransactionLabel(CommonAPI.convertModelDetailToString(modelDetailHM.get("TRANSNAME")));
        this.setTransactionStatusId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PATRANSSTATUSID")));
        this.setTransactionStatus(CommonAPI.convertModelDetailToString(modelDetailHM.get("TRANSSTATUSNAME")));
        this.setOrderTypeId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAORDERTYPEID")));

        this.setTransStatusLastUpdated(CommonAPI.convertModelDetailToString(modelDetailHM.get("TRANSSTATUSUPDATEDTM")));
        this.setLinkedToPaTransactionId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("LINKEDTOPATRANSACTIONID")));
        this.setTicketOpenedDTM(CommonAPI.convertModelDetailToString(modelDetailHM.get("TICKETOPENEDDTM")));
        this.setOrderTypeId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAORDERTYPEID")));
        this.setLinkedFromPaTransactionIdsCsv(CommonAPI.convertModelDetailToString(modelDetailHM.get("LINKEDFROMPATRANSACTIONIDS")));
        this.setLoyaltyAdjustmentTypeId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("LOYALTYADJUSTMENTTYPEID")));
        this.parseLinkedFromPATransactionIdsCsv();

        //set the Terminal Model with the current Terminal
        this.setTerminal(terminalModel);

        //fetch the original Terminal Model from the terminal model on the transaction
        if (this.getOriginalTerminal() == null) {
            this.setOriginalTerminal(new TerminalModel(this.getTerminalId()));
        }

        this.getLoyaltyAccountInfo(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PRIMARYEMPLOYEEID")));       //Set account Model on the attached transaction model
        this.getLoyaltyPointsForTransaction();
        this.getTransactionLineItems();//Set Transaction details Model on the attached transaction model

        this.matchUpLoyaltyPointsAndItemLoyaltyPoints();
        this.prepareLoyaltyPointsRedeemed(true);
        this.prepareLoyaltyPointsEarned();

        return this;

    }

    public static TransactionModel createTransactionModel(HashMap modelDetailHM, TerminalModel terminalModel) throws Exception {
        TransactionModel transactionModel = new TransactionModel();
        transactionModel = transactionModel.setModelProperties(modelDetailHM, terminalModel);
        return transactionModel;
    }

    //get existing account info with Account ID, and terminal ID
    @JsonIgnore
    public void getLoyaltyAccountInfo(Integer primaryEmployeeId) throws Exception {
        if (primaryEmployeeId != null && !primaryEmployeeId.toString().isEmpty()) {
            AccountModel accountModel = AccountModel.getAccountModelByIdForTender(this.getTerminal(), BigDecimal.ZERO, primaryEmployeeId, new AccountQueryParams(true, true));
            accountModel.setTerminal(this.getTerminal());
            this.setLoyaltyAccount(new LoyaltyAccountModel(accountModel, false, this));
        } else {
            //Check the Loyalty Account Point table for the transaction id
            Integer employeeId = LoyaltyAccountModel.getEmployeeIdByTransactionId(this.getId(), this.getTerminal().getId());
            if (employeeId != null && employeeId > 0) {
                AccountModel accountModel = AccountModel.getAccountModelByIdForTender(this.getTerminal(), BigDecimal.ZERO, employeeId, new AccountQueryParams(true, true));
                accountModel.setTerminal(this.getTerminal());
                this.setLoyaltyAccount(new LoyaltyAccountModel(accountModel, false, this));
            }
        }
    }

    //get transaction line items from the database
    @JsonIgnore
    public void getTransactionLineItems() throws Exception {
        ArrayList<HashMap> qCPosTransactionLinesHM = new ArrayList<>();
        //get all transaction line Items
        qCPosTransactionLinesHM = QCPosTransactionLineCollection.getQCPosTransactionLineHM(this, this.getTerminal().getId());
        TransactionLineBuilder.getTransactionLines(qCPosTransactionLinesHM, this);
    }

    @JsonIgnore
    public void getLoyaltyPointsForTransaction() throws Exception {
        if (this.getLoyaltyAccount() != null) {
            //fetch any points that were earned for transaction
            this.setLoyaltyPointsAll(LoyaltyPointCollection.getAllLoyaltyPointsForEmployeeAndTransactionId(this.getLoyaltyAccount().getId(), this.getId(), this.getTerminal().getId()).getCollection());
        }
    }

    @JsonIgnore
    public void getAvailableQCDiscounts() throws Exception {
        for (TenderLineItemModel tenderLineItemModel : this.getTenders()) {
            if (tenderLineItemModel.getTender() != null && tenderLineItemModel.getTender().getTenderTypeId() != null) {
                if (tenderLineItemModel.getAccount() != null) {

                    QcDiscountCollection qcDiscountsForTender = QcDiscountCollection.getAvailableQcDiscounts(tenderLineItemModel, this, true);

                    tenderLineItemModel.getAccount().setQcDiscountsAvailable(qcDiscountsForTender.getCollection());
                }
            }
        }
    }

    /**
     * Check whether the user ID is in QC_QuickChargeUsers.
     * Ref HPQC bug 507
     *
     * @param _id The user id
     * @return true if the user ID is in the table, active, and not locked
     *         false otherwise or if anything goes wrong
     * @throws Exception
     */
    public String getUserRevenueCenters(final int _id) throws Exception {
        DataManager dm = new DataManager();
        ArrayList<HashMap> result = dm.parameterizedExecuteQuery("data.posapi30.doesUserExist", new Object[]{_id}, PosAPIHelper.getLogFileName(getTerminal().getId()), true);
        String rc = "";
        if (result.size() > 0) {
            rc = result.get(0).get("REVENUECENTERIDS").toString();
        }
        return rc;
    }

    public String getValidUserName(final int userId) throws Exception {

        DataManager dm = new DataManager();
        ArrayList<HashMap> result = dm.parameterizedExecuteQuery("data.posapi30.getValidUserName", new Object[]{userId}, PosAPIHelper.getLogFileName(this.getTerminal().getId()), true);
        String rc = "";
        String userName = "";
        if (result.size() > 0) {
            rc = result.get(0).get("REVENUECENTERIDS").toString();
            userName = CommonAPI.convertModelDetailToString(result.get(0).get("USERNAME"));
        }

        boolean found = false;
        for (String s : rc.split(",")) {
            if (s.equals("-1") || s.equals(this.getTerminal().getRevenueCenterId().toString())) {
                found = true;
                break;
            }
        }
        if (!found) {
            Logger.logMessage("Missing or invalid UserId", PosAPIHelper.getLogFileName(this.getTerminal().getId()), Logger.LEVEL.TRACE);
            throw new MissingDataException("Missing or invalid UserId", this.getTerminal().getId());
        }

        return userName;
    }

    /**
     * Fetch the Payment Method type and return the Payment Method Type ID
     *
     * @param paymentMethodType
     * @return
     * @throws Exception
     */
    public Integer getValidPaymentMethodTypeId(final String paymentMethodType) throws Exception {
        Integer paymentMethodTypeId = null;
        DataManager dm = new DataManager();
        ArrayList<HashMap> result = dm.parameterizedExecuteQuery("data.posapi30.getValidPaymentMethodTypeId", new Object[]{paymentMethodType}, PosAPIHelper.getLogFileName(this.getTerminal().getId()), true);

        if (result.size() > 0) {
            paymentMethodTypeId = CommonAPI.convertModelDetailToInteger(result.get(0).get("PAYMENTMETHODTYPEID"));
        } else {
            Logger.logMessage("Missing or invalid Payment Method Type Id", PosAPIHelper.getLogFileName(this.getTerminal().getId()), Logger.LEVEL.TRACE);
            throw new PaymentMethodTypeNotFoundException(this.getTerminal().getId());
        }

        return paymentMethodTypeId;
    }

    /**
     * Fetch the CC Authorization Type ID to make sure it's valid
     *
     * @param ccAuthTypeId
     * @throws Exception - throw a custom CcAuthorizationTypeNoteFoundException
     */
    public Integer getValidCcAuthorizationTypeId(final Integer ccAuthTypeId) throws Exception {
        Integer validCcAuthTypeId = null;
        DataManager dm = new DataManager();
        ArrayList<HashMap> result = dm.parameterizedExecuteQuery("data.posapi30.getValidCCAuthTypeId", new Object[]{ccAuthTypeId}, PosAPIHelper.getLogFileName(this.getTerminal().getId()), true);

        if (result.size() > 0) {
            validCcAuthTypeId = CommonAPI.convertModelDetailToInteger(result.get(0).get("CCAUTHORIZATIONTYPEID"));
        } else {
            Logger.logMessage("Missing or Invalid CC Authorization Type Id", PosAPIHelper.getLogFileName(this.getTerminal().getId()), Logger.LEVEL.TRACE);
            throw new CcAuthorizationTypeNotFoundException(this.getTerminal().getId());
        }

        return validCcAuthTypeId;
    }

    @JsonIgnore
    public String getEarliestLinkedTicketOpenedDTM(final String linkedPaTransIdCSV) {
        String eariestRecordFromDB = "";

        DataManager dm = new DataManager();
        ArrayList<HashMap> resultHM = dm.serializeSqlWithColNames("data.posapi30.getEarliestLinkedTicketOpenedDTM", new Object[]{linkedPaTransIdCSV}, null, PosAPIHelper.getLogFileName(this.getTerminal().getId()));

        if (resultHM.size() > 0) {
            eariestRecordFromDB = CommonAPI.convertModelDetailToString(resultHM.get(0).get("TICKETOPENEDDTM"));
        }

        return eariestRecordFromDB;
    }

    /**
     * For a transactions with linkedFromTransactionIds populated, search for the earliest dates among these transactions
     * Returns TicketOpenedDTM and EstimatedOrderTime
     * @param onlineTransIdsCSV
     * @param offlineTransIdsCSV
     * @param offlineTransTerminalId
     * @return
     */
    public ArrayList<HashMap> fetchLinkedFromEarliestDateFields(String onlineTransIdsCSV, String offlineTransIdsCSV, Integer offlineTransTerminalId) {
        ArrayList<HashMap> resultsHM = new ArrayList<>();

        if (onlineTransIdsCSV != null && !onlineTransIdsCSV.isEmpty() ||
                offlineTransIdsCSV != null && !offlineTransIdsCSV.isEmpty()) {

            resultsHM = dm.parameterizedExecuteQuery("data.posapi30.getLinkedFromTransEarliestDateFields",
                    new Object[]{
                            onlineTransIdsCSV,
                            offlineTransIdsCSV,
                            offlineTransTerminalId,
                            (isOfflineMode() ? 0 : 1)

                    }, PosAPIHelper.getLogFileName(this.getTerminal().getId()), true);
        }

        return resultsHM;
    }

    /**
     * Assign the terminal, API Action Enum and Pos Type Enum to the original transaction.
     * This way we can still validate the API action and transaction type
     *
     * @param transactionHM
     * @param terminalModel
     * @throws Exception
     */
    public void prepareTransactionForVoidOrRefund(HashMap transactionHM, TerminalModel terminalModel) throws Exception {
        //map the original transaction again to perform a deep copy
        TransactionModel originalTransaction = TransactionModel.createTransactionModel(transactionHM, terminalModel);
        this.setOriginalTransaction(originalTransaction); //map the Original Transaction with the hashmap returned
        this.getOriginalTransaction().setTerminal(terminalModel);
        this.getOriginalTransaction().setApiActionEnum(this.getApiActionTypeEnum());
        this.getOriginalTransaction().setPosType(this.getPosType());
    }

    /**
     * this goes through each Product Line Item and set the Adjusted amount
     * This method is used for Loyalty Point Calculation
     *
     * @param loyaltyProgramModel
     */
    public void prepareProductLineAdjustedAmounts(LoyaltyProgramModel loyaltyProgramModel) {
        for (ProductLineItemModel productLineItem : this.getProducts()) {
            productLineItem.setAdjAmountLoyalty(BigDecimal.ZERO);
            productLineItem.setAdjExtAmtLoyalty(BigDecimal.ZERO);
            productLineItem.setAdjAmountPercentageLoyalty(BigDecimal.ZERO);

            productLineItem.setAdjExtAmtLoyalty(ProductLineItemModel.getAdjValOfExtendedAmountForProgram(productLineItem, loyaltyProgramModel.getIncludeTax()));

            if (productLineItem.getAdjExtAmtLoyalty().compareTo(BigDecimal.ZERO) != 0) {
                BigDecimal adjustedQuantity = BigDecimal.ONE;

                //set Adjusted Amount For Loyalty
                if (productLineItem.getQuantityForLoyalty() != null && productLineItem.getQuantityForLoyalty().compareTo(BigDecimal.ZERO) == 1) {
                    adjustedQuantity = productLineItem.getQuantityForLoyalty();
                }
                productLineItem.setAdjAmountLoyalty(productLineItem.getAdjExtAmtLoyalty().divide(adjustedQuantity, 10, BigDecimal.ROUND_HALF_UP));

                //set Adjusted Percentage for Loyalty
                if (productLineItem.getAdjAmountLoyalty().compareTo(BigDecimal.ZERO) == 0) {
                    productLineItem.setAdjAmountPercentageLoyalty(BigDecimal.ZERO);
                } else {

                    if (productLineItem.getAmount().compareTo(BigDecimal.ZERO) == 0) {
                        //$0 price products with modifiers
                        //This could also have discounts and rewards on it
                        productLineItem.setAdjAmountPercentageLoyalty(BigDecimal.ONE);
                    } else {
                        productLineItem.setAdjAmountPercentageLoyalty(productLineItem.getAdjAmountLoyalty().divide(productLineItem.getAmount(), 10, BigDecimal.ROUND_HALF_UP));
                    }

                    if (productLineItem.getAdjAmountPercentageLoyalty().compareTo(BigDecimal.ONE) == 1) {  //If the calculated percentage is > 1, set it to 1
                        productLineItem.setAdjAmountPercentageLoyalty(BigDecimal.ONE);
                    }
                }
            }
        }
    }

    /**
     * this goes through each Product Line Item and set the Adjusted amount
     * This method is used for Loyalty Point Calculation
     *
     * @param loyaltyAccrualPolicyModel
     */
    public void prepareProductLineAdjustedAmounts(LoyaltyAccrualPolicyModel loyaltyAccrualPolicyModel) {
        for (ProductLineItemModel productLineItem : this.getProducts()) {
            productLineItem.setAdjAmountLoyalty(BigDecimal.ZERO);
            productLineItem.setAdjExtAmtLoyalty(BigDecimal.ZERO);
            productLineItem.setAdjAmountPercentageLoyalty(BigDecimal.ZERO);
            if (!productLineItem.getProduct().isWeighted()) {
                productLineItem.setQuantityForLoyalty(productLineItem.getQuantity());
            }

            productLineItem.setQuantityLineDetails(new ArrayList<HashMap<BigDecimal, BigDecimal>>());

            productLineItem.setAdjExtAmtLoyalty(ProductLineItemModel.getAdjValOfExtendedAmountForProgram(productLineItem, loyaltyAccrualPolicyModel));

            if (productLineItem.getAdjExtAmtLoyalty().compareTo(BigDecimal.ZERO) != 0) {
                BigDecimal adjustedQuantity = BigDecimal.ONE;

                //set Adjusted Amount For Loyalty
                if (productLineItem.getQuantityForLoyalty() != null && productLineItem.getQuantityForLoyalty().compareTo(BigDecimal.ZERO) == 1) {
                    adjustedQuantity = productLineItem.getQuantityForLoyalty();
                }
                productLineItem.setAdjAmountLoyalty(productLineItem.getAdjExtAmtLoyalty().divide(adjustedQuantity, 10, BigDecimal.ROUND_HALF_UP));

                //set Adjusted Percentage for Loyalty
                if (productLineItem.getAdjAmountLoyalty().compareTo(BigDecimal.ZERO) == 0) {
                    productLineItem.setAdjAmountPercentageLoyalty(BigDecimal.ZERO);
                } else {

                    if (productLineItem.getAmount().compareTo(BigDecimal.ZERO) == 0) {
                        //$0 price products with modifiers
                        //This could also have discounts and rewards on it
                        productLineItem.setAdjAmountPercentageLoyalty(BigDecimal.ONE);
                    } else {
                        productLineItem.setAdjAmountPercentageLoyalty(productLineItem.getAdjAmountLoyalty().divide(productLineItem.getAmount(), 10, BigDecimal.ROUND_HALF_UP));
                    }

                    if (productLineItem.getAdjAmountPercentageLoyalty().compareTo(BigDecimal.ONE) == 1) {  //If the calculated percentage is > 1, set it to 1
                        productLineItem.setAdjAmountPercentageLoyalty(BigDecimal.ONE);
                    }
                }
            }

            productLineItem.createQuantityLineCalculations(); //Create the quantity calculations for Loyalty
        }
    }

    /**
     * this goes through each Product Line Item and set the Adjusted amount
     * This method is used by LoyaltyRewardApplication
     */
    public void prepareProductLineAdjustedAmounts() {
        for (ProductLineItemModel productLineItem : this.getProducts()) {
            productLineItem.setAdjAmountLoyalty(BigDecimal.ZERO);
            productLineItem.setAdjExtAmtLoyalty(BigDecimal.ZERO);
            productLineItem.setQuantityForLoyalty(productLineItem.getQuantity());
            productLineItem.setAdjExtAmtLoyalty(ProductLineItemModel.getAdjValOfExtendedAmountForRewardApplication(productLineItem, false));

            BigDecimal tempProductAmount = productLineItem.getExtendedAmount().divide(productLineItem.getQuantity(), 4, BigDecimal.ROUND_HALF_UP);
            BigDecimal tempProductQty = productLineItem.getQuantity();

            //If the product line adj extended amount is > 0
            if (productLineItem.getAdjExtAmtLoyalty().compareTo(BigDecimal.ZERO) != 0) {
                BigDecimal adjustedQuantity = BigDecimal.ONE;

                if (productLineItem.getProduct().isWeighted()) {
                    productLineItem.setQuantityForLoyalty(BigDecimal.ONE);

                    //set Adjusted Amount For Loyalty
                    if (productLineItem.getQuantityForLoyalty() != null && productLineItem.getQuantityForLoyalty().compareTo(BigDecimal.ZERO) == 1) {
                        adjustedQuantity = productLineItem.getQuantityForLoyalty();
                    }
                    productLineItem.setAdjAmountLoyalty(productLineItem.getAdjExtAmtLoyalty().divide(adjustedQuantity, 10, BigDecimal.ROUND_HALF_UP));

                } else {
                    //decrement the Quantity For Loyalty if there was a Discount Quantity restriction
                    //set Adjusted Amount For Loyalty
                    if (productLineItem.getQuantityForLoyalty() != null && productLineItem.getQuantityForLoyalty().compareTo(BigDecimal.ZERO) == 1) {
                        adjustedQuantity = productLineItem.getQuantityForLoyalty();
                    }

                    //Calculate the adjusted quantity for a product with a quantity > 1 with an discount quantity restriction applied
                    BigDecimal adjustedDqrQuantity = BigDecimal.ZERO;
                    if (productLineItem.getDiscounts().size() > 0) {
                        for (ItemDiscountModel itemDiscountModel : productLineItem.getDiscounts()) {

                            //if the discountable quantity is less than the product line quantity,
                            if (itemDiscountModel.getEligibleQuantity() != null && itemDiscountModel.getEligibleQuantity().compareTo(productLineItem.getQuantity()) == -1) {
                                //also check if the discount amount covers the entire product amount
                                if (itemDiscountModel.getAmount().abs().compareTo(tempProductAmount) == 0) {
                                    adjustedDqrQuantity = adjustedDqrQuantity.add(itemDiscountModel.getEligibleQuantity());
                                }
                            }
                        }
                    }

                    adjustedQuantity = adjustedQuantity.subtract(adjustedDqrQuantity);
                }

                //At this point, the adjustQuantity will be the product line quantity, minus free product rewards, minus discount quantity restrictions
                if (adjustedQuantity.compareTo(BigDecimal.ZERO) == 1) {
                    productLineItem.setAdjAmountLoyalty(productLineItem.getAdjExtAmtLoyalty().divide(adjustedQuantity, 10, BigDecimal.ROUND_HALF_UP));
                    productLineItem.setQuantityForLoyalty(adjustedQuantity);
                } else {
                    productLineItem.setAdjAmountLoyalty(BigDecimal.ZERO);
                    productLineItem.setQuantityForLoyalty(BigDecimal.ZERO);
                }

                //set Adjusted Percentage for Loyalty
                if (productLineItem.getAmount().compareTo(BigDecimal.ZERO) == 0) {
                    productLineItem.setAdjAmountPercentageLoyalty(BigDecimal.ZERO);
                } else {
                    productLineItem.setAdjAmountPercentageLoyalty(productLineItem.getAdjAmountLoyalty().divide(productLineItem.getAmount(), 10, BigDecimal.ROUND_HALF_UP));
                }
            }

            //At this point, we have the average amount for loyalty set
            productLineItem.createQuantityLineCalculations(); //Create the quantity calculations for Loyalty
        }
    }

    /**
     * Go through the Products and match up the Item Loyalty Points with their Loyalty Account Points
     * This is used for transaction fetch, voids, and refunds
     */
    public void matchUpLoyaltyPointsAndItemLoyaltyPoints() {
        for (ProductLineItemModel productLineItemModel : this.getProducts()) {
            for (ItemLoyaltyPointModel prodItemLoyaltyPointModel : productLineItemModel.getLoyaltyPointDetails()) {
                for (LoyaltyPointModel loyaltyPointModel : this.getLoyaltyPointsAll()) {

                    if (prodItemLoyaltyPointModel.getLoyaltyAccountPointId().equals(loyaltyPointModel.getId())) { //match up on Loyalt Account Point ID

                        loyaltyPointModel.getPointDetails().add(prodItemLoyaltyPointModel);

                    }
                }
            }
        }
    }

    /**
     * Go through all the Loyalty Account Points for the transaction
     * Exclude any Loyalty Account Points that were created for reward application
     * Display Loyalty Points that were earned by each Loyalty Accrual Policy
     */
    public void prepareLoyaltyPointsEarned() {
        switch (this.getTransactionTypeEnum()) {
            case SALE:
            case VOID:
            case REFUND:
                this.getLoyaltyPointsEarned().clear();
                TreeMap<Integer, Integer> pointsByProgram = new TreeMap<>();

                //sum up the points by Loyalty Program
                for (LoyaltyPointModel loyaltyPointModel : this.getLoyaltyPointsAll()) {
                    if (loyaltyPointModel.getLoyaltyRewardTransLineId() == null || loyaltyPointModel.getLoyaltyRewardTransLineId().toString().isEmpty()) {
                        if (pointsByProgram.containsKey(loyaltyPointModel.getLoyaltyProgram().getId())) {
                            Integer curPoints = pointsByProgram.get(loyaltyPointModel.getLoyaltyProgram().getId());
                            pointsByProgram.put(loyaltyPointModel.getLoyaltyProgram().getId(), loyaltyPointModel.getPoints() + curPoints);
                        } else {
                            pointsByProgram.put(loyaltyPointModel.getLoyaltyProgram().getId(), loyaltyPointModel.getPoints());
                        }
                    }
                }

                //Create the Loyalty Points summed by Loyalty Program for display
                for (Map.Entry<Integer, Integer> programPoint : pointsByProgram.entrySet()) {
                    LoyaltyPointModel summaryLoyaltyPointModel = new LoyaltyPointModel();
                    summaryLoyaltyPointModel.setEmployeeId(this.getLoyaltyAccount().getId());
                    summaryLoyaltyPointModel.setLoyaltyProgram((LoyaltyProgramModel) PosAPIModelCache.getOneObject(this.getTerminal().getId(), CommonAPI.PosModelType.LOYALTY_PROGRAM, programPoint.getKey()));
                    summaryLoyaltyPointModel.getLoyaltyProgram().setTenders(null);
                    summaryLoyaltyPointModel.setPoints(programPoint.getValue());

                    for (LoyaltyPointModel loyaltyPointModel : this.getLoyaltyPointsAll()) {
                        if (loyaltyPointModel.getLoyaltyRewardTransLineId() == null || loyaltyPointModel.getLoyaltyRewardTransLineId().toString().isEmpty()) {
                            if (loyaltyPointModel.getLoyaltyProgram().getId().equals(programPoint.getKey())) {
                                LoyaltyAccrualPolicyPointModel loyaltyAccrualPolicyPointModel = new LoyaltyAccrualPolicyPointModel(loyaltyPointModel);
                                summaryLoyaltyPointModel.getLoyaltyAccrualPolicyPoints().add(loyaltyAccrualPolicyPointModel);
                            }
                        }
                    }

                    this.getLoyaltyPointsEarned().add(summaryLoyaltyPointModel); //add the new Loyalty Point to the Loyalty Points Earned list
                }
        }
    }


    /**
     * Go through all the Loyalty Account Points for the transaction
     * Exclude any Loyalty Account Points that were created for Product Loyalty Points
     */
    public void prepareLoyaltyPointsRedeemed(boolean isFetchTransaction) {
        switch (this.getTransactionTypeEnum()) {
            case SALE:
            case VOID:
            case REFUND:
                this.getLoyaltyPointsRedeemed().clear();

                //now that the Loyalty Reward Points are saved, added them back on the LoyaltyPointsRedeemed List
                for (LoyaltyRewardLineItemModel loyaltyRewardLineItemModel : this.getRewards()) {
                    if (loyaltyRewardLineItemModel.getLoyaltyPoint() != null && loyaltyRewardLineItemModel.getLoyaltyPoint().getRewards() != null) {
                        loyaltyRewardLineItemModel.getLoyaltyPoint().getRewards().clear();
                    }
                }

                List<LoyaltyPointModel> redeemedRewardList = new ArrayList<>();
                HashMap<Integer, LoyaltyProgramModel> appliedLoyaltyPrograms = new HashMap<>();

                if (isFetchTransaction) {
                    //this works for the Get One transaction
                    for (LoyaltyPointModel loyaltyPointModelAll : this.getLoyaltyPointsAll()) {
                        if (loyaltyPointModelAll.getLoyaltyRewardTransLineId() != null && !loyaltyPointModelAll.getLoyaltyRewardTransLineId().toString().isEmpty()) {
                            if (appliedLoyaltyPrograms.containsKey(loyaltyPointModelAll.getLoyaltyProgram().getId())) {
                                for (LoyaltyPointModel loyaltyPointReward : redeemedRewardList) {
                                    if (loyaltyPointReward.getLoyaltyProgram().equals(loyaltyPointModelAll.getLoyaltyProgram())) {
                                        loyaltyPointReward.setPoints(loyaltyPointReward.getPoints() + (loyaltyPointModelAll.getPoints() * -1));
                                    }
                                }

                            } else {
                                LoyaltyPointModel newLoyaltyPoint = new LoyaltyPointModel();
                                newLoyaltyPoint.setLoyaltyProgram(loyaltyPointModelAll.getLoyaltyProgram());
                                newLoyaltyPoint.setPoints(loyaltyPointModelAll.getPoints() * -1);
                                redeemedRewardList.add(newLoyaltyPoint);
                                appliedLoyaltyPrograms.put(loyaltyPointModelAll.getLoyaltyProgram().getId(), loyaltyPointModelAll.getLoyaltyProgram());
                            }
                        }
                    }
                } else { //create the loyaltyPointsRedeemed from the transaction inquire or sale

                    //if the program is already in the summary, add the points

                    for (LoyaltyRewardLineItemModel loyaltyRewardLineItemModel : this.getRewards()) {
                        if (loyaltyRewardLineItemModel.getLoyaltyPoint() != null) {
                            if (appliedLoyaltyPrograms.containsKey(loyaltyRewardLineItemModel.getLoyaltyPoint().getLoyaltyProgram().getId())) {
                                for (LoyaltyPointModel loyaltyPointReward : redeemedRewardList) {
                                    if (loyaltyRewardLineItemModel.getLoyaltyPoint().getLoyaltyProgram().equals(loyaltyPointReward.getLoyaltyProgram())) {
                                        loyaltyPointReward.setPoints(loyaltyPointReward.getPoints() + loyaltyRewardLineItemModel.getLoyaltyPoint().getPoints());
                                        loyaltyPointReward.getRewards().add(loyaltyRewardLineItemModel);
                                    }
                                }
                            } else {
                                LoyaltyPointModel newLoyaltyPoint = new LoyaltyPointModel();
                                    newLoyaltyPoint.setLoyaltyProgram(loyaltyRewardLineItemModel.getLoyaltyPoint().getLoyaltyProgram());
                                    newLoyaltyPoint.setPoints(loyaltyRewardLineItemModel.getLoyaltyPoint().getPoints());
                                    redeemedRewardList.add(newLoyaltyPoint);
                                    appliedLoyaltyPrograms.put(loyaltyRewardLineItemModel.getLoyaltyPoint().getLoyaltyProgram().getId(), loyaltyRewardLineItemModel.getLoyaltyPoint().getLoyaltyProgram());
                            }
                        }
                    }
                }

                //now add on the rewards that were included in the point totals
                for (LoyaltyPointModel loyaltyPointModel : redeemedRewardList) {
                    for (LoyaltyRewardLineItemModel loyaltyRewardLineItemModel : this.getRewards()) {
                        if (loyaltyRewardLineItemModel.getLoyaltyPoint() != null) {
                            if (loyaltyPointModel.getLoyaltyProgram().equals(loyaltyRewardLineItemModel.getLoyaltyPoint().getLoyaltyProgram())) {
                                loyaltyPointModel.getRewards().add(loyaltyRewardLineItemModel);
                            }
                        }
                    }
                }

                this.setLoyaltyPointsRedeemed(redeemedRewardList);

                break;
            default:
                break;
        }
    }

    /**
     * If the QC Tender was paid using a Badge Alias, make sure the Loyalty Account shows the correct Badge Alias as well
     */
    public void checkAndSetLoyaltyAccountBadgeAlias(AccountModel accountModel) {
        if (this.getLoyaltyAccount() != null
                && this.getLoyaltyAccount().getId() != null && !this.getLoyaltyAccount().getId().toString().isEmpty()) {
            if (accountModel != null
                    && accountModel.getId() != null && !accountModel.getId().toString().isEmpty()) {
                if (this.getLoyaltyAccount().getId().equals(accountModel.getId())) {
                    if (accountModel.getBadgeAlias() != null && !accountModel.getBadgeAlias().isEmpty()) {
                        this.getLoyaltyAccount().setBadgeAlias(accountModel.getBadgeAlias());
                    }
                }
            }
        }
    }

    @JsonIgnore
    public boolean hasQcDiscountApplied() {
        boolean qcDiscountApplied = false;
        for (TenderLineItemModel tenderLineItemModel : this.getTenders()) {
            if (tenderLineItemModel.getAccount() != null
                    && tenderLineItemModel.getAccount().getQcDiscountsApplied() != null
                    && !tenderLineItemModel.getAccount().getQcDiscountsApplied().isEmpty()) {

                //QC Discounts are included
                //Figure out how much of the QC Discount is applied to this product
                qcDiscountApplied = true;
            }
        }

        return qcDiscountApplied;
    }

    /**
     * Checks if the transaction is offline
     * and if the Terminal's configuration has Item Profile mapping
     *
     * @return
     */
    public boolean shouldAPICreateQCTransactions() {
        boolean apiShouldCreateQcTransaction = true;

        if (this.hasItemProfileMappings()) {
            apiShouldCreateQcTransaction = false;
        }

        if (this.isOfflineMode()) {
            apiShouldCreateQcTransaction = false;
        }

        //if there are no QC Tenders on the transaction
        if (!this.hasQuickChargeTender() && !this.hasQCQuickChargeTenderOnReceivedOnAccount()) {
            apiShouldCreateQcTransaction = false;
        }

        switch (this.getApiActionTypeEnum()) {
            case VOID:
            case REFUND:
            case REFUND_MANUAL:
            case OPEN:
            case PAYMENT_GATEWAY_INQUIRE:
                /*for voids/refunds, the API does NOT write QC_Transactions
                 * we will defer to the Background Processor to write the QC_Transaction
                 * */
                apiShouldCreateQcTransaction = false;
                break;
        }

        if (this.skipSavingQcTransactions()){
            apiShouldCreateQcTransaction = false;
        }

        return apiShouldCreateQcTransaction;
    }

    /**
     * Determine if this is a ROA transaction
     * and if the ROA line item has a QC Transaction on it
     *
     * @return
     */
    public boolean hasQCQuickChargeTenderOnReceivedOnAccount() {

        boolean hasQcTenderOnRoa = false;

        //special rules for ROA
        switch (this.getApiActionTypeEnum()) {
            case ROA:
            case VOID:
            case REFUND:
            case REFUND_MANUAL:

                if (this.getReceivedOnAccounts() != null && !this.getReceivedOnAccounts().isEmpty()) {

                    //if there are any ROA line items, and the ROA model is marked as "QC", and there is an account, then set QCImportStatus = 0 so the Background processor picks it up.
                    for (ReceivedOnAccountLineItemModel receivedOnAccountLineItemModel : this.getReceivedOnAccounts()) {
                        if (receivedOnAccountLineItemModel.getReceivedOnAccount() != null
                                && receivedOnAccountLineItemModel.getReceivedOnAccount().isQC()
                                && receivedOnAccountLineItemModel.getAccount() != null) {
                            hasQcTenderOnRoa = true;
                        }
                    }
                }
                break;
        }

        return hasQcTenderOnRoa;
    }

    //if any Loyalty Points or rewards were included, clear them off.  Then recalculate what gets refunded depending on the products that were sent in.
    public void removeLoyaltyPoints() {

        if (this.getLoyaltyPointsEarned() != null && !this.getLoyaltyPointsEarned().isEmpty()) {
            Iterator<LoyaltyPointModel> removeEarnedLoyaltyPoints = this.getLoyaltyPointsEarned().iterator();
            while (removeEarnedLoyaltyPoints.hasNext()) {
                LoyaltyPointModel curPointModel = removeEarnedLoyaltyPoints.next();
                removeEarnedLoyaltyPoints.remove();
            }
        }

        if (this.getLoyaltyPointsRedeemed() != null && !this.getLoyaltyPointsRedeemed().isEmpty()) {
            Iterator<LoyaltyPointModel> removeRedeemedLoyaltyPoints = this.getLoyaltyPointsRedeemed().iterator();
            while (removeRedeemedLoyaltyPoints.hasNext()) {
                LoyaltyPointModel curPointModel = removeRedeemedLoyaltyPoints.next();
                removeRedeemedLoyaltyPoints.remove();
            }
        }

        for (ProductLineItemModel productLineItemModel : this.getProducts()) {
            //item LoyaltyPoints
            if (productLineItemModel.getLoyaltyPointDetails() != null && !productLineItemModel.getLoyaltyPointDetails().isEmpty()) {
                Iterator<ItemLoyaltyPointModel> removeItemLoyaltyPoints = productLineItemModel.getLoyaltyPointDetails().iterator();
                while (removeItemLoyaltyPoints.hasNext()) {
                    ItemLoyaltyPointModel curItemPointModel = removeItemLoyaltyPoints.next();
                    removeItemLoyaltyPoints.remove();
                }
            }
            //itemRewards
         /*   if (productLineItemModel.getRewards() != null && !productLineItemModel.getRewards().isEmpty()) {
                Iterator<ItemRewardModel> removeItemReward = productLineItemModel.getRewards().iterator();
                while (removeItemReward.hasNext()) {
                    ItemRewardModel curItemRewardModel = removeItemReward.next();
                    removeItemReward.remove();
                }
            }*/
        }
    }

    @JsonIgnore
    public BigDecimal getTotalQcDiscountAmount() {

        if (totalQcDiscountTotal == null) {
            totalQcDiscountTotal = BigDecimal.ZERO;

            if (this.getTenders() != null && !this.getTenders().isEmpty()) {
                for (TenderLineItemModel tenderLineItemModel : this.getTenders()) {
                    if (tenderLineItemModel.getAccount() != null) {
                        if (tenderLineItemModel.getAccount().getQcDiscountsApplied() != null && !tenderLineItemModel.getAccount().getQcDiscountsApplied().isEmpty()) {
                            for (QcDiscountLineItemModel qcDiscountLineItemModel : tenderLineItemModel.getAccount().getQcDiscountsApplied()) {
                                totalQcDiscountTotal = totalQcDiscountTotal.add(qcDiscountLineItemModel.getExtendedAmount());
                            }
                        }
                    }
                }
            }
        }

        return totalQcDiscountTotal;
    }

    public void setTotalQcDiscountTotal(BigDecimal totalQcDiscountTotal) {
        this.totalQcDiscountTotal = totalQcDiscountTotal;
    }

    @JsonIgnore
    /**
     * For item discounts, look at the productLineItemModel.discounts.  For Combos, only the item discount models are submitted.
     * For Sub-total discounts, look at the transactionModel.discounts
     */
    public BigDecimal getTotalTransactionAmount() {
        BigDecimal totalTransactionAmount = BigDecimal.ZERO;

        //products
        for (ProductLineItemModel productLineItemModel : this.getProducts()) {
            totalTransactionAmount = totalTransactionAmount.add(productLineItemModel.getExtendedAmount());

            for (ItemDiscountModel itemDiscountModel : productLineItemModel.getDiscounts()){
                if (!itemDiscountModel.getDiscount().isSubTotalDiscount()){
                    totalTransactionAmount = totalTransactionAmount.add(itemDiscountModel.getAmount()); //add in details for Item Discounts
                }
            }

            //modifiers
            for (ModifierLineItemModel modifierLineItemModel : productLineItemModel.getModifiers()) {
                totalTransactionAmount = totalTransactionAmount.add(modifierLineItemModel.getExtendedAmount());
            }
        }
        Logger.logMessage("TransactionCalc:  Ttl Products: $" + totalTransactionAmount, PosAPIHelper.getLogFileName(this.getTerminal().getId()), Logger.LEVEL.TRACE);

        //discounts
        if (this.getDiscounts() != null && !this.getDiscounts().isEmpty()) {
            for (DiscountLineItemModel discountLineItemModel : this.getDiscounts()) {
                if (discountLineItemModel.getDiscount().isSubTotalDiscount()){
                    totalTransactionAmount = totalTransactionAmount.add(discountLineItemModel.getExtendedAmount());
                }
            }
            Logger.logMessage("TransactionCalc:  Ttl after Discounts: $" + totalTransactionAmount, PosAPIHelper.getLogFileName(this.getTerminal().getId()), Logger.LEVEL.TRACE);
        }

        //rewards
        if (this.getRewards() != null && !this.getRewards().isEmpty()) {
            for (LoyaltyRewardLineItemModel loyaltyRewardLineItemModel : this.getRewards()) {
                totalTransactionAmount = totalTransactionAmount.add(loyaltyRewardLineItemModel.getExtendedAmount());
            }
            Logger.logMessage("TransactionCalc:  Ttl after Rewards: $" + totalTransactionAmount, PosAPIHelper.getLogFileName(this.getTerminal().getId()), Logger.LEVEL.TRACE);
        }

        //taxes
        if (this.getTaxes() != null && !this.getTaxes().isEmpty()) {
            HashMap<Integer, BigDecimal> taxTotals = new HashMap<>();

            //Group tax amounts by tax Id, then round
            for (TaxLineItemModel taxLineItemModel : this.getTaxes()) {
                if (!taxTotals.containsKey(taxLineItemModel.getTax().getId())){
                    taxTotals.put(taxLineItemModel.getTax().getId(), taxLineItemModel.getExtendedAmount());
                } else {
                    BigDecimal currentAmt = taxTotals.get(taxLineItemModel.getTax().getId());
                    taxTotals.put(taxLineItemModel.getTax().getId(), currentAmt.add(taxLineItemModel.getExtendedAmount()));
                }
            }

            for (Map.Entry<Integer, BigDecimal> taxEntry : taxTotals.entrySet()){
                BigDecimal taxTotal = taxEntry.getValue();
                taxTotal = taxTotal.setScale(2, RoundingMode.HALF_UP);
                totalTransactionAmount = totalTransactionAmount.add(taxTotal);
            }

            Logger.logMessage("TransactionCalc: Ttl after Taxes: $" + totalTransactionAmount, PosAPIHelper.getLogFileName(this.getTerminal().getId()), Logger.LEVEL.TRACE);
        }

        //QC Discounts
        if (this.getTotalQcDiscountAmount() != null && this.getTotalQcDiscountAmount().compareTo(BigDecimal.ZERO) != 0) {
            totalTransactionAmount = totalTransactionAmount.add(this.getTotalQcDiscountAmount());
            Logger.logMessage("TransactionCalc:  Ttl after QC Discounts: $" + totalTransactionAmount, PosAPIHelper.getLogFileName(this.getTerminal().getId()), Logger.LEVEL.TRACE);
        }

        //Service Charges
        if (this.getServiceCharges() != null && !this.getServiceCharges().isEmpty()) {
            for (ServiceChargeLineItemModel serviceChargeLineItemModel : this.getServiceCharges()) {
                totalTransactionAmount = totalTransactionAmount.add(serviceChargeLineItemModel.getExtendedAmount());
            }
            Logger.logMessage("TransactionCalc:  Ttl after Service Charges: $" + totalTransactionAmount, PosAPIHelper.getLogFileName(this.getTerminal().getId()), Logger.LEVEL.TRACE);
        }

        //Gratuities
        if (this.getGratuities() != null && !this.getGratuities().isEmpty()) {
            for (GratuityLineItemModel gratuityLineItemModel : this.getGratuities()) {
                totalTransactionAmount = totalTransactionAmount.add(gratuityLineItemModel.getExtendedAmount());
            }
            Logger.logMessage("TransactionCalc:  Ttl after Gratuities: $" + totalTransactionAmount, PosAPIHelper.getLogFileName(this.getTerminal().getId()), Logger.LEVEL.TRACE);
        }

        //Surcharges
        if (this.getSurcharges() != null && !this.getSurcharges().isEmpty()) {
            for (SurchargeLineItemModel surchargeLineItemModel : this.getSurcharges()) {
                totalTransactionAmount = totalTransactionAmount.add(surchargeLineItemModel.getExtendedAmount());
            }
            Logger.logMessage("TransactionCalc:  Ttl after Surcharges: $" + totalTransactionAmount, PosAPIHelper.getLogFileName(this.getTerminal().getId()), Logger.LEVEL.TRACE);
        }

        return totalTransactionAmount;
    }

    private static void sandboxModeCheck(Integer transactionNumber) throws Exception {

        switch (transactionNumber) {
            case 70009999: //E7000	500	Transaction Not Found	This Exception means that the referenced Transaction was not found.
                throw new TransactionNotFoundException();
        }
    }

    /**
     * Generate the next order number.  Look at the QC_RevenueCenter.PANextOrderNum
     * Take into account the PAMaxOrderNum field.  Reset Order Number to 1 if the order number hits this value.
     *
     * @return
     * @throws Exception
     */
    public Integer generateNextOrderNumber() throws Exception {

        DataManager dm = new DataManager();
        String logFileName = PosAPIHelper.getLogFileName(this.getTerminal().getId());
        Logger.logMessage("Generating Order Number. TransactionModel.generateNextOrderNumber.", logFileName, Logger.LEVEL.DEBUG);

        ArrayList<HashMap> resultsHM = dm.parameterizedExecuteQuery("data.posapi30.GenerateOrderNumberSP",
                new Object[]{
                        this.getTerminal().getRevenueCenterId()

                }, logFileName, true);

        Integer nextOrderNum = null;

        if (resultsHM != null && !resultsHM.isEmpty() && resultsHM.get(0).containsKey("NEXTORDERNUM")) {
            nextOrderNum = CommonAPI.convertModelDetailToInteger(resultsHM.get(0).get("NEXTORDERNUM"));
        }

        return nextOrderNum;
    }

    /**
     * Pass in a transaction ID, either get the previous or next transaction model
     * If the getNextTransaction is true, it will get next, otherwise it will get the previous
     *
     * @param transactionId
     * @param transQueryParams
     * @param getNextTransaction
     * @param terminalModel
     * @return
     * @throws Exception
     */
    public TransactionModel getPreviousOrNextTransaction(Integer transactionId, TransactionQueryParams transQueryParams, Boolean getNextTransaction, TerminalModel terminalModel) throws Exception {
        DataManager dm = new DataManager();

        Integer getNextTran = 0;
        Integer getPrevTran = 0;

        if (getNextTransaction) {
            getNextTran = 1;
        } else {
            getPrevTran = 1;
        }

        //get a single page of models in an array list
        ArrayList<HashMap> transactionList = dm.serializeSqlWithColNames("data.posapi30.getPrevOrNextTransaction",
                new Object[]{
                        transactionId,
                        (transQueryParams.getTerminalId() == null) ? "null" : transQueryParams.getTerminalId(),
                        getPrevTran,
                        getNextTran,
                        (transQueryParams.getStartDate()) == null ? "null" : transQueryParams.getStartDate(),  //6
                        (transQueryParams.getEndDate()) == null ? "null" : transQueryParams.getEndDate(),     //7
                        (transQueryParams.getEmployeeId()) == null ? "null" : transQueryParams.getEmployeeId(),
                        terminalModel.getRevenueCenterId(),
                        (transQueryParams.isOpenOnly()) ? 1 : 0
                },
                null,
                PosAPIHelper.getLogFileName(terminalModel.getId()),
                false
        );

        if (transactionList == null || transactionList.size() == 0) {
            throw new TransactionNotFoundException();
        }

        if (transactionList.size() > 0) {
            HashMap transactionHM = transactionList.get(0);
            CommonAPI.checkIsNullOrEmptyObject(transactionHM.get("ID"), "Transaction Id cannot be found.", terminalModel.getId());

            TransactionModel transactionModel = new TransactionModel();
            transactionModel.setModelProperties(transactionHM, terminalModel);

            //Make sure the original Terminal from the transaction is set
            transactionModel.setTerminal(transactionModel.getOriginalTerminal()); //set the Terminal back to the original terminal from the transaction
            transactionModel.getTerminal().logTransactionTime();
            return transactionModel;
        }

        return null;
    }

    public void checkForOfflineMode() {
        //check if POS is in offline mode
        if ((MMHProperties.getAppSetting("site.application.offline").length() > 0) && (MMHProperties.getAppSetting("site.application.offline").compareToIgnoreCase("yes") == 0)) {
            this.setOfflineMode(true);
        }
    }

    public void populateLoyaltyAccrualPolicyLevelsForProducts() {
        for (ProductLineItemModel productLineItemModel : this.getProducts()) {
            if (!productLineItemModel.getProduct().isAlreadyFetchedAccrualPolicyLevels()) {
                productLineItemModel.getProduct().setLoyaltyAccrualPolicyLevels(new LoyaltyAccrualPolicyLevelCollection().getAllLoyaltyAccrualPolicyLevelsByProductId(productLineItemModel.getProduct().getId(), this.getTerminal().getId()).getCollection());
                productLineItemModel.getProduct().setAlreadyFetchedAccrualPolicyLevels(true);
            }
        }
    }

    /**
     * If linked PATransactionId's are provided in the getLinkedFromPaTransactionIds field, get their TicketOpenedDTM, and EstimatedOrderTime
     * @throws Exception
     */
    public void checkAndSetLinkedFromEarliestDates() throws Exception {

        //If linked PATransactionId's are provided in the getLinkedFromPaTransactionIds field, get their TicketOpenedDTM's
        if (this.getLinkedFromPaTransactionIds() != null && !this.getLinkedFromPaTransactionIds().isEmpty()) {
            Integer offlineTransTerminalId = null;
            ArrayList<Integer> onlineTransIds = new ArrayList<>();
            ArrayList<String> onlineTransIdsStr = new ArrayList<>();
            StringBuilder linkedOnlinePATransIdsSb = new StringBuilder();
            StringBuilder linkedOfflinePATransIdsSb = new StringBuilder();

            //First, organize online and offline into different lists
            for (int i = 0; i < this.getLinkedFromPaTransactionIds().size(); i++) {
                if (this.getLinkedFromPaTransactionIds().get(i) == null || this.getLinkedFromPaTransactionIds().get(i).toString().isEmpty()) {
                    continue;
                }

                if (StringUtils.isNumeric(this.getLinkedFromPaTransactionIds().get(i).toString())) {
                    onlineTransIds.add(Integer.parseInt(this.getLinkedFromPaTransactionIds().get(i).toString()));
                } else {
                    onlineTransIdsStr.add(this.getLinkedFromPaTransactionIds().get(i).toString());
                }
            }

            //create csv list of online trans Ids
            for (int i = 0; i < onlineTransIds.size(); i++) {

                if (onlineTransIds.get(i) != null) {
                    linkedOnlinePATransIdsSb.append(onlineTransIds.get(i));
                }

                if (i < onlineTransIds.size() - 1) {
                    linkedOnlinePATransIdsSb.append(",");
                }
            }

            //create csv list of offline trans Ids
            for (int i = 0; i < onlineTransIdsStr.size(); i++) {

                HashMap<String, Integer> offlinePaTransIdParts = DataFunctions.parseOfflineTransIdFormat(onlineTransIdsStr.get(i), "T");
                Integer localPaTransId = offlinePaTransIdParts.get("localPaTransId");
                offlineTransTerminalId = offlinePaTransIdParts.get("localTerminalId");
                linkedOfflinePATransIdsSb.append(localPaTransId);

                if (i < onlineTransIdsStr.size() - 1) {
                    linkedOfflinePATransIdsSb.append(",");
                }
            }

            String linkedOnlinePATransIdsCSV = linkedOnlinePATransIdsSb.toString();
            String linkedOfflinePATransIdsCSV = linkedOfflinePATransIdsSb.toString();

            if (!linkedOnlinePATransIdsCSV.isEmpty() || !linkedOfflinePATransIdsCSV.isEmpty()) {
                ArrayList<HashMap> resultsHM = this.fetchLinkedFromEarliestDateFields(linkedOnlinePATransIdsCSV, linkedOfflinePATransIdsCSV, offlineTransTerminalId); //Fetch from db
                String ticketOpenedDtmStr = CommonAPI.convertModelDetailToString(resultsHM.get(0).get("TICKETOPENEDDTM"));
                String estimatedOrderTimeStr = CommonAPI.convertModelDetailToString(resultsHM.get(0).get("ESTIMATEDORDERTIME"));

                this.checkAndSetTicketOpenedDTM(ticketOpenedDtmStr);
                this.checkAndSetEstimatedOrderTime(estimatedOrderTimeStr);
            }
        }
    }

    private void checkAndSetTicketOpenedDTM(String ticketOpenedDtmStr){
        if (ticketOpenedDtmStr != null && !ticketOpenedDtmStr.isEmpty()) {
            Date ticketOpenedDtm = TransactionModel.convertStringToValidDateTime(ticketOpenedDtmStr);

            if (ticketOpenedDtm != null) {
                //parse out the correct transaction date on the transaction
                Date transactionDate = TransactionModel.convertStringToValidDateTime(this.getTimeStamp());

                if (transactionDate != null) {
                    if (ticketOpenedDtm.compareTo(transactionDate) == -1) {
                        //transaction date is greater, go with linkedTransDate
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
                        String ticketOpenedDtmFormatted = dateFormat.format(ticketOpenedDtm);
                        this.setTicketOpenedDTM(ticketOpenedDtmFormatted);
                    }
                }
            }
        }
    }

    private void checkAndSetEstimatedOrderTime(String estimatedOrderTimeStr){
        if (estimatedOrderTimeStr != null && !estimatedOrderTimeStr.isEmpty()) {
            Date estimatedOrderTime = TransactionModel.convertStringToValidDateTime(estimatedOrderTimeStr);

            if (estimatedOrderTime != null) {
                //parse out the correct transaction date on the transaction
                Date transactionDate = TransactionModel.convertStringToValidDateTime(this.getTimeStamp());

                if (transactionDate != null) {
                    if (estimatedOrderTime.compareTo(transactionDate) == -1) {
                        //transaction date is greater, go with linkedTransDate
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
                        String estimatedOrderTimeFormatted = dateFormat.format(estimatedOrderTime);
                        this.setEstimatedOrderTime(estimatedOrderTimeFormatted);
                    }
                }
            }
        }
    }

    public static Date convertStringToValidDateTime(String dateTimeStr) {
        boolean dateIsValid = false;
        //Date tempDate = null;
        Date validDate = null;

        try {
            //example we are looking for: 5/25/2017 9:22:07 AM
            SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
            dateFormat.setLenient(false);
            validDate = dateFormat.parse(dateTimeStr);
            dateIsValid = true;


        } catch (Exception ex) {
        }

        if (!dateIsValid) {
            try {
                //example we are looking for: 5/25/2017 9:22:07
                SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
                dateFormat.setLenient(false);
                validDate = dateFormat.parse(dateTimeStr);
                dateIsValid = true;

            } catch (Exception ex) {
            }
        }

        if (!dateIsValid) {
            try {
                //example we are looking for: 5-25-2017 9:22:07:000
                SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss.S");
                dateFormat.setLenient(false);
                validDate = dateFormat.parse(dateTimeStr);
                dateIsValid = true;
            } catch (Exception ex) {
            }
        }

        if (!dateIsValid) {
            try {
                //example we are looking for: 2017-5-25 19:22:07:000
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
                dateFormat.setLenient(false);
                validDate = dateFormat.parse(dateTimeStr);
                dateIsValid = true;
            } catch (Exception ex) {
            }
        }

        return validDate;
    }

    /**
     * Check app.properties for site.posapi.skipSavingQCTransactions
     * This this is set to yes, the POS API will not save QC Transactions.
     * @return
     */
    public boolean skipSavingQcTransactions(){
        boolean skipSavingQcTransactions = false;

        //check if POS is in offline mode
        if ((MMHProperties.getAppSetting("site.posapi.skipSavingQCTransactions").length() > 0) && (MMHProperties.getAppSetting("site.posapi.skipSavingQCTransactions").compareToIgnoreCase("yes") == 0)) {
            skipSavingQcTransactions = true;
        }
        return skipSavingQcTransactions;
    }

    public void fetchRewardMappingsForProducts() throws Exception {
        HashMap<Integer, LoyaltyRewardCollection> pluWithMappedRewards = new HashMap<>();

        for (ProductLineItemModel productLineItemModel : this.getProducts()) {
            if (pluWithMappedRewards.containsKey(productLineItemModel.getProduct().getId())) {
                List<LoyaltyRewardModel> mappedRewards = pluWithMappedRewards.get(productLineItemModel.getProduct().getId()).getCollection();
                productLineItemModel.getProduct().setRewards(mappedRewards);
            } else {
                LoyaltyRewardCollection fetchedLoyaltyRewardCollection = LoyaltyRewardCollection.getRewardsForProductId(productLineItemModel.getProduct().getId(), this.getTerminalId());
                productLineItemModel.getProduct().setRewards(fetchedLoyaltyRewardCollection.getCollection());
                pluWithMappedRewards.put(productLineItemModel.getProduct().getId(), fetchedLoyaltyRewardCollection);
            }

            for (ModifierLineItemModel modifierLineItemModel : productLineItemModel.getModifiers()) {
                if (pluWithMappedRewards.containsKey(modifierLineItemModel.getProduct().getId())) {
                    List<LoyaltyRewardModel> mappedRewards = pluWithMappedRewards.get(modifierLineItemModel.getProduct().getId()).getCollection();
                    modifierLineItemModel.getProduct().setRewards(mappedRewards);
                } else {

                    LoyaltyRewardCollection fetchedLoyaltyRewardCollection = LoyaltyRewardCollection.getRewardsForProductId(modifierLineItemModel.getProduct().getId(), this.getTerminalId());
                    modifierLineItemModel.getProduct().setRewards(fetchedLoyaltyRewardCollection.getCollection());
                    pluWithMappedRewards.put(modifierLineItemModel.getProduct().getId(), fetchedLoyaltyRewardCollection);
                }
            }
        }
    }

    public boolean isTaxDelete(Integer taxId, ProductCollectionCalculation productCollectionCalculation) {

        //check if the tax is the same as any tax deletes in the user's account group
        if( this.getLoyaltyAccount() != null && this.getLoyaltyAccount().getAccountGroup() != null) {
            AccountGroupModel accountGroupModel = this.getLoyaltyAccount().getAccountGroup();

            for(TaxModel taxDeleteModel : accountGroupModel.getTaxDeletes()) {
                Integer taxDeleteID = taxDeleteModel.getId();

                //if taxId is a Tax Delete mapped to the user's account group, return true
                if(taxId.equals(taxDeleteID)) {
                    return true;
                }
            }
        }

        List<TaxModel> taxDeletes = new ArrayList<>();

        //check if the product is the dining option and if it has any Tax Deletes
        for ( ProductCalculation productCalculation : productCollectionCalculation.getCollection() ) {
            if(taxDeletes.size() == 0 && productCalculation.getProductModel().isDiningOption()) {
                taxDeletes = productCalculation.getProductModel().getTaxDeletes();
            }
        }

        //if this tax a tax delete on the dining option product, update the isTaxDelete flag on the TaxCalculation
        for(TaxModel taxDelete : taxDeletes) {
            if(taxDelete.getId().equals(taxId)) {
                return true;
            }
        }

        return false;
    }

    /**
     * linkedFromPaTransactionIdsCsv to linkedFromPaTransactionIds
     * Convert CSV list into ArrayList<Object></>
     */
    public void parseLinkedFromPATransactionIdsCsv() {
        ArrayList<Object> tempObjList = new ArrayList<>();

        if (this.getLinkedFromPaTransactionIdsCsv() != null && !this.getLinkedFromPaTransactionIdsCsv().isEmpty()
                && this.getLinkedFromPaTransactionIdsCsv().contains(",")) {

            Object[] paTransObjArr = this.getLinkedFromPaTransactionIdsCsv().split(",");

            for (int i = 0; i < paTransObjArr.length; i++) {

                if (StringUtils.isNumeric(paTransObjArr[i].toString())) {

                    tempObjList.add(Integer.parseInt(paTransObjArr[i].toString()));

                } else {
                    tempObjList.add(paTransObjArr[i].toString());
                }
            }
        } else {

            if (StringUtils.isNumeric(this.getLinkedFromPaTransactionIdsCsv())) {
                tempObjList.add(Integer.parseInt(getLinkedFromPaTransactionIdsCsv()));
            } else {
                tempObjList.add(getLinkedFromPaTransactionIdsCsv());
            }
        }

        this.setLinkedFromPaTransactionIds(tempObjList);
    }

    /**
     * linkedFromPaTransactionIds to linkedFromPaTransactionIdsCsv
     * Convert CSV ArrayList<Object> into CSV list</>
     */
    public void populateLinkedFromPATransactionIdsCSV(){
        if (this.getLinkedFromPaTransactionIds() != null && this.getLinkedFromPaTransactionIds().size() > 0) {
            StringBuilder linkedPATransIdsb = new StringBuilder();
            if (this.getLinkedFromPaTransactionIds().size() > 0) {
                for (int i = 0; i < this.getLinkedFromPaTransactionIds().size(); i++) {
                    if (this.getLinkedFromPaTransactionIds().get(i) == null){
                        linkedPATransIdsb.append("");
                    } else {
                        linkedPATransIdsb.append(this.getLinkedFromPaTransactionIds().get(i).toString());
                    }

                    if (i < this.getLinkedFromPaTransactionIds().size() - 1) {
                        linkedPATransIdsb.append(",");
                    }
                }
            }
            this.setLinkedFromPaTransactionIdsCsv(linkedPATransIdsb.toString());
        }
    }

    @JsonIgnore
    public BigDecimal getTotalVoucherBalance(){
        BigDecimal totalVoucherBalance = BigDecimal.ZERO;

        for (TenderLineItemModel tenderLineItemModel : this.getTenders()){
            if (tenderLineItemModel.getVoucherCode() != null){
                totalVoucherBalance = totalVoucherBalance.add(tenderLineItemModel.getExtendedAmount());
            }
        }

        return totalVoucherBalance;
    }

    @JsonIgnore
    public Integer getTotalVoucherCount(){
        Integer totalVoucherCount = 0;

        for (TenderLineItemModel tenderLineItemModel : this.getTenders()){
            if (tenderLineItemModel.getVoucherCode() != null){
                totalVoucherCount++;
            }
        }

        return totalVoucherCount;
    }

    public void validateTransactionItemNumbers() throws Exception {

        if (!this.getTerminal().getTerminalModelId().equals(PosAPIHelper.PosTerminalModel.QC_POS.toInt())){
            return;
        }

        TransactionItemNumCollection transactionItemNumCollection = new TransactionItemNumCollection(this);
        transactionItemNumCollection.validate();
    }

    public void validateAndSetTransactionItemNumbers() throws Exception {

        TransactionItemNumCollection transactionItemNumCollection = new TransactionItemNumCollection(this);
        transactionItemNumCollection.validate();
        transactionItemNumCollection.checkAndSetTransactionItemNumbers();
    }

    public void removeZeroDollarVoucherTenders(){
        Logger.logMessage("Removing all voucher tender lines.", PosAPIHelper.getLogFileName(this.getTerminal().getId()), Logger.LEVEL.TRACE);
        for (TenderLineItemModel tenderLineItemModel : this.getTenders()){
            if (tenderLineItemModel.getVoucherCode() != null){
                tenderLineItemModel.setAmount(BigDecimal.ZERO);
                tenderLineItemModel.refreshExtendedAmount();
                tenderLineItemModel.getVoucherCode().setNotes("Vouchers cannot be redeemed for $0");
                this.getInvalidTenders().add(tenderLineItemModel);
            }
        }

        //If there are invalid tenders, remove them from the tenders collection
        for (TenderLineItemModel lineModel : this.getInvalidTenders()) {
            this.getTenders().remove(lineModel);
        }
    }

    public void removeVoucherTenders(){
        Logger.logMessage("Removing all voucher tender lines.", PosAPIHelper.getLogFileName(this.getTerminal().getId()), Logger.LEVEL.TRACE);
        for (TenderLineItemModel tenderLineItemModel : this.getTenders()){
            if (tenderLineItemModel.getVoucherCode() != null){
                this.getInvalidTenders().add(tenderLineItemModel);
            }
        }

        //If there are invalid tenders, remove them from the tenders collection
        for (TenderLineItemModel lineModel : this.getInvalidTenders()) {
            this.getTenders().remove(lineModel);
        }
    }

    //region getters and setters

    //getters, setters
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getId() {
        return id;
    }

    //setter
    public void setId(Integer id) {
        this.id = id;
    }

    //getter
    @ApiModelProperty(value = "Transaction Timestamp", dataType = "String", required = true)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getTimeStamp() {
        return timeStamp;
    }

    //setter
    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    //getter
    //This is necessary for the Cashier shift end
    //    Bug 572 2017/06/14. We'll ignore that comment and suppress the field being written out
    @JsonIgnore
    //@JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getTerminalId() {
        return terminalId;
    }

    //setter
    public void setTerminalId(Integer terminalId) {
        this.terminalId = terminalId;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getTransComment() {
        if (transComment != null && !transComment.isEmpty()){
            if (transComment.length() > 2000){
                transComment = transComment.substring(0, 2000);
            }
        }
        return transComment;
    }

    public void setTransComment(String transComment) {
        this.transComment = transComment;
    }

    @ApiModelProperty(value = "User ID of the user that submitted the transaction ", dataType = "Integer", required = true)
    //@JsonInclude(JsonInclude.Include.NON_EMPTY)
    //06-22-2017 egl - V1 04212, remove UserId from the TransactionModel
    @JsonIgnore
    public Integer getUserId() {
        return userId;
    }

    //setter
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    //getter
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public LoyaltyAccountModel getLoyaltyAccount() {
        return loyaltyAccount;
    }

   /* //setter
    public void setLoyaltyAccount(LoyaltyAccountModel loyaltyAccount) {
        this.loyaltyAccount = loyaltyAccount;
    }*/

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public List<TaxLineItemModel> getTaxes() {
        return taxes;
    }

    public void setTaxes(List<TaxLineItemModel> taxes) {
        this.taxes = taxes;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public List<TenderLineItemModel> getTenders() {
        return tenders;
    }

    public void setTenders(List<TenderLineItemModel> tenders) {
        this.tenders = tenders;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public List<ProductLineItemModel> getProducts() {
        return products;
    }

    public void setProducts(List<ProductLineItemModel> products) {
        this.products = products;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public List<DiscountLineItemModel> getDiscounts() {
        return discounts;
    }

    public void setDiscounts(List<DiscountLineItemModel> discounts) {
        this.discounts = discounts;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public List<LoyaltyRewardLineItemModel> getRewards() {
        return rewards;
    }

    public void setRewards(List<LoyaltyRewardLineItemModel> rewards) {
        this.rewards = rewards;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public List<GratuityLineItemModel> getGratuities() {
        return gratuities;
    }

    public void setGratuities(List<GratuityLineItemModel> gratuities) {
        this.gratuities = gratuities;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public List<ServiceChargeLineItemModel> getServiceCharges() {
        return serviceCharges;
    }

    public void setServiceCharges(List<ServiceChargeLineItemModel> serviceCharges) {
        this.serviceCharges = serviceCharges;
    }

    //getter
    //@JsonIgnore
    public TerminalModel getTerminal() {
        return terminal;
    }

    //setter
    public void setTerminal(TerminalModel terminalModel) {
        this.terminal = terminalModel;
    }

    public void setLoyaltyAccount(LoyaltyAccountModel loyaltyAccount) {
        this.loyaltyAccount = loyaltyAccount;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public List<PaidOutLineItemModel> getPaidOuts() {
        return paidOuts;
    }

    public void setPaidOuts(List<PaidOutLineItemModel> paidOuts) {
        this.paidOuts = paidOuts;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public List<ReceivedOnAccountLineItemModel> getReceivedOnAccounts() {
        return receivedOnAccounts;
    }

    public void setReceivedOnAccounts(List<ReceivedOnAccountLineItemModel> receivedOnAccounts) {
        this.receivedOnAccounts = receivedOnAccounts;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public List<LoyaltyPointModel> getLoyaltyPointsEarned() {
        return loyaltyPointsEarned;
    }

    public void setLoyaltyPointsEarned(List<LoyaltyPointModel> loyaltyPointsEarned) {
        this.loyaltyPointsEarned = loyaltyPointsEarned;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public List<LoyaltyPointModel> getLoyaltyPointsRedeemed() {
        return loyaltyPointsRedeemed;
    }

    public void setLoyaltyPointsRedeemed(List<LoyaltyPointModel> loyaltyPointsRedeemed) {
        this.loyaltyPointsRedeemed = loyaltyPointsRedeemed;
    }

    @JsonIgnore
    public List<LoyaltyPointModel> getLoyaltyPointsAll() {
        return loyaltyPointsAll;
    }

    public void setLoyaltyPointsAll(List<LoyaltyPointModel> loyaltyPointsAll) {
        this.loyaltyPointsAll = loyaltyPointsAll;
    }

    @JsonIgnore
    public List<LoyaltyAdjustmentModel> getLoyaltyAdjustments() {
        return loyaltyAdjustments;
    }

    public void setLoyaltyAdjustments(List<LoyaltyAdjustmentModel> loyaltyAdjustments) {
        this.loyaltyAdjustments = loyaltyAdjustments;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public List<DonationLineItemModel> getDonations() {
        return donations;
    }

    public void setDonations(List<DonationLineItemModel> donations) {
        this.donations = donations;
    }

    @ApiModelProperty(value = "Order Type of the transaction.", dataType = "String", required = false, allowableValues = "Normal Sale, Delivery Sale, Pickup Sale", notes = "Relates to the QC_PAOrderType table")
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    @ApiModelProperty(value = "Terminal Drawer Number", dataType = "Integer", required = true)
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getDrawerNum() {
        return drawerNum;
    }

    public void setDrawerNum(Integer drawerNum) {
        this.drawerNum = drawerNum;
    }

    public Integer getQcImportStatus() {
        return qcImportStatus;
    }

    public void setQcImportStatus(Integer qcImportStatus) {
        this.qcImportStatus = qcImportStatus;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @ApiModelProperty(value = "Original Transaction ID for a Refund.", dataType = "String", required = false, notes = "Relates to the QC_PATransactions.PATransIDOriginal table and field.")
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getOriginalTransactionId() {
        return originalTransactionId;
    }

    public void setOrginalTransactionID(Integer originalTransactionId) {
        this.originalTransactionId = originalTransactionId;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getOfflineTransactionId() {
        return offlineTransactionId;
    }

    public void setOfflineTransactionID(Integer offlineTransactionId) {
        this.offlineTransactionId = offlineTransactionId;

        //if the Offline Transaction ID was saved as a -1, mark the "recordedOffline" field as true
        if (this.offlineTransactionId != null && this.offlineTransactionId.equals(-1)) {
            this.setWasRecordedOffline(true);
        }
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getDirectVoidQuantity() {
        return directVoidQuantity;
    }

    public void setDirectVoidQuantity(Integer directVoidQuantity) {
        this.directVoidQuantity = directVoidQuantity;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public BigDecimal getDirectVoidAmount() {
        return directVoidAmount;
    }

    public void setDirectVoidAmount(BigDecimal directVoidAmount) {
        this.directVoidAmount = directVoidAmount;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getIndirectVoidQuantity() {
        return indirectVoidQuantity;
    }

    public void setIndirectVoidQuantity(Integer indirectVoidQuantity) {
        this.indirectVoidQuantity = indirectVoidQuantity;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public BigDecimal getIndirectVoidAmount() {
        return indirectVoidAmount;
    }

    public void setIndirectVoidAmount(BigDecimal indirectVoidAmount) {
        this.indirectVoidAmount = indirectVoidAmount;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getPriceOverrideQuantity() {
        return priceOverrideQuantity;
    }

    public void setPriceOverrideQuantity(Integer priceOverrideQuantity) {
        this.priceOverrideQuantity = priceOverrideQuantity;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getTransPriceLevel() {
        return transPriceLevel;
    }

    public void setTransPriceLevel(Integer transPriceLevel) {
        this.transPriceLevel = transPriceLevel;
    }

    public Integer getTransactionTypeId() {
        return transactionTypeId;
    }

    public void setTransactionTypeId(Integer transactionTypeId) {
        this.transactionTypeId = transactionTypeId;

        if (this.transactionTypeEnum == null) {
            if (this.transactionTypeId != null && !this.transactionTypeId.toString().isEmpty()) {
                this.transactionTypeEnum = PosAPIHelper.TransactionType.intToTransactionType(this.transactionTypeId);
            }
        }

        if (this.getTransactionTypeEnum() != null) {
            setTransactionType(getTransactionTypeEnum().toString());
        }
    }

    @JsonIgnore
    public PosAPIHelper.TransactionType getTransactionTypeEnum() {
        if (this.transactionTypeId == null) {
            setTransactionTypeId(transactionTypeEnum.toInt());
        }

        if (this.transactionType == null || this.transactionType.isEmpty()) {
            setTransactionType(transactionTypeEnum.toString());
        }
        return transactionTypeEnum;
    }

    public void setTransactionTypeEnum(PosAPIHelper.TransactionType transactionTypeEnum) {
        this.transactionTypeEnum = transactionTypeEnum;
        if (this.transactionTypeId == null) {
            setTransactionTypeId(transactionTypeEnum.toInt());
        }
        if (this.transactionType == null || this.transactionType.isEmpty()) {
            setTransactionType(transactionTypeEnum.toString());
        }
    }

    public void checkTransactionTypeEnum() {
        if (this.transactionTypeEnum == null) {
            if (this.transactionTypeId != null) {
                this.transactionTypeEnum = PosAPIHelper.TransactionType.intToTransactionType(this.transactionTypeId);
            } else {
                this.transactionTypeEnum = PosAPIHelper.TransactionType.SALE;
            }
        }
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;

        if (this.transactionTypeEnum == null) {
            if (this.transactionType != null && !this.transactionType.toString().isEmpty()) {
                this.transactionTypeEnum = PosAPIHelper.TransactionType.stringToTransactionType(this.transactionType);
            }
        }
    }

    @JsonIgnore
    public boolean isOfflineMode() {
        return isOfflineMode;
    }

    public void setOfflineMode(boolean offlineMode) {
        isOfflineMode = offlineMode;
    }

    @JsonIgnore
    public boolean hasQuickChargeTender() {
        return hasQuickChargeTender;
    }

    public void setHasQuickChargeTender(boolean hasQuickChargeTender) {
        this.hasQuickChargeTender = hasQuickChargeTender;
    }

    @JsonIgnore
    public boolean hasCheckTender() {
        return hasCheckTender;
    }

    public void setHasCheckTender(boolean hasCheckTender) {
        this.hasCheckTender = hasCheckTender;
    }

    @JsonIgnore
    public boolean hasCreditCardTender() {
        return hasCreditCardTender;
    }

    public void setHasCreditCardTender(boolean hasCreditCardTender) {
        this.hasCreditCardTender = hasCreditCardTender;
    }

    @JsonIgnore
    public boolean hasCashTender() {
        return hasCashTender;
    }

    public void setHasCashTender(boolean hasCashTender) {
        this.hasCashTender = hasCashTender;
    }

    @JsonIgnore
    public BigDecimal getQcTenderBalance() {
        return qcTenderBalance;
    }

    public void setQcTenderBalance(BigDecimal qcTenderBalance) {
        this.qcTenderBalance = qcTenderBalance;
    }

    @JsonIgnore
    @JsonGetter("isCancelTransactionType")  //The property name was getting changed so I had to add this annotation
    public boolean isCancelTransactionType() {
        return isCancelTransactionType;
    }

    public void setIsCancelTransactionType(boolean cancelTransactionType) {
        isCancelTransactionType = cancelTransactionType;
    }

    @JsonIgnore
    @JsonGetter("isTrainingTransactionType")
    public boolean isTrainingTransactionType() {
        return isTrainingTransactionType;
    }

    public void setIsTrainingTransactionType(boolean trainingTransactionType) {
        isTrainingTransactionType = trainingTransactionType;
    }

    @JsonIgnore
    @JsonGetter("isInquiry")
    public boolean isInquiry() {
        return isInquiry;
    }

    public void setIsInquiry(boolean inquiry) {
        isInquiry = inquiry;
    }

    @JsonIgnore
    public PosAPIHelper.ApiActionType getApiActionTypeEnum() {
        return apiActionTypeEnum;
    }

    public void setApiActionEnum(PosAPIHelper.ApiActionType apiActionTypeEnum) {
        this.apiActionTypeEnum = apiActionTypeEnum;
    }

    @JsonIgnore
    public boolean getHasValidatedLoyaltyAccount() {
        return hasValidatedLoyaltyAccount;
    }

    public void setHasValidatedLoyaltyAccount(boolean hasValidatedLoyaltyAccount) {
        this.hasValidatedLoyaltyAccount = hasValidatedLoyaltyAccount;
    }

    @JsonIgnore
    public boolean hasItemProfileMappings() {

        if (this.getTerminal() != null) {
            if (this.getTerminal().getItemProfileMappings() != null && !this.getTerminal().getItemProfileMappings().isEmpty()) {
                this.setHasItemProfileMappings(true);
            }
        }

        return hasItemProfileMappings;
    }

    public void setHasItemProfileMappings(boolean hasItemProfileMappings) {
        this.hasItemProfileMappings = hasItemProfileMappings;
    }

    @JsonIgnore
    public TransactionModel getOriginalTransaction() {
        return originalTransaction;
    }

    public void setOriginalTransaction(TransactionModel originalTransaction) {
        this.originalTransaction = originalTransaction;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getPickUpDeliveryNote() {
        return pickUpDeliveryNote;
    }

    public void setPickUpDeliveryNote(String pickUpDeliveryNote) {
        this.pickUpDeliveryNote = pickUpDeliveryNote;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getTransReferenceNumber() {
        return transReferenceNumber;
    }

    public void setTransReferenceNumber(String transReferenceNumber) {
        this.transReferenceNumber = transReferenceNumber;
    }

    @JsonIgnore
    public PosAPIHelper.PosType getPosType() {
        return posType;
    }

    public void setPosType(PosAPIHelper.PosType posType) {
        this.posType = posType;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getEndShiftUserId() {
        return endShiftUserId;
    }

    public void setEndShiftUserId(Integer endShiftUserId) {
        this.endShiftUserId = endShiftUserId;
    }

    @JsonIgnore
    public TerminalModel getOriginalTerminal() {
        return originalTerminal;
    }

    public void setOriginalTerminal(TerminalModel originalTerminal) {
        this.originalTerminal = originalTerminal;
    }

    @JsonGetter("recordedOffline")  //The property name was getting changed so I had to add this annotation
    public boolean wasRecordedOffline() {
        return recordedOffline;
    }

    public void setWasRecordedOffline(boolean recordedOffline) {
        this.recordedOffline = recordedOffline;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public String getEstimatedOrderTime() {
        return estimatedOrderTime;
    }

    public void setEstimatedOrderTime(String estimatedOrderTime) {
        this.estimatedOrderTime = estimatedOrderTime;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getClientIdentifier() {
        return clientIdentifier;
    }

    public void setClientIdentifier(String clientIdentifier) {
        this.clientIdentifier = clientIdentifier;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getTransactionLabel() {
        return transactionLabel;
    }

    public void setTransactionLabel(String transactionLabel) {
        this.transactionLabel = transactionLabel;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getTransStatusLastUpdated() {
        return transStatusLastUpdated;
    }

    public void setTransStatusLastUpdated(String transStatusLastUpdated) {
        this.transStatusLastUpdated = transStatusLastUpdated;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public ArrayList<Object> getLinkedFromPaTransactionIds() {
        return linkedFromPaTransactionIds;
    }

    public void setLinkedFromPaTransactionIds(ArrayList<Object> linkedFromPaTransactionIds) {
        this.linkedFromPaTransactionIds = linkedFromPaTransactionIds;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getLinkedToPaTransactionId() {
        return linkedToPaTransactionId;
    }

    public void setLinkedToPaTransactionId(Integer linkedToPaTransactionId) {
        this.linkedToPaTransactionId = linkedToPaTransactionId;
    }

    //@JsonIgnore
    public Integer getTransactionStatusId() {
        return transactionStatusId;
    }

    public void setTransactionStatusId(Integer transactionStatusId) {
        this.transactionStatusId = transactionStatusId;
    }

    @JsonIgnore
    public boolean isCreateOpen() {
        return isCreateOpen;
    }

    public void setCreateOpen(boolean isCreateOpen) {
        this.isCreateOpen = isCreateOpen;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public List<SurchargeLineItemModel> getSurcharges() {
        return surcharges;
    }

    public void setSurcharges(List<SurchargeLineItemModel> surcharges) {
        this.surcharges = surcharges;
    }

    public boolean isUseMealPlanDiscount() {
        return useMealPlanDiscount;
    }

    public void setUseMealPlanDiscount(boolean useMealPlanDiscount) {
        this.useMealPlanDiscount = useMealPlanDiscount;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public List<ComboLineItemModel> getCombos() {
        return combos;
    }

    public void setCombos(List<ComboLineItemModel> combos) {
        this.combos = combos;
    }

    //Invalid Tenders - getter / setter
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public List<TenderLineItemModel> getInvalidTenders() {
        return invalidTenders;
    }

    public void setInvalidTenders(List<TenderLineItemModel> val) {
        this.invalidTenders = val;
    }

    public Integer getOrderTypeId() {
        return orderTypeId;
    }

    public void setOrderTypeId(Integer orderTypeId) {
        this.orderTypeId = orderTypeId;

        if (this.orderTypeId == null){
            this.orderTypeId = PosAPIHelper.OrderType.convertStringToInt("");
        }

        if (this.getOrderType() == null || this.getOrderType().isEmpty()
                && this.getOrderTypeId() != null){
            this.setOrderType(PosAPIHelper.OrderType.convertIntToName(this.orderTypeId));
        }

    }

    @JsonIgnore
    public boolean isPaymentGatewayInquireTransactionType() {
        return paymentGatewayInquireTransactionType;
    }

    public void setPaymentGatewayInquireTransactionType(boolean paymentGatewayInquireTransactionType) {
        this.paymentGatewayInquireTransactionType = paymentGatewayInquireTransactionType;
    }

    @JsonIgnore
    public String getTicketOpenedDTM() {
        return ticketOpenedDTM;
    }

    public void setTicketOpenedDTM(String ticketOpenedDTM) {
        this.ticketOpenedDTM = ticketOpenedDTM;
    }

    //@JsonIgnore
    public Integer getPosKioskTerminalId() {
        return posKioskTerminalId;
    }

    public void setPosKioskTerminalId(Integer posKioskTerminalId) {
        this.posKioskTerminalId = posKioskTerminalId;
    }

    @JsonIgnore
    public boolean getRecordPrinterQueueRecordsLocally() {
        return recordPrinterQueueRecordsLocally;
    }

    public void setRecordPrinterQueueRecordsLocally(boolean recordPrinterQueueRecordsLocally) {
        this.recordPrinterQueueRecordsLocally = recordPrinterQueueRecordsLocally;
    }

    //@JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonIgnore
    public String getLinkedFromPaTransactionIdsCsv() {
        return linkedFromPaTransactionIdsCsv;
    }

    public void setLinkedFromPaTransactionIdsCsv(String linkedFromPaTransactionIdsCsv) {
        this.linkedFromPaTransactionIdsCsv = linkedFromPaTransactionIdsCsv;
    }

    @JsonIgnore
    public AccountQueryParams getAccountQueryParams() {
        return accountQueryParams;
    }

    public void setAccountQueryParams(AccountQueryParams accountQueryParams) {
        this.accountQueryParams = accountQueryParams;
    }

    @JsonIgnore
    public boolean isGrabAndGoTransaction() {
        return isGrabAndGoTransaction;
    }

    public void setGrabAndGoTransaction(boolean grabAndGoTransaction) {
        isGrabAndGoTransaction = grabAndGoTransaction;
    }

    @JsonIgnore
    public boolean hasDonation() {
        return hasDonation;
    }

    public void setHasDonation(boolean hasDonation) {
        this.hasDonation = hasDonation;
    }

    public Integer getLoyaltyAdjustmentTypeId() {
        return loyaltyAdjustmentTypeId;
    }

    public void setLoyaltyAdjustmentTypeId(Integer loyaltyAdjustmentTypeId) {
        this.loyaltyAdjustmentTypeId = loyaltyAdjustmentTypeId;
    }

    @JsonIgnore
    public DiscountInfoCollection getDiscountInfoCollection() {
        return discountInfoCollection;
    }

    public void setDiscountInfoCollection(DiscountInfoCollection discountInfoCollection) {
        this.discountInfoCollection = discountInfoCollection;
    }

    @JsonIgnore
    public boolean isAutoApplyQcDiscounts() {
        return autoApplyQcDiscounts;
    }

    public void setAutoApplyQcDiscounts(boolean autoApplyQcDiscounts) {
        this.autoApplyQcDiscounts = autoApplyQcDiscounts;
    }

    @JsonIgnore
    public boolean isVoucherCalcInProgress() {
        return voucherCalcInProgress;
    }

    public void setVoucherCalcInProgress(boolean voucherCalcInProgress) {
        this.voucherCalcInProgress = voucherCalcInProgress;
    }

    @JsonIgnore
    public Integer getNextTransactionItemNum() {
        Integer highestTransactionItemNum = new TransactionItemNumCollection(this).getMaxTransactionItemNumber();
        return highestTransactionItemNum + 1;
    }

}


