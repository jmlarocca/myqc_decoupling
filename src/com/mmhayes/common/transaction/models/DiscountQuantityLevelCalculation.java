package com.mmhayes.common.transaction.models;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.product.models.ProductCalculation;

import java.math.BigDecimal;
import java.util.*;

/*
 Last Updated (automatically updated by SVN)
 $Author: jmdottavio $: Author of last commit
 $Date: 2018-03-20 11:19:43 -0400 (Tue, 20 Mar 2018) $: Date of last commit
 $Rev: 6640 $: Revision of last commit
*/
public class DiscountQuantityLevelCalculation {
    private Integer departmentId = null;
    private List<DiscountQuantityLevelModel> productDQLs = new ArrayList<DiscountQuantityLevelModel>();
    private List<DiscountQuantityLevelModel> subDeptDQLs = new ArrayList<DiscountQuantityLevelModel>();
    private DiscountQuantityLevelModel deptDQL = null;

    //default constructor
    public DiscountQuantityLevelCalculation() {

    }

    public void addProductDQL( DiscountQuantityLevelModel productDQL ) {
        getProductDQLs().add( productDQL );
    }

    public void addSubDeptDQL( DiscountQuantityLevelModel subDeptDQL ) {
        getSubDeptDQLs().add( subDeptDQL );
    }

    public List<ProductCalculation> getEligibleProducts() {
        //if there is a department level restriction, use that above all else
        if ( getDeptDQL() != null ) {
            return getDeptDQL().getEligibleProducts();
        }

        List<ProductCalculation> eligibleProducts = new ArrayList<>();

        //if deptDQL is null and there's no supDeptDQLS, take all the product DQLs
        if ( getSubDeptDQLs().size() == 0 ) {
            //no product DQLs, no eligible products
            if ( getProductDQLs().size() == 0 ) {
                return eligibleProducts;
            }

            for ( DiscountQuantityLevelModel discountQuantityLevelModel : getProductDQLs() ) {
                eligibleProducts.addAll( discountQuantityLevelModel.getEligibleProducts() );
            }

            return eligibleProducts;
        }

        //if deptDQL is null and no productDQLs, take all subDeptDQLs
        if ( getProductDQLs().size() == 0 ) {
            for ( DiscountQuantityLevelModel discountQuantityLevelModel : getSubDeptDQLs() ) {
                eligibleProducts.addAll( discountQuantityLevelModel.getEligibleProducts() );
            }

            return eligibleProducts;
        }

        //if deptDQL is null, but there are subDeptDQLs and productDQLs...

        //take all subDepDQLs products
        List<Integer> subDeptIDs = new ArrayList<>();
        for ( DiscountQuantityLevelModel discountQuantityLevelModel : getSubDeptDQLs() ) {
            subDeptIDs.add( discountQuantityLevelModel.getSubDepartmentId() );
            eligibleProducts.addAll( discountQuantityLevelModel.getEligibleProducts() );
        }

        //take any productDQLs NOT covered by the subDepDQLs
        for ( DiscountQuantityLevelModel discountQuantityLevelModel : getProductDQLs() ) {
            //don't take this product if it's in a subdept already taken
            if ( subDeptIDs.contains( discountQuantityLevelModel.getEligibleProducts().get( 0 ).getProductModel().getSubDepartmentId() ) ) {
                continue;
            }

            eligibleProducts.addAll( discountQuantityLevelModel.getEligibleProducts() );
        }

        return eligibleProducts;
    }

    public Integer getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }

    public List<DiscountQuantityLevelModel> getProductDQLs() {
        return productDQLs;
    }

    public void setProductDQLs(List<DiscountQuantityLevelModel> productDQLs) {
        this.productDQLs = productDQLs;
    }

    public List<DiscountQuantityLevelModel> getSubDeptDQLs() {
        return subDeptDQLs;
    }

    public void setSubDeptDQLs(List<DiscountQuantityLevelModel> subDeptDQLs) {
        this.subDeptDQLs = subDeptDQLs;
    }

    public DiscountQuantityLevelModel getDeptDQL() {
        return deptDQL;
    }

    public void setDeptDQL(DiscountQuantityLevelModel deptDQL) {
        this.deptDQL = deptDQL;
    }
}




