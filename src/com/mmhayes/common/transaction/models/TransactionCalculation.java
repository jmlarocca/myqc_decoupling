package com.mmhayes.common.transaction.models;

//mmhayes dependencies
import com.mmhayes.common.account.models.AccountGroupModel;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.api.PosApi30.PosAPIHelper.DiscountApplicationResult;
import com.mmhayes.common.combo.collections.ComboAndProductsComparerAsc;
import com.mmhayes.common.combo.collections.ComboCollectionCalculation;
import com.mmhayes.common.combo.collections.ComboTransLineComparerAsc;
import com.mmhayes.common.combo.models.ComboCalculation;
import com.mmhayes.common.combo.models.ComboDetailModel;
import com.mmhayes.common.loyalty.models.LoyaltyRewardModel;
import com.mmhayes.common.product.collections.ProductCollectionCalculation;
import com.mmhayes.common.product.models.*;
import com.mmhayes.common.transaction.collections.TaxCollection;
import com.mmhayes.common.voucher.models.DiscountInfoModel;
import com.mmhayes.common.utils.Logger;

//API dependencies
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.voucher.models.VoucherCalculation;
import sun.rmi.runtime.Log;

//other dependencies
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.stream.Collectors;

import static com.mmhayes.common.api.PosApi30.PosAPIHelper.ApiActionType.REFUND_MANUAL;

/*
Last Updated (automatically updated by SVN)
$Author: eglundin $: Author of last commit
$Date: 2021-09-17 16:25:34 -0400 (Fri, 17 Sep 2021) $: Date of last commit
$Rev: 15401 $: Revision of last commit
*/

public class TransactionCalculation {
    private final int DEFAULT_ROUNDING_MODE = BigDecimal.ROUND_HALF_UP;

    private TransactionModel transactionModel = null;
    private Integer terminalId = null;
    private ProductCollectionCalculation productCollectionCalculation = new ProductCollectionCalculation();
    private ComboCollectionCalculation comboCollectionCalculation = new ComboCollectionCalculation();
    private List<DiscountCalculation> appliedDiscounts = new ArrayList<DiscountCalculation>();
    private List<TaxCalculation> appliedTaxes = new ArrayList<TaxCalculation>();
    private List<ComboCalculation> appliedCombos = new ArrayList<ComboCalculation>();

    //CONSTRUCTORS
    public TransactionCalculation() {

    }

    public TransactionCalculation(TransactionModel transactionModel) throws Exception {
        this(transactionModel, false);
    }

    public TransactionCalculation(TransactionModel transactionModel, boolean allowOverrideProductPrices) throws Exception {
        this.setTransactionModel(transactionModel);
        this.setTerminalId(this.getTransactionModel().getTerminalId());

        //no products, no need to calculate anything
        if ( this.getTransactionModel().getProducts() == null || this.getTransactionModel().getProducts().size() == 0 ) {
            Logger.logMessage("No products found in TransactionCalculation constructor. Skipping calculations.", Logger.LEVEL.DEBUG);
            return;
        }

        //populate product collection calculation
        this.getProductCollectionCalculation().populateCollection(this.getTransactionModel(), this.getTerminalId(), DEFAULT_ROUNDING_MODE, allowOverrideProductPrices);
    }

    //DISCOUNT METHODS

    //MyQC - checks through the eligible QC discounts compared to product mappings for the order to determine valid QC discounts
    public void evaluateQCDiscounts() throws Exception {
        BigDecimal subtotal = this.getProductCollectionCalculation().getProductSubtotal();

        //if there is no loyalty account - no way of knowing who the user is, could be guest checkout, don't check for discounts
        if ( this.getTransactionModel().getLoyaltyAccount() == null || this.getTransactionModel().getLoyaltyAccount().getId() == null ) {
            return;
        }

        Integer employeeId = this.getTransactionModel().getLoyaltyAccount().getId();

        HashMap<String, HashMap> QCDiscountListHM = CommonAPI.getQCDiscounts(employeeId, subtotal, this.getTransactionModel().getTerminal());

        //no QC discounts, return
        if ( QCDiscountListHM.size() == 0 ) {
            return;
        }

        //temporary HM to store all eligible discount data
        HashMap<Integer, DiscountCalculation> discountCalculationMap = new HashMap<Integer, DiscountCalculation>();

        //get out QCDiscount Ids from keys of HM, for looping though easily
        for (Map.Entry<String, HashMap> qcDiscountEntry : QCDiscountListHM.entrySet()) {
            Integer PADiscountId = CommonAPI.convertModelDetailToInteger(qcDiscountEntry.getValue().get("PADISCOUNTID"));
            DiscountCalculation discountCalculation = new DiscountCalculation( PADiscountId, this.getTerminalId(), employeeId );

            //if this transaction is not to use meal plan discounts, then skip consideration of any subtotal coupon discounts
            if ( !this.getTransactionModel().isUseMealPlanDiscount() && discountCalculation.getDiscountModel().isSubTotalDiscount() && (discountCalculation.getDiscountModel().getDiscountTypeId().equals("2") || discountCalculation.getDiscountModel().getDiscountTypeId().equals(2)) ) {
                continue;
            }

            overrideQcDiscountAmountForVouchers(qcDiscountEntry.getValue()); //TODO - test that this works for MyQC
            discountCalculation.setTransactionItemNum(transactionModel.getNextTransactionItemNum());
            discountCalculation.setQCDiscountDetails( qcDiscountEntry.getValue() );
            discountCalculation = this.getProductCollectionCalculation().getDiscountableAmounts( discountCalculation );
            discountCalculation.getDiscountModel().setAmount(CommonAPI.convertModelDetailToBigDecimal(qcDiscountEntry.getValue().get("AMOUNT")));
            discountCalculation.setRoundingMode(DEFAULT_ROUNDING_MODE);

            if ( discountCalculation.getDiscountableAmount().compareTo(BigDecimal.ZERO) > 0 ) {
                discountCalculationMap.put(discountCalculation.getId(), discountCalculation);
            }
        }

        //if there are any discountCalcs
        if ( discountCalculationMap.size() > 0 ) {
            this.selectBestQCDiscount(discountCalculationMap);
        }
    }

    //checks through the discountCalculations made by evaluateQCDiscounts and selects the best one for the customer, then calls applySubtotalDiscount on it
    public void selectBestQCDiscount(HashMap<Integer, DiscountCalculation> discountCalculationMap) throws Exception {
        DiscountCalculation bestDiscount = null;

        //loop through discountCalcs
        for ( HashMap.Entry<Integer, DiscountCalculation> entry: discountCalculationMap.entrySet()) {
            //calculate the total discount
            DiscountCalculation discountCalculation = entry.getValue();
            discountCalculation.calculateDiscountAmount();

            //compare to keep track of best discount
            if ( bestDiscount == null || discountCalculation.getDiscountAmount().compareTo(bestDiscount.getDiscountAmount()) > 0 ) {
                bestDiscount = discountCalculation;
            }
        }

        //discountCalculation.applySubtotalDiscount on the best one
        if ( bestDiscount != null ) {
            initQcDiscountAmountsForVouchers(bestDiscount);

            switch (this.getTransactionModel().getPosType()){
                case MMHAYES_POS:
                case THIRD_PARTY:
                    this.applyDiscountForPOS(bestDiscount);
                    break;
                default:
                    this.applyDiscount(bestDiscount);
                    break;
            }
        }
    }

    public void applyDiscount(DiscountCalculation discountCalculation) throws Exception {
        discountCalculation.applyDiscount(this.getTransactionModel(), this.getProductCollectionCalculation(), null);
        this.getAppliedDiscounts().add(discountCalculation);
    }

    public void applyDiscount(DiscountCalculation discountCalculation, Integer index) throws Exception {
        discountCalculation.applyDiscount(this.getTransactionModel(), this.getProductCollectionCalculation(), index);
        this.getAppliedDiscounts().add(discountCalculation);
    }

    public void applyDiscountForPOS(DiscountCalculation discountCalculation) throws Exception {
        discountCalculation.applyDiscountForPOS(this.getTransactionModel(), this.getProductCollectionCalculation(), null);
        this.getAppliedDiscounts().add(discountCalculation);
    }

    /**
     * Check and apply any available QC Discounts for POS
     *
     * @throws Exception
     */
    public void autoApplyQCDiscountsForPOS() throws Exception {
        Integer employeeId = null;

        if (this.getTransactionModel().getLoyaltyAccount() != null
                && this.getTransactionModel().getLoyaltyAccount().getId() != null
                && !this.getTransactionModel().getLoyaltyAccount().getId().toString().isEmpty()){
            employeeId = this.getTransactionModel().getLoyaltyAccount().getId();
        } else {
            return;
        }

        //Check if any QC Discounts are already applied to the transaction
        for (DiscountLineItemModel discountLineItemModel : this.getTransactionModel().getDiscounts()) {
            if (discountLineItemModel.getQcDiscount() != null) {
                return; //Don't apply any QC Discounts if there are any applied to the transaction
            }
        }

        List<QcDiscountModel> availableQcDiscounts = new ArrayList<>();
        //Find the main account in the tender list.  This is for split tenders
        for (TenderLineItemModel tenderLineItemModel : this.getTransactionModel().getTenders()) {
            if (tenderLineItemModel.getAccount() != null && tenderLineItemModel.getAccount().getId().equals(employeeId)) {
                availableQcDiscounts = tenderLineItemModel.getAccount().getQcDiscountsAvailable();
                break; //take the first one found
            }
        }

        //no QC discounts, return
        if (availableQcDiscounts.size() == 0) {
            return;
        }

        Boolean posaRefund = getTransactionModel().getApiActionTypeEnum().equals(REFUND_MANUAL) &&
                getTransactionModel().getTerminal().getTerminalModelId().equals(PosAPIHelper.PosTerminalModel.POSANYWHERE.toInt());

        //temporary HM to store all eligible discount data
        HashMap<Integer, DiscountCalculation> discountCalculationMap = new HashMap<>();

        for (QcDiscountModel qcDiscountModel : availableQcDiscounts) {
            Integer QCDiscountId = qcDiscountModel.getId();
            Integer PADiscountId = qcDiscountModel.getPaDiscountId();

            //If the terminal is set to not allow Offline Mode for QC Coupon Discounts and Meal Plans
            if (this.getTransactionModel().isOfflineMode() && !this.getTransactionModel().getTerminal().getAllowQcCouponDiscountOffline()) {
                if (qcDiscountModel.getDiscountTypeId().equals(PosAPIHelper.DiscountType.COUPON.toInt())) { //QC Discount is Coupon
                    continue;
                }
            }

            //If the terminal is NOT set to Auto Apply QC Coupon discounts
            if (!this.getTransactionModel().getTerminal().isAutoApplyQCCouponDiscounts()) {

                if (this.getTransactionModel().isVoucherCalcInProgress()){
                    //if recalculating for vouchers, allow
                } else {
                    if (qcDiscountModel.getDiscountTypeId().equals(PosAPIHelper.DiscountType.COUPON.toInt())) { //QC Discount is Coupon
                        continue;
                    }
                }
            }

            DiscountCalculation discountCalculation = new DiscountCalculation(PADiscountId, this.getTerminalId(), employeeId);

            //if this transaction is not to use meal plan discounts, then skip consideration of any subtotal coupon discounts
            if (!this.getTransactionModel().isUseMealPlanDiscount() && discountCalculation.getDiscountModel().isSubTotalDiscount() && discountCalculation.getDiscountModel().getDiscountTypeId().equals("2")) {
                continue;
            }

            overrideQcDiscountAmountForVouchers(qcDiscountModel);
            discountCalculation.setTransactionItemNum(transactionModel.getNextTransactionItemNum());
            discountCalculation.setQCDiscountId(QCDiscountId);
            discountCalculation.setQcDiscountModel(qcDiscountModel);
            discountCalculation = this.getProductCollectionCalculation().getDiscountableAmounts(discountCalculation);
            discountCalculation.validatePOSSubtotalDiscountRules(this); //validate POS subtotal rules
            discountCalculation.setAutoAppliedOnCurrentInquire(true);
            discountCalculation.getDiscountModel().setAmount(qcDiscountModel.getAmount());
            discountCalculation.setRoundingMode(DEFAULT_ROUNDING_MODE);
            if (discountCalculation.validatePOSDiscountRules(this, posaRefund) && discountCalculation.getDiscountableAmount().compareTo(BigDecimal.ZERO) > 0) {
                discountCalculationMap.put(discountCalculation.getId(), discountCalculation);
            }
        }

        //if there are any discountCalcs
        if (discountCalculationMap.size() > 0) {
            this.selectBestQCDiscount(discountCalculationMap);
        }
    }

    /**
     * Check and apply any available QC Discounts for Third Party POS
     *
     * @throws Exception
     */
    public void autoApplyQCDiscountsForThirdParty() throws Exception {
        Integer employeeId = null;

        if (this.getTransactionModel().getLoyaltyAccount() != null
                && this.getTransactionModel().getLoyaltyAccount().getId() != null
                && !this.getTransactionModel().getLoyaltyAccount().getId().toString().isEmpty()){
            employeeId = this.getTransactionModel().getLoyaltyAccount().getId();
        } else {
            return;
        }

        //If the terminal is NOT set to Auto Apply QC discounts on transaction sale
        if (!this.getTransactionModel().getTerminal().isAutoApplyQCDiscountOnSaleRequest()) {
            return;
        }

        //Check if any QC Discounts are already applied to the transaction
        for (DiscountLineItemModel discountLineItemModel : this.getTransactionModel().getDiscounts()) {
            if (discountLineItemModel.getQcDiscount() != null) {
                return; //Don't apply any QC Discounts if there are any applied to the transaction
            }
        }

        List<QcDiscountModel> availableQcDiscounts = new ArrayList<>();
        List<QcDiscountLineItemModel> appliedQcDiscounts = new ArrayList<>();

        //Find the main account in the tender list.  This is for split tenders
        for (TenderLineItemModel tenderLineItemModel : this.getTransactionModel().getTenders()) {
            if (tenderLineItemModel.getAccount() != null && tenderLineItemModel.getAccount().getId().equals(employeeId)) {
                availableQcDiscounts = tenderLineItemModel.getAccount().getQcDiscountsAvailable();
                break; //take the first one found
            }
        }

        //no QC discounts, return
        if (availableQcDiscounts.isEmpty()) {
            return;
        }

        if (!appliedQcDiscounts.isEmpty()){
            return;
        }

        if (!transactionModel.getProducts().isEmpty()){
            //only auto apply if there are not products
            return;
        }

        Boolean posaRefund = getTransactionModel().getApiActionTypeEnum().equals(REFUND_MANUAL) &&
                getTransactionModel().getTerminal().getTerminalModelId().equals(PosAPIHelper.PosTerminalModel.POSANYWHERE.toInt());

        //temporary HM to store all eligible discount data
        HashMap<Integer, DiscountCalculation> discountCalculationMap = new HashMap<>();

        for (QcDiscountModel qcDiscountModel : availableQcDiscounts) {
            Integer QCDiscountId = qcDiscountModel.getId();
            Integer PADiscountId = qcDiscountModel.getPaDiscountId();

            //If the terminal is set to not allow Offline Mode for QC Coupon Discounts and Meal Plans
            if (this.getTransactionModel().isOfflineMode() && !this.getTransactionModel().getTerminal().getAllowQcCouponDiscountOffline()) {
                if (qcDiscountModel.getDiscountTypeId().equals(PosAPIHelper.DiscountType.COUPON.toInt())) { //QC Discount is Coupon
                    continue;
                }
            }

            //If the terminal is NOT set to Auto Apply QC Coupon discounts
            if (!this.getTransactionModel().getTerminal().isAutoApplyQCCouponDiscounts()) {
                if (qcDiscountModel.getDiscountTypeId().equals(PosAPIHelper.DiscountType.COUPON.toInt())) { //QC Discount is Coupon
                    continue;
                }
            }

            DiscountCalculation discountCalculation = new DiscountCalculation(PADiscountId, this.getTerminalId(), employeeId);

            //if this transaction is not to use meal plan discounts, then skip consideration of any subtotal coupon discounts
            if (!this.getTransactionModel().isUseMealPlanDiscount() && discountCalculation.getDiscountModel().isSubTotalDiscount() && discountCalculation.getDiscountModel().getDiscountTypeId().equals("2")) {
                continue;
            }

            discountCalculation.setQCDiscountId(QCDiscountId);
            discountCalculation.setQcDiscountModel(qcDiscountModel);
            discountCalculation = this.getProductCollectionCalculation().getDiscountableAmounts(discountCalculation);
            discountCalculation.validatePOSSubtotalDiscountRules(this); //validate POS subtotal rules
            discountCalculation.setAutoAppliedOnCurrentInquire(true);
            discountCalculation.getDiscountModel().setAmount(qcDiscountModel.getAmount());
            discountCalculation.setRoundingMode(DEFAULT_ROUNDING_MODE);

            discountCalculationMap.put(discountCalculation.getId(), discountCalculation);
        }

        //if there are any discountCalcs
        if (discountCalculationMap.size() > 0) {
            this.selectBestQCDiscount(discountCalculationMap);
        }
    }

    public void applyAttachedDiscounts() throws Exception {

        if (this.getTransactionModel().getDiscounts().size() == 0) {
            return;
        }

        Boolean posaRefund = getTransactionModel().getApiActionTypeEnum().equals(REFUND_MANUAL) &&
                getTransactionModel().getTerminal().getTerminalModelId().equals(PosAPIHelper.PosTerminalModel.POSANYWHERE.toInt());

        ArrayList<DiscountCalculation> discountCalculations = new ArrayList<>();

        //Loop through the Discounts included on the transaction
        for (DiscountLineItemModel discountLineItemModel : this.getTransactionModel().getDiscounts()) {

            //if the eligible amount is filled in on the Discount Line Item Model, it means it was already applied
            if (discountLineItemModel.isAppliedToTransaction()) {
                if (!discountLineItemModel.isAutoAppliedOnCurrentInquire()){ //For Auto QC discounts that were just applied, preserve the application result
                    discountLineItemModel.setApplicationResult(null); //clear out the application result for discounts previously applied
                }
                continue;
            }

            DiscountCalculation discountCalculation = new DiscountCalculation(discountLineItemModel, this.getTerminalId());
            discountCalculation = this.getProductCollectionCalculation().getDiscountableAmounts(discountCalculation);  //This is where Discount Quantity Restrictions are checked
            discountCalculation.validatePOSSubtotalDiscountRules(this); //validate POS subtotal rules
            discountCalculation.validatePOSDiscountRules(this, posaRefund); //validate POS transaction level rules
            discountCalculation.getDiscountModel().setAmount(discountLineItemModel.getDiscount().getAmount());
            discountCalculation.setRoundingMode(DEFAULT_ROUNDING_MODE);
            discountCalculations.add(discountCalculation);
        }

        //Remove any discounts that have NOT been applied
        //A New DiscountLineItemModel will be created when the discount is applied
        this.getTransactionModel().getDiscounts().removeIf(d -> !d.isAppliedToTransaction());

        //Apply Item Discounts
        for (DiscountCalculation discountCalculation : discountCalculations) {
            if (!discountCalculation.getDiscountModel().isSubTotalDiscount()) {
                this.applyDiscountForPOS(discountCalculation);
            }
        }

        //Apply Subtotal Discounts
        for (DiscountCalculation discountCalculation : discountCalculations) {
            if (discountCalculation.getDiscountModel().isSubTotalDiscount()) {
                this.applyDiscountForPOS(discountCalculation);
            }
        }
    }

    //updates productCalculation discountsApplied list with information about price reductions from discounts (previously calculated), for proper tax and discount calculations
    public void setDiscountPriceReductionRecords() throws Exception {
        //if no discounts applied just return
        if ( this.getTransactionModel().getDiscounts() == null || this.getTransactionModel().getDiscounts().size() == 0 ) {
            return;
        }

        //the product lines on the transactionModel are the same in order and number as the productCollectionCalculation collection
        int productIndex = 0;
        for (ProductLineItemModel productLineItemModel : this.getTransactionModel().getProducts() ) {
            //if no discounts are applied to this product line, skip it
            if ( productLineItemModel.getDiscounts().size() == 0 ) {
                productIndex++;
                continue;
            }

            for ( ItemDiscountModel itemDiscountModel : productLineItemModel.getDiscounts() ) {
                DiscountModel discountModel = itemDiscountModel.getDiscount();
                Integer discountId = discountModel.getId();
                String taxIds = discountModel.getTaxIds();
                BigDecimal discountableAmount = itemDiscountModel.getEligibleAmount().setScale(4, DEFAULT_ROUNDING_MODE);
                BigDecimal discountAmount = new BigDecimal("-1").multiply(itemDiscountModel.getAmount()).setScale(4, DEFAULT_ROUNDING_MODE);

                //if the discount doesn't have any levels, default this to the quantity
                BigDecimal quantity = productLineItemModel.getQuantity();
                if (discountModel.hasDiscountQuantityRestrictions()){
                    quantity = itemDiscountModel.getEligibleQuantity();
                }
                this.getProductCollectionCalculation().getCollection().get(productIndex).applyPriceReduction(true, discountId, discountAmount, discountableAmount, quantity, taxIds, !discountModel.isSubTotalDiscount());
            }

            productIndex++;
        }
    }

    //REWARD METHODS

    //updates productCalculation rewardsApplied list with information about price reductions from rewards (previously calculated), for proper tax calculations
    public void setRewardPriceReductionRecords() throws Exception {
        //if no rewards applied just return
        if ( this.getTransactionModel().getRewards() == null || this.getTransactionModel().getRewards().size() == 0 ) {
            return;
        }

        //the product lines on the transactionModel are the same in order and number as the productCollectionCalculation collection
        int productIndex = 0;
        for (ProductLineItemModel productLineItemModel : this.getTransactionModel().getProducts() ) {
            //if no rewards are applied to this product line, skip it
            if ( productLineItemModel.getRewards().size() == 0 ) {
                productIndex++;
                continue;
            }

            for ( ItemRewardModel itemRewardModel : productLineItemModel.getRewards() ) {
                LoyaltyRewardModel loyaltyRewardModel = itemRewardModel.getReward();
                Integer rewardId = loyaltyRewardModel.getId();
                String taxIds = itemRewardModel.getReward().getTaxIds();
                BigDecimal rewardableAmount = itemRewardModel.getEligibleAmount().setScale(4, DEFAULT_ROUNDING_MODE);
                BigDecimal rewardAmount = new BigDecimal("-1").multiply(itemRewardModel.getAmount()).setScale(4, DEFAULT_ROUNDING_MODE);
                BigDecimal quantity =  (loyaltyRewardModel.getRewardTypeId() == 4) ? BigDecimal.ONE : productLineItemModel.getQuantity();
                this.getProductCollectionCalculation().getCollection().get(productIndex).applyPriceReduction(false, rewardId, rewardAmount, rewardableAmount, quantity, taxIds, false);
            }

            productIndex++;
        }
    }

    //TAX METHODS

    //returns a list of taxIds that are both mapped to the revenue center of the terminal and to a product on the transaction
    public List<String> getValidTaxIds() throws Exception {
        //get all of the taxIds mapped to the revenue center of the terminal for this transaction
        List<String> terminalTaxIds = TaxCollection.getTaxIdsForTerminal(this.getTerminalId());
        List<String> transactionTaxIds = new ArrayList<String>();

        //if no valid taxes, just return empty arraylist
        if ( terminalTaxIds == null || terminalTaxIds.size() == 0 ) {
            return transactionTaxIds;
        }

        //check through each product and modifiers taxIds and compare against those valid for the revenue center
        for ( ProductCalculation productCalculation : this.getProductCollectionCalculation().getCollection() ) {
            String parentProductTaxIds = productCalculation.getProductModel().getTaxIds();

            if ( parentProductTaxIds != null && parentProductTaxIds.length() > 0 ) {
                ArrayList<String> parentProductTaxIdArr = CommonAPI.convertCommaSeparatedStringToArrayList(parentProductTaxIds, true);

                for ( String taxId : parentProductTaxIdArr ) {
                    if ( terminalTaxIds.contains(taxId) && !transactionTaxIds.contains(taxId) ) {
                        transactionTaxIds.add(taxId);
                    }
                }
            }

            if ( productCalculation.getModifiers() != null && productCalculation.getModifiers().size() > 0 ) {
                for (ModifierCalculation modifierCalculation : productCalculation.getModifiers() ) {
                    //no point in attempting to calculate a tax if the price is 0
                    if ( modifierCalculation.getProductModel().getPrice() == null || modifierCalculation.getProductModel().getPrice().compareTo(BigDecimal.ZERO) == 0 ) {
                        continue;
                    }

                    String modifierProductTaxIds = modifierCalculation.getProductModel().getTaxIds();

                    if ( modifierProductTaxIds != null && modifierProductTaxIds.length() > 0 ) {
                        ArrayList<String> modifierProductTaxIdArr = CommonAPI.convertCommaSeparatedStringToArrayList(modifierProductTaxIds, true);

                        for ( String taxId : modifierProductTaxIdArr ) {
                            if ( terminalTaxIds.contains(taxId) && !transactionTaxIds.contains(taxId) ) {
                                transactionTaxIds.add(taxId);
                            }
                        }
                    }
                }
            }
        }

        return transactionTaxIds;
    }

    //attempts to calculate and apply all valid taxes for the transaction, considering discounts and rewards
    public void applyTaxes() throws Exception {
        List<String> taxIds = this.getValidTaxIds();

        //just return if there are no valid taxIds
        if ( taxIds.size() == 0 ) {
            return;
        }

        Boolean posaRefund = getTransactionModel().getApiActionTypeEnum().equals(REFUND_MANUAL) &&
                getTransactionModel().getTerminal().getTerminalModelId().equals(PosAPIHelper.PosTerminalModel.POSANYWHERE.toInt());

        //for all of the valid taxes - find the taxable amount and then apply the tax to the transaction model
        for ( String taxId : taxIds ) {
            TaxCalculation taxCalculation = this.getProductCollectionCalculation().getTaxableAmounts(Integer.parseInt(taxId), this.getTerminalId());
            taxCalculation.setRoundingMode(DEFAULT_ROUNDING_MODE);

            taxCalculation.applyTax(this.getTransactionModel(), this.getProductCollectionCalculation(), posaRefund);
            this.getAppliedTaxes().add(taxCalculation);
        }

        //build ItemTaxModels for discounts
        if ( ( this.getTransactionModel().getDiscounts().size() == 0 && this.getTransactionModel().getRewards().size() == 0 ) || this.getAppliedTaxes().size() == 0 ) {
            return;
        }

        HashMap<String, TaxModel> appliedTaxRateHM = new HashMap<String, TaxModel>();
        for ( TaxCalculation taxCalculation : this.getAppliedTaxes() ) {
            appliedTaxRateHM.put(taxCalculation.getTaxModel().getId().toString(), taxCalculation.getTaxModel());
        }

        //TODO this section needs to be refactored, didn't have time to do it concisely

        if ( this.getTransactionModel().getDiscounts().size() > 0 ) {
            //build discounttax hashmap - to hold all price reductions associated with each discount considering tax mappings
            HashMap<String, HashMap<String, BigDecimal>> discountTaxReductionSummaryHM = new HashMap<String, HashMap<String, BigDecimal>>();
            for ( ProductCalculation productCalculation : this.getProductCollectionCalculation().getCollection() ) {
                //look through the discounts applied - no need to check discount mappings to the product, they were applied already
                for ( HashMap discountAppliedHM : productCalculation.getDiscountTaxes() ) {
                    String discountId = CommonAPI.convertModelDetailToString(discountAppliedHM.get("id"));
                    String taxId = CommonAPI.convertModelDetailToString(discountAppliedHM.get("taxId"));
                    BigDecimal discountAmount = CommonAPI.convertModelDetailToBigDecimal(discountAppliedHM.get("amount"));

                    //build out a default hashmap if its the first time looking at this discount
                    if ( !discountTaxReductionSummaryHM.containsKey(discountId) ) {
                        discountTaxReductionSummaryHM.put(discountId, new HashMap<String, BigDecimal>());
                    }

                    //holds information for a single tax pertaining to a discount
                    HashMap<String, BigDecimal> discountTaxReductionSummaryHMItem = discountTaxReductionSummaryHM.get(discountId);

                    //get out the list of valid taxes for this discount
                    if ( discountTaxReductionSummaryHMItem.containsKey(taxId) ) {
                        BigDecimal existingDiscountAmount = discountTaxReductionSummaryHMItem.get(taxId);
                        discountTaxReductionSummaryHMItem.put( taxId, discountAmount.add(existingDiscountAmount) );
                    } else {
                        discountTaxReductionSummaryHMItem.put( taxId, discountAmount );
                    }
                }

                //continue if this product has no modifiers
                if ( productCalculation.getModifiers() == null || productCalculation.getModifiers().size() == 0 ) {
                    continue;
                }

                for ( ModifierCalculation modifierCalculation : productCalculation.getModifiers() ) {
                    if ( modifierCalculation.getDiscountsApplied().size() == 0 ) {
                        continue;
                    }

                    //look through the discounts applied - no need to check mappings to the product, they were applied already
                    for ( HashMap discountAppliedHM : modifierCalculation.getDiscountTaxes() ) {
                        String discountId = CommonAPI.convertModelDetailToString(discountAppliedHM.get("id"));
                        String taxId = CommonAPI.convertModelDetailToString(discountAppliedHM.get("taxId"));
                        BigDecimal discountAmount = CommonAPI.convertModelDetailToBigDecimal(discountAppliedHM.get("amount"));

                        //build out a default hashmap if its the first time looking at this discount
                        if ( !discountTaxReductionSummaryHM.containsKey(discountId) ) {
                            discountTaxReductionSummaryHM.put(discountId, new HashMap<String, BigDecimal>());
                        }

                        HashMap<String, BigDecimal> discountTaxReductionSummaryHMItem = discountTaxReductionSummaryHM.get(discountId);

                        if ( discountTaxReductionSummaryHMItem.containsKey(taxId) ) {
                            BigDecimal existingDiscountAmount = discountTaxReductionSummaryHMItem.get(taxId);
                            discountTaxReductionSummaryHMItem.put( taxId, discountAmount.add(existingDiscountAmount) );
                        } else {
                            discountTaxReductionSummaryHMItem.put( taxId, discountAmount );
                        }
                    }
                }
            }

            for ( DiscountLineItemModel discountLineItemModel : this.getTransactionModel().getDiscounts() ) {
                String discountId = discountLineItemModel.getDiscount().getId().toString();
                discountLineItemModel.setTaxes(new ArrayList<ItemTaxModel>());

                //if this discount is not present, continue
                if ( !discountTaxReductionSummaryHM.containsKey(discountId) ) {
                    continue;
                }

                HashMap<String, BigDecimal> discountTaxReductionSummaryHMItem = discountTaxReductionSummaryHM.get(discountId);
                for ( Map.Entry<String, BigDecimal> entry: discountTaxReductionSummaryHMItem.entrySet() ) {
                    String taxId = entry.getKey();

                    if ( !appliedTaxRateHM.containsKey(taxId) ) {
                        continue;
                    }

                    TaxModel taxModel = appliedTaxRateHM.get(taxId);
                    BigDecimal taxRate = taxModel.getTaxRate().setScale(4, DEFAULT_ROUNDING_MODE);
                    BigDecimal eligibleAmount = new BigDecimal("-1").multiply(entry.getValue()).setScale(4, DEFAULT_ROUNDING_MODE);
                    BigDecimal amount = taxRate.multiply(eligibleAmount).setScale(4, DEFAULT_ROUNDING_MODE);

                    ItemTaxModel itemTaxModel = new ItemTaxModel();
                    itemTaxModel.setItemId(Integer.parseInt(taxId));
                    itemTaxModel.setEligibleAmount(eligibleAmount);
                    itemTaxModel.setAmount(amount);
                    itemTaxModel.setTax(taxModel);
                    itemTaxModel.setItemTypeId(PosAPIHelper.ObjectType.TAX.toInt());

                    if(getTransactionModel().isTaxDelete(itemTaxModel.getItemId(), this.getProductCollectionCalculation())) {
                        itemTaxModel.setItemTypeId(PosAPIHelper.ObjectType.TAX_DELETE.toInt());
                    }

                    discountLineItemModel.getTaxes().add(itemTaxModel);
                }
            }
        }

        if ( this.getTransactionModel().getRewards().size() > 0 ) {
            //build rewardtax hashmap - to hold all price reductions associated with each reward considering tax mappings
            HashMap<String, HashMap<String, BigDecimal>> rewardTaxReductionSummaryHM = new HashMap<String, HashMap<String, BigDecimal>>();
            for ( ProductCalculation productCalculation : this.getProductCollectionCalculation().getCollection() ) {
                //look through the rewards applied - no need to check reward mappings to the product, they were applied already
                for ( HashMap rewardAppliedHM : productCalculation.getRewardTaxes() ) {
                    String rewardId = CommonAPI.convertModelDetailToString(rewardAppliedHM.get("id"));
                    String taxId = CommonAPI.convertModelDetailToString(rewardAppliedHM.get("taxId"));
                    BigDecimal rewardAmount = CommonAPI.convertModelDetailToBigDecimal(rewardAppliedHM.get("amount"));

                    //build out a default hashmap if its the first time looking at this reward
                    if ( !rewardTaxReductionSummaryHM.containsKey(rewardId) ) {
                        rewardTaxReductionSummaryHM.put(rewardId, new HashMap<String, BigDecimal>());
                    }

                    //holds information for a single tax pertaining to a reward
                    HashMap<String, BigDecimal> rewardTaxReductionSummaryHMItem = rewardTaxReductionSummaryHM.get(rewardId);

                    //get out the list of valid taxes for this reward
                    if ( rewardTaxReductionSummaryHMItem.containsKey(taxId) ) {
                        BigDecimal existingRewardAmount = rewardTaxReductionSummaryHMItem.get(taxId);
                        rewardTaxReductionSummaryHMItem.put( taxId, rewardAmount.add(existingRewardAmount) );
                    } else {
                        rewardTaxReductionSummaryHMItem.put( taxId, rewardAmount );
                    }
                }

                //continue if this product has no modifiers
                if ( productCalculation.getModifiers() == null || productCalculation.getModifiers().size() == 0 ) {
                    continue;
                }

                for ( ModifierCalculation modifierCalculation : productCalculation.getModifiers() ) {
                    if ( modifierCalculation.getRewardsApplied().size() == 0 ) {
                        continue;
                    }

                    //look through the rewards applied - no need to check mappings to the product, they were applied already
                    for ( HashMap rewardAppliedHM : modifierCalculation.getRewardTaxes() ) {
                        String rewardId = CommonAPI.convertModelDetailToString(rewardAppliedHM.get("id"));
                        String taxId = CommonAPI.convertModelDetailToString(rewardAppliedHM.get("taxId"));
                        BigDecimal rewardAmount = CommonAPI.convertModelDetailToBigDecimal(rewardAppliedHM.get("amount"));

                        //build out a default hashmap if its the first time looking at this reward
                        if ( !rewardTaxReductionSummaryHM.containsKey(rewardId) ) {
                            rewardTaxReductionSummaryHM.put(rewardId, new HashMap<String, BigDecimal>());
                        }

                        HashMap<String, BigDecimal> rewardTaxReductionSummaryHMItem = rewardTaxReductionSummaryHM.get(rewardId);

                        if ( rewardTaxReductionSummaryHMItem.containsKey(taxId) ) {
                            BigDecimal existingRewardAmount = rewardTaxReductionSummaryHMItem.get(taxId);
                            rewardTaxReductionSummaryHMItem.put( taxId, rewardAmount.add(existingRewardAmount) );
                        } else {
                            rewardTaxReductionSummaryHMItem.put( taxId, rewardAmount );
                        }
                    }
                }
            }

            for ( LoyaltyRewardLineItemModel loyaltyRewardLineItemModel : this.getTransactionModel().getRewards() ) {
                String rewardId = loyaltyRewardLineItemModel.getReward().getId().toString();
                Boolean isFreeProductReward = (loyaltyRewardLineItemModel.getReward().getRewardTypeId() == 4);
                loyaltyRewardLineItemModel.setTaxes(new ArrayList<ItemTaxModel>());

                //if this reward is not present, continue
                if ( !rewardTaxReductionSummaryHM.containsKey(rewardId) ) {
                    continue;
                }

                HashMap<String, BigDecimal> rewardTaxReductionSummaryHMItem = rewardTaxReductionSummaryHM.get(rewardId);
                for ( Map.Entry<String, BigDecimal> entry: rewardTaxReductionSummaryHMItem.entrySet() ) {
                    String taxId = entry.getKey();

                    if ( !appliedTaxRateHM.containsKey(taxId) ) {
                        continue;
                    }

                    TaxModel taxModel = appliedTaxRateHM.get(taxId);
                    BigDecimal taxRate = taxModel.getTaxRate().setScale(4, DEFAULT_ROUNDING_MODE);

                    BigDecimal eligibleAmount = BigDecimal.ZERO;

                    if ( isFreeProductReward ) {
                        eligibleAmount = loyaltyRewardLineItemModel.getAmount();
                    } else {
                        eligibleAmount = new BigDecimal("-1").multiply(entry.getValue()).setScale(4, DEFAULT_ROUNDING_MODE);
                    }

                    BigDecimal amount = taxRate.multiply(eligibleAmount).setScale(4, DEFAULT_ROUNDING_MODE);

                    ItemTaxModel itemTaxModel = new ItemTaxModel();
                    itemTaxModel.setItemId(Integer.parseInt(taxId));
                    itemTaxModel.setEligibleAmount(eligibleAmount);
                    itemTaxModel.setAmount(amount);
                    itemTaxModel.setTax(taxModel);
                    itemTaxModel.setItemTypeId(PosAPIHelper.ObjectType.TAX.toInt());

                    if(getTransactionModel().isTaxDelete(itemTaxModel.getItemId(), this.getProductCollectionCalculation())) {
                        itemTaxModel.setItemTypeId(PosAPIHelper.ObjectType.TAX_DELETE.toInt());
                    }

                    loyaltyRewardLineItemModel.getTaxes().add(itemTaxModel);
                }
            }
        }
    }

    /**
     * When POS or Third Parties make an inquire, remove off all the tax records because they will be calculated again
     * @throws Exception
     */
    public void removeTaxes() throws Exception{

        this.getTransactionModel().getTaxes().clear();

        for (ProductLineItemModel productLineItemModel : this.getTransactionModel().getProducts()){
            productLineItemModel.getTaxes().clear();
        }

        for (DiscountLineItemModel discountLineItemModel : this.getTransactionModel().getDiscounts()){
            if (!discountLineItemModel.isAppliedToTransaction()) {
                discountLineItemModel.getTaxes().clear(); //Keep the Item Tax Models if the Discount has been applied
            }
        }

        for (LoyaltyRewardLineItemModel loyaltyRewardLineItemModel : this.getTransactionModel().getRewards()){
            loyaltyRewardLineItemModel.getTaxes().clear();
        }
    }

    //COMBO METHODS
    public void evaluateCombos() throws Exception {

        //retrieve combos and set them on the combo collection calculation
        this.getComboCollectionCalculation().retrieveValidCombos( this.getTransactionModel().getTerminal().getId() );

        //build combo objects for any products already mapped to a combo
        this.getComboCollectionCalculation().validateCurrentCombos( this.getProductCollectionCalculation() );

        //no combos are valid, no need to check anything
        if ( this.getComboCollectionCalculation().getCombos().size() == 0 ) {
            return;
        }

        //check if there's only one product
        if ( this.getTransactionModel().getProducts().size() == 1 && this.getTransactionModel().getProducts().get(0).getQuantity().compareTo(BigDecimal.ONE) == 0 ) {
            Logger.logMessage("Only one product sent in, skipping searching for combos", Logger.LEVEL.TRACE);
            return;
        }

        //check for combos
        if ( !this.getTransactionModel().getTerminal().getDisableSmartCombos() ) {
            this.getComboCollectionCalculation().determineProductCombos( this.getProductCollectionCalculation() );
        }

        //if there are combos
        if ( this.getComboCollectionCalculation().getCollection().size() > 0 ) {

            //update productLineItems based on productCollectionCalculation
            updateProductLineItems();

            //create comboLineItems based on comboCalculationCollection
            createComboLineItems();
        }
    }

    //creates a new product line item list for each product calculation
    public void updateProductLineItems() {

        //clean up product calculation lines if there are products with 0 quantity
        this.getProductCollectionCalculation().removeEmptyQuantityProducts();

        //clean up product calculation lines so if there are duplicate products set to the same combo, update the first line's quantity
        combineDuplicateComboProductCalculations();

        this.getProductCollectionCalculation().updateProductCollectionIndexes();

        List<ProductCalculation> productCalculationList = this.getProductCollectionCalculation().getCollection();

        List<ProductLineItemModel> newProductLineItems = new ArrayList<>();
        List<Integer> productLinesFound = new ArrayList<>();
        Integer index = 0;

        //loop over each product calculation
        for(ProductCalculation productCalculation : productCalculationList) {

            //loop over each product line item
            for(int i=0; i<this.getTransactionModel().getProducts().size(); i++) {

                ProductLineItemModel productLineItemModel = this.getTransactionModel().getProducts().get(i);
                ProductModel product = productLineItemModel.getProduct();

                //continue if the product line item's productID is not the same as the product calculation itemID OR if this product line has already been created and there is another line with the same product OR if the product line's modifiers don't match OR if the product line's prep options don't match
                if(!productLineItemModel.getItemId().equals( productCalculation.getId() ) || !product.getId().equals(productCalculation.getId()) || productLinesFound.contains(i) || !productCalculation.areModifiersValid(productLineItemModel) || !productCalculation.arePrepOptionsValid(productLineItemModel)) {
                    continue;
                }

                //get the number of product line items using this product, so if you added a Burger to the cart 3 different times, could be different quantities each time or different mods
                Integer productLineItemCount = productCalculation.countMatchingProductLineItems(this.getTransactionModel().getProducts());

                //continue if there is more than one product line using this product and this product line has already been accounted for
                if(productLineItemCount > 1 && productLinesFound.contains(i)) {
                    continue;
                }

                //create a new product line
                ProductLineItemModel newProductLineItemModel = new ProductLineItemModel();
                newProductLineItemModel.createProductModelLineItemFromProduct(productLineItemModel);

                newProductLineItemModel.setQuantity(productCalculation.getQuantity());

                //set the combo details on the product line
                if(productCalculation.getComboTransLineItemId() != null) {

                    newProductLineItemModel.setQuantity(productCalculation.getQuantityInCombos());

                    newProductLineItemModel.setComboTransLineItemId(productCalculation.getComboTransLineItemId());
                    newProductLineItemModel.setComboDetailId(productCalculation.getComboDetailId());
                    newProductLineItemModel.setBasePrice(productCalculation.getBasePrice());
                    newProductLineItemModel.setComboPrice(productCalculation.getComboPrice());
                    newProductLineItemModel.setUpcharge(productCalculation.getUpcharge());

                    //if the combo price is less than the base price set an item discount on the product
                    if(productCalculation.getComboPrice().compareTo(productCalculation.getBasePrice()) < 0) {

                        this.setComboItemDiscount(newProductLineItemModel, productCalculation, index);

                        // Set temporary linkedPATransLineItemId on new product line
                        if(newProductLineItemModel.getDiscounts().size() > 0 && productLineItemModel.getDiscounts().size() > 0) {
                            newProductLineItemModel.getDiscounts().get(0).setTempLinkedLineItemId(productLineItemModel.getDiscounts().get(0).getTempLinkedLineItemId());
                        }
                    }

                //if this product is not associated with a combo but there are more than one lines of it in the cart, mark this line as found
                } else if(productLineItemCount > 1) {
                    productLinesFound.add(i);
                }

                //add the new product line to the list
                newProductLineItems.add(newProductLineItemModel);

                break;
            }

            index++;
        }

        this.getTransactionModel().setProducts(newProductLineItems);

        //reorder the product line items so combo items are grouped together
        reorderProductLineItems();
    }

    //set the combo line items based on the combo calculation collection
    public void createComboLineItems() {
        List<ComboLineItemModel> comboLineItemModelList = new ArrayList<>();

        //look over each combo being used
        for(ComboCalculation combo : this.getComboCollectionCalculation().getCollection()) {

            //set the combo line item properties
            ComboLineItemModel comboLineItemModel = new ComboLineItemModel();

            comboLineItemModel.setItemId(combo.getId());
            comboLineItemModel.setCombo(combo.getComboModel());
            comboLineItemModel.setQuantity(combo.getQuantity());

            comboLineItemModel.setItemTypeId(PosAPIHelper.ObjectType.COMBO.toInt()); //combo paItemTypeID

            BigDecimal amount = BigDecimal.ZERO;

            //calculate the amount
            for ( Map.Entry<Integer, ComboDetailModel> entry : combo.getSelectedProductsHM().entrySet() ) {
                ComboDetailModel comboDetail = entry.getValue();
                ProductCalculation selectedProduct = comboDetail.getSelectedProduct();

                BigDecimal productPrice = selectedProduct.getLinePrice();
                BigDecimal basePrice = comboDetail.getBasePrice();
                BigDecimal comboPrice = comboDetail.getComboPrice();

                //add combo price to total amount
                amount = amount.add(comboPrice);

                //add product upcharge to total amount
                BigDecimal upcharge = productPrice.subtract(basePrice);
                if(upcharge.compareTo(BigDecimal.ZERO) != 0) {
                    amount = amount.add(upcharge);
                }
            }

            comboLineItemModel.setAmount(amount);

            //add to list of combo line items
            comboLineItemModelList.add(comboLineItemModel);
        }

        //set combo collection
        this.getTransactionModel().setCombos(comboLineItemModelList);
    }

    //set the combo item discount model on the productLineItem
    public void setComboItemDiscount(ProductLineItemModel newProductLineItemModel, ProductCalculation productCalculation, Integer index) {

        ItemDiscountModel itemDiscountModel = new ItemDiscountModel();

        //get the paDiscountID from the combo this product correspond with
        Integer paDiscountID = this.getComboCollectionCalculation().getDiscountAppliedToCombo(productCalculation.getComboDetailId());
        itemDiscountModel.setItemId(paDiscountID);
        itemDiscountModel.setItemTypeId(PosAPIHelper.ObjectType.POSDISCOUNT.toInt());

        //price customer pays is (product price - base price) + combo price
        BigDecimal productPrice = newProductLineItemModel.getAmount();
        BigDecimal priceCustomerPays = (productPrice.subtract(productCalculation.getBasePrice())).add(productCalculation.getComboPrice());

        //discount amount is the product's price - what customer pays
        BigDecimal discountAmount = productPrice.subtract(priceCustomerPays);
        discountAmount = discountAmount.multiply(productCalculation.getQuantity());

        BigDecimal productLinePrice = (productCalculation.getLinePrice().setScale(2, RoundingMode.HALF_UP)).multiply(productCalculation.getQuantity());

        itemDiscountModel.setAmount(BigDecimal.valueOf(-1).multiply(discountAmount));
        itemDiscountModel.setEligibleAmount(productLinePrice);
        itemDiscountModel.setIsComboDiscount(true);

        //get DiscountModel
        DiscountModel discountModel = null;
        try {
            discountModel = DiscountModel.getOneDiscountModel(paDiscountID, this.getTerminalId());

            //set discount model amount
            if(discountModel != null) {
                discountModel.setAmount(BigDecimal.valueOf(-1).multiply(discountAmount));
                itemDiscountModel.setDiscount(discountModel);

                DiscountCalculation discountCalculation = new DiscountCalculation();
                discountCalculation.setId(paDiscountID);
                discountCalculation.setDiscountModel(discountModel);
                discountCalculation.setDiscountAmount(discountAmount);
                discountCalculation.setDiscountableAmount(productLinePrice);
                discountCalculation.addQuantityLineDiscountable(productCalculation.getIndex(), productCalculation.getQuantity());

                //apply the discount to the ProductCalculation
                applyDiscount(discountCalculation, index);

                itemDiscountModel.setAmount(discountCalculation.getDiscountAmount());
                itemDiscountModel.setAmount(BigDecimal.valueOf(-1).multiply(discountAmount));
            }

        } catch (Exception e) {
            Logger.logMessage("Could not determine Discount Model for combo productLineItem in TransactionCalculation.updateProductLineItems", Logger.LEVEL.ERROR);
        }

        //add the combo item discount, combo discount takes precedence
        newProductLineItemModel.getDiscounts().add( itemDiscountModel );

    }

    public void reorderProductLineItems() {
        List<ProductLineItemModel> productLineItems  = this.getTransactionModel().getProducts();

        Integer lineSequence = 0;
        for(ProductLineItemModel productLineItemModel : productLineItems) {
            productLineItemModel.setTransactionItemNum(++lineSequence); //moved the ++ because the transactionItemNum now starts with 1 instead of 0
        }

        //first sort the products by their comboTransLineItemId so the combos are grouped together
        Collections.sort(productLineItems, new ComboTransLineComparerAsc());

        /*
        * Description:
        *   Groups together products per combo into one ArrayList. If Product isn't in combo, it goes into it's own arraylist and hashamp.
        *   The HashMap key for that combo will be the line sequence of the first product in the combo.
        *   This way when we order that list by the hashmap keys the combos will be grouped an ordered correctly.
        * Example:
        *   User adds Cookie, Burger, Cupcake, Soda to cart.  Burger and Soda make a combo
        * Result:
        *    [
        *       { "0" : [ {Cookie ProductLineItemModel. LineSequenceId = 0} ] },
        *       { "2" : [ {Cupcake ProductLineItemModel. LineSequenceId = 2} ] },
        *       { "1" : [ {Burger ProductLineItemModel. LineSequenceId = 1},
        *                 {Soda ProductLineItemModel. LineSequenceId = 3} ] }
        *    ]
        * */

        ArrayList<HashMap<Integer, List<ProductLineItemModel>>> combosAndProductsList = new ArrayList<>();
        ArrayList<ProductLineItemModel> productList = new ArrayList<>();
        HashMap productToIndexMap = new HashMap();
        Integer currentCombo = 0;

        //loop over each product
        for(ProductLineItemModel productLine : productLineItems) {
            Integer comboTransLineItemID = productLine.getComboTransLineItemId();

            //if this product isn't part of a combo
            if(comboTransLineItemID == null) {

                //if there are products in productList, set those as a hashmap in the combosAndProductsList.  Set key as first product's LineSequenceId so you know where the combo should be
                if(productList.size() > 0) {
                    productToIndexMap.put(productList.get(0).getTransactionItemNum(), productList);
                    combosAndProductsList.add(productToIndexMap);
                    productList = new ArrayList<>();
                    productToIndexMap = new HashMap();
                }

                //add the non-combo product as it's own hashmap and to the combosAndProductsList.  Set key as LineSequenceId
                productList.add(productLine);
                productToIndexMap.put(productLine.getTransactionItemNum(), productList);
                combosAndProductsList.add(productToIndexMap);

                productList = new ArrayList<>();
                productToIndexMap = new HashMap();

            //if product is in a combo, and product's comboTransLineItemId is the same as the currentCombo, just add productLineItemModel
            } else if (currentCombo.equals(comboTransLineItemID)) {
                productList.add(productLine);

            //if product is in a combo and not part of the currentCombo, if the productList has products in it,  set those in combosAndProductsList
            } else {
                if(productList.size() > 0) {
                    productToIndexMap.put(productList.get(0).getTransactionItemNum(), productList);
                    combosAndProductsList.add(productToIndexMap);
                    productList = new ArrayList<>();
                    productToIndexMap = new HashMap();
                }

                //set this comboTransLineItemId as the new current combo, add product to productList
                currentCombo = comboTransLineItemID;
                productList.add(productLine);
            }
        }

        //if some products haven't been added to combosAndProductsList, add them
        if(productList.size() > 0) {
            productToIndexMap.put(productList.get(0).getTransactionItemNum(), productList);
            combosAndProductsList.add(productToIndexMap);
        }

        //sort the HashMaps in the combosAndProductsList by their key to get the correct order of the combos and products based on how they were added to the cart
        Collections.sort(combosAndProductsList, new ComboAndProductsComparerAsc());

        List<ProductLineItemModel> reorderProductLineItems = new ArrayList<>();
        Integer comboLineSequence = 0;

        //set comboLineSequenceIds for all products. Set the lineSequenceId's back to 0.
        for(HashMap<Integer, List<ProductLineItemModel>> listItem : combosAndProductsList) {
            for(Integer key : listItem.keySet()) {

                List<ProductLineItemModel> products = listItem.get(key);
                for(ProductLineItemModel productLineItemModel : products) {
                    productLineItemModel.setComboLineItemSequence(comboLineSequence ++);
                    productLineItemModel.setTransactionItemNum(0);
                    reorderProductLineItems.add(productLineItemModel);
                }
            }
        }

        this.getTransactionModel().setProducts(reorderProductLineItems);
    }

    //if there are duplicate product calculation lines set to a combo, update the quantity of the first line and remove the duplicates
    public void combineDuplicateComboProductCalculations() {
        List<ProductCalculation> productCalculationList = this.getProductCollectionCalculation().getCollection();

        List<Integer> duplicateProductCalculationLinesIndex = new ArrayList<Integer>();
        Integer loopCount = 0;

        do {

            loopCount++; //keep track of iterations to avoid getting stuck in while loop

            //loop over each product calculation
            for(int originalIndex =0; originalIndex<productCalculationList.size(); originalIndex++) {

                ProductCalculation productCalculation = productCalculationList.get(originalIndex);

                //continue if the product is not set to a combo
                if(productCalculation.getComboTransLineItemId() == null) {
                    continue;
                }

                boolean comboHasModifiersOrPrep = false;
                for(ComboCalculation comboCalculation : this.getComboCollectionCalculation().getCollection()) {

                    for(ComboDetailModel comboDetailModel : comboCalculation.getComboModel().getDetails()) {

                        //if the combo has combo details with modifiers set on the product, don't combine duplicates
                        if(comboDetailModel.getId().equals(productCalculation.getComboDetailId()) && (comboCalculation.isComboHasModifiers() || comboCalculation.isComboHasPrepOption()) ) {
                            comboHasModifiersOrPrep = true;
                            break;
                        }
                    }
                }

                if(comboHasModifiersOrPrep) {
                    continue;
                }

                //get the original product calculation line's quantity
                BigDecimal quantityInCombos = productCalculation.getQuantityInCombos();

                duplicateProductCalculationLinesIndex = new ArrayList<Integer>();

                //loop over each product calculation line again looking for the same product line but not at this index
                for(int checkIndex =0; checkIndex<productCalculationList.size(); checkIndex++) {
                    ProductCalculation productCalculationCheck = productCalculationList.get(checkIndex);

                    //if there is a duplicate product check that all of the other details in the product's combo are a exact match as well, if so combine duplicates
                    if(originalIndex != checkIndex && productCalculation.isProductPartOfDuplicateCombo(productCalculationList, productCalculationCheck) ) {

                        duplicateProductCalculationLinesIndex.add(checkIndex);

                        BigDecimal updatedQuantity = quantityInCombos.add(productCalculation.getQuantityInCombos());
                        productCalculation.setQuantityInCombos(updatedQuantity);
                        productCalculation.setQuantity(updatedQuantity);
                    }
                }

                //remove duplicate productCalculation lines
                if(duplicateProductCalculationLinesIndex.size() > 0) {
                    for ( int prodIndex = duplicateProductCalculationLinesIndex.size() - 1; prodIndex >= 0; prodIndex-- ) {
                        Integer indexToRemove = duplicateProductCalculationLinesIndex.get(prodIndex);
                        productCalculationList.remove(productCalculationList.get(indexToRemove));
                    }

                    break;
                }
            }

            //stop while loop if loopCount is over 100
            if(loopCount > 100) {
                return;
            }

            //start over if duplicate product calculation lines are found so there are no inconsistencies
        } while(duplicateProductCalculationLinesIndex.size() > 0);
    }

    //SURCHARGE METHODS
    public void evaluateSurcharges() throws Exception {

        for(ProductLineItemModel productLineItemModel : this.getTransactionModel().getProducts()) {
            ProductModel productModel = productLineItemModel.getProduct();

            if(productModel.getSurcharges() == null || productModel.getSurcharges().size() == 0) {
                continue;
            }

            //only one surcharge can be mapped to a product so get the first one in the list
            SurchargeModel surchargeModel = productModel.getSurcharges().get(0);

            //get SurchargeDisplayModel
            SurchargeDisplayModel surchargeDisplayModel = SurchargeDisplayModel.createSurchargeModel(surchargeModel);

            //Item surcharge
            if(surchargeDisplayModel.getApplicationTypeId().equals(2)) {
                Integer surchargeID = surchargeModel.getId();

                //calculate the product total the surcharge will be applied to based on the 'include' flags
                BigDecimal itemTotal = surchargeModel.calculateItemTotal(productLineItemModel);

                //calculate the surcharge amount based off the Amount Type, percent or dollar
                BigDecimal surchargeExtendedAmount = surchargeModel.calculateItemSurchargeAmount(productLineItemModel);
                BigDecimal surchargeAmount = surchargeExtendedAmount.divide(productLineItemModel.getQuantity(), 4, RoundingMode.HALF_UP);
                surchargeExtendedAmount = surchargeExtendedAmount.setScale(2, RoundingMode.HALF_UP);

                //set SurchargeLineItemModel
                SurchargeLineItemModel surchargeLineItemModel = new SurchargeLineItemModel();
                surchargeLineItemModel.setItemId(surchargeModel.getId());
                surchargeLineItemModel.setItemTypeId(PosAPIHelper.ObjectType.SURCHARGE.toInt());
                surchargeLineItemModel.setQuantity(productLineItemModel.getQuantity());

                surchargeLineItemModel.setAmount(surchargeAmount);
                surchargeLineItemModel.setExtendedAmount(surchargeExtendedAmount);
                surchargeLineItemModel.setEligibleAmount(itemTotal.setScale(2, RoundingMode.HALF_UP));

                surchargeLineItemModel.setSurcharge(surchargeDisplayModel);

                //if RefundsWithProduct is ON, set ItemSurchargeModels on product lines
                if(surchargeDisplayModel.getRefundsWithProducts()) {
                    ItemSurchargeModel itemSurchargeModel = new ItemSurchargeModel();
                    itemSurchargeModel.setItemId(surchargeID);
                    itemSurchargeModel.setItemTypeId(PosAPIHelper.ObjectType.SURCHARGE.toInt());

                    itemSurchargeModel.setAmount(surchargeExtendedAmount);
                    itemSurchargeModel.setEligibleAmount(itemTotal.setScale(2, RoundingMode.HALF_UP));
                    itemSurchargeModel.setSurcharge(surchargeDisplayModel);

                    productLineItemModel.getSurcharges().add(itemSurchargeModel);
                }

                //Link the SurchargeLineItemModel to the corresponding ProductLineItemModel for Item Surcharges
                productLineItemModel.setTempId(productLineItemModel.getOrGenerateTempId());
                surchargeLineItemModel.setLinkedTransLineItemId(productLineItemModel.getOrGenerateTempId());

                //if there are taxes on the surcharge, check that the tax is also mapped to the product
                if(surchargeDisplayModel.getTaxes().size() > 0) {
                    for(TaxModel taxModel : surchargeDisplayModel.getTaxes()) {

                        boolean found = false;
                        for(ItemTaxModel itemTaxModel : productLineItemModel.getTaxes()) {
                            if(itemTaxModel.getItemId().equals(taxModel.getId())) {
                                found = true;
                                break;
                            }
                        }

                        //if the tax is also mapped to the product add tax models
                        if(found) {
                            BigDecimal surchargeTaxAmount = taxModel.getTaxRate().multiply(surchargeExtendedAmount.setScale(2, BigDecimal.ROUND_HALF_UP));

                            //set the surcharge tax's TaxLineItemModel
                            TaxLineItemModel taxLineItemModel = new TaxLineItemModel();
                            taxLineItemModel.setId(taxModel.getId());
                            taxLineItemModel.setAmount(surchargeTaxAmount);
                            taxLineItemModel.setExtendedAmount(surchargeTaxAmount.setScale(2, BigDecimal.ROUND_HALF_UP));
                            taxLineItemModel.setEligibleAmount(surchargeExtendedAmount);  //eligible amount is the surcharge amount
                            taxLineItemModel.setTax(taxModel);

                            getTransactionModel().getTaxes().add(taxLineItemModel);

                            //set the surcharge tax's ItemItemModel
                            ItemTaxModel itemTaxModel = new ItemTaxModel();
                            itemTaxModel.setItemId(taxModel.getId());
                            itemTaxModel.setItemTypeId(PosAPIHelper.ObjectType.TAX.toInt());
                            itemTaxModel.setAmount(surchargeTaxAmount);
                            itemTaxModel.setEligibleAmount(surchargeExtendedAmount); //eligible amount is the surcharge amount
                            itemTaxModel.setTax(taxModel);

                            surchargeLineItemModel.getTaxes().add(itemTaxModel);
                        }
                    }
                }

                //add surcharges to transaction model
                this.getTransactionModel().getSurcharges().add(surchargeLineItemModel);
            }
        }
    }

    public void removeSurcharges(){
        this.getTransactionModel().getSurcharges().clear();

        for (ProductLineItemModel productLineItemModel : this.getTransactionModel().getProducts()){
            productLineItemModel.getSurcharges().clear();
        }
    }

    /**
     * Override a QC Discount amount for voucher recalculation.
     * @param qcDiscountModel
     * @throws Exception
     */
    public void overrideQcDiscountAmountForVouchers(QcDiscountModel qcDiscountModel) throws Exception {
        if (!transactionModel.getDiscountInfoCollection().getCollection().isEmpty()){
            if (transactionModel.getDiscountInfoCollection().containsQcDiscountId(qcDiscountModel.getId())){
                DiscountInfoModel discountInfoModel = transactionModel.getDiscountInfoCollection().getOneByQcDiscountId(qcDiscountModel.getId());
                qcDiscountModel.setAmount(discountInfoModel.getQcDiscountAmt());
             }
        }
    }

    /**
     * Add QC Discount information to TransactionModel.DiscountInfoCollection.
     * This is used for recalculating QC Discounts for vouchers.
     * @param qcDiscountHM
     * @throws Exception
     */
    public void overrideQcDiscountAmountForVouchers(HashMap qcDiscountHM) throws Exception {
        Integer qcDiscountId = CommonAPI.convertModelDetailToInteger(qcDiscountHM.get("DISCOUNTID"));
        BigDecimal amount = CommonAPI.convertModelDetailToBigDecimal((qcDiscountHM.get("AMOUNT")));

        if (!transactionModel.getDiscountInfoCollection().getCollection().isEmpty()){
            if (transactionModel.getDiscountInfoCollection().containsQcDiscountId(qcDiscountId)){
                DiscountInfoModel discountInfoModel = transactionModel.getDiscountInfoCollection().getOneByQcDiscountId(qcDiscountId);
                qcDiscountHM.put("AMOUNT", discountInfoModel.getQcDiscountAmt());
            }
        }
    }

    /**
     * Add QC Discount information into transactionModel.DiscountInfoCollection if it does not exist.
     * This is used for QC Discounts that were already applied by QCPOS.
     * @throws Exception
     */
    public void initQcDiscountAmountsForVouchers() throws Exception {
        for (DiscountLineItemModel discountLineItemModel : transactionModel.getDiscounts()){
            if (discountLineItemModel.getQcDiscount() != null){
                initQcDiscountAmountForVouchers(discountLineItemModel.getQcDiscount(), discountLineItemModel.getTransactionItemNum());
            }
        }
    }

    /**
     * Add QC Discount information into transactionModel.DiscountInfoCollection if it does not exist.
     * @param discountCalculation
     * @throws Exception
     */
    public void initQcDiscountAmountsForVouchers(DiscountCalculation discountCalculation) throws Exception {
        if (transactionModel.getDiscountInfoCollection().getCollection().isEmpty()
                || !transactionModel.getDiscountInfoCollection().containsQcDiscountId(discountCalculation.getQcDiscountModel().getId())) {

            DiscountInfoModel discountInfoModel = new DiscountInfoModel();
            discountInfoModel.setQcDiscountId(discountCalculation.getQCDiscountId());
            discountInfoModel.setQcDiscountAmt(discountCalculation.getDiscountAmount());
            this.getTransactionModel().getDiscountInfoCollection().getCollection().add(discountInfoModel);
        }
    }

    /**
     * Add QC Discount information into transactionModel.DiscountInfoCollection if it does not exist.
     * Used for QCPOS discounts that were previously applied.
     * @param qcDiscountModel
     * @param transactionItemNum
     * @throws Exception
     */
    public void initQcDiscountAmountForVouchers(QcDiscountModel qcDiscountModel, Integer transactionItemNum) throws Exception {
        if (transactionModel.getDiscountInfoCollection().getCollection().isEmpty()
                || !transactionModel.getDiscountInfoCollection().containsQcDiscountId(qcDiscountModel.getId())) {

            DiscountInfoModel discountInfoModel = new DiscountInfoModel(qcDiscountModel, transactionItemNum);
            this.getTransactionModel().getDiscountInfoCollection().getCollection().add(discountInfoModel);
        }
    }

    //GETTERS AND SETTERS
    public TransactionModel getTransactionModel() {
        return transactionModel;
    }

    public void setTransactionModel(TransactionModel transactionModel) {
        this.transactionModel = transactionModel;
    }

    public ProductCollectionCalculation getProductCollectionCalculation() {
        return productCollectionCalculation;
    }

    public void setProductCollectionCalculation(ProductCollectionCalculation productCollectionCalculation) {
        this.productCollectionCalculation = productCollectionCalculation;
    }

    public Integer getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(Integer terminalId) {
        this.terminalId = terminalId;
    }

    public List<DiscountCalculation> getAppliedDiscounts() {
        return appliedDiscounts;
    }

    public void setAppliedDiscounts(List<DiscountCalculation> appliedDiscounts) {
        this.appliedDiscounts = appliedDiscounts;
    }

    public List<TaxCalculation> getAppliedTaxes() {
        return appliedTaxes;
    }

    public void setAppliedTaxes(List<TaxCalculation> appliedTaxes) {
        this.appliedTaxes = appliedTaxes;
    }

    public List<ComboCalculation> getAppliedCombos() {
        return appliedCombos;
    }

    public void setAppliedCombos(List<ComboCalculation> appliedCombos) {
        this.appliedCombos = appliedCombos;
    }

    public ComboCollectionCalculation getComboCollectionCalculation() {
        return comboCollectionCalculation;
    }

    public void setComboCollectionCalculation(ComboCollectionCalculation comboCollectionCalculation) {
        this.comboCollectionCalculation = comboCollectionCalculation;
    }
}