package com.mmhayes.common.transaction.models;

//MMHayes Dependencies
import com.mmhayes.common.api.PosApi30.PosAPIHelper;

//Other Dependencies
import java.math.BigDecimal;

/*
 $Author: eglundin $: Author of last commit
 $Date: 2019-03-25 08:10:10 -0400 (Mon, 25 Mar 2019) $: Date of last commit
 $Rev: 8585 $: Revision of last commit
 Notes:
*/
public class TransactionObjectComparisonLine {

    PosAPIHelper.ObjectType objectType = null;
    Integer objectId = null;
    BigDecimal transactionTotal = BigDecimal.ZERO;
    BigDecimal productTotal = BigDecimal.ZERO;

    public TransactionObjectComparisonLine(){

    }

    public TransactionObjectComparisonLine(PosAPIHelper.ObjectType objectType, int objectId, BigDecimal transTotal, BigDecimal prodTotal){
        this.setObjectType(objectType);
        this.setObjectId(objectId);
        this.setTransactionTotal(transTotal);
        this.setProductTotal(prodTotal);

    }

    public PosAPIHelper.ObjectType getObjectType() {
        return objectType;
    }

    public void setObjectType(PosAPIHelper.ObjectType objectType) {
        this.objectType = objectType;
    }

    public Integer getObjectId() {
        return objectId;
    }

    public void setObjectId(Integer objectId) {
        this.objectId = objectId;
    }

    public BigDecimal getTransactionTotal() {
        return transactionTotal;
    }

    public void setTransactionTotal(BigDecimal transactionTotal) {
        this.transactionTotal = transactionTotal;
    }

    public BigDecimal getProductTotal() {
        return productTotal;
    }

    public void setProductTotal(BigDecimal productTotal) {
        this.productTotal = productTotal;
    }



}
