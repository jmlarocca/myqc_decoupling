package com.mmhayes.common.transaction.models;

//mmhayes dependencies

//API dependencies
import com.mmhayes.common.api.CommonAPI;

//other dependencies
import java.util.HashMap;

/*
 $Author: eglundin $: Author of last commit
 $Date: 2019-03-25 08:10:10 -0400 (Mon, 25 Mar 2019) $: Date of last commit
 $Rev: 8585 $: Revision of last commit
 Notes:
*/
public class GratuityModel extends SurchargeModel {

    public GratuityModel() {
        super();
    }

    public GratuityModel(SurchargeModel surchargeModel) {
        super.setId(surchargeModel.getId());
        super.setName(surchargeModel.getName());
    }

    //Constructor- takes in a Hashmap
    public GratuityModel(HashMap modelHM) {
        setModelProperties(modelHM);
    }

    //setter for all of this model's properties
    public GratuityModel setModelProperties(HashMap modelDetailHM) {
        setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));
        setName(CommonAPI.convertModelDetailToString(modelDetailHM.get("NAME")));

        return this;
    }
}

