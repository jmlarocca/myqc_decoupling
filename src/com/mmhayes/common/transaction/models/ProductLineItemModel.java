package com.mmhayes.common.transaction.models;

//MMHayes dependencies
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionDeserializer;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.loyalty.models.LoyaltyAccrualPolicyModel;
import com.mmhayes.common.loyalty.models.LoyaltyRewardModel;
import com.mmhayes.common.product.models.*;
import com.mmhayes.common.loyalty.models.ItemLoyaltyPointModel;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.StringFunctions;

//API dependencies

import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.fasterxml.jackson.annotation.*;

//other dependencies
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/*
 Last Updated (automatically updated by SVN)
 $Author: nyu $: Author of last commit
 $Date: 2021-08-17 14:37:51 -0400 (Tue, 17 Aug 2021) $: Date of last commit
 $Rev: 14929 $: Revision of last commit
*/

@JsonPropertyOrder({"id", "itemTypeId", "employeeId", "quantity", "amount", "extendedAmount", "refundedQty", "refundedAmount",
        "refundedExtAmount", "eligibleAmount", "itemComment", "badgeAssignmentId", "itemId", "keypadId", "originalOrderNumber", "printStatusId", "tempId", "transactionItemNum"})
public class ProductLineItemModel extends TransactionLineItemModel {
    protected ProductModel product = null;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private ArrayList<ItemTaxModel> taxes = new ArrayList<>();

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private ArrayList<ItemDiscountModel> discounts = new ArrayList<>();

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private ArrayList<ModifierLineItemModel> modifiers = new ArrayList<>();

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private ArrayList<ItemRewardModel> rewards = new ArrayList<>();

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private ArrayList<ItemLoyaltyPointModel> loyaltyPointDetails = new ArrayList<>();

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private ArrayList<LoyaltyRewardModel> rewardsAvailableForProduct = new ArrayList<>();

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private ArrayList<ItemSurchargeModel> surcharges = new ArrayList<>();

    private BigDecimal appliedRewardValue = new BigDecimal(0.00);
    private BigDecimal adjAmountLoyalty = new BigDecimal(0.00);
    private BigDecimal adjExtAmtLoyalty = new BigDecimal(0.00);
    private BigDecimal adjAmountPercentageLoyalty = new BigDecimal(0.00);
    private BigDecimal quantityForLoyalty = new BigDecimal(0.00);

    private boolean hasRewardBeenApplied = false; //used when applying rewards
    private boolean isQuantityRefund = false;  //used when doing partial refunds
    private boolean refundTheAmount = false;  //used when doing partial refunds

    private boolean productLevelAppliedToProduct = false;   //used to apply Product level Loyalty points
    private boolean originallyJustSubDepartment = false;   //when the product was sent into the api, it was originally only the subdepartment
    private boolean originallyJustDepartment = false;   //when the product was sent into the api, it was originally only the department
    private boolean wellnessAppliedToProduct = false;   //used to apply Product level Loyalty points
    private boolean discountApplied = false;
    private boolean mealPlanApplied = false;
    //private Integer lineItemSequence = 0;
    private Integer comboLineItemSequence = 0;
    private Integer keypadId = null;
    private Integer printStatusId = null;
    private Integer comboTransLineItemId = null;
    private Integer comboDetailId = null;
    private Integer tempId = null;
    private Integer parentPaTransLineItemId = null;
    private BigDecimal basePrice = null;
    private BigDecimal comboPrice = null;
    private BigDecimal upcharge = null;
    private String linkedFromPaTransLineItemIds = null;

    private PrepOptionModel prepOption = null;

    PosAPIHelper.LoyaltyLevelEarningType loyaltyLevelEarningType;
    private ArrayList<HashMap<BigDecimal, BigDecimal>> quantityLineDetails = new ArrayList<>();

    public ProductLineItemModel() {
        this.setQuantityForLoyalty(super.getQuantity());

        if (this.getProduct() != null) {
            if (this.getProduct().isWeighted()) {
                quantityForLoyalty = BigDecimal.ONE;
            }
        }
    }

    public static ProductLineItemModel createProductLineItemModel(HashMap modelDetailHM) throws Exception {
        ProductLineItemModel productLineItemModel = new ProductLineItemModel();
        productLineItemModel.setModelProperties(modelDetailHM);
        productLineItemModel.setProductModelProperties(modelDetailHM);
        return productLineItemModel;
    }

    //map product line item specific fields
    public void setProductModelProperties(HashMap modelDetailHM) throws Exception {
        this.setKeypadId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAKEYPADID")));
        this.setPrintStatusId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PRINTSTATUSID")));
        this.setComboTransLineItemId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PACOMBOTRANSLINEITEMID")));
        this.setComboDetailId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PACOMBODETAILID")));
        this.setBasePrice(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("BASEPRICE")));
        this.setComboPrice(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("COMBOPRICE")));
        this.setLinkedFromPaTransLineItemIds(CommonAPI.convertModelDetailToString(modelDetailHM.get("LINKEDFROMPATRANSLINEITEMIDS")));
    }

    public void createProductModelLineItemFromProduct(ProductLineItemModel productLineItemModel) {
        this.setProduct(productLineItemModel.getProduct());
        this.setAmount(productLineItemModel.getAmount());
        this.setItemId(productLineItemModel.getItemId());
        this.setItemTypeId(productLineItemModel.getItemTypeId());
        this.setModifiers(productLineItemModel.getModifiers());
        this.setTaxes(productLineItemModel.getTaxes());
        this.setKeypadId(productLineItemModel.getKeypadId());
        this.setPrintStatusId(productLineItemModel.getPrintStatusId());
        this.setPrepOption(productLineItemModel.getPrepOption());
        this.setBasePrice(productLineItemModel.getBasePrice());
    }

    //region Get Adjusted ProductLineItem values

    //region Used for Transaction Balances

    /**
     * Calculate the net amount of the product line's extended amount
     * @param productLineItemModel
     * @return
     */
    public static BigDecimal getAdjValOfExtendedAmountForTransaction(ProductLineItemModel productLineItemModel) {
        BigDecimal adjustedProductExtendedCost = productLineItemModel.getExtendedAmount();

        BigDecimal discountAmount = getDiscountAmount(productLineItemModel);
        adjustedProductExtendedCost = adjustedProductExtendedCost.add(discountAmount);

        BigDecimal rewardAmount = getRewardAmount(productLineItemModel);
        adjustedProductExtendedCost = adjustedProductExtendedCost.add(rewardAmount);

        BigDecimal modifierAmount = getModifierAmount(productLineItemModel);
        adjustedProductExtendedCost = adjustedProductExtendedCost.add(modifierAmount);

        BigDecimal taxAmount = BigDecimal.ZERO;
        taxAmount = getTaxAmount(productLineItemModel);
        adjustedProductExtendedCost = adjustedProductExtendedCost.add(taxAmount);

        if (adjustedProductExtendedCost.compareTo(BigDecimal.ZERO) < 0) {
            adjustedProductExtendedCost = BigDecimal.ZERO;
        }

        return adjustedProductExtendedCost;
    }

    /**
     * This is used for calculating the net amount of the product line item.
     * This will exclude ItemTaxDiscount lines that has  isDiscountPlaceHolder = true
     */
    public static BigDecimal getAdjValOfExtendedAmountForDiscount(ProductLineItemModel productLineItemModel, Boolean includeTaxes) {
        BigDecimal adjustedProductExtendedCost = productLineItemModel.getExtendedAmount();

        BigDecimal discountAmount = getDiscountAmountWithoutPlaceHolders(productLineItemModel);
        adjustedProductExtendedCost = adjustedProductExtendedCost.add(discountAmount);

        BigDecimal rewardAmount = getRewardAmount(productLineItemModel);
        adjustedProductExtendedCost = adjustedProductExtendedCost.add(rewardAmount);

        BigDecimal modifierAmount = getModifierAmount(productLineItemModel);
        adjustedProductExtendedCost = adjustedProductExtendedCost.add(modifierAmount);

        if (includeTaxes) {
            BigDecimal taxAmount = BigDecimal.ZERO;
            taxAmount = getTaxAmount(productLineItemModel);
            adjustedProductExtendedCost = adjustedProductExtendedCost.add(taxAmount);
        }

        if (adjustedProductExtendedCost.compareTo(BigDecimal.ZERO) < 0) {
            adjustedProductExtendedCost = BigDecimal.ZERO;
        }

        return adjustedProductExtendedCost;
    }

    //endregion

    //region Used for Loyalty Point Calculation

    /**
     * Used to Calculate Loyalty Points in LoyaltyPointCalculation.calculatePoints().
     *
     * @param productLineItemModel
     * @return
     */
    public static BigDecimal getAdjValOfExtendedAmountForProgram(ProductLineItemModel productLineItemModel, LoyaltyAccrualPolicyModel loyaltyAccrualPolicyModel) {
        BigDecimal adjustedProductExtendedCost = productLineItemModel.getExtendedAmount();

        BigDecimal discountAmount = getDiscountAmount(productLineItemModel);
        adjustedProductExtendedCost = adjustedProductExtendedCost.add(discountAmount);

        BigDecimal rewardAmount = getRewardAmount(productLineItemModel);
        adjustedProductExtendedCost = adjustedProductExtendedCost.add(rewardAmount);

        BigDecimal modifierAmount = getModifierAmount(productLineItemModel);
        adjustedProductExtendedCost = adjustedProductExtendedCost.add(modifierAmount);

        BigDecimal taxAmount = BigDecimal.ZERO;
        if (loyaltyAccrualPolicyModel.getIncludeTax()) {
            taxAmount = getTaxAmount(productLineItemModel);
            adjustedProductExtendedCost = adjustedProductExtendedCost.add(taxAmount);
        }

        if (adjustedProductExtendedCost.compareTo(BigDecimal.ZERO) < 0) {
            adjustedProductExtendedCost = BigDecimal.ZERO;
        }

        return adjustedProductExtendedCost;
    }

    /**
     * Used to Calculate Loyalty Points in LoyaltyPointCalculation.calculatePoints().
     *
     * @param productLineItemModel
     * @return
     */
    @Deprecated
    public static BigDecimal getAdjValOfExtendedAmountForProgram(ProductLineItemModel productLineItemModel, boolean includeTaxes) {
        BigDecimal adjustedProductExtendedCost = productLineItemModel.getExtendedAmount();

        BigDecimal discountAmount = getDiscountAmount(productLineItemModel);
        adjustedProductExtendedCost = adjustedProductExtendedCost.add(discountAmount);

        BigDecimal rewardAmount = getRewardAmount(productLineItemModel);
        adjustedProductExtendedCost = adjustedProductExtendedCost.add(rewardAmount);

        BigDecimal modifierAmount = getModifierAmount(productLineItemModel);
        adjustedProductExtendedCost = adjustedProductExtendedCost.add(modifierAmount);

        BigDecimal taxAmount = BigDecimal.ZERO;
        if (includeTaxes) {
            taxAmount = getTaxAmount(productLineItemModel);
            adjustedProductExtendedCost = adjustedProductExtendedCost.add(taxAmount);
        }

        if (adjustedProductExtendedCost.compareTo(BigDecimal.ZERO) < 0) {
            adjustedProductExtendedCost = BigDecimal.ZERO;
        }

        return adjustedProductExtendedCost;
    }

    //endregion

    //region Used for Loyalty Reward Calculation

    /**
     * Used to calculate the product total for Transaction Credit rewards in LoyaltyRewardCalculation.
     * This Extended Amount will exclude the current Loyalty Reward
     *
     * @param productLineItemModel
     * @param loyaltyReward
     * @return
     */
    public static BigDecimal getAdjValOfExtendedAmountForRewardCalculation(ProductLineItemModel productLineItemModel, LoyaltyRewardModel loyaltyReward) {
        BigDecimal adjustedProductExtendedCost = BigDecimal.ZERO;

        //HP Bug 879 - We should add up all the products in the transaction to get the transaction total
        adjustedProductExtendedCost = adjustedProductExtendedCost.add(productLineItemModel.getExtendedAmount());

        BigDecimal discountAmount = getDiscountAmountForReward(productLineItemModel, loyaltyReward);
        adjustedProductExtendedCost = adjustedProductExtendedCost.add(discountAmount);

        BigDecimal rewardAmount = getRewardAmountExcludingLoyaltyReward(productLineItemModel, loyaltyReward);
        adjustedProductExtendedCost = adjustedProductExtendedCost.add(rewardAmount);

        BigDecimal modifierAmount = getModifierAmountForReward(productLineItemModel, loyaltyReward);
        adjustedProductExtendedCost = adjustedProductExtendedCost.add(modifierAmount);

        if (adjustedProductExtendedCost.compareTo(BigDecimal.ZERO) < 0) {
            adjustedProductExtendedCost = BigDecimal.ZERO;
        }

        return adjustedProductExtendedCost;
    }

    /**
     * Using to calculate Reward Free Product Rewards, if they meet the Minimum Product amount
     * This is used for calculating the Minimum Product amount or the Maximum Reward Amount
     *
     * @param productLineItemModel
     * @param loyaltyReward
     * @return
     */
    public static BigDecimal getAdjValOfAmountForRewardCalculation(ProductLineItemModel productLineItemModel, LoyaltyRewardModel loyaltyReward) {
        BigDecimal adjustedExtendedAmount = getAdjValOfExtendedAmountForRewardCalculation(productLineItemModel, loyaltyReward);
        BigDecimal adjustedAmount = adjustedExtendedAmount.divide(productLineItemModel.getQuantityForLoyalty(), 4, RoundingMode.HALF_UP);

        return adjustedAmount;
    }

    /**
     * Used for Reward Calculation for Free Product Rewards
     *
     * @param productLineItemModel
     * @return
     */
    public static BigDecimal getAdjValOfExtendedAmountForRewardCalculation(ProductLineItemModel productLineItemModel, LoyaltyRewardModel loyaltyReward, Boolean checkTax) {
        BigDecimal adjustedProductExtendedCost = productLineItemModel.getExtendedAmount();

        BigDecimal discountAmount = getDiscountAmount(productLineItemModel);
        adjustedProductExtendedCost = adjustedProductExtendedCost.add(discountAmount);

        BigDecimal rewardAmount = getRewardAmount(productLineItemModel);
        adjustedProductExtendedCost = adjustedProductExtendedCost.add(rewardAmount);

        BigDecimal modifierAmount = getModifierAmount(productLineItemModel);
        adjustedProductExtendedCost = adjustedProductExtendedCost.add(modifierAmount);

        BigDecimal taxAmount = BigDecimal.ZERO;
        if (checkTax) {
            //Check the Tax Mappings on the Loyalty Reward.  If it is set, add the tax to the product amt
            for (TaxModel rewardTaxModel : loyaltyReward.getTaxes()) {
                for (ItemTaxModel itemTaxModel : productLineItemModel.getTaxes()) {
                    if (rewardTaxModel.getId().equals(itemTaxModel.getTax().getId())) {
                        taxAmount = taxAmount.add(itemTaxModel.getAmount());
                    }
                }
            }
        }
        adjustedProductExtendedCost = adjustedProductExtendedCost.add(taxAmount);

        if (adjustedProductExtendedCost.compareTo(BigDecimal.ZERO) < 0) {
            adjustedProductExtendedCost = BigDecimal.ZERO;
        }

        return adjustedProductExtendedCost;
    }

    //endregion

    //region Used for Loyalty Reward Application

    /**
     * Used to Apply Loyalty Rewards in LoyaltyRewardApplication.createItemTaxDsctsFreeProduct(), and also by getAdjValOfExtendedAmountForReward and getAdjValOfAmountForReward
     * This Extended Amount will include the current Loyalty Reward
     *
     * @param productLineItemModel
     * @return
     */
    public static BigDecimal getAdjValOfExtendedAmountForReward(ProductLineItemModel productLineItemModel, LoyaltyRewardModel loyaltyReward, boolean includeRewardTotals) {
        BigDecimal adjustedProductExtendedCost = BigDecimal.ZERO;

        adjustedProductExtendedCost = adjustedProductExtendedCost.add(productLineItemModel.getExtendedAmount());

        BigDecimal discountAmount = getDiscountAmountForReward(productLineItemModel, loyaltyReward);
        adjustedProductExtendedCost = adjustedProductExtendedCost.add(discountAmount);

        if (includeRewardTotals) {
            BigDecimal rewardAmount = getRewardAmount(productLineItemModel);
            adjustedProductExtendedCost = adjustedProductExtendedCost.add(rewardAmount);
        }

        BigDecimal modifierAmount = getModifierAmountForReward(productLineItemModel, loyaltyReward);
        adjustedProductExtendedCost = adjustedProductExtendedCost.add(modifierAmount);

        if (adjustedProductExtendedCost.compareTo(BigDecimal.ZERO) < 0) {
            adjustedProductExtendedCost = BigDecimal.ZERO;
        }

        return adjustedProductExtendedCost;
    }

    /**
     * Used to Apply Transaction Credit Rewards in LoyaltyRewardApplication.createItemTaxDsctsTransactionCredit()
     *
     * @param productLineItemModel
     * @param loyaltyReward
     * @return
     */
    public static BigDecimal getAdjValOfExtendedAmountForReward(ProductLineItemModel productLineItemModel, LoyaltyRewardModel loyaltyReward) {
        return getAdjValOfExtendedAmountForReward(productLineItemModel, loyaltyReward, true);
    }

    /**
     * Calculate the Product total.  Used by LoyaltyRewardApplication
     *
     * @param productLineItemModel
     * @param includeRewardTotals
     * @return
     */
    public static BigDecimal getAdjValOfExtendedAmountForRewardApplication(ProductLineItemModel productLineItemModel, boolean includeRewardTotals) {
        BigDecimal adjustedProductExtendedCost = BigDecimal.ZERO;

        adjustedProductExtendedCost = adjustedProductExtendedCost.add(productLineItemModel.getExtendedAmount());

        BigDecimal discountAmount = getDiscountAmount(productLineItemModel);
        adjustedProductExtendedCost = adjustedProductExtendedCost.add(discountAmount);

        if (includeRewardTotals) {
            BigDecimal rewardAmount = getRewardAmount(productLineItemModel);
            adjustedProductExtendedCost = adjustedProductExtendedCost.add(rewardAmount);
        }

        BigDecimal modifierAmount = getModifierAmount(productLineItemModel);
        adjustedProductExtendedCost = adjustedProductExtendedCost.add(modifierAmount);

        if (adjustedProductExtendedCost.compareTo(BigDecimal.ZERO) < 0) {
            adjustedProductExtendedCost = BigDecimal.ZERO;
        }

        return adjustedProductExtendedCost;
    }

    /**
     * do not count the included Loyalty Reward Line Item in the calculation of the reward total
     * used for Item Loyalty Points in LoyaltyRewardApplication
     *
     * @param productLineItemModel
     * @param loyaltyRewardLineItemModel
     * @return
     */
    public static BigDecimal getAdjValOfExtendedAmountForTransactionCreditRewardApplication(ProductLineItemModel productLineItemModel, LoyaltyRewardLineItemModel loyaltyRewardLineItemModel) {
        BigDecimal adjustedProductExtendedCost = BigDecimal.ZERO;

        adjustedProductExtendedCost = adjustedProductExtendedCost.add(productLineItemModel.getExtendedAmount());

        BigDecimal discountAmount = getDiscountAmountForReward(productLineItemModel, loyaltyRewardLineItemModel.getReward());
        adjustedProductExtendedCost = adjustedProductExtendedCost.add(discountAmount);

        BigDecimal rewardTotalAmount = BigDecimal.ZERO;
        for (ItemRewardModel rewardItemModel : productLineItemModel.getRewards()) {
            if (rewardItemModel.getAmount().compareTo(BigDecimal.ZERO) != 0) {   //if the modifier isn't ZERO
                if (!rewardItemModel.getRewardGuid().equals(loyaltyRewardLineItemModel.getRewardGuid())) { //only sum up the rewards that have been applied that are not for these points
                    rewardTotalAmount = rewardTotalAmount.add(rewardItemModel.getAmount());
                }
            }
        }
        adjustedProductExtendedCost = adjustedProductExtendedCost.add(rewardTotalAmount);

        BigDecimal modifierAmount = getModifierAmountForReward(productLineItemModel, loyaltyRewardLineItemModel.getReward());
        adjustedProductExtendedCost = adjustedProductExtendedCost.add(modifierAmount);

        if (adjustedProductExtendedCost.compareTo(BigDecimal.ZERO) < 0) {
            adjustedProductExtendedCost = BigDecimal.ZERO;
        }

        return adjustedProductExtendedCost;
    }

    /**
     * Calculate the net amount of the product line's extended amount for Purchase Restrictions. Exclude modifiers.
     * @param productLineItemModel
     * @return
     */
    public static BigDecimal getAdjValOfExtendedAmountForPurchaseRestrictions(ProductLineItemModel productLineItemModel) {
        BigDecimal adjustedProductExtendedCost = productLineItemModel.getExtendedAmount();

        BigDecimal discountAmount = getDiscountAmount(productLineItemModel);
        adjustedProductExtendedCost = adjustedProductExtendedCost.add(discountAmount);

        BigDecimal rewardAmount = getRewardAmount(productLineItemModel);
        adjustedProductExtendedCost = adjustedProductExtendedCost.add(rewardAmount);

        /* Modifiers are not included in the check for Purchase Restrictions
        BigDecimal modifierAmount = getModifierAmount(productLineItemModel);
        adjustedProductExtendedCost = adjustedProductExtendedCost.add(modifierAmount);*/

        BigDecimal taxAmount = BigDecimal.ZERO;
        taxAmount = getTaxAmount(productLineItemModel, true);
        adjustedProductExtendedCost = adjustedProductExtendedCost.add(taxAmount);

        if (adjustedProductExtendedCost.compareTo(BigDecimal.ZERO) < 0) {
            adjustedProductExtendedCost = BigDecimal.ZERO;
        }

        adjustedProductExtendedCost = adjustedProductExtendedCost.setScale(2, RoundingMode.HALF_UP);

        return adjustedProductExtendedCost;
    }

    //endregion

    //region Get ProductLineItem related items (ItemTaxes, ItemDiscounts, ItemRewards, Modifiers)

    public static BigDecimal getDiscountAmount(ProductLineItemModel productLineItemModel) {
        BigDecimal discountTotalAmount = BigDecimal.ZERO;

        //take off the discounts
        for (ItemDiscountModel itemDiscountModel : productLineItemModel.getDiscounts()) {
            if (!itemDiscountModel.getDiscount().isMealPlan()) {
                //use add here as it will be a negative
                discountTotalAmount = discountTotalAmount.add(itemDiscountModel.getAmount().setScale(4, RoundingMode.HALF_UP));
                productLineItemModel.setDiscountApplied(true);
            } else {
                productLineItemModel.setMealPlanApplied(true);
            }
        }

        return discountTotalAmount;
    }

    public static BigDecimal getDiscountAmountWithoutPlaceHolders(ProductLineItemModel productLineItemModel) {
        BigDecimal discountTotalAmount = BigDecimal.ZERO;

        //take off the discounts
        for (ItemDiscountModel itemDiscountModel : productLineItemModel.getDiscounts()) {
            if (!itemDiscountModel.isDiscountPlaceHolder()) {
                //use add here as it will be a negative
                discountTotalAmount = discountTotalAmount.add(itemDiscountModel.getAmount().setScale(4, RoundingMode.HALF_UP));
            }
        }

        return discountTotalAmount;
    }

    public static BigDecimal getDiscountAmountForReward(ProductLineItemModel productLineItemModel, LoyaltyRewardModel loyaltyReward) {
        BigDecimal discountTotalAmount = BigDecimal.ZERO;

        if (loyaltyReward.isApplyToAllPlus()) {

            //take off the discounts
            for (ItemDiscountModel itemDiscountModel : productLineItemModel.getDiscounts()) {
                //use add here as it will be a negative
                discountTotalAmount = discountTotalAmount.add(itemDiscountModel.getAmount().setScale(4, RoundingMode.HALF_UP));
            }
        } else {

            for (LoyaltyRewardModel rewardModel : productLineItemModel.getProduct().getRewards()) {
                if (loyaltyReward.getId().equals(rewardModel.getId())) {

                    //take off the discounts
                    for (ItemDiscountModel itemDiscountModel : productLineItemModel.getDiscounts()) {
                        //use add here as it will be a negative
                        discountTotalAmount = discountTotalAmount.add(itemDiscountModel.getAmount().setScale(4, RoundingMode.HALF_UP));
                    }
                }
            }
        }

        return discountTotalAmount;
    }

    /**
     * For Earning Loyalty Points: If the parent product is available to earn, then all Modifiers earn points.
     * If the parent product is not available to earn points, then all Modifiers earn no points.
     * @param productLineItemModel
     * @return
     */
    private static BigDecimal getModifierAmount(ProductLineItemModel productLineItemModel) {
        BigDecimal modifierTotalAmount = BigDecimal.ZERO;

        for (ModifierLineItemModel modifierLineItemModel : productLineItemModel.getModifiers()) {
            if (modifierLineItemModel.getExtendedAmount() != null && modifierLineItemModel.getExtendedAmount().compareTo(BigDecimal.ZERO) != 0) {   //if the modifier isn't ZERO
                modifierTotalAmount = modifierTotalAmount.add(modifierLineItemModel.getExtendedAmount());
            }
        }

        return modifierTotalAmount;
    }

    private static BigDecimal getTaxAmount(ProductLineItemModel productLineItemModel) {
        return getTaxAmount(productLineItemModel, false);
    }

    /**
     *
     * @param productLineItemModel
     * @param enforceProductTaxMapping - only get tax amounts for ItemTaxModels with their taxes mapped to the Product.  In some cases the ItemTaxModels are created only for the modifiers and not the parent product.
     * @return
     */
    private static BigDecimal getTaxAmount(ProductLineItemModel productLineItemModel, boolean enforceProductTaxMapping) {
        BigDecimal taxTotalAmount = BigDecimal.ZERO;

        for (ItemTaxModel itemTaxModel : productLineItemModel.getTaxes()) {

            //Product does not have tax mapped, it must be mapped to a modifier, don't include the total in the tax total
            if (enforceProductTaxMapping && !productLineItemModel.getProduct().getTaxIds().contains(itemTaxModel.getItemId().toString())){
                continue;
            }

            if (itemTaxModel.getItemTypeId().equals(PosAPIHelper.ObjectType.TAX.toInt())) { //Only include taxes, not tax deletes
                if (itemTaxModel.getAmount().compareTo(BigDecimal.ZERO) != 0) {   //if the tax amount isn't ZERO
                    taxTotalAmount = taxTotalAmount.add(itemTaxModel.getAmount().setScale(4, RoundingMode.HALF_UP));
                }
            }
        }

        return taxTotalAmount;
    }

    //04-13/2018 egl - we decided that modifiers don't get applied the rewards
    //unless they are explicitly set, or if they are set to "isApplyToAllPlus"
    public static BigDecimal getModifierAmountForReward(ProductLineItemModel productLineItemModel, LoyaltyRewardModel loyaltyReward) {
        BigDecimal modifierTotalAmount = BigDecimal.ZERO;

        for (ModifierLineItemModel modifierLineItemModel : productLineItemModel.getModifiers()) {
            if (modifierLineItemModel.getProduct() != null) {
                if (modifierLineItemModel.getExtendedAmount() != null && modifierLineItemModel.getExtendedAmount().compareTo(BigDecimal.ZERO) != 0) {   //if the modifier isn't ZERO
                    if (loyaltyReward.isApplyToAllPlus()) {
                        modifierTotalAmount = modifierTotalAmount.add(modifierLineItemModel.getExtendedAmount());
                    } else {
                        for (LoyaltyRewardModel rewardModel : modifierLineItemModel.getProduct().getRewards()) {
                            if (loyaltyReward.getId().equals(rewardModel.getId())) {
                                modifierTotalAmount = modifierTotalAmount.add(modifierLineItemModel.getExtendedAmount());
                            }
                        }
                    }
                }
            }
        }

        return modifierTotalAmount;
    }

    private static BigDecimal getRewardAmount(ProductLineItemModel productLineItemModel) {
        BigDecimal rewardTotalAmount = BigDecimal.ZERO;


        for (ItemRewardModel rewardItemModel : productLineItemModel.getRewards()) {
            if (rewardItemModel.getAmount().compareTo(BigDecimal.ZERO) != 0) {   //if the modifier isn't ZERO
                rewardTotalAmount = rewardTotalAmount.add(rewardItemModel.getAmount().setScale(4, RoundingMode.HALF_UP));
            }
        }

        return rewardTotalAmount;
    }

    private static BigDecimal getRewardAmountExcludingLoyaltyReward(ProductLineItemModel productLineItemModel, LoyaltyRewardModel loyaltyReward) {
        BigDecimal rewardTotalAmount = BigDecimal.ZERO;

        for (ItemRewardModel rewardItemModel : productLineItemModel.getRewards()) {

            if (!rewardItemModel.getReward().getId().equals(loyaltyReward.getId())) {
                if (rewardItemModel.getAmount().compareTo(BigDecimal.ZERO) != 0) {   //if the reward isn't ZERO
                    rewardTotalAmount = rewardTotalAmount.add(rewardItemModel.getAmount());
                }
            }
        }

        return rewardTotalAmount;
    }

    public void createQuantityLineCalculations() {
        int freeProductRewardCount = 0;
        boolean freeProductRewardApplied = false;
        boolean discountQuantityRestrictionApplied = false;
        this.setQuantityLineDetails(new ArrayList<>());

        BigDecimal discountQuantityRestrictionCount = BigDecimal.ZERO;
        BigDecimal dqrDiscountAmount = BigDecimal.ZERO;

        BigDecimal tempProductAmount = this.getExtendedAmount().divide(this.getQuantity(), 4, BigDecimal.ROUND_HALF_UP);
        BigDecimal tempModifierAmount = getModifierAmount(this);
        tempProductAmount = tempProductAmount.add(tempModifierAmount);

        BigDecimal tempProductQty = this.getQuantity();

        //check for Rewards being applied
        for (ItemRewardModel rewardItemModel : this.getRewards()) {
            if (rewardItemModel.getReward().getRewardTypeId().equals(PosAPIHelper.LoyaltyRewardType.FREE_PRODUCTS.toInt())) {
                freeProductRewardCount++;
                freeProductRewardApplied = true;
            }
        }

        //check for Discount Quantity Restrictions applied
        for (ItemDiscountModel itemDiscountModel : this.getDiscounts()) {
            if (itemDiscountModel.getEligibleQuantity() != null && itemDiscountModel.getEligibleQuantity().compareTo(BigDecimal.ZERO) == 1
                    && itemDiscountModel.getEligibleQuantity().compareTo(this.getQuantity()) == -1) {
                discountQuantityRestrictionApplied = true;
                discountQuantityRestrictionCount = discountQuantityRestrictionCount.add(itemDiscountModel.getEligibleQuantity());
                dqrDiscountAmount = itemDiscountModel.getAmount().divide(itemDiscountModel.getEligibleQuantity(), 4, RoundingMode.HALF_UP);
            }
        }

        //loop through each of the quantities
        BigDecimal workingQuantity = BigDecimal.ZERO;
        while (workingQuantity.compareTo(this.getQuantity()) == -1) {
            BigDecimal adjustedQuantity = BigDecimal.ZERO;
            //check if remainder is less than zero
            if (this.getQuantity().subtract(workingQuantity).compareTo(BigDecimal.ONE) == -1) {
                adjustedQuantity = this.getQuantity().subtract(workingQuantity);
            } else {
                adjustedQuantity = BigDecimal.ONE;
            }
            workingQuantity = workingQuantity.add(adjustedQuantity);

            HashMap quantityHM = new HashMap<>();
            if (freeProductRewardCount > 0) {  //First remove off any free product rewards
                quantityHM.put(adjustedQuantity, BigDecimal.ZERO);
                freeProductRewardCount--; //If there is a free product reward, mark a quantity as $0.00 for earning Point Per Product
            } else if (discountQuantityRestrictionCount.compareTo(BigDecimal.ZERO) == 1) {
                //Second, remove off any discount quantity restriction
                quantityHM.put(adjustedQuantity, tempProductAmount.add(dqrDiscountAmount));
                discountQuantityRestrictionCount = discountQuantityRestrictionCount.subtract(BigDecimal.ONE);
            } else {
                if (discountQuantityRestrictionApplied || freeProductRewardApplied) {
                    quantityHM.put(adjustedQuantity, tempProductAmount);
                } else {
                    quantityHM.put(adjustedQuantity, this.getAdjAmountLoyalty());
                }
            }

            this.getQuantityLineDetails().add(quantityHM);
        }
    }

    /**
     * For Third Party transactions with QCDiscountsApplied, create placeholder ItemDiscountModel's on the Product line items
     * This way, the QcDiscounts are taken into account for loyalty
     *
     * @param totalQcDiscountAmount
     * @param productLineItemAdjAmount
     * @param totalProductAmount
     * @throws Exception
     */
    public void addQcDiscountPlaceHolderLines(BigDecimal totalQcDiscountAmount, BigDecimal productLineItemAdjAmount, BigDecimal totalProductAmount) throws Exception {
        BigDecimal weightedQcDiscountAmount = getWeightedQcDiscountValue(totalQcDiscountAmount, productLineItemAdjAmount, totalProductAmount);
        this.getDiscounts().add(ItemDiscountModel.createQcDiscountLineItemModel(weightedQcDiscountAmount));
    }

    /**
     * For Third Party transactions with TransactionModel.discounts applied, create placeholder ItemDiscountModel's on the Product line items
     * This way, the Discounts are taken into account for loyalty
     *
     * @param productLineItemAdjAmount
     * @param totalProductAmount
     * @throws Exception
     */
    public void addDiscountPlaceHolderLines(BigDecimal productLineItemAdjAmount, BigDecimal totalProductAmount, DiscountLineItemModel discountLineItemModel, ProductLineItemModel productLineItemModel, BigDecimal netDiscountsToApply) throws Exception {
        BigDecimal weightedDiscountAmount = getWeightedQcDiscountValue(netDiscountsToApply, productLineItemAdjAmount, totalProductAmount);
        this.getDiscounts().add(ItemDiscountModel.createDiscountLineItemModel(weightedDiscountAmount, discountLineItemModel, productLineItemModel));
    }

    private BigDecimal getWeightedQcDiscountValue(BigDecimal totalQcDiscountAmount, BigDecimal productLineItemAdjAmount, BigDecimal totalProductAmount) {

        //First, calculate how much the amount will be for this product
        //BigDecimal rewardPoints = new BigDecimal(loyaltyRewardLineItemModel.getReward().getPointsToRedeem());

        BigDecimal weightedRewardValue = BigDecimal.ZERO;
        BigDecimal productPercentageOfTotal = BigDecimal.ZERO;
        BigDecimal oneHundred = new BigDecimal(100);
        //figure out the point amount on the weighted average
        //e.g.:  If all products = $10, and this product amount is $2.00, the percentage is %20
        //this will calculate the point details if only a portion of the tenders are valid

        BigDecimal step1 = productLineItemAdjAmount.multiply(oneHundred);

        //check for a Zero totalProductAmount
        BigDecimal step2 = BigDecimal.ZERO;
        if (totalProductAmount.compareTo(BigDecimal.ZERO) > 0) {
            step2 = step1.divide(totalProductAmount, 10, BigDecimal.ROUND_HALF_UP);   //round this to 10 places to get more accurate values
        }

        productPercentageOfTotal = step2.divide(oneHundred);
        weightedRewardValue = totalQcDiscountAmount.multiply(productPercentageOfTotal);
        weightedRewardValue = weightedRewardValue.setScale(4, RoundingMode.HALF_UP);  //round it back to 2 places so the it appears as a money value

        return weightedRewardValue;
    }

    @JsonIgnore
    public BigDecimal getHighestQuantityLineAmount() {
        BigDecimal adjustedAmount = BigDecimal.ZERO;
        if (this.getQuantityLineDetails().size() > 0) {
            for (HashMap<BigDecimal, BigDecimal> quantityLineHM : this.getQuantityLineDetails()) {

                for (HashMap.Entry<BigDecimal, BigDecimal> entry : quantityLineHM.entrySet()) {
                    if (entry.getValue().compareTo(adjustedAmount) == 1) {
                        adjustedAmount = entry.getValue();
                    }
                }
            }
        }

        return adjustedAmount;
    }

    @JsonIgnore
    public int getOrGenerateTempId(){
        int tempId = (this.getTempId() != null) ? this.getTempId() : ((int) (Math.random() * 100000) + 1);
        return tempId;
    }

    //endregion

    public ArrayList<ItemTaxModel> getTaxes() {
        return taxes;
    }

    public void setTaxes(ArrayList<ItemTaxModel> taxes) {
        this.taxes = taxes;
    }

    public ProductModel getProduct() {
        return product;
    }

    public void setProduct(ProductModel product) {
        this.product = product;
    }

    public ArrayList<ModifierLineItemModel> getModifiers() {
        return modifiers;
    }

    public void setModifiers(ArrayList<ModifierLineItemModel> modifiers) {
        this.modifiers = modifiers;
    }

    public ArrayList<ItemDiscountModel> getDiscounts() {
        return discounts;
    }

    public void setDiscounts(ArrayList<ItemDiscountModel> discounts) {
        this.discounts = discounts;
    }

    public ArrayList<ItemRewardModel> getRewards() {
        return rewards;
    }

    public void setRewards(ArrayList<ItemRewardModel> rewards) {
        this.rewards = rewards;
    }

    @JsonIgnore
    public ArrayList<LoyaltyRewardModel> getRewardsAvailableForProduct() {
        return rewardsAvailableForProduct;
    }

    public void setRewardsAvailableForProduct(ArrayList<LoyaltyRewardModel> rewardsAvailableForProduct) {
        this.rewardsAvailableForProduct = rewardsAvailableForProduct;
    }

    public
    @Override
    @JsonIgnore
        //not returning this for this transaction line item type.  This should be assumed I think.  Trying to clean up the json.
    Integer getItemTypeId() {
        return PosAPIHelper.ObjectType.PRODUCT.toInt();
    }

    public ArrayList<ItemLoyaltyPointModel> getLoyaltyPointDetails() {
        return loyaltyPointDetails;
    }

    public void setLoyaltyPointDetails(ArrayList<ItemLoyaltyPointModel> loyaltyPointDetails) {
        this.loyaltyPointDetails = loyaltyPointDetails;
    }

    @Override
    public BigDecimal getQuantity() {
        return super.getQuantity();
    }

    @JsonIgnore
    public BigDecimal getQuantityForLoyalty() {
        return quantityForLoyalty;
    }

    public void setQuantityForLoyalty(BigDecimal quantityForLoyalty) {
        this.quantityForLoyalty = quantityForLoyalty;
    }

    public boolean hasRewardBeenApplied() {
        return hasRewardBeenApplied;
    }

    public void setHasRewardBeenApplied(boolean hasRewardBeenApplied) {
        this.hasRewardBeenApplied = hasRewardBeenApplied;
    }

    @JsonIgnore
    public BigDecimal getAppliedRewardValue() {
        return appliedRewardValue;
    }

    public void setAppliedRewardValue(BigDecimal appliedRewardValue) {
        this.appliedRewardValue = appliedRewardValue;
    }

    @JsonIgnore
    public BigDecimal getAdjExtAmtLoyalty() {
        return adjExtAmtLoyalty;
    }

    public void setAdjExtAmtLoyalty(BigDecimal adjExtAmtLoyalty) {
        this.adjExtAmtLoyalty = adjExtAmtLoyalty;
    }

    @JsonIgnore
    public BigDecimal getAdjAmountLoyalty() {
        return adjAmountLoyalty;
    }

    public void setAdjAmountLoyalty(BigDecimal adjAmountLoyalty) {
        this.adjAmountLoyalty = adjAmountLoyalty;
    }

    @JsonIgnore
    public BigDecimal getAdjAmountPercentageLoyalty() {
        return adjAmountPercentageLoyalty;
    }

    public void setAdjAmountPercentageLoyalty(BigDecimal adjAmountPercentageLoyalty) {
        this.adjAmountPercentageLoyalty = adjAmountPercentageLoyalty;
    }

    @JsonIgnore
    public boolean isQuantityRefund() {
        return isQuantityRefund;
    }

    public void setIsQuantityRefund(boolean quantityRefund) {
        isQuantityRefund = quantityRefund;
    }

    @JsonIgnore
    public boolean isRefundTheAmount() {
        return refundTheAmount;
    }

    public void setRefundTheAmount(boolean refundTheAmount) {
        this.refundTheAmount = refundTheAmount;
    }

    @JsonIgnore
    public boolean productLevelAppliedToProduct() {
        return productLevelAppliedToProduct;
    }

    public void setProductLevelAppliedToProduct(boolean productLevelAppliedToProduct) {
        this.productLevelAppliedToProduct = productLevelAppliedToProduct;
    }

    @JsonIgnore
    public boolean originallyJustSubDepartment() {
        return originallyJustSubDepartment;
    }

    public void setOriginallyJustSubDepartment(boolean originallyJustSubDepartment) {
        this.originallyJustSubDepartment = originallyJustSubDepartment;
    }

    @JsonIgnore
    public boolean originallyJustDepartment() {
        return originallyJustDepartment;
    }

    public void setOriginallyJustDepartment(boolean originallyJustDepartment) {
        this.originallyJustDepartment = originallyJustDepartment;
    }

    @JsonIgnore
    public boolean wellnessAppliedToProduct() {
        return wellnessAppliedToProduct;
    }

    public void setWellnessAppliedToProduct(boolean wellnessAppliedToProduct) {
        this.wellnessAppliedToProduct = wellnessAppliedToProduct;
    }

    @JsonIgnore
    public Integer getComboLineItemSequence() {
        return comboLineItemSequence;
    }

    public void setComboLineItemSequence(Integer comboLineItemSequence) {
        this.comboLineItemSequence = comboLineItemSequence;
    }

    @JsonIgnore
    public PosAPIHelper.LoyaltyLevelEarningType getLoyaltyLevelEarningType() {
        return loyaltyLevelEarningType;
    }

    public void setLoyaltyLevelEarningType(PosAPIHelper.LoyaltyLevelEarningType loyaltyLevelEarningType) {
        this.loyaltyLevelEarningType = loyaltyLevelEarningType;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getKeypadId() {
        return keypadId;
    }

    public void setKeypadId(Integer keypadId) {
        this.keypadId = keypadId;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getPrintStatusId() {
        return printStatusId;
    }

    public void setPrintStatusId(Integer printStatusId) {
        this.printStatusId = printStatusId;
    }

    public ArrayList<ItemSurchargeModel> getSurcharges() {
        return surcharges;
    }

    public void setSurcharges(ArrayList<ItemSurchargeModel> surcharges) {
        this.surcharges = surcharges;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getComboTransLineItemId() {
        return comboTransLineItemId;
    }

    public void setComboTransLineItemId(Integer comboTransLineItemId) {
        this.comboTransLineItemId = comboTransLineItemId;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getComboDetailId() {
        return comboDetailId;
    }

    public void setComboDetailId(Integer comboDetailId) {
        this.comboDetailId = comboDetailId;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public BigDecimal getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(BigDecimal basePrice) {
        this.basePrice = basePrice;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public BigDecimal getComboPrice() {
        return comboPrice;
    }

    public void setComboPrice(BigDecimal comboPrice) {
        this.comboPrice = comboPrice;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public BigDecimal getUpcharge() {
        return upcharge;
    }

    public void setUpcharge(BigDecimal upcharge) {
        this.upcharge = upcharge;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getTempId() {
        return tempId;
    }

    public void setTempId(Integer tempId) {
        this.tempId = tempId;
    }

    @JsonIgnore
    public ArrayList<HashMap<BigDecimal, BigDecimal>> getQuantityLineDetails() {
        return quantityLineDetails;
    }

    public void setQuantityLineDetails(ArrayList<HashMap<BigDecimal, BigDecimal>> quantityLineDetails) {
        this.quantityLineDetails = quantityLineDetails;
    }

    @JsonIgnore
    public boolean hasDiscountApplied() {
        return discountApplied;
    }

    public void setDiscountApplied(boolean discountApplied) {
        this.discountApplied = discountApplied;
    }

    @JsonIgnore
    public boolean hasMealPlanApplied() {
        return mealPlanApplied;
    }

    public void setMealPlanApplied(boolean mealPlanApplied) {
        this.mealPlanApplied = mealPlanApplied;
    }

    public PrepOptionModel getPrepOption() {
        return prepOption;
    }

    public void setPrepOption(PrepOptionModel prepOption) {
        this.prepOption = prepOption;
    }

    @JsonIgnore
    public Integer getParentPaTransLineItemId() {
        return parentPaTransLineItemId;
    }

    public void setParentPaTransLineItemId(Integer parentPaTransLineItemId) {
        this.parentPaTransLineItemId = parentPaTransLineItemId;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getLinkedFromPaTransLineItemIds() {
        return linkedFromPaTransLineItemIds;
    }

    public void setLinkedFromPaTransLineItemIds(String linkedFromPaTransLineItemIds) {
        this.linkedFromPaTransLineItemIds = linkedFromPaTransLineItemIds;
    }

    /**
     * Overridden toString method for a ProductLineItemModel.
     * @return The {@link String} representation of a ProductLineItemModel.
     */
    @Override
    public String toString () {

        String taxesStr = "";
        if (!DataFunctions.isEmptyCollection(taxes)) {
            taxesStr += "[";
            for (ItemTaxModel tax : taxes) {
                if (tax != null && tax.equals(taxes.get(taxes.size() - 1))) {
                    taxesStr += "[" + tax.toString() + "]";
                }
                else if (tax != null && !tax.equals(taxes.get(taxes.size() - 1))) {
                    taxesStr += "[" + tax.toString() + "]; ";
                }
            }
            taxesStr += "]";
        }
        
        String discountsStr = "";
        if (!DataFunctions.isEmptyCollection(discounts)) {
            discountsStr += "[";
            for (ItemDiscountModel discount : discounts) {
                if (discount != null && discount.equals(discounts.get(discounts.size() - 1))) {
                    discountsStr += "[" + discount.toString() + "]";
                }
                else if (discount != null && !discount.equals(discounts.get(discounts.size() - 1))) {
                    discountsStr += "[" + discount.toString() + "]; ";
                }
            }
            discountsStr += "]";
        }

        String modifiersStr = "";
        if (!DataFunctions.isEmptyCollection(modifiers)) {
            modifiersStr += "[";
            for (ModifierLineItemModel modifier : modifiers) {
                if (modifier != null && modifier.equals(modifiers.get(modifiers.size() - 1))) {
                    modifiersStr += "[" + modifier.toString() + "]";
                }
                else if (modifier != null && !modifier.equals(modifiers.get(modifiers.size() - 1))) {
                    modifiersStr += "[" + modifier.toString() + "]; ";
                }
            }
            modifiersStr += "]";
        }
        
        String rewardsStr = "";
        if (!DataFunctions.isEmptyCollection(rewards)) {
            rewardsStr += "[";
            for (ItemRewardModel reward : rewards) {
                if (reward != null && reward.equals(rewards.get(rewards.size() - 1))) {
                    rewardsStr += "[" + reward.toString() + "]";
                }
                else if (reward != null && !reward.equals(rewards.get(rewards.size() - 1))) {
                    rewardsStr += "[" + reward.toString() + "]; ";
                }
            }
            rewardsStr += "]";
        }
        
        String loyaltyPointDetailsStr = "";
        if (!DataFunctions.isEmptyCollection(loyaltyPointDetails)) {
            loyaltyPointDetailsStr += "[";
            for (ItemLoyaltyPointModel loyaltyPointDetail : loyaltyPointDetails) {
                if (loyaltyPointDetail != null && loyaltyPointDetail.equals(loyaltyPointDetails.get(loyaltyPointDetails.size() - 1))) {
                    loyaltyPointDetailsStr += "[" + loyaltyPointDetail.toString() + "]";
                }
                else if (loyaltyPointDetail != null && !loyaltyPointDetail.equals(loyaltyPointDetails.get(loyaltyPointDetails.size() - 1))) {
                    loyaltyPointDetailsStr += "[" + loyaltyPointDetail.toString() + "]; ";
                }
            }
            loyaltyPointDetailsStr += "]";
        }

        String rewardsAvailableForProductStr = "";
        if (!DataFunctions.isEmptyCollection(rewardsAvailableForProduct)) {
            rewardsAvailableForProductStr += "[";
            for (LoyaltyRewardModel rewardAvailableForProduct : rewardsAvailableForProduct) {
                if (rewardAvailableForProduct != null && rewardAvailableForProduct.equals(rewardsAvailableForProduct.get(rewardsAvailableForProduct.size() - 1))) {
                    rewardsAvailableForProductStr += "[" + rewardAvailableForProduct.toString() + "]";
                }
                else if (rewardAvailableForProduct != null && !rewardAvailableForProduct.equals(rewardsAvailableForProduct.get(rewardsAvailableForProduct.size() - 1))) {
                    rewardsAvailableForProductStr += "[" + rewardAvailableForProduct.toString() + "]; ";
                }
            }
            rewardsAvailableForProductStr += "]";
        }

        String surchargesStr = "";
        if (!DataFunctions.isEmptyCollection(surcharges)) {
            surchargesStr += "[";
            for (ItemSurchargeModel surcharge : surcharges) {
                if (surcharge != null && surcharge.equals(surcharges.get(surcharges.size() - 1))) {
                    surchargesStr += "[" + surcharge.toString() + "]";
                }
                else if (surcharge != null && !surcharge.equals(surcharges.get(surcharges.size() - 1))) {
                    surchargesStr += "[" + surcharge.toString() + "]; ";
                }
            }
            surchargesStr += "]";
        }

        String quantityLineDetailsStr = "";
        if (!DataFunctions.isEmptyCollection(quantityLineDetails)) {
            quantityLineDetailsStr += "[";
            for (HashMap<BigDecimal, BigDecimal> quantityLineDetail : quantityLineDetails) {
                quantityLineDetailsStr += "[";

                if (!DataFunctions.isEmptyMap(quantityLineDetail)) {
                    int quantityLineDetailSize = quantityLineDetail.size();
                    int numberOfEntriesExamined = 0;
                    for (Map.Entry<BigDecimal, BigDecimal> entry : quantityLineDetail.entrySet()) {
                        if ((entry.getKey() != null) && (entry.getValue() != null) && (numberOfEntriesExamined - 1 < quantityLineDetailSize)) {
                            quantityLineDetailsStr += "[" + String.valueOf(entry.getKey()) + ": " + entry.getValue().toString() + "], ";
                        }
                        else if ((entry.getKey() != null) && (entry.getValue() != null) && (numberOfEntriesExamined - 1 == quantityLineDetailSize)) {
                            quantityLineDetailsStr+= "[" + String.valueOf(entry.getKey()) + ": " + entry.getValue().toString() + "]";
                        }
                        numberOfEntriesExamined++;
                    }
                }

                if (quantityLineDetail.equals(quantityLineDetails.get(quantityLineDetails.size() - 1))) {
                    quantityLineDetailsStr += "]";
                }
                else if (!quantityLineDetail.equals(quantityLineDetails.get(quantityLineDetails.size() - 1))) {
                    quantityLineDetailsStr += "], ";
                }
            }
            quantityLineDetailsStr += "]";
        }

        return String.format("PRODUCT: %s, TAXES:%s, DISCOUNTS: %s, MODIFIERS: %s, REWARDS: %s, LOYALTYPOINTDETAILS: %s, " +
                "REWARDSAVAILABLEFORPRODUCT: %s, SURCHARGES: %s, APPLIEDREWARDVALUE: %s, ADJAMOUNTLOYALTY: %s, " +
                "ADJEXTAMTLOYALTY: %s, ADJAMOUNTPERCENTAGELOYALTY: %s, QUANTITYFORLOYALTY: %s, HASREWARDBEENAPPLIED: %s, " +
                "ISQUANTITYREFUND: %s, REFUNDTHEAMOUNT: %s, PRODUCTLEVELAPPLIEDTOPRODUCT: %s, ORIGINALLYJUSTSUBDEPARTMENT: %s, " +
                "ORIGINALLYJUSTDEPARTMENT: %s, WELLNESSAPPLIEDTOPRODUCT: %s, DISCOUNTAPPLIED: %s, MEALPLANAPPLIED: %s, " +
                "LINEITEMSEQUENCE: %s, COMBOLINEITEMSEQUENCE: %s, KEYPADID: %s, PRINTSTATUSID: %s, COMBOTRANSLINEITEMID: %s, " +
                "COMBODETAILID: %s, TEMPID: %s, PARENTPATRANSLINEITEMID: %s, BASEPRICE: %s, COMBOPRICE: %s, UPCHARGE: %s, " +
                "LOYALTYLEVELEARNINGTYPE: %s, QUANTITYLINEDETAILS: %s",
                Objects.toString((product != null ? product.toString() : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(taxesStr) ? taxesStr : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(discountsStr) ? discountsStr : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(modifiersStr) ? modifiersStr : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(rewardsStr) ? rewardsStr : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(loyaltyPointDetailsStr) ? loyaltyPointDetailsStr : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(rewardsAvailableForProductStr) ? rewardsAvailableForProductStr : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(surchargesStr) ? surchargesStr : "N/A"), "N/A"),
                Objects.toString((appliedRewardValue != null ? appliedRewardValue.toPlainString() : "N/A"), "N/A"),
                Objects.toString((adjAmountLoyalty != null ? adjAmountLoyalty.toPlainString() : "N/A"), "N/A"),
                Objects.toString((adjExtAmtLoyalty != null ? adjExtAmtLoyalty.toPlainString() : "N/A"), "N/A"),
                Objects.toString((adjAmountPercentageLoyalty != null ? adjAmountPercentageLoyalty.toPlainString() : "N/A"), "N/A"),
                Objects.toString((quantityForLoyalty != null ? quantityForLoyalty.toPlainString() : "N/A"), "N/A"),
                Objects.toString(hasRewardBeenApplied, "N/A"),
                Objects.toString(isQuantityRefund, "N/A"),
                Objects.toString(refundTheAmount, "N/A"),
                Objects.toString(productLevelAppliedToProduct, "N/A"),
                Objects.toString(originallyJustSubDepartment, "N/A"),
                Objects.toString(originallyJustDepartment, "N/A"),
                Objects.toString(wellnessAppliedToProduct, "N/A"),
                Objects.toString(discountApplied, "N/A"),
                Objects.toString(mealPlanApplied, "N/A"),
                Objects.toString((comboLineItemSequence != null && comboLineItemSequence > 0 ? comboLineItemSequence : "N/A"), "N/A"),
                Objects.toString((keypadId != null && keypadId > 0 ? keypadId : "N/A"), "N/A"),
                Objects.toString((printStatusId != null && printStatusId > 0 ? printStatusId : "N/A"), "N/A"),
                Objects.toString((comboTransLineItemId != null && comboTransLineItemId > 0 ? comboTransLineItemId : "N/A"), "N/A"),
                Objects.toString((comboDetailId != null && comboDetailId > 0 ? comboDetailId : "N/A"), "N/A"),
                Objects.toString((tempId != null && tempId > 0 ? tempId : "N/A"), "N/A"),
                Objects.toString((parentPaTransLineItemId != null && parentPaTransLineItemId > 0 ? parentPaTransLineItemId : "N/A"), "N/A"),
                Objects.toString((basePrice != null ? basePrice.toPlainString() : "N/A"), "N/A"),
                Objects.toString((comboPrice != null ? comboPrice.toPlainString() : "N/A"), "N/A"),
                Objects.toString((upcharge != null ? upcharge.toPlainString() : "N/A"), "N/A"),
                Objects.toString((loyaltyLevelEarningType != null ? String.valueOf(loyaltyLevelEarningType.toInt()) : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(quantityLineDetailsStr) ? quantityLineDetailsStr : "N/A"), "N/A"),
                Objects.toString((getTransactionItemNum() != null && getTransactionItemNum() > 0 ? getTransactionItemNum() : "N/A"), "N/A"));

    }

}
