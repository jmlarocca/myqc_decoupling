package com.mmhayes.common.transaction.models;

//MMHayes Dependencies
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.terminal.models.TerminalModel;

//API Dependencies
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionDeserializer;

//Other Dependencies
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2018-01-12 10:37:41 -0500 (Fri, 12 Jan 2018) $: Date of last commit
 $Rev: 9024 $: Revision of last commit
*/

@JsonPropertyOrder({"id", "transactionTypeId", "transactionType", "transactionStatusId", "timeStamp", "employeeId", "terminalId", "userName", "transactionLabel",
        "transactionTotal", "tenders", "terminalModel" })
public class TransactionSummaryModel {
    private Integer id = null;
    private Integer transactionTypeId;
    private Integer employeeId = null;
    private Integer terminalId;
    private Integer transactionStatusId = null;
    private String timeStamp = null;
    private String transactionType = "";
    private String userName = "";
    private String transactionLabel = "";

    private BigDecimal transactionTotal = BigDecimal.ZERO;
    private List<TenderLineItemModel> tenders = new ArrayList<>();
    private TerminalModel terminalModel = null;

    public TransactionSummaryModel() {

    }

    public TransactionSummaryModel(HashMap modelDetailHM) throws Exception {

        setModelProperties(modelDetailHM);
    }

    public TransactionSummaryModel setModelProperties(HashMap modelDetailHM) throws Exception {
        this.setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));
        this.setTransactionTypeId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("TRANSTYPEID")));
        this.setTransactionType(CommonAPI.convertModelDetailToString(modelDetailHM.get("TRANSTYPENAME")));
        this.setTimeStamp(CommonAPI.convertModelDetailToString(modelDetailHM.get("TRANSACTIONDATE")));
        this.setTerminalId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("TERMINALID")));
        this.setEmployeeId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("EMPLOYEEID")));
        this.setTransactionStatusId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PATRANSSTATUSID")));
        this.setTransactionTotal(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("TOTALTRANSACTIONAMOUNT")));
        this.setUserName(CommonAPI.convertModelDetailToString(modelDetailHM.get("USERNAME")));
        this.setTransactionLabel(CommonAPI.convertModelDetailToString(modelDetailHM.get("TRANSNAME")));

        this.setTerminalModel(TerminalModel.createTerminalModel(modelDetailHM, false, false));
        this.getTerminalModel().setId(this.getTerminalId()); //reset the Terminal ID so we don't have to change the TerminalModel.CreateModel

        return this;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public Integer getTransactionTypeId() {
        return transactionTypeId;
    }

    public void setTransactionTypeId(Integer transactionTypeId) {
        this.transactionTypeId = transactionTypeId;
    }

    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public List<TenderLineItemModel> getTenders() {
        return tenders;
    }

    public void setTenders(List<TenderLineItemModel> tenders) {
        this.tenders = tenders;
    }

    public BigDecimal getTransactionTotal() {
        return transactionTotal;
    }

    public void setTransactionTotal(BigDecimal transactionTotal) {
        this.transactionTotal = transactionTotal;
    }

    @JsonIgnore
    public Integer getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(Integer terminalId) {
        this.terminalId = terminalId;
    }

    public TerminalModel getTerminalModel() {
        return terminalModel;
    }

    public void setTerminalModel(TerminalModel terminalModel) {
        this.terminalModel = terminalModel;
    }

    @JsonIgnore
    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public Integer getTransactionStatusId() {
        return transactionStatusId;
    }

    public void setTransactionStatusId(Integer transactionStatusId) {
        this.transactionStatusId = transactionStatusId;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getTransactionLabel() {
        return transactionLabel;
    }

    public void setTransactionLabel(String transactionLabel) {
        this.transactionLabel = transactionLabel;
    }

}
