package com.mmhayes.common.transaction.models;

//MMHayes Dependencies

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mmhayes.common.terminal.models.TerminalModel;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

//API Dependencies
//Other Dependencies

/*
 Last Updated (automatically updated by SVN)
 $Author: jkflanagan $: Author of last commit
 $Date: 2020-12-31 16:39:34 -0500 (Thu, 31 Dec 2020) $: Date of last commit
 $Rev: 13381 $: Revision of last commit
*/
public class SurchargeDisplayModel extends SurchargeModel {

    public SurchargeDisplayModel() {
        super();

    }

    public SurchargeDisplayModel(HashMap surchargeHM, TerminalModel terminalModel) {
        setModelProperties(surchargeHM, terminalModel);
    }

    public static SurchargeDisplayModel createSurchargeModel(HashMap surchargeHM, TerminalModel terminalModel) {
        return new SurchargeDisplayModel(surchargeHM, terminalModel);
    }

    public static SurchargeDisplayModel createSurchargeModel(SurchargeModel surchargeModel) {
        SurchargeDisplayModel surchargeDisplayModel = new SurchargeDisplayModel();

        surchargeDisplayModel.setId(surchargeModel.getId());
        surchargeDisplayModel.setName(surchargeModel.getName());
        surchargeDisplayModel.setType(surchargeModel.getType());
        surchargeDisplayModel.setTypeId(surchargeModel.getTypeId());
        surchargeDisplayModel.setApplicationType(surchargeModel.getApplicationType());
        surchargeDisplayModel.setApplicationTypeId(surchargeModel.getApplicationTypeId());

        surchargeDisplayModel.setAmountType(surchargeModel.getAmountType());
        surchargeDisplayModel.setAmountTypeId(surchargeModel.getAmountTypeId());

        surchargeDisplayModel.setShortName(surchargeModel.getShortName());
        surchargeDisplayModel.setIncludeRewards(surchargeModel.getIncludeRewards());
        surchargeDisplayModel.setIncludeItemDiscounts(surchargeModel.getIncludeItemDiscounts());
        surchargeDisplayModel.setIncludeSubtotalDiscounts(surchargeModel.getIncludeSubtotalDiscounts());
        surchargeDisplayModel.setIncludeTaxes(surchargeModel.getIncludeTaxes());
        surchargeDisplayModel.setUseAsConvenienceFee(surchargeModel.getUseAsConvenienceFee());
        surchargeDisplayModel.setEligibleForSubtotalSurcharge(surchargeModel.getEligibleForSubtotalSurcharge());

        surchargeDisplayModel.setOpen(surchargeModel.isOpen());
        surchargeDisplayModel.setPreset(surchargeModel.isPreset());
        surchargeDisplayModel.setRefundsWithProducts(surchargeModel.getRefundsWithProducts());
        surchargeDisplayModel.setAmount(surchargeModel.getAmount());

        surchargeDisplayModel.setTaxes(surchargeModel.getTaxes());

        return surchargeDisplayModel;
    }

    @Override
    @JsonIgnore
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    @JsonIgnore
    public String getApplicationType() {
        return applicationType;
    }

    public void setApplicationType(String applicationType) {
        this.applicationType = applicationType;
    }

    @Override
    @JsonIgnore
    public String getAmountType() {
        return amountType;
    }

    public void setAmountType(String amountType) {
        this.amountType = amountType;
    }

    @Override
    @JsonIgnore
    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    @Override
    @JsonIgnore
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    @JsonIgnore
    public Boolean getIncludeRewards() {
        return includeRewards;
    }

    public void setIncludeRewards(Boolean includeRewards) {
        this.includeRewards = includeRewards;
    }

    @Override
    @JsonIgnore
    public Boolean getIncludeItemDiscounts() {
        return includeItemDiscounts;
    }

    public void setIncludeItemDiscounts(Boolean includeItemDiscounts) {
        this.includeItemDiscounts = includeItemDiscounts;
    }

    @Override
    @JsonIgnore
    public Boolean getIncludeSubtotalDiscounts() {
        return includeSubtotalDiscounts;
    }

    public void setIncludeSubtotalDiscounts(Boolean includeSubtotalDiscounts) {
        this.includeSubtotalDiscounts = includeSubtotalDiscounts;
    }

    @Override
    @JsonIgnore
    public Boolean getIncludeTaxes() {
        return includeTaxes;
    }

    public void setIncludeTaxes(Boolean includeTaxes) {
        this.includeTaxes = includeTaxes;
    }

    @Override
    @JsonIgnore
    public Boolean isOpen() {
        return open;
    }

    public void setOpen(Boolean open) {
        this.open = open;
    }

    @Override
    @JsonIgnore
    public Boolean isPreset() {
        return preset;
    }

    public void setPreset(Boolean preset) {
        this.preset = preset;
    }

    @Override
    @JsonIgnore
    public Integer getApplicationTypeId() {
        return applicationTypeId;
    }

    public void setApplicationTypeId(Integer applicationTypeId) {
        this.applicationTypeId = applicationTypeId;
    }

    @Override
    @JsonIgnore
    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    @Override
    @JsonIgnore
    public Integer getAmountTypeId() {
        return amountTypeId;
    }

    public void setAmountTypeId(Integer amountTypeId) {
        this.amountTypeId = amountTypeId;
    }

    @Override
    @JsonIgnore
    public Boolean getRefundsWithProducts() {
        return refundsWithProducts;
    }

    public void setRefundsWithProducts(Boolean refundsWithProducts) {
        this.refundsWithProducts = refundsWithProducts;
    }

    @Override
    @JsonIgnore
    public Integer getLinkedLineItemId() {
        return linkedLineItemId;
    }

    public void setLinkedLineItemId(Integer linkedLineItemId) {
        this.linkedLineItemId = linkedLineItemId;
    }

    @Override
    @JsonIgnore
    public List<TaxModel> getTaxes() {
        return taxes;
    }

    public void setTaxes(List<TaxModel> taxes) {
        this.taxes = taxes;
    }

    /**
     * Overridden toString method for a SurchargeDisplayModel.
     * @return The {@link String} representation of a SurchargeDisplayModel.
     */
    @Override
    public String toString () {
        return super.toString();
    }

}
