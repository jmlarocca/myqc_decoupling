package com.mmhayes.common.transaction.models;

//mmhayes dependencies

import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.api.PosApi30.TransactionLineBuilder;
import com.mmhayes.common.dataaccess.DataManager;

//API dependencies
import com.mmhayes.common.api.CommonAPI;
import com.fasterxml.jackson.annotation.JsonIgnore;

//other dependencies
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

/*
 $Author: nyu $: Author of last commit
 $Date: 2018-06-28 17:11:03 -0400 (Thu, 28 Jun 2018) $: Date of last commit
 $Rev: 7351 $: Revision of last commit
 Notes:
*/
public class CashierShiftEndModel {
    private Integer transactionId;
    private Integer customerCount;
    private Integer voidModeQty;
    private Integer refundModeQty;
    private Integer cancelTransQty;
    private Integer noSalesQty;
    private Integer newPaTransactionId;
    private Integer priceOverridesQty = 0;
    private Integer directVoidQty;
    private Integer inDirectVoidQty;
    private Integer offlineCreditCardChargesQty = 0;
    private BigDecimal grossRefundVoids;
    private BigDecimal voidModeAmt;
    private BigDecimal refundModeAmt;
    private BigDecimal cancelTransAmt;
    private BigDecimal totalTax;
    private BigDecimal totalTaxExempt;
    private BigDecimal grossSales;
    private BigDecimal directVoidAmt;
    private BigDecimal inDirectVoidAmt;
    private boolean isXOut = true; //isXout == true, nothing get closed out and saved to the database

    ArrayList<CashierShiftEndTransCount> transCountList = new ArrayList<>();
    private Integer startTransactionId;
    private String startTransactionDate;

    private Integer previousZTransactionId;
    private String previousZTransactionDate;
    private TransactionModel xOutTransaction = null;

    public CashierShiftEndModel() {

    }

    //Constructor- takes in a Hashmap
    public CashierShiftEndModel(HashMap modelHM) throws Exception {
        setModelProperties(modelHM);
    }

    //setter for all of this model's properties
    public CashierShiftEndModel setModelProperties(HashMap modelDetailHM) throws Exception
    {
        setTransactionId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PATRANSACTIONID")));
        setGrossSales(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("GROSSPLUSALES")));
        setGrossRefundVoids(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("GROSSPLUREFUNDVOIDS")));

        setCustomerCount(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("CUSTOMERCOUNT")));
        setVoidModeQty(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("VOIDMODEQTY")));
        setVoidModeAmt(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("VOIDMODEAMT")));
        setRefundModeQty(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("REFUNDMODEQTY")));
        setRefundModeAmt(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("REFUNDMODEAMT")));
        setCancelTransQty(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("CANCELTRANSQTY")));
        setCancelTransAmt(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("CANCELTRANSAMT")));
        setNoSalesQty(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("NOSALESQTY")));

        return this;
    }

    public static CashierShiftEndModel getOneCashierShiftEndModel(Integer paTransactionId, Integer terminalId) throws Exception
    {
        DataManager dm = new DataManager();
        CashierShiftEndModel cashierShiftEndModel = new CashierShiftEndModel();

        //get Discount information Discount Id

        //get all models in an array list
        ArrayList<HashMap> cashierShiftEndList = dm.parameterizedExecuteQuery("data.posapi30.getOneCashierShiftEnd",
                new Object[]{
                        paTransactionId
                },
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        HashMap modelDetailHM = null;

        if (cashierShiftEndList != null && cashierShiftEndList.size() > 0)
        {
            modelDetailHM = cashierShiftEndList.get(0);
            return cashierShiftEndModel.setModelProperties(modelDetailHM);
        }
        else
        {
            return null;
        }
    }

    //used to convert the arraylist to a CashierShiftEndModel after the records are saved in POSAnyWhereDataManager.endCashierShift()
    public static CashierShiftEndModel createCashierShiftEndModel(ArrayList cashierEndShiftArrayList, TransactionModel transactionModel, boolean isXOut) throws Exception
    {
        if (cashierEndShiftArrayList == null || cashierEndShiftArrayList.size() == 0)
        {
            return null;
        }
        else
        {
            if (cashierEndShiftArrayList.get(0) != null)
            {
                CashierShiftEndModel cashierShiftEndModel = new CashierShiftEndModel();
                cashierShiftEndModel.setXOut(isXOut);

                HashMap modelDetailHM = (HashMap) cashierEndShiftArrayList.get(0); //the first record is normally the CashierShiftModel record
                cashierShiftEndModel.setTransactionId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PATRANSACTIONID")));
                cashierShiftEndModel.setGrossSales(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("GROSSALES")));
                cashierShiftEndModel.setGrossRefundVoids(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("GROSSREFUNDVOIDS")));
                cashierShiftEndModel.setCustomerCount(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("CUSTOMERCOUNT")));
                cashierShiftEndModel.setVoidModeQty(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("VOIDMODEQTY")));
                cashierShiftEndModel.setVoidModeAmt(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("VOIDMODEAMT")));
                cashierShiftEndModel.setRefundModeQty(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("REFUNDMODEQTY")));
                cashierShiftEndModel.setRefundModeAmt(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("REFUNDMODEAMT")));
                cashierShiftEndModel.setCancelTransQty(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("CANCELQTY")));
                cashierShiftEndModel.setCancelTransAmt(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("CANCELTRANSAMT")));
                cashierShiftEndModel.setNoSalesQty(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("NOSALESQTY")));
                cashierShiftEndModel.setTotalTax(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("TOTALTAX")));
                cashierShiftEndModel.setTotalTaxExempt(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("TOTALTAXEXEMPT")));
                cashierShiftEndModel.setPriceoverridesQty(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PRICEOVERRIDESQTY")));
                cashierShiftEndModel.setNewPaTransactionId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("NEWTRANSID")));
                cashierShiftEndModel.setDirectVoidQty(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("DIRECTVOIDQTY")));
                cashierShiftEndModel.setDirectVoidAmt(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("DIRECTVOIDAMT")));
                cashierShiftEndModel.setInDirectVoidQty(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("INDIRECTVOIDQTY")));
                cashierShiftEndModel.setInDirectVoidAmt(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("INDIRECTVOIDAMT")));
                cashierShiftEndModel.setOfflineCreditCardChargesQty(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("NUMOFFLINECCCHARGES")));

                //set up Start Trans Info
                Object startTransInfo = modelDetailHM.get("STARTTRANSINFO");
                if (startTransInfo != null)
                {
                    ArrayList<HashMap> startTransList = (ArrayList<HashMap>) startTransInfo;

                    if (startTransList.size() > 0)
                    {
                        cashierShiftEndModel.setStartTransactionId(CommonAPI.convertModelDetailToInteger(startTransList.get(0).get("PATRANSACTIONID")));
                        cashierShiftEndModel.setStartTransactionDate(CommonAPI.convertModelDetailToString(startTransList.get(0).get("TRANSACTIONDATE")));
                    }
                }

                //set up previous z info
                Object prevZInfo = modelDetailHM.get("PREVZINFO");
                if (prevZInfo != null)
                {
                    ArrayList<HashMap> prevZList = (ArrayList<HashMap>) prevZInfo;

                    if (prevZList.size() > 0)
                    {
                        cashierShiftEndModel.setPreviousZTransactionId(CommonAPI.convertModelDetailToInteger(prevZList.get(0).get("PATRANSACTIONID")));
                        cashierShiftEndModel.setPreviousZTransactionDate(CommonAPI.convertModelDetailToString(prevZList.get(0).get("TRANSACTIONDATE")));
                    }
                }

                //Create an ArrayList of the TransCount per ObjectTypeId and ObjectId
                ArrayList<CashierShiftEndTransCount> transCountList = new ArrayList<>();
                if (cashierEndShiftArrayList.size() > 1)
                {
                    ArrayList<HashMap> transLineHM = (ArrayList<HashMap>) cashierEndShiftArrayList;
                    for (HashMap cashierEndShiftHM : transLineHM)
                    {
                        //skip the first field
                        if (CommonAPI.convertModelDetailToInteger(cashierEndShiftHM.get("PAITEMID")) == null
                                && CommonAPI.convertModelDetailToInteger(cashierEndShiftHM.get("PAITEMTYPEID")) == null
                                && CommonAPI.convertModelDetailToInteger(cashierEndShiftHM.get("TRANSCOUNT")) == null)
                        {
                            continue;
                        }

                        CashierShiftEndTransCount cashierEndShiftTransCount = new CashierShiftEndTransCount();
                        cashierEndShiftTransCount.setObjectId(CommonAPI.convertModelDetailToInteger(cashierEndShiftHM.get("PAITEMID")));
                        cashierEndShiftTransCount.setObjectTypeId(CommonAPI.convertModelDetailToInteger(cashierEndShiftHM.get("PAITEMTYPEID")));
                        cashierEndShiftTransCount.setTransCount(CommonAPI.convertModelDetailToInteger(cashierEndShiftHM.get("TRANSCOUNT")));
                        transCountList.add(cashierEndShiftTransCount);
                    }
                }
                cashierShiftEndModel.setTransCountList(transCountList);

                if (isXOut)
                {
                    if (cashierEndShiftArrayList.size() > 1)
                    {
                        //remove the first line which was used for the Shift End Model
                        cashierEndShiftArrayList.remove(0);

                        //map all subsequent lines
                        ArrayList<HashMap> transLineHM = (ArrayList<HashMap>) cashierEndShiftArrayList;

                        //Create transLine from hashmap
                        transactionModel = TransactionLineBuilder.getTransactionLinesForCashierEndShift(transLineHM, transactionModel);

                        cashierShiftEndModel.setxOutTransaction(transactionModel);
                    }
                }

                return cashierShiftEndModel;
            }
            else
            {
                return null;
            }
        }
    }

    public Integer getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Integer transactionId) {
        this.transactionId = transactionId;
    }

    public Integer getCustomerCount() {
        return customerCount;
    }

    public void setCustomerCount(Integer customerCount) {
        this.customerCount = customerCount;
    }

    public Integer getVoidModeQty() {
        return voidModeQty;
    }

    public void setVoidModeQty(Integer voidModeQty) {
        this.voidModeQty = voidModeQty;
    }

    public BigDecimal getVoidModeAmt() {
        return voidModeAmt;
    }

    public void setVoidModeAmt(BigDecimal voidModeAmt) {
        this.voidModeAmt = voidModeAmt;
    }

    public Integer getRefundModeQty() {
        return refundModeQty;
    }

    public void setRefundModeQty(Integer refundModeQty) {
        this.refundModeQty = refundModeQty;
    }

    public BigDecimal getRefundModeAmt() {
        return refundModeAmt;
    }

    public void setRefundModeAmt(BigDecimal refundModeAmt) {
        this.refundModeAmt = refundModeAmt;
    }

    public Integer getCancelTransQty() {
        return cancelTransQty;
    }

    public void setCancelTransQty(Integer cancelTransQty) {
        this.cancelTransQty = cancelTransQty;
    }

    public BigDecimal getCancelTransAmt() {
        return cancelTransAmt;
    }

    public void setCancelTransAmt(BigDecimal cancelTransAmt) {
        this.cancelTransAmt = cancelTransAmt;
    }

    public Integer getNoSalesQty() {
        return noSalesQty;
    }

    public void setNoSalesQty(Integer noSalesQty) {
        this.noSalesQty = noSalesQty;
    }

    public Integer getNewPaTransactionId() {
        return newPaTransactionId;
    }

    public void setNewPaTransactionId(Integer newPaTransactionId) {
        this.newPaTransactionId = newPaTransactionId;
    }

    public BigDecimal getTotalTax() {
        return totalTax;
    }

    public void setTotalTax(BigDecimal totalTax) {
        this.totalTax = totalTax;
    }

    public BigDecimal getTotalTaxExempt() {
        return totalTaxExempt;
    }

    public void setTotalTaxExempt(BigDecimal totalTaxExempt) {
        this.totalTaxExempt = totalTaxExempt;
    }

    public BigDecimal getGrossSales() {
        return grossSales;
    }

    public void setGrossSales(BigDecimal grossSales) {
        this.grossSales = grossSales;
    }

    public Integer getPriceoverridesQty() {
        return priceOverridesQty;
    }

    public void setPriceoverridesQty(Integer priceOverridesQty) {
        this.priceOverridesQty = priceOverridesQty;
    }

    public BigDecimal getGrossRefundVoids() {
        return grossRefundVoids;
    }

    public void setGrossRefundVoids(BigDecimal grossRefundVoids) {
        this.grossRefundVoids = grossRefundVoids;
    }

    public Integer getDirectVoidQty() {
        return directVoidQty;
    }

    public void setDirectVoidQty(Integer directVoidQty) {
        this.directVoidQty = directVoidQty;
    }

    public BigDecimal getDirectVoidAmt() {
        return directVoidAmt;
    }

    public void setDirectVoidAmt(BigDecimal directVoidAmt) {
        this.directVoidAmt = directVoidAmt;
    }

    public Integer getInDirectVoidQty() {
        return inDirectVoidQty;
    }

    public void setInDirectVoidQty(Integer inDirectVoidQty) {
        this.inDirectVoidQty = inDirectVoidQty;
    }

    public BigDecimal getInDirectVoidAmt() {
        return inDirectVoidAmt;
    }

    public void setInDirectVoidAmt(BigDecimal inDirectVoidAmt) {
        this.inDirectVoidAmt = inDirectVoidAmt;
    }

    public boolean isXOut() {
        return isXOut;
    }

    public void setXOut(boolean XOut) {
        isXOut = XOut;
    }

    public Integer getStartTransactionId() {
        return startTransactionId;
    }

    public void setStartTransactionId(Integer startTransactionId) {
        this.startTransactionId = startTransactionId;
    }

    public String getStartTransactionDate() {
        return startTransactionDate;
    }

    public void setStartTransactionDate(String startTransactionDate) {
        this.startTransactionDate = startTransactionDate;
    }

    public Integer getPreviousZTransactionId() {
        return previousZTransactionId;
    }

    public void setPreviousZTransactionId(Integer previousZTransactionId) {
        this.previousZTransactionId = previousZTransactionId;
    }

    public String getPreviousZTransactionDate() {
        return previousZTransactionDate;
    }

    public void setPreviousZTransactionDate(String previousZTransactionDate) {
        this.previousZTransactionDate = previousZTransactionDate;
    }

    @JsonIgnore
    public TransactionModel getxOutTransaction() {
        return xOutTransaction;
    }

    public void setxOutTransaction(TransactionModel xOutTransaction) {
        this.xOutTransaction = xOutTransaction;
    }

    public ArrayList<CashierShiftEndTransCount> getTransCountList() {
        return transCountList;
    }

    public void setTransCountList(ArrayList<CashierShiftEndTransCount> transCountList) {
        this.transCountList = transCountList;
    }

    public Integer getOfflineCreditCardChargesQty()
    {
        return offlineCreditCardChargesQty;
    }

    public void setOfflineCreditCardChargesQty(Integer offlineCreditCardChargesQty)
    {
        this.offlineCreditCardChargesQty = offlineCreditCardChargesQty;
    }
}
