package com.mmhayes.common.transaction.models;

//API dependencies
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mmhayes.common.loyalty.collections.LoyaltyProgramCollection;
import com.mmhayes.common.loyalty.models.LoyaltyPointModel;

//other dependencies
import java.util.List;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2017-02-16 11:12:54 -0500 (Thu, 16 Feb 2017) $: Date of last commit
 $Rev: 3553 $: Revision of last commit
*/
public class TransactionResponseModel {
    private TransactionModel submittedTransaction;

    private LoyaltyProgramCollection loyaltyPointsEarned = new LoyaltyProgramCollection();

    //default constructor
    public TransactionResponseModel() {

    }

    //constructor that takes in a TransactionModel
    private TransactionResponseModel(TransactionModel transactionModel) throws Exception {
        setSubmittedTransaction(transactionModel);
        //setTerminalModel(terminalModel);

        //figure out best way to deal with employee (account)
        //setAvailableDiscountInfo();
    }

    //set the Discount Collection as Available discounts
    private void getAvailableQcDiscountInfo() throws Exception {
        //CommonAPI.checkIsNullOrEmptyObject(submittedTransaction.getTerminalId(), "Terminal Id cannot be found"); //Fetch discount records using Terminal ID, if not found, throw error
        //CommonAPI.checkIsNullOrEmptyObject(this.submittedTransaction.getAmount(), "Transaction Sub-Total cannot be found"); //Fetch discount records using transaction sub-total, if not found, throw error

        //get discounts
        //TODO - show the correct QC discounts

        //this.setQcDiscountsAvailable(QcDiscountCollection.getAvailableQcDiscounts(this.getSubmittedTransaction().getQcDiscountModels()));
    }

    //create a Transaction Response Model dependent on the Transaction that is passed in
    public static TransactionResponseModel createTransactionResponseModel(TransactionModel transactionModel) throws Exception {
        TransactionResponseModel transactionResponseModel = new TransactionResponseModel(transactionModel);
        return transactionResponseModel;
    }

    //Getters/Setters
    public TransactionModel getSubmittedTransaction() {
        return submittedTransaction;
    }

    //setter
    public void setSubmittedTransaction(TransactionModel transactionModel) {
        this.submittedTransaction = transactionModel;
    }


    //getter
    @JsonIgnore
    public List<LoyaltyPointModel> getLoyaltyPointsEarned() {
        return this.submittedTransaction.getLoyaltyPointsEarned();
    }
}