package com.mmhayes.common.transaction.models;

//mmhayes dependencies

import com.mmhayes.common.api.errorhandling.exceptions.*;
import com.mmhayes.common.login.LoginModel;
import com.mmhayes.common.terminal.models.TerminalModel;

//API dependencies
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;
import java.text.SimpleDateFormat;

//Other Dependencies
import org.apache.commons.lang3.StringUtils;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2018-01-12 10:37:41 -0500 (Fri, 12 Jan 2018) $: Date of last commit
 $Rev: 7701 $: Revision of last commit
*/
public class TransactionSummaryQueryParams {
    private Integer terminalId = null;
    private Integer employeeId = null;
    private String startDate = null;
    private String endDate = null;
    private Boolean openOnly = false;

    private LoginModel loginModel = null;
    private TerminalModel terminalModel = null;


    public TransactionSummaryQueryParams() {

    }

    public TransactionSummaryQueryParams(LoginModel loginModel, TerminalModel terminalModel) {
        this.setLoginModel(loginModel);
        this.setTerminalModel(terminalModel);
    }

    //populates the passed in paginationParamsHM with HTTP GET parameters in the uriInfo
    public TransactionSummaryQueryParams populateTransactionSummaryParamsFromURI(@Context UriInfo uriInfo) throws Exception {

        MultivaluedMap<String, String> httpGetParams = uriInfo.getQueryParameters();
        if (httpGetParams != null
                && !httpGetParams.isEmpty()) {

            //Start Date
            if (httpGetParams.get("startdate") != null
                    && httpGetParams.get("startdate").get(0) != null
                    && !httpGetParams.get("startdate").get(0).toString().isEmpty()) {

                if (httpGetParams.get("startdate").get(0).toString().length() != 8) {
                    throw new MissingDataException("Invalid Start Date Parameter");
                }

                if (!StringUtils.isNumeric(httpGetParams.get("startdate").get(0).toString())) {
                    throw new MissingDataException("Invalid Start Date Parameter");
                }

                this.setStartDate(httpGetParams.get("startdate").get(0).toString());

                String tempStartDate = this.getStartDate().substring(0, 2) + "/" + this.getStartDate().substring(2, 4) + "/" + this.getStartDate().substring(4, 8);
                this.setStartDate(tempStartDate);

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                try {
                    dateFormat.parse(this.getStartDate());
                } catch (Exception ex) {
                    throw new MissingDataException("Invalid Start Date Parameter");
                }
            }

            //End Date
            if (httpGetParams.get("enddate") != null
                    && httpGetParams.get("enddate").get(0) != null
                    && !httpGetParams.get("enddate").get(0).toString().isEmpty()) {

                if (httpGetParams.get("enddate").get(0).toString().length() != 8) {
                    throw new MissingDataException("Invalid End Date Parameter");
                }

                if (!StringUtils.isNumeric(httpGetParams.get("enddate").get(0).toString())) {
                    throw new MissingDataException("Invalid End Date Parameter");
                }

                this.setEndDate(httpGetParams.get("enddate").get(0).toString());

                String tempEndDate = this.getEndDate().substring(0, 2) + "/" + this.getEndDate().substring(2, 4) + "/" + this.getEndDate().substring(4, 8);
                this.setEndDate(tempEndDate);

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                try {
                    dateFormat.parse(this.getEndDate());
                } catch (Exception ex) {
                    throw new MissingDataException("Invalid End Date Parameter");
                }
            }

            //Terminal ID
            if (httpGetParams.get("terminalid") != null
                    && httpGetParams.get("terminalid").get(0) != null
                    && !httpGetParams.get("terminalid").get(0).toString().isEmpty()) {

                if (!StringUtils.isNumeric(httpGetParams.get("terminalid").get(0).toString())) {
                    throw new MissingDataException("Invalid Terminal ID Parameter");
                }
                this.setTerminalId(Integer.parseInt(httpGetParams.get("terminalid").get(0).toString()));
            }

            //Employee ID
            if (httpGetParams.get("employeeid") != null
                    && httpGetParams.get("employeeid").get(0) != null
                    && !httpGetParams.get("employeeid").get(0).toString().isEmpty()) {

                if (!StringUtils.isNumeric(httpGetParams.get("employeeid").get(0).toString())) {
                    throw new MissingDataException("Invalid Employee ID Parameter");
                }

                this.setEmployeeId(Integer.parseInt(httpGetParams.get("employeeid").get(0).toString()));
            }

            if (httpGetParams.get("openonly") != null
                    && httpGetParams.get("openonly").get(0) != null
                    && !httpGetParams.get("openonly").get(0).toString().isEmpty()
                    && httpGetParams.get("openonly").get(0).toString().equalsIgnoreCase("yes")) {

                this.setOpenOnly(true);
            }
            
            //Employee ID
            if (httpGetParams.get("employeeid") != null
                    && httpGetParams.get("employeeid").get(0) != null
                    && !httpGetParams.get("employeeid").get(0).toString().isEmpty()) {

                if (!StringUtils.isNumeric(httpGetParams.get("employeeid").get(0).toString())) {
                    throw new MissingDataException("Invalid Employee ID Parameter");
                }

                this.setEmployeeId(Integer.parseInt(httpGetParams.get("employeeid").get(0).toString()));
            }
        }

        this.validate();

        return this;
    }

    private void validate() throws Exception {
        //if the Employee ID is found on the DSK, automatically apply this Employee ID as a filter
        if (this.getLoginModel() != null
                && this.getLoginModel().getEmployeeId() != null && !this.getLoginModel().getEmployeeId().toString().isEmpty()) {

            //If the employeeid parameter was passed in
            //and the EmployeeID was on the DSKey, validate that they are the same.
            if (this.getEmployeeId() != null && !this.getEmployeeId().toString().isEmpty()) {
                if (!this.getEmployeeId().equals(this.getLoginModel().getEmployeeId())) {
                    throw new AccountNotFoundException("Authorized Account cannot search for this Employee ID.");
                }
            } else {
                //the EmployeeID was only found on the DSKey
                this.setEmployeeId(this.getLoginModel().getEmployeeId());
            }
        }
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Integer getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(Integer terminalId) {
        this.terminalId = terminalId;
    }

    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public LoginModel getLoginModel() {
        return loginModel;
    }

    public void setLoginModel(LoginModel loginModel) {
        this.loginModel = loginModel;
    }

    public TerminalModel getTerminalModel() {
        return terminalModel;
    }

    public void setTerminalModel(TerminalModel terminalModel) {
        this.terminalModel = terminalModel;
    }

    public Boolean isOpenOnly() {
        return openOnly;
    }

    public void setOpenOnly(Boolean openOnly) {
        this.openOnly = openOnly;
    }
}
