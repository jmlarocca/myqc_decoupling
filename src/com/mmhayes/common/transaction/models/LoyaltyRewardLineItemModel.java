package com.mmhayes.common.transaction.models;

//MMHayes dependencies

import com.mmhayes.common.account.models.AccountModel;
import com.mmhayes.common.loyalty.models.*;

//API dependencies
import com.fasterxml.jackson.annotation.*;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.product.models.ItemTaxModel;

//other dependencies
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.UUID;


/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2017-11-15 15:16:54 -0500 (Wed, 15 Nov 2017) $: Date of last commit
 $Rev: 5677 $: Revision of last commit
*/
public class LoyaltyRewardLineItemModel extends TransactionLineItemModel {
    private LoyaltyPointModel loyaltyPoint = null; //Only a single LoyaltyPoint can be attached to the Loyalty Reward Line Item
    private LoyaltyRewardModel reward = null;

    private ArrayList<ItemLoyaltyPointModel> loyaltyPointDetails = new ArrayList<>();
    private AccountModel account = null;

    private BigDecimal appliedValue = new BigDecimal(0.00);
    private boolean hasBeenApplied = false;
    private UUID rewardGuid = null;
    private Integer lineItemSequence = 0;


    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private ArrayList<ItemTaxModel> taxes = new ArrayList<>();

    public LoyaltyRewardLineItemModel() {

    }

    public LoyaltyRewardModel getReward() {
        return reward;
    }

    public void setReward(LoyaltyRewardModel reward) {
        this.reward = reward;
    }

    public
    @Override
    @JsonIgnore
        //not returning this for this transaction line item type.  This should be assumed I think.  Trying to clean up the json.
    Integer getItemTypeId() {
        if (itemTypeId != null) {
            return itemTypeId;
        } else {
            return PosAPIHelper.ObjectType.LOYALTYREWARD.toInt();
        }
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonIgnore
    public LoyaltyPointModel getLoyaltyPoint() {
        return loyaltyPoint;
    }

    public void setLoyaltyPoint(LoyaltyPointModel loyaltyPoint) {
        this.loyaltyPoint = loyaltyPoint;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonIgnore
    public AccountModel getAccount() {
        return account;
    }

    public void setAccount(AccountModel account) {
        this.account = account;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public ArrayList<ItemLoyaltyPointModel> getLoyaltyPointDetails() {
        return loyaltyPointDetails;
    }

    public void setLoyaltyPointDetails(ArrayList<ItemLoyaltyPointModel> loyaltyPointDetails) {
        this.loyaltyPointDetails = loyaltyPointDetails;
    }

    public boolean equalsRewardModel(Object objLoyaltyRewardModelLineItem) {
        if (this == objLoyaltyRewardModelLineItem) return true;
        if (!(objLoyaltyRewardModelLineItem instanceof LoyaltyRewardLineItemModel)) return false;

        LoyaltyRewardLineItemModel loyaltyRewardLineItemModel = (LoyaltyRewardLineItemModel) objLoyaltyRewardModelLineItem;

        //check the amount
        /*if ((loyaltyRewardLineItemModel.getAmount().compareTo(this.getAmount()) == 0)
                && (loyaltyRewardLineItemModel.getAmount().add(this.getAmount()).compareTo(BigDecimal.ZERO) == 0)) {
            return false;
        }*/

        //check the amount
       /* if (loyaltyRewardLineItemModel.getAmount().compareTo(this.getAmount()) != 0){
            return false;
        }

        //check the extended amount
        if (loyaltyRewardLineItemModel.getExtendedAmount().compareTo(this.getExtendedAmount()) != 0){
            return false;
        }


        if (!loyaltyRewardLineItemModel.getQuantity().equals(this.getQuantity())) {
            return false;
        }*/

       /* if ((loyaltyRewardLineItemModel.getExtendedAmount().compareTo(this.getExtendedAmount()) == 0)
                && (loyaltyRewardLineItemModel.getExtendedAmount().add(this.getExtendedAmount()).compareTo(BigDecimal.ZERO) == 0)) {
            return false;
        }*/

        if (!loyaltyRewardLineItemModel.getReward().getId().equals(this.getReward().getId())) {
            return false;
        }

        return true;
    }

    @Override
    public boolean equals(Object objLoyaltyRewardModelLineItem) {
        if (this == objLoyaltyRewardModelLineItem) return true;
        if (!(objLoyaltyRewardModelLineItem instanceof LoyaltyRewardLineItemModel)) return false;

        LoyaltyRewardLineItemModel loyaltyRewardLineItemModel = (LoyaltyRewardLineItemModel) objLoyaltyRewardModelLineItem;

        //check the amount
        /*if ((loyaltyRewardLineItemModel.getAmount().compareTo(this.getAmount()) == 0)
                && (loyaltyRewardLineItemModel.getAmount().add(this.getAmount()).compareTo(BigDecimal.ZERO) == 0)) {
            return false;
        }*/

        //check the amount
        if (loyaltyRewardLineItemModel.getAmount().compareTo(this.getAmount()) != 0) {
            return false;
        }

        //check the extended amount
        if (loyaltyRewardLineItemModel.getExtendedAmount().compareTo(this.getExtendedAmount()) != 0) {
            return false;
        }


        if (!loyaltyRewardLineItemModel.getQuantity().equals(this.getQuantity())) {
            return false;
        }

       /* if ((loyaltyRewardLineItemModel.getExtendedAmount().compareTo(this.getExtendedAmount()) == 0)
                && (loyaltyRewardLineItemModel.getExtendedAmount().add(this.getExtendedAmount()).compareTo(BigDecimal.ZERO) == 0)) {
            return false;
        }*/

        if (!loyaltyRewardLineItemModel.getReward().getId().equals(this.getReward().getId())) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = this.getAmount().hashCode();

        result = 31 * result + this.getQuantity().hashCode();
        result = 31 * result + this.getExtendedAmount().hashCode();
        result = 31 * result + this.getReward().getId().hashCode();
        return result;
    }

    @JsonIgnore
    public boolean hasBeenApplied() {
        return hasBeenApplied;
    }

    public void setHasBeenApplied(boolean hasBeenApplied) {
        this.hasBeenApplied = hasBeenApplied;
    }

    @JsonIgnore
    public BigDecimal getAppliedValue() {
        return appliedValue;
    }

    public void setAppliedValue(BigDecimal appliedValue) {
        this.appliedValue = appliedValue;
    }

    public ArrayList<ItemTaxModel> getTaxes() {
        return taxes;
    }

    public void setTaxes(ArrayList<ItemTaxModel> taxes) {
        this.taxes = taxes;
    }

    @JsonIgnore
    public UUID getRewardGuid() {
        return rewardGuid;
    }

    public void setRewardGuid(UUID rewardGuid) {
        this.rewardGuid = rewardGuid;
    }

    @JsonIgnore
    public Integer getLineItemSequence() {
        return lineItemSequence;
    }

    public void setLineItemSequence(Integer lineItemSequence) {
        this.lineItemSequence = lineItemSequence;
    }
}
