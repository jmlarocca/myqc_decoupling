package com.mmhayes.common.transaction.models;

//MMHayes dependencies
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.combo.models.ComboModel;

//API dependencies
import com.fasterxml.jackson.annotation.JsonInclude;

//other dependencies

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2020-04-24 13:41:58 -0400 (Fri, 24 Apr 2020) $: Date of last commit
 $Rev: 11468 $: Revision of last commit
*/
public class ComboLineItemModel extends TransactionLineItemModel {
    //extends the TransactionLineItemModel so we don't need id, amount or quantity fields

    private Integer tempId = null;
    private ComboModel combo = null;
    private boolean refundComplete = false;

    public ComboLineItemModel() {

    }

    public ComboModel getCombo() {
        return combo;
    }

    public void setCombo(ComboModel combo) {
        this.combo = combo;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getTempId() {
        return tempId;
    }

    public void setTempId(Integer tempId) {
        this.tempId = tempId;
    }

    @Override
    @JsonIgnore
        //not returning this for this transaction line item type.  This should be assumed I think.  Trying to clean up the json.
    public Integer getItemTypeId() {
        return PosAPIHelper.ObjectType.COMBO.toInt();
    }

    @JsonIgnore
    public boolean isRefundComplete() {
        return refundComplete;
    }

    public void setRefundComplete(boolean refundComplete) {
        this.refundComplete = refundComplete;
    }
}
