package com.mmhayes.common.transaction.models;

//MMHayes Dependencies
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.loyalty.models.ItemLoyaltyPointModel;
import com.mmhayes.common.loyalty.models.LoyaltyPointModel;
import com.mmhayes.common.product.models.ItemRewardModel;
import com.mmhayes.common.terminal.models.TerminalModel;

//Other Dependencies
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2021-08-30 14:16:40 -0400 (Mon, 30 Aug 2021) $: Date of last commit
 $Rev: 15226 $: Revision of last commit
*/
public class TransactionPartialRefund {
    private TransactionModel transactionModel = null;
    private TerminalModel terminalModel = null;

    //region Constructors

    public TransactionPartialRefund() {

    }

    public TransactionPartialRefund(TransactionModel transactionModel) {
        this.transactionModel = transactionModel;
        this.terminalModel = transactionModel.getTerminal();
    }

    //endregion

    //for a partial refund, go through the loyalty points and recalculate the values based on the partial product values.
    public void calculatePointsForPartialRefund() throws Exception {
        switch (this.getTransactionModel().getApiActionTypeEnum()) {
            case REFUND:
                if (this.getTransactionModel().getId() != null) {

                    //remove loyalty Points off the transaction.  Then recalculate what should be refunded depending on the product
                    transactionModel.removeLoyaltyPoints(); //remove any loyalty points on the incoming transaction model, the api will recalculate them

                    if (this.getTransactionModel().getOriginalTransaction() != null) {
                        /*
                        Populate the Points already refunded with SQL query
                        It proved too difficult to figure out how many points were refunded from the Item Loyalty Points
                        I had to fetch the refunded Loyalty Account Points from the DB - egl 12/19/2017
                         */
                        ArrayList<HashMap> loyaltyAccountPointSummaryNew = this.getAccountPointSummaryForPartialRefund(this.getTransactionModel().getOriginalTransaction().getId());
                        this.markLoyaltyAccountPointsAsRefunded(loyaltyAccountPointSummaryNew);

                        this.calculateItemLoyaltyRecords(); //ItemRewardModels and ItemLoyaltyPoints
                        this.replaceItemLoyaltyPoints();

                        //Now calculate the Loyalty Account Points
                        this.calculateLoyaltyAccountPointsForPartialRefund(); //Figure out the Loyalty Account Points
                    }
                }
                break;
            default:
                break;
        }
    }

    //region Loyalty Partial Refund Methods

    private void markLoyaltyAccountPointsAsRefunded(ArrayList<HashMap> loyaltyAccountPointSummary) {

        if (!loyaltyAccountPointSummary.isEmpty()) {
            //keep track of the Points Already Refunded
            if (this.getTransactionModel().getOriginalTransaction() != null) {
                //Match up the Loyalty Reward Line Item ID, then set the Reward ID on the Loyalty Point
                if (this.getTransactionModel().getOriginalTransaction().getRewards() != null
                        && !this.getTransactionModel().getOriginalTransaction().getRewards().isEmpty()) {
                    for (LoyaltyRewardLineItemModel loyaltyRewardLineItemModel : this.getTransactionModel().getOriginalTransaction().getRewards()) {
                        for (LoyaltyPointModel loyaltyPointModel : this.getTransactionModel().getOriginalTransaction().getLoyaltyPointsAll()) {
                            if (loyaltyPointModel.getLoyaltyRewardTransLineId() != null
                                    && !loyaltyPointModel.getLoyaltyRewardTransLineId().toString().isEmpty()
                                    && loyaltyPointModel.getLoyaltyRewardTransLineId().equals(loyaltyRewardLineItemModel.getId())) {

                                loyaltyPointModel.setLoyaltyRewardId(loyaltyRewardLineItemModel.getItemId());
                            }
                        }
                    }
                }

                //Loop through all the points already refunded
                for (HashMap pointsSummaryHM : loyaltyAccountPointSummary) {
                    BigDecimal sumPointsAlreadyRefunded = CommonAPI.convertModelDetailToBigDecimal(pointsSummaryHM.get("SUMPOINTS"));
                    Integer loyaltyProgramId = CommonAPI.convertModelDetailToInteger(pointsSummaryHM.get("LOYALTYPROGRAMID"));
                    Integer loyaltyRewardId = CommonAPI.convertModelDetailToInteger(pointsSummaryHM.get("LOYALTYREWARDID"));
                    Integer loyaltyAccrualPolicyId = CommonAPI.convertModelDetailToInteger(pointsSummaryHM.get("LOYALTYACCRUALPOLICYID"));

                    if (sumPointsAlreadyRefunded != null) {
                        sumPointsAlreadyRefunded = sumPointsAlreadyRefunded.negate();
                    }

                    for (LoyaltyPointModel loyaltyPointModel : this.getTransactionModel().getOriginalTransaction().getLoyaltyPointsAll()) {
                        if (pointsSummaryHM.containsKey("alreadyRefunded")) {
                            continue;
                        }

                        if (loyaltyPointModel.areAllPointsAlreadyRefunded()) {
                            continue;
                        }

                        //redeemed summary, earned Loyalty Point
                        if (loyaltyPointModel.getLoyaltyRewardId() == null && loyaltyRewardId != null) {
                            continue;
                        }

                        //redeemed summary, not matching redeemed Loyalty Point
                        if (loyaltyPointModel.getLoyaltyRewardId() != null && !loyaltyPointModel.getLoyaltyRewardId().equals(loyaltyRewardId)) {
                            continue;
                        }

                        if (!loyaltyPointModel.getLoyaltyProgram().getId().equals(loyaltyProgramId)) {
                            continue;
                        }

                        //only check Loyalty Accrual Policy for earned points
                        if (loyaltyRewardId == null && loyaltyPointModel.getLoyaltyAccrualPolicy() != null) {
                            if (loyaltyPointModel.getLoyaltyAccrualPolicy().getId() == null || !loyaltyPointModel.getLoyaltyAccrualPolicy().getId().equals(loyaltyAccrualPolicyId)) {
                                continue;
                            }
                        }

                        if (loyaltyRewardId != null) { //Reward
                            if (sumPointsAlreadyRefunded.compareTo(new BigDecimal(loyaltyPointModel.getPoints())) <= 0) {
                                loyaltyPointModel.setAllPointsAlreadyRefunded(true);
                                pointsSummaryHM.put("alreadyRefunded", true);
                            } else {
                                loyaltyPointModel.setPointsAlreadyRefunded(sumPointsAlreadyRefunded.intValue());
                            }
                        } else { //Earned Points
                            if (sumPointsAlreadyRefunded.compareTo(new BigDecimal(loyaltyPointModel.getPoints())) >= 0) {
                                loyaltyPointModel.setAllPointsAlreadyRefunded(true);
                                pointsSummaryHM.put("alreadyRefunded", true);
                            } else {
                                loyaltyPointModel.setPointsAlreadyRefunded(sumPointsAlreadyRefunded.intValue());
                            }
                        }
                    }
                }
            }
        }
    }

    //region LoyaltyAccountPoints

    /**
     * Calculate the amount of Loyalty Accounts we need to reverse off
     * We have to look at the Earned Loyalty Points on the original transaction,
     * And look at the refunded Item Loyalty Points on the current transaction
     * On the first refund, round the points up to deal with decimal amounts (ie- .5 on the Item Loyalty Point which is a BigDecimal, the Loyalty Account Point is an Integer value)
     * On any subsequent refunds, round normally
     */
    private void calculateLoyaltyAccountPointsForPartialRefund() {
        //decrement the Loyalty Account Points with the new totals
        for (LoyaltyPointModel loyaltyPointModelOriginal : this.getTransactionModel().getOriginalTransaction().getLoyaltyPointsAll()) {

            //Separate out the redeemed and earned by the points value
            if (loyaltyPointModelOriginal.getPoints().compareTo(0) == -1) {
                this.calculateRedeemedLoyaltyAccountPoint(loyaltyPointModelOriginal); //redeemed Loyalty Account Points
            } else {
                this.calculateEarnedLoyaltyAccountPoint(loyaltyPointModelOriginal); //Earned Loyalty Account Points

            } //end earned loyalty points
        } //end Original Loyalty Point Loop
    }

    /**
     * Calculate the Loyalty Account points for any redeemed points
     *
     * @param loyaltyPointModelOriginal
     */
    private void calculateRedeemedLoyaltyAccountPoint(LoyaltyPointModel loyaltyPointModelOriginal) {
        if (loyaltyPointModelOriginal.areAllPointsAlreadyRefunded()) {
            this.removeUpdatedItemLoyaltyPoints(loyaltyPointModelOriginal);

            return; //If the Loyalty Point Model has already been refunded, don't add anything
        }

        //don't calculate points for rewards that weren't submitted
        for (LoyaltyRewardLineItemModel loyaltyRewardLineItemModel : this.getTransactionModel().getRewards()) {

            if (!loyaltyRewardLineItemModel.getId().equals(loyaltyPointModelOriginal.getLoyaltyRewardTransLineId())) {
                return;
            }
        }

        boolean someWasRefunded = false;
        BigDecimal amountToRefunded = BigDecimal.ZERO;

        for (ProductLineItemModel productLineItemModel : this.getTransactionModel().getProducts()) {
            for (ItemLoyaltyPointModel itemLoyaltyPoint : productLineItemModel.getLoyaltyPointDetails()) {
                if (itemLoyaltyPoint.getLoyaltyAccountPointId().equals(loyaltyPointModelOriginal.getId()) && itemLoyaltyPoint.getPoints().compareTo(BigDecimal.ZERO) == -1) {
                    amountToRefunded = amountToRefunded.add(itemLoyaltyPoint.getPoints());
                    someWasRefunded = true;
                }
            }
        }

        //If some has been refunded in the current transaction, and this is the first refund for the Original, update the Loyalty Account Points
        if (someWasRefunded && isFirstRefundForOriginalTransaction()) {

            //set the point total to the number of points refunded from the ItemLoyaltyPoint records
            amountToRefunded = amountToRefunded.setScale(0, RoundingMode.UP);
            loyaltyPointModelOriginal.setPoints(amountToRefunded.intValue());

            //if pointsAlreadyRefunded + amountToRefund == the original amount of points
            if ((loyaltyPointModelOriginal.getPointsAlreadyRefunded() + loyaltyPointModelOriginal.getPoints()) == loyaltyPointModelOriginal.getOriginalPoints()) {
                //clear out the ItemLoyaltyPoints
                loyaltyPointModelOriginal.setAllPointsAlreadyRefunded(true);

                for (ItemLoyaltyPointModel itemLoyaltyPoint : loyaltyPointModelOriginal.getPointDetails()) {
                    itemLoyaltyPoint.setPoints(itemLoyaltyPoint.getOriginalPoints().subtract(itemLoyaltyPoint.getRefundedPoints()));
                    itemLoyaltyPoint.setEligibleAmount(itemLoyaltyPoint.getOriginalEligibleAmount().subtract(itemLoyaltyPoint.getRefundedEligibleAmount()));
                }
            }

            this.getTransactionModel().getLoyaltyPointsAll().add(loyaltyPointModelOriginal);
        } else if (someWasRefunded) {

            Integer pointsAlreadyRefundedNegative = loyaltyPointModelOriginal.getPointsAlreadyRefunded();

            amountToRefunded = amountToRefunded.setScale(0, RoundingMode.UP);
            Integer iAmountToRefund = amountToRefunded.intValue();

            //If original points will be greater, then we are refunding too many points
            Integer tempRefundedTotal = pointsAlreadyRefundedNegative + iAmountToRefund; //add what's already refunded to the current total of Item Loyalty Points
            if (loyaltyPointModelOriginal.getPoints().compareTo(tempRefundedTotal) == 1) {
                //If the refunded amount will be more than the original.  Override this refund so the refunded points equal the earned points

                //egl 03-12-2018: API was refunding large amounts
                iAmountToRefund = loyaltyPointModelOriginal.getPoints() - pointsAlreadyRefundedNegative;
                //iAmountToRefund = loyaltyPointModelOriginal.getPoints() + pointsAlreadyRefundedNegative;
            }

            loyaltyPointModelOriginal.setPoints(iAmountToRefund);

            //if pointsAlreadyRefunded + amountToRefund == the original amount of points
            if ((loyaltyPointModelOriginal.getPointsAlreadyRefunded() + loyaltyPointModelOriginal.getPoints()) == loyaltyPointModelOriginal.getOriginalPoints()) {

                loyaltyPointModelOriginal.setAllPointsAlreadyRefunded(true);

                //clear out the ItemLoyaltyPoints
                for (ItemLoyaltyPointModel itemLoyaltyPoint : loyaltyPointModelOriginal.getPointDetails()) {
                    itemLoyaltyPoint.setPoints(itemLoyaltyPoint.getOriginalPoints().subtract(itemLoyaltyPoint.getRefundedPoints()));
                    itemLoyaltyPoint.setEligibleAmount(itemLoyaltyPoint.getOriginalEligibleAmount().subtract(itemLoyaltyPoint.getRefundedEligibleAmount()));
                }
            }

            this.getTransactionModel().getLoyaltyPointsAll().add(loyaltyPointModelOriginal);
        }
    }

    /**
     * Calculate the Loyalty Account points for any earned points
     *
     * @param loyaltyPointModelOriginal
     */
    private void calculateEarnedLoyaltyAccountPoint(LoyaltyPointModel loyaltyPointModelOriginal) {

        if (loyaltyPointModelOriginal.areAllPointsAlreadyRefunded()) {

            this.removeUpdatedItemLoyaltyPoints(loyaltyPointModelOriginal);

            return; //If the Loyalty Point Model has already been refunded, don't add anything
        }

        //earned Loyalty Account Points
        boolean someWasRefunded = false;
        BigDecimal amountToRefunded = BigDecimal.ZERO;

        //try and figure out if any was refunded
        for (ProductLineItemModel productLineItemModel : this.getTransactionModel().getProducts()) {
            for (ItemLoyaltyPointModel itemLoyaltyPoint : productLineItemModel.getLoyaltyPointDetails()) {
                if (itemLoyaltyPoint.getLoyaltyAccountPointId().equals(loyaltyPointModelOriginal.getId()) && itemLoyaltyPoint.getPoints().compareTo(BigDecimal.ZERO) == 1) {
                    amountToRefunded = amountToRefunded.add(itemLoyaltyPoint.getPoints());
                    someWasRefunded = true;
                    continue;
                }
            }
        }

        //If some has been refunded in the current transaction, and this is the first refund for the Original, update the Loyalty Account Points
        if (someWasRefunded && isFirstRefundForOriginalTransaction()) {
            //set the point total to the number of points refunded from the ItemLoyaltyPoint records
            amountToRefunded = amountToRefunded.setScale(0, RoundingMode.UP); //HP 2497: for point totals that are between 0 and .5, round up
            loyaltyPointModelOriginal.setPoints(amountToRefunded.intValue());

            //if pointsAlreadyRefunded + amountToRefund == the original amount of points
            if (((loyaltyPointModelOriginal.getPointsAlreadyRefunded() * -1) + loyaltyPointModelOriginal.getPoints()) == loyaltyPointModelOriginal.getOriginalPoints()) {
                //clear out the ItemLoyaltyPoints

                loyaltyPointModelOriginal.setAllPointsAlreadyRefunded(true);

                for (ItemLoyaltyPointModel itemLoyaltyPoint : loyaltyPointModelOriginal.getPointDetails()) {
                    itemLoyaltyPoint.setPoints(itemLoyaltyPoint.getOriginalPoints().subtract(itemLoyaltyPoint.getRefundedPoints()));
                    itemLoyaltyPoint.setEligibleAmount(itemLoyaltyPoint.getOriginalEligibleAmount().subtract(itemLoyaltyPoint.getRefundedEligibleAmount()));
                }
            }

            this.getTransactionModel().getLoyaltyPointsAll().add(loyaltyPointModelOriginal);
        } else if (someWasRefunded) {
            Integer pointsAlreadyRefundedPositive = loyaltyPointModelOriginal.getPointsAlreadyRefunded();
            if (loyaltyPointModelOriginal.getPointsAlreadyRefunded().compareTo(0) == -1){
                pointsAlreadyRefundedPositive = loyaltyPointModelOriginal.getPointsAlreadyRefunded() * -1;
            }

            amountToRefunded = amountToRefunded.setScale(0, RoundingMode.UP);
            Integer iAmountToRefund = amountToRefunded.intValue();

            Integer tempRefundedTotal = pointsAlreadyRefundedPositive + iAmountToRefund; //add what's already refunded to the current total of Item Loyalty Points
            if (loyaltyPointModelOriginal.getPoints().compareTo(tempRefundedTotal) == -1) {
                //If the refunded amount will be more than the original.  Override this refund so the refunded points equal the earned points
                iAmountToRefund = loyaltyPointModelOriginal.getPoints() - pointsAlreadyRefundedPositive;
            }

            loyaltyPointModelOriginal.setPoints(iAmountToRefund);

            //if pointsAlreadyRefunded + amountToRefund == the original amount of points
            if (((loyaltyPointModelOriginal.getPointsAlreadyRefunded() * -1) + loyaltyPointModelOriginal.getPoints()) == loyaltyPointModelOriginal.getOriginalPoints()) {

                loyaltyPointModelOriginal.setAllPointsAlreadyRefunded(true);

                //clear out the ItemLoyaltyPoints
                for (ItemLoyaltyPointModel itemLoyaltyPoint : loyaltyPointModelOriginal.getPointDetails()) {
                    itemLoyaltyPoint.setPoints(itemLoyaltyPoint.getOriginalPoints().subtract(itemLoyaltyPoint.getRefundedPoints()));
                    itemLoyaltyPoint.setEligibleAmount(itemLoyaltyPoint.getOriginalEligibleAmount().subtract(itemLoyaltyPoint.getRefundedEligibleAmount()));
                }
            }

            this.getTransactionModel().getLoyaltyPointsAll().add(loyaltyPointModelOriginal);
        }
    }

    /**
     * Get all refunded Loyalty Account Points for a Transaction Id
     * Group By Loyalty Program, Loyalty Reward Id, and if the line is a reward of type "Free Product"
     * This way, Reward points can be properly refunded for both Free Product rewards and Transaction Credit Rewards.
     * Transaction Credit Rewards can span multiple products
     *
     * @param originalTransID
     * @return
     */
    private ArrayList<HashMap> getAccountPointSummaryForPartialRefund(Integer originalTransID) {
        DataManager dm = new DataManager();

        //send in the original transaction ID
        ArrayList<HashMap> loyaltyPointHM = dm.parameterizedExecuteQuery("data.posapi30.getAccountPointSummaryForPartialRefund",
                new Object[]{originalTransID},
                PosAPIHelper.getLogFileName(terminalModel.getId()),
                true
        );

        return loyaltyPointHM;
    }

    //endregion

    //region ItemLoyaltyPoints
    private void calculateItemLoyaltyRecords() {
        //calculate and write the Item Loyalty Points
        for (ProductLineItemModel productLineOriginal : this.getTransactionModel().getOriginalTransaction().getProducts()) {
            for (ProductLineItemModel productLineRefund : this.getTransactionModel().getProducts()) {
                if (productLineOriginal.getId().equals(productLineRefund.getId())) {
                    //go through each product line item on a partial refund, and recalculate the ItemRewardModels and ItemLoyaltyPoints
                    productLineRefund = this.calculatePartialRewardAmount(productLineOriginal, productLineRefund);
                    continue;
                }
            }
        }
    }

    public ProductLineItemModel calculatePartialRewardAmount(ProductLineItemModel productLineItemOriginal, ProductLineItemModel productLineItemRefund) {
        productLineItemRefund.getLoyaltyPointDetails().clear();

        //stamp the original refundedPoints and refundedEligibleAmount
        for (ItemLoyaltyPointModel itemLoyaltyPointModel : productLineItemOriginal.getLoyaltyPointDetails()) {
            itemLoyaltyPointModel.setOriginalEligibleAmount(itemLoyaltyPointModel.getEligibleAmount());
            itemLoyaltyPointModel.setOriginalPoints(itemLoyaltyPointModel.getPoints());
        }

        if (productLineItemOriginal.getQuantity().compareTo(BigDecimal.ONE) == 1) { //the original has a quantity > 1
            BigDecimal percentage = getPartialProductPercentage(productLineItemOriginal, productLineItemRefund);

            productLineItemRefund = createProductLineWithQuantity(productLineItemOriginal, productLineItemRefund, percentage);

        } else { //the original product line had a quantity of 1
            BigDecimal percentage = getPartialProductPercentage(productLineItemOriginal, productLineItemRefund);

            if (percentage.compareTo(BigDecimal.ZERO) == 0) {
                percentage = new BigDecimal(1);
            }

            for (ItemLoyaltyPointModel itemLoyaltyPoint : productLineItemOriginal.getLoyaltyPointDetails()) {
                //do the calculation
                itemLoyaltyPoint.setEligibleAmount(itemLoyaltyPoint.getEligibleAmount().multiply(percentage));
                itemLoyaltyPoint.setPoints(itemLoyaltyPoint.getPoints().multiply(percentage));

                //add a new ItemLoyaltyPoint record
                productLineItemRefund.getLoyaltyPointDetails().add(itemLoyaltyPoint);
            }

            for (ItemRewardModel itemRewardOriginal : productLineItemOriginal.getRewards()) {
                //if ItemRewardModel is already on the newly refunded product, don't add it again
                Boolean itemRewardOrginalAlreadyAdded = false;
                for (ItemRewardModel itemRewardModelRefund : productLineItemRefund.getRewards()) {
                    if (itemRewardModelRefund.getId() != null && itemRewardModelRefund.getId().equals(itemRewardModelRefund.getId())) {
                        itemRewardOrginalAlreadyAdded = true;
                    }
                }

                if (!itemRewardOrginalAlreadyAdded) {
                    //do the calculation
                    itemRewardOriginal.setEligibleAmount(itemRewardOriginal.getEligibleAmount().multiply(percentage));
                    itemRewardOriginal.setAmount(itemRewardOriginal.getAmount().multiply(percentage));
                    productLineItemRefund.getRewards().add(itemRewardOriginal);
                }
            }
        }

        return productLineItemRefund;
    }

    //endregion

    /**
     * Look to see if any ItemLoyaltyPoint records have been previously refunded
     *
     * @return
     */
    private boolean isFirstRefundForOriginalTransaction() {

        boolean originalProductHasAlreadyBeenRefunded = true;

        for (ProductLineItemModel productLineItemModel : this.getTransactionModel().getOriginalTransaction().getProducts()) {
            for (ItemLoyaltyPointModel itemLoyaltyPointModel : productLineItemModel.getLoyaltyPointDetails()) {

                if (itemLoyaltyPointModel.getRefundedPoints().compareTo(BigDecimal.ZERO) != 0) {
                    originalProductHasAlreadyBeenRefunded = false;
                }
            }
        }

        return originalProductHasAlreadyBeenRefunded;
    }

    private void removeUpdatedItemLoyaltyPoints(LoyaltyPointModel loyaltyPointModelOriginal) {

        //If all points have been refunded, find the Item Loyalty Points and clear them out
        for (ProductLineItemModel productLineItemModel : this.getTransactionModel().getProducts()) {
            //remove the submitted reward from the Loyalty Account - available rewards for transaction list
            Iterator<ItemLoyaltyPointModel> removeItemLoyaltyPointIterator = productLineItemModel.getLoyaltyPointDetails().iterator();
            while (removeItemLoyaltyPointIterator.hasNext()) {
                ItemLoyaltyPointModel curItemLoyaltyPoint = removeItemLoyaltyPointIterator.next();
                if (loyaltyPointModelOriginal.getId().equals(curItemLoyaltyPoint.getLoyaltyAccountPointId())) {
                    removeItemLoyaltyPointIterator.remove();
                }
            }
        }
    }

    //The ItemLoyaltyPoints on the transactionModel.productionLineItemModel has the correct amounts
    //Move those records to the Loyalty Point Model to be saved
    private void replaceItemLoyaltyPoints() {

        for (LoyaltyPointModel loyaltyPointModelOriginal : this.getTransactionModel().getOriginalTransaction().getLoyaltyPointsAll()) {

            Iterator<ItemLoyaltyPointModel> removeItemLoyaltyPointIterator = loyaltyPointModelOriginal.getPointDetails().iterator();
            while (removeItemLoyaltyPointIterator.hasNext()) {
                ItemLoyaltyPointModel curItemLoyaltyPoint = removeItemLoyaltyPointIterator.next();
                removeItemLoyaltyPointIterator.remove();
            }

            for (ProductLineItemModel productLineItemModel : this.getTransactionModel().getProducts()) {
                for (ItemLoyaltyPointModel itemLoyaltyPointModel : productLineItemModel.getLoyaltyPointDetails()) {

                    if (itemLoyaltyPointModel.getLoyaltyAccountPointId().equals(loyaltyPointModelOriginal.getId())) {
                        loyaltyPointModelOriginal.getPointDetails().add(itemLoyaltyPointModel);
                    }
                }
            }
        }
    }

    //endregion

    //region Combos Partial Refund Methods

    /**
     * Check if the Combo Line will be fully refunded after this partial refund
     * Mark the Combo line as Completely Refunded
     */
    public void validateComboLineItemAmounts() {
        for (ComboLineItemModel comboLineItemModel : this.getTransactionModel().getCombos()) {
            for (ComboLineItemModel comboLineItemModelOrig : this.getTransactionModel().getOriginalTransaction().getCombos()) {
                if (!comboLineItemModel.getId().equals(comboLineItemModelOrig.getId())) {
                    continue;
                }

                BigDecimal newRefundTotal = comboLineItemModelOrig.getRefundedExtAmount().add(comboLineItemModel.getExtendedAmount());
                if (newRefundTotal.compareTo(comboLineItemModelOrig.getExtendedAmount()) >= 0){
                    //if the new total is greater than or equal to
                    comboLineItemModel.setRefundComplete(true);
                }
            }
        }
    }

    //endregion

    /**
     * Used when determining partial refund amounts for product lines with a quantity > 1
     * @param productLineItemOriginal
     * @param productLineItemRefund
     * @return
     */
    private BigDecimal getPartialProductPercentage(ProductLineItemModel productLineItemOriginal, ProductLineItemModel productLineItemRefund) {
        //This is the percentage of the net product line
        //Transaction Credit rewards are spread across the quantities
        //
        //Example:
        // Original Product line
        //  Product 1, QTY. 2, Extended Amt. $6, Reward $3
        //
        // Partially Refunded Product line
        //  Product 1, QTY. 1, Extended Amt. $3, Reward $1.50

        BigDecimal originalExtendedCost = productLineItemOriginal.getExtendedAmount();
        BigDecimal refundExtendedCost = productLineItemRefund.getExtendedAmount();

        BigDecimal oneHundred = new BigDecimal(100);

        //figure out the percentage
        BigDecimal step1 = refundExtendedCost.multiply(new BigDecimal(100));
        BigDecimal step2 = BigDecimal.ZERO;

        //don't divide by zero
        if (originalExtendedCost.compareTo(BigDecimal.ZERO) != 0) {
            step2 = step1.divide(originalExtendedCost, 10, RoundingMode.HALF_UP);
        }
        BigDecimal percentage = step2.divide(oneHundred); //percentage of the refunded extended amount compared to the original extended amount

        return percentage;
    }

    private ProductLineItemModel createProductLineWithQuantity(ProductLineItemModel productLineItemOriginal, ProductLineItemModel productLineItemRefund, BigDecimal percentage) {
        BigDecimal freeProductRewardCount = BigDecimal.ZERO;
        BigDecimal transactionCreditRewardCount = BigDecimal.ZERO;

        //First get a count of all the rewards that are being refunded
        for (ItemRewardModel itemRewardModelRefund : productLineItemRefund.getRewards()) {
            if (itemRewardModelRefund.getReward() != null) {
                PosAPIHelper.LoyaltyRewardType rewardTypeEnum = PosAPIHelper.LoyaltyRewardType.convertIntToEnum(itemRewardModelRefund.getReward().getRewardTypeId());

                switch (rewardTypeEnum) {
                    case FREE_PRODUCTS:
                        if (itemRewardModelRefund.getRefundedAmount() == null || itemRewardModelRefund.getAmount().compareTo(itemRewardModelRefund.getRefundedAmount()) != 0) {
                            freeProductRewardCount = freeProductRewardCount.add(BigDecimal.ONE); //only count the rewards that haven't been refunded yet
                        }
                        break;
                    case TRANSACTION_CREDIT:
                        if (itemRewardModelRefund.getRefundedAmount() == null || itemRewardModelRefund.getAmount().compareTo(itemRewardModelRefund.getRefundedAmount()) != 0) {
                            transactionCreditRewardCount = transactionCreditRewardCount.add(BigDecimal.ONE); //only count the rewards that haven't been refunded yet
                        }
                        break;
                }
            }
        }

        //create Item Reward Models
        productLineItemRefund = createItemRewardModelForQuantity(productLineItemOriginal, productLineItemRefund, percentage, freeProductRewardCount);

        //create Item Loyalty Points
        productLineItemRefund = createItemLoyaltyPointsForQuantity(productLineItemOriginal, productLineItemRefund, percentage, freeProductRewardCount);

        return productLineItemRefund;
    }

    private ProductLineItemModel createItemRewardModelForQuantity(ProductLineItemModel productLineItemOriginal, ProductLineItemModel productLineItemRefund, BigDecimal percentage, BigDecimal freeProductRewardCount) {

        //only refund Item Reward Models if they are sent in on the refunded product line
        if (productLineItemRefund.getRewards() != null && !productLineItemRefund.getRewards().isEmpty()) {
            //Now go through the original ItemRewardModels
            for (ItemRewardModel itemRewardOriginal : productLineItemOriginal.getRewards()) {
                //if ItemRewardModel is already on the newly refunded product, don't add it again
                Boolean itemRewardLineOriginalAddedToNewLine = false;
                for (ItemRewardModel itemRewardModelRefund : productLineItemRefund.getRewards()) {
                    //match the refund line on the original ItemRewardModel
                    if (itemRewardModelRefund.getId() != null
                            && itemRewardModelRefund.getId().equals(itemRewardOriginal.getId())) {
                        itemRewardLineOriginalAddedToNewLine = true;
                        if (itemRewardModelRefund.getReward() != null) {
                            PosAPIHelper.LoyaltyRewardType rewardTypeEnum = PosAPIHelper.LoyaltyRewardType.convertIntToEnum(itemRewardModelRefund.getReward().getRewardTypeId());

                            for (ItemLoyaltyPointModel itemLoyaltyPointOriginal : productLineItemOriginal.getLoyaltyPointDetails()) {
                                if (itemLoyaltyPointOriginal.getPoints().compareTo(BigDecimal.ZERO) == -1) { //the Item Loyalty Point is a Reward
                                    //mark the Item Loyalty Point record with the type of Loyalty Reward so it can be used below
                                    if (itemLoyaltyPointOriginal.getTransLineItemId().equals(itemRewardOriginal.getTransLineItemId())) {
                                        switch (rewardTypeEnum) {
                                            case FREE_PRODUCTS:
                                                itemLoyaltyPointOriginal.setLoyaltyRewardType(PosAPIHelper.LoyaltyRewardType.FREE_PRODUCTS);
                                                break;
                                            case TRANSACTION_CREDIT:
                                                itemLoyaltyPointOriginal.setLoyaltyRewardType(PosAPIHelper.LoyaltyRewardType.TRANSACTION_CREDIT);
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                } // end partial refund loop

                //Add the ItemRewardModels on to the refunded Product line
                if (!itemRewardLineOriginalAddedToNewLine) {
                    PosAPIHelper.LoyaltyRewardType rewardTypeEnum = PosAPIHelper.LoyaltyRewardType.convertIntToEnum(itemRewardOriginal.getReward().getRewardTypeId());
                    switch (rewardTypeEnum) {
                        case FREE_PRODUCTS:
                            if (productLineItemRefund.getQuantityForLoyalty().compareTo(freeProductRewardCount) == 0) {
                                //don't do any calculations on the ItemRewardModel
                            } else {

                                //do the calculation only if need be
                                itemRewardOriginal.setEligibleAmount(itemRewardOriginal.getEligibleAmount().multiply(percentage));
                                itemRewardOriginal.setAmount(itemRewardOriginal.getAmount().multiply(percentage));
                            }
                            break;
                        case TRANSACTION_CREDIT:
                            //do the calculation only if need be
                            itemRewardOriginal.setEligibleAmount(itemRewardOriginal.getEligibleAmount().multiply(percentage));
                            itemRewardOriginal.setAmount(itemRewardOriginal.getAmount().multiply(percentage));

                            break;
                    }
                }
            } //end original original rewards loop
        }

        return productLineItemRefund;
    }

    private ProductLineItemModel createItemLoyaltyPointsForQuantity(ProductLineItemModel productLineItemOriginal, ProductLineItemModel productLineItemRefund, BigDecimal percentage, BigDecimal freeProductRewardCount) {

        //CREATE NEW ITEM LOYALTY POINTS
        BigDecimal itemLoyaltyRewardFreeProductCount = BigDecimal.ZERO;

        for (ItemLoyaltyPointModel itemLoyaltyPointOriginal : productLineItemOriginal.getLoyaltyPointDetails()) {
            if (itemLoyaltyPointOriginal.getPoints().compareTo(BigDecimal.ZERO) == -1) { //if the Item Loyalty Point record is negative, assume it's the reward point

                if (itemLoyaltyPointOriginal.getPoints().compareTo(itemLoyaltyPointOriginal.getRefundedPoints()) == 0
                        && itemLoyaltyPointOriginal.getEligibleAmount().compareTo(itemLoyaltyPointOriginal.getRefundedEligibleAmount()) == 0) {
                    //do nothing, earned points equals the refunded points
                    continue;
                }

                switch (itemLoyaltyPointOriginal.getLoyaltyRewardType()) {
                    case FREE_PRODUCTS:
                        if (freeProductRewardCount.compareTo(itemLoyaltyRewardFreeProductCount) == 0) {
                            //the number of free products equals the
                            continue; //HP Bug 1074 - This was stopping the ItemLoyaltyPoints from getting created
                        }

                        itemLoyaltyRewardFreeProductCount = itemLoyaltyRewardFreeProductCount.add(BigDecimal.ONE);
                        break;
                    case TRANSACTION_CREDIT:
                        //Eligible Amount
                        BigDecimal elligibleAmtTimesPercentage = itemLoyaltyPointOriginal.getEligibleAmount().multiply(percentage);
                        BigDecimal newRefundedEligibleAmt = itemLoyaltyPointOriginal.getRefundedEligibleAmount().add(elligibleAmtTimesPercentage);

                        BigDecimal eligibleAmountDiff = itemLoyaltyPointOriginal.getEligibleAmount().abs().subtract(newRefundedEligibleAmt.abs());
                        //if the difference is less than .2, do the math and add the difference to the Eligible Amount
                        if (eligibleAmountDiff.compareTo(new BigDecimal(.01)) == -1) {
                            itemLoyaltyPointOriginal.setEligibleAmount(itemLoyaltyPointOriginal.getEligibleAmount().multiply(percentage).add(eligibleAmountDiff));
                        } else {
                            itemLoyaltyPointOriginal.setEligibleAmount(itemLoyaltyPointOriginal.getEligibleAmount().multiply(percentage));
                        }

                        //Points
                        BigDecimal pointsTimesPercentage = itemLoyaltyPointOriginal.getPoints().multiply(percentage);
                        BigDecimal newPointsAmt = itemLoyaltyPointOriginal.getRefundedPoints().add(pointsTimesPercentage);

                        BigDecimal amountDiff = itemLoyaltyPointOriginal.getPoints().abs().subtract(newPointsAmt.abs());
                        //if the difference is less than .2, do the math and add the difference to the Eligible Amount
                        if (amountDiff.compareTo(new BigDecimal(.01)) == -1) {
                            //itemLoyaltyPoint.setRefundedPoints(itemLoyaltyPoint.getPoints());
                            itemLoyaltyPointOriginal.setPoints(itemLoyaltyPointOriginal.getPoints().multiply(percentage).subtract(amountDiff));
                        } else {
                            itemLoyaltyPointOriginal.setPoints(itemLoyaltyPointOriginal.getPoints().multiply(percentage));
                        }

                        break;
                }
            } else { //adjust the earned points

                if (itemLoyaltyPointOriginal.getPoints().compareTo(itemLoyaltyPointOriginal.getRefundedPoints()) == 0
                        && itemLoyaltyPointOriginal.getEligibleAmount().compareTo(itemLoyaltyPointOriginal.getRefundedEligibleAmount()) == 0) {
                    //do nothing, earned points equals the refunded points
                    continue;
                }

                //if the product quantity equals the number of Free Product Rewards
                if (productLineItemRefund.getQuantityForLoyalty().compareTo(freeProductRewardCount) == 0) {
                    //do nothing, earned points should not be updated
                    continue;
                } else {
                    //do the calculation

                    //Eligible Amount
                    BigDecimal elligibleAmtTimesPercentage = itemLoyaltyPointOriginal.getEligibleAmount().multiply(percentage);
                    BigDecimal newRefundedEligibleAmt = itemLoyaltyPointOriginal.getRefundedEligibleAmount().add(elligibleAmtTimesPercentage);

                    BigDecimal eligibleAmountDiff = itemLoyaltyPointOriginal.getEligibleAmount().abs().subtract(newRefundedEligibleAmt.abs());
                    //if the difference is less than .2, just set the refunded value to the original amount
                    if (eligibleAmountDiff.compareTo(new BigDecimal(.01)) == -1) {
                        itemLoyaltyPointOriginal.setEligibleAmount(itemLoyaltyPointOriginal.getEligibleAmount().multiply(percentage).add(eligibleAmountDiff));
                    } else {
                        itemLoyaltyPointOriginal.setEligibleAmount(itemLoyaltyPointOriginal.getEligibleAmount().multiply(percentage));
                    }

                    //points
                    BigDecimal pointsTimesPercentage = itemLoyaltyPointOriginal.getPoints().multiply(percentage);
                    //itemLoyaltyPoint.setPoints(pointsTimesPercentage);
                    BigDecimal newPointsAmt = itemLoyaltyPointOriginal.getRefundedPoints().add(pointsTimesPercentage);

                    BigDecimal amountDiff = itemLoyaltyPointOriginal.getPoints().abs().subtract(newPointsAmt.abs());
                    //if the difference is less than .2, just set the refunded value to the original amount
                    if (amountDiff.compareTo(new BigDecimal(.01)) == -1) {
                        //itemLoyaltyPoint.setRefundedPoints(itemLoyaltyPoint.getPoints());
                        itemLoyaltyPointOriginal.setPoints(itemLoyaltyPointOriginal.getPoints().multiply(percentage).add(amountDiff));

                    } else {
                        itemLoyaltyPointOriginal.setPoints(itemLoyaltyPointOriginal.getPoints().multiply(percentage));
                    }
                }

            } //end create Item Loyalty Points loop

            //add a new ItemLoyaltyPoint record
            productLineItemRefund.getLoyaltyPointDetails().add(itemLoyaltyPointOriginal);

        } //Item Loyalty Point loop

        return productLineItemRefund;
    }

    //region Getter/Setters
    public TransactionModel getTransactionModel() {
        return transactionModel;
    }

    public void setTransactionModel(TransactionModel transactionModel) {
        this.transactionModel = transactionModel;
    }

    //endregion
}

