package com.mmhayes.common.transaction.models;

//MMHayes Dependencies

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionDeserializer;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.transaction.collections.DiscountQuantityLevelCollection;
import com.mmhayes.common.utils.StringFunctions;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

//API Dependencies
//Other Dependencies

/*
 Last Updated (automatically updated by SVN)
 $Author: jkflanagan $: Author of last commit
 $Date: 2020-12-31 16:39:34 -0500 (Thu, 31 Dec 2020) $: Date of last commit
 $Rev: 13381 $: Revision of last commit
*/
@JsonPropertyOrder({"id", "name", "type", "amount"})
public class DiscountModel {

    DataManager dm = new DataManager();

    private Integer id = null;
    private String type = "";
    private String name = "";
    private String shortName = "";
    private BigDecimal amount = null;
    private Integer discountTypeId = null;
    private Integer mappedPosItem = null;
    private Integer maximumOpenPercent = null;
    private boolean isSubTotalDiscount = false;
    private BigDecimal maximumDiscountAmount = null;
    private BigDecimal minimumPurchaseAmount = null;
    private boolean isMealPlan = false;
    private boolean isAutoItemDiscount = false;
    private boolean isOpenPrice = false;
    private boolean isPresetPrice = false;
    private boolean hasDiscountQuantityRestrictions = false;

    private String taxIds = "";
    private DiscountQuantityLevelCollection discountQuantityLevelCollection = new DiscountQuantityLevelCollection();

    //default constructor
    public DiscountModel() {

    }

    //Constructor- takes in a Hashmap
    private DiscountModel(HashMap DiscountHM) {
        setModelProperties(DiscountHM);
    }

    //Constructor- takes in a Hashmap
    private DiscountModel(Integer discountId, Integer terminalId) throws Exception {

        getOneDiscountModel(discountId, terminalId);
    }

    //setter for all of this model's properties
    public DiscountModel setModelProperties(HashMap modelDetailHM) {
        setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));
        setName(CommonAPI.convertModelDetailToString(modelDetailHM.get("NAME")));
        setAmount(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("AMOUNT")));
        setDiscountTypeId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("DISCOUNTTYPEID")));
        setType(CommonAPI.convertModelDetailToString(modelDetailHM.get("DISCOUNTTYPETEXT")));
        setIsSubTotalDiscount(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("SUBTOTALDISCOUNT")));
        setMaximumDiscountAmount(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("MAXIMUMDISCOUNTAMOUNT")));
        setTaxIds(CommonAPI.convertModelDetailToString(modelDetailHM.get("TAXIDS")));
        setAutoItemDiscount(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("ISAUTOITEMDISCOUNT"), false));
        setOpenPrice(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("OPENDISCOUNT"), false));
        setPresetPrice(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("PRESETDISCOUNT"), false));
        setMinimumPurchaseAmount(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("MINPURCHASEAMT")));
        setMaximumOpenPercent(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("MAXOPENPERCENT")));
        setShortName(CommonAPI.convertModelDetailToString(modelDetailHM.get("SHORTNAME")));

        if ( modelDetailHM.containsKey("ISMEALPLAN") ) {
            setIsMealPlan(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("ISMEALPLAN")));
        }

        if (modelDetailHM.containsKey("HASDISCQUANTRESTRICTIONS") && !CommonAPI.convertModelDetailToString(modelDetailHM.get("HASDISCQUANTRESTRICTIONS")).isEmpty()){
           setHasDiscountQuantityRestrictions(true);
        }

        return this;
    }

    public void retrieveDiscountQuantityLevels(Integer terminalId) throws Exception {
        this.setDiscountQuantityLevelCollection( new DiscountQuantityLevelCollection( this.getId(), terminalId ) );
    }

    public static DiscountModel getOneDiscountModel(Integer discountId, Integer terminalId) throws Exception {
        DataManager dm = new DataManager();
        DiscountModel discountModel = new DiscountModel();

        //get Discount information Discount Id

        //get all models in an array list
        ArrayList<HashMap> discountList = dm.parameterizedExecuteQuery("data.posapi30.getOnePADiscount",
                new Object[]{
                        discountId
                },
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        HashMap modelDetailHM = discountList.get(0);
        return discountModel.setModelProperties(modelDetailHM);

    }

    public static DiscountModel getDiscountModel(Integer discountId, Integer terminalId) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> discountList = dm.parameterizedExecuteQuery("data.posapi30.getOnePADiscountById",
                new Object[]{discountId},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        //product list is null or empty, throw exception
        CommonAPI.checkIsNullOrEmptyList(discountList, "Discount Not Found", terminalId);
        return DiscountModel.createDiscountModel((discountList.get(0)));

    }

    public static DiscountModel getDiscountModel(String discountName, Integer terminalId) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> discountList = dm.parameterizedExecuteQuery("data.posapi30.getOnePADiscountByName",
                new Object[]{discountName},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        //product list is null or empty, throw exception
        CommonAPI.checkIsNullOrEmptyList(discountList, "Discount Not Found", terminalId);
        return DiscountModel.createDiscountModel((discountList.get(0)));

    }

    synchronized
    public static DiscountModel getDiscountModelWithRevenueCenter(final Integer discountId, final Integer revenueCenter, final Integer terminalId) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> discountList = dm.parameterizedExecuteQuery("data.posapi30.getOnePADiscountByIdAndRevenueCenter",
                new Object[]{discountId, revenueCenter},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        //product list is null or empty, throw exception
        CommonAPI.checkIsNullOrEmptyList(discountList, "Discount Not Found", terminalId);
        return DiscountModel.createDiscountModel((discountList.get(0)));
    }

    synchronized
    public static DiscountModel getDiscountModelWithRevenueCenter(final String discountName, final Integer revenueCenter, final Integer terminalId) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> discountList = dm.parameterizedExecuteQuery("data.posapi30.getOnePADiscountByNameAndRevenueCenter",
                new Object[]{discountName, revenueCenter},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        //product list is null or empty, throw exception
        CommonAPI.checkIsNullOrEmptyList(discountList, "Discount Not Found", terminalId);
        return DiscountModel.createDiscountModel((discountList.get(0)));
    }

    public static DiscountModel createDiscountModel(HashMap discountHM) throws Exception {
        return new DiscountModel(discountHM);
    }

    //getter
    public Integer getId() {
        return id;
    }

    //setter
    public void setId(Integer id) {
        this.id = id;
    }

    //getter
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getType() {
        return type;
    }

    //setter
    public void setType(String type) {
        this.type = type;
    }

    //getter
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getName() {
        return name;
    }

    //setter
    public void setName(String name) {
        this.name = name;
    }

    //getter
    public BigDecimal getAmount() {
        return amount;
    }

    //setter
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Integer getDiscountTypeId() {
        return discountTypeId;
    }

    public void setDiscountTypeId(Integer discountTypeId) {
        this.discountTypeId = discountTypeId;
    }

    @JsonIgnore
    public Integer getMappedPosItem() {
        return mappedPosItem;
    }

    public void setMappedPosItem(Integer mappedPosItem) {
        this.mappedPosItem = mappedPosItem;
    }

    @JsonGetter("subTotalDiscount")
    public boolean isSubTotalDiscount() {
        return isSubTotalDiscount;
    }

    @JsonSetter("subTotalDiscount")
    public void setIsSubTotalDiscount(boolean isSubTotalDiscount) {
        this.isSubTotalDiscount = isSubTotalDiscount;
    }

    @JsonIgnore
    public BigDecimal getMaximumDiscountAmount() {
        return maximumDiscountAmount;
    }

    public void setMaximumDiscountAmount(BigDecimal maximumDiscountAmount) {
        this.maximumDiscountAmount = maximumDiscountAmount;
    }

    @JsonIgnore
    public String getTaxIds() {
        return taxIds;
    }

    public void setTaxIds(String taxIds) {
        this.taxIds = taxIds;
    }

    @JsonIgnore
    public DiscountQuantityLevelCollection getDiscountQuantityLevelCollection() {
        return discountQuantityLevelCollection;
    }

    public void setDiscountQuantityLevelCollection(DiscountQuantityLevelCollection discountQuantityLevelCollection) {
        this.discountQuantityLevelCollection = discountQuantityLevelCollection;
    }

    @JsonGetter("isMealPlan")
    public boolean isMealPlan() {
        return isMealPlan;
    }

    public void setIsMealPlan(boolean isMealPlan) {
        this.isMealPlan = isMealPlan;
    }

    @JsonIgnore
    public boolean isAutoItemDiscount() {
        return isAutoItemDiscount;
    }

    public void setAutoItemDiscount(boolean autoItemDiscount) {
        isAutoItemDiscount = autoItemDiscount;
    }

    @JsonIgnore
    public boolean isPresetPrice() {
        return isPresetPrice;
    }

    public void setPresetPrice(boolean presetPrice) {
        isPresetPrice = presetPrice;
    }

    @JsonIgnore
    public boolean isOpenPrice() {
        return isOpenPrice;
    }

    public void setOpenPrice(boolean openPrice) {
        isOpenPrice = openPrice;
    }

    @JsonIgnore
    public BigDecimal getMinimumPurchaseAmount() {
        return minimumPurchaseAmount;
    }

    public void setMinimumPurchaseAmount(BigDecimal minimumPurchaseAmount) {
        this.minimumPurchaseAmount = minimumPurchaseAmount;
    }

    @JsonIgnore
    public Integer getMaximumOpenPercent() {
        return maximumOpenPercent;
    }

    public void setMaximumOpenPercent(Integer maximumOpenPercent) {
        this.maximumOpenPercent = maximumOpenPercent;
    }

    @JsonIgnore
    public boolean hasDiscountQuantityRestrictions() {
        return hasDiscountQuantityRestrictions;
    }

    public void setHasDiscountQuantityRestrictions(boolean hasDiscountQuantityRestrictions) {
        this.hasDiscountQuantityRestrictions = hasDiscountQuantityRestrictions;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    /**
     * Overridden toString method for a DiscountModel.
     * @return The {@link String} representation of a DiscountModel.
     */
    @Override
    public String toString () {

        return String.format("ID: %s, TYPE: %s, NAME:%s, SHORTNAME: %s, AMOUNT: %s, DISCOUNTTYPEID: %s, " +
                "MAPPEDPOSITEM: %s, MAXIMUMOPENPERCENT: %s, ISSUBTOTALDISCOUNT: %s, MAXIMUMDISCOUNTAMOUNT: %s, " +
                "MINIMUMPURCHASEAMOUNT: %s, ISMEALPLAN: %s, ISAUTOITEMDISCOUNT: %s, ISOPENPRICE: %s, ISPRESETPRICE: %s, " +
                "HASDISCOUNTQUANTITYRESTRICTIONS: %s, TAXIDS: %s, DISCOUNTQUANTITYLEVELCOLLECTION: %s",
                Objects.toString((id != null && id > 0 ? id : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(type) ? type : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(name) ? name : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(shortName) ? shortName : "N/A"), "N/A"),
                Objects.toString((amount != null ? amount.toPlainString() : "N/A"), "N/A"),
                Objects.toString((discountTypeId != null && discountTypeId > 0 ? discountTypeId : "N/A"), "N/A"),
                Objects.toString((mappedPosItem != null && mappedPosItem > 0 ? mappedPosItem : "N/A"), "N/A"),
                Objects.toString((maximumOpenPercent != null && maximumOpenPercent > 0 ? maximumOpenPercent : "N/A"), "N/A"),
                Objects.toString(isSubTotalDiscount, "N/A"),
                Objects.toString((maximumDiscountAmount != null ? maximumDiscountAmount.toPlainString() : "N/A"), "N/A"),
                Objects.toString((minimumPurchaseAmount != null ? minimumPurchaseAmount.toPlainString() : "N/A"), "N/A"),
                Objects.toString(isMealPlan, "N/A"),
                Objects.toString(isAutoItemDiscount, "N/A"),
                Objects.toString(isOpenPrice, "N/A"),
                Objects.toString(isPresetPrice, "N/A"),
                Objects.toString(hasDiscountQuantityRestrictions, "N/A"),
                Objects.toString((StringFunctions.stringHasContent(taxIds) ? taxIds : "N/A"), "N/A"),
                Objects.toString((discountQuantityLevelCollection != null ? discountQuantityLevelCollection.toString() : "N/A"), "N/A"));

    }

}




