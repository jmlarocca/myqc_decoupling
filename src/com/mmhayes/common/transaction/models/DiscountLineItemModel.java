package com.mmhayes.common.transaction.models;

//MMHayes dependencies
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.api.PosApi30.PosAPIHelper.DiscountApplicationResult;
import com.mmhayes.common.product.models.*;

//other dependencies
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import com.fasterxml.jackson.annotation.*;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2021-03-11 09:58:48 -0500 (Thu, 11 Mar 2021) $: Date of last commit
 $Rev: 13619 $: Revision of last commit
*/
public class DiscountLineItemModel extends TransactionLineItemModel {
    private DiscountModel discount = null;
    private QcDiscountModel qcDiscount = null;
    private Integer tempId = null;
    private Integer linkedPATransLineItemId = null;
    private boolean appliedToTransaction = false;
    private boolean autoAppliedOnCurrentInquire = false;
    private boolean requireAmountCheck = false;
    private DiscountApplicationResult applicationResult = DiscountApplicationResult.NOT_APPLIED_YET;
    private Integer resultCode = null;
    private String resultMessage = "";

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private ArrayList<ItemTaxModel> taxes = new ArrayList<>();

    //constructor
    public DiscountLineItemModel(){

    }

    public static DiscountLineItemModel createDiscountLineItemModel(HashMap modelDetailHM) throws Exception {
        DiscountLineItemModel discountLineItemModel = new DiscountLineItemModel();
        discountLineItemModel.setModelProperties(modelDetailHM);

        return discountLineItemModel;
    }

    public DiscountModel getDiscount() {
        return discount;
    }

    public void setDiscount(DiscountModel discount) {
        this.discount = discount;
    }

    @JsonIgnore //not returning this for this transaction line item type.  This should be assumed I think.  Trying to clean up the json.
    public @Override Integer getItemTypeId(){
        return PosAPIHelper.ObjectType.POSDISCOUNT.toInt();
    }

    public ArrayList<ItemTaxModel> getTaxes() {
        return taxes;
    }

    public void setTaxes(ArrayList<ItemTaxModel> taxes) {
        this.taxes = taxes;
    }

    public QcDiscountModel getQcDiscount() {
        return qcDiscount;
    }

    public void setQcDiscount(QcDiscountModel qcDiscount) {
        this.qcDiscount = qcDiscount;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getTempId() {
        return tempId;
    }

    public void setTempId(Integer tempId) {
        this.tempId = tempId;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getLinkedPATransLineItemId() {
        return linkedPATransLineItemId;
    }

    public void setLinkedPATransLineItemId(Integer linkedPATransLineItemId) {
        this.linkedPATransLineItemId = linkedPATransLineItemId;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getResultCode() {
        if (this.getApplicationResult() == null){
            resultCode = null;
        } else {
            resultCode = this.getApplicationResult().getCode();
        }

        return resultCode;
    }

    public void setResultCode(Integer resultCode) {
        this.resultCode = resultCode;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public String getResultMessage() {
        if (this.getApplicationResult() == null){
            resultMessage = "";
        } else {
            resultMessage = this.getApplicationResult().getMessage();
        }

        return resultMessage;
    }

    public void setResultMessage(String resultMessage) {
        this.resultMessage = resultMessage;
    }

    @JsonIgnore
    public DiscountApplicationResult getApplicationResult() {
        return applicationResult;
    }

    public void setApplicationResult(DiscountApplicationResult applicationResult) {
        this.applicationResult = applicationResult;
    }

    @JsonIgnore
    public boolean isAppliedToTransaction() {

        if (this.getEligibleAmount() != null && this.getEligibleAmount().compareTo(BigDecimal.ZERO) != 0){
            appliedToTransaction = true;
        }
        if (this.getAmount() != null && this.getAmount().compareTo(BigDecimal.ZERO) != 0){
            appliedToTransaction = true;
        }

        return appliedToTransaction;
    }

    @JsonIgnore
    public boolean isAutoAppliedOnCurrentInquire() {
        return autoAppliedOnCurrentInquire;
    }

    public void setAutoAppliedOnCurrentInquire(boolean autoAppliedOnCurrentInquire) {
        this.autoAppliedOnCurrentInquire = autoAppliedOnCurrentInquire;
    }

    @JsonIgnore
    /**
     * After the discount is applied, double check the transaction total to make sure not rounding issues occurred.
     */
    public boolean doesRequireAmountCheck() {
        return requireAmountCheck;
    }

    public void setRequireAmountCheck(boolean requireAmountCheck) {
        this.requireAmountCheck = requireAmountCheck;
    }
}
