package com.mmhayes.common.transaction.models;

//mmhayes dependencies

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionDeserializer;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.utils.StringFunctions;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

//API dependencies
//other dependencies

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2021-05-17 09:05:08 -0400 (Mon, 17 May 2021) $: Date of last commit
 $Rev: 13996 $: Revision of last commit
*/
public class TenderModel implements ITenderModel {
    Integer id = null;
    String name = "";
    Integer tenderTypeId = null;
    String tenderTypeName = "";
    String badgeNumPromptText = "";
    String tenderAccountGroupMappings = "";

    boolean allowOverTender = false;
    boolean allowUnderTender = false;
    boolean printSignatureReceipt = false;
    boolean printAccountLastFour = false;
    boolean restrictByAccountGroup = false;

    BigDecimal noSignatureUnderAmt = null;

    //default constructor
    public TenderModel() {
    }

    //Constructor- takes in a Hashmap
    public TenderModel(HashMap TenderHM) {
        setModelProperties(TenderHM);
    }

    //setter for all of this model's properties
    public TenderModel setModelProperties(HashMap modelDetailHM) {
        setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PATENDERID")));
        setName(CommonAPI.convertModelDetailToString(modelDetailHM.get("NAME")));
        setAllowOverTender(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("ALLOWOVERTENDER"), false));
        setAllowUnderTender(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("ALLOWUNDERTENDER"), false));

        setTenderTypeId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("TENDERTYPEID")));
        setTenderTypeName(CommonAPI.convertModelDetailToString(modelDetailHM.get("TENDERTYPENAME")));
        setBadgeNumPromptText(CommonAPI.convertModelDetailToString(modelDetailHM.get("BADGENUMPROMPTTEXT")));
        setPrintSignatureReceipt(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("PRINTSIGNATURERECEIPT"), false));
        setPrintAccountLastFour(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("PRINTACCTLASTFOUR"), false));

        setNoSignatureUnderAmt(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("NOSIGNATUREUNDERAMT")));
        setRestrictByAccountGroup(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("PARESTRICTPAYROLLGROUP"), false));
        setTenderAccountGroupMappings(CommonAPI.convertModelDetailToString(modelDetailHM.get("TENDERACCOUNTGROUPMAPPINGS")));
        return this;
    }

    public static TenderModel createTenderModel(HashMap tenderHM) {
        return new TenderModel(tenderHM);
    }

    //getOneActiveTenderById
    public static TenderModel getTenderModel(Integer tenderId, Integer terminalId) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> tenderList = dm.parameterizedExecuteQuery("data.posapi30.getOneTenderById",
                new Object[]{ tenderId },
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        //product list is null or empty, throw exception
        CommonAPI.checkIsNullOrEmptyList(tenderList, CommonAPI.PosModelType.TENDER, "", terminalId);
        return TenderModel.createTenderModel(tenderList.get(0));

    }

    public static TenderModel getTenderModel(String tenderName, Integer terminalId) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> tenderList = dm.parameterizedExecuteQuery("data.posapi30.getOneTenderByName",
                new Object[]{ tenderName },
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        //product list is null or empty, throw exception
        CommonAPI.checkIsNullOrEmptyList(tenderList, CommonAPI.PosModelType.TENDER, "", terminalId);
        return TenderModel.createTenderModel(tenderList.get(0));

    }

    @ApiModelProperty(value = "Tender ID.", dataType = "Integer", required=false,
            allowableValues = "1, 2, 3, 4, 5, etc.", notes= "Relates to the QC_PATenderType.PATenderID table and field.")
    public Integer getId() {
        return id;
    }

    //setter
    public void setId(Integer id) {
        this.id = id;
    }

    //getter
    @ApiModelProperty(value = "Name of the Tender.", dataType = "String", required=true,
            example="quickcharge", allowableValues = "quickcharge, cash, credit card, check, etc.",
            notes= "Relates to the QC_PATender.Name table and field.")
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getName() {
        return name;
    }

    //setter
    public void setName(String name) {
        this.name = name;
    }

    @ApiModelProperty(value = "Tender Type ID.", dataType = "Integer", required=false,
            allowableValues = "1, 2, 3, 4, 5, etc.",
            notes= "Relates to the QC_PATenderType.PATenderTypeID table and field.")
    public Integer getTenderTypeId() {
        return tenderTypeId;
    }

    public void setTenderTypeId(Integer tenderTypeId) {
        this.tenderTypeId = tenderTypeId;
    }

    @ApiModelProperty(value = "Tender Type Name.", dataType = "String", required=false,
            allowableValues = "cash, credit card, check, quickcharge, etc.",
            notes= "Relates to the QC_PATenderType.Name table and field.")
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getTenderTypeName() {
        return tenderTypeName;
    }

    public void setTenderTypeName(String tenderTypeName) {
        this.tenderTypeName = tenderTypeName;
    }

    @ApiModelProperty(value = "Does tender allow over tender.", dataType = "boolean", required=false,
            allowableValues = "true, false",
            notes= "Relates to the QC_PATender.AllowOverTender table and field.")
    public boolean isAllowOverTender() {
        return allowOverTender;
    }

    public void setAllowOverTender(boolean allowOverTender) {
        this.allowOverTender = allowOverTender;
    }

    @ApiModelProperty(value = "Does tender allow under tender.", dataType = "boolean", required=false,
            allowableValues = "true, false",
            notes= "Relates to the QC_PATender.AllowOverTender table and field.")
    public boolean isAllowUnderTender() {
        return allowUnderTender;
    }

    public void setAllowUnderTender(boolean allowUnderTender) {
        this.allowUnderTender = allowUnderTender;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getBadgeNumPromptText() {
        return badgeNumPromptText;
    }

    public void setBadgeNumPromptText(String badgeNumPromptText) {
        this.badgeNumPromptText = badgeNumPromptText;
    }

    @JsonIgnore
    public boolean isPrintSignatureReceipt() {
        return printSignatureReceipt;
    }

    public void setPrintSignatureReceipt(boolean printSignatureReceipt) {
        this.printSignatureReceipt = printSignatureReceipt;
    }

    @JsonIgnore
    public boolean isPrintAccountLastFour() {
        return printAccountLastFour;
    }

    public void setPrintAccountLastFour(boolean printAccountLastFour) {
        this.printAccountLastFour = printAccountLastFour;
    }

    @JsonIgnore
    public BigDecimal getNoSignatureUnderAmt() {
        return noSignatureUnderAmt;
    }

    public void setNoSignatureUnderAmt(BigDecimal noSignatureUnderAmt) {
        this.noSignatureUnderAmt = noSignatureUnderAmt;
    }

    @JsonIgnore
    public boolean isRestrictByAccountGroup() {
        return restrictByAccountGroup;
    }

    public void setRestrictByAccountGroup(boolean restrictByAccountGroup) {
        this.restrictByAccountGroup = restrictByAccountGroup;
    }

    @JsonIgnore
    public String getTenderAccountGroupMappings() {
        return tenderAccountGroupMappings;
    }

    public void setTenderAccountGroupMappings(String tenderAccountGroupMappings) {
        this.tenderAccountGroupMappings = tenderAccountGroupMappings;
    }

    /**
     * Overridden toString method for a TenderModel.
     * @return The {@link String} representation of a TenderModel.
     */
    @Override
    public String toString () {

        return String.format("ID: %s, NAME:%s, TENDERTYPEID: %s, TENDERTYPENAME: %s, BADGENUMPROMPTTEXT: %s, " +
                "ALLOWOVERTENDER: %s, ALLOWUNDERTENDER: %s, PRINTSIGNATURERECEIPT: %s, PRINTACCOUNTLASTFOUR: %s, " +
                "NOSIGNATUREUNDERAMT: %s",
                Objects.toString((id != null && id > 0 ? id : "N/A"), "N/A"),
                Objects.toString((tenderTypeId != null && tenderTypeId > 0 ? tenderTypeId : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(name) ? name : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(tenderTypeName) ? tenderTypeName : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(badgeNumPromptText) ? badgeNumPromptText : "N/A"), "N/A"),
                Objects.toString(allowOverTender, "N/A"),
                Objects.toString(allowUnderTender, "N/A"),
                Objects.toString(printSignatureReceipt, "N/A"),
                Objects.toString(printAccountLastFour, "N/A"),
                Objects.toString((noSignatureUnderAmt != null ? noSignatureUnderAmt.toPlainString() : "N/A"), "N/A"));
    }
    
}
