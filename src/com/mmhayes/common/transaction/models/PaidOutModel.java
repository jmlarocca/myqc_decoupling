package com.mmhayes.common.transaction.models;

//mmhayes dependencies
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.dataaccess.DataManager;

//API dependencies
import com.mmhayes.common.api.CommonAPI;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionDeserializer;

//other dependencies
import java.util.ArrayList;
import java.util.HashMap;

/*
 $Author: eglundin $: Author of last commit
 $Date: 2019-07-02 08:12:14 -0400 (Tue, 02 Jul 2019) $: Date of last commit
 $Rev: 9024 $: Revision of last commit
 Notes:
*/
public class PaidOutModel {
    Integer id = null;
    String name = "";

    public PaidOutModel(){

    }

    //Constructor- takes in a Hashmap
    public PaidOutModel(HashMap modelHM) {
        setModelProperties(modelHM);
    }

    //setter for all of this model's properties
    public PaidOutModel setModelProperties(HashMap modelDetailHM) {
        setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));
        setName(CommonAPI.convertModelDetailToString(modelDetailHM.get("NAME")));

        return this;
    }

    public static PaidOutModel getPaidOutModel(Integer paidOutId, Integer terminalId) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> paidOutList = dm.parameterizedExecuteQuery("data.posapi30.getOnePaidOutById",
                new Object[]{ paidOutId },
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        //product list is null or empty, throw exception
        CommonAPI.checkIsNullOrEmptyList(paidOutList, "Paid Out Not Found", terminalId);
        return PaidOutModel.createPaidOutModel(paidOutList.get(0));

    }

    public static PaidOutModel getPaidOutModel(String paidOutName, Integer terminalId) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> paidOutList = dm.parameterizedExecuteQuery("data.posapi30.getOnePaidOutByName",
                new Object[]{ paidOutName },
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        //product list is null or empty, throw exception
        CommonAPI.checkIsNullOrEmptyList(paidOutList, "Paid Out Not Found", terminalId);
        CommonAPI.checkMoreThanOneRecord(paidOutList, "More than one Paid Out record was found with that name");
        return  PaidOutModel.createPaidOutModel(paidOutList.get(0));

    }

    public static PaidOutModel createPaidOutModel(HashMap modelHM) {
        return new PaidOutModel(modelHM);
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
