package com.mmhayes.common.transaction.models;

//MMHayes Dependencies

import com.mmhayes.common.account.models.AccountModel;
import com.mmhayes.common.terminal.models.TerminalModel;
import com.mmhayes.common.utils.Logger;

//API dependencies
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.api.CommonAPI;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionDeserializer;

//other dependencies
import com.fasterxml.jackson.annotation.*;
import com.mmhayes.common.voucher.models.VoucherCodeModel;

import java.math.BigDecimal;
import java.util.HashMap;

/*
 Last Updated (automatically updated by SVN)
 $Author: nyu $: Author of last commit
 $Date: 2021-08-17 14:37:51 -0400 (Tue, 17 Aug 2021) $: Date of last commit
 $Rev: 14929 $: Revision of last commit
*/
@JsonPropertyOrder({"id", "itemTypeId", "employeeId", "badgeAssignmentId", "quantity", "amount", "extendedAmount", "refundedQty", "refundedAmount", "refundedExtAmount",
        "eligibleAmount", "payments", "creditCardTransInfo", "itemComment", "paymentMethodType", "ccAuthorizationTypeId", "safTransInfo", "originalOrderNumber", "tender", "account", "transactionItemNum"})
public class TenderLineItemModel extends TransactionLineItemModel {
    private Integer payments = null;
    private Integer posItemId = null;
    private Integer paymentMethodTypeId = null;
    private String creditCardTransInfo = "";
    private String paymentMethodType = "";
    private String safTransInfo = "";
    private boolean isQuickChargeTender = false;
    private boolean meetsMinSignatureAmount = false;

    private Integer ccAuthorizationTypeId = null;

    private AccountModel account = null;
    private TenderDisplayModel tender = null;
    private QcTransactionModel qcTransaction = null;
    private VoucherCodeModel voucherCode = null;

    //constructor
    public TenderLineItemModel() {
    }

    //map common qc pos transaction line item fields
    public void setTenderModelProperties(HashMap modelDetailHM, Integer terminalId) throws Exception {
        this.setPayments(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("SPLITS")));
        this.setCreditCardTransInfo(CommonAPI.convertModelDetailToString(modelDetailHM.get("CREDITCARDTRANSINFO")));
        this.setPaymentMethodTypeId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAYMENTMETHODTYPEID")));
        this.setPaymentMethodType(CommonAPI.convertModelDetailToString(modelDetailHM.get("PAYMENTMETHODTYPE")));
        this.setCcAuthorizationTypeId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("CCAUTHORIZATIONTYPEID")));
        this.setSafTransInfo(CommonAPI.convertModelDetailToString(modelDetailHM.get("SAFTRANSID")));

        //create a voucher if one exists
        if (modelDetailHM.containsKey("VOUCHERCODE")) {
            String voucherCode = CommonAPI.convertModelDetailToString(modelDetailHM.get("VOUCHERCODE"));
            if (!voucherCode.equals("")) {
                VoucherCodeModel vcm = new VoucherCodeModel();              //Instantiate a VoucherCodeModel
                vcm.setCode(voucherCode);                                   //Set the code to what we were given
                vcm.createVoucherFromCode(new TerminalModel(terminalId));   //Recreate the VoucherCodeModel from the code
                setVoucherCode(vcm);                                        //Set this object's VoucherCodeModel to the newly created one
            }
        }
    }

    public static TenderLineItemModel createTenderLineItemModel(HashMap modelDetailHM, TerminalModel terminalModel) throws Exception {
        TenderLineItemModel tenderLineItemModel = new TenderLineItemModel();
        tenderLineItemModel.setModelProperties(modelDetailHM);
        tenderLineItemModel.setTenderModelProperties(modelDetailHM, terminalModel.getId());
        return tenderLineItemModel;
    }

    public static TenderLineItemModel createTenderLineItemModel(Integer tenderID) throws Exception {
        TenderDisplayModel tenderDisplayModel = new TenderDisplayModel();
        tenderDisplayModel.setId(tenderID);

        TenderLineItemModel tenderLineItemModel = new TenderLineItemModel();
        tenderLineItemModel.setTender(tenderDisplayModel);
        tenderLineItemModel.setItemId(tenderID);
        tenderLineItemModel.setItemTypeId(PosAPIHelper.ObjectType.TENDER.toInt());
        tenderLineItemModel.setAmount(BigDecimal.ZERO);
        tenderLineItemModel.setQuantity(BigDecimal.ONE);
        return tenderLineItemModel;
    }

    /**
     * Determine if the tender line amount is under tender
     * @param totalProductAmount
     * @return
     */
    public boolean isUnderTender(BigDecimal totalProductAmount){
        boolean isUnderTender = false;

        if (this.getExtendedAmount().compareTo(totalProductAmount) < 0) { //Under Tender
            if (!this.getTender().isAllowUnderTender()) {
                {
                    isUnderTender = true;
                }
            }
        }

        return isUnderTender;
    }

    /**
     * Determine if the tender line amount is over tender
     * @param transactionModel
     * @param totalProductAmount
     * @return
     */
    public boolean isOverTender(TransactionModel transactionModel, BigDecimal totalProductAmount) {
        boolean isOverTender = false;
        BigDecimal roundingBufferAmt = new BigDecimal(.02);

        if (((transactionModel.getProducts() != null && !transactionModel.getProducts().isEmpty())  //if there are products
                || totalProductAmount.compareTo(BigDecimal.ZERO) > 0) &&                                                      //or the total product amount > 0
                this.getExtendedAmount().compareTo(totalProductAmount.add(roundingBufferAmt)) > 0) { //Over Tender

            if (!this.getTender().isAllowOverTender()) {
                isOverTender = true;
            }
        }

        return isOverTender;
    }

    public TenderDisplayModel getTender() {
        return tender;
    }

    public void setTender(TenderDisplayModel tender) {
        this.tender = tender;
    }

    @Override
    public Integer getItemTypeId() {
        return PosAPIHelper.ObjectType.TENDER.toInt();
    }

    public Integer getPayments() {
        return payments;
    }

    public void setPayments(Integer payments) {
        this.payments = payments;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public String getCreditCardTransInfo() {
        return creditCardTransInfo;
    }

    public void setCreditCardTransInfo(String creditCardTransInfo) {
        this.creditCardTransInfo = creditCardTransInfo;
    }

    @JsonIgnore
    public Integer getPosItemId() {
        return posItemId;
    }

    public void setPosItemId(Integer posItemId) {
        this.posItemId = posItemId;
    }

    @JsonIgnore
    public boolean isQuickChargeTender() {
        if (this.getTender() != null) {

            if (this.getTender().getTenderTypeId() != null) {
                if (this.getTender().getTenderTypeId().equals(PosAPIHelper.TenderType.QUICKCHARGE.toInt())) {
                    this.isQuickChargeTender = true;
                }
            } else if (this.getTender().getTenderTypeName() != null) {
                if (this.getTender().getTenderTypeName().equals(PosAPIHelper.TenderType.QUICKCHARGE.toString())) {
                    this.isQuickChargeTender = true;
                }
            } else if (this.getTender().getName() != null) {
                if (this.getTender().getName().equals(PosAPIHelper.TenderType.QUICKCHARGE.toString())) {
                    this.isQuickChargeTender = true;
                }
            }
        }

        return this.isQuickChargeTender;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public AccountModel getAccount() {
        return account;
    }

    public void setAccount(AccountModel account) {
        this.account = account;
    }

    @JsonIgnore
    public QcTransactionModel getQcTransaction() {
        return qcTransaction;
    }

    public void setQcTransaction(QcTransactionModel qcTransaction) {
        this.qcTransaction = qcTransaction;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getPaymentMethodType() {
        return paymentMethodType;
    }

    public void setPaymentMethodType(String paymentMethodType) {
        this.paymentMethodType = paymentMethodType;
    }

    @JsonIgnore
    public Integer getPaymentMethodTypeId() {
        return paymentMethodTypeId;
    }

    public void setPaymentMethodTypeId(Integer paymentMethodTypeId) {
        this.paymentMethodTypeId = paymentMethodTypeId;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getCcAuthorizationTypeId() {
        return ccAuthorizationTypeId;
    }

    public void setCcAuthorizationTypeId(Integer ccAuthorizationTypeId) {
        this.ccAuthorizationTypeId = ccAuthorizationTypeId;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getSafTransInfo() {
        return safTransInfo;
    }

    public void setSafTransInfo(String safTransInfo) {
        this.safTransInfo = safTransInfo;
    }

    //Voucher Code - getter / setter
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public VoucherCodeModel getVoucherCode() {
        return voucherCode;
    }
    public void setVoucherCode(VoucherCodeModel val) { this.voucherCode = val; }

    @JsonIgnore
    public boolean doesMeetMinSignatureAmount() {

        if (this.getTender() != null) {

            if (this.getTender().getNoSignatureUnderAmt() == null) {
                meetsMinSignatureAmount = true;
            } else {
                if (this.getTender().getNoSignatureUnderAmt() != null
                        && this.getExtendedAmount().compareTo(this.getTender().getNoSignatureUnderAmt()) >= 0) { //Tender line extended amount >= minimum Signature Amount
                    meetsMinSignatureAmount = true;
                } else {
                    Logger.logMessage("Tender amount does not meet Min Signature Amount.  Nothing will be printed", Logger.LEVEL.TRACE);
                }
            }
        }

        return meetsMinSignatureAmount;
    }
}
