package com.mmhayes.common.transaction.models;

//mmhayes dependencies


import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.dataaccess.DataManager;

//API dependencies
import com.mmhayes.common.api.CommonAPI;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionDeserializer;

//other dependencies
import java.util.ArrayList;
import java.util.HashMap;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2019-07-02 08:12:14 -0400 (Tue, 02 Jul 2019) $: Date of last commit
 $Rev: 9024 $: Revision of last commit
*/
public class PurchaseCategoryModel {

    Integer id = null;
    String name = "";

    public PurchaseCategoryModel() {

    }

    public PurchaseCategoryModel(Integer posItemId) {
        this.id = posItemId;
    }

    //Constructor- takes in a Hashmap
    public PurchaseCategoryModel(HashMap modelHM) {
        setModelProperties(modelHM);
    }

    //setter for all of this model's properties
    public PurchaseCategoryModel setModelProperties(HashMap modelDetailHM) {
        setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));
        setName(CommonAPI.convertModelDetailToString(modelDetailHM.get("NAME")));

        return this;
    }

    public static PurchaseCategoryModel getPurchaseCategoryModel(Integer purchaseCategoryId, Integer terminalId) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> purchaseCategoryList = dm.parameterizedExecuteQuery("data.posapi30.getOnePurchaseCategoryById",
                new Object[]{purchaseCategoryId},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        //TODO - this needs to be fixed, there is a bug in the Backoffice to allow mappings with non Active Purchase Categories
        if (!purchaseCategoryList.isEmpty()) {
            return PurchaseCategoryModel.createPurchaseCategoryModel(purchaseCategoryList.get(0));
        } else {
            PurchaseCategoryModel purchaseCategoryModel = new PurchaseCategoryModel();
            purchaseCategoryModel.setId(purchaseCategoryId);
            return purchaseCategoryModel;
        }

    }

    public static PurchaseCategoryModel getPurchaseCategoryModel(String purchaseCategoryName, Integer terminalId) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> purchaseCategoryList = dm.parameterizedExecuteQuery("data.posapi30.getOnePurchaseCategoryByName",
                new Object[]{purchaseCategoryName},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        //product list is null or empty, throw exception
        //CommonAPI.checkIsNullOrEmptyList(purchaseCategoryList, "Purchase Category Not Found");
        return PurchaseCategoryModel.createPurchaseCategoryModel(purchaseCategoryList.get(0));

    }

    public static PurchaseCategoryModel createPurchaseCategoryModel(HashMap modelHM) {
        return new PurchaseCategoryModel(modelHM);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
