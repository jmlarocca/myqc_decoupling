package com.mmhayes.common.transaction.models;

//MMHayes Dependencies

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.api.PosApi30.PosAPIModelCache;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionDeserializer;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.product.models.ItemDiscountModel;
import com.mmhayes.common.product.models.ItemRewardModel;
import com.mmhayes.common.product.models.ItemTaxModel;
import com.mmhayes.common.terminal.models.TerminalModel;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.StringFunctions;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

//API Dependencies
//Other dependencies

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2017-06-19 10:51:51 -0400 (Mon, 19 Jun 2017) $: Date of last commit
 $Rev: 4154 $: Revision of last commit
*/
public class SurchargeModel {

    Integer id = null;
    Integer applicationTypeId = null;
    Integer typeId = null;
    Integer amountTypeId = null;

    String name = "";
    String type = ""; //Revenue or Non-Revenue
    String applicationType = "";  //Subtotal or item
    String amountType = ""; //dollar or percent
    String shortName = "";
    String taxIds = "";
    String description = "";

    Boolean includeRewards;
    Boolean includeItemDiscounts;
    Boolean includeSubtotalDiscounts;
    Boolean includeTaxes;
    Boolean open;
    Boolean preset;
    Boolean refundsWithProducts;
    Boolean useAsConvenienceFee;
    Boolean eligibleForSubtotalSurcharge;

    BigDecimal amount = BigDecimal.ZERO;
    Integer linkedLineItemId = null;
    List<TaxModel> taxes = new ArrayList<>();

    public SurchargeModel() {

    }

    public static SurchargeModel createSurchargeModel(HashMap surchargeHM, TerminalModel terminalModel) {
        SurchargeModel surchargeModel = new SurchargeModel();
        surchargeModel.setModelProperties(surchargeHM, terminalModel);
        return surchargeModel;
    }

    public static SurchargeModel createSurchargeModel(HashMap surchargeHM, Integer terminalID) {
        TerminalModel terminalModel = new TerminalModel();
        terminalModel.setId(terminalID);
        SurchargeModel surchargeModel = new SurchargeModel();
        surchargeModel.setModelProperties(surchargeHM, terminalModel);
        return surchargeModel;
    }

    //setter for all of this model's properties
    public SurchargeModel setModelProperties(HashMap modelDetailHM, TerminalModel terminalModel) {
        setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));
        setName(CommonAPI.convertModelDetailToString(modelDetailHM.get("NAME")));
        setType(CommonAPI.convertModelDetailToString(modelDetailHM.get("TYPE")));
        setTypeId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PASURCHARGETYPEID")));

        setApplicationType(CommonAPI.convertModelDetailToString(modelDetailHM.get("APPLICATIONTYPE")));
        setApplicationTypeId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PASURCHARGEAPPLICATIONTYPEID")));
        setAmountType(CommonAPI.convertModelDetailToString(modelDetailHM.get("AMOUNTTYPE")));
        setAmountTypeId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PASURCHARGEAMOUNTTYPEID")));
        setShortName(CommonAPI.convertModelDetailToString(modelDetailHM.get("SHORTNAME")));
        setAmount(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("AMOUNT")));

        setIncludeRewards(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("INCLUDEREWARDS"), false));
        setIncludeItemDiscounts(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("INCLUDEITEMDISCOUNTS"), false));
        setIncludeSubtotalDiscounts(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("INCLUDESUBTOTALDISCOUNTS"), false));
        setIncludeTaxes(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("INCLUDETAXES"), false));
        setOpen(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("ISOPEN"), false));
        setPreset(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("ISPRESET"), false));
        setRefundsWithProducts(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("REFUNDSWITHPRODUCTS"), false));
        setTaxIds(CommonAPI.convertModelDetailToString(modelDetailHM.get("TAXIDS")));
        setDescription(CommonAPI.convertModelDetailToString(modelDetailHM.get("DESCRIPTION")));
        setUseAsConvenienceFee(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("USEASCONVENIENCEFEE"), false));
        setEligibleForSubtotalSurcharge(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("ELIGIBLEFORSUBTOTALSURCHARGE"), false));

        setLinkedLineItemId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("LINKEDLINEITEMID")));

        if (getTaxIds() != null && !getTaxIds().isEmpty()) {
            try {
                List<String> taxIdsStrList = CommonAPI.convertCommaSeparatedStringToArrayList(getTaxIds(), false);
                for (String taxIdStr : taxIdsStrList){
                    TaxModel taxModel = (TaxModel) PosAPIModelCache.getOneObject(terminalModel.getId(), CommonAPI.PosModelType.TAX, Integer.parseInt(taxIdStr));
                    getTaxes().add(taxModel);
                }
            }
            catch (Exception ex) {

            }
        }

        return this;
    }

    public static SurchargeModel getSurchargeModel(Integer surchargeId, TerminalModel terminalModel) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> surchargeList = dm.parameterizedExecuteQuery("data.posapi30.getOneSurchargeById",
                new Object[]{ surchargeId },
                PosAPIHelper.getLogFileName(terminalModel.getId()),
                true
        );

        CommonAPI.checkIsNullOrEmptyList(surchargeList, CommonAPI.PosModelType.SURCHARGE, "", terminalModel.getId());
        return SurchargeModel.createSurchargeModel(surchargeList.get(0), terminalModel);

    }

    public static SurchargeModel getSurchargeModel(String surchargeName, TerminalModel terminalModel) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> surchargeList = dm.parameterizedExecuteQuery("data.posapi30.getOneSurchargeByName",
                new Object[]{ surchargeName },
                PosAPIHelper.getLogFileName(terminalModel.getId()),
                true
        );

        CommonAPI.checkIsNullOrEmptyList(surchargeList, CommonAPI.PosModelType.SURCHARGE, "", terminalModel.getId());
        return SurchargeModel.createSurchargeModel(surchargeList.get(0), terminalModel);

    }

    public static SurchargeModel getActiveSurchargeModel(Integer surchargeId, TerminalModel terminalModel) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> surchargeList = dm.parameterizedExecuteQuery("data.posapi30.getOneActiveSurchargeById",
                new Object[]{ surchargeId, terminalModel.getRevenueCenterId() },
                PosAPIHelper.getLogFileName(terminalModel.getId()),
                true
        );

        CommonAPI.checkIsNullOrEmptyList(surchargeList, CommonAPI.PosModelType.SURCHARGE, "", terminalModel.getId());
        return SurchargeModel.createSurchargeModel(surchargeList.get(0), terminalModel);

    }

    public static SurchargeModel getActiveSurchargeModel(String surchargeName, TerminalModel terminalModel) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> surchargeList = dm.parameterizedExecuteQuery("data.posapi30.getOneActiveSurchargeByName",
                new Object[]{ surchargeName, terminalModel.getRevenueCenterId() },
                PosAPIHelper.getLogFileName(terminalModel.getId()),
                true
        );

        CommonAPI.checkIsNullOrEmptyList(surchargeList, CommonAPI.PosModelType.SURCHARGE, "", terminalModel.getId());
        return SurchargeModel.createSurchargeModel(surchargeList.get(0), terminalModel);

    }

    //parse out the TaxIds field in the db to get the taxes available on the reward
    private String getTaxesForSurcharge() throws Exception {
        StringBuilder sb = new StringBuilder();
        String[] taxIds = this.getTaxIds().split(",");

        if (taxIds != null && taxIds.length > 0) {
            for (Integer iCount = 0; iCount < taxIds.length; iCount++) {
                sb.append(taxIds[iCount]);
                if (iCount != taxIds.length - 1) {
                    sb.append(",");
                }
            }

        } else {
            sb.append("''"); //add a blank string in for the name
        }

        return sb.toString();
    }

    //calculate the product total the surcharge will be applied to
    public BigDecimal calculateItemTotal(ProductLineItemModel productLineItemModel) {
        BigDecimal itemTotal = productLineItemModel.getExtendedAmount();

        //if product is in combo, calculate surcharge amount off product's price in combo
        if(productLineItemModel.getComboTransLineItemId() != null) {
            BigDecimal priceInCombo = (productLineItemModel.getProduct().getPrice().subtract(productLineItemModel.getBasePrice())).add(productLineItemModel.getComboPrice());
            itemTotal = priceInCombo.multiply(productLineItemModel.getQuantity());
        }

        //if 'Include Taxes' is set to TRUE, include product's tax amount in total
        if(getIncludeTaxes()) {
            for(ItemTaxModel itemTaxModel : productLineItemModel.getTaxes()) {
                itemTotal = itemTotal.add(itemTaxModel.getAmount().setScale(3, RoundingMode.HALF_UP));
            }
        }

        //if 'Include Item Discounts' is set to TRUE, include product's item discount amount in total
        if(getIncludeItemDiscounts()) {
            for(ItemDiscountModel itemDiscountModel : productLineItemModel.getDiscounts()) {
                if(!itemDiscountModel.getDiscount().isSubTotalDiscount() && !itemDiscountModel.isComboDiscount()) {
                    itemTotal = itemTotal.add(itemDiscountModel.getAmount().setScale(3, RoundingMode.HALF_UP));
                }
            }
        }

        //if 'Include Subtotal Discounts' is set to TRUE, include product's subtotal discount amount in total
        if(getIncludeSubtotalDiscounts()) {
            for(ItemDiscountModel itemDiscountModel : productLineItemModel.getDiscounts()) {
                if(itemDiscountModel.getDiscount().isSubTotalDiscount()) {
                    itemTotal = itemTotal.add(itemDiscountModel.getAmount().setScale(3, RoundingMode.HALF_UP));
                }
            }
        }

        //if 'Include Rewards' is set to TRUE, include product's reward amount in total
        if(getIncludeRewards()) {
            for(ItemRewardModel itemRewardModel : productLineItemModel.getRewards()) {
                itemTotal = itemTotal.add(itemRewardModel.getAmount().setScale(3, RoundingMode.HALF_UP));
            }
        }
        return itemTotal;
    }

    //calculate the surcharge amount based on the product's total and the Surcharge Amount Type - Percent or Dollar
    public BigDecimal calculateItemSurchargeAmount(ProductLineItemModel productLineItemModel) {
        BigDecimal surchargeAmount = getAmount();

        if(getAmountTypeId().equals(1)) { //Dollar
            surchargeAmount = surchargeAmount.multiply(productLineItemModel.getQuantity());
            return surchargeAmount;
        }

        if(getAmountTypeId().equals(2)) { //Percent
            BigDecimal itemTotal = calculateItemTotal(productLineItemModel);
            BigDecimal oneHundred = new BigDecimal(100);

            //don't divide by zero
            if (surchargeAmount.compareTo(BigDecimal.ZERO) != 0) {
                BigDecimal surchargePercent = surchargeAmount.divide(oneHundred);
                surchargeAmount = itemTotal.multiply(surchargePercent);
            }
        }

        return surchargeAmount;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getApplicationType() {
        return applicationType;
    }

    public void setApplicationType(String applicationType) {
        this.applicationType = applicationType;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getAmountType() {
        return amountType;
    }

    public void setAmountType(String amountType) {
        this.amountType = amountType;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Boolean getIncludeRewards() {
        return includeRewards;
    }

    public void setIncludeRewards(Boolean includeRewards) {
        this.includeRewards = includeRewards;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Boolean getIncludeItemDiscounts() {
        return includeItemDiscounts;
    }

    public void setIncludeItemDiscounts(Boolean includeItemDiscounts) {
        this.includeItemDiscounts = includeItemDiscounts;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Boolean getIncludeSubtotalDiscounts() {
        return includeSubtotalDiscounts;
    }

    public void setIncludeSubtotalDiscounts(Boolean includeSubtotalDiscounts) {
        this.includeSubtotalDiscounts = includeSubtotalDiscounts;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Boolean getIncludeTaxes() {
        return includeTaxes;
    }

    public void setIncludeTaxes(Boolean includeTaxes) {
        this.includeTaxes = includeTaxes;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Boolean isOpen() {
        return open;
    }

    public void setOpen(Boolean open) {
        this.open = open;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Boolean isPreset() {
        return preset;
    }

    public void setPreset(Boolean preset) {
        this.preset = preset;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Boolean getRefundsWithProducts() {
        return refundsWithProducts;
    }

    public void setRefundsWithProducts(Boolean refundsWithProducts) {
        this.refundsWithProducts = refundsWithProducts;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getLinkedLineItemId() {
        return linkedLineItemId;
    }

    public void setLinkedLineItemId(Integer linkedLineItemId) {
        this.linkedLineItemId = linkedLineItemId;
    }

    @JsonIgnore
    public String getTaxIds() {
        return taxIds;
    }

    public void setTaxIds(String taxIds) {
        this.taxIds = taxIds;
    }

    public List<TaxModel> getTaxes() {
        return taxes;
    }

    public void setTaxes(List<TaxModel> taxes) {
        this.taxes = taxes;
    }

    @JsonIgnore
    public Integer getApplicationTypeId() {
        return applicationTypeId;
    }

    public void setApplicationTypeId(Integer applicationTypeId) {
        this.applicationTypeId = applicationTypeId;
    }

    @JsonIgnore
    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    @JsonIgnore
    public Integer getAmountTypeId() {
        return amountTypeId;
    }

    public void setAmountTypeId(Integer amountTypeId) {
        this.amountTypeId = amountTypeId;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Boolean getUseAsConvenienceFee() {
        return useAsConvenienceFee;
    }

    public void setUseAsConvenienceFee(Boolean useAsConvenienceFee) {
        this.useAsConvenienceFee = useAsConvenienceFee;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Boolean getEligibleForSubtotalSurcharge() {
        return eligibleForSubtotalSurcharge;
    }

    public void setEligibleForSubtotalSurcharge(Boolean eligibleForSubtotalSurcharge) {
        this.eligibleForSubtotalSurcharge = eligibleForSubtotalSurcharge;
    }

    /**
     * Overridden toString method for a SurchargeModel.
     * @return The {@link String} representation of a SurchargeModel.
     */
    @Override
    public String toString () {

        String taxesStr = "";
        if (!DataFunctions.isEmptyCollection(taxes)) {
            taxesStr += "[";
            for (TaxModel tax : taxes) {
                taxesStr += "[";
                if (tax.equals(taxes.get(taxes.size() - 1))) {
                    taxesStr += tax.toString() + "; ";
                }
                else {
                    taxesStr += tax.toString() + "]";
                }
            }
            taxesStr += "]";
        }

        return String.format("ID: %s, APPLICATIONTYPEID: %s, TYPEID: %s, AMOUNTTYPEID: %s, NAME: %s, TYPE: %s, " +
                "APPLICATIONTYPE: %s, AMOUNTTYPE: %s, SHORTNAME: %s, TAXIDS: %s, DESCRIPTION: %s, INCLUDEREWARDS: %s, " +
                "INCLUDEITEMDISCOUNTS: %s, INCLUDESUBTOTALDISCOUNTS: %s, INCLUDETAXES: %s, OPEN: %s, PRESET: %s, " +
                "REFUNDSWITHPRODUCTS: %s, USEASCONVENIENCEFEE: %s, ELIGIBLEFORSUBTOTALSURCHARGE: %s, AMOUNT: %s, " +
                "LINKEDLINEITEMID: %s, TAXES: %s",
                Objects.toString((id != null && id > 0 ? id : "N/A"), "N/A"),
                Objects.toString((applicationTypeId != null && applicationTypeId > 0 ? applicationTypeId : "N/A"), "N/A"),
                Objects.toString((typeId != null && typeId > 0 ? typeId : "N/A"), "N/A"),
                Objects.toString((amountTypeId != null && amountTypeId > 0 ? amountTypeId : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(name) ? name : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(type) ? type : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(applicationType) ? applicationType : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(amountType) ? amountType : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(shortName) ? shortName : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(taxIds) ? taxIds : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(description) ? description : "N/A"), "N/A"),
                Objects.toString((includeRewards != null ? includeRewards : "N/A"), "N/A"),
                Objects.toString((includeItemDiscounts != null ? includeItemDiscounts : "N/A"), "N/A"),
                Objects.toString((includeSubtotalDiscounts != null ? includeSubtotalDiscounts : "N/A"), "N/A"),
                Objects.toString((includeTaxes != null ? includeTaxes : "N/A"), "N/A"),
                Objects.toString((open != null ? open : "N/A"), "N/A"),
                Objects.toString((preset != null ? preset : "N/A"), "N/A"),
                Objects.toString((refundsWithProducts != null ? refundsWithProducts : "N/A"), "N/A"),
                Objects.toString((useAsConvenienceFee != null ? useAsConvenienceFee : "N/A"), "N/A"),
                Objects.toString((eligibleForSubtotalSurcharge != null ? eligibleForSubtotalSurcharge : "N/A"), "N/A"),
                Objects.toString((amount != null ? amount.toPlainString() : "N/A"), "N/A"),
                Objects.toString((linkedLineItemId != null && linkedLineItemId > 0 ? linkedLineItemId : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(taxesStr) ? taxesStr : "N/A"), "N/A"));

    }

}
