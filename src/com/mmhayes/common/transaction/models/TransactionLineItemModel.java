package com.mmhayes.common.transaction.models;

//MMHayes dependencies
import com.mmhayes.common.api.errorhandling.exceptions.MissingDataException;

//API dependencies

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.mmhayes.common.api.CommonAPI;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionDeserializer;

//other dependencies
import java.util.HashMap;
import io.swagger.annotations.*;
import java.math.BigDecimal;

/*
 Last Updated (automatically updated by SVN)
 $Author: nyu $: Author of last commit
 $Date: 2021-08-17 18:18:58 -0400 (Tue, 17 Aug 2021) $: Date of last commit
 $Rev: 14978 $: Revision of last commit
*/
@JsonPropertyOrder({"id", "itemTypeId", "employeeId", "quantity", "amount", "extendedAmount", "refundedQty", "refundedAmount", "refundedExtAmount", "eligibleAmount", "itemComment", "badgeAssignmentId", "itemId", "transactionItemNum"})
public abstract class TransactionLineItemModel {
    private Integer id = null;
    private Integer itemId = null;
    private Integer employeeId = null;
    private Integer transactionId = null;
    protected Integer itemTypeId = null;
    private Integer linkedTransLineItemId = null;
    private Integer transactionItemNum = null;

    private String itemComment = "";
    private String originalOrderNumber = "";

    private BigDecimal quantity = null;
    private BigDecimal amount = BigDecimal.ZERO;
    private BigDecimal extendedAmount = null;  //leave extended amount at null so it is calculated
    private BigDecimal refundedAmount = null;
    private BigDecimal refundedExtAmount = null;  //leave extended amount at null so it is calculated
    private BigDecimal refundedQty = null;
    private BigDecimal eligibleAmount = BigDecimal.ZERO;

    //badgeAssignmentId links to the QC_BadgeAssignment table, then to the QC_Badge table.
    //QC_Badge.BadgeNumber is the actual Badge Alias
    private Integer badgeAssignmentId = null;

    //map common qc pos transaction line item fields
    public void setModelProperties(HashMap modelDetailHM) throws Exception {

        this.setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));
        this.setItemTypeId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAITEMTYPEID")));
        this.setTransactionId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("TRANSACTIONID")));
        this.setAmount(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("AMOUNT")));
        this.setExtendedAmount(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("EXTENDEDAMOUNT")));
        this.setQuantity(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("QUANTITY")));
        this.setEmployeeId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("EMPLOYEEID")));
        this.setItemComment(CommonAPI.convertModelDetailToString(modelDetailHM.get("ITEMCOMMENT")));
        this.setRefundedAmount(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("REFUNDEDAMOUNT")));
        this.setRefundedQty(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("REFUNDEDQUANTITY")));
        this.setRefundedExtAmount(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("REFUNDEDEXTAMOUNT")));
        this.setEligibleAmount(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("ELIGIBLEAMT")));
        this.setBadgeAssignmentId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("BADGEASSIGNMENTID")));
        this.setItemId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAITEMID")));
        this.setOriginalOrderNumber(CommonAPI.convertModelDetailToString(modelDetailHM.get("ORIGINALORDERNUM")));
        this.setLinkedTransLineItemId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("LINKEDPATRANSLINEITEMID")));
    }

    //@JsonIgnore
    @ApiModelProperty(value = "Transaction Line Item ID.", dataType = "Integer", required = false,
            notes = "Relates to the QC_PATransLineItem.PATransLineItemID table and field.")
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @JsonIgnore
    //This is used for fetching the Item ID from the DB
    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    //@JsonIgnore
    //used for tax deletes, and to save a specific trans line item type
    //04/10/2017 egl - this is required by QCPOS
    public Integer getItemTypeId() {
        return itemTypeId;
    }

    public void setItemTypeId(Integer itemTypeId) {
        this.itemTypeId = itemTypeId;
    }

    @ApiModelProperty(value = "Employee ID for Trans Line Item.", dataType = "Integer", required = true,
            notes = "Relates to the QC_PATransLineItem.EmployeeID table and field.")
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    @JsonIgnore
    public Integer getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Integer transactionId) {
        this.transactionId = transactionId;
    }

    @ApiModelProperty(value = "Transaction line item quantity.", dataType = "java.math.BigDecimal", required = true,
            notes = "Relates to the QC_PATransLineItem.Quantity table and field.")
    public BigDecimal getQuantity() {
        //set a default value of quantity
        if (quantity == null) {
            quantity = BigDecimal.ONE;
        }

        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    @ApiModelProperty(value = "Transaction line item amount.", dataType = "java.math.BigDecimal", required = true,
            notes = "Relates to the QC_PATransLineItem.Amount table and field.")
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getExtendedAmount() {
        if (extendedAmount == null) {
            if (this.getQuantity() != null && amount != null) {
                extendedAmount = amount.multiply(quantity);
            }
        }

        return extendedAmount;
    }

    public void setExtendedAmount(BigDecimal extendedAmount) {
        this.extendedAmount = extendedAmount;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getItemComment() {
        return itemComment;
    }

    public void setItemComment(String itemComment) {
        this.itemComment = itemComment;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public BigDecimal getRefundedAmount() {
        return refundedAmount;
    }

    public void setRefundedAmount(BigDecimal refundedAmount) {
        this.refundedAmount = refundedAmount;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public BigDecimal getRefundedQty() {
        return refundedQty;
    }

    public void setRefundedQty(BigDecimal refundedQty) {
        this.refundedQty = refundedQty;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public BigDecimal getRefundedExtAmount() {
        return refundedExtAmount;
    }

    public void setRefundedExtAmount(BigDecimal refundedExtAmount) {
        this.refundedExtAmount = refundedExtAmount;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public BigDecimal getEligibleAmount() {
        return eligibleAmount;
    }

    public void setEligibleAmount(BigDecimal eligibleAmount) {
        this.eligibleAmount = eligibleAmount;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getBadgeAssignmentId() {
        return badgeAssignmentId;
    }

    public void setBadgeAssignmentId(Integer badgeAssignmentId) {
        this.badgeAssignmentId = badgeAssignmentId;
    }

    public void refreshExtendedAmount() {
        if (this.getQuantity() != null && amount != null) {
            extendedAmount = amount.multiply(quantity);
        }
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getOriginalOrderNumber() {
        return originalOrderNumber;
    }

    public void setOriginalOrderNumber(String originalOrderNumber) {
        this.originalOrderNumber = originalOrderNumber;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getLinkedTransLineItemId() {
        return linkedTransLineItemId;
    }

    public void setLinkedTransLineItemId(Integer linkedTransLineItemId) {
        this.linkedTransLineItemId = linkedTransLineItemId;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    //If this field is not passed into the API, it acts as and index set by the API
    //This can be set by a client and passed into the API
    public Integer getTransactionItemNum() {
        return transactionItemNum;
    }

    public void setTransactionItemNum(Integer transactionItemNum) {
        this.transactionItemNum = transactionItemNum;
    }

    @JsonIgnore
    public void checkAndSetTransactionItemNum(Integer transactionItemNum) throws Exception {
        if (this.getTransactionItemNum() == null){
            this.setTransactionItemNum(transactionItemNum);
        }

        checkTransactionItemNum(this.getTransactionItemNum());
    }

    private void checkTransactionItemNum(Integer transactionItemNum) throws Exception {
        if (transactionItemNum == null){
            throw new MissingDataException("Invalid Transaction Item Num submitted.");
        }
    }
}