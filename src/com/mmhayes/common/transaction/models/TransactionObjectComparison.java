package com.mmhayes.common.transaction.models;

//MMHayes Dependencies
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.api.errorhandling.exceptions.MissingDataException;
import com.mmhayes.common.product.models.*;

//Other Dependencies
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

/*
 $Author: gematuszyk $: Author of last commit
 $Date: 2019-04-12 16:39:46 -0400 (Fri, 12 Apr 2019) $: Date of last commit
 $Rev: 8716 $: Revision of last commit
 Notes:
*/
public class TransactionObjectComparison {

    TransactionModel transactionModel = null;
    List<TransactionObjectComparisonLine> comparisonList = new ArrayList<>();

    public TransactionObjectComparison() {

    }

    public TransactionObjectComparison(PosAPIHelper.ObjectType objectType) {

    }

    public TransactionObjectComparison(TransactionModel transactionModel) {
        this.transactionModel = transactionModel;

        this.populateTotals();
    }

    public TransactionObjectComparison(TransactionModel transactionModel, PosAPIHelper.ObjectType objectType) {
        this.transactionModel = transactionModel;

        this.populateTotals(objectType);
    }

    public List<TransactionObjectComparisonLine> getTransObjComparisonList() {
        return comparisonList;
    }

    public void setComparisonList(List<TransactionObjectComparisonLine> comparisonList) {
        this.comparisonList = comparisonList;
    }

    public Boolean objectExists(PosAPIHelper.ObjectType objectType, Integer objectId) {
        Boolean exists = false;

        for (TransactionObjectComparisonLine transObjectComparisonLine : this.getTransObjComparisonList()) {
            if (transObjectComparisonLine.getObjectType() != null && transObjectComparisonLine.getObjectType().equals(objectType)) {
                if (transObjectComparisonLine.getObjectId().equals(objectId)) {
                    exists = true;
                }
            }
        }

        return exists;
    }

    public void addObjectComparisonLineInfo(PosAPIHelper.ObjectType objectType, Integer objectId, BigDecimal transAmount, BigDecimal prodAmount) {
        Boolean exists = false;

        for (TransactionObjectComparisonLine transObjectComparisonLine : this.getTransObjComparisonList()) {
            if (transObjectComparisonLine.getObjectType() != null && transObjectComparisonLine.getObjectType().equals(objectType)) {
                if (transObjectComparisonLine.getObjectId().equals(objectId)) {
                    transObjectComparisonLine.setTransactionTotal(transObjectComparisonLine.getTransactionTotal().add(transAmount));
                    transObjectComparisonLine.setProductTotal(transObjectComparisonLine.getProductTotal().add(prodAmount));
                    exists = true;
                }
            }
        }

        if (!exists) {
            TransactionObjectComparisonLine transObjectComparisonLine = new TransactionObjectComparisonLine(objectType, objectId, transAmount, prodAmount);
            this.getTransObjComparisonList().add(transObjectComparisonLine);
        }
    }

    public void validate() throws Exception {
        for (TransactionObjectComparisonLine transObjectComparisonLine : this.getTransObjComparisonList()) {
            if (transObjectComparisonLine.getTransactionTotal().setScale(2, RoundingMode.HALF_UP).compareTo(transObjectComparisonLine.getProductTotal().setScale(2, RoundingMode.HALF_UP)) != 0) {
                throw new MissingDataException("Transaction Line Items don't add up with product Item details");
            }
        }
    }

    //Only validate the balances for one object type
    public void validate(PosAPIHelper.ObjectType objectType) throws Exception {
        for (TransactionObjectComparisonLine transObjectComparisonLine : this.getTransObjComparisonList()) {
            if (transObjectComparisonLine.getObjectType().equals(objectType)) {
                if (transObjectComparisonLine.getTransactionTotal().setScale(2, RoundingMode.HALF_UP).compareTo(transObjectComparisonLine.getProductTotal().setScale(2, RoundingMode.HALF_UP)) != 0) {
                    throw new MissingDataException("Line Items don't add up with product details");
                }
            }
        }
    }

    //Only validate the balances for one object type and one Id
    public void validate(PosAPIHelper.ObjectType objectType, Integer objectId) throws Exception {
        for (TransactionObjectComparisonLine transObjectComparisonLine : this.getTransObjComparisonList()) {
            if (transObjectComparisonLine.getObjectType().equals(objectType) && transObjectComparisonLine.getObjectId().equals(objectId)) {
                if (transObjectComparisonLine.getTransactionTotal().setScale(2, RoundingMode.HALF_UP).compareTo(transObjectComparisonLine.getProductTotal().setScale(2, RoundingMode.HALF_UP)) != 0) {
                    throw new MissingDataException("Line Items don't add up with product details");
                }
            }
        }
    }

    private void populateTotals() {


        //Discount
        for (DiscountLineItemModel discountLineItemModel : transactionModel.getDiscounts()) {
            this.addObjectComparisonLineInfo(PosAPIHelper.ObjectType.POSDISCOUNT, discountLineItemModel.getDiscount().getId(), discountLineItemModel.getExtendedAmount().setScale(2, RoundingMode.HALF_UP), BigDecimal.ZERO);
        }

        //Taxes
        for (TaxLineItemModel taxLineItemModel : transactionModel.getTaxes()) {
            this.addObjectComparisonLineInfo(PosAPIHelper.ObjectType.TAX, taxLineItemModel.getTax().getId(), taxLineItemModel.getExtendedAmount().setScale(2, RoundingMode.HALF_UP), BigDecimal.ZERO);
        }

        //Rewards
        for (LoyaltyRewardLineItemModel loyaltyRewardLineItemModel : transactionModel.getRewards()) {
            this.addObjectComparisonLineInfo(PosAPIHelper.ObjectType.LOYALTYREWARD, loyaltyRewardLineItemModel.getReward().getId(), loyaltyRewardLineItemModel.getExtendedAmount().setScale(2, RoundingMode.HALF_UP), BigDecimal.ZERO);
        }

        //Surcharges
        for (SurchargeLineItemModel surchargeLineItemModel : transactionModel.getSurcharges()) {
            this.addObjectComparisonLineInfo(PosAPIHelper.ObjectType.SURCHARGE, surchargeLineItemModel.getSurcharge().getId(), surchargeLineItemModel.getExtendedAmount().setScale(2, RoundingMode.HALF_UP), BigDecimal.ZERO);
        }

        for (ProductLineItemModel productLineItemModel : transactionModel.getProducts()) {

            //Discounts
            for (ItemDiscountModel itemDiscountModel : productLineItemModel.getDiscounts()) {
                this.addObjectComparisonLineInfo(PosAPIHelper.ObjectType.POSDISCOUNT, itemDiscountModel.getDiscount().getId(), BigDecimal.ZERO, itemDiscountModel.getAmount().setScale(2, RoundingMode.HALF_UP));
            }

            //Taxes
            for (ItemTaxModel itemTaxModel : productLineItemModel.getTaxes()) {
                this.addObjectComparisonLineInfo(PosAPIHelper.ObjectType.TAX, itemTaxModel.getTax().getId(), BigDecimal.ZERO, itemTaxModel.getAmount().setScale(2, RoundingMode.HALF_UP));
            }

            //Rewards
            for (ItemRewardModel itemRewardModel : productLineItemModel.getRewards()) {
                this.addObjectComparisonLineInfo(PosAPIHelper.ObjectType.LOYALTYREWARD, itemRewardModel.getReward().getId(), BigDecimal.ZERO, itemRewardModel.getAmount().setScale(2, RoundingMode.HALF_UP));
            }

            //Surcharges
            for (ItemSurchargeModel itemSurchargeModel : productLineItemModel.getSurcharges()) {
                this.addObjectComparisonLineInfo(PosAPIHelper.ObjectType.SURCHARGE, itemSurchargeModel.getSurcharge().getId(), BigDecimal.ZERO, itemSurchargeModel.getAmount().setScale(2, RoundingMode.HALF_UP));
            }
        }
    }

    private void populateTotals(PosAPIHelper.ObjectType objectType) {


        switch (objectType) {

            case POSDISCOUNT:
                //Discount
                for (DiscountLineItemModel discountLineItemModel : transactionModel.getDiscounts()) {
                    this.addObjectComparisonLineInfo(PosAPIHelper.ObjectType.POSDISCOUNT, discountLineItemModel.getDiscount().getId(), discountLineItemModel.getExtendedAmount().setScale(2, RoundingMode.HALF_UP), BigDecimal.ZERO);
                }
                break;
            case TAX:
                //Taxes
                for (TaxLineItemModel taxLineItemModel : transactionModel.getTaxes()) {
                    this.addObjectComparisonLineInfo(PosAPIHelper.ObjectType.TAX, taxLineItemModel.getTax().getId(), taxLineItemModel.getExtendedAmount().setScale(2, RoundingMode.HALF_UP), BigDecimal.ZERO);
                }
                break;
            case LOYALTYREWARD:
                //Rewards
                for (LoyaltyRewardLineItemModel loyaltyRewardLineItemModel : transactionModel.getRewards()) {
                    this.addObjectComparisonLineInfo(PosAPIHelper.ObjectType.LOYALTYREWARD, loyaltyRewardLineItemModel.getReward().getId(), loyaltyRewardLineItemModel.getExtendedAmount().setScale(2, RoundingMode.HALF_UP), BigDecimal.ZERO);
                }
                break;

            case SURCHARGE:
                //Surcharges
                for (SurchargeLineItemModel surchargeLineItemModel : transactionModel.getSurcharges()) {
                    if(surchargeLineItemModel.getSurcharge().getRefundsWithProducts()) {
                        this.addObjectComparisonLineInfo(PosAPIHelper.ObjectType.SURCHARGE, surchargeLineItemModel.getSurcharge().getId(), surchargeLineItemModel.getExtendedAmount().setScale(2, RoundingMode.HALF_UP), BigDecimal.ZERO);
                    }
                }
                break;

        }


        for (ProductLineItemModel productLineItemModel : transactionModel.getProducts()) {

            switch (objectType) {
                case POSDISCOUNT:
                    //Discounts
                    for (ItemDiscountModel itemDiscountModel : productLineItemModel.getDiscounts()) {
                        this.addObjectComparisonLineInfo(PosAPIHelper.ObjectType.POSDISCOUNT, itemDiscountModel.getDiscount().getId(), BigDecimal.ZERO, itemDiscountModel.getAmount().setScale(2, RoundingMode.HALF_UP));
                    }
                    break;
                case TAX:
                    //Taxes
                    for (ItemTaxModel itemTaxModel : productLineItemModel.getTaxes()) {
                        this.addObjectComparisonLineInfo(PosAPIHelper.ObjectType.TAX, itemTaxModel.getTax().getId(), BigDecimal.ZERO, itemTaxModel.getAmount().setScale(2, RoundingMode.HALF_UP));
                    }
                    break;
                case LOYALTYREWARD:
                    //Rewards
                    for (ItemRewardModel itemRewardModel : productLineItemModel.getRewards()) {
                        this.addObjectComparisonLineInfo(PosAPIHelper.ObjectType.LOYALTYREWARD, itemRewardModel.getReward().getId(), BigDecimal.ZERO, itemRewardModel.getAmount().setScale(2, RoundingMode.HALF_UP));
                    }
                    break;
                case SURCHARGE:
                    //Surcharges
                    for (ItemSurchargeModel itemSurchargeModel : productLineItemModel.getSurcharges()) {
                        this.addObjectComparisonLineInfo(PosAPIHelper.ObjectType.SURCHARGE, itemSurchargeModel.getSurcharge().getId(), BigDecimal.ZERO, itemSurchargeModel.getAmount().setScale(2, RoundingMode.HALF_UP));
                    }
                    break;
            }
        }
    }
}
