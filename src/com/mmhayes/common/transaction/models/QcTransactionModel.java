package com.mmhayes.common.transaction.models;

//mmhayes dependencies

import com.mmhayes.common.api.PosApi30.PosAPIHelper;

import com.mmhayes.common.dataaccess.DataManager;

//API dependencies
import com.fasterxml.jackson.annotation.JsonInclude;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.transaction.collections.QcTransactionLineItemCollection;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionDeserializer;

//other dependencies
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2019-07-02 08:12:14 -0400 (Tue, 02 Jul 2019) $: Date of last commit
 $Rev: 9024 $: Revision of last commit
*/
public class QcTransactionModel {

    private Integer id = null;
    private String timeStamp = null;
    private String transComment = "";
    private Integer userId = null;
    private Integer employeeId = null;
    private Integer paTransLineItemId = null;
    private Integer terminalId = null;
    private Integer payments = null;
    private List<QcTransactionLineItemModel> transactionDetails = new ArrayList<>();

    public QcTransactionModel() {

    }

    public QcTransactionModel(TransactionModel transactionModel, TenderLineItemModel tenderLineItemModel) {
        this.setTimeStamp(transactionModel.getTimeStamp());
        this.setTransComment(transactionModel.getTransComment());
        this.setUserId(transactionModel.getUserId());
        this.setEmployeeId(tenderLineItemModel.getAccount().getId());
        this.setTerminalId(transactionModel.getTerminal().getId());
        this.setPayments(tenderLineItemModel.getPayments());
    }

    public QcTransactionModel(TransactionModel transactionModel, ReceivedOnAccountLineItemModel receivedOnAccountLineItemModel) {
        this.setTimeStamp(transactionModel.getTimeStamp());
        this.setTransComment(transactionModel.getTransComment());
        this.setUserId(transactionModel.getUserId());
        this.setEmployeeId(receivedOnAccountLineItemModel.getAccount().getId());
        this.setTerminalId(transactionModel.getTerminal().getId());
        this.setPayments(1); //override payments to 1 for ROA transactions
    }

    //get one transaction from the database
    public QcTransactionModel getOneTransactionById(Integer _id, Integer terminalId) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> transactionList = dm.parameterizedExecuteQuery("data.posapi30.getOneQCTransactionById",
                new Object[]{_id},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        CommonAPI.checkIsNullOrEmptyList(transactionList, CommonAPI.PosModelType.TRANSACTION, "", terminalId);
        HashMap transactionHM = transactionList.get(0);
        CommonAPI.checkIsNullOrEmptyObject(transactionHM.get("ID"), CommonAPI.PosModelType.TRANSACTION, terminalId);
        setModelProperties(transactionHM, terminalId);

        return this;
    }

    //this is used to populate the Quickcharge transaction on TenderLineItems
    public QcTransactionModel getOneTransactionByTenderTransId(Integer _id, Integer terminalId) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> transactionList = dm.parameterizedExecuteQuery("data.posapi30.getOneQCTransactionByTenderLineId",
                new Object[]{_id},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        if (transactionList == null || transactionList.isEmpty()) {
            return null;
        } else {
            HashMap transactionHM = transactionList.get(0);
            CommonAPI.checkIsNullOrEmptyObject(transactionHM.get("ID"), CommonAPI.PosModelType.TRANSACTION, terminalId);
            setModelProperties(transactionHM, terminalId);
            return this;
        }
    }

    //used by the GET command
    private void setModelProperties(HashMap modelDetailHM, Integer terminalId) throws Exception {
        this.setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));
        this.setTimeStamp(CommonAPI.convertModelDetailToString(modelDetailHM.get("TRANSACTIONDATE")));
        this.setTerminalId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("TERMINALID")));
        this.setTransComment(CommonAPI.convertModelDetailToString(modelDetailHM.get("REFNUMBER")));
        this.setPayments(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("SPLITS")));
        this.setUserId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("USERID")));
        this.setEmployeeId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("EMPLOYEEID")));
        this.setPaTransLineItemId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PATRANSLINEITEMID")));

        this.setTransactionDetails(QcTransactionLineItemCollection.getQcTransactionLineItems(this.getId(), terminalId).getCollection());
    }

    public static QcTransactionModel createQcTransactionModel(HashMap modelDetailHM, Integer terminalId) throws Exception {
        QcTransactionModel qcTransactionModel = new QcTransactionModel();
        qcTransactionModel.setModelProperties(modelDetailHM, terminalId);
        return qcTransactionModel;
    }

    public static QcTransactionModel createQcTransactionModel(Integer qcTransactionId, Integer terminalId) throws Exception {
        QcTransactionModel qcTransactionModel = new QcTransactionModel();
        return qcTransactionModel.getOneTransactionById(qcTransactionId, terminalId);
    }

    //fetch the QCTransaction for a specified PATransLineItem
    public static QcTransactionModel createQcTransactionModel(TenderLineItemModel tenderLineItemModel, Integer terminalId) throws Exception {
        QcTransactionModel qcTransactionModel = new QcTransactionModel();
        return qcTransactionModel.getOneTransactionByTenderTransId(tenderLineItemModel.getId(), terminalId);
    }

    //fetch the QCTransaction for a specified ROA line item
    public static QcTransactionModel createQcTransactionModel(ReceivedOnAccountLineItemModel receivedOnAccountLineItemModel, Integer terminalId) throws Exception {
        QcTransactionModel qcTransactionModel = new QcTransactionModel();
        return qcTransactionModel.getOneTransactionByTenderTransId(receivedOnAccountLineItemModel.getId(), terminalId);
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getTransComment() {
        return transComment;
    }

    public void setTransComment(String transComment) {
        this.transComment = transComment;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getPaTransLineItemId() {
        return paTransLineItemId;
    }

    public void setPaTransLineItemId(Integer paTransLineItemId) {
        this.paTransLineItemId = paTransLineItemId;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(Integer terminalId) {
        this.terminalId = terminalId;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public List<QcTransactionLineItemModel> getTransactionDetails() {
        return transactionDetails;
    }

    public void setTransactionDetails(List<QcTransactionLineItemModel> transactionDetails) {
        this.transactionDetails = transactionDetails;
    }

    public Integer getPayments() {
        return payments;
    }

    public void setPayments(Integer payments) {
        this.payments = payments;
    }
}
