package com.mmhayes.common.transaction.models;

//MMHayes dependencies
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionDeserializer;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.product.models.PrepOptionModel;
import com.mmhayes.common.product.models.ProductModel;

//API dependencies
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
//other dependencies
import java.util.HashMap;

/*
 Last Updated (automatically updated by SVN)
 $Author: nyu $: Author of last commit
 $Date: 2021-08-17 14:37:51 -0400 (Tue, 17 Aug 2021) $: Date of last commit
 $Rev: 14929 $: Revision of last commit
*/
@JsonPropertyOrder({"id", "itemTypeId", "employeeId", "quantity", "amount", "extendedAmount", "refundedQty", "refundedAmount", "refundedExtAmount", "eligibleAmount", "itemComment", "badgeAssignmentId", "itemId", "keypadId", "transactionItemNum"})
public class ModifierLineItemModel extends ProductLineItemModel {

    private String linkedFromPaTransLineItemModIds = null;

    public ModifierLineItemModel() {

    }

    public static ModifierLineItemModel createModifierLineItemModel(HashMap modelDetailHM) throws Exception {
        ModifierLineItemModel modifierLineItemModel = new ModifierLineItemModel();
        modifierLineItemModel.setModelProperties(modelDetailHM);
        modifierLineItemModel.setModifierModelProperties(modelDetailHM);
        modifierLineItemModel.setPrepOption(PrepOptionModel.getPrepOptionModel(modelDetailHM));

        return modifierLineItemModel;
    }

    //map common qc pos transaction line item fields
    public void setModifierModelProperties(HashMap modelDetailHM) throws Exception {
        this.setKeypadId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAKEYPADID")));
        this.setParentPaTransLineItemId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PATRANSLINEITEMID")));
        this.setLinkedFromPaTransLineItemModIds(CommonAPI.convertModelDetailToString(modelDetailHM.get("LINKEDFROMPATRANSLINEITEMMODIDS")));
    }

    public
    @Override
    @JsonIgnore //not returning this for this transaction line item type.  This should be assumed I think.  Trying to clean up the json.
    Integer getItemTypeId() {
        return PosAPIHelper.ObjectType.PRODUCT.toInt();
    }

    @Override
    public ProductModel getProduct() {

        if (product != null){
            product.setIsModifier(true);
        }

        return product;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getLinkedFromPaTransLineItemModIds() {
        return linkedFromPaTransLineItemModIds;
    }

    public void setLinkedFromPaTransLineItemModIds(String linkedFromPaTransLineItemModIds) {
        this.linkedFromPaTransLineItemModIds = linkedFromPaTransLineItemModIds;
    }

    /**
     * Overridden toString method for a ModifierLineItemModel.
     * @return The {@link String} representation of a ModifierLineItemModel.
     */
    @Override
    public String toString () {
        return super.toString();
    }

}
