package com.mmhayes.common.transaction.models;

/**
 * Created with IntelliJ IDEA.
 * User: admin
 * Date: 8/3/17
 * Time: 9:20 AM
 * To change this template use File | Settings | File Templates.
 */
public class CashierShiftEndTransCount {

    private Integer objectTypeId = null;
    private Integer objectId = null;
    private Integer transCount = null;

    public Integer getObjectTypeId() {
        return objectTypeId;
    }

    public void setObjectTypeId(Integer objectTypeId) {
        this.objectTypeId = objectTypeId;
    }

    public Integer getObjectId() {
        return objectId;
    }

    public void setObjectId(Integer objectId) {
        this.objectId = objectId;
    }

    public Integer getTransCount() {
        return transCount;
    }

    public void setTransCount(Integer transCount) {
        this.transCount = transCount;
    }

    public CashierShiftEndTransCount() {


    }

    public static CashierShiftEndTransCount createCashierEndShiftTransCount(){

        CashierShiftEndTransCount cashierEndShiftTransCount = new CashierShiftEndTransCount();
        return cashierEndShiftTransCount;

    }
}
