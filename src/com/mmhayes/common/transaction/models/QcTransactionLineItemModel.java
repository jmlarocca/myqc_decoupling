package com.mmhayes.common.transaction.models;

//mmhayes dependencies

//API dependencies
import com.fasterxml.jackson.annotation.JsonInclude;
import com.mmhayes.common.api.CommonAPI;

//other dependencies
import java.math.BigDecimal;
import java.util.HashMap;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2017-04-20 16:34:33 -0400 (Thu, 20 Apr 2017) $: Date of last commit
 $Rev: 3849 $: Revision of last commit
*/
public class QcTransactionLineItemModel {

    private Integer id = null;
    private PurchaseCategoryModel purchaseCategory = null;
    //used by Third Party POS API
    private Integer posItemId = null;
    private BigDecimal amount = null;

   //constructor
    public QcTransactionLineItemModel(){
    }

    public QcTransactionLineItemModel(Integer posItemId, BigDecimal amount) {
        this.setAmount(amount);
        this.setPosItemId(posItemId);
        this.setPurchaseCategory(new PurchaseCategoryModel(posItemId));
    }

    public QcTransactionLineItemModel(Integer posItemId) {

        this.setPosItemId(posItemId);
        this.setPurchaseCategory(new PurchaseCategoryModel(posItemId));
    }

    private void setModelProperties(HashMap modelDetailHM, Integer terminalId) throws Exception {
        this.setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));
        this.setPosItemId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("POSITEMID")));
        this.setAmount(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("AMOUNT")));

        if (this.getPosItemId() != null && !this.getPosItemId().toString().isEmpty()){
            this.setPurchaseCategory(PurchaseCategoryModel.getPurchaseCategoryModel(this.getPosItemId(), terminalId));
        }
    }

    public static QcTransactionLineItemModel createQcTransactionLineItem(HashMap modelDetailHM, Integer terminalId) throws Exception {
        QcTransactionLineItemModel qcTransactionLineItemModel = new QcTransactionLineItemModel();
        qcTransactionLineItemModel.setModelProperties(modelDetailHM, terminalId);
        return qcTransactionLineItemModel;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public PurchaseCategoryModel getPurchaseCategory() {
        return purchaseCategory;
    }

    public void setPurchaseCategory(PurchaseCategoryModel purchaseCategory) {
        this.purchaseCategory = purchaseCategory;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getPosItemId() {
        return posItemId;
    }

    public void setPosItemId(Integer posItemId) {
        this.posItemId = posItemId;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

}
