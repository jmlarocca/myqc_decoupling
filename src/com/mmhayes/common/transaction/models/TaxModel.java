package com.mmhayes.common.transaction.models;

//mmhayes dependencies

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.api.PosApi30.PosAPIModelCache;
import com.mmhayes.common.api.errorhandling.exceptions.MissingDataException;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionDeserializer;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.utils.StringFunctions;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

//API dependencies
//other dependencies

/*
 Last Updated (automatically updated by SVN)
 $Author: jkflanagan $: Author of last commit
 $Date: 2020-12-31 16:39:34 -0500 (Thu, 31 Dec 2020) $: Date of last commit
 $Rev: 13381 $: Revision of last commit
*/
@JsonPropertyOrder({"id", "name", "taxRate"})
public class TaxModel {
    Integer id = null;
    String name = "";
    String shortName = "";



    BigDecimal taxRate = null;
    Integer mappedPosItem = null;
    boolean isRevCenterMapped = false;

    //default constructor
    public TaxModel() {
    }

    //Constructor- takes in a Hashmap
    public TaxModel(HashMap TaxHM) {
        setModelProperties(TaxHM);
    }

    //setter for all of this model's properties
    public TaxModel setModelProperties(HashMap modelDetailHM) {
        setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));
        setName(CommonAPI.convertModelDetailToString(modelDetailHM.get("NAME")));
        setTaxRate(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("TAXRATE")));
        setShortName(CommonAPI.convertModelDetailToString(modelDetailHM.get("SHORTNAME")));

        if (modelDetailHM.get("REVCENTERISMAPPED") != null && modelDetailHM.get("REVCENTERISMAPPED").toString().equals("1")){
            setRevCenterMapped(true);
        }

        return this;
    }

    public static TaxModel getTaxModel(Integer taxId, Integer terminalId) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> taxList = dm.parameterizedExecuteQuery("data.posapi30.getOneTaxById",
                new Object[]{taxId, terminalId},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        //product list is null or empty, throw exception
        CommonAPI.checkIsNullOrEmptyList(taxList, "Tax Not Found", terminalId);
        return TaxModel.createTaxModel(taxList.get(0));

    }

    public static TaxModel getTaxModelFromCache(Integer taxId, Integer terminalId, boolean enforceRevenueCenterMapping) throws Exception {
        TaxModel taxModel = (TaxModel) PosAPIModelCache.getOneObject(terminalId, CommonAPI.PosModelType.TAX, taxId);

        if (taxModel == null){
            taxModel = TaxModel.getTaxModel(taxId, terminalId);
        }

        if (enforceRevenueCenterMapping){
            if (!taxModel.isRevCenterMapped()){
                throw new MissingDataException("Tax Not Found", terminalId); //Use the same exception that was used previously
            }
        }

        return taxModel;
    }

    public static TaxModel getTaxModel(String taxName, Integer terminalId) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> taxList = dm.parameterizedExecuteQuery("data.posapi30.getOneTaxByName",
                new Object[]{taxName},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        //product list is null or empty, throw exception
        CommonAPI.checkIsNullOrEmptyList(taxList, "Tax Not Found", terminalId);
        return TaxModel.createTaxModel(taxList.get(0));

    }

    public static TaxModel getTaxModelWithRevenueCenter(final Integer taxId, final Integer revenueCenter, final Integer terminalId) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> taxList = dm.parameterizedExecuteQuery("data.posapi30.getOneTaxByIdAndRevenueCenter",
                new Object[]{taxId, revenueCenter},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        //product list is null or empty, throw exception
        CommonAPI.checkIsNullOrEmptyList(taxList, "Tax Not Found", terminalId);
        return TaxModel.createTaxModel(taxList.get(0));

    }

    public static TaxModel getTaxModelWithRevenueCenter(final String taxName, final Integer revenueCenter, final Integer terminalId) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> taxList = dm.parameterizedExecuteQuery("data.posapi30.getOneTaxByNameAndRevenueCenter",
                new Object[]{taxName, revenueCenter},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        //product list is null or empty, throw exception
        CommonAPI.checkIsNullOrEmptyList(taxList, "Tax Not Found", terminalId);
        return TaxModel.createTaxModel(taxList.get(0));

    }

    public static TaxModel createTaxModel(HashMap modelHM) {
        return new TaxModel(modelHM);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ApiModelProperty(value = "Tax Name.", dataType = "String", required=true,
            example = "Hospital Tax.",
            notes= "Relates to the QC_PATaxRate.Name table and field.")
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public BigDecimal getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(BigDecimal taxRate) {
        this.taxRate = taxRate;
    }

    @JsonIgnore
    public Integer getMappedPosItem() {
        return mappedPosItem;
    }

    public void setMappedPosItem(Integer mappedPosItem) {
        this.mappedPosItem = mappedPosItem;
    }

    @JsonIgnore
    public boolean isRevCenterMapped() {
        return isRevCenterMapped;
    }

    public void setRevCenterMapped(boolean revCenterMapped) {
        isRevCenterMapped = revCenterMapped;
    }

    /**
     * Overridden toString method for a TaxModel.
     * @return The {@link String} representation of a TaxModel.
     */
    @Override
    public String toString () {

        return String.format("ID: %s, NAME: %s, SHORTNAME: %s, TAXRATE: %s, MAPPEDPOSITEM: %s, ISREVCENTERMAPPED: %s",
                Objects.toString((id != null && id > 0 ? id : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(name) ? name : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(shortName) ? shortName : "N/A"), "N/A"),
                Objects.toString((taxRate != null ? taxRate.toPlainString() : "N/A"), "N/A"),
                Objects.toString((mappedPosItem != null && mappedPosItem > 0 ? mappedPosItem : "N/A"), "N/A"),
                Objects.toString(isRevCenterMapped));

    }

}