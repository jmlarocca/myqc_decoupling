package com.mmhayes.common.transaction.models;

//mmhayes dependencies

import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.terminal.models.TerminalModel;

//API dependencies
import com.mmhayes.common.api.CommonAPI;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionDeserializer;

//other dependencies
import java.util.ArrayList;
import java.util.HashMap;

/*
 $Author: eglundin $: Author of last commit
 $Date: 2019-07-02 08:12:14 -0400 (Tue, 02 Jul 2019) $: Date of last commit
 $Rev: 9024 $: Revision of last commit
 Notes:
*/
public class ReceivedOnAccountModel {
    private Integer id = null;
    private String name = "";
    private boolean isQC = false;

    public ReceivedOnAccountModel(){

    }

    //Constructor- takes in a Hashmap
    public ReceivedOnAccountModel(HashMap modelHM) {
        setModelProperties(modelHM);
    }

    //setter for all of this model's properties
    public ReceivedOnAccountModel setModelProperties(HashMap modelDetailHM) {
        setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));
        setName(CommonAPI.convertModelDetailToString(modelDetailHM.get("NAME")));
        setQC(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("ISQC")));

        return this;
    }

    public static ReceivedOnAccountModel getReceivedOnAccountModel(Integer receivedOnAccountId, Integer terminalId) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> receivedOnAccountList = dm.parameterizedExecuteQuery("data.posapi30.getOneReceivedOnAccountById",
                new Object[]{ receivedOnAccountId },
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        //product list is null or empty, throw exception
        CommonAPI.checkIsNullOrEmptyList(receivedOnAccountList, "Received On Account Not Found", terminalId);
        return ReceivedOnAccountModel.createReceivedOnAccountModel(receivedOnAccountList.get(0));

    }

    public static ReceivedOnAccountModel getReceivedOnAccountModel(String receivedOnAccountName, Integer terminalId) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> receivedOnAccountList = dm.parameterizedExecuteQuery("data.posapi30.getOneReceivedOnAccountByName",
                new Object[]{ receivedOnAccountName },
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        //product list is null or empty, throw exception
        CommonAPI.checkIsNullOrEmptyList(receivedOnAccountList, "Received On Account Not Found", terminalId);
        return ReceivedOnAccountModel.createReceivedOnAccountModel(receivedOnAccountList.get(0));

    }

    public static ReceivedOnAccountModel getActiveReceivedOnAccountModel(Integer receivedOnAccountId, TerminalModel terminalModel) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> receivedOnAccountList = dm.parameterizedExecuteQuery("data.posapi30.getOneReceivedOnAccountByIdAndRevenueCenter",
                new Object[]{ receivedOnAccountId, terminalModel.getRevenueCenterId() },
                PosAPIHelper.getLogFileName(terminalModel.getId()),
                true
        );

        //product list is null or empty, throw exception
        CommonAPI.checkIsNullOrEmptyList(receivedOnAccountList, "Received On Account Not Found", terminalModel.getId());
        return ReceivedOnAccountModel.createReceivedOnAccountModel(receivedOnAccountList.get(0));

    }

    public static ReceivedOnAccountModel getActiveReceivedOnAccountModel(String receivedOnAccountName, TerminalModel terminalModel) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> receivedOnAccountList = dm.parameterizedExecuteQuery("data.posapi30.getOneReceivedOnAccountByNameAndRevenueCenter",
                new Object[]{ receivedOnAccountName, terminalModel.getRevenueCenterId() },
                PosAPIHelper.getLogFileName(terminalModel.getId()),
                true
        );

        //product list is null or empty, throw exception
        CommonAPI.checkIsNullOrEmptyList(receivedOnAccountList, "Received On Account Not Found", terminalModel.getId());
        return ReceivedOnAccountModel.createReceivedOnAccountModel(receivedOnAccountList.get(0));

    }

    public static ReceivedOnAccountModel createReceivedOnAccountModel(HashMap modelHM) {
        return new ReceivedOnAccountModel(modelHM);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonIgnore
    public boolean isQC() {
        return isQC;
    }

    public void setQC(boolean QC) {
        isQC = QC;
    }

}