package com.mmhayes.common.transaction.models;

//mmhayes dependencies
import com.fasterxml.jackson.annotation.JsonInclude;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.dataaccess.DataManager;

//API dependencies
import com.mmhayes.common.api.CommonAPI;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionDeserializer;

//other dependencies
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;

/*
 Last Updated (automatically updated by SVN)
 $Author: nyu $: Author of last commit
 $Date: 2021-08-16 18:29:13 -0400 (Mon, 16 Aug 2021) $: Date of last commit
 $Rev: 14886 $: Revision of last commit
*/
public class QcDiscountModel {
    private Integer id = null;
    private Integer paDiscountId = null;
    private Integer discountTypeId = null;
    private String name = "";
    private String discountType = "";
    private BigDecimal amount = null;
    private BigDecimal amountOriginal = null;

    private String startTime = null;
    private String endTime = null;
    private boolean singleUse = false;
    private LocalDateTime resetDTM = null;

    public QcDiscountModel(){

    }

    public QcDiscountModel(HashMap modelDetailHM){
        setModelProperties(modelDetailHM);
    }

    //setter for all of this model's properties
    public QcDiscountModel setModelProperties(HashMap modelDetailHM) {
        setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));
        setName(CommonAPI.convertModelDetailToString(modelDetailHM.get("NAME")));
        setDiscountType(CommonAPI.convertModelDetailToString(modelDetailHM.get("DISCOUNTTYPENAME")));
        setAmount(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("AMOUNT")));
        setAmountOriginal(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("AMOUNT")));
        setPaDiscountId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PADISCOUNTID")));
        setDiscountTypeId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("DISCOUNTTYPEID")));
        setStartTime(CommonAPI.convertModelDetailToString(modelDetailHM.get("STARTTIME")));
        setEndTime(CommonAPI.convertModelDetailToString(modelDetailHM.get("ENDTIME")));
        setSingleUse(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("SINGLETRANSACTION"), false));

        safelySetResetDtm(modelDetailHM);

        return this;
    }

    public static QcDiscountModel getQcDiscountModel(Integer qcDiscountId, Integer terminalId) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> qcDiscountList = dm.parameterizedExecuteQuery("data.posapi30.getOneQCDiscountById",
                new Object[]{qcDiscountId},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        //product list is null or empty, throw exception
        CommonAPI.checkIsNullOrEmptyList(qcDiscountList, "QC Discount Not Found", terminalId);
        return QcDiscountModel.createQcDiscountModel(qcDiscountList.get(0));

    }

    public static QcDiscountModel getQcDiscountModel(String qcDiscountName, Integer terminalId) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> qcDiscountList = dm.parameterizedExecuteQuery("data.posapi30.getOneQCDiscountByName",
                new Object[]{qcDiscountName},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        //product list is null or empty, throw exception
        CommonAPI.checkIsNullOrEmptyList(qcDiscountList, "QC Discount Not Found", terminalId);
        return QcDiscountModel.createQcDiscountModel(qcDiscountList.get(0));

    }

    public static QcDiscountModel createQcDiscountModel(HashMap qcDiscountHM) {
        return new QcDiscountModel(qcDiscountHM);
    }


    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getDiscountType() {
        return discountType;
    }

    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    @JsonIgnore
    public Integer getPaDiscountId() {
        return paDiscountId;
    }

    public void setPaDiscountId(Integer paDiscountId) {
        this.paDiscountId = paDiscountId;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getDiscountTypeId() {
        return discountTypeId;
    }

    public void setDiscountTypeId(Integer discountTypeId) {
        this.discountTypeId = discountTypeId;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public boolean isSingleUse() {
        return singleUse;
    }

    public void setSingleUse(boolean singleUse) {
        this.singleUse = singleUse;
    }

    @JsonIgnore
    public LocalDateTime getResetDTM() {
        return resetDTM;
    }

    public void setResetDTM(LocalDateTime resetDTM) {
        this.resetDTM = resetDTM;
    }

    public void safelySetResetDtm(HashMap modelDetailHM) {
        try {
            String reset = (CommonAPI.convertModelDetailToString(modelDetailHM.get("RESETDTM"))).substring(0, 19);
            LocalDateTime resetDTM = LocalDateTime.parse(reset, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
            setResetDTM(resetDTM);
        } catch (Exception ex) {
            //If this errors, leave it at null
        }
    }

    @JsonIgnore
    public BigDecimal getAmountOriginal() {
        return amountOriginal;
    }

    public void setAmountOriginal(BigDecimal amountOriginal) {
        this.amountOriginal = amountOriginal;
    }
}
