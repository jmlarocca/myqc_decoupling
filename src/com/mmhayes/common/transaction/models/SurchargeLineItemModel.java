package com.mmhayes.common.transaction.models;

//API Dependencies
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.product.models.ItemTaxModel;

//Other dependencies

import java.util.ArrayList;
import java.util.HashMap;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2019-07-02 08:12:14 -0400 (Tue, 02 Jul 2019) $: Date of last commit
 $Rev: 9024 $: Revision of last commit
*/
public class SurchargeLineItemModel extends TransactionLineItemModel {
    private SurchargeDisplayModel surcharge = null;
    private ArrayList<ItemTaxModel> taxes = new ArrayList<>();

    public SurchargeLineItemModel() {
        super();
    }

    public static SurchargeLineItemModel createSurchargeLineItemModel(HashMap modelDetailHM) throws Exception {
        SurchargeLineItemModel surchargeLineItemModel = new SurchargeLineItemModel();
        surchargeLineItemModel.setModelProperties(modelDetailHM);

        return surchargeLineItemModel;
    }

    //@JsonIgnore
    public SurchargeDisplayModel getSurcharge() {
        return surcharge;
    }

    public void setSurcharge(SurchargeDisplayModel surcharge) {
        this.surcharge = surcharge;
    }

    @JsonIgnore
    //We need the ItemTypeId on the TaxLineItemModel due to Tax Deletes
    public
    @Override
    Integer getItemTypeId() {
        if (itemTypeId != null) {
            return itemTypeId;
        } else {
            return PosAPIHelper.ObjectType.SURCHARGE.toInt();
        }
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public ArrayList<ItemTaxModel> getTaxes() {
        return taxes;
    }

    public void setTaxes(ArrayList<ItemTaxModel> taxes) {
        this.taxes = taxes;
    }
}
