package com.mmhayes.common.transaction.models;

//mmhayes dependencies
import com.mmhayes.common.api.PosApi30.PosAPIHelper;

//API dependencies
import com.fasterxml.jackson.annotation.JsonIgnore;

//other dependencies
import java.util.HashMap;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2019-03-25 08:10:10 -0400 (Mon, 25 Mar 2019) $: Date of last commit
 $Rev: 8585 $: Revision of last commit
*/
public class GratuityLineItemModel extends TransactionLineItemModel {
    private GratuityModel gratuity = null;

    public GratuityLineItemModel() {
        super();
        setAmount(null);
    }

    public GratuityLineItemModel(HashMap modelDetailHM) throws Exception {
        this.setModelProperties(modelDetailHM);
    }

    //@JsonIgnore
    public GratuityModel getGratuity() {
        return gratuity;
    }

    public void setGratuity(GratuityModel gratuity) {
        this.gratuity = gratuity;
    }

    @JsonIgnore
    //We need the ItemTypeId on the TaxLineItemModel due to Tax Deletes
    public
    @Override
    Integer getItemTypeId() {
        if (itemTypeId != null) {
            return itemTypeId;
        } else {
            return PosAPIHelper.ObjectType.SURCHARGE.toInt();
        }
    }
}
