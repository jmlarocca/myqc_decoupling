package com.mmhayes.common.transaction.models;

//mmhayes dependencies
import com.mmhayes.common.api.PosApi30.PosAPIHelper;

//API dependencies
import com.fasterxml.jackson.annotation.JsonIgnore;

//other dependencies
import java.util.HashMap;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2019-03-25 08:10:10 -0400 (Mon, 25 Mar 2019) $: Date of last commit
 $Rev: 8585 $: Revision of last commit
*/
public class ServiceChargeLineItemModel extends TransactionLineItemModel {
    private ServiceChargeModel serviceCharge = null;

    public ServiceChargeLineItemModel() {
        super();
    }

    public ServiceChargeLineItemModel(HashMap modelDetailHM) throws Exception {
        this.setModelProperties(modelDetailHM);
    }

    //@JsonIgnore
    public ServiceChargeModel getServiceCharge() {
        return serviceCharge;
    }

    public void setServiceCharge(ServiceChargeModel serviceCharge) {
        this.serviceCharge = serviceCharge;
    }

    @JsonIgnore
    //We need the ItemTypeId on the TaxLineItemModel due to Tax Deletes
    public
    @Override
    Integer getItemTypeId() {
        if (itemTypeId != null) {
            return itemTypeId;
        } else {
            return PosAPIHelper.ObjectType.SURCHARGE.toInt();
        }
    }
}