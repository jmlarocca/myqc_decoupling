package com.mmhayes.common.transaction.models;

//mmhayes dependencies
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.receiptGen.TransactionData.Discount;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.voucher.models.DiscountInfoModel;
import com.sun.media.sound.InvalidDataException;
import com.mmhayes.common.product.collections.ProductCollectionCalculation;
import com.mmhayes.common.product.models.ItemDiscountModel;
import com.mmhayes.common.product.models.ProductCalculation;

//API dependencies
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.api.PosApi30.PosAPIHelper.DiscountApplicationResult;
import com.mmhayes.common.api.errorhandling.exceptions.MissingDataException;
import java.time.LocalDateTime;
import sun.nio.cs.ext.IBM037;

//other dependencies
import java.math.BigDecimal;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.math.RoundingMode;

import static com.mmhayes.common.api.PosApi30.PosAPIHelper.ApiActionType.REFUND_MANUAL;

/*
Last Updated (automatically updated by SVN)
$Author: eglundin $: Author of last commit
$Date: 2021-09-10 13:41:40 -0400 (Fri, 10 Sep 2021) $: Date of last commit
$Rev: 15343 $: Revision of last commit
*/

public class DiscountCalculation {
    private Integer id; //POS Discount ID
    private Integer QCDiscountId = null; //holds the corresponding QC discount ID if there is one
    private DiscountModel discountModel; //contains all of the discountProperties
    private QcDiscountModel qcDiscountModel; //contains all of the discountProperties
    private BigDecimal discountableAmount = BigDecimal.ZERO; //the total EXTENDED discountable for this discount (discountable amounts are ALWAYS EXTENDED amounts)
    private BigDecimal discountAmount = null; //the total discount for this discount - individual amount for item discounts and EXTENDED amount for subtotal discounts
    private BigDecimal discountQuantity = BigDecimal.ONE; //for item discounts, multiple of a discount
    private HashMap<Integer, BigDecimal> lineDiscounts = new HashMap<Integer, BigDecimal>(); //holds EXTENDED discountableAmounts for each index of productLine
    private HashMap<Integer, BigDecimal> lineQuantities = new HashMap<Integer, BigDecimal>(); //holds quantity discountableAmounts for each index of productLine
    private String taxIds = "";
    private int roundingMode = BigDecimal.ROUND_HALF_UP;
    private Integer employeeId = null; //if this discount needs specific employee considerations

    private boolean autoAppliedOnCurrentInquire = false;
    private DiscountLineItemModel discountLineItemModel = null;
    boolean allowDiscountAmountOverride = false;
    private DiscountApplicationResult applicationResult = DiscountApplicationResult.NOT_APPLIED_YET;
    private Integer transactionItemNum = null;

    //constructor
    public DiscountCalculation() throws Exception {
    }

    public DiscountCalculation(Integer discountId, Integer terminalId) throws Exception {
        this( discountId, terminalId, null );
    }

    public DiscountCalculation(Integer discountId, Integer terminalId, Integer employeeId) throws Exception {
        DiscountModel discountModel = DiscountModel.getOneDiscountModel(discountId, terminalId);
        this.setId(discountId);
        this.setDiscountModel(discountModel);
        this.setEmployeeId( employeeId );

        this.getDiscountModel().retrieveDiscountQuantityLevels(terminalId);
    }

    /**
     * Pass in a DiscountLineItemModel to override the Discount amount
     * @param discountLineItemModel
     * @param terminalId
     * @throws Exception
     */
    public DiscountCalculation(DiscountLineItemModel discountLineItemModel, Integer terminalId) throws Exception {
        this.setDiscountLineItemModel(discountLineItemModel);

        if (discountLineItemModel.getQcDiscount() != null){
            this.setQCDiscountId(discountLineItemModel.getQcDiscount().getId());
            this.setQcDiscountModel(discountLineItemModel.getQcDiscount());
        }

        this.setEmployeeId(discountLineItemModel.getEmployeeId());
        this.setAllowDiscountAmountOverride(true);
        this.setId(discountLineItemModel.getDiscount().getId());
        this.setTransactionItemNum(discountLineItemModel.getTransactionItemNum());
        this.setDiscountModel(discountLineItemModel.getDiscount());
        this.getDiscountModel().retrieveDiscountQuantityLevels(terminalId);
    }

    //gets the discountable amounts based on the given product collection and terminal, note that discountable amounts are ALWAYS EXTENDED amounts
    public void determineDiscountableAmount(ProductCollectionCalculation productCollectionCalculation, Integer terminalId) throws Exception {
        this.setDiscountableAmount(productCollectionCalculation.getDiscountableAmounts(this).getDiscountableAmount());
    }

    //calculates the discount amount based on the current set discountable amount
    public void calculateDiscountAmount() throws Exception {
        //TODO - not fully complete for item discounts, need to update calculation and take the quantity into account
        BigDecimal discountValue = this.getDiscountModel().getAmount(); //either percent or coupon amount

        BigDecimal discountAmount;

        //handle percent
        if ( this.getDiscountModel().getDiscountTypeId().equals(PosAPIHelper.DiscountType.PERCENT.toInt()) ) {
            discountAmount = this.getDiscountableAmount().multiply(discountValue.divide(new BigDecimal("100"), 4, this.getRoundingMode()));
            //handle coupon
        } else if ( this.getDiscountModel().getDiscountTypeId().equals(PosAPIHelper.DiscountType.COUPON.toInt()) ) {
            //if the discount value is MORE than the discountable amount, set to the total discountable
            if ( this.getDiscountableAmount().compareTo(discountValue) < 0 ) {
                discountAmount = this.getDiscountableAmount();
            } else {
                discountAmount = discountValue;
            }
        } else {
            throw new InvalidDataException("No valid discount type found when attempting to calculate the discount amount.");
        }

        //if the max discount amount is set and the current amount is more, set the discount amount to the max
        if ( this.getDiscountModel().getMaximumDiscountAmount() != null && this.getDiscountModel().getMaximumDiscountAmount().compareTo(BigDecimal.ZERO) != 0
                && this.getDiscountModel().getMaximumDiscountAmount().compareTo(discountAmount) < 0 ) {
            discountAmount = this.getDiscountModel().getMaximumDiscountAmount();
        }

        this.setDiscountAmount(discountAmount.setScale(4, this.getRoundingMode()));
    }

    //calculates the discount amount based on the current set discountable amount
    //Added in code to calculate the discount amounts for refunds
    public void calculateDiscountAmountPOS() throws Exception {
        //TODO - not fully complete for item discounts, need to update calculation and take the quantity into account
        BigDecimal discountValue = this.getDiscountModel().getAmount(); //either percent or coupon amount

        BigDecimal discountAmount;
        boolean isRefund = false;

        //handle percent
        if (this.getDiscountModel().getDiscountTypeId().equals(PosAPIHelper.DiscountType.PERCENT.toInt())) {
            discountAmount = this.getDiscountableAmount().multiply(discountValue.divide(new BigDecimal("100"), 4, this.getRoundingMode()));

            //handle coupon
        } else if (this.getDiscountModel().getDiscountTypeId().equals(PosAPIHelper.DiscountType.COUPON.toInt())) {

            BigDecimal tempDiscountableAmount = this.getDiscountableAmount();
            if (this.getDiscountableAmount().compareTo(BigDecimal.ZERO) < 0) {
                tempDiscountableAmount = this.getDiscountableAmount().abs();
                isRefund = true;
            }

            //sale
            //if the discount value is MORE than the discountable amount, set to the total discountable
            if (tempDiscountableAmount.compareTo(discountValue) < 0) {
                discountAmount = tempDiscountableAmount;
            } else {
                discountAmount = discountValue;
            }
        } else {
            throw new InvalidDataException("No valid discount type found when attempting to calculate the discount amount.");
        }

        //if the max discount amount is set and the current amount is more, set the discount amount to the max
        if (this.getDiscountModel().getMaximumDiscountAmount() != null && this.getDiscountModel().getMaximumDiscountAmount().compareTo(BigDecimal.ZERO) != 0
                && this.getDiscountModel().getMaximumDiscountAmount().compareTo(discountAmount) < 0) {
            discountAmount = this.getDiscountModel().getMaximumDiscountAmount();
        }

        if (isRefund && discountAmount.compareTo(BigDecimal.ZERO) == 1) {
            discountAmount = discountAmount.negate(); //For a refund, at this point we want a negative
        }

        this.setDiscountAmount(discountAmount.setScale(4, this.getRoundingMode()));
    }

    //calculates the discount for a product line based on the data in lineDiscounts hashmap
    public BigDecimal calculateProductLineDiscountAmount(Integer index) throws Exception {
        BigDecimal lineDiscountAmount;

        //if no valid discountable amount set or if it is zero, just return zero
        if ( !this.getLineDiscounts().containsKey(index) || this.getLineDiscounts().get(index) == null || this.getLineDiscounts().get(index).compareTo(BigDecimal.ZERO) == 0 ) {
            return BigDecimal.ZERO;
        }

        //lineDiscount = totalDiscount * (lineDiscountAble/totalDiscountable)
        BigDecimal linePercentOfTotal = this.getLineDiscounts().get(index).divide(this.getDiscountableAmount().setScale(4, this.getRoundingMode()), this.getRoundingMode());
        lineDiscountAmount = this.getDiscountAmount().multiply(linePercentOfTotal);

        return lineDiscountAmount.setScale(4, this.getRoundingMode());
    }

    //validates and applies the discount based on discount application type
    public void applyDiscount(TransactionModel transactionModel, ProductCollectionCalculation productCollectionCalculation, Integer index) throws Exception {
        if ( this.getDiscountableAmount() == null || this.getDiscountableAmount().compareTo(BigDecimal.ZERO) == 0 ) {
            throw new MissingDataException("Invalid Discountable Amount.");
        }

        //if discount hasn't been calculated yet, calculate it
        if ( this.getDiscountAmount() == null ) {
            this.calculateDiscountAmount();
        }

        //ensure that the discount amount is calculated now
        if ( this.getDiscountAmount() == null || this.getDiscountAmount().compareTo(BigDecimal.ZERO) == 0 ) {
            throw new MissingDataException("Invalid Discount Amount");
        }

        //if item discount then applyItemDiscount else applySubtotalDiscount
        if ( this.getDiscountModel().isSubTotalDiscount() ) {
            this.applySubtotalDiscount(transactionModel, productCollectionCalculation);
        } else {
            applyItemDiscount(transactionModel, productCollectionCalculation.getCollection().get(index));
        }
    }

    //validates and applies the discount based on discount application type
    public void applyDiscountForPOS(TransactionModel transactionModel, ProductCollectionCalculation productCollectionCalculation, Integer index) throws Exception {
        //if discount hasn't been calculated yet, calculate it
        if (this.getDiscountAmount() == null) {
            this.calculateDiscountAmountPOS();
        }

        //if item discount then applyItemDiscount else applySubtotalDiscount
        if (this.getDiscountModel().isSubTotalDiscount()) {
            switch (transactionModel.getPosType()){
                case THIRD_PARTY:
                    this.applySubtotalDiscountForThirdParty(transactionModel, productCollectionCalculation);
                    break;
                default:
                    this.applySubtotalDiscountForPOS(transactionModel, productCollectionCalculation);
            }
        } else {
            applyItemDiscountForPOS(transactionModel, productCollectionCalculation);
        }
    }

    //build and attach discount related models to transmodel, update affected products
    private void applySubtotalDiscount(TransactionModel transactionModel, ProductCollectionCalculation productCollectionCalculation) throws Exception {
        //build discountLineModel and attach to transmodel
        transactionModel.getDiscounts().add(this.buildDiscountLineItemModel(transactionModel.getLoyaltyAccount().getId()));
        this.checkAndSetNextTransactionItemNum(transactionModel);

        //foreach productCalc
        for (ProductCalculation productCalculation : productCollectionCalculation.getCollection() ) {
            //calculate line discounts
            BigDecimal lineDiscount = this.calculateProductLineDiscountAmount(productCalculation.getIndex());

            //no need to add ItemDiscountModel if there's no discount on this item (ProductLine)
            if ( lineDiscount.compareTo(BigDecimal.ZERO) == 0 ) {
                continue;
            }

            //calculate the per unit discount -- should be 4 decimal EXTENDED amount
            productCalculation.applyPriceReduction(true, this.getId(), lineDiscount, this.getLineDiscounts().get(productCalculation.getIndex()), this.getLineQuantities().get(productCalculation.getIndex()), this.getDiscountModel().getTaxIds(), false);

            //build and attach ItemTaxDisc models to transmodel -> productlines
            ItemDiscountModel lineItemDiscountModel = this.buildDiscountItemDiscountModel(productCalculation.getIndex(), lineDiscount);
            transactionModel.getProducts().get(productCalculation.getIndex()).getDiscounts().add(lineItemDiscountModel);
        }
    }

    //build and attach discount related models to transmodel, update affected products
    private void applySubtotalDiscountForPOS(TransactionModel transactionModel, ProductCollectionCalculation productCollectionCalculation) throws Exception {
        //build discountLineModel and attach to transmodel
        int tempDiscountLineItemId = this.getOrGenerateTempId();
        boolean itemDiscountRecordsAdded = false;
        this.fixDiscountDiscrepancyAmt(productCollectionCalculation, transactionModel);
        this.checkAndSetNextTransactionItemNum(transactionModel);

        //foreach productCalc
        for (ProductCalculation productCalculation : productCollectionCalculation.getCollection()) {
            //calculate line discounts
            BigDecimal lineDiscount = this.calculateProductLineDiscountAmount(productCalculation.getIndex());

            //no need to add ItemDiscountModel if there's no discount on this item (ProductLine)
            if (lineDiscount.compareTo(BigDecimal.ZERO) == 0) {
                continue;
            }

            //calculate the per unit discount -- should be 4 decimal EXTENDED amount
            productCalculation.applyPriceReduction(true, this.getId(), lineDiscount, this.getLineDiscounts().get(productCalculation.getIndex()), this.getLineQuantities().get(productCalculation.getIndex()), this.getDiscountModel().getTaxIds(), false);

            //build and attach ItemTaxDisc models to transmodel -> productlines
            ItemDiscountModel lineItemDiscountModel = this.buildDiscountItemDiscountModel(productCalculation.getIndex(), lineDiscount);
            lineItemDiscountModel.setTempLinkedLineItemId(tempDiscountLineItemId);
            transactionModel.getProducts().get(productCalculation.getIndex()).getDiscounts().add(lineItemDiscountModel);  //set the temp ID on the Item Discount record
            itemDiscountRecordsAdded = true;
        }

        Integer employeeId = null;
        employeeId = (transactionModel.getLoyaltyAccount()) == null ? null : transactionModel.getLoyaltyAccount().getId();
        DiscountLineItemModel discountLineItemModel = this.buildDiscountLineItemModel(employeeId);
        if (this.getQcDiscountModel() != null){
            discountLineItemModel.setRequireAmountCheck(true);
        }

        if (itemDiscountRecordsAdded) {
            discountLineItemModel.setApplicationResult(DiscountApplicationResult.SUCCESS);
        }

        Boolean posaRefund = transactionModel.getApiActionTypeEnum().equals(REFUND_MANUAL) &&
        transactionModel.getTerminal().getTerminalModelId().equals(PosAPIHelper.PosTerminalModel.POSANYWHERE.toInt());

        if (posaRefund) {
            discountLineItemModel.setQuantity(discountLineItemModel.getQuantity().multiply(new BigDecimal(-1)));
            discountLineItemModel.setAmount(discountLineItemModel.getAmount().multiply(new BigDecimal(-1)));
        }

        discountLineItemModel.setTempId(tempDiscountLineItemId);
        transactionModel.getDiscounts().add(discountLineItemModel);
    }

    //build and attach discount related models to transactionModel, update affected products
    private void applySubtotalDiscountForThirdParty(TransactionModel transactionModel, ProductCollectionCalculation productCollectionCalculation) throws Exception {
        //build discountLineModel and attach to transmodel
        int tempDiscountLineItemId = this.getOrGenerateTempId();
        boolean itemDiscountRecordsAdded = false;
        this.fixDiscountDiscrepancyAmt(productCollectionCalculation, transactionModel);

        //foreach productCalc
        for (ProductCalculation productCalculation : productCollectionCalculation.getCollection()) {
            //calculate line discounts
            BigDecimal lineDiscount = this.calculateProductLineDiscountAmount(productCalculation.getIndex());

            //no need to add ItemDiscountModel if there's no discount on this item (ProductLine)
            if (lineDiscount.compareTo(BigDecimal.ZERO) == 0) {
                continue;
            }

            //calculate the per unit discount -- should be 4 decimal EXTENDED amount
            productCalculation.applyPriceReduction(true, this.getId(), lineDiscount, this.getLineDiscounts().get(productCalculation.getIndex()), this.getLineQuantities().get(productCalculation.getIndex()), this.getDiscountModel().getTaxIds(), false);

            //build and attach ItemTaxDisc models to transmodel -> productlines
            ItemDiscountModel lineItemDiscountModel = this.buildDiscountItemDiscountModel(productCalculation.getIndex(), lineDiscount);
            lineItemDiscountModel.setTempLinkedLineItemId(tempDiscountLineItemId);
            transactionModel.getProducts().get(productCalculation.getIndex()).getDiscounts().add(lineItemDiscountModel);  //set the temp ID on the Item Discount record
            itemDiscountRecordsAdded = true;
        }

        Integer employeeId = null;
        employeeId = (transactionModel.getLoyaltyAccount()) == null ? null : transactionModel.getLoyaltyAccount().getId();
        Boolean thirdPartyOverride = false;

        if (transactionModel.getPosType().equals(PosAPIHelper.PosType.THIRD_PARTY)) {
            thirdPartyOverride = true;

            BigDecimal tenderAmount = BigDecimal.ZERO;
            BigDecimal qcDiscountAmount = BigDecimal.ZERO;
            TenderLineItemModel qcTenderLineItem = null;

            //get tender amount
            for (TenderLineItemModel tenderLineItemModel : transactionModel.getTenders()) {
                if (tenderLineItemModel.getAccount() != null) {
                    if (tenderLineItemModel.getAccount().getId().equals(employeeId)) {
                        if (tenderLineItemModel.getTender().getTenderTypeId() == PosAPIHelper.TenderType.QUICKCHARGE.toInt()) {
                            tenderAmount = tenderAmount.add(tenderLineItemModel.getAmount());
                            qcTenderLineItem = tenderLineItemModel;
                            break;
                        }
                    }
                }
            }

            if (this.getQcDiscountModel() != null) {
                if (this.getQcDiscountModel().getDiscountTypeId().equals(PosAPIHelper.DiscountType.COUPON.toInt())){
                    if (this.getQcDiscountModel().getAmount().compareTo(BigDecimal.ZERO) != 0) {
                        qcDiscountAmount = this.getQcDiscountModel().getAmount();
                    }
                }

                if (this.getQcDiscountModel().getDiscountTypeId().equals(PosAPIHelper.DiscountType.PERCENT.toInt())){
                    if (this.getQcDiscountModel().getAmount().compareTo(BigDecimal.ZERO) != 0) {
                        BigDecimal percent = this.getQcDiscountModel().getAmount().divide(new BigDecimal(100), 2, RoundingMode.HALF_UP);
                        qcDiscountAmount = tenderAmount.multiply(percent);
                    }
                }
            } else {
                return;  //Only QC Discounts are supported
            }

            //Check Minimum Discount amount
            if (this.getDiscountModel().getMinimumPurchaseAmount() != null && this.getDiscountModel().getMinimumPurchaseAmount().compareTo(BigDecimal.ZERO) > 0){
                if (tenderAmount.compareTo(this.getDiscountModel().getMinimumPurchaseAmount()) < 0){
                    Logger.logMessage("Minimum Purchase amount is set.  Transaction amount is not high enough.  Discount will not be applied.", PosAPIHelper.getLogFileName(transactionModel.getTerminal().getId()), Logger.LEVEL.TRACE);
                    return;
                }
            }

            //Check Max Discount amount
            if (this.getDiscountModel().getMaximumDiscountAmount() != null && this.getDiscountModel().getMaximumDiscountAmount().compareTo(BigDecimal.ZERO) > 0){
                if (qcDiscountAmount.compareTo(this.getDiscountModel().getMaximumDiscountAmount()) > 0){
                    Logger.logMessage("Maximum Discount amount is set.  Discount amount is higher than this maximum amount.  Discount amount will be updated.", PosAPIHelper.getLogFileName(transactionModel.getTerminal().getId()), Logger.LEVEL.TRACE);
                    qcDiscountAmount = this.getDiscountModel().getMaximumDiscountAmount();
                }
            }

            //QC Discount covers full tender transaction
            if (qcDiscountAmount.compareTo(tenderAmount) >= 0) {
                this.setDiscountAmount(tenderAmount);
                qcTenderLineItem.setAmount(BigDecimal.ZERO);
                qcTenderLineItem.refreshExtendedAmount();

            } else if (tenderAmount.compareTo(qcDiscountAmount) > 0) {
                this.setDiscountAmount(qcDiscountAmount);

                BigDecimal newTenderAmount = tenderAmount.subtract(qcDiscountAmount);
                qcTenderLineItem.setAmount(newTenderAmount);
                qcTenderLineItem.refreshExtendedAmount();
            }

            itemDiscountRecordsAdded = true;
        }

        DiscountLineItemModel discountLineItemModel = this.buildDiscountLineItemModel(employeeId);
        if (this.getQcDiscountModel() != null){
            discountLineItemModel.setRequireAmountCheck(true);

            if (thirdPartyOverride){
                discountLineItemModel.setRequireAmountCheck(false);
            }
        }

        if (itemDiscountRecordsAdded) {
            discountLineItemModel.setApplicationResult(DiscountApplicationResult.SUCCESS);
        }

        Boolean posaRefund = transactionModel.getApiActionTypeEnum().equals(REFUND_MANUAL) &&
        transactionModel.getTerminal().getTerminalModelId().equals(PosAPIHelper.PosTerminalModel.POSANYWHERE.toInt());

        if (posaRefund) {
            discountLineItemModel.setQuantity(discountLineItemModel.getQuantity().multiply(new BigDecimal(-1)));
            discountLineItemModel.setAmount(discountLineItemModel.getAmount().multiply(new BigDecimal(-1)));
        }

        discountLineItemModel.setTempId(tempDiscountLineItemId);
        transactionModel.getDiscounts().add(discountLineItemModel);
    }

    private void applyItemDiscount(TransactionModel transactionModel, ProductCalculation productCalculation) throws Exception {
        //TODO - not fully complete, need to update calculation and take the quantity into account for item discounts
        //build discountLineModel and attach to transmodel
        int tempDiscountLineItemId = this.getOrGenerateTempId();

        DiscountLineItemModel discountLineItemModel = this.buildDiscountLineItemModel(transactionModel.getLoyaltyAccount().getId());
        discountLineItemModel.setTempId(tempDiscountLineItemId);
        this.checkAndSetNextTransactionItemNum(transactionModel);

        transactionModel.getDiscounts().add(discountLineItemModel);

        boolean isComboDiscount = false;
        if(productCalculation.getComboTransLineItemId() != null && productCalculation.getDiscountsApplied().size() == 0) {
            isComboDiscount = true;
        }
        productCalculation.applyPriceReduction(true, this.getId(), this.getDiscountAmount(), this.getDiscountableAmount(), this.getLineQuantities().get(productCalculation.getIndex()), this.getDiscountModel().getTaxIds(), true, isComboDiscount);

        //build and attach ItemTaxDisc models to transmodel -> productlines
        ItemDiscountModel lineItemDiscountModel = this.buildDiscountItemDiscountModel(productCalculation.getIndex(), this.getDiscountAmount());
        lineItemDiscountModel.setTempLinkedLineItemId(tempDiscountLineItemId);
        transactionModel.getProducts().get(productCalculation.getIndex()).getDiscounts().add(lineItemDiscountModel);
    }

    private void applyItemDiscountForPOS(TransactionModel transactionModel, ProductCollectionCalculation productCollectionCalculation) throws Exception {
        //build discountLineModel and attach to transmodel
        int tempDiscountLineItemId = this.getOrGenerateTempId();
        boolean itemDiscountApplied = false;
        this.checkAndSetNextTransactionItemNum(transactionModel);

        for (ProductCalculation productCalculation : productCollectionCalculation.getCollection() ) {
            if (itemDiscountApplied){
                break;
            }

            //calculate line discounts
            BigDecimal lineDiscount = this.calculateProductLineDiscountAmount(productCalculation.getIndex());

            //no need to add ItemDiscountModel if there's no discount on this item (ProductLine)
            if ( lineDiscount.compareTo(BigDecimal.ZERO) == 0 ) {
                continue;
            }

            //calculate the per unit discount -- should be 4 decimal EXTENDED amount
            productCalculation.applyPriceReduction(true, this.getId(), lineDiscount, this.getLineDiscounts().get(productCalculation.getIndex()), this.getLineQuantities().get(productCalculation.getIndex()), this.getDiscountModel().getTaxIds(), false);

            //build and attach ItemTaxDisc models to transmodel -> productlines
            ItemDiscountModel lineItemDiscountModel = this.buildDiscountItemDiscountModel(productCalculation.getIndex(), lineDiscount);
            lineItemDiscountModel.setTempLinkedLineItemId(tempDiscountLineItemId);
            transactionModel.getProducts().get(productCalculation.getIndex()).getDiscounts().add(lineItemDiscountModel);
            itemDiscountApplied = true;
        }

        DiscountLineItemModel discountLineItemModel = this.buildDiscountLineItemModel(transactionModel.getLoyaltyAccount().getId());
        if (itemDiscountApplied){
            discountLineItemModel.setApplicationResult(DiscountApplicationResult.SUCCESS);
        }
        discountLineItemModel.setTempId(tempDiscountLineItemId);
        transactionModel.getDiscounts().add(discountLineItemModel);
    }

    public DiscountLineItemModel buildDiscountLineItemModel(Integer employeeId) throws Exception {
        DiscountLineItemModel discountLineItemModel = new DiscountLineItemModel();
        discountLineItemModel.setItemId(this.getId());
        discountLineItemModel.setItemTypeId(PosAPIHelper.ObjectType.POSDISCOUNT.toInt());
        discountLineItemModel.setQuantity(BigDecimal.ONE);
        discountLineItemModel.setDiscount(this.getDiscountModel());
        discountLineItemModel.setEligibleAmount(this.getDiscountableAmount());
        discountLineItemModel.setAmount(BigDecimal.valueOf(-1).multiply(this.getDiscountAmount()));
        discountLineItemModel.setAmount(discountLineItemModel.getAmount().setScale(2, RoundingMode.HALF_UP));
        discountLineItemModel.setEmployeeId(employeeId);
        discountLineItemModel.setApplicationResult(this.getApplicationResult());
        discountLineItemModel.setAutoAppliedOnCurrentInquire(this.isAutoAppliedOnCurrentInquire());

        if ( this.getQCDiscountId() != null ) {
            QcDiscountModel qcDiscountModel = new QcDiscountModel();
            qcDiscountModel.setId(this.getQCDiscountId());
            if (this.getQcDiscountModel() != null){
                qcDiscountModel.setSingleUse(this.getQcDiscountModel().isSingleUse());
            }
            discountLineItemModel.setQcDiscount(qcDiscountModel);
        }

        return discountLineItemModel;
    }

    public ItemDiscountModel buildDiscountItemDiscountModel(Integer index, BigDecimal lineDiscount) throws Exception {
        ItemDiscountModel itemDiscountModel = new ItemDiscountModel();
        itemDiscountModel.setItemId(this.getId());
        itemDiscountModel.setItemTypeId(PosAPIHelper.ObjectType.POSDISCOUNT.toInt());
        itemDiscountModel.setEligibleAmount(this.getLineDiscounts().get(index));
        itemDiscountModel.setAmount((BigDecimal.valueOf(-1).multiply(lineDiscount)));
        itemDiscountModel.setDiscount(this.getDiscountModel());
        itemDiscountModel.setEligibleQuantity(this.getLineQuantities().get(index));

        return itemDiscountModel;
    }

    //adds a discountable amount to the appropriate lineDiscount key/value, and updates the total discountable
    public void addLineDiscountable(Integer index, BigDecimal amount) {
        //update line discountable
        if ( !this.getLineDiscounts().containsKey(index) ) {
            this.getLineDiscounts().put(index, BigDecimal.ZERO);
        }

        //if the amount is 0 after rounding, then it is zero
        if ( amount.setScale(4, this.getRoundingMode()).compareTo( BigDecimal.ZERO ) == 0 ) {
            amount = BigDecimal.ZERO;
        }

        this.getLineDiscounts().put(index, this.getLineDiscounts().get(index).add(amount));

        //update total discountable
        this.setDiscountableAmount(this.getDiscountableAmount().add(amount));
    }

    public void addQuantityLineDiscountable(Integer index, BigDecimal quantity){
        //update quantity line discountable
        if ( !this.getLineQuantities().containsKey(index) ) {
            this.getLineQuantities().put(index, BigDecimal.ZERO);
        }

        //if the amount is 0 after rounding, then it is zero
        if ( quantity.setScale(4, this.getRoundingMode()).compareTo( BigDecimal.ZERO ) == 0 ) {
            quantity = BigDecimal.ZERO;
        }

        this.getLineQuantities().put(index, this.getLineQuantities().get(index).add(quantity));
    }

    public boolean validatePOSDiscountRules(TransactionCalculation transactionCalculation, Boolean posaRefund){
        boolean result = true;

        //Check Minimum purchase amount
        if (this.getDiscountModel().getMinimumPurchaseAmount() != null && this.getDiscountModel().getMinimumPurchaseAmount().compareTo(BigDecimal.ZERO) != 0) {
            if ((transactionCalculation.getProductCollectionCalculation().getProductSubtotal().compareTo(this.getDiscountModel().getMinimumPurchaseAmount()) < 0 && !posaRefund) ||
                (transactionCalculation.getProductCollectionCalculation().getProductSubtotal().multiply(new BigDecimal(-1)).compareTo(this.getDiscountModel().getMinimumPurchaseAmount()) < 0 && posaRefund)) {
                //Transaction subtotal less than Discounts' minimum purchase amount
                this.setApplicationResult(DiscountApplicationResult.INVALID_MIN_PURCHASE_AMT);
                this.setDiscountAmount(BigDecimal.ZERO);
                result = false;
            }
        }

        return result;
    }

    public void validatePOSSubtotalDiscountRules(TransactionCalculation transactionCalculation) {
        if (this.getDiscountModel().isSubTotalDiscount()) {
            for (ProductCalculation productCalculation : transactionCalculation.getProductCollectionCalculation().getCollection()) {
                //check to see if the product already has records applied
                for (ItemDiscountModel itemDiscountModel : transactionCalculation.getTransactionModel().getProducts().get(productCalculation.getIndex()).getDiscounts()) {
                    if (itemDiscountModel.getDiscount().isSubTotalDiscount()) {
                        //Check for duplicate Subtotal Discounts
                        if (itemDiscountModel.getDiscount().getId().equals(this.getDiscountModel().getId())) {
                            this.setApplicationResult(DiscountApplicationResult.INVALID_DUPLICATE_SUBTOTAL_DISCOUNTS);
                            //Remove the line discount amount from the discountable amount
                            BigDecimal lineDiscountAmount = this.getLineDiscounts().get(productCalculation.getIndex());
                            this.setDiscountableAmount(this.getDiscountableAmount().subtract(lineDiscountAmount));
                            //update the product with a zero line discount
                            this.getLineDiscounts().put(productCalculation.getIndex(), BigDecimal.ZERO);
                        } else {
                            //Check if there is already a subtotal discount applied, and the Terminal's Revenue Center is not set to allow Compound Subtotal Discounts
                            if (!transactionCalculation.getTransactionModel().getTerminal().getRevenueCenter().isAllowCompoundSubTotalDiscounts()) {
                                this.setApplicationResult(DiscountApplicationResult.INVALID_COMPOUND_SUBTOTAL_DISCOUNT_RULE);
                                //Remove the line discount amount from the discountable amount
                                BigDecimal lineDiscountAmount = this.getLineDiscounts().get(productCalculation.getIndex());
                                this.setDiscountableAmount(this.getDiscountableAmount().subtract(lineDiscountAmount));
                                //update the product with a zero line discount
                                this.getLineDiscounts().put(productCalculation.getIndex(), BigDecimal.ZERO);
                            }
                        }
                    } else {
                        //Check if there is already an item discount applied, and the Terminal's Revenue Center is not set to allow Compound Item Discounts
                        if (!transactionCalculation.getTransactionModel().getTerminal().getRevenueCenter().isAllowCompoundSubTotalDiscounts()) {
                            this.setApplicationResult(DiscountApplicationResult.INVALID_COMPOUND_ITEM_DISCOUNT_RULE);
                            //Remove the line discount amount from the discountable amount
                            BigDecimal lineDiscountAmount = this.getLineDiscounts().get(productCalculation.getIndex());
                            this.setDiscountableAmount(this.getDiscountableAmount().subtract(lineDiscountAmount));
                            //update the product with a zero line discount
                            this.getLineDiscounts().put(productCalculation.getIndex(), BigDecimal.ZERO);
                        }
                    }
                }
            }
        }
    }

    private int getOrGenerateTempId(){
        int tempId = (this.getDiscountLineItemModel() != null && this.getDiscountLineItemModel().getTempId() != null) ? this.getDiscountLineItemModel().getTempId() : ((int) (Math.random() * 100000) + 1);
        return tempId;
    }

    public void setQCDiscountDetails( HashMap discountHM ) throws Exception {
        Integer qcDiscountID = CommonAPI.convertModelDetailToInteger(discountHM.get("DISCOUNTID"));

        if ( qcDiscountID == null ) {
            return;
        }

        setQCDiscountId(qcDiscountID);

        QcDiscountModel qcDiscountModel = new QcDiscountModel();
        qcDiscountModel.setId( qcDiscountID );

        String startTime = CommonAPI.convertModelDetailToString( discountHM.get("STARTTIME") );
        String endTime = CommonAPI.convertModelDetailToString( discountHM.get("ENDTIME") );
        boolean singleUse = CommonAPI.convertModelDetailToBoolean(discountHM.get("SINGLETRANSACTION"));

        qcDiscountModel.setStartTime(startTime);
        qcDiscountModel.setEndTime(endTime);
        qcDiscountModel.setSingleUse(singleUse);

        setQcDiscountModel( qcDiscountModel );
    }

    /**
     * Compare previously submitted Discount Line Item Amounts with Product Calculation lines
     * If the amounts on previously submitted discounts don't match, add the difference to the new discount
     * @param productCollectionCalculation
     * @param transactionModel
     */
    public void fixDiscountDiscrepancyAmt(ProductCollectionCalculation productCollectionCalculation, TransactionModel transactionModel){
        BigDecimal totalDifference = getDiscountDiscrepancyAmt(productCollectionCalculation, transactionModel);
        if (totalDifference.compareTo(BigDecimal.ZERO) > 0){
            this.setDiscountAmount(this.getDiscountAmount().add(totalDifference));
            this.setDiscountableAmount(this.getDiscountableAmount().add(totalDifference));
            Logger.logMessage("Previously submitted discounts were found to have a discrepancy.  Discount Amount updated.", PosAPIHelper.getLogFileName(transactionModel.getTerminal().getId()));
        }
    }

    /**
     * Compare previously submitted Discount Line Item Amounts with Product Calculation lines
     * If there is a discrepancy, try to add the discrepancy to the new discount.  This should correct the discount amount and eligible amount.
     * @param productCollectionCalculation
     * @param transactionModel
     * @return
     */
    private BigDecimal getDiscountDiscrepancyAmt(ProductCollectionCalculation productCollectionCalculation, TransactionModel transactionModel) {
        HashMap<Integer, BigDecimal> discountTotals = new HashMap<>();
        BigDecimal totalDifference = BigDecimal.ZERO;

        try {
            for (ProductCalculation productCalculation : productCollectionCalculation.getCollection()) {
                for (HashMap hmDiscountApplied : productCalculation.getDiscountsApplied()) {
                    Integer discountId = CommonAPI.convertModelDetailToInteger(hmDiscountApplied.get("discountId"));
                    BigDecimal amount = CommonAPI.convertModelDetailToBigDecimal(hmDiscountApplied.get("amount"));
                    if (discountTotals.containsKey(discountId)) {
                        BigDecimal existingAmt = discountTotals.get(discountId);
                        discountTotals.put(discountId, existingAmt.add(amount));
                    } else {
                        discountTotals.put(discountId, amount);
                    }
                }
            }

            if (!discountTotals.isEmpty()) {
                for (DiscountLineItemModel discountLineItemModel : transactionModel.getDiscounts()) {
                    BigDecimal discountDifference = BigDecimal.ZERO;
                    if (discountTotals.containsKey(discountLineItemModel.getDiscount().getId())) {
                        BigDecimal calculatedTotal = discountTotals.get(discountLineItemModel.getDiscount().getId()).setScale(2, RoundingMode.HALF_UP);
                        BigDecimal submittedAmount = discountLineItemModel.getAmount().negate();

                        //if submitted discount amount > what the POS API calculated the ItemDiscount records, we have to adjust the new discount
                        if (submittedAmount.compareTo(calculatedTotal) < 0) {
                            //we have to somehow add the difference to the new discount?
                            discountDifference = calculatedTotal.subtract(submittedAmount);
                            totalDifference = totalDifference.add(discountDifference);
                        }
                    }
                }
            }

        } catch (Exception ex) {
            Logger.logMessage("DiscountCalculation.getDiscountDiscrepancyAmt: Error checking for Discount Line discrepancies.", Logger.LEVEL.ERROR);
        }

        return totalDifference;
    }

    private Integer checkAndSetNextTransactionItemNum(TransactionModel transactionModel){
        Integer transactionLineItemNum = null;

        if (this.getQcDiscountModel() != null) {

            if (transactionModel.getDiscountInfoCollection().getCollection() != null
                    && !transactionModel.getDiscountInfoCollection().getCollection().isEmpty()) {

                //If voucher re-calculation is in progress, get the original transaction item num
                DiscountInfoModel discountInfoModel = transactionModel.getDiscountInfoCollection().getOneByQcDiscountId(this.getQcDiscountModel().getId());
                if (discountInfoModel.getTransactionItemNum() != null &&
                        !discountInfoModel.getTransactionItemNum().toString().isEmpty()) {
                    transactionLineItemNum = discountInfoModel.getTransactionItemNum();
                }
            }
        }

        if (transactionLineItemNum == null){
            transactionLineItemNum = transactionModel.getNextTransactionItemNum();
        }

        return transactionLineItemNum;
    }

    //getters and setters
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getQCDiscountId() {
        return QCDiscountId;
    }

    public void setQCDiscountId(Integer QCDiscountId) {
        this.QCDiscountId = QCDiscountId;
    }

    public DiscountModel getDiscountModel() {
        return discountModel;
    }

    public void setDiscountModel(DiscountModel discountModel) {
        this.discountModel = discountModel;
    }

    public BigDecimal getDiscountableAmount() {
        return discountableAmount;
    }

    public void setDiscountableAmount(BigDecimal discountableAmount) {
        this.discountableAmount = discountableAmount;
    }

    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(BigDecimal discountAmount) {
        this.discountAmount = discountAmount;
    }

    public HashMap<Integer, BigDecimal> getLineDiscounts() {
        return lineDiscounts;
    }

    public void setLineDiscounts(HashMap<Integer, BigDecimal> lineDiscounts) {
        this.lineDiscounts = lineDiscounts;
    }

    public String getTaxIds() {
        return taxIds;
    }

    public void setTaxIds(String taxIds) {
        this.taxIds = taxIds;
    }

    public int getRoundingMode() {
        return roundingMode;
    }

    public void setRoundingMode(int roundingMode) {
        this.roundingMode = roundingMode;
    }

    public HashMap<Integer, BigDecimal> getLineQuantities() {
        return lineQuantities;
    }

    public void setLineQuantities(HashMap<Integer, BigDecimal> lineQuantities) {
        this.lineQuantities = lineQuantities;
    }

    public DiscountLineItemModel getDiscountLineItemModel() {
        return discountLineItemModel;
    }

    public void setDiscountLineItemModel(DiscountLineItemModel discountLineItemModel) {
        this.discountLineItemModel = discountLineItemModel;
    }

    public boolean isAllowDiscountAmountOverride() {
        return allowDiscountAmountOverride;
    }

    public void setAllowDiscountAmountOverride(boolean allowDiscountAmountOverride) {
        this.allowDiscountAmountOverride = allowDiscountAmountOverride;
    }

    public DiscountApplicationResult getApplicationResult() {
        return applicationResult;
    }

    public void setApplicationResult(DiscountApplicationResult applicationResult) {
        this.applicationResult = applicationResult;
    }

    public boolean isAutoAppliedOnCurrentInquire() {
        return autoAppliedOnCurrentInquire;
    }

    public void setAutoAppliedOnCurrentInquire(boolean autoAppliedOnCurrentInquire) {
        this.autoAppliedOnCurrentInquire = autoAppliedOnCurrentInquire;
    }

    public QcDiscountModel getQcDiscountModel() {
        return qcDiscountModel;
    }

    public void setQcDiscountModel(QcDiscountModel qcDiscountModel) {
        this.qcDiscountModel = qcDiscountModel;
    }

    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public Integer getTransactionItemNum() {
        return transactionItemNum;
    }

    public void setTransactionItemNum(Integer transactionItemNum) {
        this.transactionItemNum = transactionItemNum;
    }
}
