package com.mmhayes.common.transaction.models;

//API dependencies
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;

import java.util.HashMap;

/*
 Last Updated (automatically updated by SVN)
 $Author: nyu $: Author of last commit
 $Date: 2020-04-16 22:49:26 -0400 (Thu, 16 Apr 2020) $: Date of last commit
 $Rev: 11393 $: Revision of last commit
*/
public class PaidOutLineItemModel extends TransactionLineItemModel {
    private PaidOutModel paidOut = null;

    private String creditCardTransInfo = "";

    public PaidOutLineItemModel(){
        boolean stopHere = true;
    }

    public void setPaidOutModelProperties(HashMap modelDetailHM, Integer terminalId) throws Exception {
        this.setModelProperties(modelDetailHM);
        this.setCreditCardTransInfo(CommonAPI.convertModelDetailToString(modelDetailHM.get("CREDITCARDTRANSINFO")));
    }

    public PaidOutModel getPaidOut() {
        return paidOut;
    }

    public void setPaidOut(PaidOutModel paidOut) {
        this.paidOut = paidOut;
    }

    @JsonIgnore //not returning this for this transaction line item type.  This should be assumed I think.  Trying to clean up the json.
    public @Override Integer getItemTypeId(){
        return PosAPIHelper.ObjectType.PAIDOUT.toInt();
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public String getCreditCardTransInfo() {
        return creditCardTransInfo;
    }

    public void setCreditCardTransInfo(String creditCardTransInfo) {
        this.creditCardTransInfo = creditCardTransInfo;
    }
}
