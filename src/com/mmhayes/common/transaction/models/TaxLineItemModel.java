package com.mmhayes.common.transaction.models;

//API dependencies

import com.mmhayes.common.api.PosApi30.PosAPIHelper;

import java.util.HashMap;
import java.util.Objects;

//other dependencies

/*
 Last Updated (automatically updated by SVN)
 $Author: jkflanagan $: Author of last commit
 $Date: 2020-12-31 16:39:34 -0500 (Thu, 31 Dec 2020) $: Date of last commit
 $Rev: 13381 $: Revision of last commit
*/
public class TaxLineItemModel extends TransactionLineItemModel {

    private TaxModel tax = null;

    public TaxLineItemModel() {
        super();
        setAmount(null);
    }

    public static TaxLineItemModel createTaxLineItemModel(HashMap modelDetailHM) throws Exception {
        TaxLineItemModel taxLineItemModel = new TaxLineItemModel();
        taxLineItemModel.setModelProperties(modelDetailHM);

        return taxLineItemModel;
    }

    public TaxModel getTax() {
        return tax;
    }

    public void setTax(TaxModel tax) {
        this.tax = tax;
    }
    //@JsonIgnore
    //We need the ItemTypeId on the TaxLineItemModel due to Tax Deletes
    public @Override Integer getItemTypeId() {
        if (itemTypeId != null){
            return itemTypeId;
        } else {
            return PosAPIHelper.ObjectType.TAX.toInt();
        }
    }

    /**
     * Overridden toString method for a TaxModel.
     * @return The {@link String} representation of a TaxModel.
     */
    @Override
    public String toString () {

        return String.format("TAX: %s", Objects.toString((tax != null ? tax.toString() : "N/A"), "N/A"));

    }

}
