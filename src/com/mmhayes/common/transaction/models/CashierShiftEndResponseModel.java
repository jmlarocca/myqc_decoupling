package com.mmhayes.common.transaction.models;

/*
 $Author: eglundin $: Author of last commit
 $Date: 2017-02-16 11:12:54 -0500 (Thu, 16 Feb 2017) $: Date of last commit
 $Rev: 3553 $: Revision of last commit
 Notes:
*/
public class CashierShiftEndResponseModel {

    private TransactionModel transaction;
    private CashierShiftEndModel cashierShiftEnd;

     /*1. The PATransactionID of the new end shift transaction
    2. The PATransactionID of the previous end shift transaction (aka the starting point the backend used)
    3. The PATransactionID of the first transaction in the shift*/


    public static CashierShiftEndResponseModel createCashierShiftEndResponseModel(TransactionModel transactionModel, CashierShiftEndModel cashierShiftEndModel) throws Exception {
        CashierShiftEndResponseModel cashierShiftEndResponseModel = new CashierShiftEndResponseModel();
        cashierShiftEndResponseModel.setTransaction(transactionModel);
        cashierShiftEndResponseModel.setCashierShiftEnd(cashierShiftEndModel);
        return cashierShiftEndResponseModel;
    }

    public TransactionModel getTransaction() {
        return transaction;
    }

    public void setTransaction(TransactionModel transaction) {
        this.transaction = transaction;
    }

    public CashierShiftEndModel getCashierShiftEnd() {
        return cashierShiftEnd;
    }

    public void setCashierShiftEnd(CashierShiftEndModel cashierShiftEnd) {
        this.cashierShiftEnd = cashierShiftEnd;
    }


}
