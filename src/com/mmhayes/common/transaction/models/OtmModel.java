package com.mmhayes.common.transaction.models;

//MMHayes Dependencies
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.login.OtmLoginModel;
import com.mmhayes.common.utils.Logger;

//API Dependencies
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionDeserializer;

//Other Dependencies
import com.mmhayes.common.utils.StringFunctions;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2020-08-19 12:20:38 -0400 (Wed, 19 Aug 2020) $: Date of last commit
 $Rev: 12389 $: Revision of last commit
*/

public class OtmModel {
    private Integer id = null;
    private String name = "";
    private String macAddress = "";
    private String macAddressPlainText = "";
    private String ipAddress = "";
    private String typeName = "";
    private String logFileName = "";

    private Integer typeId = null;
    private Integer ownerShipGroupId = null;

    private long startTime;
    private long endTime;
    private long transactionDuration;

    public OtmModel() {

    }

    public static OtmModel createOtmModel(OtmLoginModel otmLoginModel) throws Exception {
        OtmModel otmModel = new OtmModel().createOtmModel(otmLoginModel.getOtm().getId());
        Logger.logMessage("OTM Created.  OtmModel.createOtmModel.", otmModel.getLogFileName(), Logger.LEVEL.DEBUG);

        return otmModel;
    }

    public OtmModel createOtmModel(Integer otmId) throws Exception {
        DataManager dm = new DataManager();

        this.setLogFileName(PosAPIHelper.getLogFileName(this));
        this.setStartTime(System.nanoTime());

        this.setId(otmId);
        ArrayList<HashMap> otmList = null;


        otmList = dm.parameterizedExecuteQuery("data.posapi30.getOneOtmByOtmId",
                new Object[]{
                        this.getId()
                },
                this.getLogFileName(),
                true
        );

        //populate this collection from an array list of hashmaps
        HashMap modelDetailHM = otmList.get(0);
        CommonAPI.checkIsNullOrEmptyObject(modelDetailHM.get("ID"), PosAPIHelper.getLogFileName(this));
        setModelProperties(modelDetailHM);

        return this;
    }

    public OtmModel setModelProperties(HashMap modelDetailHM) {

        setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));
        setName(CommonAPI.convertModelDetailToString(modelDetailHM.get("NAME")));
        setMacAddressPlainText(CommonAPI.convertModelDetailToString(modelDetailHM.get("MACADDRESS")));
        setIpAddress(CommonAPI.convertModelDetailToString(modelDetailHM.get("IPADDRESS")));
        setTypeId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("OTMTYPEID")));
        setTypeName(CommonAPI.convertModelDetailToString(modelDetailHM.get("OTMTYPENAME")));
        setOwnerShipGroupId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("OWNERSHIPGROUPID")));

        encryptMacAddress();

        return this;
    }

    public BigDecimal logTransactionTime() {
        this.setEndTime(System.nanoTime());
        this.setTransactionDuration(this.getEndTime() - this.getStartTime());

        BigDecimal duration = new BigDecimal(this.getTransactionDuration());
        BigDecimal billion = new BigDecimal(1000000000);
        BigDecimal durationInSeconds = duration.divide(billion).setScale(4, RoundingMode.HALF_UP);
        Logger.logMessage("Transaction Duration: " + durationInSeconds + " seconds", this.getLogFileName());
        return durationInSeconds;
    }

    //validate the mac address on the OTM Model then updates the status of terminals using that mac address
    public void updateTerminalStatusFromOTMMacAddress(OtmModel otmModel) throws Exception {
        DataManager dm = new DataManager();

        if (otmModel == null || otmModel.getMacAddressPlainText().equals("")) {
            return;
        }

        Integer foundTerminalID = CommonAPI.verifyMacAddressForDeploymentManager(otmModel.getMacAddressPlainText());

        if(foundTerminalID == 0) {
            Logger.logMessage("Error validating OTM Mac Address for updating Terminal Status. No Terminals found.", this.getLogFileName(), Logger.LEVEL.ERROR);
            return;
        }

        Integer result = dm.parameterizedExecuteNonQuery("data.posapi30.updateTerminalStatus",
                new Object[]{
                        otmModel.getMacAddressPlainText()
                },
                this.getLogFileName()
        );

        if (result <= 0) {
            Logger.logMessage("Error updating terminal status record from OTM Mac Address in OTMModel.updateTerminalStatus.", this.getLogFileName(), Logger.LEVEL.ERROR);
        } else {
            Logger.logMessage("Updated "+ result +" Terminals in CommonAPI.updateTerminalStatus.", this.getLogFileName() , Logger.LEVEL.TRACE);
        }
    }

    public void encryptMacAddress(){
        this.setMacAddress(StringFunctions.encodePassword(this.getMacAddressPlainText()));
    }

    public void decryptMacAddress(){
        this.setMacAddressPlainText(StringFunctions.decodePassword(this.getMacAddress()));
    }

    /**
     * Return a Log File Name string using the OTM ID and a custom log name string
     *
     * @param id
     * @param customText
     * @return
     */
    public static String getLogFileName(Integer id, String customText) {
        String baseLogFileName = "QCPOS_";
        String fileType = ".log";

        try {

            if (customText != null && !customText.isEmpty()) {
                if (!customText.endsWith("_")) {
                    customText = customText + "_";
                }
            }

            if (id == null || Integer.parseInt(id.toString()) <= 0) { //id = -1
                return baseLogFileName + customText + fileType;
            } else {
                return baseLogFileName + customText + id.toString() + fileType;
            }

        } catch (Exception ex) {
            return baseLogFileName + "OTM_0" + fileType; //QCPOS_OTM_0.log
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonIgnore
    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    @JsonIgnore
    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    @JsonIgnore
    public long getTransactionDuration() {
        return transactionDuration;
    }

    public void setTransactionDuration(long transactionDuration) {
        this.transactionDuration = transactionDuration;
    }

    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    @JsonIgnore
    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    @JsonIgnore
    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    @JsonIgnore
    public Integer getOwnerShipGroupId() {
        return ownerShipGroupId;
    }

    public void setOwnerShipGroupId(Integer ownerShipGroupId) {
        this.ownerShipGroupId = ownerShipGroupId;
    }

    @JsonIgnore
    public String getMacAddressPlainText() {
        return macAddressPlainText;
    }

    public void setMacAddressPlainText(String macAddressPlainText) {
        this.macAddressPlainText = macAddressPlainText;
    }

    @JsonIgnore
    public String getLogFileName() {
        return logFileName;
    }

    public void setLogFileName(String logFileName) {
        this.logFileName = logFileName;
    }
}
