package com.mmhayes.common.transaction.models;

//mmhayes dependencies
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.mmhayes.common.dataaccess.DataManager;

//API dependencies
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.api.CommonAPI;

//other dependencies
import java.util.HashMap;
import java.math.BigDecimal;
import java.util.ArrayList;

/*
 Last Updated (automatically updated by SVN)
 $Author: nyu $: Author of last commit
 $Date: 2021-08-16 18:29:13 -0400 (Mon, 16 Aug 2021) $: Date of last commit
 $Rev: 14886 $: Revision of last commit
 */
public class QcDiscountLineItemModel extends TransactionLineItemModel  {

    private Integer qcDiscountId = null;
    private String transactionDate = null;
    private String transComment = "";
    private Integer terminalId = null;
    private Integer paTransLineItemId = null;

    private QcDiscountModel qcDiscount = null;

    public QcDiscountLineItemModel(){

    }

    //Constructor- takes in a Hashmap
    public QcDiscountLineItemModel(HashMap qcQcDiscountLineItemHM) throws Exception {
        setModelProperties(qcQcDiscountLineItemHM);
    }

    //setter for all of this model's properties
    public QcDiscountLineItemModel setQcDiscountModelProperties(HashMap modelDetailHM, Integer terminalId) throws Exception {
        setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));
        setEmployeeId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("EMPLOYEEID")));
        setTransactionId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("TRANSACTIONID")));
        setQcDiscountId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("DISCOUNTID")));
        setTransactionDate(CommonAPI.convertModelDetailToString(modelDetailHM.get("TRANSACTIONDATE")));
        setAmount(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("AMOUNT")));
        setTransComment(CommonAPI.convertModelDetailToString(modelDetailHM.get("TRANSCOMMENT")));
        setPaTransLineItemId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PATRANSLINEITEMID")));
        setTerminalId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("TERMINALID")));

        if (super.getItemComment() != null && super.getItemComment().length() > 0 && super.getItemComment().contains("QC Discount")) {
            //The QC Discount Id is in the Item Comment field

            Integer indexStart = 15;

            String parsedId = super.getItemComment().substring(indexStart, super.getItemComment().length());

            if (parsedId != null && parsedId.trim().length() > 0 ) {
            setQcDiscountId(Integer.parseInt(parsedId.trim()));
            }
        }

        if (this.getId() != null && !this.getId().toString().isEmpty()){
            this.setQcDiscount(QcDiscountModel.getQcDiscountModel(this.getQcDiscountId(), terminalId));

        }


        return this;
    }

    public static QcDiscountLineItemModel getOneQcDiscountTransactionLine(Integer qcDiscountId, Integer terminalId) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> qcDiscountList = dm.parameterizedExecuteQuery("data.posapi30.getOneQCDiscountByDiscountId",
                new Object[]{ qcDiscountId },
                true
        );

        //product list is null or empty, throw exception
        //CommonAPI.checkIsNullOrEmptyList(qcDiscountList, "Product Not Found");
        return QcDiscountLineItemModel.createQcDiscountLineItem(qcDiscountList.get(0), terminalId);

    }

    public static QcDiscountLineItemModel createQcDiscountLineItem(HashMap qcDiscountHM, Integer terminalId) throws Exception {
        QcDiscountLineItemModel qcDiscountLineItemModel = new QcDiscountLineItemModel();
        qcDiscountLineItemModel = qcDiscountLineItemModel.setQcDiscountModelProperties(qcDiscountHM, terminalId);

        return qcDiscountLineItemModel;

    }

    public QcDiscountModel getQcDiscount() {
        return qcDiscount;
    }

    public void setQcDiscount(QcDiscountModel qcDiscount) {
        this.qcDiscount = qcDiscount;
    }

    //@Override
    @JsonIgnore
    public  Integer getItemTypeId(){
        return PosAPIHelper.ObjectType.QCDISCOUNT.toInt();
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public String getTransComment() {
        return transComment;
    }

    public void setTransComment(String transComment) {
        this.transComment = transComment;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(Integer terminalId) {
        this.terminalId = terminalId;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getQcDiscountId() {
        return qcDiscountId;
    }

    public void setQcDiscountId(Integer qcDiscountId) {
        this.qcDiscountId = qcDiscountId;
    }

    public Integer getPaTransLineItemId() {
        return paTransLineItemId;
    }

    public void setPaTransLineItemId(Integer paTransLineItemId) {
        this.paTransLineItemId = paTransLineItemId;
    }

}
