package com.mmhayes.common.transaction.models;

//mmhayes dependencies
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.product.models.ModifierCalculation;
import com.mmhayes.common.product.models.ItemTaxModel;
import com.mmhayes.common.product.collections.ProductCollectionCalculation;
import com.mmhayes.common.product.models.ProductCalculation;

//API dependencies
import com.mmhayes.common.api.errorhandling.exceptions.MissingDataException;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;

//other dependencies
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.util.HashMap;

/*
Last Updated (automatically updated by SVN)
$Author: ijgerstein $: Author of last commit
$Date: 2021-03-15 14:50:18 -0400 (Mon, 15 Mar 2021) $: Date of last commit
$Rev: 13649 $: Revision of last commit
*/

public class TaxCalculation {
    private Integer id; //tax id
    private Integer itemTypeID;
    private TaxModel taxModel; //contains all of the tax properties
    private BigDecimal taxableAmount = BigDecimal.ZERO; //the total taxable for this taxrate
    private BigDecimal taxAmount = null; //the total tax for this taxrate
    private HashMap<Integer, BigDecimal> lineTaxes = new HashMap<Integer, BigDecimal>(); //holds taxableAmounts for each index of productLine
    private int roundingMode = BigDecimal.ROUND_HALF_UP;

    public TaxCalculation() throws Exception {

    }

    public TaxCalculation(Integer taxId, Integer terminalId) throws Exception {
        TaxModel taxModel = TaxModel.getTaxModelFromCache(taxId, terminalId, false);
        this.setId(taxId);
        this.setTaxModel(taxModel);
    }

    public void determineTaxableAmount(ProductCollectionCalculation productCollectionCalculation, Integer terminalId) throws Exception {
        this.setTaxableAmount(productCollectionCalculation.getTaxableAmounts(this.getId(), terminalId).getTaxableAmount());
    }

    //calculates the tax amount based on the current set taxable amount
    public void calculateTaxAmount() throws Exception {
        //tax rate is stored as a decimal value
        BigDecimal taxRate = this.getTaxModel().getTaxRate(); //either percent or coupon amount
        BigDecimal taxAmount = this.getTaxableAmount().multiply(taxRate);

        this.setTaxAmount(taxAmount.setScale(5, this.getRoundingMode()));
    }

    //calculates the tax for a product line based on the data in lineTaxs hashmap
    public BigDecimal calculateProductLineTaxAmount(Integer index) throws Exception {
        BigDecimal lineTaxAmount;

        //if no valid taxable amount set or if it is zero, just return zero
        if ( !this.getLineTaxes().containsKey(index) || this.getLineTaxes().get(index) == null || this.getLineTaxes().get(index).compareTo(BigDecimal.ZERO) == 0 || this.getTaxableAmount().setScale(5, this.getRoundingMode()).compareTo(BigDecimal.ZERO) == 0 ) {
            return BigDecimal.ZERO;
        }

        //lineTax = totalTax * (lineTaxAble/totalTaxable)
        BigDecimal linePercentOfTotal = this.getLineTaxes().get(index).divide(this.getTaxableAmount().setScale(5, this.getRoundingMode()), this.getRoundingMode());
        lineTaxAmount = this.getTaxAmount().multiply(linePercentOfTotal);

        return lineTaxAmount.setScale(5, this.getRoundingMode());
    }

    public void determineItemTypeID(TransactionModel transactionModel, ProductCollectionCalculation productCollectionCalculation) {
        setItemTypeID(PosAPIHelper.ObjectType.TAX.toInt());

        if(transactionModel.isTaxDelete(this.getId(), productCollectionCalculation)) {
            setItemTypeID(PosAPIHelper.ObjectType.TAX_DELETE.toInt());
        }
    }

    //validates and applies the tax based on tax application type
    public void applyTax(TransactionModel transactionModel, ProductCollectionCalculation productCollectionCalculation, Boolean posaRefund) throws Exception {
        if ( this.getTaxableAmount() == null ) {
            throw new MissingDataException("Invalid Taxable Amount.");
        }

        //if tax hasn't been calculated yet, calculate it
        if ( this.getTaxAmount() == null ) {
            this.calculateTaxAmount();
        }

        //ensure that the tax amount is calculated now
        if ( this.getTaxAmount() == null ) {
            throw new MissingDataException("Invalid Tax Amount");
        }

        if (posaRefund) {
            this.setTaxAmount(this.getTaxAmount().multiply(new BigDecimal(-1)));
        }

        this.determineItemTypeID(transactionModel, productCollectionCalculation);

        //build taxLineItemModel and attach to transModel
        transactionModel.getTaxes().add(this.buildTaxLineItemModel(posaRefund));

        for (ProductCalculation productCalculation : productCollectionCalculation.getCollection() ) {
            //calculate line tax
            BigDecimal lineEligible = this.getLineTaxes().containsKey(productCalculation.getIndex()) ? this.getLineTaxes().get(productCalculation.getIndex()) : BigDecimal.ZERO;
            BigDecimal lineTax = this.calculateProductLineTaxAmount(productCalculation.getIndex());

            //if no modifiers
            if ( productCalculation.getModifiers() == null || productCalculation.getModifiers().size() == 0 ) {
                if ( !productCalculation.getProductModel().isTaxValidForProduct(this.getId()) ) {
                    continue;
                }

                //build and attach ItemTaxDisc models to transmodel -> productlines
                ItemTaxModel lineItemTaxModel = this.buildTaxItemTaxModel(lineEligible, lineTax, posaRefund);
                transactionModel.getProducts().get(productCalculation.getIndex()).getTaxes().add(lineItemTaxModel);
            } else {
                //make an Item Tax Discount line for the main product, if eligible
                if ( productCalculation.getProductModel().isTaxValidForProduct(this.getId()) ) {
                    // Get the base price of the main product and add the price of the prep option
                    BigDecimal productEligible = productCalculation.getCurrentPrice(true, false, null, this.getId());
                    if (productCalculation.getPrepOption() != null) {
                        productEligible = productEligible.add(productCalculation.getPrepOption().getPrice().multiply(productCalculation.getQuantity()));
                    }
                    productEligible = productEligible.setScale(4, this.getRoundingMode());

                    // Calculate the tax on the main product with prep option
                    BigDecimal productTax = lineEligible.compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ZERO : productEligible.divide(lineEligible, this.getRoundingMode()).multiply(lineTax).setScale(4, this.getRoundingMode());

                    ItemTaxModel itemTaxModel = this.buildTaxItemTaxModel(productEligible.setScale(2, this.getRoundingMode()), productTax, posaRefund);
                    transactionModel.getProducts().get(productCalculation.getIndex()).getTaxes().add(itemTaxModel);
                }

                //make an Item Tax Discount line for each eligible modifier
                for ( ModifierCalculation modifierCalculation : productCalculation.getModifiers() ) {
                    if ( !modifierCalculation.getProductModel().isTaxValidForProduct(this.getId()) ) {
                        continue;
                    }

                    BigDecimal modEligible = modifierCalculation.getCurrentPrice(true, this.getId()).setScale(4, this.getRoundingMode());

                    if ( modEligible.setScale(2, this.getRoundingMode()).compareTo(BigDecimal.ZERO) == 0 ) {
                        continue;
                    }

                    BigDecimal modTax = lineEligible.compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ZERO : modEligible.divide(lineEligible, this.getRoundingMode()).multiply(lineTax).setScale(4, this.getRoundingMode());

                    ItemTaxModel itemTaxModel = this.buildTaxItemTaxModel(modEligible.setScale(2, this.getRoundingMode()), modTax, posaRefund);
                    transactionModel.getProducts().get(productCalculation.getIndex()).getTaxes().add(itemTaxModel);
                }
            }
        }
    }

    public TaxLineItemModel buildTaxLineItemModel(Boolean posaRefund) throws Exception {
        TaxLineItemModel taxLineItemModel = new TaxLineItemModel();
        taxLineItemModel.setItemId(this.getId());

        if (posaRefund) {
            taxLineItemModel.setQuantity(new BigDecimal(-1));
        }
        else {
            taxLineItemModel.setQuantity(BigDecimal.ONE);
        }

        taxLineItemModel.setEligibleAmount(this.getTaxableAmount());
        taxLineItemModel.setAmount(this.getTaxAmount());
        taxLineItemModel.setTax(this.getTaxModel());
        taxLineItemModel.setItemTypeId(this.getItemTypeID());

        return taxLineItemModel;
    }

    public ItemTaxModel buildTaxItemTaxModel(BigDecimal lineEligible, BigDecimal lineTax, Boolean posaRefund) throws Exception {
        ItemTaxModel itemTaxModel = new ItemTaxModel();
        itemTaxModel.setItemId(this.getId());
        itemTaxModel.setEligibleAmount(lineEligible);
        if (posaRefund) {
            itemTaxModel.setAmount(lineTax.multiply(new BigDecimal(-1)));
        }
        else {
            itemTaxModel.setAmount(lineTax);
        }
        itemTaxModel.setTax(this.getTaxModel());
        itemTaxModel.setItemTypeId(this.getItemTypeID());

        return itemTaxModel;
    }

    //adds a taxable amount to the appropriate lineTax key/value, and updates the total taxable
    public void addLineTaxable(Integer index, BigDecimal amount) {
        //update line taxable
        if ( !this.getLineTaxes().containsKey(index) ) {
            this.getLineTaxes().put(index, BigDecimal.ZERO);
        }

        //if the amount is 0 after rounding, then it is zero
        if ( amount.setScale(4, this.getRoundingMode()).compareTo( BigDecimal.ZERO ) == 0 ) {
            amount = BigDecimal.ZERO;
        }

        this.getLineTaxes().put(index, this.getLineTaxes().get(index).add(amount));

        //update total taxable
        this.setTaxableAmount(this.getTaxableAmount().add(amount));
    }

    //GETTERS AND SETTERS
    public int getRoundingMode() {
        return roundingMode;
    }

    public void setRoundingMode(int roundingMode) {
        this.roundingMode = roundingMode;
    }

    public HashMap<Integer, BigDecimal> getLineTaxes() {
        return lineTaxes;
    }

    public void setLineTaxes(HashMap<Integer, BigDecimal> lineTaxes) {
        this.lineTaxes = lineTaxes;
    }

    public BigDecimal getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(BigDecimal taxAmount) {
        this.taxAmount = taxAmount;
    }

    public BigDecimal getTaxableAmount() {
        return taxableAmount;
    }

    public void setTaxableAmount(BigDecimal taxableAmount) {
        this.taxableAmount = taxableAmount;
    }

    public TaxModel getTaxModel() {
        return taxModel;
    }

    public void setTaxModel(TaxModel taxModel) {
        this.taxModel = taxModel;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getItemTypeID() {
        return itemTypeID;
    }

    public void setItemTypeID(Integer itemTypeID) {
        this.itemTypeID = itemTypeID;
    }

}
