package com.mmhayes.common.transaction.models;


//MMHayes dependencies
import com.mmhayes.common.account.models.AccountModel;

//API dependencies
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;

import java.util.HashMap;

/*
 Last Updated (automatically updated by SVN)
 $Author: nyu $: Author of last commit
 $Date: 2020-04-13 18:08:27 -0400 (Mon, 13 Apr 2020) $: Date of last commit
 $Rev: 11350 $: Revision of last commit
*/
public class ReceivedOnAccountLineItemModel extends TransactionLineItemModel {
    private ReceivedOnAccountModel receivedOnAccount = null;
    private AccountModel account = null;
    private QcTransactionModel qcTransaction = null;
    private String creditCardTransInfo = "";

    public ReceivedOnAccountLineItemModel() {

    }

    public void setReceivedOnAccountModelProperties(HashMap modelDetailHM, Integer terminalId) throws Exception {
        this.setModelProperties(modelDetailHM);
        this.setCreditCardTransInfo(CommonAPI.convertModelDetailToString(modelDetailHM.get("CREDITCARDTRANSINFO")));
    }

    public ReceivedOnAccountModel getReceivedOnAccount() {
        return receivedOnAccount;
    }

    public void setReceivedOnAccount(ReceivedOnAccountModel receivedOnAccount) {
        this.receivedOnAccount = receivedOnAccount;
    }

    @JsonIgnore //not returning this for this transaction line item type.  This should be assumed I think.  Trying to clean up the json.
    public @Override Integer getItemTypeId(){
        return PosAPIHelper.ObjectType.RECEIVEDONACCOUNT.toInt();
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public AccountModel getAccount() {
        return account;
    }

    public void setAccount(AccountModel account) {
        this.account = account;
    }

    @JsonIgnore
    public QcTransactionModel getQcTransaction() {
        return qcTransaction;
    }

    public void setQcTransaction(QcTransactionModel qcTransaction) {
        this.qcTransaction = qcTransaction;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public String getCreditCardTransInfo() {
        return creditCardTransInfo;
    }

    public void setCreditCardTransInfo(String creditCardTransInfo) {
        this.creditCardTransInfo = creditCardTransInfo;
    }
}
