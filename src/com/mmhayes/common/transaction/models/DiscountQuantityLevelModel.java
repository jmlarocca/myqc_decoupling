package com.mmhayes.common.transaction.models;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.product.models.ProductCalculation;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.StringFunctions;

import java.math.BigDecimal;
import java.util.*;

/*
 Last Updated (automatically updated by SVN)
 $Author: jmdottavio $: Author of last commit
 $Date: 2018-03-20 11:19:43 -0400 (Tue, 20 Mar 2018) $: Date of last commit
 $Rev: 6640 $: Revision of last commit
*/
@JsonPropertyOrder({"id", "name", "type", "amount"})
public class DiscountQuantityLevelModel {

    private static DataManager dm = new DataManager();

    private Integer id = null;
    private Integer discountId = null;
    private Integer productId = null;
    private Integer subDepartmentId = null;
    private Integer departmentId = null;
    private Integer maxQuantity = null;
    private Integer minQuantity = null;
    private BigDecimal previousProductCount = BigDecimal.ZERO;

    private List<ProductCalculation> eligibleProducts = new ArrayList<ProductCalculation>(); //holds the list products that apply to this level

    //default constructor
    public DiscountQuantityLevelModel() {

    }

    //Constructor- takes in a Hashmap
    public DiscountQuantityLevelModel(HashMap DiscountHM) {
        setModelProperties(DiscountHM);
    }

    //setter for all of this model's properties
    public DiscountQuantityLevelModel setModelProperties(HashMap modelDetailHM) {
        setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));
        setDiscountId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PADISCOUNTID")));
        setProductId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAPLUID")));
        setSubDepartmentId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PASUBDEPTID")));
        setDepartmentId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PADEPARTMENTID")));
        setMaxQuantity(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("MAXQUANTITY")));
        setMinQuantity(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("MINQUANTITY")));

        return this;
    }

    //for sub dept / dept discount quantity levels - removes any eligible quantity of a productId over the maxProductQuantity
    public void restrictProductQuantity(Integer productId, BigDecimal maxProductQuantity) {
        BigDecimal productCount = BigDecimal.ZERO;

        for ( ProductCalculation productCalculation : this.getEligibleProducts() ) {
            if ( !productCalculation.getId().equals( productId ) ) {
                continue;
            }

            //if the maxProductQuantity has been reached
            if ( productCount.compareTo( maxProductQuantity ) == 0 ) {
                productCalculation.setQuantity(BigDecimal.ZERO);
                continue;
            }

            BigDecimal productQuantity = productCalculation.getQuantity();

            //count up to maxProductQuantity over multiple potential eligibleProducts (product split across multiple lines)
            if ( productQuantity.add( productCount ).compareTo( maxProductQuantity ) > 0 ) {
                productCalculation.setQuantity( maxProductQuantity.subtract( productCount ) );
                productCount = maxProductQuantity;
            } else {
                productCount = productCount.add( productQuantity );
            }
        }
    }

    //for dept discount quantity levels - removes any eligible quantity of products (sorted by price desc) within the subdept over the maxSubDeptQuantity
    public void restrictSubDeptQuantity(Integer subDepartmentId, BigDecimal maxSubDeptQuantity) {
        List<ProductCalculation> productsToRemove = new ArrayList<>();
        BigDecimal productCount = BigDecimal.ZERO;

        sortEligibleProductsByPrice();

        for ( ProductCalculation productCalculation : this.getEligibleProducts() ) {
            if ( productCalculation.getProductModel().getSubDepartmentId().compareTo(subDepartmentId) != 0) {
                continue;
            }

            //if restricting to zero, or if the maxProductQuantity has been reached, simply remove the product from the eligible list
            if ( maxSubDeptQuantity.compareTo( BigDecimal.ZERO ) == 0 || productCount.compareTo( maxSubDeptQuantity ) == 0 ) {
                productsToRemove.add( productCalculation );
                continue;
            }

            BigDecimal productQuantity = productCalculation.getQuantity();

            //count up to maxProductQuantity over multiple potential eligibleProducts (product split across multiple lines)
            if ( productQuantity.add( productCount ).compareTo( maxSubDeptQuantity ) > 0 ) {
                productCalculation.setQuantity( maxSubDeptQuantity.subtract( productCount ) );
                productCount = maxSubDeptQuantity;
            } else {
                productCount = productCount.add( productQuantity );
            }
        }

        for ( int i = productsToRemove.size() - 1; i >= 0; i-- ) {
            this.getEligibleProducts().remove( productsToRemove.get(i) );
        }
    }

    //Split this method out to handle manual refund transactions
    public boolean determineProductsForBestDiscount() {
        BigDecimal totalProductCount = getPreviousProductCount();

        for (ProductCalculation productCalculation : this.getEligibleProducts()) {
            totalProductCount = totalProductCount.add(productCalculation.getQuantity());
        }

        if (totalProductCount.compareTo(BigDecimal.ZERO) == -1) {
            //refund
            return determineProductsForRefund(totalProductCount);
        } else {
            //sale
            return determineProductsForSale(totalProductCount);
        }
    }

    public boolean determineProductsForRefund(BigDecimal totalProductCount) {
        BigDecimal tempMaxQuantityBD = this.getMaxQuantityBD().negate();

        //if there are less products than the max allows, all are actually eligible
        if (totalProductCount.compareTo(tempMaxQuantityBD) > 0) {
            return false;
        }

        //more products than max quantity, need to choose the max quantity best (highest price)
        sortEligibleProductsByPriceDesc();

        //look through eligible products and allow up to maxQuantity
        int cutoffIndex = 0;
        BigDecimal currentProductCount = BigDecimal.ZERO;

        for (int i = 0; i < this.getEligibleProducts().size(); i++) {
            ProductCalculation productCalculation = this.getEligibleProducts().get(i);
            Integer addQuantityResult = currentProductCount.add(productCalculation.getQuantity()).compareTo(tempMaxQuantityBD);

            //all quantity of this product are eligible
            if (addQuantityResult > 0) {
                currentProductCount = currentProductCount.add(productCalculation.getQuantity());
                continue;
            } else if (addQuantityResult == 0) {
                //include all quantity, max quantity reached
                cutoffIndex = i + 1;
                break;
            }

            BigDecimal remainingQuantityAllowed = tempMaxQuantityBD.subtract(currentProductCount);
            if (remainingQuantityAllowed.compareTo(BigDecimal.ZERO) >= 0) {
                productCalculation.setQuantity(BigDecimal.ZERO);  //HP2650 - Don't set the quantity to a negative
            } else {
                productCalculation.setQuantity(remainingQuantityAllowed);
            }
            cutoffIndex = i + 1;
            break;
        }

        this.setEligibleProducts(this.getEligibleProducts().subList(0, cutoffIndex));

        return true;
    }

    public boolean determineProductsForSale(BigDecimal totalProductCount) {
        //if there are less products than the max allows, all are actually eligible
        if (totalProductCount.compareTo(this.getMaxQuantityBD()) <= 0) {
            return false;
        }

        //more products than max quantity, need to choose the max quantity best (highest price)
        sortEligibleProductsByPrice();

        //look through eligible products and allow up to maxQuantity
        int cutoffIndex = 0;
        BigDecimal currentProductCount = getPreviousProductCount();

        for (int i = 0; i < this.getEligibleProducts().size(); i++) {
            ProductCalculation productCalculation = this.getEligibleProducts().get(i);
            Integer addQuantityResult = currentProductCount.add(productCalculation.getQuantity()).compareTo(this.getMaxQuantityBD());

            //all quantity of this product are eligible
            if (addQuantityResult < 0) {
                currentProductCount = currentProductCount.add(productCalculation.getQuantity());
                continue;
            } else if (addQuantityResult == 0) {
                //include all quantity, max quantity reached
                cutoffIndex = i + 1;
                break;
            }

            //adding this whole quantity would result in going over max, restrict the quantity
            BigDecimal remainingQuantityAllowed = this.getMaxQuantityBD().subtract( currentProductCount );
            if (remainingQuantityAllowed.compareTo(BigDecimal.ZERO) < 0){
                productCalculation.setQuantity(BigDecimal.ZERO);  //HP2650 - Don't set the quantity to a negative
            } else {
                productCalculation.setQuantity( remainingQuantityAllowed );
            }
            cutoffIndex = i + 1;
            break;
        }

        this.setEligibleProducts(this.getEligibleProducts().subList(0, cutoffIndex));

        return true;
    }

    public void sortEligibleProductsByPrice() {
        //comparator that compares discountable amount for each product
        Comparator<ProductCalculation> productComparison = (ProductCalculation p1, ProductCalculation p2) -> ( p1.getCurrentPrice(false, true, this.getDiscountId(), null) ).compareTo( p2.getCurrentPrice(false, true, this.getDiscountId(), null) );

        //sort by the comparator
//        this.getEligibleProducts().sort(productComparison);
        Collections.sort(this.getEligibleProducts(), Collections.reverseOrder(productComparison) );
    }

    public void sortEligibleProductsByPriceDesc() {
        //comparator that compares discountable amount for each product
        Comparator<ProductCalculation> productComparison = (ProductCalculation p1, ProductCalculation p2) -> ( p1.getCurrentPrice(false, true, this.getDiscountId(), null) ).compareTo( p2.getCurrentPrice(false, true, this.getDiscountId(), null) );

        //sort lowest first for refund
        this.getEligibleProducts().sort(productComparison);
    }

    public BigDecimal calculateTotalProductAmount() {
        BigDecimal total = BigDecimal.ZERO;

        for ( ProductCalculation productCalculation : this.getEligibleProducts() ) {
            total = total.add( productCalculation.getCurrentPrice(true, true, this.getDiscountId(), null) );
        }

        return total;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDiscountId() {
        return discountId;
    }

    public void setDiscountId(Integer discountId) {
        this.discountId = discountId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getSubDepartmentId() {
        return subDepartmentId;
    }

    public void setSubDepartmentId(Integer subDepartmentId) {
        this.subDepartmentId = subDepartmentId;
    }

    public Integer getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }

    public Integer getMaxQuantity() {
        return maxQuantity;
    }

    public BigDecimal getMaxQuantityBD() {
        return new BigDecimal( this.getMaxQuantity().toString() ) ;
    }

    public void setMaxQuantity(Integer maxQuantity) {
        this.maxQuantity = maxQuantity;
    }

    public Integer getMinQuantity() {
        return minQuantity;
    }

    public void setMinQuantity(Integer minQuantity) {
        this.minQuantity = minQuantity;
    }

    public List<ProductCalculation> getEligibleProducts() {
        return eligibleProducts;
    }

    public void setEligibleProducts(List<ProductCalculation> eligibleProducts) {
        this.eligibleProducts = eligibleProducts;
    }

    public void incrementPreviousProductCount( BigDecimal amount ) {
        setPreviousProductCount( getPreviousProductCount().add( amount ) );
    }

    public BigDecimal getPreviousProductCount() {
        return previousProductCount;
    }

    public void setPreviousProductCount(BigDecimal previousProductCount) {
        this.previousProductCount = previousProductCount;
    }

    /**
     * Overridden toString method for a DiscountQuantityLevelModel.
     * @return The {@link String} representation of a DiscountQuantityLevelModel.
     */
    @Override
    public String toString () {

        String eligibleProductsStr = "";
        if (!DataFunctions.isEmptyCollection(eligibleProducts)) {
            eligibleProductsStr += "[";
            for (ProductCalculation eligibleProduct : eligibleProducts) {
                if (eligibleProduct != null && eligibleProduct.equals(eligibleProducts.get(eligibleProducts.size() - 1))) {
                    eligibleProductsStr += "[" + eligibleProduct.toString() + "]";
                }
                else if (eligibleProduct != null && !eligibleProduct.equals(eligibleProducts.get(eligibleProducts.size() - 1))) {
                    eligibleProductsStr += "[" + eligibleProduct.toString() + "]; ";
                }
            }
            eligibleProductsStr += "]";
        }

        return String.format("ID: %s, DISCOUNTID: %s, PRODUCTID: %s, SUBDEPARTMENTID: %s, DEPARTMENTID: %s, " +
                "MAXQUANTITY: %s, MINQUANTITY: %s, PREVIOUSPRODUCTCOUNT: %s, ELIGIBLEPRODUCTS: %s",
                Objects.toString((id != null && id > 0 ? id : "N/A"), "N/A"),
                Objects.toString((discountId != null && discountId > 0 ? discountId : "N/A"), "N/A"),
                Objects.toString((productId != null && productId > 0 ? productId : "N/A"), "N/A"),
                Objects.toString((subDepartmentId != null && subDepartmentId > 0 ? subDepartmentId : "N/A"), "N/A"),
                Objects.toString((departmentId != null && departmentId > 0 ? departmentId : "N/A"), "N/A"),
                Objects.toString((maxQuantity != null && maxQuantity > 0 ? maxQuantity : "N/A"), "N/A"),
                Objects.toString((minQuantity != null && minQuantity > 0 ? minQuantity : "N/A"), "N/A"),
                Objects.toString((previousProductCount != null ? previousProductCount.toPlainString() : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(eligibleProductsStr) ? eligibleProductsStr : "N/A"), "N/A"));
    }

}




