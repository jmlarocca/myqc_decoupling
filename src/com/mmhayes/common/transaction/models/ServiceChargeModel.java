package com.mmhayes.common.transaction.models;

//mmhayes dependencies

import com.mmhayes.common.product.models.ProductModel;

//API dependencies
import com.mmhayes.common.api.CommonAPI;

//other dependencies
import java.util.HashMap;

/*
 $Author: eglundin $: Author of last commit
 $Date: 2019-03-25 08:10:10 -0400 (Mon, 25 Mar 2019) $: Date of last commit
 $Rev: 8585 $: Revision of last commit
 Notes:
*/
public class ServiceChargeModel extends ProductModel {

    public ServiceChargeModel() {

    }

    //Constructor- takes in a Hashmap
    public ServiceChargeModel(HashMap modelHM) {
        setModelProperties(modelHM);
    }

    //setter for all of this model's properties
    public ServiceChargeModel setModelProperties(HashMap modelDetailHM) {
        setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));
        setName(CommonAPI.convertModelDetailToString(modelDetailHM.get("NAME")));

        return this;
    }

    public ServiceChargeModel(SurchargeModel surchargeModel) {
        super.setId(surchargeModel.getId());
        super.setName(surchargeModel.getName());
    }
}

