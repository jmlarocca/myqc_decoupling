package com.mmhayes.common.transaction.models;

//api dependencies

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.terminal.models.TerminalModel;
import io.swagger.annotations.ApiModelProperty;

//Other Dependencies
import java.util.ArrayList;
import java.util.HashMap;

/*
 Last Updated (automatically updated by SVN)
 $Author: jmkimber $: Author of last commit
 $Date: 2020-12-30 14:23:57 -0500 (Wed, 30 Dec 2020) $: Date of last commit
 $Rev: 13369 $: Revision of last commit
*/

public class TenderDisplayModel extends TenderModel implements ITenderModel {

    public TenderDisplayModel() {

    }

    public TenderDisplayModel(HashMap TenderHM) {
        setModelProperties(TenderHM);
    }

    public TenderDisplayModel(Integer tenderId) {
        this.setId(tenderId);
    }

    public static TenderDisplayModel createTenderModel(HashMap tenderHM) {
        return new TenderDisplayModel(tenderHM);
    }

    public static TenderDisplayModel createTenderModel(TenderModel tenderModel) {
        TenderDisplayModel tenderDisplayModel = new TenderDisplayModel();

        tenderDisplayModel.setAllowOverTender(tenderModel.isAllowOverTender());
        tenderDisplayModel.setAllowUnderTender(tenderModel.isAllowUnderTender());
        tenderDisplayModel.setId(tenderModel.getId());
        tenderDisplayModel.setName(tenderModel.getName());
        tenderDisplayModel.setTenderTypeId(tenderModel.getTenderTypeId());
        tenderDisplayModel.setTenderTypeName(tenderModel.getTenderTypeName());
        tenderDisplayModel.setPrintSignatureReceipt(tenderModel.isPrintSignatureReceipt());
        tenderDisplayModel.setNoSignatureUnderAmt(tenderModel.getNoSignatureUnderAmt());
        tenderDisplayModel.setPrintAccountLastFour(tenderModel.isPrintAccountLastFour());

        return tenderDisplayModel;
    }

    //get an active tender, by revenue center mapping
    public static TenderDisplayModel getActiveTenderModel(Integer tenderId, TerminalModel terminalModel) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> tenderList = dm.parameterizedExecuteQuery("data.posapi30.getOneActiveTenderById",
                new Object[]{ tenderId, terminalModel.getRevenueCenterId() },
                PosAPIHelper.getLogFileName(terminalModel.getId()),
                true
        );

        //tender list is null or empty, throw exception
        CommonAPI.checkIsNullOrEmptyList(tenderList, "Tender Not Found", terminalModel.getId());
        return TenderDisplayModel.createTenderModel(tenderList.get(0));

    }

    //get an active tender, by revenue center mapping
    public static TenderDisplayModel getActiveTenderModel(String tenderName, TerminalModel terminalModel) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> tenderList = dm.parameterizedExecuteQuery("data.posapi30.getOneActiveTenderByName",
                new Object[]{ tenderName, terminalModel.getRevenueCenterId() },
                PosAPIHelper.getLogFileName(terminalModel.getId()),
                true
        );

        //tender list is null or empty, throw exception
        CommonAPI.checkIsNullOrEmptyList(tenderList, "Tender Not Found", terminalModel.getId());
        return TenderDisplayModel.createTenderModel(tenderList.get(0));
    }

    //get an active tender, where the tender is the refund tender for the given tender id, by revenue center mapping
    public static TenderDisplayModel getActiveRefundTenderModel(Integer tenderId, TerminalModel terminalModel) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> tenderList = dm.parameterizedExecuteQuery("data.posapi30.getOneActiveRefundTenderByTenderId",
                new Object[]{ tenderId, terminalModel.getRevenueCenterId() },
                PosAPIHelper.getLogFileName(terminalModel.getId()),
                true
        );

        //tender list is null or empty, throw exception
        CommonAPI.checkIsNullOrEmptyList(tenderList, "Tender Not Found", terminalModel.getId());
        return TenderDisplayModel.createTenderModel(tenderList.get(0));

    }

    //get an active tender, where the tender is the change tender for the given tender id, by revenue center mapping
    public static TenderDisplayModel getActiveChangeTenderModel(Integer tenderId, TerminalModel terminalModel) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> tenderList = dm.parameterizedExecuteQuery("data.posapi30.getOneActiveChangeTenderByTenderId",
                new Object[]{ tenderId, terminalModel.getRevenueCenterId() },
                PosAPIHelper.getLogFileName(terminalModel.getId()),
                true
        );

        //tender list is null or empty, throw exception
        CommonAPI.checkIsNullOrEmptyList(tenderList, "Tender Not Found", terminalModel.getId());
        return TenderDisplayModel.createTenderModel(tenderList.get(0));

    }

    @ApiModelProperty(value = "Tender ID.", dataType = "Integer", required = false,
            allowableValues = "1, 2, 3, 4, 5, etc.", notes = "Relates to the QC_PATenderType.PATenderID table and field.")
    public Integer getId() {
        return id;
    }

    //setter
    public void setId(Integer id) {
        this.id = id;
    }

    //getter
    @ApiModelProperty(value = "Name of the Tender.", dataType = "String", required = true,
            example = "quickcharge", allowableValues = "quickcharge, cash, credit card, check, etc.",
            notes = "Relates to the QC_PATender.Name table and field.")
    public String getName() {
        return name;
    }

    //setter
    public void setName(String name) {
        this.name = name;
    }

    @ApiModelProperty(value = "Tender Type ID.", dataType = "Integer", required = false,
            allowableValues = "1, 2, 3, 4, 5, etc.",
            notes = "Relates to the QC_PATenderType.PATenderTypeID table and field.")
    @Override
    @JsonIgnore
    public Integer getTenderTypeId() {
        return tenderTypeId;
    }

    public void setTenderTypeId(Integer tenderTypeId) {
        this.tenderTypeId = tenderTypeId;
    }

    @ApiModelProperty(value = "Tender Type Name.", dataType = "String", required = false,
            allowableValues = "cash, credit card, check, quickcharge, etc.",
            notes = "Relates to the QC_PATenderType.Name table and field.")
    @Override
    @JsonIgnore
    public String getTenderTypeName() {
        return tenderTypeName;
    }

    public void setTenderTypeName(String tenderTypeName) {
        this.tenderTypeName = tenderTypeName;
    }

    @ApiModelProperty(value = "Does tender allow over tender.", dataType = "boolean", required = false,
            allowableValues = "true, false",
            notes = "Relates to the QC_PATender.AllowOverTender table and field.")
    @Override
    @JsonIgnore
    public boolean isAllowOverTender() {
        return allowOverTender;
    }

    public void setAllowOverTender(boolean allowOverTender) {
        this.allowOverTender = allowOverTender;
    }

    @ApiModelProperty(value = "Does tender allow under tender.", dataType = "boolean", required = false,
            allowableValues = "true, false",
            notes = "Relates to the QC_PATender.AllowOverTender table and field.")
    @Override
    @JsonIgnore
    public boolean isAllowUnderTender() {
        return allowUnderTender;
    }

    public void setAllowUnderTender(boolean allowUnderTender) {
        this.allowUnderTender = allowUnderTender;
    }
}
