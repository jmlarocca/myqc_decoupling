package com.mmhayes.common.transaction.collections;

import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.transaction.models.*;
import com.mmhayes.common.dataaccess.DataManager;

import java.util.ArrayList;
import java.util.HashMap;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2017-04-20 16:34:33 -0400 (Thu, 20 Apr 2017) $: Date of last commit
 $Rev: 3849 $: Revision of last commit
*/
public class QcTransactionLineItemCollection {

    public QcTransactionLineItemCollection() {

    }

    private ArrayList<QcTransactionLineItemModel> collection = new ArrayList<>();

    public static QcTransactionLineItemCollection getQcTransactionLineItems(Integer qcTransactionId, Integer terminalId) throws Exception {
        DataManager dm = new DataManager();
        QcTransactionLineItemCollection qcTransactionLineItemCollection = new QcTransactionLineItemCollection();

        //get all models in an array list
        ArrayList<HashMap> qcTransactionLineItemListHM = dm.parameterizedExecuteQuery("data.posapi30.getQcTransactionLineItems",
                new Object[]{
                        qcTransactionId
                },
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        for (HashMap modelHM : qcTransactionLineItemListHM) {
            //CommonAPI.checkIsNullOrEmptyObject(modelHM.get("ID"), "LoyaltyProgramId Id could not be found");
            qcTransactionLineItemCollection.getCollection().add(QcTransactionLineItemModel.createQcTransactionLineItem(modelHM, terminalId));  //create a new model and add to the collection
        }

        return qcTransactionLineItemCollection;

    }


    public ArrayList<QcTransactionLineItemModel> getCollection() {
        return collection;
    }

    public void setCollection(ArrayList<QcTransactionLineItemModel> collection) {
        this.collection = collection;
    }


}
