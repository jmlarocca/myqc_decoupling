package com.mmhayes.common.transaction.collections;

//MMHayes Dependencies

import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.terminal.models.TerminalModel;
import com.mmhayes.common.transaction.models.*;

import java.util.ArrayList;
import java.util.HashMap;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2018-08-29 11:12:59 -0400 (Wed, 29 Aug 2018) $: Date of last commit
 $Rev: 7701 $: Revision of last commit
*/
public class TenderLineItemCollection {
    private ArrayList<TenderLineItemModel> collection = new ArrayList<>();

    public TenderLineItemCollection() {

    }

    public static ArrayList<HashMap> getAllTenderLineItems(Integer transactionId, TerminalModel terminalModel) throws Exception {
        DataManager dm = new DataManager();
        TenderLineItemCollection tenderLineItemCollection = new TenderLineItemCollection();

        //get all models in an array list
        ArrayList<HashMap> tenderLineItemListHM = dm.parameterizedExecuteQuery("data.posapi30.getQCPosTransactionLineItemsByObjectType",
                new Object[]{transactionId, 3},
                PosAPIHelper.getLogFileName(terminalModel.getId()),
                true
        );

        return tenderLineItemListHM;
    }

    public ArrayList<TenderLineItemModel> getCollection() {
        return collection;
    }

    public void setCollection(ArrayList<TenderLineItemModel> collection) {
        this.collection = collection;
    }

}
