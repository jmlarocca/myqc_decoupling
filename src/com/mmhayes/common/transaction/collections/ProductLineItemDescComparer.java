package com.mmhayes.common.transaction.collections;

import com.mmhayes.common.transaction.models.ProductLineItemModel;

import java.util.Comparator;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2017-10-16 11:38:31 -0400 (Mon, 16 Oct 2017) $: Date of last commit
 $Rev: 5459 $: Revision of last commit
*/
public class ProductLineItemDescComparer implements Comparator<ProductLineItemModel> {

    private String fieldToSort = "";

    public ProductLineItemDescComparer() {

    }

    public ProductLineItemDescComparer(String fieldToSort) {
        this.fieldToSort = fieldToSort;
    }

    public int compare(ProductLineItemModel productLineA, ProductLineItemModel productLineB) {
        int productAmountCheck = 0;

        //fieldToSort was set to something
        switch (fieldToSort) {
            case "amount":
                productAmountCheck = productLineA.getAmount().compareTo(productLineB.getAmount());
                break;
            default:
                productAmountCheck = productLineA.getAdjAmountLoyalty().compareTo(productLineB.getAdjAmountLoyalty());
                break;
        }

        //I want to sort the products with the highest values first
        return productAmountCheck * -1;

    }
}
