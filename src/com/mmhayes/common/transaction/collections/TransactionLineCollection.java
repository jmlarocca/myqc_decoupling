package com.mmhayes.common.transaction.collections;

//mmhayes dependencies
import com.mmhayes.common.dataaccess.DataManager;

//API dependencies
import com.mmhayes.common.transaction.models.TransactionLineItemModel;
import com.mmhayes.common.account.models.AccountModel;

//other dependencies
import java.util.*;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2017-02-16 11:12:54 -0500 (Thu, 16 Feb 2017) $: Date of last commit
 $Rev: 3553 $: Revision of last commit
*/
//List of TransactionLineModels
public class TransactionLineCollection {
    private static DataManager dm = new DataManager();

    private List<TransactionLineItemModel> collection = new ArrayList<>();
    private AccountModel accountModel = null;

    //constructor - default
    public TransactionLineCollection() {
    }

    //getter
    public List<TransactionLineItemModel> getCollection() {
        return this.collection;
    }

    //setter
    public void setCollection(List<TransactionLineItemModel> collection) {
        this.collection = collection;
    }

    private AccountModel getAccountModel() {
        return accountModel;
    }

    private void setAccountModel(AccountModel accountModel) {
        this.accountModel = accountModel;
    }
}




