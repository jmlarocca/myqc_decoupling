package com.mmhayes.common.transaction.collections;

import com.mmhayes.common.transaction.models.LoyaltyRewardLineItemModel;

import java.util.Comparator;

/*
Last Updated (automatically updated by SVN)
$Author: eglundin $: Author of last commit
$Date: 2017-11-15 15:16:54 -0500 (Wed, 15 Nov 2017) $: Date of last commit
$Rev: 5677 $: Revision of last commit
*/
public class LoyaltyRewardLineItemAscComparer implements Comparator<LoyaltyRewardLineItemModel> {

    String fieldToSort = "";

    public LoyaltyRewardLineItemAscComparer() {

    }

    public LoyaltyRewardLineItemAscComparer(String fieldToSort) {
        this.fieldToSort = fieldToSort;
    }

    public int compare(LoyaltyRewardLineItemModel rewardLineA, LoyaltyRewardLineItemModel rewardLineB) {
        int iComparisonCheck = 0;

        switch (fieldToSort) {
            case "value":
                iComparisonCheck = rewardLineA.getReward().getValue().compareTo(rewardLineB.getReward().getValue());
                break;
            case "pointsToRedeem":
                iComparisonCheck = rewardLineA.getReward().getPointsToRedeem().compareTo(rewardLineB.getReward().getPointsToRedeem());
                break;
            case "extendedAmount":
                iComparisonCheck = rewardLineA.getExtendedAmount().compareTo(rewardLineB.getExtendedAmount());
                break;
            case "originalSequence":
                iComparisonCheck = rewardLineA.getLineItemSequence().compareTo(rewardLineB.getLineItemSequence());
                break;
        }

        //I want to sort the products with the highest values first
        return iComparisonCheck;
    }


}


