package com.mmhayes.common.transaction.collections;

//MMHayes Dependencies

import com.mmhayes.common.account.models.AccountModel;
import com.mmhayes.common.account.models.AccountQueryParams;
import com.mmhayes.common.api.PosApi30.PosAPIModelCache;
import com.mmhayes.common.terminal.models.TerminalModel;
import com.mmhayes.common.transaction.models.*;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.dataaccess.DataManager;

//API Dependencies
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.ModelPaginator;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.api.errorhandling.exceptions.MissingDataException;

//Other Dependencies
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/*
 Last Updated (automatically updated by SVN)
 $Author: mjbatko $: Author of last commit
 $Date: 2021-06-28 08:23:25 -0400 (Mon, 28 Jun 2021) $: Date of last commit
 $Rev: 14230 $: Revision of last commit
*/
public class TransactionSummaryCollection {

    private PosAPIModelCache posAPIModelCache = null;
    List<TransactionSummaryModel> collection = new ArrayList<>();

    public void searchRangeOfTransactionSummaries(ModelPaginator modelPaginator, TransactionSummaryQueryParams transSummaryQueryParams, TerminalModel terminalModel) throws Exception {

        DataManager dm = new DataManager();

        modelPaginator.finalizePaginationProperties();

        String sortByColumn = ""; //what column should this result set be sorted by

        if (modelPaginator.getSort().toUpperCase().equals("ID")) {
            sortByColumn = "t.PATransactionID";
        } else if (modelPaginator.getSort().toUpperCase().equals("DATE")) { //this is the default
            sortByColumn = "t.TransactionDate";
        } else if (modelPaginator.getSort().toUpperCase().equals("NAME")) { //this is the default
            sortByColumn = "t.PersonName";
        } else {
            Logger.logMessage("ERROR: Invalid sort property in TransactionCollection.populateCollection(). sort property = " + modelPaginator.getSort(), Logger.LEVEL.ERROR);
            throw new MissingDataException("Invalid sort parameter for Transaction Endpoint", terminalModel.getId());
        }

        //get a single page of models in an array list
        ArrayList<HashMap> transactionList = dm.serializeSqlWithColNames("data.posapi30.getTransactionSummariesPaginate",
                new Object[]{
                        (transSummaryQueryParams.getTerminalId()) == null ? "null" : transSummaryQueryParams.getTerminalId(),
                        modelPaginator.getStartingModelIndex().toString(),
                        modelPaginator.getEndingModelIndex().toString(),
                        sortByColumn,
                        modelPaginator.getOrder().toUpperCase(),
                        (transSummaryQueryParams.getStartDate()) == null ? "null" : transSummaryQueryParams.getStartDate(),  //6
                        (transSummaryQueryParams.getEndDate()) == null ? "null" : transSummaryQueryParams.getEndDate(),     //7
                        (transSummaryQueryParams.getEmployeeId()) == null ? "null" : transSummaryQueryParams.getEmployeeId(),
                        (terminalModel.getRevenueCenterId()) == null ? "null" : terminalModel.getRevenueCenterId(),
                        (transSummaryQueryParams.isOpenOnly() == true) ? 1 : 0
                },
                null,
                PosAPIHelper.getLogFileName(terminalModel.getId()),
                false
        );

        if (transactionList.size() > 0) {
            for (HashMap transactionSummaryHM : transactionList) {
                CommonAPI.checkIsNullOrEmptyObject(transactionSummaryHM.get("ID"), "Transaction Id cannot be found.", terminalModel.getId());
                TransactionSummaryModel transactionSummaryModel = new TransactionSummaryModel(transactionSummaryHM);


                //Get all the Tender Lines for the current transaction ID
                //It is fetched like this so the Tenders are cached.  This cut the DB calls down by 75%
                ArrayList<HashMap> tendersHM = TenderLineItemCollection.getAllTenderLineItems(transactionSummaryModel.getId(), terminalModel);

                for (HashMap tenderLineHM : tendersHM) {
                    TenderLineItemModel tenderLineItemModel = TenderLineItemModel.createTenderLineItemModel(tenderLineHM, terminalModel);

                    //Validate the Account on the Tender Line
                    if (tenderLineItemModel.getAccount() == null
                            && (tenderLineItemModel.getEmployeeId() != null
                            && !tenderLineItemModel.getEmployeeId().toString().isEmpty()
                            && !tenderLineItemModel.getEmployeeId().equals(0))) {
                        //check to see if we have validated this Account Model previously
                        AccountModel validatedAccountModel = this.getPosAPIModelCache().checkAccountCache(tenderLineItemModel.getEmployeeId());
                        if (validatedAccountModel != null) {
                            tenderLineItemModel.setAccount(validatedAccountModel);
                        } else {

                            if (terminalModel.getId() == null) { //if there is no terminal ID, only grab a simple AccountModel (no balances)
                                validatedAccountModel = AccountModel.getAccountModelById(tenderLineItemModel.getEmployeeId(), terminalModel);
                            } else {
                                validatedAccountModel = AccountModel.getAccountModelByIdForTender(terminalModel, BigDecimal.ONE, tenderLineItemModel.getEmployeeId(), new AccountQueryParams(true, true));
                            }

                            tenderLineItemModel.setAccount(validatedAccountModel);
                            this.getPosAPIModelCache().getValidatedAccountModels().add(validatedAccountModel);
                        }
                    }

                    //check to see if we have validated this Tender Model previously
                    TenderModel validatedTenderModel = this.getPosAPIModelCache().checkTenderCache(tenderLineItemModel.getItemId());
                    if (validatedTenderModel != null) {
                        tenderLineItemModel.setTender(TenderDisplayModel.createTenderModel(validatedTenderModel));
                    } else {
                        validatedTenderModel = TenderModel.getTenderModel(tenderLineItemModel.getItemId(), terminalModel.getId());
                        tenderLineItemModel.setTender(TenderDisplayModel.createTenderModel(validatedTenderModel));
                        this.getPosAPIModelCache().getValidatedTenderModels().add(validatedTenderModel);
                    }
                    transactionSummaryModel.getTenders().add(tenderLineItemModel);
                }

                getCollection().add(transactionSummaryModel);
            }
        }

        terminalModel.logTransactionTime();

    }

    public List<TransactionSummaryModel> getCollection() {
        return collection;
    }

    public void setCollection(List<TransactionSummaryModel> collection) {
        this.collection = collection;
    }

    public PosAPIModelCache getPosAPIModelCache() throws Exception {

        if (posAPIModelCache == null) {

            posAPIModelCache = new PosAPIModelCache();
        }

        return posAPIModelCache;
    }

    public void setPosAPIModelCache(PosAPIModelCache posAPIModelCache) {
        this.posAPIModelCache = posAPIModelCache;
    }
}
