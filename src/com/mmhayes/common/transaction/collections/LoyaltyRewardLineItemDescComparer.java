package com.mmhayes.common.transaction.collections;

import com.mmhayes.common.transaction.models.LoyaltyRewardLineItemModel;

import java.util.Comparator;

/*
Last Updated (automatically updated by SVN)
$Author: eglundin $: Author of last commit
$Date: 2017-11-15 15:16:54 -0500 (Wed, 15 Nov 2017) $: Date of last commit
$Rev: 5677 $: Revision of last commit
*/


public class LoyaltyRewardLineItemDescComparer implements Comparator<LoyaltyRewardLineItemModel> {

    String fieldToSort = "";

    public LoyaltyRewardLineItemDescComparer() {

    }

    public LoyaltyRewardLineItemDescComparer(String fieldToSort) {
        this.fieldToSort = fieldToSort;
    }

    public int compare(LoyaltyRewardLineItemModel rewardLineA, LoyaltyRewardLineItemModel rewardLineB) {
        int pointsToRedeemCheck = 0;

        switch (fieldToSort) {
            case "value":
                pointsToRedeemCheck = rewardLineA.getReward().getValue().compareTo(rewardLineB.getReward().getValue());
                break;
            case "pointsToRedeem":
                pointsToRedeemCheck = rewardLineA.getReward().getPointsToRedeem().compareTo(rewardLineB.getReward().getPointsToRedeem());
                break;
        }

        //I want to sort the products with the highest values first
        return pointsToRedeemCheck * -1;
    }
}

