package com.mmhayes.common.transaction.collections;

//mmhayes dependencies
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.dataaccess.DataManager;

//API dependencies
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.errorhandling.exceptions.*;
import com.mmhayes.common.api.CommonAuthResource;
import com.mmhayes.common.product.models.*;
import com.mmhayes.common.transaction.models.*;

//other dependencies
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2017-06-16 12:44:46 -0400 (Fri, 16 Jun 2017) $: Date of last commit
 $Rev: 4152 $: Revision of last commit
*/
public class ProductCollection {
    private List<ProductModel> collection = new ArrayList<ProductModel>();
    private static DataManager dm = new DataManager();
    private String authenticatedMacAddress = null;
    private TransactionModel transactionModel = null;

    //default constructor - used by the API
    public ProductCollection() {
    }

    public ProductCollection(TransactionModel transactionModel) {
        this.transactionModel = transactionModel;
    }

    //constructor - populates the collection
    public ProductCollection(Integer productID, HttpServletRequest request, Integer terminalId) throws Exception {

        setAuthenticatedMacAddress(CommonAuthResource.getMacAddressFromAuthHeader(request));   //Verify the header has Custom Header and MAC Address Header
        CommonAPI.checkIsNullOrEmptyObject(productID, "Product Id could not be found", terminalId);   //If the productID parameter is null, reply with a Missing Data Exception

        //populate this collection with models
        populateCollection(productID, terminalId);
    }

    public static ProductCollection getAllProducts(Integer terminalId) throws Exception {
        ProductCollection productCollection = new ProductCollection();

        //get all models in an array list
        ArrayList<HashMap> productList = dm.parameterizedExecuteQuery("data.posapi30.getAllProducts",
                new Object[]{},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        //product list is null or empty, throw exception
        CommonAPI.checkIsNullOrEmptyList(productList, "Product List is empty", terminalId);
        for (HashMap productHM : productList) {
            CommonAPI.checkIsNullOrEmptyObject(productHM.get("ID"), "Product Id could not be found", terminalId);
            //create a new model and add to the collection
            productCollection.getCollection().add(ProductModel.createProductModel(productHM, terminalId));
        }

        return productCollection;
    }

    //populates this collection with models
    private ProductCollection populateCollection(Integer productID, Integer terminalId) throws Exception {

        //get all models in an array list
        ArrayList<HashMap> ProductList = dm.parameterizedExecuteQuery("data.labelPrinting.getProductDetail",
                new Object[]{
                        productID
                },
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        //product list is null or empty, throw exception
        CommonAPI.checkIsNullOrEmptyList(ProductList, "Product List is empty", terminalId);
        for (HashMap productHM : ProductList) {
            CommonAPI.checkIsNullOrEmptyObject(productHM.get("ID"), "Product Id could not be found", terminalId);
            //create a new model and add to the collection
            getCollection().add(new ProductModel(productHM, terminalId));
        }

        return this;
    }

    public TransactionModel validateProducts() throws Exception {

        //if there are departments and sub-departments on the Products, make sure they are valid
        validateProductSubDepartments(transactionModel.getTerminal().getId());

        //make sure all Products are valid
        List<ProductModel> productList = getProductsUsingOneCall(transactionModel);

        return mapProductsToLineItems(productList);

    }

    //check all the products to see if there are any sub-departments or departments
    private boolean productsHaveSubDepartmentsOrDepartments() throws Exception {
        boolean result = false;

        for (ProductLineItemModel productLineItemModel : transactionModel.getProducts()) {
            if (productLineItemModel.getProduct() != null) {
                if ((productLineItemModel.getProduct().getSubDepartment() != null && !productLineItemModel.getProduct().getSubDepartment().isEmpty()) || (productLineItemModel.getProduct().getSubDepartment() != null && !productLineItemModel.getProduct().getDepartment().isEmpty())) {
                    return true;

                }
            }
        }

        return false;
    }

    private void validateProductSubDepartments(Integer terminalId) throws Exception {

        //check all the products to see if there are any subdepartments or departments
        if (productsHaveSubDepartmentsOrDepartments()) {

            //get all sub-dapartments and departments from the db
            List<ProductSubDepartmentModel> productSubDepartmentModelList = ProductSubDepartmentModel.getAllProductSubDepartments(terminalId);

            List<String> subDepartmentList = new ArrayList<String>();
            List<String> departmentList = new ArrayList<String>();

            //make a lis of distinct sub-departments and departments
            for (ProductSubDepartmentModel productSubDepartmentModel : productSubDepartmentModelList) {
                if (!subDepartmentList.contains(productSubDepartmentModel.getSubDepartmentName().toUpperCase())) {  //add to distinct sub-department list
                    subDepartmentList.add(productSubDepartmentModel.getSubDepartmentName().toUpperCase());
                }
                if (!departmentList.contains(productSubDepartmentModel.getDepartmentName().toUpperCase())) { //add to distinct department list
                    departmentList.add(productSubDepartmentModel.getDepartmentName().toUpperCase());
                }
            }

            //check if the department or sub-department on the product exists in the sub-department list or the department list
            for (ProductLineItemModel productLineItemModel : transactionModel.getProducts()) {
                if (productLineItemModel.getProduct() == null) {
                    //error out
                } else {
                    //validate sub department
                    if (productLineItemModel.getProduct().getSubDepartment() != null && !productLineItemModel.getProduct().getSubDepartment().isEmpty()) {
                        if (!subDepartmentList.contains(productLineItemModel.getProduct().getSubDepartment().toUpperCase())) {
                            throw new MissingDataException("Invalid Subdepartment found on product", transactionModel.getTerminal().getId());
                        }
                    }

                    if (productLineItemModel.getProduct().getDepartment() != null && !productLineItemModel.getProduct().getDepartment().isEmpty()) {
                        if (!departmentList.contains(productLineItemModel.getProduct().getDepartment().toUpperCase())) {
                            throw new MissingDataException("Invalid Department found on product", transactionModel.getTerminal().getId());
                        }
                    }
                }
            }
        }
    }

    //get a list of Products by ID, Name, or PLU code using one call to the database
    public static List<ProductModel> getProductsUsingOneCall(TransactionModel transactionModel) throws Exception {
        DataManager dm = new DataManager();
        ArrayList productListHM = new ArrayList();
        ArrayList<ProductModel> productList = new ArrayList<>();

        StringBuilder sbCode = new StringBuilder();
        StringBuilder sbName = new StringBuilder();
        StringBuilder sbId = new StringBuilder();


        for (ProductLineItemModel productLineItemModel : transactionModel.getProducts()) {
            if (productLineItemModel.getProduct() == null) {
                throw new MissingDataException("Product information is missing", transactionModel.getTerminal().getId());
            }

            //check if id, name, or pluCode is not null or empty
            if ((productLineItemModel.getProduct().getId() != null && !productLineItemModel.getProduct().getId().toString().isEmpty()) || (productLineItemModel.getProduct().getName() != null && !productLineItemModel.getProduct().getName().isEmpty()) || (productLineItemModel.getProduct().getProductCode() != null && !productLineItemModel.getProduct().getProductCode().isEmpty())) {

                if (productLineItemModel.getProduct().getId() != null && !productLineItemModel.getProduct().getId().toString().isEmpty()) {
                    sbId.append("'");
                    sbId.append(productLineItemModel.getProduct().getId());
                    sbId.append("'");

                    if (productLineItemModel != transactionModel.getProducts().get(transactionModel.getProducts().size() - 1)) {
                        sbId.append(",");
                    }
                } else if (productLineItemModel.getProduct().getProductCode() != null && !productLineItemModel.getProduct().getProductCode().isEmpty()) {
                    sbCode.append("'");
                    sbCode.append(productLineItemModel.getProduct().getProductCode());
                    sbCode.append("'");

                    if (productLineItemModel != transactionModel.getProducts().get(transactionModel.getProducts().size() - 1)) {
                        sbCode.append(",");
                    }
                } else if (productLineItemModel.getProduct().getName() != null && !productLineItemModel.getProduct().getName().isEmpty()) {
                    sbName.append("'");
                    sbName.append(productLineItemModel.getProduct().getName());
                    sbName.append("'");

                    if (productLineItemModel != transactionModel.getProducts().get(transactionModel.getProducts().size() - 1)) {
                        sbName.append(",");
                    }
                }
            }
        }

        String idList = sbId.toString();
        String nameList = sbName.toString();
        String codeList = sbCode.toString();

        if (idList.isEmpty()) {
            idList = idList + "''";
        } else if (idList.endsWith(",")) {
            idList = idList.substring(0, idList.length() - 1);
        }

        if (nameList.isEmpty()) {
            nameList = nameList + "''";
        } else if (nameList.endsWith(",")) {
            nameList = nameList.substring(0, nameList.length() - 1);
        }

        if (codeList.isEmpty()) {
            codeList = codeList + "''";
        } else if (codeList.endsWith(",")) {
            codeList = codeList.substring(0, codeList.length() - 1);
        }

        //get all products using one call
        //EGL 09-07-2016 - used the serializeSqlWithColNames in order to use an "IN" clause
        productListHM = dm.serializeSqlWithColNames("data.posapi30.getAllProductsByCommaDelimitedLists", new Object[]{idList, nameList, codeList}, null, transactionModel.getTerminal().getTerminalLogFileName());

        //product list is null or empty, throw exception
        //CommonAPI.checkIsNullOrEmptyList(loyaltyProgramList, CommonAPI.PosModelType.LOYALTY_PROGRAM);
        List<HashMap> listHM = (List<HashMap>) productListHM;

        for (HashMap productHM : listHM) {
            //CommonAPI.checkIsNullOrEmptyObject(productHM.get("ID"), "Product Id could not be found");
            productList.add(ProductModel.createProductModel(productHM, transactionModel.getTerminal().getId()));  //create a new model and add to the collection
        }

        return productList;
    }

    public TransactionModel mapProductsToLineItems(List<ProductModel> productList) throws Exception {
        //map ProductModels to ProductLineItemModel
        for (ProductLineItemModel productLineItemModel : transactionModel.getProducts()) {

            if ((productLineItemModel.getProduct().getId() == null || productLineItemModel.getProduct().getId().toString().isEmpty()) && (productLineItemModel.getProduct().getName() == null || productLineItemModel.getProduct().getName().isEmpty()) && (productLineItemModel.getProduct().getProductCode() == null || productLineItemModel.getProduct().getProductCode().isEmpty())) {
                //don't map product
            } else {

                try {
                    if (productLineItemModel.getProduct().getId() != null && !productLineItemModel.getProduct().getId().toString().isEmpty()) {
                        for (ProductModel productModel : productList) {
                            if (productModel.getId() == productLineItemModel.getProduct().getId()) ;
                            productLineItemModel.setProduct(productModel);
                            break;
                        }
                    } else if (productLineItemModel.getProduct().getName() != null && !productLineItemModel.getProduct().getName().isEmpty()) {
                        for (ProductModel productModel : productList) {
                            if (productModel.getName().equalsIgnoreCase(productLineItemModel.getProduct().getName())) ;
                            productLineItemModel.setProduct(productModel);
                            break;
                        }
                    } else if (productLineItemModel.getProduct().getProductCode() != null && !productLineItemModel.getProduct().getProductCode().isEmpty()) {
                        for (ProductModel productModel : productList) {
                            if (productModel.getProductCode().equalsIgnoreCase(productLineItemModel.getProduct().getProductCode()))
                                ;
                            productLineItemModel.setProduct(productModel);
                            break;
                        }
                    }
                } catch (Exception ex) {
                    throw new MissingDataException("Invalid Product submitted.", transactionModel.getTerminal().getId());

                }

            }
        }

        return this.getTransactionModel();
    }

    //getter
    public List<ProductModel> getCollection() {
        return this.collection;
    }

    //getter
    private String getAuthenticatedMacAddress() {
        return this.authenticatedMacAddress;
    }

    //setter
    private void setAuthenticatedMacAddress(String authenticatedMacAddress) {
        this.authenticatedMacAddress = authenticatedMacAddress;
    }


    public TransactionModel getTransactionModel() {
        return transactionModel;
    }

    public void setTransactionModel(TransactionModel transactionModel) {
        this.transactionModel = transactionModel;
    }

}
