package com.mmhayes.common.transaction.collections;

import com.mmhayes.common.transaction.models.ProductLineItemModel;

import java.util.Comparator;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2017-08-21 16:23:41 -0400 (Mon, 21 Aug 2017) $: Date of last commit
 $Rev: 5087 $: Revision of last commit
*/
public class ProductLineItemAscComparer implements Comparator<ProductLineItemModel> {

    public int compare(ProductLineItemModel productLineA, ProductLineItemModel productLineB){

        int productAmountCheck = productLineA.getAdjAmountLoyalty().compareTo(productLineB.getAdjAmountLoyalty());

        //I want to sort the products with the highest values first
        return productAmountCheck;

    }
}