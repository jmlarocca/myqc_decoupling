package com.mmhayes.common.transaction.collections;

import com.mmhayes.common.transaction.models.QcTransactionModel;
import com.mmhayes.common.transaction.models.TransactionModel;
import com.mmhayes.common.dataaccess.DataManager;

import java.util.ArrayList;
import java.util.HashMap;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2017-04-20 16:34:33 -0400 (Thu, 20 Apr 2017) $: Date of last commit
 $Rev: 3849 $: Revision of last commit
*/
public class QcTransactionCollection {

    public QcTransactionCollection() {

    }

    private ArrayList<QcTransactionModel> collection = new ArrayList<>();

    public static QcTransactionCollection getQcTransactions(TransactionModel transactionModel, Integer terminalId) throws Exception {
        DataManager dm = new DataManager();
        QcTransactionCollection qcTransactionCollection = new QcTransactionCollection();

        //get all models in an array list
        ArrayList<HashMap> qcTransactionListHM = dm.parameterizedExecuteQuery("data.posapi30.getQCTransactions",
                new Object[]{
                        transactionModel.getId()
                },
                true
        );

        for (HashMap modelHM : qcTransactionListHM) {
            //CommonAPI.checkIsNullOrEmptyObject(modelHM.get("ID"), "LoyaltyProgramId Id could not be found");
            qcTransactionCollection.getCollection().add(QcTransactionModel.createQcTransactionModel(modelHM, terminalId));  //create a new model and add to the collection
        }



        return qcTransactionCollection;

    }


    public ArrayList<QcTransactionModel> getCollection() {
        return collection;
    }

    public void setCollection(ArrayList<QcTransactionModel> collection) {
        this.collection = collection;
    }


}
