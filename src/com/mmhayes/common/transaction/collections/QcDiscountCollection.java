package com.mmhayes.common.transaction.collections;

//mmhayes dependencies

import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.terminal.models.TerminalModel;
import com.mmhayes.common.transaction.models.*;
import com.mmhayes.common.utils.Logger;

//API dependencies
import com.mmhayes.common.api.CommonAPI;

//other dependencies
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2021-04-27 08:57:14 -0400 (Tue, 27 Apr 2021) $: Date of last commit
 $Rev: 13856 $: Revision of last commit
*/
public class QcDiscountCollection {

    private ArrayList<QcDiscountModel> collection = new ArrayList<>();

    public QcDiscountCollection() {

    }

    //constructor - populates the collection
    private QcDiscountCollection(TenderLineItemModel tenderLineItemModel, TransactionModel transactionModel, Boolean returnOne) throws Exception {
        CommonAPI.checkIsNullOrEmptyObject(transactionModel.getTerminal().getId(), "Terminal Id could not be found", transactionModel.getTerminal().getId());   //If the Terminal Id parameter is null, reply with a Missing Data Exception

        //populate this collection with models
        populateCollection(tenderLineItemModel, transactionModel, returnOne);
    }

    private QcDiscountCollection(Integer employeeId, BigDecimal tenderAmount, TerminalModel terminalModel, Boolean returnOne) throws Exception {
        CommonAPI.checkIsNullOrEmptyObject(terminalModel.getId(), "Terminal Id could not be found", terminalModel.getId());   //If the Terminal Id parameter is null, reply with a Missing Data Exception

        //populate this collection with models
        populateCollection(employeeId, tenderAmount, terminalModel, returnOne);
    }

    //populates this collection with models
    private QcDiscountCollection populateCollection(TenderLineItemModel tenderLIneItemModel, TransactionModel transactionModel, Boolean returnOne) throws Exception {

        //call CommonAPI to fetch the valid QCDiscount available for this employee id, subtotal, and terminal Id
        setCollection(this.getQCDiscounts(tenderLIneItemModel.getAccount().getId(), tenderLIneItemModel.getAmount(), transactionModel, returnOne));

        return this;
    }

    //populates this collection with models
    private QcDiscountCollection populateCollection(Integer employeeId, BigDecimal tenderAmount, TerminalModel terminalModel, Boolean returnOne) throws Exception {

        //call CommonAPI to fetch the valid QCDiscount available for this employee id, subtotal, and terminal Id
        setCollection(this.getQCDiscounts(employeeId, tenderAmount, terminalModel, returnOne));

        return this;
    }

    public static QcDiscountCollection getAvailableQcDiscounts(TenderLineItemModel tenderLineItemModel, TransactionModel transactionModel, boolean returnOne) throws Exception {

        QcDiscountCollection qcDiscountCollection = new QcDiscountCollection(tenderLineItemModel, transactionModel, returnOne);

        return qcDiscountCollection;
    }

    public static QcDiscountCollection getAvailableQcDiscounts(Integer employeeId, BigDecimal tenderAmount, TerminalModel terminalModel, boolean returnOne) throws Exception {

        QcDiscountCollection qcDiscountCollection = new QcDiscountCollection(employeeId,tenderAmount, terminalModel, returnOne);

        return qcDiscountCollection;
    }

    public static ArrayList<QcDiscountModel> getQCDiscounts(Integer employeeID, BigDecimal subtotal, TransactionModel transactionModel, Boolean returnOne) throws Exception {

        return getQCDiscounts(employeeID, subtotal, transactionModel.getTerminal(), returnOne);

    }

    public static ArrayList<QcDiscountModel> getQCDiscounts(Integer employeeID, BigDecimal subtotal, TerminalModel terminalModel, Boolean returnOne) throws Exception {
        DataManager dm = new DataManager();
        //get eligible discounts (same name as QC POS Discount, mapped RC, open, preset, subtotal, meet subtotal min)

        ArrayList<HashMap> QCDiscountList = null;
        ArrayList<QcDiscountModel> qcDiscountModelList = new ArrayList<>();

        QCDiscountList = dm.parameterizedExecuteQuery("data.posapi30.getQCDiscountsWithPOSDiscountMapping",
                new Object[]{
                        employeeID,
                        subtotal,
                        terminalModel.getId()
                },
                PosAPIHelper.getLogFileName(terminalModel.getId()), true
        );

        //if any results, check the discount\'s schedules
        if (QCDiscountList == null || QCDiscountList.size() == 0) {
            Logger.logMessage("No discounts returned for this user", Logger.LEVEL.DEBUG);
            return qcDiscountModelList;
        }

        //NEED TO CHECK DATES AND TIMES FOR VALIDITY
        LocalDateTime currentTime = LocalDateTime.now();

        Integer timeDiff = 0;
        if (QCDiscountList.get(0).get("TIMEDIFFERENCE") != null) {
            timeDiff = CommonAPI.convertModelDetailToInteger(QCDiscountList.get(0).get("TIMEDIFFERENCE"), 0, true);
            currentTime = currentTime.plusHours(timeDiff);
        }

        String dayOfWeek = currentTime.getDayOfWeek().toString();

        //only include discounts that are valid for right now
        for (HashMap thisDiscount : QCDiscountList) {

            //check if discount is valid for today
            if (thisDiscount.get(dayOfWeek).toString().equals("false")) {
                continue;
            }

            //check if discount is valid for the current time
            String startTime = thisDiscount.get("STARTTIME").toString();
            String endTime = thisDiscount.get("ENDTIME").toString();

            if (startTime.length() == 0 || endTime.length() == 0) {
                continue;
            }

            LocalTime discountOpen = LocalTime.parse(startTime);
            LocalTime discountClose = LocalTime.parse(endTime);

            //if now is after close and before open.. continue to next discount
            if (discountOpen.compareTo(currentTime.toLocalTime()) > 0 || discountClose.compareTo(currentTime.toLocalTime()) < 0) {
                continue;
            }

            //check the amount/reset properties of any coupon discounts to determine validity
            if (CommonAPI.convertModelDetailToString(thisDiscount.get("DISCOUNTTYPEID")).equals("2")) {
                Integer discountFrequencyID = CommonAPI.convertModelDetailToInteger(thisDiscount.get("DISCOUNTFREQUENCYID"));
                SimpleDateFormat sdfMilitaryTime = new SimpleDateFormat("HH:mm");
                Date discountResetDate = new Date();
                Calendar now = Calendar.getInstance();
                now.add(Calendar.HOUR, timeDiff);

                //get the date to compare to based on the frequency setting
                switch (discountFrequencyID) {
                    case 1:
                        // DAILY
                        if (sdfMilitaryTime.format(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(thisDiscount.get("RESETDTM").toString())).compareTo(sdfMilitaryTime.format(now.getTime())) <= 0) {
                            //current time of day is after reset time of day (reset today scenario)
                            discountResetDate = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss").parse(
                                    new SimpleDateFormat("MM/dd/yyyy").format(now.getTime()) + " " + (new SimpleDateFormat("hh:mm:ss").format(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(thisDiscount.get("RESETDTM").toString())))
                            );
                        } else {
                            //current time of day is before reset time, set date one day back (reset did not occur yet today)
                            Calendar c = Calendar.getInstance();
                            c.setTime(now.getTime());
                            c.add(Calendar.DATE, -1);
                            discountResetDate = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss").parse(
                                    new SimpleDateFormat("MM/dd/yyyy").format(c.getTime()) + " " + (new SimpleDateFormat("hh:mm:ss").format(thisDiscount.get("RESETDTM")))
                            );
                        }
                        break;
                    case 2:
                        Calendar c1 = Calendar.getInstance();
                        c1.setTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(thisDiscount.get("RESETDTM").toString()));
                        while (c1.getTime().compareTo(now.getTime()) < 0) {
                            c1.add(Calendar.DATE, 7);
                        }
                        c1.add(Calendar.DATE, -7);
                        discountResetDate = c1.getTime();
                        break;
                    case 3:
                        Calendar c2 = Calendar.getInstance();
                        c2.setTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(thisDiscount.get("RESETDTM").toString()));
                        while (c2.getTime().compareTo(now.getTime()) < 0) {
                            c2.add(Calendar.MONTH, 1);
                        }
                        c2.add(Calendar.MONTH, -1);
                        discountResetDate = c2.getTime();
                        break;
                    case 4:
                        // Transaction
                        discountResetDate = new SimpleDateFormat("MM/dd/yyyy").parse("01/01/3000");
                        break;
                }

                //get the sum of the amounts of the usage of this coupon since the last reset date
                ArrayList<HashMap> result = dm.parameterizedExecuteQuery("data.ordering.checkCouponUsage",
                        new Object[]{
                                employeeID,
                                thisDiscount.get("ID").toString(),
                                discountResetDate
                        },
                        PosAPIHelper.getLogFileName(terminalModel.getId()),
                        true
                );

                if (result != null) {
                    //should only be 0 or 1 record in result because it is a sum of amounts, if none then amount on thisDiscount is OK
                    if (result.size() == 1) {
                        HashMap resultHM = result.get(0);

                        if (resultHM.containsKey("DISCTAKEN") && resultHM.get("DISCTAKEN") != null) {
                            BigDecimal amount = CommonAPI.convertModelDetailToBigDecimal(thisDiscount.get("AMOUNT"));
                            BigDecimal discTaken = CommonAPI.convertModelDetailToBigDecimal(resultHM.get("DISCTAKEN"));

                            if (discTaken != null && discTaken.compareTo(BigDecimal.ZERO) == 1) {
                                //if singleTransaction is on and the coupon has been used since the last reset date, set amount to zero
                                if (thisDiscount.get("SINGLETRANSACTION").toString().equals("true")) {
                                    thisDiscount.put("AMOUNT", BigDecimal.ZERO);
                                } else {
                                    //otherwise just take the difference
                                    BigDecimal amtRemaining = amount.subtract(discTaken);

                                    //prevent negatives (this could only happen with bad data)
                                    if (amtRemaining.compareTo(BigDecimal.ZERO) == -1) {
                                        amtRemaining = BigDecimal.ZERO;
                                    }

                                    thisDiscount.put("AMOUNT", amtRemaining);
                                }
                            }
                        }
                    }
                } else {
                    Logger.logMessage("There was an error attempting to get the previous transactions for coupon with DiscountID: " + thisDiscount.get("ID").toString(), Logger.LEVEL.ERROR);
                }
            }

            if (CommonAPI.isLocalWar()){
                if (CommonAPI.convertModelDetailToInteger(thisDiscount.get("DISCOUNTTYPEID")).equals(PosAPIHelper.DiscountType.COUPON.toInt()) &&
                        !terminalModel.getAllowQcCouponDiscountOffline()){
                    continue; //if terminal is offline, check if the terminal allows POS Discounts of type Coupon
                }
            }

            //valid discount, add to map

            if (CommonAPI.convertModelDetailToBigDecimal(thisDiscount.get("AMOUNT")) != null && CommonAPI.convertModelDetailToBigDecimal(thisDiscount.get("AMOUNT")).compareTo(new BigDecimal(0)) == 1) {
                qcDiscountModelList.add(new QcDiscountModel(thisDiscount));

                if (returnOne && qcDiscountModelList.size() == 1){
                    break;
                }
            }
        }

        return qcDiscountModelList;
    }

    /**
     * Gets the available POS Anywhere Subtotal Discounts. Similar to getQCDiscounts, but specific
     * to POS Anywhere
     * @param terminalId the terminal id the discounts are available at
     * @return a list of discount models representing the available discounts
     * @throws Exception
     */
    public static ArrayList<QcDiscountModel> getPOSAnywhereSubtotalDiscounts(Integer terminalId) throws Exception {
        DataManager dm = new DataManager();
        //get eligible discounts (same name as QC POS Discount, mapped RC, open, preset, subtotal, meet subtotal min)

        ArrayList<HashMap> QCDiscountList = null;
        ArrayList<QcDiscountModel> qcDiscountModelList = new ArrayList<>();

        QCDiscountList = dm.parameterizedExecuteQuery("data.posanywhere.getSubtotalDiscounts",
                new Object[]{
                        terminalId
                },
                PosAPIHelper.getLogFileName(terminalId), true
        );

        //if any results, check the discount\'s schedules
        if (QCDiscountList == null || QCDiscountList.size() == 0) {
            Logger.logMessage("No discounts returned for this user", Logger.LEVEL.DEBUG);
            return qcDiscountModelList;
        }

        //only include discounts that are valid for right now
        for (HashMap thisDiscount : QCDiscountList) {

            //valid discount, add to map
            if (CommonAPI.convertModelDetailToBigDecimal(thisDiscount.get("AMOUNT")) != null && CommonAPI.convertModelDetailToBigDecimal(thisDiscount.get("AMOUNT")).compareTo(new BigDecimal(0)) == 1) {
                qcDiscountModelList.add(new QcDiscountModel(thisDiscount));
            }
        }

        return qcDiscountModelList;
    }

    public ArrayList<QcDiscountModel> getCollection() {
        return collection;
    }

    public void setCollection(ArrayList<QcDiscountModel> collection) {
        this.collection = collection;
    }
}
