package com.mmhayes.common.transaction.collections;

import com.mmhayes.common.transaction.models.ProductLineItemModel;

import java.util.Comparator;

/*
 Last Updated (automatically updated by SVN)
 $Author: nyu $: Author of last commit
 $Date: 2021-08-17 14:37:51 -0400 (Tue, 17 Aug 2021) $: Date of last commit
 $Rev: 14929 $: Revision of last commit
*/
public class ProductLineItemSequenceComparerAsc implements Comparator<ProductLineItemModel> {

    public int compare(ProductLineItemModel productLineA, ProductLineItemModel productLineB){

        int sequenceCheck = productLineA.getTransactionItemNum().compareTo(productLineB.getTransactionItemNum());

        //I want to sort the products with the lowest sequence first
        return sequenceCheck;
    }
}