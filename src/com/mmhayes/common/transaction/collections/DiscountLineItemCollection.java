package com.mmhayes.common.transaction.collections;

//mmhayes dependencies
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.api.errorhandling.exceptions.APIException;
import com.mmhayes.common.dataaccess.DataManager;

//API dependencies
import com.mmhayes.common.transaction.models.*;

//other dependencies
import java.util.ArrayList;
import java.util.HashMap;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2017-04-05 11:09:14 -0400 (Wed, 05 Apr 2017) $: Date of last commit
 $Rev: 3793 $: Revision of last commit
*/
public class DiscountLineItemCollection {

    private ArrayList<DiscountLineItemModel> collection = new ArrayList<>();

    public DiscountLineItemCollection(){

    }

    public static DiscountLineItemCollection getAllDiscountLineItems(ProductLineItemModel productLineItemModel, Integer terminalId) throws Exception {
        DataManager dm = new DataManager();
        DiscountLineItemCollection discountLineItemCollection = new DiscountLineItemCollection();

        //get all models in an array list
        ArrayList<HashMap> discountLineItemListHM = dm.parameterizedExecuteQuery("data.posapi30.getDiscountsForTransactionId",
                new Object[]{ productLineItemModel.getId() },
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        //modifier list is null or empty, throw exception
        //CommonAPI.checkIsNullOrEmptyList(modifierLineItemListHM, "Modifier could not be found");

        for (HashMap modelHM : discountLineItemListHM) {
            //CommonAPI.checkIsNullOrEmptyObject(modelHM.get("ID"), "LoyaltyProgramId Id could not be found");
            discountLineItemCollection.getCollection().add(DiscountLineItemModel.createDiscountLineItemModel(modelHM));  //create a new model and add to the collection
        }

        return discountLineItemCollection;
    }

    public ArrayList<DiscountLineItemModel> getCollection() {
        return collection;
    }

    public void setCollection(ArrayList<DiscountLineItemModel> collection) {
        this.collection = collection;
    }

}