package com.mmhayes.common.transaction.collections;

//mmhayes dependencies

import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.transaction.models.SurchargeModel;
import com.mmhayes.common.terminal.models.TerminalModel;

//API dependencies
import com.mmhayes.common.api.CommonAPI;

//other dependencies
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/*
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2019-05-16 11:45:27 -0400 (Thu, 16 May 2019) $: Date of last commit
 $Rev: 8865 $: Revision of last commit
*/
public class SurchargeCollection {

    private List<SurchargeModel> collection = new ArrayList<>();
    private static DataManager dm = new DataManager();

    public SurchargeCollection() {

    }

    public static SurchargeCollection getAllSurcharges(TerminalModel terminalModel) throws Exception {
        SurchargeCollection surchargeCollection = new SurchargeCollection();

        //get all models in an array list
        ArrayList<HashMap> surchargeList = dm.parameterizedExecuteQuery("data.posapi30.getAllActiveSurcharges",
                new Object[]{terminalModel.getRevenueCenterId()},
                PosAPIHelper.getLogFileName(terminalModel.getId()),
                true
        );

        for (HashMap surchargeHM : surchargeList) {
            CommonAPI.checkIsNullOrEmptyObject(surchargeHM.get("ID"), "Surcharge Id could not be found", terminalModel.getId());
            //create a new model and add to the collection
            surchargeCollection.getCollection().add(SurchargeModel.createSurchargeModel(surchargeHM, terminalModel));
        }

        return surchargeCollection;
    }

    public static SurchargeCollection getSurchargesForProductId(Integer productId, Integer terminalID) throws Exception {
        DataManager dm = new DataManager();
        ArrayList<SurchargeModel> surchargeList = new ArrayList<>();
        SurchargeCollection surchargeCollection = new SurchargeCollection();

        //get all models in an array list
        ArrayList<HashMap> surchargeListHM = dm.parameterizedExecuteQuery("data.posapi30.getOneActiveSurchargeByProductID",
                new Object[]{productId},
                PosAPIHelper.getLogFileName(terminalID),
                true
        );

        for (HashMap surchargeHM : surchargeListHM) {
            //CommonAPI.checkIsNullOrEmptyObject(productHM.get("ID"), "Product Id could not be found");
            surchargeList.add(SurchargeModel.createSurchargeModel(surchargeHM, terminalID));  //create a new model and add to the collection
        }

        surchargeCollection.setCollection(surchargeList);

        return surchargeCollection;
    }


    public List<SurchargeModel> getCollection() {
        return collection;
    }

    public void setCollection(List<SurchargeModel> collection) {
        this.collection = collection;
    }
}
