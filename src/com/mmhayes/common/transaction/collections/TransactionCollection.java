package com.mmhayes.common.transaction.collections;

//MMHayes dependencies

import com.mmhayes.common.account.models.AccountQueryParams;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.api.PosApi30.TransactionBuilder;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.donation.models.DonationLineItemModel;
import com.mmhayes.common.donation.models.DonationModel;
import com.mmhayes.common.loyalty.models.LoyaltyAccrualPolicyModel;
import com.mmhayes.common.loyalty.models.LoyaltyPointModel;
import com.mmhayes.common.loyalty.models.LoyaltyProgramModel;
import com.mmhayes.common.loyalty.models.LoyaltyRewardModel;
import com.mmhayes.common.terminal.models.TerminalModel;
import com.mmhayes.common.transaction.models.*;
import com.mmhayes.common.utils.Logger;

//API dependencies
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.ModelPaginator;
import com.mmhayes.common.api.errorhandling.exceptions.MissingDataException;

//other dependencies
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/*
 Last Updated (automatically updated by SVN)
 $Author: jmkimber $: Author of last commit
 $Date: 2020-06-29 15:24:35 -0400 (Mon, 29 Jun 2020) $: Date of last commit
 $Rev: 12096 $: Revision of last commit
*/
public class TransactionCollection {
    List<TenderModel> collection = new ArrayList<TenderModel>();

    public TransactionCollection() {

    }

    public static List<TransactionModel> getAllTransactions(ModelPaginator modelPaginator, TerminalModel terminalModel) throws Exception {

        DataManager dm = new DataManager();

        List<TransactionModel> transactionModels = new ArrayList<TransactionModel>();

        modelPaginator.finalizePaginationProperties();

        String sortByColumn = ""; //what column should this result set be sorted by

        if (modelPaginator.getSort().toUpperCase().equals("ID")) {
            sortByColumn = "t.ID";
        } else if (modelPaginator.getSort().toUpperCase().equals("DATE")) { //this is the default
            sortByColumn = "t.TransactionDate";
        } else if (modelPaginator.getSort().toUpperCase().equals("NAME")) { //this is the default
            sortByColumn = "t.PersonName";
        } else {
            Logger.logMessage("ERROR: Invalid sort property in TransactionCollection.populateCollection(). sort property = " + modelPaginator.getSort(), Logger.LEVEL.ERROR);
            throw new MissingDataException("Invalid sort parameter for Transaction Endpoint", terminalModel.getId());
        }

        //get a single page of models in an array list
        ArrayList<HashMap> transactionList = dm.serializeSqlWithColNames("data.posapi30.getAllQcPosTransactionsPaginate",
                new Object[]{
                        terminalModel.getId(),
                        modelPaginator.getStartingModelIndex().toString(),
                        modelPaginator.getEndingModelIndex().toString(),
                        sortByColumn,
                        modelPaginator.getOrder().toUpperCase()
                },
                false,
                false
        );

        //product list is null or empty, return empty list eglundin - 07-26-2016
        if (transactionList.size() > 0) {
            for (HashMap transactionHM : transactionList) {
                CommonAPI.checkIsNullOrEmptyObject(transactionHM.get("ID"), "Transaction Id cannot be found.", terminalModel.getId());

                TransactionModel transactionModel = new TransactionModel().setModelProperties(transactionHM, terminalModel);
                transactionModel.setTerminal(transactionModel.getOriginalTerminal());
                transactionModels.add(transactionModel);

            }
        }

        return transactionModels;

    }

    /**
     * Gets all transactions that affected a given employee's loyalty points
     *
     * @param accountId : the account id to get the transactions for
     * @param summaryMode : true or false
     * @return : a list of TransactionModels representing the loyalty transactions for the given employee
     * @throws Exception
     */
    public static List<TransactionModel> getAllLoyaltyTransactionsByEmployee(Integer accountId, boolean summaryMode) throws Exception {
        return getAllLoyaltyTransactionsByEmployee(accountId, null, summaryMode);
    }

    /**
     * Gets all transactions that affected a given employee's loyalty points
     *
     * @param accountId : the account id to get the transactions for
     * @param terminalId : the id of the terminal retrieving this information
     * @param summaryMode : true or false
     * @return
     * @throws Exception
     */
    public static List<TransactionModel> getAllLoyaltyTransactionsByEmployee(Integer accountId, Integer terminalId, boolean summaryMode) throws Exception {
        DataManager dm = new DataManager();
        List<TransactionModel> transactionModels = new ArrayList<>();

        ArrayList<HashMap> transactionList = dm.parameterizedExecuteQuery("data.posapi30.getAllLoyaltyTransactionsByEmployee",
                new Object[]{accountId},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        TerminalModel terminalModel = new TerminalModel();

        //product list is null or empty, return empty list eglundin - 07-26-2016
        if (transactionList.size() > 0) {
            for (HashMap transactionHM : transactionList) {
                CommonAPI.checkIsNullOrEmptyObject(transactionHM.get("ID"), "Transaction Id cannot be found.", terminalModel.getId());

                TransactionModel transactionModel;

                // if the terminal given is null, not everything can be set using setModelProperties
                // because specific properties require a valid terminal
                if (terminalModel.getId() == null) {
                    transactionModel = new TransactionModel();
                    transactionModel.setId(CommonAPI.convertModelDetailToInteger(transactionHM.get("ID")));
                    transactionModel.setTimeStamp(CommonAPI.convertModelDetailToString(transactionHM.get("TRANSACTIONDATE")));
                    transactionModel.setOrderNumber(CommonAPI.convertModelDetailToString(transactionHM.get("ORDERNUM")));
                    transactionModel.setTransComment(CommonAPI.convertModelDetailToString(transactionHM.get("TRANSCOMMENT")));
                    transactionModel.setTerminalId(CommonAPI.convertModelDetailToInteger(transactionHM.get("TERMINALID")));
                    transactionModel.setUserName(CommonAPI.convertModelDetailToString(transactionHM.get("USERNAME")));
                } else {
                    transactionModel = new TransactionModel().setModelProperties(transactionHM, terminalModel);
                }

                transactionModel.setTerminal(transactionModel.getOriginalTerminal());

                // set loyalty point details
                LoyaltyProgramModel loyaltyProgramModel = LoyaltyProgramModel.getOneLoyaltyProgram(CommonAPI.convertModelDetailToInteger(transactionHM.get("LOYALTYPROGRAMID")), terminalId, summaryMode);

                LoyaltyAccrualPolicyModel loyaltyAccrualPolicyModel = new LoyaltyAccrualPolicyModel();
                loyaltyAccrualPolicyModel.setId(CommonAPI.convertModelDetailToInteger(transactionHM.get("LOYALTYACCRUALPOLICYID")));
                loyaltyAccrualPolicyModel.setName(CommonAPI.convertModelDetailToString(transactionHM.get("LOYALTYACCRUALPOLICYNAME")));

                LoyaltyPointModel loyaltyPointModel = new LoyaltyPointModel();
                loyaltyPointModel.setEmployeeId(CommonAPI.convertModelDetailToInteger(transactionHM.get("EMPLOYEEID")));
                loyaltyPointModel.setPoints(CommonAPI.convertModelDetailToInteger(transactionHM.get("POINTS")));

                // if there was a reward, add the details of it to the transaction
                if (CommonAPI.convertModelDetailToInteger(transactionHM.get("LOYALTYREWARDID")) != null) {
                    loyaltyPointModel.setIsReward(true);

                    LoyaltyRewardLineItemModel loyaltyRewardLineItemModel = new LoyaltyRewardLineItemModel();

                    LoyaltyRewardModel loyaltyRewardModel = new LoyaltyRewardModel();
                    loyaltyRewardModel.setName(CommonAPI.convertModelDetailToString(transactionHM.get("LOYALTYREWARDNAME")));

                    loyaltyRewardLineItemModel.setReward(loyaltyRewardModel);

                    List<LoyaltyRewardLineItemModel> loyaltyRewardLineItems = new ArrayList<>();
                    loyaltyRewardLineItems.add(loyaltyRewardLineItemModel);

                    loyaltyPointModel.setRewards(loyaltyRewardLineItems);
                }

                // if there was a loyalty donation, add the details off it to the transaction
                if (CommonAPI.convertModelDetailToInteger(transactionHM.get("LOYALTYDONATIONID")) != null) {

                    DonationModel donationModel = new DonationModel();
                    donationModel.setId(CommonAPI.convertModelDetailToInteger(transactionHM.get("LOYALTYDONATIONID")));
                    donationModel.setName(CommonAPI.convertModelDetailToString(transactionHM.get("LOYALTYDONATIONNAME")));

                    DonationLineItemModel donationLineItemModel = new DonationLineItemModel();
                    donationLineItemModel.setDonation(donationModel);

                    List<DonationLineItemModel> donationLineItemModelList = new ArrayList<>();
                    donationLineItemModelList.add(donationLineItemModel);

                    transactionModel.setDonations(donationLineItemModelList);
                }

                loyaltyPointModel.setLoyaltyProgram(loyaltyProgramModel);
                loyaltyPointModel.setLoyaltyAccrualPolicy(loyaltyAccrualPolicyModel);

                List<LoyaltyPointModel> loyaltyPointModels = new ArrayList<>();
                loyaltyPointModels.add(loyaltyPointModel);
                transactionModel.setLoyaltyPointsEarned(loyaltyPointModels);

                transactionModels.add(transactionModel);

            }
        }

        return transactionModels;
    }

    /**
     * Expire any open Transactions
     * Looks for Transactions in the current Revenue Center, Trans Type = 15 (Open), Trans Status = 1 (Available)
     * and where QC_RevenueCenter.PAOpenTxnLifeSpan (in minutes) + QC_PATransaction.TransactionDate < Current Date
     * @param terminalModel
     * @return
     * @throws Exception
     */
    public static Integer expireOpenTransactions(TerminalModel terminalModel) throws Exception {

        DataManager dm = new DataManager();
        String logFileName = PosAPIHelper.getLogFileName(terminalModel.getId());
        Logger.logMessage("Expiring Open Transaction. TransactionCollection.expireOpenTransactions.", logFileName, Logger.LEVEL.DEBUG);

        Integer result = dm.parameterizedExecuteNonQuery("data.posapi30.ExpireOpenTransactions",
                new Object[]{
                        terminalModel.getRevenueCenterId(), //Revenue Center Id

                }, logFileName
        );

        Logger.logMessage(result + " Open Transaction(s) were expired by expireOpenTransactions");

        return result;
    }


    public static List<TransactionModel> updateTransactionStatuses(List<TransactionModel> transactionList, TerminalModel terminalModel) throws Exception {

        DataManager dm = new DataManager();
        List<TransactionModel> updatedTransactionList  = new ArrayList<TransactionModel>();
        for (TransactionModel curTM : transactionList) {
            if (curTM != null)
            {
                Integer transactionId = curTM.getId();
                TransactionQueryParams transactionQueryParams = new TransactionQueryParams();
                transactionQueryParams.setOpenOnly(true);
                TransactionModel validTransaction = new TransactionModel().getOneTransactionByIdAndSetOriginalTransaction(transactionId, terminalModel, transactionQueryParams);
                TransactionBuilder transactionBuilder = TransactionBuilder.createTransactionBuilder(terminalModel, validTransaction, PosAPIHelper.ApiActionType.OPEN, null);
                transactionBuilder.getTransactionModel().setAccountQueryParams(new AccountQueryParams(true));
                transactionBuilder.buildTransaction();
                //transactionBuilder.updateOpenTransactionStatus(curTM.getTransactionStatus());
               // TransactionModel updatedTransModel = transactionBuilder.getTransactionModel();

                String logFileName = PosAPIHelper.getLogFileName(validTransaction.getId());

                Logger.logMessage("Updating Open Transaction Status. TransactionCollection.updateTransactionStatuses.", logFileName, Logger.LEVEL.DEBUG);

                String lastUpdatedDate = TransactionBuilder.getCurrentDate();
                Integer newTransStatusId =  PosAPIHelper.TransactionStatus.fromStringToInt(curTM.getTransactionStatus());

                String errorMsg = "Error updating Open Transaction Status";
                Boolean cantUpdateStatus = false;


                if ( (curTM.getTransactionStatus().length()<1) || (newTransStatusId == -1) )
                {
                    cantUpdateStatus = true;
                    errorMsg = "Error updating Open Transaction Status - No valid status found on transaction model";
                }
                else
                {
                    curTM.setTransactionStatusId(newTransStatusId);
                }

                String newTransStatus = curTM.getTransactionStatus();
                Integer curTransStatusId = validTransaction.getTransactionStatusId();


                Boolean isSameStatus = false;

                if (curTransStatusId.equals(newTransStatusId))
                {
                    isSameStatus = true;
                    errorMsg = "Error: Attempted to Update Open Transaction Status to Current Status";
                }
                else if (!cantUpdateStatus)
                {
                    switch (curTransStatusId)
                    {
                        // OPEN/SUSPENDED(1, "Suspended") - Can be changed to anything
                        case 1:
                            break;
                        // EXPIRED(2, "Expired")
                        case 2:
                            errorMsg = "Error Updating EXPIRED Transaction Status";
                            cantUpdateStatus = true;
                            break;
                        // EDIT_COMPLETED(3, "Edit Completed") - can't be changed
                        case 3:
                            errorMsg = "Error Updating EDIT COMPLETED Transaction Status";
                            cantUpdateStatus = true;
                            break;
                        // EDIT_IN_PROGRESS(4, "Edit In Progress") - can go to edit completed (3) or open (1)
                        case 4:
                            switch (newTransStatusId)
                            {
                                case 2:
                                    errorMsg = "Error Updating EDIT IN PROGRESS Transaction Status to EXPIRED";
                                    cantUpdateStatus = true;
                                    break;
                                case 5:
                                    errorMsg = "Error Updating EDIT IN PROGRESS Transaction Status to COMPLETED";
                                    cantUpdateStatus = true;
                                    break;
                            }
                            break;
                        // COMPLETED(5, "Completed")  - can't be changed
                        case 5:
                            errorMsg = "Error Updating COMPLETED Transaction Status";
                            cantUpdateStatus = true;
                            break;
                    }
                }

                if ( (cantUpdateStatus) || (isSameStatus) )
                {
                    throw new MissingDataException(errorMsg, validTransaction.getTerminal().getId());
                }
                else
                {
                    Integer updateResult = dm.parameterizedExecuteNonQuery("data.posapi30.UpdateOpenTransaction", new Object[]{
                            validTransaction.getId(),
                            newTransStatusId,
                            lastUpdatedDate

                    }, logFileName);
                    if (updateResult == null || updateResult <= 0) {
                        throw new MissingDataException(errorMsg, validTransaction.getTerminal().getId());
                    }
                    else
                    {
                        if (newTransStatus.toUpperCase().equals("OPEN"))
                        {
                            newTransStatus = "SUSPENDED";
                        }
                        validTransaction.setTransactionStatus(newTransStatus);
                        validTransaction.setTransactionStatusId(newTransStatusId);
                        TransactionModel updatedTransModel = transactionBuilder.getTransactionModel();
                        updatedTransactionList.add(updatedTransModel);
                    }
                }

            }
        }
        return updatedTransactionList;
    }

    public List<TenderModel> getCollection() {
        return collection;
    }

    public void setCollection(List<TenderModel> collection) {
        this.collection = collection;
    }
}
