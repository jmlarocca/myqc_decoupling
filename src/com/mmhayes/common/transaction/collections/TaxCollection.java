package com.mmhayes.common.transaction.collections;

//MMHayes dependencies
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.transaction.models.TaxModel;
import com.mmhayes.common.terminal.models.TerminalModel;

//API dependencies
import com.mmhayes.common.api.CommonAPI;

//other dependencies
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/*
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2019-10-03 12:11:13 -0400 (Thu, 03 Oct 2019) $: Date of last commit
 $Rev: 9536 $: Revision of last commit
*/
public class TaxCollection {

    private List<TaxModel> collection = new ArrayList<>();
    private static DataManager dm = new DataManager();

    //constructor
    public TaxCollection() {

    }

    public static TaxCollection getAllTaxes(TerminalModel terminalModel) throws Exception {
        TaxCollection taxCollection = new TaxCollection();

        //get all models in an array list
        ArrayList<HashMap> taxList = dm.parameterizedExecuteQuery("data.posapi30.getAllTaxes",
                new Object[]{terminalModel.getRevenueCenterId()},
                PosAPIHelper.getLogFileName(terminalModel.getId()),
                true
        );

        //product list is null or empty, throw exception
        for (HashMap taxHM : taxList) {
            CommonAPI.checkIsNullOrEmptyObject(taxHM.get("ID"), "Tax Id could not be found", terminalModel.getId());
            //create a new model and add to the collection
            taxCollection.getCollection().add(TaxModel.createTaxModel(taxHM));
        }

        return taxCollection;
    }

    public static TaxCollection getTaxesForReward(String rewardIds, Integer terminalId) throws Exception {
        TaxCollection taxCollection = new TaxCollection();
        ArrayList<HashMap> taxList = new ArrayList<>();

        taxList = dm.serializeSqlWithColNames("data.posapi30.getTaxesForReward", new Object[]{ rewardIds }, null, PosAPIHelper.getLogFileName(terminalId));

        //product list is null or empty, throw exception
        for (HashMap taxHM : taxList) {
            CommonAPI.checkIsNullOrEmptyObject(taxHM.get("ID"), "Tax Id could not be found", terminalId);
            //create a new model and add to the collection
            taxCollection.getCollection().add(TaxModel.createTaxModel(taxHM));
        }

        return taxCollection;
    }

    /**
     * Make a collection of TaxModel for a loyalty reward. Only active taxes which are mapped to the revenue center will be included.
     * @param taxIds Comma separated list suitable for insertion into a select statement as in "in ('taxIds')" clause
     * @param terminal Provide terminal id and revenue center id
     * @return
     * @throws Exception
     */
    public static TaxCollection getTaxesForReward(final String taxIds, final TerminalModel terminal)
            throws Exception
    {
        TaxCollection taxCollection = new TaxCollection();
        ArrayList<HashMap> taxList = new ArrayList<>();

        taxList = dm.serializeSqlWithColNames("data.posapi30.getMappedTaxesForRewardWithRevCtr",
                new Object[]{ taxIds, terminal.getId(), terminal.getRevenueCenterId() }, null, PosAPIHelper.getLogFileName(terminal.getId()));

        //product list is null or empty, throw exception
        //CommonAPI.checkIsNullOrEmptyList(taxList, "Tax List is empty");
        for (HashMap taxHM : taxList) {
            CommonAPI.checkIsNullOrEmptyObject(taxHM.get("ID"), "Tax Id could not be found", terminal.getId());
            //create a new model and add to the collection
            taxCollection.getCollection().add(TaxModel.createTaxModel(taxHM));
        }

        return taxCollection;
    }

    //returns an List<String> containing all taxIDs mapped to the revenue center of the given terminal
    public static List<String> getTaxIdsForTerminal(Integer terminalId) throws Exception {
        List<String> taxIdList = new ArrayList<String>();

        Object[] params = new Object[] {
            terminalId
        };

        ArrayList<HashMap> taxIds = dm.parameterizedExecuteQuery("data.posapi30.getTaxIdsForTerminal", params, PosAPIHelper.getLogFileName(terminalId), true);

        for ( HashMap taxIdHM : taxIds ) {
            taxIdList.add(CommonAPI.convertModelDetailToString(taxIdHM.get("TAXRATEID")));
        }

        return taxIdList;
    }

    public static TaxCollection getTaxesForModel(final String taxIds, final TerminalModel terminal) throws Exception
    {
        TaxCollection taxCollection = new TaxCollection();

        ArrayList<HashMap> taxList = dm.serializeSqlWithColNames("data.posapi30.getMappedTaxesForRewardWithRevCtr",
                new Object[]{ taxIds, terminal.getId(), terminal.getRevenueCenterId() }, null, PosAPIHelper.getLogFileName(terminal.getId()));

        //product list is null or empty, throw exception
        for (HashMap taxHM : taxList) {
            CommonAPI.checkIsNullOrEmptyObject(taxHM.get("ID"), "Tax Id could not be found", terminal.getId());
            //create a new model and add to the collection
            taxCollection.getCollection().add(TaxModel.createTaxModel(taxHM));
        }

        return taxCollection;
    }

    public static TaxCollection getTaxDeletesForDiningOption(Integer productId, Integer terminalId) throws Exception {
        TaxCollection taxCollection = new TaxCollection();
        ArrayList<HashMap> taxList = new ArrayList<>();

        taxList = dm.serializeSqlWithColNames("data.posapi30.getTaxDeletesForDiningOptionProductById", new Object[]{ productId }, null, PosAPIHelper.getLogFileName(terminalId));

        //product list is null or empty, throw exception
        for (HashMap taxHM : taxList) {
            CommonAPI.checkIsNullOrEmptyObject(taxHM.get("ID"), "Tax Id could not be found", terminalId);
            //create a new model and add to the collection
            taxCollection.getCollection().add(TaxModel.createTaxModel(taxHM));
        }

        return taxCollection;
    }

    //getter
    public List<TaxModel> getCollection() {
        return collection;
    }

    //setter
    public void setCollection(List<TaxModel> collection) {
        this.collection = collection;
    }

}
