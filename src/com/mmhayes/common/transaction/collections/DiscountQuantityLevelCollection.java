package com.mmhayes.common.transaction.collections;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.api.errorhandling.exceptions.MissingDataException;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.product.models.ProductCalculation;
import com.mmhayes.common.product.models.ProductModel;
import com.mmhayes.common.transaction.models.DiscountQuantityLevelCalculation;
import com.mmhayes.common.transaction.models.DiscountQuantityLevelModel;
import com.mmhayes.common.transaction.models.QcDiscountModel;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.StringFunctions;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/*
 Last Updated (automatically updated by SVN)
 $Author: jmdottavio $: Author of last commit
 $Date: 2018-03-20 11:19:43 -0400 (Tue, 20 Mar 2018) $: Date of last commit
 $Rev: 6640 $: Revision of last commit
*/
@JsonPropertyOrder({"id", "name", "type", "amount"})
public class DiscountQuantityLevelCollection {

    private static DataManager dm = new DataManager();

    private Integer discountId = null;
    private HashMap<Integer, DiscountQuantityLevelModel> departmentLevels = new HashMap<Integer, DiscountQuantityLevelModel>();
    private HashMap<Integer, DiscountQuantityLevelModel> subDepartmentLevels = new HashMap<Integer, DiscountQuantityLevelModel>();
    private HashMap<Integer, DiscountQuantityLevelModel> productLevels = new HashMap<Integer, DiscountQuantityLevelModel>();
    private List<DiscountQuantityLevelModel> collection = new ArrayList<DiscountQuantityLevelModel>(); //holds all related discount quantity levels

    //default constructor
    public DiscountQuantityLevelCollection() {

    }

    public DiscountQuantityLevelCollection(Integer discountId, Integer terminalId) {
        this.setDiscountId( discountId );

        this.populateCollection( terminalId );
    }

    public DiscountQuantityLevelCollection populateCollection(Integer terminalId) {
        ArrayList<HashMap> discountQuantityLevelList = dm.parameterizedExecuteQuery("data.posapi30.getDiscountQuantityLevels",
                new Object[]{
                        this.getDiscountId()
                },
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        if ( discountQuantityLevelList == null || discountQuantityLevelList.size() == 0 ) {
            return this;
        }

        for ( HashMap discountQuantityLevelHM : discountQuantityLevelList ) {
            DiscountQuantityLevelModel discountQuantityLevelModel = new DiscountQuantityLevelModel(discountQuantityLevelHM);
            this.getCollection().add( discountQuantityLevelModel );
        }

        return this;
    }

    //return a list of products that were previously purchased today within the time period of the given QC Discount
    private List<ProductCalculation> determinePastPurchaseProducts( QcDiscountModel qcDiscountModel, Integer employeeId ) throws Exception {
        List<ProductCalculation> previousPurchaseProducts = new ArrayList<ProductCalculation>();

        LocalTime discountStartTime = LocalTime.parse( qcDiscountModel.getStartTime() );

        //search for transactions using this discount from today
        ArrayList<HashMap> transactions = dm.parameterizedExecuteQuery("data.posapi30.getPreviousPurchaseTransactions",
                new Object[]{
                        employeeId,
                        getDiscountId()
                },
                true
        );

        //if no transactions found, just return that there are no previous products
        if ( transactions == null || transactions.size() == 0 ) {
            return previousPurchaseProducts;
        }

        //build a list of all transactions that fall inside the startTime of the discount for today
        List<String> transactionIds = new ArrayList<>();
        for ( HashMap transaction : transactions ) {
            String transDTM = CommonAPI.convertModelDetailToString( transaction.get("TRANSACTIONDATE") ).substring(0, 19);
            LocalTime transDateTime = LocalTime.parse( transDTM, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));

            //if this check fails, it's ok to just break because the transactions are in reverse date order, all the next ones will fail too
            if ( transDateTime.isBefore( discountStartTime ) ) {
                break;
            }

            String transactionId = CommonAPI.convertModelDetailToString(transaction.get("PATRANSACTIONID"));
            transactionIds.add(transactionId);
        }

        //if no transactions fall inside the startTime of the discount, return no previous products
        if ( transactionIds.size() == 0 ) {
            return previousPurchaseProducts;
        }

        String transIdsStr = String.join(",", transactionIds);

        ArrayList<HashMap> products = dm.parameterizedExecuteQuery("data.posapi30.getPreviousPurchaseProducts",
            new Object[]{
                transIdsStr
            },
            true
        );

        //if no products found, just return that there are no previous products
        if ( products == null || products.size() == 0 ) {
            return previousPurchaseProducts;
        }

        //build product calculation models with product models holding the necessary (deptID, subdeptID) data
        for ( HashMap product : products ) {
            Integer productId = CommonAPI.convertModelDetailToInteger( product.get("PAITEMID") );
            ProductModel productModel = ProductModel.getSimpleProductModelById( productId );

            //could not find product model for some reason, skipping this product
            if ( productModel == null ) {
                continue;
            }

            ProductCalculation productCalculation = new ProductCalculation( product, productModel, null );

            previousPurchaseProducts.add( productCalculation );
        }

        return previousPurchaseProducts;
    }

    //returns a list of product calculations with appropriate quantity for the discount quantity level restrictions
    public List<ProductCalculation> determineEligibleProducts( List<ProductCalculation> transactionProducts, QcDiscountModel qcDiscountModel, Integer employeeId ) throws Exception {
        List<ProductCalculation> eligibleProducts = new ArrayList<ProductCalculation>();

        //sort the discount quantity levels into types - department, sub department, product
        for ( DiscountQuantityLevelModel discountQuantityLevelModel : this.getCollection() ) {
            if ( discountQuantityLevelModel.getDepartmentId() != null ) {
                this.getDepartmentLevels().put( discountQuantityLevelModel.getDepartmentId(), discountQuantityLevelModel );
            } else if ( discountQuantityLevelModel.getSubDepartmentId() != null ) {
                this.getSubDepartmentLevels().put( discountQuantityLevelModel.getSubDepartmentId(), discountQuantityLevelModel );
            } else if ( discountQuantityLevelModel.getProductId() != null ) {
                this.getProductLevels().put( discountQuantityLevelModel.getProductId(), discountQuantityLevelModel );
            } else {
                throw new MissingDataException("Could not determine type of discount quantity level");
            }
        }

        boolean validDiscountQuantityRestrictionsForTransaction = false;

        //add products to eligible lists inside of appropriate discount quantity level models
        for ( ProductCalculation transactionProduct : transactionProducts ) {
            boolean productHasDiscountQuantityRestriction = false;

            //first check if this product is even valid for this discount
            if ( !transactionProduct.getProductModel().isDiscountValidForProduct( this.getDiscountId() ) ) {
                continue;
            }

            //check if the product matches any department discount quantity levels
            if ( this.getDepartmentLevels().containsKey( transactionProduct.getProductModel().getDepartmentId() ) ) {
                //flag that this product is governed by a discount quantity restriction
                productHasDiscountQuantityRestriction = true;

                //if the quantity restriction is to zero, prevent this product from being added to any sub dept or product levels
                if ( getDepartmentLevels().get( transactionProduct.getProductModel().getDepartmentId() ).getMaxQuantity().equals( 0 ) ) {
                    validDiscountQuantityRestrictionsForTransaction = true;
                    continue;
                }

                //clone the product calculation model so changing the quantity won't affect things later on
                ProductCalculation productCalculation = new ProductCalculation(transactionProduct);

                //if so add the product to the list of eligible products for that discount quantity level
                this.getDepartmentLevels().get( transactionProduct.getProductModel().getDepartmentId() ).getEligibleProducts().add( productCalculation );
            }

            //check if the product matches any sub department discount quantity levels
            if ( this.getSubDepartmentLevels().containsKey( transactionProduct.getProductModel().getSubDepartmentId() ) ) {
                //flag that this product is governed by a discount quantity restriction
                productHasDiscountQuantityRestriction = true;

                //if the quantity restriction is to zero, prevent this product from being added to any product levels
                if ( getSubDepartmentLevels().get( transactionProduct.getProductModel().getSubDepartmentId() ).getMaxQuantity().equals( 0 ) ) {
                    validDiscountQuantityRestrictionsForTransaction = true;
                    continue;
                }

                //clone the product calculation model so changing the quantity won't affect things later on
                ProductCalculation productCalculation = new ProductCalculation( transactionProduct );

                //if so add the product to the list of eligible products for that discount quantity level
                this.getSubDepartmentLevels().get( transactionProduct.getProductModel().getSubDepartmentId() ).getEligibleProducts().add( productCalculation );
            }

            //check if the product matches any product discount quantity levels
            if ( this.getProductLevels().containsKey( transactionProduct.getId() ) ) {
                //flag that this product is governed by a discount quantity restriction
                productHasDiscountQuantityRestriction = true;

                //if the quantity restriction is to zero, prevent this product from being added to any product levels
                if ( getProductLevels().get( transactionProduct.getId() ).getMaxQuantity().equals( 0 ) ) {
                    validDiscountQuantityRestrictionsForTransaction = true;
                    continue;
                }

                //clone the product calculation model so changing the quantity won't affect things later on
                ProductCalculation productCalculation = new ProductCalculation( transactionProduct );

                //if so add the product to the list of eligible products for that discount quantity level
                this.getProductLevels().get( transactionProduct.getId() ).getEligibleProducts().add( productCalculation );
            }

            //if this product is not governed by any discount quantity restriction, then add it as eligible like normal
            if ( !productHasDiscountQuantityRestriction ) {
                eligibleProducts.add( transactionProduct );
            } else {
                validDiscountQuantityRestrictionsForTransaction = true;
            }
        }

        //no products in this transaction are restricted by the discount quantity levels, proceed as normal
        if ( !validDiscountQuantityRestrictionsForTransaction ) {
            return eligibleProducts;
        }

        //if this discount is not single use, then consider the products purchased today in addition to those on this transaction
        if ( qcDiscountModel != null && !qcDiscountModel.isSingleUse() ) {
            List<ProductCalculation> previousPurchaseProducts = determinePastPurchaseProducts(qcDiscountModel, employeeId);

            for ( ProductCalculation previousPurchaseProduct : previousPurchaseProducts ) {
                //check if the product matches any department discount quantity levels
                if ( this.getDepartmentLevels().containsKey( previousPurchaseProduct.getProductModel().getDepartmentId() ) ) {
                    //if so increment the previous product count for this level
                    this.getDepartmentLevels().get( previousPurchaseProduct.getProductModel().getDepartmentId() ).incrementPreviousProductCount( previousPurchaseProduct.getQuantity() );
                }

                //check if the product matches any sub department discount quantity levels
                if ( this.getSubDepartmentLevels().containsKey( previousPurchaseProduct.getProductModel().getSubDepartmentId() ) ) {
                    //if so increment the previous product count for this level
                    this.getSubDepartmentLevels().get(previousPurchaseProduct.getProductModel().getSubDepartmentId()).incrementPreviousProductCount( previousPurchaseProduct.getQuantity() );
                }

                //check if the product matches any product discount quantity levels
                if ( this.getProductLevels().containsKey( previousPurchaseProduct.getProductModel().getId() ) ) {
                    //if so increment the previous product count for this level
                    this.getProductLevels().get( previousPurchaseProduct.getProductModel().getId()).incrementPreviousProductCount( previousPurchaseProduct.getQuantity() );
                }
            }
        }

        //holds <DepartmentID, List of rules that apply to this department>
        HashMap<Integer, DiscountQuantityLevelCalculation> applicableLevels = new HashMap<Integer, DiscountQuantityLevelCalculation>();

        //ensure that only the maxQuantity of products is in each quantity level, choose the best products if necessary
        if ( this.getProductLevels().size() > 0 ) {
            for (Map.Entry<Integer, DiscountQuantityLevelModel> entry : this.getProductLevels().entrySet() ) {
                DiscountQuantityLevelModel productDQL = entry.getValue();

                //if there are no products that meet the criteria of this level, continue
                if ( productDQL.getEligibleProducts().size() == 0 ) {
                    continue;
                }

                //check to restrict the quantity of products in this level and return whether a restriction was necessary
                boolean restrictionNecessary = productDQL.determineProductsForBestDiscount();

                ProductModel productModel = productDQL.getEligibleProducts().get(0).getProductModel();

                Integer departmentID = productModel.getDepartmentId();

                //add the productDQL to the appropriate department's DiscountQuantityLevelCalculation
                DiscountQuantityLevelCalculation discountQuantityLevelCalculation;
                if ( applicableLevels.containsKey( departmentID ) ) {
                    discountQuantityLevelCalculation = applicableLevels.get( departmentID );
                    discountQuantityLevelCalculation.addProductDQL( productDQL );
                } else {
                    discountQuantityLevelCalculation = new DiscountQuantityLevelCalculation();
                    discountQuantityLevelCalculation.setDepartmentId( departmentID );
                    discountQuantityLevelCalculation.addProductDQL( productDQL );
                    applicableLevels.put( departmentID, discountQuantityLevelCalculation );
                }

                //enforce a product level restriction on the sub department and department of this product as necessary

                //if the sub department of this product has a discount quantity restriction
                if ( this.getSubDepartmentLevels().containsKey( productModel.getSubDepartmentId() ) ) {
                    DiscountQuantityLevelModel subDeptDQL = this.getSubDepartmentLevels().get( productModel.getSubDepartmentId() );

                    //only do a restriction if the max of the product is less than the max of the subdept
                    if ( restrictionNecessary && productDQL.getMaxQuantity() < subDeptDQL.getMaxQuantity() ) {
                        subDeptDQL.restrictProductQuantity( entry.getKey(), productDQL.getMaxQuantityBD() );
                    }
                }

                //if the department of this product has a discount quantity restriction
                if ( this.getDepartmentLevels().containsKey( departmentID ) ) {
                    DiscountQuantityLevelModel deptDQL = this.getDepartmentLevels().get( departmentID );

                    //only do a restriction if the max of the product is less than the max of the dept
                    if ( restrictionNecessary && productDQL.getMaxQuantity() < deptDQL.getMaxQuantity() ) {
                        deptDQL.restrictProductQuantity( entry.getKey(), productDQL.getMaxQuantityBD() );
                    }
                }
            }
        }

        if ( this.getSubDepartmentLevels().size() > 0 ) {
            for (Map.Entry<Integer, DiscountQuantityLevelModel> entry : this.getSubDepartmentLevels().entrySet() ) {
                DiscountQuantityLevelModel subDeptDQL = entry.getValue();

                //if there are no products that meet the criteria of this level, continue
                if ( subDeptDQL.getEligibleProducts().size() == 0 ) {
                    continue;
                }

                boolean restrictionNecessary = subDeptDQL.determineProductsForBestDiscount();

                ProductModel productModel = subDeptDQL.getEligibleProducts().get(0).getProductModel();

                Integer departmentID = productModel.getDepartmentId();

                //add the productDQL to the appropriate department's DiscountQuantityLevelCalculation
                DiscountQuantityLevelCalculation discountQuantityLevelCalculation;
                if ( applicableLevels.containsKey( departmentID ) ) {
                    discountQuantityLevelCalculation = applicableLevels.get( departmentID );
                    discountQuantityLevelCalculation.addSubDeptDQL( subDeptDQL );
                } else {
                    discountQuantityLevelCalculation = new DiscountQuantityLevelCalculation();
                    discountQuantityLevelCalculation.setDepartmentId( departmentID );
                    discountQuantityLevelCalculation.addSubDeptDQL(subDeptDQL);
                    applicableLevels.put( departmentID, discountQuantityLevelCalculation );
                }

                //if the department of this product has a discount quantity restriction
                if ( this.getDepartmentLevels().containsKey( productModel.getDepartmentId() ) ) {
                    DiscountQuantityLevelModel deptDQL = this.getDepartmentLevels().get( productModel.getDepartmentId() );

                    if ( restrictionNecessary && subDeptDQL.getMaxQuantity() < deptDQL.getMaxQuantity() ) {
                        deptDQL.restrictSubDeptQuantity( entry.getKey(), subDeptDQL.getMaxQuantityBD() );
                    }
                }
            }
        }

        if ( this.getDepartmentLevels().size() > 0 ) {
            for (Map.Entry<Integer, DiscountQuantityLevelModel> entry : this.getDepartmentLevels().entrySet() ) {
                DiscountQuantityLevelModel deptDQL = entry.getValue();

                //if there are no products that meet the criteria of this level, continue
                if ( deptDQL.getEligibleProducts().size() == 0 ) {
                    continue;
                }

                deptDQL.determineProductsForBestDiscount();

                ProductModel productModel = deptDQL.getEligibleProducts().get(0).getProductModel();

                Integer departmentID = productModel.getDepartmentId();

                //add the productDQL to the appropriate department's DiscountQuantityLevelCalculation
                DiscountQuantityLevelCalculation discountQuantityLevelCalculation;
                if ( !applicableLevels.containsKey( departmentID ) ) {
                    discountQuantityLevelCalculation = new DiscountQuantityLevelCalculation();
                    discountQuantityLevelCalculation.setDepartmentId( departmentID );
                    discountQuantityLevelCalculation.setDeptDQL( deptDQL );
                    applicableLevels.put(departmentID, discountQuantityLevelCalculation);
                } else {
                    discountQuantityLevelCalculation = applicableLevels.get( departmentID );
                    discountQuantityLevelCalculation.setDeptDQL( deptDQL );
                }
            }
        }

        //go through all applicable levels and add the products to the eligible list
        for ( Map.Entry<Integer, DiscountQuantityLevelCalculation> entry : applicableLevels.entrySet() ) {
            DiscountQuantityLevelCalculation discountQuantityLevelCalculation = entry.getValue();
            eligibleProducts.addAll( discountQuantityLevelCalculation.getEligibleProducts() );
        }

        return eligibleProducts;
    }

    public Integer getDiscountId() {
        return discountId;
    }

    public void setDiscountId(Integer discountId) {
        this.discountId = discountId;
    }

    public List<DiscountQuantityLevelModel> getCollection() {
        return collection;
    }

    public void setCollection(List<DiscountQuantityLevelModel> collection) {
        this.collection = collection;
    }

    public HashMap<Integer, DiscountQuantityLevelModel> getDepartmentLevels() {
        return departmentLevels;
    }

    public void setDepartmentLevels(HashMap<Integer, DiscountQuantityLevelModel> departmentLevels) {
        this.departmentLevels = departmentLevels;
    }

    public HashMap<Integer, DiscountQuantityLevelModel> getSubDepartmentLevels() {
        return subDepartmentLevels;
    }

    public void setSubDepartmentLevels(HashMap<Integer, DiscountQuantityLevelModel> subDepartmentLevels) {
        this.subDepartmentLevels = subDepartmentLevels;
    }

    public HashMap<Integer, DiscountQuantityLevelModel> getProductLevels() {
        return productLevels;
    }

    public void setProductLevels(HashMap<Integer, DiscountQuantityLevelModel> productLevels) {
        this.productLevels = productLevels;
    }

    /**
     * Overridden toString method for a DiscountQuantityLevelCollection.
     * @return The {@link String} representation of a DiscountQuantityLevelCollection.
     */
    @Override
    public String toString () {

        String departmentLevelsStr = "";
        if (!DataFunctions.isEmptyMap(departmentLevels)) {
            departmentLevelsStr += "[";
            int departmentLevelsSize = departmentLevels.size();
            int numberOfEntriesExamined = 0;
            for (Map.Entry<Integer, DiscountQuantityLevelModel> entry : departmentLevels.entrySet()) {
                if ((entry.getKey() != null) && (entry.getValue() != null) && (numberOfEntriesExamined - 1 < departmentLevelsSize)) {
                    departmentLevelsStr += "[" + String.valueOf(entry.getKey()) + ": " + entry.getValue().toString() + "], ";
                }
                else if ((entry.getKey() != null) && (entry.getValue() != null) && (numberOfEntriesExamined - 1 == departmentLevelsSize)) {
                    departmentLevelsStr += "[" + String.valueOf(entry.getKey()) + ": " + entry.getValue().toString() + "]";
                }
                numberOfEntriesExamined++;
            }
            departmentLevelsStr += "]";
        }

        String subDepartmentLevelsStr = "";
        if (!DataFunctions.isEmptyMap(subDepartmentLevels)) {
            subDepartmentLevelsStr += "[";
            int subDepartmentLevelsSize = subDepartmentLevels.size();
            int numberOfEntriesExamined = 0;
            for (Map.Entry<Integer, DiscountQuantityLevelModel> entry : subDepartmentLevels.entrySet()) {
                if ((entry.getKey() != null) && (entry.getValue() != null) && (numberOfEntriesExamined - 1 < subDepartmentLevelsSize)) {
                    subDepartmentLevelsStr += "[" + String.valueOf(entry.getKey()) + ": " + entry.getValue().toString() + "], ";
                }
                else if ((entry.getKey() != null) && (entry.getValue() != null) && (numberOfEntriesExamined - 1 == subDepartmentLevelsSize)) {
                    subDepartmentLevelsStr += "[" + String.valueOf(entry.getKey()) + ": " + entry.getValue().toString() + "]";
                }
                numberOfEntriesExamined++;
            }
            subDepartmentLevelsStr += "]";
        }
        
        String productLevelsStr = "";
        if (!DataFunctions.isEmptyMap(productLevels)) {
            productLevelsStr += "[";
            int productLevelsSize = productLevels.size();
            int numberOfEntriesExamined = 0;
            for (Map.Entry<Integer, DiscountQuantityLevelModel> entry : productLevels.entrySet()) {
                if ((entry.getKey() != null) && (entry.getValue() != null) && (numberOfEntriesExamined - 1 < productLevelsSize)) {
                    productLevelsStr += "[" + String.valueOf(entry.getKey()) + ": " + entry.getValue().toString() + "], ";
                }
                else if ((entry.getKey() != null) && (entry.getValue() != null) && (numberOfEntriesExamined - 1 == productLevelsSize)) {
                    productLevelsStr += "[" + String.valueOf(entry.getKey()) + ": " + entry.getValue().toString() + "]";
                }
                numberOfEntriesExamined++;
            }
            productLevelsStr += "]";
        }

        String discountCollectionStr = "";
        if (!DataFunctions.isEmptyCollection(collection)) {
            discountCollectionStr += "[";
            for (DiscountQuantityLevelModel discount : collection) {
                if (discount != null && discount.equals(collection.get(collection.size() - 1))) {
                    discountCollectionStr += "[" + discount.toString() + "]";
                }
                else if (discount != null && !discount.equals(collection.get(collection.size() - 1))) {
                    discountCollectionStr += "[" + discount.toString() + "]; ";
                }
            }
            discountCollectionStr += "]";
        }

        return String.format("DISCOUNTID: %s, DEPARTMENTLEVELS: %s, SUBDEPARTMENTLEVELS: %s, PRODUCTLEVELS: %s, DISCOUNTCOLLECTION: %s",
                Objects.toString((discountId != null && discountId > 0 ? discountId : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(departmentLevelsStr) ? departmentLevelsStr : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(subDepartmentLevelsStr) ? subDepartmentLevelsStr : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(productLevelsStr) ? productLevelsStr : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(discountCollectionStr) ? discountCollectionStr : "N/A"), "N/A"));
        
    }
    
}




