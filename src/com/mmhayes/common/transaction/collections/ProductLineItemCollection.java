package com.mmhayes.common.transaction.collections;

//API dependencies
import com.mmhayes.common.transaction.models.ProductLineItemModel;

//other dependencies
import java.util.ArrayList;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2017-02-16 11:12:54 -0500 (Thu, 16 Feb 2017) $: Date of last commit
 $Rev: 3553 $: Revision of last commit
*/
public class ProductLineItemCollection {
    private ArrayList<ProductLineItemModel> collection = new ArrayList<>();

    public ProductLineItemCollection() {

    }

    public ArrayList<ProductLineItemModel> getCollection() {
        return collection;

    }

    public void setCollection(ArrayList<ProductLineItemModel> collection) {
        this.collection = collection;
    }
}
