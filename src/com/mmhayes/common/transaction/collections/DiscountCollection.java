package com.mmhayes.common.transaction.collections;

//mmhayes dependencies

import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.dataaccess.DataManager;

//API dependencies
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.errorhandling.exceptions.*;
import com.mmhayes.common.transaction.models.*;

//other dependencies
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2017-06-16 12:44:46 -0400 (Fri, 16 Jun 2017) $: Date of last commit
 $Rev: 4152 $: Revision of last commit
*/
public class DiscountCollection {
    private List<DiscountModel> collection = new ArrayList<>();
    private DataManager dm = new DataManager();

    public DiscountCollection() {

    }

    //get all discounts from the database in order to map the discount
    public static List<DiscountModel> getAllDiscountsInDB(Integer terminalId) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> discountList = dm.parameterizedExecuteQuery("data.posapi30.getAllPADiscounts",
                new Object[]{},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        List<DiscountModel> allDiscountCollection = new ArrayList<>();

        for (HashMap discountHM : discountList) {
            CommonAPI.checkIsNullOrEmptyObject(discountHM.get("ID"), "Discount Id cannot be found.", terminalId);
            //create a new model and add to the collection
            allDiscountCollection.add(DiscountModel.createDiscountModel(discountHM));
        }

        return allDiscountCollection;
    }

    //validate the list of discounts on the transaction with what's in the database
    //All discounts are grabbed at once, so it's only one call to the database
    public static List<DiscountModel> verifyListOfDiscounts(List<DiscountModel> discountsFromTransaction, Integer terminalId) throws Exception {
        //get a list of all valid discounts from the db
        List<DiscountModel> discountsFromDB = DiscountCollection.getAllDiscountsInDB(terminalId);
        List<DiscountModel> validatedListOfDiscounts = new ArrayList<>();


        //Discounts coming in from json
        for (DiscountModel discountFromTx : discountsFromTransaction) {
            boolean hasBeenFound = false;

            //Discounts from DB
            for (DiscountModel discountModelFromDB : discountsFromDB) {

                if (discountFromTx.equals(discountModelFromDB)) {
                    hasBeenFound = true;
                    validatedListOfDiscounts.add(discountModelFromDB);
                }
            }

            if (!hasBeenFound) {
                throw new MissingDataException("Invalid Discount found in the request", terminalId);
            }
        }


        return validatedListOfDiscounts;
    }

    //getter
    public List<DiscountModel> getCollection() {
        return collection;
    }

    //setter
    public void setCollection(List<DiscountModel> collection) {
        this.collection = collection;
    }
}

