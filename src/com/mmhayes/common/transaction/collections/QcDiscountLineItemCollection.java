package com.mmhayes.common.transaction.collections;

//mmhayes dependencies
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.dataaccess.DataManager;

//API dependencies
import com.mmhayes.common.transaction.models.*;

//other dependencies
import java.util.ArrayList;
import java.util.HashMap;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2017-05-05 11:39:16 -0400 (Fri, 05 May 2017) $: Date of last commit
 $Rev: 3905 $: Revision of last commit
*/
public class QcDiscountLineItemCollection {

    public QcDiscountLineItemCollection(){

    }

    private ArrayList<QcDiscountLineItemModel> collection = new ArrayList<>();

    public static QcDiscountLineItemCollection getAllQcDiscountsByTransactionId(Integer transactionId, Integer terminalId) throws Exception {
        DataManager dm = new DataManager();
        QcDiscountLineItemCollection qcDiscountLineItemCollection = new QcDiscountLineItemCollection();

        //get all models in an array list
        ArrayList<HashMap>qcDiscountLineItemListHM = dm.parameterizedExecuteQuery("data.posapi30.getQcDiscountsForTransactionId",
                new Object[]{ transactionId },
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        //modifier list is null or empty, throw exception
        //CommonAPI.checkIsNullOrEmptyList(modifierLineItemListHM, "Modifier could not be found");

        for (HashMap modelHM : qcDiscountLineItemListHM) {
            //CommonAPI.checkIsNullOrEmptyObject(modelHM.get("ID"), "LoyaltyProgramId Id could not be found");
            qcDiscountLineItemCollection.getCollection().add(QcDiscountLineItemModel.createQcDiscountLineItem(modelHM, terminalId));  //create a new model and add to the collection
        }

        return qcDiscountLineItemCollection;
    }

    public static QcDiscountLineItemCollection getAllQcDiscountsByTransIdAndTransLineId(Integer transactionId, Integer transLineId, Integer terminalId) throws Exception {
        DataManager dm = new DataManager();
        QcDiscountLineItemCollection qcDiscountLineItemCollection = new QcDiscountLineItemCollection();

        //get all models in an array list
        ArrayList<HashMap>qcDiscountLineItemListHM = dm.parameterizedExecuteQuery("data.posapi30.getQcDiscountsForTransIdAndTransLineId",
                new Object[]{ transactionId, transLineId },
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        //modifier list is null or empty, throw exception
        //CommonAPI.checkIsNullOrEmptyList(modifierLineItemListHM, "Modifier could not be found");

        for (HashMap modelHM : qcDiscountLineItemListHM) {
            //CommonAPI.checkIsNullOrEmptyObject(modelHM.get("ID"), "LoyaltyProgramId Id could not be found");
            qcDiscountLineItemCollection.getCollection().add(QcDiscountLineItemModel.createQcDiscountLineItem(modelHM, terminalId));  //create a new model and add to the collection
        }

        return qcDiscountLineItemCollection;
    }

    public static QcDiscountLineItemModel getOneQcDiscountsByTransIdAndTransLineId(Integer transactionId, Integer transLineId, Integer terminalId) throws Exception {
        DataManager dm = new DataManager();
        QcDiscountLineItemModel qcDiscountLineItemModel = new QcDiscountLineItemModel();

        //get all models in an array list
        ArrayList<HashMap>qcDiscountLineItemListHM = dm.parameterizedExecuteQuery("data.posapi30.getQcDiscountsForTransIdAndTransLineId",
                new Object[]{ transactionId, transLineId },
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        //modifier list is null or empty, throw exception
        //CommonAPI.checkIsNullOrEmptyList(modifierLineItemListHM, "Modifier could not be found");

        if (qcDiscountLineItemListHM != null && qcDiscountLineItemListHM.size() > 0){
            qcDiscountLineItemModel =    QcDiscountLineItemModel.createQcDiscountLineItem(qcDiscountLineItemListHM.get(0), terminalId);
        }

        return qcDiscountLineItemModel;
    }

    public static QcDiscountLineItemCollection createQcDiscountLineItems(QcDiscountCollection qcDiscountCollection) throws Exception {
       QcDiscountLineItemCollection qcDiscountLineItemCollection = new QcDiscountLineItemCollection();

        for (QcDiscountModel qcDiscountModel : qcDiscountCollection.getCollection()) {
           QcDiscountLineItemModel qcDiscountLineItemModel = new QcDiscountLineItemModel();
            qcDiscountLineItemModel.setQcDiscount(qcDiscountModel);


            qcDiscountLineItemCollection.getCollection().add(qcDiscountLineItemModel);
        }

        return qcDiscountLineItemCollection;
    }

    public static QcDiscountLineItemCollection getAllQcDiscountsByQcPosTransactionId(String transactionId, Integer terminalId) throws Exception {
        DataManager dm = new DataManager();
        QcDiscountLineItemCollection qcDiscountLineItemCollection = new QcDiscountLineItemCollection();

        //get all models in an array list
        ArrayList<HashMap>qcDiscountLineItemListHM = dm.parameterizedExecuteQuery("data.posapi30.getQcDiscountsForQcPosTransactionId",
                new Object[]{ transactionId },
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        //modifier list is null or empty, throw exception
        //CommonAPI.checkIsNullOrEmptyList(modifierLineItemListHM, "Modifier could not be found");

        for (HashMap modelHM : qcDiscountLineItemListHM) {
            //CommonAPI.checkIsNullOrEmptyObject(modelHM.get("ID"), "LoyaltyProgramId Id could not be found");
            qcDiscountLineItemCollection.getCollection().add(QcDiscountLineItemModel.createQcDiscountLineItem(modelHM, terminalId));  //create a new model and add to the collection
        }

        return qcDiscountLineItemCollection;
    }

    public static QcDiscountLineItemCollection getAllQcDiscountsByPATransLineItemId(Integer paTransLineItemId, Integer terminalId) throws Exception {
        DataManager dm = new DataManager();
        QcDiscountLineItemCollection qcDiscountLineItemCollection = new QcDiscountLineItemCollection();

        //get all models in an array list
        ArrayList<HashMap>qcDiscountLineItemListHM = dm.parameterizedExecuteQuery("data.posapi30.getQcDiscountsForPATransLineItemId",
                new Object[]{ paTransLineItemId },
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        //modifier list is null or empty, throw exception
        //CommonAPI.checkIsNullOrEmptyList(modifierLineItemListHM, "Modifier could not be found");

        for (HashMap modelHM : qcDiscountLineItemListHM) {
            //CommonAPI.checkIsNullOrEmptyObject(modelHM.get("ID"), "LoyaltyProgramId Id could not be found");
            qcDiscountLineItemCollection.getCollection().add(QcDiscountLineItemModel.createQcDiscountLineItem(modelHM, terminalId));  //create a new model and add to the collection
        }

        return qcDiscountLineItemCollection;
    }

    public ArrayList<QcDiscountLineItemModel> getCollection() {
        return collection;
    }

    public void setCollection(ArrayList<QcDiscountLineItemModel> collection) {
        this.collection = collection;
    }

}
