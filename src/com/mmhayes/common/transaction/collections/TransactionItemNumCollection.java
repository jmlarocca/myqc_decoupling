package com.mmhayes.common.transaction.collections;

import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.api.errorhandling.exceptions.MissingDataException;
import com.mmhayes.common.donation.models.DonationLineItemModel;
import com.mmhayes.common.transaction.models.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2021-08-26 09:45:03 -0400 (Thu, 26 Aug 2021) $: Date of last commit
 $Rev: 15204 $: Revision of last commit
*/
public class TransactionItemNumCollection {

    private TransactionModel transactionModel = null;
    private ArrayList<Integer> numberList = new ArrayList<>();
    private String logFileName = "";

    public TransactionItemNumCollection(){

    }

    public TransactionItemNumCollection(TransactionModel transactionModel){
        this.setTransactionModel(transactionModel);
        this.setLogFileName(PosAPIHelper.getLogFileName(this.getTransactionModel().getTerminal().getId()));
    }

    private void initNumberList(HashMap<Integer, Integer> transactionItemNumDupeCheck) throws Exception {
        ArrayList<Integer> numberList = new ArrayList<>();

        for (Map.Entry<Integer, Integer> entry : transactionItemNumDupeCheck.entrySet()){
            if (entry.getKey() != null){
                numberList.add(entry.getKey());
            }
        }

        this.setNumberList(numberList);
    }

    public void validate() throws Exception{
        if (this.getTransactionModel().getTerminal().getTerminalModelId().compareTo(PosAPIHelper.PosTerminalModel.QC_POS.toInt()) != 0){
            return;
        }

        HashMap<Integer, Integer> transactionItemNumDupeCheck = this.validateDuplicateTransactionItemNums();
        this.initNumberList(transactionItemNumDupeCheck);
        this.validateNonSequentialTransactionItemNums();
    }

    /***
     * Check for any missing Transaction Item Numbers
     * It could be that Discounts or Rewards have been applied by the API
     */
    public void checkAndSetTransactionItemNumbers() throws Exception {
        /*The following object types should be assigned a TransactionItemNum value:
            Products (including modifiers)
            Subtotal discounts
            Rewards
            Tenders
            Donations
            Surcharges*/

        /*if (this.getTransactionModel().getTerminal().getTerminalModelId().compareTo(PosAPIHelper.PosTerminalModel.QC_POS.toInt()) != 0){
           return;
        }*/

        Integer lastTransactionItemNum = this.getMaxTransactionItemNumber();

        for (ProductLineItemModel productLineItemModel : this.getTransactionModel().getProducts()){
            if (productLineItemModel.getTransactionItemNum() == null){
                productLineItemModel.checkAndSetTransactionItemNum(++lastTransactionItemNum);
            }

            for (ModifierLineItemModel modifierLineItemModel : productLineItemModel.getModifiers()){
                if (modifierLineItemModel.getTransactionItemNum() == null) {
                    modifierLineItemModel.checkAndSetTransactionItemNum(++lastTransactionItemNum);
                }
            }
        }

        for (DiscountLineItemModel discountLineItemModel : this.getTransactionModel().getDiscounts()){
            if (discountLineItemModel.getDiscount().isSubTotalDiscount()){
                if (discountLineItemModel.getTransactionItemNum() == null) {
                    discountLineItemModel.checkAndSetTransactionItemNum(++lastTransactionItemNum);
                }
            }
        }

        for (LoyaltyRewardLineItemModel loyaltyRewardLineItemModel : this.getTransactionModel().getRewards()){
            if (loyaltyRewardLineItemModel.getTransactionItemNum() == null){
                loyaltyRewardLineItemModel.checkAndSetTransactionItemNum(++lastTransactionItemNum);
            }
        }

        for (TenderLineItemModel tenderLineItemModel : this.getTransactionModel().getTenders()){
            if (tenderLineItemModel.getTransactionItemNum() == null){
                tenderLineItemModel.checkAndSetTransactionItemNum(++lastTransactionItemNum);
            }
        }

        for (DonationLineItemModel donationLineItemModel : this.getTransactionModel().getDonations()){
            if (donationLineItemModel.getTransactionItemNum() == null){
                donationLineItemModel.checkAndSetTransactionItemNum(++lastTransactionItemNum);
            }
        }

        for (SurchargeLineItemModel surchargeLineItemModel : this.getTransactionModel().getSurcharges()){
            if (surchargeLineItemModel.getTransactionItemNum() == null){
                surchargeLineItemModel.checkAndSetTransactionItemNum(++lastTransactionItemNum);
            }
        }
    }

    private HashMap<Integer, Integer> validateDuplicateTransactionItemNums() throws Exception {
        HashMap<Integer, Integer> transactionItemNumDupeCheck = new HashMap<Integer, Integer>();

        for (ProductLineItemModel productLineItemModel : this.getTransactionModel().getProducts()){
            if (transactionItemNumDupeCheck.containsKey(productLineItemModel.getTransactionItemNum())){
                Integer count = transactionItemNumDupeCheck.get(productLineItemModel.getTransactionItemNum());
                transactionItemNumDupeCheck.put(productLineItemModel.getTransactionItemNum(), count + 1);
            } else {
                transactionItemNumDupeCheck.put(productLineItemModel.getTransactionItemNum(), 1);
            }

            for (ModifierLineItemModel modifierLineItemModel : productLineItemModel.getModifiers()){
                if (transactionItemNumDupeCheck.containsKey(modifierLineItemModel.getTransactionItemNum())){
                    Integer count = transactionItemNumDupeCheck.get(modifierLineItemModel.getTransactionItemNum());
                    transactionItemNumDupeCheck.put(modifierLineItemModel.getTransactionItemNum(), count + 1);
                } else {
                    transactionItemNumDupeCheck.put(modifierLineItemModel.getTransactionItemNum(), 1);
                }
            }
        }

        for (DiscountLineItemModel discountLineItemModel : this.getTransactionModel().getDiscounts()){
            if (discountLineItemModel.getDiscount().isSubTotalDiscount()){
                if (transactionItemNumDupeCheck.containsKey(discountLineItemModel.getTransactionItemNum())){
                    Integer count = transactionItemNumDupeCheck.get(discountLineItemModel.getTransactionItemNum());
                    transactionItemNumDupeCheck.put(discountLineItemModel.getTransactionItemNum(), count + 1);
                } else {
                    transactionItemNumDupeCheck.put(discountLineItemModel.getTransactionItemNum(), 1);
                }
            }
        }

        for (LoyaltyRewardLineItemModel loyaltyRewardLineItemModel : this.getTransactionModel().getRewards()){
            if (transactionItemNumDupeCheck.containsKey(loyaltyRewardLineItemModel.getTransactionItemNum())){
                Integer count = transactionItemNumDupeCheck.get(loyaltyRewardLineItemModel.getTransactionItemNum());
                transactionItemNumDupeCheck.put(loyaltyRewardLineItemModel.getTransactionItemNum(), count + 1);
            } else {
                transactionItemNumDupeCheck.put(loyaltyRewardLineItemModel.getTransactionItemNum(), 1);
            }
        }

        for (TenderLineItemModel tenderLineItemModel : this.getTransactionModel().getTenders()){
            if (transactionItemNumDupeCheck.containsKey(tenderLineItemModel.getTransactionItemNum())){
                Integer count = transactionItemNumDupeCheck.get(tenderLineItemModel.getTransactionItemNum());
                transactionItemNumDupeCheck.put(tenderLineItemModel.getTransactionItemNum(), count + 1);
            } else {
                transactionItemNumDupeCheck.put(tenderLineItemModel.getTransactionItemNum(), 1);
            }
        }

        for (DonationLineItemModel donationLineItemModel : this.getTransactionModel().getDonations()){
            if (transactionItemNumDupeCheck.containsKey(donationLineItemModel.getTransactionItemNum())){
                Integer count = transactionItemNumDupeCheck.get(donationLineItemModel.getTransactionItemNum());
                transactionItemNumDupeCheck.put(donationLineItemModel.getTransactionItemNum(), count + 1);
            } else {
                transactionItemNumDupeCheck.put(donationLineItemModel.getTransactionItemNum(), 1);
            }
        }

        for (SurchargeLineItemModel surchargeLineItemModel : this.getTransactionModel().getSurcharges()){
            if (transactionItemNumDupeCheck.containsKey(surchargeLineItemModel.getTransactionItemNum())){
                Integer count = transactionItemNumDupeCheck.get(surchargeLineItemModel.getTransactionItemNum());
                transactionItemNumDupeCheck.put(surchargeLineItemModel.getTransactionItemNum(), count + 1);
            } else {
                transactionItemNumDupeCheck.put(surchargeLineItemModel.getTransactionItemNum(), 1);
            }
        }

        for (Map.Entry<Integer, Integer> entry : transactionItemNumDupeCheck.entrySet()){
            if (entry.getKey() != null){
                if (entry.getValue() > 1){
                    throw new MissingDataException("Duplicate Transaction Item Number submitted.");
                }
            }
        }

        return transactionItemNumDupeCheck;
    }

    private void validateNonSequentialTransactionItemNums() throws Exception {

        Collections.sort(getNumberList());
        Integer previousInt = -1;
        Integer firstValue = null;

        if (getNumberList().isEmpty()){
            return;
        }

        firstValue = getNumberList().get(0);

        if (firstValue.compareTo(1) != 0){
            throw new MissingDataException("Invalid first transactionItemNum submitted", this.getTransactionModel().getTerminal().getId());
        }

        for (int i = 0; i < getNumberList().size(); i++){
            if (previousInt.compareTo(-1) == 0){
                previousInt = getNumberList().get(i);
                continue;
            }

            Integer currInt = getNumberList().get(i);

            if (currInt.compareTo(previousInt + 1) != 0){
                throw new MissingDataException("Non-sequential transactionItemNums submitted", this.getTransactionModel().getTerminal().getId());
            }

            previousInt = currInt;
        }
    }

    public Integer getMaxTransactionItemNumber() {
        /*The following object types should be assigned a TransactionItemNum value:
            Products (including modifiers)
            Subtotal discounts
            Rewards
            Tenders
            Donations
            Surcharges*/

        Integer maxTransactionItemNum = 0;

        for (ProductLineItemModel productLineItemModel : this.getTransactionModel().getProducts()){
            if (productLineItemModel.getTransactionItemNum() != null && productLineItemModel.getTransactionItemNum().compareTo(maxTransactionItemNum) > 0){
                maxTransactionItemNum = productLineItemModel.getTransactionItemNum();
            }

            for (ModifierLineItemModel modifierLineItemModel : productLineItemModel.getModifiers()){
                if (modifierLineItemModel.getTransactionItemNum() != null && modifierLineItemModel.getTransactionItemNum().compareTo(maxTransactionItemNum) > 0){
                    maxTransactionItemNum = modifierLineItemModel.getTransactionItemNum();
                }
            }
        }

        for (DiscountLineItemModel discountLineItemModel : this.getTransactionModel().getDiscounts()){
            if (discountLineItemModel.getDiscount().isSubTotalDiscount()){
                if (discountLineItemModel.getTransactionItemNum() != null && discountLineItemModel.getTransactionItemNum().compareTo(maxTransactionItemNum) > 0){
                    maxTransactionItemNum = discountLineItemModel.getTransactionItemNum();
                }
            }
        }

        for (LoyaltyRewardLineItemModel loyaltyRewardLineItemModel : this.getTransactionModel().getRewards()){
            if (loyaltyRewardLineItemModel.getTransactionItemNum() != null && loyaltyRewardLineItemModel.getTransactionItemNum().compareTo(maxTransactionItemNum) > 0){
                maxTransactionItemNum = loyaltyRewardLineItemModel.getTransactionItemNum();
            }
        }

        for (TenderLineItemModel tenderLineItemModel : this.getTransactionModel().getTenders()){
            if (tenderLineItemModel.getTransactionItemNum() != null && tenderLineItemModel.getTransactionItemNum().compareTo(maxTransactionItemNum) > 0){
                maxTransactionItemNum = tenderLineItemModel.getTransactionItemNum();
            }
        }

        for (DonationLineItemModel donationLineItemModel : this.getTransactionModel().getDonations()){
            if (donationLineItemModel.getTransactionItemNum() != null && donationLineItemModel.getTransactionItemNum().compareTo(maxTransactionItemNum) > 0){
                maxTransactionItemNum = donationLineItemModel.getTransactionItemNum();
            }
        }

        for (SurchargeLineItemModel surchargeLineItemModel : this.getTransactionModel().getSurcharges()){
            if (surchargeLineItemModel.getTransactionItemNum() != null && surchargeLineItemModel.getTransactionItemNum().compareTo(maxTransactionItemNum)  > 0){
                maxTransactionItemNum = surchargeLineItemModel.getTransactionItemNum();
            }
        }

        return maxTransactionItemNum;
    }

    public TransactionModel getTransactionModel() {
        return transactionModel;
    }

    public void setTransactionModel(TransactionModel transactionModel) {
        this.transactionModel = transactionModel;
    }

    public ArrayList<Integer> getNumberList() {
        return numberList;
    }

    public void setNumberList(ArrayList<Integer> numberList) {
        this.numberList = numberList;
    }

    public String getLogFileName() {
        return logFileName;
    }

    public void setLogFileName(String logFileName) {
        this.logFileName = logFileName;
    }
}
