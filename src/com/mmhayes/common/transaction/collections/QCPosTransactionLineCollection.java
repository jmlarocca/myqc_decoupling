package com.mmhayes.common.transaction.collections;

//MMHayes Dependencies

import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.transaction.models.TransactionModel;
import com.mmhayes.common.dataaccess.DataManager;

//Other Dependencies
import java.util.ArrayList;
import java.util.HashMap;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2018-08-09 08:05:30 -0400 (Thu, 09 Aug 2018) $: Date of last commit
 $Rev: 7577 $: Revision of last commit
*/
public class QCPosTransactionLineCollection extends TransactionLineCollection {

    public QCPosTransactionLineCollection() {

    }

    public static ArrayList<HashMap> getQCPosTransactionLineHM(TransactionModel transactionModel, Integer terminalId) throws Exception {
        DataManager dm = new DataManager();

        ArrayList<HashMap> transactionLineListHM = null;
        transactionModel.checkForOfflineMode(); //check app properties to see if the pos is offline

        if (transactionModel.isOfflineMode()) {
            //get all models in an array list
            transactionLineListHM = dm.parameterizedExecuteQuery("data.posapi30.getQCPosTransactionLineItemsOffline",
                    new Object[]{
                            transactionModel.getId()
                    },
                    PosAPIHelper.getLogFileName(terminalId),
                    true
            );
        } else {
            //get all models in an array list
            transactionLineListHM = dm.parameterizedExecuteQuery("data.posapi30.getQCPosTransactionLineItems",
                    new Object[]{
                            transactionModel.getId()
                    },
                    PosAPIHelper.getLogFileName(terminalId),
                    true
            );
        }

        return transactionLineListHM;
    }

}



