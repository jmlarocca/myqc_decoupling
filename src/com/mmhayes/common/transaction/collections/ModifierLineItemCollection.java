package com.mmhayes.common.transaction.collections;

//mmhayes dependencies
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.dataaccess.DataManager;

//API dependencies
import com.mmhayes.common.transaction.models.*;

//other dependencies
import java.util.ArrayList;
import java.util.HashMap;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2017-04-05 11:09:14 -0400 (Wed, 05 Apr 2017) $: Date of last commit
 $Rev: 3793 $: Revision of last commit
*/
public class ModifierLineItemCollection {
    private ArrayList<ModifierLineItemModel> collection = new ArrayList<>();

    public ModifierLineItemCollection(){

    }

    public static ModifierLineItemCollection getAllModifierLineItems(ProductLineItemModel productLineItemModel, Integer terminalId) throws Exception {
        DataManager dm = new DataManager();
        ModifierLineItemCollection modifierLineItemCollection = new ModifierLineItemCollection();

        //get all models in an array list
        ArrayList<HashMap> taxLineItemListHM = dm.parameterizedExecuteQuery("data.posapi30.getModifiersForTransLineItemId",
                new Object[]{ productLineItemModel.getId() },
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        //modifier list is null or empty, throw exception
        //CommonAPI.checkIsNullOrEmptyList(modifierLineItemListHM, "Modifier could not be found");

        for (HashMap modelHM : taxLineItemListHM) {
            //CommonAPI.checkIsNullOrEmptyObject(modelHM.get("ID"), "LoyaltyProgramId Id could not be found");
            modifierLineItemCollection.getCollection().add(ModifierLineItemModel.createModifierLineItemModel(modelHM));  //create a new model and add to the collection
        }

        return modifierLineItemCollection;
    }

    public ArrayList<ModifierLineItemModel> getCollection() {
        return collection;
    }

    public void setCollection(ArrayList<ModifierLineItemModel> collection) {
        this.collection = collection;
    }
}
