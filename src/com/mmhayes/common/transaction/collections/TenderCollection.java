package com.mmhayes.common.transaction.collections;

//mmhayes dependencies
import com.mmhayes.common.api.ModelPaginator;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.dataaccess.DataManager;

//API dependencies
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.terminal.models.TerminalModel;
import com.mmhayes.common.transaction.models.TenderModel;

//other dependencies
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2021-07-22 13:41:30 -0400 (Thu, 22 Jul 2021) $: Date of last commit
 $Rev: 14466 $: Revision of last commit
*/
public class TenderCollection {

    List<TenderModel> collection = new ArrayList<TenderModel>();
    private static DataManager dm = new DataManager();

    //constructor
    public TenderCollection(){

    }

    public TenderCollection(HashMap<String, LinkedList> paginationParamsHm, Integer terminalId) {
        populateCollection(new ModelPaginator(paginationParamsHm), terminalId);
    }

    public static TenderCollection getAllTenders(Integer terminalId) throws Exception {
        TenderCollection tenderCollection = new TenderCollection();

        //get all models in an array list
        ArrayList<HashMap> tenderList = dm.parameterizedExecuteQuery("data.posapi30.getAllTenders",
                new Object[]{ }, PosAPIHelper.getLogFileName(terminalId),
                true
        );


        //product list is null or empty, throw exception
        CommonAPI.checkIsNullOrEmptyList(tenderList, "Tender List is empty", terminalId);
        for (HashMap tenderHM : tenderList) {
            CommonAPI.checkIsNullOrEmptyObject(tenderHM.get("ID"), "Tender Id could not be found", terminalId);
            //create a new model and add to the collection
            tenderCollection.getCollection().add(TenderModel.createTenderModel(tenderHM));
        }

        return tenderCollection;
    }

    private List<TenderModel> populateCollection(ModelPaginator modelPaginator, Integer terminalId) {

        modelPaginator.finalizePaginationProperties();

        // always sort by the tender name, at least for now
        String sortByColumn = "";
        if (modelPaginator.getSort().toUpperCase().equals("NAME")) {
            sortByColumn = "POSAT.Name";
        }
        // default to sorting by the tender's name
        else {
            sortByColumn = "POSAT.Name";
        }

        ArrayList<HashMap> tenderList = dm.serializeSqlWithColNames("data.posapi30.getPosAnywhereTendersPaginate",
                new Object[]{
                        modelPaginator.getStartingModelIndex().toString(),
                        modelPaginator.getEndingModelIndex().toString(),
                        sortByColumn,
                        modelPaginator.getOrder().toUpperCase(),
                        terminalId
                },
                null,
                PosAPIHelper.getLogFileName(terminalId),
                false
        );

        //populate this collection from an array list of hashmaps
        if (tenderList != null && tenderList.size() > 0) {
            for (HashMap tenderHM : tenderList) {

                TenderModel tenderModel = TenderModel.createTenderModel(tenderHM);
                getCollection().add(tenderModel);
            }
        }
        return this.getCollection();
    }

    public static List<TenderModel> getAllTendersByTerminal(TerminalModel terminalModel) throws Exception {
        ArrayList<HashMap> tenderListHM = dm.parameterizedExecuteQuery("data.posapi30.getAllTendersByRevCenter",
                new Object[]{terminalModel.getRevenueCenterId()}, PosAPIHelper.getLogFileName(terminalModel.getId()),
                true
        );

        return mapTenderList(tenderListHM, terminalModel);
    }

    public static List<TenderModel> getAllTendersByTerminalAndAccount(TerminalModel terminalModel, Integer employeeId) throws Exception {
        ArrayList<HashMap> tenderListHM = dm.parameterizedExecuteQuery("data.posapi30.getAllTendersByRevCenterAndAccountId",
                new Object[]{terminalModel.getRevenueCenterId(), employeeId}, PosAPIHelper.getLogFileName(terminalModel.getId()),
                true
        );

        return mapTenderList(tenderListHM, terminalModel);
    }

    public static List<TenderModel> getAllTendersByTerminal(TerminalModel terminalModel, String tenderType) throws Exception {
        ArrayList<HashMap> tenderListHM = dm.parameterizedExecuteQuery("data.posapi30.getAllTendersByRevCenterAndType",
                new Object[]{terminalModel.getRevenueCenterId()}, PosAPIHelper.getLogFileName(terminalModel.getId()),
                true
        );

        return mapTenderList(tenderListHM, terminalModel);
    }

    public static List<TenderModel> getAllTendersByTerminalAndAccount(TerminalModel terminalModel, Integer employeeId, String tenderType) throws Exception {
        ArrayList<HashMap> tenderListHM = dm.parameterizedExecuteQuery("data.posapi30.getAllTendersByRevCenterAndTypeAndAccountId",
                new Object[]{terminalModel.getRevenueCenterId(), employeeId, tenderType}, PosAPIHelper.getLogFileName(terminalModel.getId()),
                true
        );

        return mapTenderList(tenderListHM, terminalModel);
    }

    public static List<TenderModel> getAllTendersByAccountIdAndAccountGroup(Integer employeeId) throws Exception {
        ArrayList<HashMap> tenderListHM = dm.parameterizedExecuteQuery("data.posapi30.getAllTendersByAccountIdAndAccountGroup",
                new Object[]{employeeId},
                true
        );

        return mapTenderList(tenderListHM, null);
    }

    private static List<TenderModel> mapTenderList(ArrayList<HashMap> tenderListHM, TerminalModel terminalModel) throws Exception {
        List<TenderModel> validTenders = new ArrayList<>();
        if (terminalModel == null){
            terminalModel = new TerminalModel();
        }
        //tender list is null or empty, throw exception
        //CommonAPI.checkIsNullOrEmptyList(tenderListHM, "Tender List is empty", terminalModel.getId());
        for (HashMap tenderHM : tenderListHM) {
            CommonAPI.checkIsNullOrEmptyObject(tenderHM.get("ID"), "Tender Id could not be found", terminalModel.getId());
            //create a new model and add to the collection
            validTenders.add(TenderModel.createTenderModel(tenderHM));
        }

        return validTenders;
    }

    //getter
    public List<TenderModel> getCollection() {
        return collection;
    }

    //setter
    public void setCollection(List<TenderModel> collection) {
        this.collection = collection;
    }
}