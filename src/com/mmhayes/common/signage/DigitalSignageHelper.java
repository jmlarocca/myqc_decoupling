package com.mmhayes.common.signage;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.CommonAuthResource;
import com.mmhayes.common.api.Validation;
import com.mmhayes.common.api.errorhandling.exceptions.InvalidAuthException;
import com.mmhayes.common.api.errorhandling.exceptions.SessionTimeoutException;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.signage.models.DisplayModel;
import com.mmhayes.common.signage.models.SignDetailModel;
import com.mmhayes.common.signage.models.SignModel;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.MMHServletStarter;

import javax.servlet.http.HttpServletRequest;
import java.net.UnknownHostException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;

/*
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2018-08-24 11:24:09 -0400 (Fri, 24 Aug 2018) $: Date of last commit
 $Rev: 32607 $: Revision of last commit

 Notes: Digital Signage resource for the REST API
*/

public class DigitalSignageHelper {
    @JsonIgnore private static DataManager dm = new DataManager();

    private static String hostName = "";
    private static DisplayModel displayModel = new DisplayModel();
    private static ArrayList<String> imagesArray = new ArrayList<>();
    private static String globalWellnessIcon = "";
    private static String globalVegetarianIcon = "";
    private static String globalVeganIcon = "";
    private static String globalGlutenFreeIcon = "";
    private static LocalDateTime currentTVDTM = null;

    //determines if there is a display connected to the token, if valid then creates a DSKey
    public static HashMap testDisplayConnection(String token) {
        HashMap dsKeyHM = new HashMap();

        try {
            //throw InvalidAuthException if token is found
            if (token == null || token.equals("")) {
                throw new InvalidAuthException();
            }

            //get all models in an array list
            ArrayList<HashMap> display = dm.parameterizedExecuteQuery("data.signage.getDisplayConnection",
                    new Object[]{
                            token
                    },
                    true
            );

            if(display != null && display.size() > 0) {
                HashMap displayHM = display.get(0);
                if(displayHM.get("DSDISPLAYID") != null && !displayHM.get("DSDISPLAYID").equals("")) {

                    Integer displayID = CommonAPI.convertModelDetailToInteger(displayHM.get("DSDISPLAYID"));

                    Logger.logMessage("In testDisplayConnection(), found displayID: " + displayID, Logger.LEVEL.DEBUG);

                    DisplayModel displayModel = new DisplayModel();
                    displayModel.setID(displayID);

                    String dsKey = CommonAPI.createDSKey(displayModel);

                    Logger.logMessage("Sucessfully created DSKey in testDisplayConnection(), DSKey: " + dsKey, Logger.LEVEL.TRACE);

                    dsKeyHM.put("dsKey", dsKey);

                    clearTokenFromDisplay(displayID);
                }
            }
        } catch (Exception ex) {
            Logger.logException(ex);
        }

        return dsKeyHM;
    }

    public static Integer getDisplayIDFromAuthHeader(HttpServletRequest request) throws Exception{
        Integer displayID = null;
        try {

            Logger.logMessage("In getDisplayIDFromAuthHeader(), calling CommonAuthResource.determineAuthDetailsFromAuthHeader(request)", Logger.LEVEL.DEBUG);

            String mmhayesAuthHeader = CommonAuthResource.getMMHayesAuthHeader(request);

            //for testing locally
//            ArrayList<HashMap> arrayList = dm.parameterizedExecuteQuery("data.signage.getDSKey", new Object[]{}, true);
//            String mmhayesAuthHeader = arrayList.get(0).get("DEVICESESSIONKEY").toString();

            Validation requestValidation = new Validation();
            requestValidation.setDSKey(mmhayesAuthHeader);

            HashMap DSKeyHM = checkDSKeyStatusWithDetails(requestValidation);

            if ( DSKeyHM == null || ( DSKeyHM.containsKey("result") && DSKeyHM.get("result").toString().equals("invalid") ) ) {
                throw new InvalidAuthException();
            }

            if ( DSKeyHM.containsKey("result") && DSKeyHM.get("result").toString().equals("expired") ) {
                throw new SessionTimeoutException();
            }

            if ( DSKeyHM.containsKey("DSDISPLAYID") && DSKeyHM.get("DSDISPLAYID") != null && !DSKeyHM.get("DSDISPLAYID").toString().equals("")) {
                displayID = CommonAPI.convertModelDetailToInteger(DSKeyHM.get("DSDISPLAYID"));

                //if a display is found, set the hostname to be used for the webimages url
                DigitalSignageHelper.setHostName(request);
                DigitalSignageHelper.setGlobalHealthyIcons();

                Logger.logMessage("Sucessfully retrieved DisplayID from DSKey in getDisplayIDFromAuthHeader(), displayID: " + displayID, Logger.LEVEL.TRACE);
            }

            return displayID;

        } catch(Exception ex){
            Logger.logMessage("Error in method: getDisplayIDFromAuthHeader()", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }
        return displayID;
    }

    public static HashMap checkDSKeyStatusWithDetails(Validation requestValidation) {
        HashMap DSKeyDetailsHM = new HashMap();

        //pull dskey record
        HashMap DSKeyHM = retrieveDSKeyDetails(requestValidation);

        //test that a valid record was returned
        if ( DSKeyHM == null ) {
            DSKeyDetailsHM.put("result", "invalid");
            return DSKeyDetailsHM;
        }

        //check dskey expiration
        String expiresOn = CommonAPI.convertModelDetailToString(DSKeyHM.get("EXPIRESON"));

        if ( checkDSKeyValidOrExpired(expiresOn).equals("expired") ) {
            DSKeyDetailsHM.put("result", "expired");
            return DSKeyDetailsHM;
        }

        return DSKeyHM;
    }

    public static HashMap retrieveDSKeyDetails(Validation requestValidation) {
        ArrayList<HashMap> DSKeyList = dm.parameterizedExecuteQuery("data.validation.checkSignageDSKeyStatus",
                new Object[]{
                        requestValidation.getDSKey()
                },
                true
        );

        if ( DSKeyList == null || DSKeyList.size() == 0 ) {
            return null;
        }

        return DSKeyList.get(0);
    }

    //takes the DSKey table field ExpiresOn as a String and returns whether it is valid or expired
    public static String checkDSKeyValidOrExpired(String expiresOn) {
        String date = expiresOn.substring(0, 10);
        String time = expiresOn.substring(11, expiresOn.length());
        String utcTime = date.concat("T").concat(time).concat("Z");

        Instant expiration = Instant.parse(utcTime);

        //if not expired yet
        if ( expiration.isAfter(Instant.now())) {
            return "valid";
        }

        return "expired";
    }

    //removes the token set on the display
    public static void clearTokenFromDisplay(Integer displayID) {
        try {

            Logger.logMessage("In clearTokenFromDisplay(), about to clear token from QC_DSDisplay for displayID: " + displayID, Logger.LEVEL.DEBUG);

            Integer updateResult = dm.parameterizedExecuteNonQuery("data.signage.removeTokenFromDisplay",
                new Object[]{
                        displayID
                }
            );

            if (updateResult != 1) {
                Logger.logMessage("Could not remove token from displayID: " + displayID, Logger.LEVEL.ERROR);
            }

            Logger.logMessage("In clearTokenFromDisplay(), successfully cleared token for displayID: " + displayID, Logger.LEVEL.DEBUG);

        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
    }

    public static void setHostName(HttpServletRequest request){
        String hostname="";

        try {
            // Get Hostname from Common Method
            hostname = CommonAPI.getHostName(request);

            //for displaying images with dev environment
//            if( MMHServletStarter.isDevEnv()) {
//                hostname = "http://" + hostname;
//            }

            // remove internal from mmhcloud shenanigans
            if(hostname.contains("internal.mmhcloud.com")){
                hostname = hostname.replace("internal.mmhcloud.com", "mmhcloud.com");
            }

            Logger.logMessage("Computed HostName: " + hostname, Logger.LEVEL.DEBUG);

            hostName = hostname;

        } catch(Exception ex){
            Logger.logMessage("Error in method: getHostName()", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }
    }

    public static void setGlobalHealthyIcons() {

        //get all models in an array list
        ArrayList<HashMap> globalHealthyList = dm.parameterizedExecuteQuery("data.signage.getGlobalHealthyIcons",
                new Object[]{},
                true
        );

        if (globalHealthyList != null && globalHealthyList.size() > 0) {
            HashMap globalHealthyHM = globalHealthyList.get(0);

            String wellnessTokenized = CommonAPI.convertModelDetailToString(globalHealthyHM.get("WELLNESSTOKENIZEDFILENAME"));
            String vegetarianTokenized = CommonAPI.convertModelDetailToString(globalHealthyHM.get("VEGETARIANTOKENIZEDFILENAME"));
            String veganTokenized = CommonAPI.convertModelDetailToString(globalHealthyHM.get("VEGANTOKENIZEDFILENAME"));
            String glutenFreeTokenized = CommonAPI.convertModelDetailToString(globalHealthyHM.get("GLUTENFREETOKENIZEDFILENAME"));

            setGlobalWellnessIcon(wellnessTokenized);
            setGlobalVegetarianIcon(vegetarianTokenized);
            setGlobalVeganIcon(veganTokenized);
            setGlobalGlutenFreeIcon(glutenFreeTokenized);
        }
    }

    public static String findSurroundingTag(String html, String contextName) {
        String textBetween = "";

        Integer index = html.indexOf(contextName);
        Integer backEndTagIndex = index;

        if(index != -1) {

            char tagEnd = '\0';

            while( tagEnd != '>' && backEndTagIndex - 1 != 0) {

                backEndTagIndex = backEndTagIndex - 1;
                tagEnd = html.charAt(backEndTagIndex);
            }
        }

        Integer backStartTagIndex = backEndTagIndex;

        if(backEndTagIndex != -1) {

            char tagStart = '\0';

            while(tagStart != '<' && backStartTagIndex - 1 != 0) {

                backStartTagIndex = backStartTagIndex - 1;
                tagStart = html.charAt(backStartTagIndex);
            }
        }

        if(backEndTagIndex != -1 && backStartTagIndex != -1) {
            textBetween = html.substring(backStartTagIndex, backEndTagIndex);
        }
        return textBetween;
    }

    public static String addTitleClasses(String html, String contextName, HashMap colorHM) {

        String newTextBetween = "";
        String textBetween = findSurroundingTag(html, contextName);

        if(!textBetween.contains("style=")) {

            newTextBetween = textBetween + " style='font-size:"+colorHM.get("font-size")+"; color:"+colorHM.get("color")+"; background-color:"+colorHM.get("background-color")+";" +
                    " font-family:"+colorHM.get("font-family")+"; font-weight:"+colorHM.get("font-weight")+";'>" + contextName;

            textBetween += '>' + contextName;

            html = html.replace(textBetween, newTextBetween);

            //if it already has a class set
        } else if( !textBetween.equals("")) {

            char styleEnd = '\0';
            Integer styleEndIndex = textBetween.indexOf("style=") + 7;

            while(styleEnd != '"' && styleEnd != '\'' && styleEndIndex + 1 != textBetween.length()) {

                styleEndIndex = styleEndIndex + 1;
                styleEnd = textBetween.charAt(styleEndIndex);

            }

            String textStyleBetween = textBetween.substring(textBetween.indexOf("style="), styleEndIndex);

            String newStyleTextBetween = textStyleBetween;

            if(!newStyleTextBetween.contains("font-size")) {
                newStyleTextBetween +=  "; font-size:" + colorHM.get("font-size");
            }

            if(!newStyleTextBetween.contains(" color") && !newStyleTextBetween.contains(";color")) {
                newStyleTextBetween +=  "; color:" + colorHM.get("color");
            }

            if(!newStyleTextBetween.contains("background-color") && !textStyleBetween.contains("background:")) {
                newStyleTextBetween +=  "; background-color:" + colorHM.get("background-color");
            }

            if(!newStyleTextBetween.contains("font-family")) {
                newStyleTextBetween +=  "; font-family:" + colorHM.get("font-family");
            }

            if(!newStyleTextBetween.contains("font-weight")) {
                newStyleTextBetween += "; font-weight:" + colorHM.get("font-weight");
            }

            String lastChar = newStyleTextBetween.substring(newStyleTextBetween.length() -1);
            if(!lastChar.equals(";")) {
                newStyleTextBetween += ";";
            }

            newTextBetween = textBetween.replace(textStyleBetween, newStyleTextBetween);

            html = html.replace(textBetween, newTextBetween);
        }

        return html;
    }

    public static String addKeypadColClasses(String html, SignDetailModel signDetailModel) {

        String newTextBetween = "";

        Integer numKeypadCols = signDetailModel.getNumKeypadCols();
        String contextName = signDetailModel.getContextName();
        String textBetween = findSurroundingTag(html, contextName);
        boolean isSurroundingDiv = false;

        //replace div with ul
        if (textBetween.contains("div")) {
            isSurroundingDiv = true;

            //replace the div with the ul element
            String ulTextReplacement = textBetween;
            ulTextReplacement = ulTextReplacement.replace("div", "ul");

            textBetween += ">[[" + contextName + "]]";
            ulTextReplacement += ">[[" + contextName + "]]";
            //replace the html with the new string using the ul element
            html = html.replace(textBetween, ulTextReplacement);
            textBetween = findSurroundingTag(html, contextName);
        }

        if(textBetween.contains("ul")) {

            if(!textBetween.contains("style=")) {

                newTextBetween = textBetween + " style='column-count:"+numKeypadCols+"; -webkit-column-count:"+numKeypadCols+"; column-gap:0; -webkit-column-gap:0'";

                textBetween += ">[[" +contextName+ "]]";
                newTextBetween += ">[[" +contextName+ "]]";

                html = html.replace(textBetween, newTextBetween);

            } else {

                char styleEnd = '\0';
                Integer styleEndIndex = textBetween.indexOf("style=") + 7;

                while(styleEnd != '"' && styleEnd != '\'' && styleEndIndex + 1 != textBetween.length()) {

                    styleEndIndex = styleEndIndex + 1;
                    styleEnd = textBetween.charAt(styleEndIndex);

                }

                String textStyleBetween = textBetween.substring(textBetween.indexOf("style="), styleEndIndex);

                String newStyleTextBetween = textStyleBetween + " column-count:"+numKeypadCols+"; -webkit-column-count:"+numKeypadCols+"; column-gap:0; -webkit-column-gap:0;";

                newTextBetween = textBetween.replace(textStyleBetween, newStyleTextBetween);

                textBetween += ">[[" +contextName+ "]]";
                newTextBetween += ">[[" +contextName+ "]]";

                html = html.replace(textBetween, newTextBetween);
            }
        }

        if(isSurroundingDiv) {
            if(html.contains("[[" + contextName + "]]</div>")) {
                html = html.replace("[[" + contextName + "]]</div>", "[[" + contextName + "]]</ul>");

            } else if (html.contains("[[" + contextName + "]] </div>")) {
                html = html.replace("[[" + contextName + "]] </div>", "[[" + contextName + "]]</ul>");
            }
        }

        return html;

    }

    public static HashMap determineSignColors(SignModel signModel, DisplayModel displayModel) {
        HashMap colorHM = new HashMap();

        colorHM.put("color", "#" + signModel.getTitleFontColor());
        if(signModel.getTitleFontColor().equals("")) {
            colorHM.put("color", "transparent");
        }

        colorHM.put("background-color", "#" + signModel.getTitleBackgroundColor());
        if(signModel.getTitleBackgroundColor().equals("")) {
            colorHM.put("background-color", "transparent");
        }

        colorHM.put("font-size", signModel.getTitleFontSize() + "px");
        colorHM.put("font-family", signModel.getTitleFontFamily());

        if(signModel.isTitleFontIsBold() || (!signModel.isTitleFontIsBold() && displayModel.isTitleFontIsBold())) {
            colorHM.put("font-weight", "bold");
        } else if (!signModel.isTitleFontIsBold() && !displayModel.isTitleFontIsBold()){
            colorHM.put("font-weight", "normal");
        }

        if(signModel.getTitleFontColor().equals("")) {
            colorHM.put("color", "#" + displayModel.getTitleFontColor());
            if(displayModel.getTitleFontColor().equals("")) {
                colorHM.put("color", "transparent");
            }
        }

        if(signModel.getTitleBackgroundColor().equals("")) {
            colorHM.put("background-color", "#" + displayModel.getTitleBackgroundColor());
            if(displayModel.getTitleBackgroundColor().equals("")) {
                colorHM.put("background-color", "transparent");
            }
        }

        if(signModel.getTitleFontSize().equals("")) {
            colorHM.put("font-size", displayModel.getTitleFontSize() + "px");
        }

        if(signModel.getTitleFontFamily().equals("")) {
            colorHM.put("font-family", displayModel.getTitleFontFamily());
        }

        return colorHM;
    }

    public static String updateSignTemplate(SignDetailModel signDetailModel, String html, HashMap colorHM) {

        String contextName = "[[" + signDetailModel.getContextName() + "]]";
        Integer contextType = signDetailModel.getContextTypeID();

        if(html.contains(contextName)) {

            if(contextType == 1) {  //Title
                html = DigitalSignageHelper.addTitleClasses(html, contextName, colorHM);

                html = html.replace(contextName, signDetailModel.getTitle());
            }

            if(contextType == 2) {  //Keypad

                //add column-count style rule
                html = DigitalSignageHelper.addKeypadColClasses(html, signDetailModel);

                //html for all the keypad items
                String keypadItemHtml = signDetailModel.getHtml();

                html = html.replace(contextName, keypadItemHtml);

            }

            if(contextType == 3) {  //Product

                //html for all the keypad items
                String keypadItemHtml = signDetailModel.getHtml();

                html = html.replace(contextName, keypadItemHtml);
            }

            if(contextType == 4) {  //Image
                // Get Hostname
                String hostName = DigitalSignageHelper.getHostName();

                //for testing
                if(hostName.contains("GEMDEV")) {
                    hostName = "http://10.1.246.84";
                }

                // Build image path
                String imagePath = hostName+"/webimages/"+signDetailModel.getImage();

                DigitalSignageHelper.addToImagesArray(imagePath);

                // Build Image tag
                String imageTag = "<img src=\""+imagePath+"\" style=\"display:block;\" />";

                if(signDetailModel.getImage().equals("")) {
                    imageTag = "";
                }

                html = html.replace(contextName, imageTag);
            }

            if(contextType == 5) {  //Text

                html = html.replace(contextName, signDetailModel.getText());
            }
        }

        return html;
    }

    public static String getHostName() {
        return hostName;
    }

    public static void setDisplayModel(DisplayModel displayModel) {
        DigitalSignageHelper.displayModel = displayModel;
    }

    public static DisplayModel getDisplayModel() {
        return displayModel;
    }

    public static ArrayList<String> getImagesArray() {
        return imagesArray;
    }

    public static void addToImagesArray(String imagePath) {
        ArrayList<String> images = getImagesArray();
        images.add(imagePath);
        setImagesArray(images);
    }

    public static void setImagesArray(ArrayList<String> imagesArray) {
        DigitalSignageHelper.imagesArray = imagesArray;
    }

    public static LocalDateTime getCurrentTVDTM() {
        return currentTVDTM;
    }

    public static void setCurrentTVDTM(LocalDateTime currentTVDTM) {
        DigitalSignageHelper.currentTVDTM = currentTVDTM;
    }

    public static String getGlobalWellnessIcon() {
        return globalWellnessIcon;
    }

    public static void setGlobalWellnessIcon(String globalWellnessIcon) {
        DigitalSignageHelper.globalWellnessIcon = globalWellnessIcon;
    }

    public static String getGlobalVegetarianIcon() {
        return globalVegetarianIcon;
    }

    public static void setGlobalVegetarianIcon(String globalVegetarianIcon) {
        DigitalSignageHelper.globalVegetarianIcon = globalVegetarianIcon;
    }

    public static String getGlobalVeganIcon() {
        return globalVeganIcon;
    }

    public static void setGlobalVeganIcon(String globalVeganIcon) {
        DigitalSignageHelper.globalVeganIcon = globalVeganIcon;
    }

    public static String getGlobalGlutenFreeIcon() {
        return globalGlutenFreeIcon;
    }

    public static void setGlobalGlutenFreeIcon(String globalGlutenFreeIcon) {
        DigitalSignageHelper.globalGlutenFreeIcon = globalGlutenFreeIcon;
    }

}
