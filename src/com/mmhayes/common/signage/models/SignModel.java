package com.mmhayes.common.signage.models;

import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.signage.DigitalSignageHelper;
import com.mmhayes.common.signage.collections.SignDetailCollection;
import org.owasp.esapi.ESAPI;

import java.util.HashMap;

/* Sign Model
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2017-11-15 17:41:26 -0500 (Wed, 15 Nov 2017) $: Date of last commit
 $Rev: 5682 $: Revision of last commit

 Notes: Contains information for a single sign
*/

public class SignModel {
    private Integer id = null;
    private Integer signGroupID = null; //not sure if necessary
    private Integer sortOrder = null;
    private Integer templateID = null;
    private String html = "";
    private String name = "";
    private String backgroundImage = "";
    private String titleFontSize = "";
    private String titleFontColor = "";
    private String titleBackgroundColor = "";
    private String titleFontFamily = "";
    private String contentFontSize = "";
    private String contentFontColor = "";
    private String contentBackgroundColor = "";
    private String contentFontFamily = "";
    private boolean titleFontIsBold = false;
    private boolean contentFontIsBold = false;
    SignDetailCollection signDetailCollection = null;

    //constructor - takes hashmap that get sets to this models properties
    public SignModel(HashMap modelDetailHM) throws Exception {
        setModelProperties(modelDetailHM);
    }

    //setter for all of this model's properties
    public SignModel setModelProperties(HashMap modelDetailHM) throws Exception {

        setID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("DSSIGNID")));

        setName(CommonAPI.convertModelDetailToString(modelDetailHM.get("NAME")));

        setSignGroupID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("DSSIGNGROUPID")));

        setSortOrder(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("SORTORDER")));

        setTemplateID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("TEMPLATEID")));

        setHtml(CommonAPI.convertModelDetailToString(modelDetailHM.get("HTML")));

        setBackgroundImage(CommonAPI.convertModelDetailToString(modelDetailHM.get("BACKGROUNDIMAGETOKENIZEDFILENAME")));

        setTitleFontSize(CommonAPI.convertModelDetailToString(modelDetailHM.get("TITLEFONTSIZE")));

        setTitleFontColor(CommonAPI.convertModelDetailToString(modelDetailHM.get("TITLEFONTCOLOR")));

        setTitleBackgroundColor(CommonAPI.convertModelDetailToString(modelDetailHM.get("TITLEBACKGROUNDCOLOR")));

        setTitleFontFamily(CommonAPI.convertModelDetailToString(modelDetailHM.get("TITLEFONTFAMILYID")));

        setTitleFontIsBold(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("TITLEFONTBOLD")));

        setContentFontSize(CommonAPI.convertModelDetailToString(modelDetailHM.get("CONTENTFONTSIZE")));

        setContentFontColor(CommonAPI.convertModelDetailToString(modelDetailHM.get("CONTENTFONTCOLOR")));

        setContentBackgroundColor(CommonAPI.convertModelDetailToString(modelDetailHM.get("CONTENTBACKGROUNDCOLOR")));

        setContentFontFamily(CommonAPI.convertModelDetailToString(modelDetailHM.get("CONTENTFONTFAMILYID")));

        setContentFontIsBold(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("CONTENTFONTBOLD")));

        return this;
    }

    public Integer getID() {
        return id;
    }

    public void setID(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSignGroupID() {
        return signGroupID;
    }

    public void setSignGroupID(Integer signGroupID) {
        this.signGroupID = signGroupID;
    }

    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    public Integer getTemplateID() {
        return templateID;
    }

    public void setTemplateID(Integer templateID) {
        this.templateID = templateID;
    }

    public String getBackgroundImage() {
        return backgroundImage;
    }

    public void setBackgroundImage(String backgroundImage) {
        if(!backgroundImage.equals("")) {
            // Get Hostname
            String hostName = DigitalSignageHelper.getHostName();

            //for testing
            if(hostName.contains("GEMDEV")) {
                hostName = "http://10.1.246.84";
            }

            // Build image path
            backgroundImage = hostName+"/webimages/"+backgroundImage;
        }

        this.backgroundImage = backgroundImage;
    }

    public SignDetailCollection getSignDetailCollection() {
        return signDetailCollection;
    }

    public void setSignDetailCollection(SignDetailCollection signDetailCollection) {
        this.signDetailCollection = signDetailCollection;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public String getTitleFontSize() {
        return titleFontSize;
    }

    public void setTitleFontSize(String titleFontSize) {
        this.titleFontSize = titleFontSize;
    }

    public String getTitleFontColor() {
        return titleFontColor;
    }

    public void setTitleFontColor(String titleFontColor) {
        this.titleFontColor = titleFontColor;
    }

    public String getTitleBackgroundColor() {
        return titleBackgroundColor;
    }

    public void setTitleBackgroundColor(String titleBackgroundColor) {
        this.titleBackgroundColor = titleBackgroundColor;
    }

    public String getTitleFontFamily() {
        return titleFontFamily;
    }

    public void setTitleFontFamily(String titleFontFamily) {
        this.titleFontFamily = titleFontFamily;
    }

    public String getContentFontSize() {
        return contentFontSize;
    }

    public void setContentFontSize(String contentFontSize) {
        this.contentFontSize = contentFontSize;
    }

    public String getContentFontColor() {
        return contentFontColor;
    }

    public void setContentFontColor(String contentFontColor) {
        this.contentFontColor = contentFontColor;
    }

    public String getContentBackgroundColor() {
        return contentBackgroundColor;
    }

    public void setContentBackgroundColor(String contentBackgroundColor) {
        this.contentBackgroundColor = contentBackgroundColor;
    }

    public String getContentFontFamily() {
        return contentFontFamily;
    }

    public void setContentFontFamily(String contentFontFamily) {
        this.contentFontFamily = contentFontFamily;
    }

    public boolean isTitleFontIsBold() {
        return titleFontIsBold;
    }

    public void setTitleFontIsBold(boolean titleFontIsBold) {
        this.titleFontIsBold = titleFontIsBold;
    }

    public boolean isContentFontIsBold() {
        return contentFontIsBold;
    }

    public void setContentFontIsBold(boolean contentFontIsBold) {
        this.contentFontIsBold = contentFontIsBold;
    }

}

