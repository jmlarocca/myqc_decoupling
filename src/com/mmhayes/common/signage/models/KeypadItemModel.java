package com.mmhayes.common.signage.models;

import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.dataaccess.DataManager;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;

/* Keypad Item Model
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2017-11-15 17:41:26 -0500 (Wed, 15 Nov 2017) $: Date of last commit
 $Rev: 5682 $: Revision of last commit

 Notes: Contains information for a single keypad item detail
*/

public class KeypadItemModel {
    private Integer id = null;
    private Integer productID = null;
    private Integer comboID = null;
    private Integer modifierSetID = null;
    private BigDecimal price = null;
    private String name = "";
    private String description = "";
    private String nutrition1 = "";
    private String nutrition2 = "";
    private String nutrition3 = "";
    private String nutrition4 = "";
    private String nutrition5 = "";
    private String image = "";
    private boolean wellness = false;
    private boolean vegetarian = false;
    private boolean vegan = false;
    private boolean glutenFree = false;
    private boolean isModifier = false;

    //constructor - takes hashmap that get sets to this models properties
    public KeypadItemModel(HashMap modelDetailHM) throws Exception {
        setModelProperties(modelDetailHM);
    }

    //setter for all of this model's properties
    public KeypadItemModel setModelProperties(HashMap modelDetailHM) throws Exception {

        setID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAKEYPADDETAILID")));

        setName(CommonAPI.convertModelDetailToString(modelDetailHM.get("NAME")));

        setPrice(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("PRICE")));

        setDescription(CommonAPI.convertModelDetailToString(modelDetailHM.get("DESCRIPTION")));

        setNutrition1(CommonAPI.convertModelDetailToString(modelDetailHM.get("NUTRITION1")));

        setNutrition2(CommonAPI.convertModelDetailToString(modelDetailHM.get("NUTRITION2")));

        setNutrition3(CommonAPI.convertModelDetailToString(modelDetailHM.get("NUTRITION3")));

        setNutrition4(CommonAPI.convertModelDetailToString(modelDetailHM.get("NUTRITION4")));

        setNutrition5(CommonAPI.convertModelDetailToString(modelDetailHM.get("NUTRITION5")));

        setImage(CommonAPI.convertModelDetailToString(modelDetailHM.get("IMAGE")));

        setWellness(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("WELLNESS"),false));

        setVegetarian(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("VEGETARIAN"),false));

        setVegan(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("VEGAN"),false));

        setGlutenFree(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("GLUTENFREE"),false));

        setModifierSetID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("MODIFIERSETID")));

        setModifier(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("ISMODIFIER"),false));

        Integer buttonTypeID = CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PABUTTONTYPEID"));

        if(buttonTypeID.equals(2)) {
            setProductID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ITEMID")));

        } else if(buttonTypeID.equals(36)) {
            setComboID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ITEMID")));
        }

        return this;
    }

    public static String convertNutition(String nutrition) {
        if(!nutrition.isEmpty()) {
            BigDecimal nutritionBD = new BigDecimal(nutrition);
            BigDecimal val = nutritionBD.setScale(0, RoundingMode.HALF_UP);
            nutrition = val.toString();
        }
        return nutrition;
    }

    public Integer getID() {
        return id;
    }

    public void setID(Integer id) {
        this.id = id;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNutrition1() {
        return nutrition1;
    }

    public void setNutrition1(String nutrition1) {
        nutrition1 = convertNutition(nutrition1);
        this.nutrition1 = nutrition1;
    }

    public String getNutrition2() {
        return nutrition2;
    }

    public void setNutrition2(String nutrition2) {
        nutrition2 = convertNutition(nutrition2);
        this.nutrition2 = nutrition2;
    }

    public String getNutrition3() {
        return nutrition3;
    }

    public void setNutrition3(String nutrition3) {
        nutrition3 = convertNutition(nutrition3);
        this.nutrition3 = nutrition3;
    }

    public String getNutrition4() {
        return nutrition4;
    }

    public void setNutrition4(String nutrition4) {
        nutrition4 = convertNutition(nutrition4);
        this.nutrition4 = nutrition4;
    }

    public String getNutrition5() {
        return nutrition5;
    }

    public void setNutrition5(String nutrition5) {
        nutrition5 = convertNutition(nutrition5);
        this.nutrition5 = nutrition5;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isWellness() {
        return wellness;
    }

    public void setWellness(boolean wellness) {
        this.wellness = wellness;
    }

    public boolean isVegetarian() {
        return vegetarian;
    }

    public void setVegetarian(boolean vegetarian) {
        this.vegetarian = vegetarian;
    }

    public boolean isVegan() {
        return vegan;
    }

    public void setVegan(boolean vegan) {
        this.vegan = vegan;
    }

    public boolean isGlutenFree() {
        return glutenFree;
    }

    public void setGlutenFree(boolean glutenFree) {
        this.glutenFree = glutenFree;
    }

    public Integer getModifierSetID() {
        return modifierSetID;
    }

    public void setModifierSetID(Integer modifierSetID) {
        this.modifierSetID = modifierSetID;
    }

    public boolean isModifier() {
        return isModifier;
    }

    public void setModifier(boolean modifier) {
        isModifier = modifier;
    }

    public Integer getProductID() {
        return productID;
    }

    public void setProductID(Integer productID) {
        this.productID = productID;
    }

    public Integer getComboID() {
        return comboID;
    }

    public void setComboID(Integer comboID) {
        this.comboID = comboID;
    }
}

