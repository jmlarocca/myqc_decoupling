package com.mmhayes.common.signage.models;

import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.signage.DigitalSignageHelper;
import com.mmhayes.common.signage.collections.SignGroupCollection;
import com.mmhayes.common.utils.Logger;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/* Display Model
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2017-11-15 17:41:26 -0500 (Wed, 15 Nov 2017) $: Date of last commit
 $Rev: 5682 $: Revision of last commit

 Notes: Contains information for a single display
*/

public class DisplayModel {
    private Integer id = null;
    private Integer dsDisplayID = null;
    private Integer token = null;
    private Integer scheduleID = null;
    private String name = "";
    private String updateInterval = "";
    private String scheduleName = "";
    private String titleFontSize = "";
    private String titleFontColor = "";
    private String titleBackgroundColor = "";
    private String titleFontFamily = "";
    private String contentFontSize = "";
    private String contentFontColor = "";
    private String contentBackgroundColor = "";
    private String contentFontFamily = "";
    private boolean titleFontIsBold = false;
    private boolean contentFontIsBold = false;
    SignGroupCollection signGroupCollection = null;
    ArrayList<String> imagesArray = new ArrayList<>();
    private String currentTVTime = ""; //holds the String representation of the current time
    private LocalDateTime currentTVDTM = null; //holds the LocalDateTime representation of the current time
    Integer timezoneOffset = 0; //for when the terminal and server are in different time zones

    //constructor
    public DisplayModel()  {

    }

    //constructor - takes hashmap that get sets to this models properties
    public DisplayModel(HashMap modelDetailHM) throws Exception {
        setModelProperties(modelDetailHM);
    }

    //setter for all of this model's properties
    public DisplayModel setModelProperties(HashMap modelDetailHM) throws Exception {

        setID(1); //set to 1 for backbone model, we're only ever using one display so this id can always be 1

        setDsDisplayID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("DSDISPLAYID")));

        setToken(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("TOKEN")));

        setScheduleID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("SCHEDULEID")));

        setName(CommonAPI.convertModelDetailToString(modelDetailHM.get("DISPLAYNAME")));

        setScheduleName(CommonAPI.convertModelDetailToString(modelDetailHM.get("SCHEDULENAME")));

        setUpdateInterval(CommonAPI.convertModelDetailToString(modelDetailHM.get("UPDATEINTERVALMINUTES")));

        setTitleFontSize(CommonAPI.convertModelDetailToString(modelDetailHM.get("TITLEFONTSIZE")));

        setTitleFontColor(CommonAPI.convertModelDetailToString(modelDetailHM.get("TITLEFONTCOLOR")));

        setTitleBackgroundColor(CommonAPI.convertModelDetailToString(modelDetailHM.get("TITLEBACKGROUNDCOLOR")));

        setTitleFontFamily(CommonAPI.convertModelDetailToString(modelDetailHM.get("TITLEFONTFAMILYID")));

        setTitleFontIsBold(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("TITLEFONTBOLD")));

        setContentFontSize(CommonAPI.convertModelDetailToString(modelDetailHM.get("CONTENTFONTSIZE")));

        setContentFontColor(CommonAPI.convertModelDetailToString(modelDetailHM.get("CONTENTFONTCOLOR")));

        setContentBackgroundColor(CommonAPI.convertModelDetailToString(modelDetailHM.get("CONTENTBACKGROUNDCOLOR")));

        setContentFontFamily(CommonAPI.convertModelDetailToString(modelDetailHM.get("CONTENTFONTFAMILYID")));

        setContentFontIsBold(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("CONTENTFONTBOLD")));

        setTimezoneOffset( CommonAPI.convertModelDetailToInteger(modelDetailHM.get("TIMEDIFFERENCE"), 0, true));


        return this;
    }

    public Integer getID() {
        return id;
    }

    public void setID(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getToken() {
        return token;
    }

    public void setToken(Integer token) {
        this.token = token;
    }

    public Integer getScheduleID() {
        return scheduleID;
    }

    public void setScheduleID(Integer scheduleID) {
        this.scheduleID = scheduleID;
    }

    public String getScheduleName() {
        return scheduleName;
    }

    public void setScheduleName(String scheduleName) {
        this.scheduleName = scheduleName;
    }

    public String getTitleFontSize() {
        return titleFontSize;
    }

    public void setTitleFontSize(String titleFontSize) {
        this.titleFontSize = titleFontSize;
    }

    public String getTitleFontColor() {
        return titleFontColor;
    }

    public void setTitleFontColor(String titleFontColor) {
        this.titleFontColor = titleFontColor;
    }

    public String getTitleBackgroundColor() {
        return titleBackgroundColor;
    }

    public void setTitleBackgroundColor(String titleBackgroundColor) {
        this.titleBackgroundColor = titleBackgroundColor;
    }

    public String getTitleFontFamily() {
        return titleFontFamily;
    }

    public void setTitleFontFamily(String titleFontFamily) {
        this.titleFontFamily = titleFontFamily;
    }

    public String getContentFontSize() {
        return contentFontSize;
    }

    public void setContentFontSize(String contentFontSize) {
        this.contentFontSize = contentFontSize;
    }

    public String getContentFontColor() {
        return contentFontColor;
    }

    public void setContentFontColor(String contentFontColor) {
        this.contentFontColor = contentFontColor;
    }

    public String getContentBackgroundColor() {
        return contentBackgroundColor;
    }

    public void setContentBackgroundColor(String contentBackgroundColor) {
        this.contentBackgroundColor = contentBackgroundColor;
    }

    public String getContentFontFamily() {
        return contentFontFamily;
    }

    public void setContentFontFamily(String contentFontFamily) {
        this.contentFontFamily = contentFontFamily;
    }

    public boolean isTitleFontIsBold() {
        return titleFontIsBold;
    }

    public void setTitleFontIsBold(boolean titleFontIsBold) {
        this.titleFontIsBold = titleFontIsBold;
    }

    public boolean isContentFontIsBold() {
        return contentFontIsBold;
    }

    public void setContentFontIsBold(boolean contentFontIsBold) {
        this.contentFontIsBold = contentFontIsBold;
    }

    public SignGroupCollection getSignGroupCollection() {
        return signGroupCollection;
    }

    public void setSignGroupCollection(SignGroupCollection signGroupCollection) {
        this.signGroupCollection = signGroupCollection;
    }

    public String getUpdateInterval() {
        return updateInterval;
    }

    public void setUpdateInterval(String updateInterval) {
        this.updateInterval = updateInterval;
    }

    public Integer getDsDisplayID() {
        return dsDisplayID;
    }

    public void setDsDisplayID(Integer dsDisplayID) {
        this.dsDisplayID = dsDisplayID;
    }

    public ArrayList<String> getImagesArray() {
        return imagesArray;
    }

    public void setImagesArray(ArrayList<String> imagesArray) {
        this.imagesArray = imagesArray;
    }

    public String getCurrentTVTime() {
        return currentTVTime;
    }

    public void setCurrentTVTime(String currentTVTime) {
        this.currentTVTime = currentTVTime;
    }

    public Integer getTimezoneOffset() {
        return timezoneOffset;
    }

    public void setTimezoneOffset(Integer timezoneOffset) {
        this.timezoneOffset = timezoneOffset;
    }

    public LocalDateTime getCurrentTVDTM() {
        return currentTVDTM;
    }

    public void setCurrentTVDTM(LocalDateTime currentTVDTM) {
        this.currentTVDTM = currentTVDTM;
    }

    public void setCurrentTime() throws Exception {
        LocalDateTime currentTime = LocalDateTime.now();

        if ( getTimezoneOffset() != null && !getTimezoneOffset().equals("") ) {
            currentTime = currentTime.plusHours( getTimezoneOffset()) ;
        }

        this.setCurrentTVDTM( currentTime );

        this.setCurrentTVTime( currentTime.toLocalTime().toString() );

        //set the current time on the Digital Signage Helper for determine the rotation when we get the keypad details
        DigitalSignageHelper.setCurrentTVDTM( currentTime );
    }
}

