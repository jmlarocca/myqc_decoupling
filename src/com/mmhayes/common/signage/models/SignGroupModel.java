package com.mmhayes.common.signage.models;

import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.signage.collections.SignCollection;

import java.util.HashMap;

/* Sign Group Model
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2017-11-15 17:41:26 -0500 (Wed, 15 Nov 2017) $: Date of last commit
 $Rev: 5682 $: Revision of last commit

 Notes: Contains information for a single sign group
*/

public class SignGroupModel {
    private Integer id = null;
    private Integer transitionEffectID = null;
    private Integer rotationFrequency = null;
    private String name = "";
    private String startTime = "";
    private String endTime = "";
    private String referenceTime = "";
    private boolean onMon = false;
    private boolean onTue = false;
    private boolean onWed = false;
    private boolean onThu = false;
    private boolean onFri = false;
    private boolean onSat = false;
    private boolean onSun = false;
    SignCollection signCollection = null;

    //constructor - takes hashmap that get sets to this models properties
    public SignGroupModel(HashMap modelDetailHM) throws Exception {
        setModelProperties(modelDetailHM);
    }

    //setter for all of this model's properties
    public SignGroupModel setModelProperties(HashMap modelDetailHM) throws Exception {

        setID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("DSSIGNGROUPID")));

        setName(CommonAPI.convertModelDetailToString(modelDetailHM.get("SIGNGROUPNAME")));

        setTransitionEffectID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("TRANSITIONEFFECTID")));

        setStartTime(CommonAPI.convertModelDetailToString(modelDetailHM.get("STARTTIME")));

        setEndTime(CommonAPI.convertModelDetailToString(modelDetailHM.get("ENDTIME")));

        if(modelDetailHM.containsKey("REFERENCETIME") && !modelDetailHM.get("REFERENCETIME").toString().equals("")) {
            setReferenceTime(CommonAPI.convertModelDetailToString(modelDetailHM.get("REFERENCETIME")));
        }

        setRotationFrequency(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ROTATIONFREQUENCY")));

        setOnMon(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("ONMON")));

        setOnTue(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("ONTUE")));

        setOnWed(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("ONWED")));

        setOnThu(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("ONTHU")));

        setOnFri(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("ONFRI")));

        setOnSat(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("ONSAT")));

        setOnSun(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("ONSUN")));

        return this;
    }

    public Integer getID() {
        return id;
    }

    public void setID(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getTransitionEffectID() {
        return transitionEffectID;
    }

    public void setTransitionEffectID(Integer transitionEffectID) {
        this.transitionEffectID = transitionEffectID;
    }

    public Integer getRotationFrequency() {
        return rotationFrequency;
    }

    public void setRotationFrequency(Integer rotationFrequency) {
        this.rotationFrequency = rotationFrequency;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getReferenceTime() {
        return referenceTime;
    }

    public void setReferenceTime(String referenceTime) {
        String time = referenceTime.substring(referenceTime.indexOf(" "), referenceTime.length());
        time = CommonAPI.convertFromMilitaryTime(time);

        this.referenceTime = time;
    }

    public boolean isOnMon() {
        return onMon;
    }

    public void setOnMon(boolean onMon) {
        this.onMon = onMon;
    }

    public boolean isOnTue() {
        return onTue;
    }

    public void setOnTue(boolean onTue) {
        this.onTue = onTue;
    }

    public boolean isOnWed() {
        return onWed;
    }

    public void setOnWed(boolean onWed) {
        this.onWed = onWed;
    }

    public boolean isOnThu() {
        return onThu;
    }

    public void setOnThu(boolean onThu) {
        this.onThu = onThu;
    }

    public boolean isOnFri() {
        return onFri;
    }

    public void setOnFri(boolean onFri) {
        this.onFri = onFri;
    }

    public boolean isOnSat() {
        return onSat;
    }

    public void setOnSat(boolean onSat) {
        this.onSat = onSat;
    }

    public boolean isOnSun() {
        return onSun;
    }

    public void setOnSun(boolean onSun) {
        this.onSun = onSun;
    }

    public SignCollection getSignCollection() {
        return signCollection;
    }

    public void setSignCollection(SignCollection signCollection) {
        this.signCollection = signCollection;
    }

}

