package com.mmhayes.common.signage.models;

import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.errorhandling.exceptions.InvalidAuthException;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.rotations.RotationHandler;
import com.mmhayes.common.signage.DigitalSignageHelper;
import com.mmhayes.common.signage.collections.KeypadItemCollection;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;

/* Keypad Item Model
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2017-11-15 17:41:26 -0500 (Wed, 15 Nov 2017) $: Date of last commit
 $Rev: 5682 $: Revision of last commit

 Notes: Contains information for a single keypad item detail
*/

public class ModifierKeypadModel {
    private Integer id = null;
    private Integer keypadID = null;
    private Integer originalKeypadID = null;
    private Integer signTemplateID = null;
    private Integer signSubTemplateID = null;
    private Integer modifierSetSignTemplateID = null;
    private String modifierSetName = "";
    private String modifierSetDescription = "";
    private String name = "";
    private String description = "";
    private String menuLabel = "";
    private String signTemplateHTML = "";
    private String signSubTemplateHTML = "";
    private String modifierSetSignTemplateHTML = "";
    private static DataManager dm = new DataManager();

    //constructor - takes hashmap that get sets to this models properties
    public ModifierKeypadModel(HashMap modelDetailHM) throws Exception {
        setModelProperties(modelDetailHM);
    }

    //setter for all of this model's properties
    public ModifierKeypadModel setModelProperties(HashMap modelDetailHM) throws Exception {

        setID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAKEYPADID")));

        setKeypadID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAKEYPADID")));

        setOriginalKeypadID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAKEYPADID")));

        setSignTemplateID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("SIGNTEMPLATEID")));

        setSignSubTemplateID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("SIGNSUBTEMPLATEID")));

        setModifierSetSignTemplateID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("MSSIGNTEMPLATEID")));

        setName(CommonAPI.convertModelDetailToString(modelDetailHM.get("NAME")));

        setDescription(CommonAPI.convertModelDetailToString(modelDetailHM.get("DESCRIPTION")));

        setMenuLabel(CommonAPI.convertModelDetailToString(modelDetailHM.get("MENULABEL")));

        setSignTemplateHTML(CommonAPI.convertModelDetailToString(modelDetailHM.get("SIGNTEMPLATEHTML")));

        setSignSubTemplateHTML(CommonAPI.convertModelDetailToString(modelDetailHM.get("SIGNSUBTEMPLATEHTML")));

        setModifierSetSignTemplateHTML(CommonAPI.convertModelDetailToString(modelDetailHM.get("MSSIGNTEMPLATEHTML")));

        setModifierSetName(CommonAPI.convertModelDetailToString(modelDetailHM.get("MSNAME")));

        setModifierSetDescription(CommonAPI.convertModelDetailToString(modelDetailHM.get("MSDESCRIPTION")));

        //check if it's a rotating modifier keypad and return the correct keypadID if it is
        Integer keypadID = RotationHandler.checkForRotatingKeypad(getOriginalKeypadID(), DigitalSignageHelper.getCurrentTVDTM());

        if(!keypadID.equals(getOriginalKeypadID())) {

            setKeypadID(CommonAPI.convertModelDetailToInteger(keypadID));

            // Set modifier keypad details for rotating modifier keypad
            setRotatingKeypadDetails();
        }

        return this;
    }

    public void setRotatingKeypadDetails() throws Exception {

        if (getKeypadID() == null) {
            throw new InvalidAuthException();
        }

        //get all models in an array list
        ArrayList<HashMap> templateList = dm.parameterizedExecuteQuery("data.signage.getRotatingKeypadDetails",
                new Object[]{
                        getKeypadID()
                },
                true
        );

        //iterate through list, create models and add them to the collection
        if (templateList != null && templateList.size() > 0) {
            HashMap templateDetails = templateList.get(0);

            setSignTemplateID(CommonAPI.convertModelDetailToInteger(templateDetails.get("SIGNTEMPLATEID")));

            setSignSubTemplateID(CommonAPI.convertModelDetailToInteger(templateDetails.get("SIGNSUBTEMPLATEID")));

            setName(CommonAPI.convertModelDetailToString(templateDetails.get("NAME")));

            setDescription(CommonAPI.convertModelDetailToString(templateDetails.get("DESCRIPTION")));

            setMenuLabel(CommonAPI.convertModelDetailToString(templateDetails.get("MENULABEL")));

            setSignTemplateHTML(CommonAPI.convertModelDetailToString(templateDetails.get("SIGNTEMPLATEHTML")));

            setSignSubTemplateHTML(CommonAPI.convertModelDetailToString(templateDetails.get("SIGNSUBTEMPLATEHTML")));
        }
    }

    public String updateModifierSignTemplate() throws Exception {
        String html = getSignTemplateHTML();


        if(getSignTemplateHTML().contains("[[Name]]")) {  //Keypad Name
            //html = DigitalSignageHelper.addTitleClasses(html, contextName, colorHM);

            html = html.replace("[[Name]]", getName());
        }

        if(getSignTemplateHTML().contains("[[MenuLabel]]")) {  //Keypad Menu Label

            //add column-count style rule
            //html = DigitalSignageHelper.addKeypadColClasses(html, signDetailModel);

            html = html.replace("[[MenuLabel]]", getMenuLabel());

        }

        if(getSignTemplateHTML().contains("[[Description]]")) {  //Keypad Description

            html = html.replace("[[Description]]", getDescription());
        }

        if(getSignTemplateHTML().contains("[[Keypad]]")) {  //Keypad details

            //add column-count style rule
            //html = DigitalSignageHelper.addKeypadColClasses(html, signDetailModel);

            //html for all the keypad items
            String keypadItemHTML = getSignSubTemplateHTML();

            //get the product's detail and creates the html from the sub template
            KeypadItemCollection keypadItemCollection = new KeypadItemCollection(keypadItemHTML, getKeypadID());

            keypadItemHTML = keypadItemCollection.getKeypadItemHtml();

            html = html.replace("[[Keypad]]", keypadItemHTML);
        }

        return html;
    }

    public Integer getID() {
        return id;
    }

    public void setID(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getKeypadID() {
        return keypadID;
    }

    public void setKeypadID(Integer keypadID) {
        this.keypadID = keypadID;
    }

    public Integer getSignTemplateID() {
        return signTemplateID;
    }

    public void setSignTemplateID(Integer signTemplateID) {
        this.signTemplateID = signTemplateID;
    }

    public Integer getSignSubTemplateID() {
        return signSubTemplateID;
    }

    public void setSignSubTemplateID(Integer signSubTemplateID) {
        this.signSubTemplateID = signSubTemplateID;
    }

    public String getMenuLabel() {
        return menuLabel;
    }

    public void setMenuLabel(String menuLabel) {
        this.menuLabel = menuLabel;
    }

    public String getSignTemplateHTML() {
        return signTemplateHTML;
    }

    public void setSignTemplateHTML(String signTemplateHTML) {

        signTemplateHTML = signTemplateHTML.replace("_lt_", "<");
        signTemplateHTML = signTemplateHTML.replace("_gt_", ">");

        this.signTemplateHTML = signTemplateHTML;
    }

    public String getSignSubTemplateHTML() {
        return signSubTemplateHTML;
    }

    public void setSignSubTemplateHTML(String signSubTemplateHTML) {

        signSubTemplateHTML = signSubTemplateHTML.replace("_lt_", "<");
        signSubTemplateHTML = signSubTemplateHTML.replace("_gt_", ">");

        this.signSubTemplateHTML = signSubTemplateHTML;
    }

    public Integer getModifierSetSignTemplateID() {
        return modifierSetSignTemplateID;
    }

    public void setModifierSetSignTemplateID(Integer modifierSetSignTemplateID) {
        this.modifierSetSignTemplateID = modifierSetSignTemplateID;
    }

    public String getModifierSetSignTemplateHTML() {
        return modifierSetSignTemplateHTML;
    }

    public void setModifierSetSignTemplateHTML(String modifierSetSignTemplateHTML) {

        modifierSetSignTemplateHTML = modifierSetSignTemplateHTML.replace("_lt_", "<");
        modifierSetSignTemplateHTML = modifierSetSignTemplateHTML.replace("_gt_", ">");

        this.modifierSetSignTemplateHTML = modifierSetSignTemplateHTML;
    }

    public String getModifierSetName() {
        return modifierSetName;
    }

    public void setModifierSetName(String modifierSetName) {
        this.modifierSetName = modifierSetName;
    }

    public String getModifierSetDescription() {
        return modifierSetDescription;
    }

    public void setModifierSetDescription(String modifierSetDescription) {
        this.modifierSetDescription = modifierSetDescription;
    }

    public Integer getOriginalKeypadID() {
        return originalKeypadID;
    }

    public void setOriginalKeypadID(Integer originalKeypadID) {
        this.originalKeypadID = originalKeypadID;
    }

}

