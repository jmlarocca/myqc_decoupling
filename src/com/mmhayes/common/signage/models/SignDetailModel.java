package com.mmhayes.common.signage.models;

import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.signage.collections.KeypadItemCollection;

import java.util.HashMap;

/* Sign Detail Model
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2017-11-15 17:41:26 -0500 (Wed, 15 Nov 2017) $: Date of last commit
 $Rev: 5682 $: Revision of last commit

 Notes: Contains information for a single sign detail
*/

public class SignDetailModel {
    private Integer id = null;
    private Integer contextTypeID = null; //not sure if necessary
    private Integer contextValue = null;
    private Integer keypadID = null;
    private Integer templateID = null;
    private Integer numKeypadCols = null;
    private Integer productID = null;
    private String contextName = "";
    private String title = "";
    private String text = "";
    private String image = "";
    private String html = "";
    private boolean listKeypadItemsVertical = false;
    KeypadItemCollection keypadItemCollection = null;

    //constructor - takes hashmap that get sets to this models properties
    public SignDetailModel(HashMap modelDetailHM) throws Exception {
        setModelProperties(modelDetailHM);
    }

    //setter for all of this model's properties
    public SignDetailModel setModelProperties(HashMap modelDetailHM) throws Exception {

        setID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("DSSIGNDETAILID")));

        setContextTypeID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("CONTEXTTYPEID")));

        setContextValue(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("CONTEXTVALUE")));

        setContextName(CommonAPI.convertModelDetailToString(modelDetailHM.get("CONTEXTNAME")));

        setTitle(CommonAPI.convertModelDetailToString(modelDetailHM.get("TITLE")));

        setKeypadID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAKEYPADID")));

        setTemplateID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("TEMPLATEID")));

        setHtml(CommonAPI.convertModelDetailToString(modelDetailHM.get("HTML")));

        setNumKeypadCols(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("KEYPADNUMCOLUMNS")));

        setListKeypadItemsVertical(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("KEYPADLISTITEMSVERTICAL")));

        setProductID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAPLUID")));

        setImage(CommonAPI.convertModelDetailToString(modelDetailHM.get("IMAGETOKENIZEDFILENAME")));

        setText(CommonAPI.convertModelDetailToString(modelDetailHM.get("TEXT")));

        return this;
    }

    public Integer getID() {
        return id;
    }

    public void setID(Integer id) {
        this.id = id;
    }

    public Integer getContextTypeID() {
        return contextTypeID;
    }

    public void setContextTypeID(Integer contextTypeID) {
        this.contextTypeID = contextTypeID;
    }

    public Integer getContextValue() {
        return contextValue;
    }

    public void setContextValue(Integer contextValue) {
        this.contextValue = contextValue;
    }

    public Integer getKeypadID() {
        return keypadID;
    }

    public void setKeypadID(Integer keypadID) {
        this.keypadID = keypadID;
    }

    public Integer getTemplateID() {
        return templateID;
    }

    public void setTemplateID(Integer templateID) {
        this.templateID = templateID;
    }


    public Integer getNumKeypadCols() {
        return numKeypadCols;
    }

    public void setNumKeypadCols(Integer numKeypadCols) {
        this.numKeypadCols = numKeypadCols;
    }

    public Integer getProductID() {
        return productID;
    }

    public void setProductID(Integer productID) {
        this.productID = productID;
    }

    public String getContextName() {
        return contextName;
    }

    public void setContextName(String contextName) {
        this.contextName = contextName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean getListKeypadItemsVertical() {
        return listKeypadItemsVertical;
    }

    public void setListKeypadItemsVertical(boolean listKeypadItemsVertical) {
        this.listKeypadItemsVertical = listKeypadItemsVertical;
    }

    public KeypadItemCollection getKeypadItemCollection() {
        return keypadItemCollection;
    }

    public void setKeypadItemCollection(KeypadItemCollection keypadItemCollection) {
        this.keypadItemCollection = keypadItemCollection;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }
}

