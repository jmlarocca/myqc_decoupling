package com.mmhayes.common.signage.models;

import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.errorhandling.exceptions.InvalidAuthException;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.rotations.RotationHandler;
import com.mmhayes.common.signage.DigitalSignageHelper;
import com.mmhayes.common.signage.collections.KeypadItemCollection;

import java.util.ArrayList;
import java.util.HashMap;

/* Keypad Item Model
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2017-11-15 17:41:26 -0500 (Wed, 15 Nov 2017) $: Date of last commit
 $Rev: 5682 $: Revision of last commit

 Notes: Contains information for a single combo detail
*/

public class ComboKeypadModel {
    private Integer id = null;
    private Integer keypadID = null;
    private Integer originalKeypadID = null;
    private Integer signTemplateID = null;
    private Integer signSubTemplateID = null;
    private Integer comboSignTemplateID = null;
    private String comboName = "";
    private String comboDescription = "";
    private String name = "";
    private String description = "";
    private String menuLabel = "";
    private String signTemplateHTML = "";
    private String signSubTemplateHTML = "";
    private String comboSignTemplateHTML = "";
    private static DataManager dm = new DataManager();

    //constructor - takes hashmap that get sets to this models properties
    public ComboKeypadModel(HashMap modelDetailHM) throws Exception {
        setModelProperties(modelDetailHM);
    }

    //setter for all of this model's properties
    public ComboKeypadModel setModelProperties(HashMap modelDetailHM) throws Exception {

        setID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAKEYPADID")));

        setKeypadID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAKEYPADID")));

        setOriginalKeypadID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAKEYPADID")));

        setSignTemplateID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("SIGNTEMPLATEID")));

        setSignSubTemplateID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("SIGNSUBTEMPLATEID")));

        setComboSignTemplateID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("COMBOSIGNTEMPLATEID")));

        setName(CommonAPI.convertModelDetailToString(modelDetailHM.get("NAME")));

        setDescription(CommonAPI.convertModelDetailToString(modelDetailHM.get("DESCRIPTION")));

        setMenuLabel(CommonAPI.convertModelDetailToString(modelDetailHM.get("MENULABEL")));

        setSignTemplateHTML(CommonAPI.convertModelDetailToString(modelDetailHM.get("SIGNTEMPLATEHTML")));

        setSignSubTemplateHTML(CommonAPI.convertModelDetailToString(modelDetailHM.get("SIGNSUBTEMPLATEHTML")));

        setComboSignTemplateHTML(CommonAPI.convertModelDetailToString(modelDetailHM.get("COMBOSIGNTEMPLATEHTML")));

        setComboName(CommonAPI.convertModelDetailToString(modelDetailHM.get("COMBONAME")));

        setComboDescription(CommonAPI.convertModelDetailToString(modelDetailHM.get("COMBODESCRIPTION")));

        //check if it's a rotating modifier keypad and return the correct keypadID if it is
        Integer keypadID = RotationHandler.checkForRotatingKeypad(getOriginalKeypadID(), DigitalSignageHelper.getCurrentTVDTM());

        if(!keypadID.equals(getOriginalKeypadID())) {

            setKeypadID(CommonAPI.convertModelDetailToInteger(keypadID));

            // Set modifier keypad details for rotating modifier keypad
            setRotatingKeypadDetails();
        }

        return this;
    }

    public void setRotatingKeypadDetails() throws Exception {

        if (getKeypadID() == null) {
            throw new InvalidAuthException();
        }

        //get all models in an array list
        ArrayList<HashMap> templateList = dm.parameterizedExecuteQuery("data.signage.getRotatingKeypadDetails",
                new Object[]{
                        getKeypadID()
                },
                true
        );

        //iterate through list, create models and add them to the collection
        if (templateList != null && templateList.size() > 0) {
            HashMap templateDetails = templateList.get(0);

            setSignTemplateID(CommonAPI.convertModelDetailToInteger(templateDetails.get("SIGNTEMPLATEID")));

            setSignSubTemplateID(CommonAPI.convertModelDetailToInteger(templateDetails.get("SIGNSUBTEMPLATEID")));

            setName(CommonAPI.convertModelDetailToString(templateDetails.get("NAME")));

            setDescription(CommonAPI.convertModelDetailToString(templateDetails.get("DESCRIPTION")));

            setMenuLabel(CommonAPI.convertModelDetailToString(templateDetails.get("MENULABEL")));

            setSignTemplateHTML(CommonAPI.convertModelDetailToString(templateDetails.get("SIGNTEMPLATEHTML")));

            setSignSubTemplateHTML(CommonAPI.convertModelDetailToString(templateDetails.get("SIGNSUBTEMPLATEHTML")));
        }
    }

    public String updateComboSignTemplate() throws Exception {
        String html = getSignTemplateHTML();


        if(getSignTemplateHTML().contains("[[Name]]")) {  //Keypad Name

            html = html.replace("[[Name]]", getName());
        }

        if(getSignTemplateHTML().contains("[[MenuLabel]]")) {  //Keypad Menu Label

            html = html.replace("[[MenuLabel]]", getMenuLabel());
        }

        if(getSignTemplateHTML().contains("[[Description]]")) {  //Keypad Description

            html = html.replace("[[Description]]", getDescription());
        }

        if(getSignTemplateHTML().contains("[[Keypad]]")) {  //Keypad details

            //html for all the keypad items
            String keypadItemHTML = getSignSubTemplateHTML();

            //get the product's detail and creates the html from the sub template
            KeypadItemCollection keypadItemCollection = new KeypadItemCollection(keypadItemHTML, getKeypadID());

            keypadItemHTML = keypadItemCollection.getKeypadItemHtml();

            html = html.replace("[[Keypad]]", keypadItemHTML);
        }

        return html;
    }

    public Integer getID() {
        return id;
    }

    public void setID(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getKeypadID() {
        return keypadID;
    }

    public void setKeypadID(Integer keypadID) {
        this.keypadID = keypadID;
    }

    public Integer getSignTemplateID() {
        return signTemplateID;
    }

    public void setSignTemplateID(Integer signTemplateID) {
        this.signTemplateID = signTemplateID;
    }

    public Integer getSignSubTemplateID() {
        return signSubTemplateID;
    }

    public void setSignSubTemplateID(Integer signSubTemplateID) {
        this.signSubTemplateID = signSubTemplateID;
    }

    public String getMenuLabel() {
        return menuLabel;
    }

    public void setMenuLabel(String menuLabel) {
        this.menuLabel = menuLabel;
    }

    public String getSignTemplateHTML() {
        return signTemplateHTML;
    }

    public void setSignTemplateHTML(String signTemplateHTML) {

        signTemplateHTML = signTemplateHTML.replace("_lt_", "<");
        signTemplateHTML = signTemplateHTML.replace("_gt_", ">");

        this.signTemplateHTML = signTemplateHTML;
    }

    public String getSignSubTemplateHTML() {
        return signSubTemplateHTML;
    }

    public void setSignSubTemplateHTML(String signSubTemplateHTML) {

        signSubTemplateHTML = signSubTemplateHTML.replace("_lt_", "<");
        signSubTemplateHTML = signSubTemplateHTML.replace("_gt_", ">");

        this.signSubTemplateHTML = signSubTemplateHTML;
    }

    public Integer getComboSignTemplateID() {
        return comboSignTemplateID;
    }

    public void setComboSignTemplateID(Integer comboSignTemplateID) {
        this.comboSignTemplateID = comboSignTemplateID;
    }

    public String getComboName() {
        return comboName;
    }

    public void setComboName(String comboName) {
        this.comboName = comboName;
    }

    public String getComboDescription() {
        return comboDescription;
    }

    public void setComboDescription(String comboDescription) {
        this.comboDescription = comboDescription;
    }

    public String getComboSignTemplateHTML() {
        return comboSignTemplateHTML;
    }

    public void setComboSignTemplateHTML(String comboSignTemplateHTML) {

        comboSignTemplateHTML = comboSignTemplateHTML.replace("_lt_", "<");
        comboSignTemplateHTML = comboSignTemplateHTML.replace("_gt_", ">");

        this.comboSignTemplateHTML = comboSignTemplateHTML;
    }


    public Integer getOriginalKeypadID() {
        return originalKeypadID;
    }

    public void setOriginalKeypadID(Integer originalKeypadID) {
        this.originalKeypadID = originalKeypadID;
    }

}

