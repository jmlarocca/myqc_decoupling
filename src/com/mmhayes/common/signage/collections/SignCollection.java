package com.mmhayes.common.signage.collections;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mmhayes.common.api.errorhandling.exceptions.InvalidAuthException;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.signage.DigitalSignageHelper;
import com.mmhayes.common.signage.models.DisplayModel;
import com.mmhayes.common.signage.models.SignModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/* Sign Collection
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2017-11-15 17:41:26 -0500 (Wed, 15 Nov 2017) $: Date of last commit
 $Rev: 5682 $: Revision of last commit

 Notes: Collection of Sign Models
*/

public class SignCollection {
    List<SignModel> collection = new ArrayList<SignModel>();
    @JsonIgnore
    private static DataManager dm = new DataManager();
    private Integer signGroupID = null;

    //constructor - populates the collection
    public SignCollection(Integer signGroupID) throws Exception {

        //determine the sign group from the request
        setSignGroupID(signGroupID);

        //populate this collection with models
        populateCollection();

    }

    //populates this collection with models
    public SignCollection populateCollection() throws Exception {

        //throw InvalidAuthException if no sign group is found
        if (getSignGroupID() == null) {
            throw new InvalidAuthException();
        }

        //get all models in an array list
        ArrayList<HashMap> signList = dm.parameterizedExecuteQuery("data.signage.getSigns",
                new Object[]{
                        getSignGroupID()
                },
                true
        );

        //iterate through list, create models and add them to the collection
        if (signList != null && signList.size() > 0) {
            for (HashMap signHM : signList) {
                if (signHM.get("DSSIGNID") != null && !signHM.get("DSSIGNID").toString().isEmpty()) {
                    SignModel signModel = new SignModel(signHM);

                    //if there is a sign ID set on the sign
                    if ( signModel.getID() != null) {

                        String signHTML = signModel.getHtml();
                        signHTML = signHTML.replace("_lt_", "<");
                        signHTML = signHTML.replace("_gt_", ">");

                        //determine if the sign should use the display or template colors
                        DisplayModel displayModel = DigitalSignageHelper.getDisplayModel();
                        HashMap colorHM = DigitalSignageHelper.determineSignColors(signModel, displayModel);

                        SignDetailCollection signDetailCollection = new SignDetailCollection(signModel.getID(), signHTML, colorHM);
                        signModel.setSignDetailCollection(signDetailCollection);

                        String signDetailHTML = signDetailCollection.getSignHtml();
                        signDetailHTML = signDetailHTML.replace("<", "_lt_");
                        signDetailHTML = signDetailHTML.replace(">", "_gt_");
                        signModel.setHtml(signDetailHTML);
                    }

                    getCollection().add(signModel);

                }
            }
        }

        return this;
    }

    //getter
    public List<SignModel> getCollection() {
        return this.collection;
    }

    //getter
    private Integer getSignGroupID() {
        return this.signGroupID;
    }

    //setter
    private void setSignGroupID(Integer signGroupID) {
        this.signGroupID = signGroupID;
    }

}
