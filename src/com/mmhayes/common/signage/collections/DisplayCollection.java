package com.mmhayes.common.signage.collections;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.signage.DigitalSignageHelper;
import com.mmhayes.common.signage.models.DisplayModel;
import com.mmhayes.common.utils.Logger;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;

/* Display Collection
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2017-11-15 17:41:26 -0500 (Wed, 15 Nov 2017) $: Date of last commit
 $Rev: 5682 $: Revision of last commit

 Notes: Collection of Display Models
*/

public class DisplayCollection {
    DisplayModel displayModel = new DisplayModel();
    @JsonIgnore private static DataManager dm = new DataManager();
    private Integer displayID = null;

    //constructor - populates the collection
    public DisplayCollection(HttpServletRequest request) throws Exception {

        //populate this collection with models
        populateCollection(request);

    }

    //populates this collection with models
    public DisplayCollection populateCollection(HttpServletRequest request) throws Exception {
        ArrayList<String> newImagesArray = new ArrayList<>();
        DigitalSignageHelper.setImagesArray(newImagesArray);

        Integer displayID = DigitalSignageHelper.getDisplayIDFromAuthHeader(request);

        Logger.logMessage("In DisplayCollection, found displayID about to create JSON object for displayID: " + displayID, Logger.LEVEL.DEBUG);

        //determine the displayID from the request
        setDisplayID(displayID);

        //throw InvalidAuthException if no authenticated employee is found
        if (getDisplayID() == null) {
            Logger.logMessage("Error in DisplayCollection.populateCollection(), displayID is null", Logger.LEVEL.TRACE);
            return this;
        }

        //get all models in an array list
        ArrayList<HashMap> displayList = dm.parameterizedExecuteQuery("data.signage.getDisplayDetails",
                new Object[]{
                        getDisplayID()
                },
                true
        );

        //iterate through list, create models and add them to the collection
        if (displayList != null && displayList.size() > 0) {
            for (HashMap displayHM : displayList) {
                if (displayHM.get("DSDISPLAYID") != null && !displayHM.get("DSDISPLAYID").toString().isEmpty()) {
                    DisplayModel displayModel = new DisplayModel(displayHM);

                    displayModel.setCurrentTime();

                    DigitalSignageHelper.setDisplayModel(displayModel);

                    //if there is a schedule ID set on the display
                    if ( displayModel.getScheduleID() != null) {

                        SignGroupCollection signGroupCollection = new SignGroupCollection(displayModel.getScheduleID());
                        displayModel.setSignGroupCollection(signGroupCollection);
                    }

                    displayModel.setImagesArray(DigitalSignageHelper.getImagesArray());

                    this.setDisplayModel(displayModel);
                }
            }
        }

        return this;
    }

    //getter
    public DisplayModel getDisplayModel() {
        return this.displayModel;
    }

    public void setDisplayModel(DisplayModel displayModel) {
        this.displayModel = displayModel;
    }


    //getter
    private Integer getDisplayID() {
        return this.displayID;
    }

    //setter
    private void setDisplayID(Integer displayID) {
        this.displayID = displayID;
    }

}
