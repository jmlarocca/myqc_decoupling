package com.mmhayes.common.signage.collections;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mmhayes.common.api.errorhandling.exceptions.InvalidAuthException;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.rotations.RotationHandler;
import com.mmhayes.common.signage.DigitalSignageHelper;
import com.mmhayes.common.signage.models.KeypadItemModel;
import com.mmhayes.common.signage.models.SignDetailModel;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/* Keypad Item Collection
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2017-11-15 17:41:26 -0500 (Wed, 15 Nov 2017) $: Date of last commit
 $Rev: 5682 $: Revision of last commit

 Notes: Collection of Keypad Item Models
*/

public class KeypadItemCollection {
    List<KeypadItemModel> collection = new ArrayList<KeypadItemModel>();
    @JsonIgnore
    private static DataManager dm = new DataManager();
    private Integer paKeypadID = null;
    private Integer paPluD = null;
    private String keypadItemHtml = "";

    //constructor - populates the collection
    public KeypadItemCollection(SignDetailModel signDetailModel) throws Exception {

        //check if it's a rotating keypad and return the correct keypadID if it is
        Integer keypadID = RotationHandler.checkForRotatingKeypad( signDetailModel.getKeypadID(), DigitalSignageHelper.getCurrentTVDTM() );

        //set the keypad id
        setPAKeypadID(keypadID);

        //populate this collection with models
        populateCollection(signDetailModel);

    }

    //constructor - populates the collection
    public KeypadItemCollection(Integer paPluID, String html) throws Exception {

        //set the product id
        setPaPluD(paPluID);

        //populate this collection with models
        populateProductCollection(html);

    }

    //constructor - populates the collection
    public KeypadItemCollection(String html, Integer paKeypadID) throws Exception {

        //set the product id
        setPAKeypadID(paKeypadID);

        //populate this collection with models
        populateModifierKeypadCollection(html);
    }

    //populates this collection with models
    public KeypadItemCollection populateCollection(SignDetailModel signDetailModel) throws Exception {
        String html = signDetailModel.getHtml();
        Integer keypadColumns = signDetailModel.getNumKeypadCols();
        boolean listKeypadItemsVertical = signDetailModel.getListKeypadItemsVertical();

        String keypadItemHTML = "";

        //throw InvalidAuthException if no keypad is found
        if (getPAKeypadID() == null) {
            throw new InvalidAuthException();
        }

        //get all models in an array list
        ArrayList<HashMap> keypadDetailList = dm.parameterizedExecuteQuery("data.signage.getKeypadItems",
                new Object[]{
                        getPAKeypadID()
                },
                true
        );

        //iterate through list, create models and add them to the collection
        if (keypadDetailList != null && keypadDetailList.size() > 0) {

            html = checkForListTag(html);

            if(listKeypadItemsVertical) {  //List keypad items vertically
                for (HashMap keypadDetailHM : keypadDetailList) {
                    String keypadTemplate = html;
                    if (keypadDetailHM.get("PAKEYPADDETAILID") != null && !keypadDetailHM.get("PAKEYPADDETAILID").toString().isEmpty()) {
                        KeypadItemModel keypadItemModel = new KeypadItemModel(keypadDetailHM);

                        getCollection().add(keypadItemModel);

                        keypadTemplate = updateKeypadItemTemplate(keypadItemModel, keypadTemplate);
                        keypadItemHTML += keypadTemplate;
                    }
                }
            } else {  //List keypad items horizontally
                ArrayList keypadItemOrderList = new ArrayList();
                Integer startIndex = 0;
                Integer nextIndex = startIndex;

                for(int i=0; i<keypadDetailList.size(); i++) {  //reorder the items so they will display horizontally
                    keypadItemOrderList.add(nextIndex);

                    if(nextIndex + keypadColumns < keypadDetailList.size()) {
                        nextIndex += keypadColumns;
                    } else {
                        startIndex += 1;
                        nextIndex = startIndex;
                    }
                }

                for(int i=0; i<keypadItemOrderList.size(); i++) {
                    Integer index = Integer.parseInt(keypadItemOrderList.get(i).toString());
                    HashMap keypadDetailHM = keypadDetailList.get(index);
                    String keypadTemplate = html;

                    if (keypadDetailHM.get("PAKEYPADDETAILID") != null && !keypadDetailHM.get("PAKEYPADDETAILID").toString().isEmpty()) {
                        KeypadItemModel keypadItemModel = new KeypadItemModel(keypadDetailHM);

                        getCollection().add(keypadItemModel);

                        keypadTemplate = updateKeypadItemTemplate(keypadItemModel, keypadTemplate);
                        keypadItemHTML += keypadTemplate;
                    }
                }
            }
        }

        this.setKeypadItemHtml(keypadItemHTML);

        return this;
    }

    //get the product's detail, populate the model and build the html
    public KeypadItemCollection populateProductCollection(String html) throws Exception {
        String productItemHTML = "";

        //throw InvalidAuthException if no keypad is found
        if (getPaPluD() == null) {
            throw new InvalidAuthException();
        }

        //get all models in an array list
        ArrayList<HashMap> productDetailList = dm.parameterizedExecuteQuery("data.signage.getProductDetails",
                new Object[]{
                        getPaPluD()
                },
                true
        );

        //iterate through list, create models and add them to the collection
        if (productDetailList != null && productDetailList.size() > 0) {
            for (HashMap productDetailHM : productDetailList) {
                String productTemplate = html;
                if (productDetailHM.get("PAKEYPADDETAILID") != null && !productDetailHM.get("PAKEYPADDETAILID").toString().isEmpty()) {
                    KeypadItemModel productItemModel = new KeypadItemModel(productDetailHM);

                    getCollection().add(productItemModel);

                    productTemplate = updateKeypadItemTemplate(productItemModel, productTemplate);
                    productItemHTML += productTemplate;
                }
            }
        }

        this.setKeypadItemHtml(productItemHTML);

        return this;
    }

    //get the product's detail, populate the model and build the html
    public KeypadItemCollection populateModifierKeypadCollection(String html) throws Exception {
        String modifierKeypadHTML = "";

        //throw InvalidAuthException if no keypad is found
        if (getPAKeypadID() == null) {
            throw new InvalidAuthException();
        }

        //get all models in an array list
        ArrayList<HashMap> modifierKeypadDetailList = dm.parameterizedExecuteQuery("data.signage.getKeypadItems",
                new Object[]{
                        getPAKeypadID()
                },
                true
        );

        //iterate through list, create models and add them to the collection
        if (modifierKeypadDetailList != null && modifierKeypadDetailList.size() > 0) {
            for (HashMap modifierKeypadHM : modifierKeypadDetailList) {
                String modifierKeypadSubTemplate = html;

                if (modifierKeypadHM.get("PAKEYPADDETAILID") != null && !modifierKeypadHM.get("PAKEYPADDETAILID").toString().isEmpty()) {
                    KeypadItemModel modifierKeypadItemModel = new KeypadItemModel(modifierKeypadHM);

                    getCollection().add(modifierKeypadItemModel);

                    modifierKeypadSubTemplate = updateKeypadItemTemplate(modifierKeypadItemModel, modifierKeypadSubTemplate);
                    modifierKeypadHTML += modifierKeypadSubTemplate;
                }
            }
        }

        this.setKeypadItemHtml(modifierKeypadHTML);

        return this;
    }

    public String updateKeypadItemTemplate(KeypadItemModel keypadItemModel, String html) throws Exception {

        if(html.contains("[[Name]]")) {
            if(keypadItemModel.getName().contains("\\r")) {
                keypadItemModel.setName(keypadItemModel.getName().replace("\\r", " "));
            }
            html = html.replace("[[Name]]", keypadItemModel.getName());
        }

        if(html.contains("[[Price]]")) {
            BigDecimal price = keypadItemModel.getPrice().setScale(2, RoundingMode.HALF_UP);
            html = html.replace("[[Price]]", price.toString());
        }

        if(html.contains("[[Description]]")) {
            html = html.replace("[[Description]]", keypadItemModel.getDescription());
        }

        if(html.contains("[[Image]]")) {
            // Get Hostname
            String hostName = DigitalSignageHelper.getHostName();

            //for testing
            if(hostName.contains("GEMDEV")) {
                hostName = "http://10.1.246.84";
            }

            // Build image path
            String imagePath = hostName+"/webimages/"+keypadItemModel.getImage();

            DigitalSignageHelper.addToImagesArray(imagePath);

            // Build Image tag
            String imageTag = "<img src=\""+imagePath+"\" style=\"display:block;\" />";

            if(keypadItemModel.getImage().equals("")) {
                imageTag = "";
            }

            html = html.replace("[[Image]]", imageTag);
        }

        if(html.contains("[[Nutrition1]]")) {
            html = html.replace("[[Nutrition1]]", keypadItemModel.getNutrition1());
        }

        if(html.contains("[[Nutrition2]]")) {
            html = html.replace("[[Nutrition2]]", keypadItemModel.getNutrition2());
        }

        if(html.contains("[[Nutrition3]]")) {
            html = html.replace("[[Nutrition3]]", keypadItemModel.getNutrition3());
        }

        if(html.contains("[[Nutrition4]]")) {
            html = html.replace("[[Nutrition4]]", keypadItemModel.getNutrition4());
        }

        if(html.contains("[[Nutrition5]]")) {
            html = html.replace("[[Nutrition5]]", keypadItemModel.getNutrition5());
        }

        if(html.contains("[[WellnessIcon]]")) {
            String wellnessImagePath = "";
            String wellnessImageTag = "";

            if( keypadItemModel.isWellness() && !DigitalSignageHelper.getGlobalWellnessIcon().equals("") ) {
                // Get Hostname
                String hostName = DigitalSignageHelper.getHostName();

                //for testing
                if(hostName.contains("GEMDEV")) {
                    hostName = "http://10.1.246.84";
                }

                // Build image path
                wellnessImagePath = hostName+"/webimages/"+DigitalSignageHelper.getGlobalWellnessIcon();

                DigitalSignageHelper.addToImagesArray(wellnessImagePath);

                // Build Image tag
                wellnessImageTag = "<img src=\""+wellnessImagePath+"\" style=\"display:block;\" />";

            } else if( keypadItemModel.isWellness() ) {

                wellnessImagePath = "images/common/icon-healthy.svg";
                // Build Image tag
                wellnessImageTag = "<img src=\""+wellnessImagePath+"\" style=\"display:block;\" />";
            }

            html = html.replace("[[WellnessIcon]]", wellnessImageTag);
        }

        if(html.contains("[[VegetarianIcon]]")) {
            String wellnessImagePath = "";
            String wellnessImageTag = "";

            if( keypadItemModel.isVegetarian() && !DigitalSignageHelper.getGlobalVegetarianIcon().equals("") ) {
                // Get Hostname
                String hostName = DigitalSignageHelper.getHostName();

                //for testing
                if(hostName.contains("GEMDEV")) {
                    hostName = "http://10.1.246.84";
                }

                // Build image path
                wellnessImagePath = hostName+"/webimages/"+DigitalSignageHelper.getGlobalVegetarianIcon();

                DigitalSignageHelper.addToImagesArray(wellnessImagePath);

                // Build Image tag
                wellnessImageTag = "<img src=\""+wellnessImagePath+"\" style=\"display:block;\" />";

            } else if( keypadItemModel.isVegetarian() ) {

                wellnessImagePath = "images/common/icon-vegetarian.svg";
                // Build Image tag
                wellnessImageTag = "<img src=\""+wellnessImagePath+"\" style=\"display:block;\" />";
            }

            html = html.replace("[[VegetarianIcon]]", wellnessImageTag);
        }

        if(html.contains("[[VeganIcon]]")) {
            String wellnessImagePath = "";
            String wellnessImageTag = "";

            if( keypadItemModel.isVegan() && !DigitalSignageHelper.getGlobalVeganIcon().equals("") ) {
                // Get Hostname
                String hostName = DigitalSignageHelper.getHostName();

                //for testing
                if(hostName.contains("GEMDEV")) {
                    hostName = "http://10.1.246.84";
                }

                // Build image path
                wellnessImagePath = hostName+"/webimages/"+DigitalSignageHelper.getGlobalVeganIcon();

                DigitalSignageHelper.addToImagesArray(wellnessImagePath);

                // Build Image tag
                wellnessImageTag = "<img src=\""+wellnessImagePath+"\" style=\"display:block;\" />";

            } else if( keypadItemModel.isVegan() ) {

                wellnessImagePath = "images/common/icon-vegan.svg";
                // Build Image tag
                wellnessImageTag = "<img src=\""+wellnessImagePath+"\" style=\"display:block;\" />";
            }

            html = html.replace("[[VeganIcon]]", wellnessImageTag);
        }

        if(html.contains("[[GlutenFreeIcon]]")) {
            String wellnessImagePath = "";
            String wellnessImageTag = "";

            if( keypadItemModel.isGlutenFree() && !DigitalSignageHelper.getGlobalGlutenFreeIcon().equals("") ) {
                // Get Hostname
                String hostName = DigitalSignageHelper.getHostName();

                //for testing
                if(hostName.contains("GEMDEV")) {
                    hostName = "http://10.1.246.84";
                }

                // Build image path
                wellnessImagePath = hostName+"/webimages/"+DigitalSignageHelper.getGlobalGlutenFreeIcon();

                DigitalSignageHelper.addToImagesArray(wellnessImagePath);

                // Build Image tag
                wellnessImageTag = "<img src=\""+wellnessImagePath+"\" style=\"display:block;\" />";

            } else if( keypadItemModel.isGlutenFree() ) {

                wellnessImagePath = "images/common/icon-glutenfree.svg";
                // Build Image tag
                wellnessImageTag = "<img src=\""+wellnessImagePath+"\" style=\"display:block;\" />";
            }

            html = html.replace("[[GlutenFreeIcon]]", wellnessImageTag);
        }

        if(html.contains("[[ModifierSet]]")) {
            String modifierSetTag = "";

            if(keypadItemModel.getModifierSetID() != null && !keypadItemModel.isModifier()) {
                ModifierKeypadCollection modifierKeypadCollection = new ModifierKeypadCollection(keypadItemModel.getModifierSetID());
                modifierSetTag = modifierKeypadCollection.getModifierSetHTML();
            }

            html = html.replace("[[ModifierSet]]", modifierSetTag);
        }

        if(html.contains("[[ComboSet]]")) {
            String comboSetTag = "";

            if(keypadItemModel.getComboID() != null ) {
                ComboKeypadCollection comboKeypadCollection = new ComboKeypadCollection(keypadItemModel.getComboID());
                comboSetTag = comboKeypadCollection.getComboHTML();
            }

            html = html.replace("[[ComboSet]]", comboSetTag);
        }

        return html;
    }

    public String checkForListTag(String html) {
        if(html.contains("<li")) {
            return html;
        } else {
            //if the keypad line template begins and ends with a div tag, replace the divs with li
            if(!html.equals("") && html.substring(0, 4).equals("<div") && html.substring(html.length()-6, html.length()).contains("/div")) {
                String afterStartingDiv = "<li" +html.substring(4, html.length());
                afterStartingDiv = afterStartingDiv.substring(0, afterStartingDiv.length()-6);
                afterStartingDiv += "</li>";
                html = afterStartingDiv;

            //if the keypad line template begins with a style tag, replace divs with li's after the style tag
            } else if (!html.equals("") && html.substring(0, 6).equals("<style") && html.substring(html.length()-6, html.length()).contains("/div")) {
                if(html.contains("</style> <li>") || html.contains("</style><li>")) {
                    return html;
                } else {
                    if(html.contains("</style> <div")) {
                        html = html.replace("</style> <div", "</style> <li");
                    } else if(html.contains("</style><div")) {
                        html = html.replace("</style><div", "</style> <li");
                    }

                    //replace ending div tag with an li tag
                    if( html.substring(html.length()-6, html.length()).equals("</div>")) {
                        html = html.substring(0, html.length()-6);
                    } else if(html.substring(html.length()-7, html.length()).equals("</div >")) {
                        html = html.substring(0, html.length()-7);
                    }

                    html += "</li>";
                }
            }
        }

        return html;
    }

    //getter
    public List<KeypadItemModel> getCollection() {
        return this.collection;
    }

    //getter
    private Integer getPAKeypadID() {
        return this.paKeypadID;
    }

    //setter
    private void setPAKeypadID(Integer paKeypadID) {
        this.paKeypadID = paKeypadID;
    }

    public String getKeypadItemHtml() {
        return keypadItemHtml;
    }

    public void setKeypadItemHtml(String keypadItemHtml) {
        this.keypadItemHtml = keypadItemHtml;
    }

    public Integer getPaPluD() {
        return paPluD;
    }

    public void setPaPluD(Integer paPluD) {
        this.paPluD = paPluD;
    }
}
