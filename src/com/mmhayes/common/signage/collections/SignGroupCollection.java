package com.mmhayes.common.signage.collections;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mmhayes.common.api.CommonAuthResource;
import com.mmhayes.common.api.errorhandling.exceptions.InvalidAuthException;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.signage.models.DisplayModel;
import com.mmhayes.common.signage.models.SignGroupModel;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/* Sign Group Collection
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2017-11-15 17:41:26 -0500 (Wed, 15 Nov 2017) $: Date of last commit
 $Rev: 5682 $: Revision of last commit

 Notes: Collection of Sign Group Models
*/

public class SignGroupCollection {
    List<SignGroupModel> collection = new ArrayList<SignGroupModel>();
    @JsonIgnore
    private static DataManager dm = new DataManager();
    private Integer scheduleID = null;

    //constructor - populates the collection
    public SignGroupCollection(Integer scheduleID) throws Exception {

        //determine the scheduleID from the request
        setScheduleID(scheduleID);

        //populate this collection with models
        populateCollection();

    }

    //populates this collection with models
    public SignGroupCollection populateCollection() throws Exception {

        //throw InvalidAuthException if no authenticated employee is found
        if (getScheduleID() == null) {
            throw new InvalidAuthException();
        }

        //get all models in an array list
        ArrayList<HashMap> signGroupList = dm.parameterizedExecuteQuery("data.signage.getSignGroups",
                new Object[]{
                        getScheduleID()
                },
                true
        );

        //iterate through list, create models and add them to the collection
        if (signGroupList != null && signGroupList.size() > 0) {
            for (HashMap signGroupHM : signGroupList) {
                if (signGroupHM.get("DSSIGNGROUPID") != null && !signGroupHM.get("DSSIGNGROUPID").toString().isEmpty()) {
                    SignGroupModel signGroupModel = new SignGroupModel(signGroupHM);

                    //if there is a sign group ID set on the sign group
                    if ( signGroupModel.getID() != null) {

                        SignCollection signCollection = new SignCollection(signGroupModel.getID());
                        signGroupModel.setSignCollection(signCollection);
                    }

                    getCollection().add(signGroupModel);
                }
            }
        }

        return this;
    }

    //getter
    public List<SignGroupModel> getCollection() {
        return this.collection;
    }

    //getter
    private Integer getScheduleID() {
        return this.scheduleID;
    }

    //setter
    private void setScheduleID(Integer scheduleID) {
        this.scheduleID = scheduleID;
    }

}
