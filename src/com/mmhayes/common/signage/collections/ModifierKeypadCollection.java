package com.mmhayes.common.signage.collections;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mmhayes.common.api.errorhandling.exceptions.InvalidAuthException;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.rotations.RotationHandler;
import com.mmhayes.common.signage.DigitalSignageHelper;
import com.mmhayes.common.signage.models.KeypadItemModel;
import com.mmhayes.common.signage.models.ModifierKeypadModel;
import com.mmhayes.common.signage.models.SignDetailModel;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/* Keypad Item Collection
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2017-11-15 17:41:26 -0500 (Wed, 15 Nov 2017) $: Date of last commit
 $Rev: 5682 $: Revision of last commit

 Notes: Collection of Modifier Keypad Models
*/

public class ModifierKeypadCollection {
    List<ModifierKeypadModel> collection = new ArrayList<ModifierKeypadModel>();
    @JsonIgnore
    private static DataManager dm = new DataManager();
    private Integer modifierSetID = null;
    private String modifierSetHTML = "";

    //constructor - populates the collection
    public ModifierKeypadCollection(Integer modifierSetID) throws Exception {

        //set the modifierSetID id
        setModifierSetID(modifierSetID);

        //populate this collection with models
        populateCollection();

    }

    //populates this collection with models
    public ModifierKeypadCollection populateCollection() throws Exception {
        String modifierSetHTML = "";

        //throw InvalidAuthException if no keypad is found
        if (getModifierSetID() == null) {
            throw new InvalidAuthException();
        }

        //get all models in an array list
        ArrayList<HashMap> modifierKeypadList = dm.parameterizedExecuteQuery("data.signage.getModifierSetKeypads",
                new Object[]{
                        getModifierSetID()
                },
                true
        );

        //iterate through list, create models and add them to the collection
        if (modifierKeypadList != null && modifierKeypadList.size() > 0) {
            Integer sortOrderID = 1;

            for(HashMap modifierKeypadHM : modifierKeypadList) {
                ModifierKeypadModel modifierKeypadModel = new ModifierKeypadModel(modifierKeypadHM);

                String modifierKeypadHTML = modifierKeypadModel.updateModifierSignTemplate();

                modifierKeypadHTML = modifierKeypadHTML.replace("<", "_lt_");
                modifierKeypadHTML = modifierKeypadHTML.replace(">", "_gt_");

                if(modifierKeypadModel.getModifierSetSignTemplateID() != null && !modifierKeypadModel.getModifierSetSignTemplateHTML().equals("")) {
                    modifierSetHTML = updateModifierSetKeypad(modifierKeypadModel, sortOrderID, modifierKeypadHTML, modifierSetHTML);

                } else {
                    modifierSetHTML += modifierKeypadHTML;
                }

                getCollection().add(modifierKeypadModel);

                sortOrderID++;
            }

            for(int x = sortOrderID; x < sortOrderID + 10; x++) {
                String modifierKeypadString = "[[ModifierKeypad" + x + "]]";
                if(modifierSetHTML.contains(modifierKeypadString)) {
                    modifierSetHTML = modifierSetHTML.replace(modifierKeypadString, "");
                }
            }

            this.setModifierSetHTML(modifierSetHTML);
        }

        return this;
    }

    public String updateModifierSetKeypad(ModifierKeypadModel modifierKeypadModel, Integer sortOrderID, String modifierKeypadHTML, String modifierSetHTML) {

        String modifierSetSignTemplate = modifierKeypadModel.getModifierSetSignTemplateHTML();
        String modifierKeypadID = "[[ModifierKeypad" + sortOrderID.toString() + "]]";

        if(modifierSetHTML.equals("")) {
            modifierSetHTML = modifierSetSignTemplate;
        }

        if(modifierSetHTML.contains("[[Name]]")) {
            modifierSetHTML = modifierSetHTML.replace("[[Name]]", modifierKeypadModel.getModifierSetName());
        }

        if(modifierSetHTML.contains("[[Description]]")) {
            modifierSetHTML = modifierSetHTML.replace("[[Description]]", modifierKeypadModel.getModifierSetDescription());
        }

        //If the Modifier Set template contains the [[ModifierKeyad]] tag for that keypad
        if(modifierSetHTML.contains(modifierKeypadID)) {
            modifierSetHTML = modifierSetHTML.replace(modifierKeypadID, modifierKeypadHTML);
        } else {
            modifierSetHTML += modifierKeypadHTML;
        }

        return modifierSetHTML;
    }

    //getter
    public List<ModifierKeypadModel> getCollection() {
        return this.collection;
    }

    public Integer getModifierSetID() {
        return modifierSetID;
    }

    public void setModifierSetID(Integer modifierSetID) {
        this.modifierSetID = modifierSetID;
    }

    public String getModifierSetHTML() {
        return modifierSetHTML;
    }

    public void setModifierSetHTML(String modifierSetHTML) {
        this.modifierSetHTML = modifierSetHTML;
    }
}
