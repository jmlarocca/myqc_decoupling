package com.mmhayes.common.signage.collections;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mmhayes.common.api.errorhandling.exceptions.InvalidAuthException;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.signage.DigitalSignageHelper;
import com.mmhayes.common.signage.models.SignDetailModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/* Sign Detail Collection
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2017-11-15 17:41:26 -0500 (Wed, 15 Nov 2017) $: Date of last commit
 $Rev: 5682 $: Revision of last commit

 Notes: Collection of Sign Detail Models
*/

public class SignDetailCollection {
    List<SignDetailModel> collection = new ArrayList<SignDetailModel>();
    @JsonIgnore
    private static DataManager dm = new DataManager();
    private Integer signID = null;
    private String signHtml = "";

    //constructor - populates the collection
    public SignDetailCollection(Integer signID, String html, HashMap colorHM) throws Exception {

        //determine the sign from the request
        setSignID(signID);

        //populate this collection with models
        populateCollection(html, colorHM);

    }

    //populates this collection with models
    public SignDetailCollection populateCollection(String html, HashMap colorHM) throws Exception {
        String signHTML = html;

        //throw InvalidAuthException if no sign is found
        if (getSignID() == null) {
            throw new InvalidAuthException();
        }

        //get all models in an array list
        ArrayList<HashMap> signDetailList = dm.parameterizedExecuteQuery("data.signage.getSignDetails",
                new Object[]{
                        getSignID()
                },
                true
        );

        //iterate through list, create models and add them to the collection
        if (signDetailList != null && signDetailList.size() > 0) {
            for (HashMap signDetailHM : signDetailList) {
                if (signDetailHM.get("DSSIGNDETAILID") != null && !signDetailHM.get("DSSIGNDETAILID").toString().isEmpty()) {
                    SignDetailModel signDetailModel = new SignDetailModel(signDetailHM);

                    //if there is a keypad ID set on the sign detail get the keypad html
                    if ( signDetailModel.getKeypadID() != null) {

                        String signDetailHTML = signDetailModel.getHtml();
                        signDetailHTML = signDetailHTML.replace("_lt_", "<");
                        signDetailHTML = signDetailHTML.replace("_gt_", ">");
                        signDetailModel.setHtml(signDetailHTML);

                        KeypadItemCollection keypadItemCollection = new KeypadItemCollection(signDetailModel);
                        signDetailModel.setKeypadItemCollection(keypadItemCollection);

                        String keypadItemHTML = keypadItemCollection.getKeypadItemHtml();
                        keypadItemHTML = keypadItemHTML.replace("<", "_lt_");
                        keypadItemHTML = keypadItemHTML.replace(">", "_gt_");
                        signDetailModel.setHtml(keypadItemHTML);
                    }

                    //if there is a product ID set on the sign detail get the product html
                    if ( signDetailModel.getProductID() != null) {

                        String signDetailHTML = signDetailModel.getHtml();
                        signDetailHTML = signDetailHTML.replace("_lt_", "<");
                        signDetailHTML = signDetailHTML.replace("_gt_", ">");
                        signDetailModel.setHtml(signDetailHTML);

                        //get the product's detail and creates the html from the sub template
                        KeypadItemCollection keypadItemCollection = new KeypadItemCollection(signDetailModel.getProductID(), signDetailModel.getHtml());
                        signDetailModel.setKeypadItemCollection(keypadItemCollection);

                        String keypadItemHTML = keypadItemCollection.getKeypadItemHtml();
                        keypadItemHTML = keypadItemHTML.replace("<", "_lt_");
                        keypadItemHTML = keypadItemHTML.replace(">", "_gt_");
                        signDetailModel.setHtml(keypadItemHTML);
                    }

                    signHTML = DigitalSignageHelper.updateSignTemplate(signDetailModel, signHTML, colorHM);

                    getCollection().add(signDetailModel);
                }
            }
        }

        this.setSignHtml(signHTML);
        return this;
    }

    //getter
    public List<SignDetailModel> getCollection() {
        return this.collection;
    }

    //getter
    private Integer getSignID() {
        return this.signID;
    }

    //setter
    private void setSignID(Integer signID) {
        this.signID = signID;
    }

    public String getSignHtml() {
        return signHtml;
    }

    public void setSignHtml(String signHtml) {
        this.signHtml = signHtml;
    }

}
