package com.mmhayes.common.signage.collections;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mmhayes.common.api.errorhandling.exceptions.InvalidAuthException;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.signage.models.ComboKeypadModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/* Keypad Item Collection
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2017-11-15 17:41:26 -0500 (Wed, 15 Nov 2017) $: Date of last commit
 $Rev: 5682 $: Revision of last commit

 Notes: Collection of Combo Keypad Models
*/

public class ComboKeypadCollection {
    List<ComboKeypadModel> collection = new ArrayList<ComboKeypadModel>();
    @JsonIgnore
    private static DataManager dm = new DataManager();
    private Integer comboID = null;
    private String comboHTML = "";

    //constructor - populates the collection
    public ComboKeypadCollection(Integer comboID) throws Exception {

        //set the comboID id
        setComboID(comboID);

        //populate this collection with models
        populateCollection();

    }

    //populates this collection with models
    public ComboKeypadCollection populateCollection() throws Exception {
        String comboHTML = "";

        //throw InvalidAuthException if no keypad is found
        if (getComboID() == null) {
            throw new InvalidAuthException();
        }

        //get all models in an array list
        ArrayList<HashMap> comboKeypadList = dm.parameterizedExecuteQuery("data.signage.getComboKeypads",
                new Object[]{
                        getComboID()
                },
                true
        );

        //iterate through list, create models and add them to the collection
        if (comboKeypadList != null && comboKeypadList.size() > 0) {
            Integer sortOrderID = 1;

            for(HashMap comboKeypadHM : comboKeypadList) {
                ComboKeypadModel comboKeypadModel = new ComboKeypadModel(comboKeypadHM);

                String comboKeypadHTML = comboKeypadModel.updateComboSignTemplate();

                comboKeypadHTML = comboKeypadHTML.replace("<", "_lt_");
                comboKeypadHTML = comboKeypadHTML.replace(">", "_gt_");

                if(comboKeypadModel.getComboSignTemplateID() != null && !comboKeypadModel.getComboSignTemplateHTML().equals("")) {
                    comboHTML = updateComboKeypad(comboKeypadModel, sortOrderID, comboKeypadHTML, comboHTML);

                } else {
                    comboHTML += comboKeypadHTML;
                }

                getCollection().add(comboKeypadModel);

                sortOrderID++;
            }

            this.setComboHTML(comboHTML);
        }

        return this;
    }

    public String updateComboKeypad(ComboKeypadModel comboKeypadModel, Integer sortOrderID, String comboKeypadHTML, String comboHTML) {

        String comboSetSignTemplate = comboKeypadModel.getComboSignTemplateHTML();
        String comboKeypadID = "[[ComboKeypad" + sortOrderID.toString() + "]]";

        if(comboHTML.equals("")) {
            comboHTML = comboSetSignTemplate;
        }

        if(comboHTML.contains("[[Name]]")) {
            comboHTML = comboHTML.replace("[[Name]]", comboKeypadModel.getComboName());
        }

        if(comboHTML.contains("[[Description]]")) {
            comboHTML = comboHTML.replace("[[Description]]", comboKeypadModel.getComboDescription());
        }

        //If the Combo template contains the [[ComboKeypad]] tag for that keypad
        if(comboHTML.contains(comboKeypadID)) {
            comboHTML = comboHTML.replace(comboKeypadID, comboKeypadHTML);
        } else {
            comboHTML += comboKeypadHTML;
        }

        return comboHTML;
    }

    //getter
    public List<ComboKeypadModel> getCollection() {
        return this.collection;
    }

    public Integer getComboID() {
        return comboID;
    }

    public void setComboID(Integer comboID) {
        this.comboID = comboID;
    }

    public String getComboHTML() {
        return comboHTML;
    }

    public void setComboHTML(String comboHTML) {
        this.comboHTML = comboHTML;
    }
}
