package com.mmhayes.common.product.models;

//API dependencies
import com.mmhayes.common.api.CommonAPI;

//other dependencies
import java.util.HashMap;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2017-03-24 08:50:00 -0400 (Fri, 24 Mar 2017) $: Date of last commit
 $Rev: 3757 $: Revision of last commit
*/
public class ItemProfileMapping {

    Integer objectTypeId = null; //maps to objectType
    Integer objectId = null;   //maps to objectItemID
    Integer itemDefaultPosItemId = null; //default pos Item Id for the specific product

    public ItemProfileMapping() {
    }

    public ItemProfileMapping(HashMap itemProfileMappingHM) {
        setModelProperties(itemProfileMappingHM);
    }

    //setter for all of this model's properties
    //used to map the default purchase category when items are saved to QC_Transaction
    public ItemProfileMapping setModelProperties(HashMap modelDetailHM) {
        setObjectTypeId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAITEMTYPEID")));
        setObjectId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAITEMID")));
        setItemDefaultPosItemId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ITEMDEFAULTPOSITEM")));

        return this;
    }

    public static ItemProfileMapping ItemProfileMapping(HashMap itemProfileMappingHM){
        ItemProfileMapping itemProfileMapping = new ItemProfileMapping(itemProfileMappingHM);

        return itemProfileMapping;
    }

    public Integer getObjectTypeId() {
        return objectTypeId;
    }

    public void setObjectTypeId(Integer objectTypeId) {
        this.objectTypeId = objectTypeId;
    }

    public Integer getObjectId() {
        return objectId;
    }

    public void setObjectId(Integer objectId) {
        this.objectId = objectId;
    }

    public Integer getItemDefaultPosItemId() {
        return itemDefaultPosItemId;
    }

    public void setItemDefaultPosItemId(Integer itemDefaultPosItemId) {
        this.itemDefaultPosItemId = itemDefaultPosItemId;
    }
}

