package com.mmhayes.common.product.models;

//mmhayes dependencies

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.loyalty.models.LoyaltyRewardModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;
import java.util.UUID;

//API dependencies
//other dependencies

/*
 Last Updated (automatically updated by SVN)
 $Author: jkflanagan $: Author of last commit
 $Date: 2020-12-31 16:39:34 -0500 (Thu, 31 Dec 2020) $: Date of last commit
 $Rev: 13381 $: Revision of last commit
*/
public class ItemRewardModel {
    private Integer id;
    private Integer transLineItemId;
    private Integer itemTypeId;
    private Integer itemId;

    private BigDecimal eligibleAmount = BigDecimal.ZERO;
    private BigDecimal amount;
    private BigDecimal refundedEligibleAmount;
    private BigDecimal refundedAmount;
    private LoyaltyRewardModel reward;
    private UUID rewardGuid = null;

    public ItemRewardModel(){
    }

    public ItemRewardModel(LoyaltyRewardModel reward, BigDecimal eligibleAmount, BigDecimal amount, boolean reverseSign){
        this.reward = reward;
        this.itemId = reward.getId();
        this.eligibleAmount = eligibleAmount;
        this.amount = amount;
        if (reverseSign){   //Rewards will be saved the database as negatives
            this.amount = amount.multiply(new BigDecimal(-1));
        }
     }

    public ItemRewardModel(LoyaltyRewardModel reward, BigDecimal eligibleAmount, BigDecimal amount){
        this.reward = reward;
        this.itemId = reward.getId();
        this.eligibleAmount = eligibleAmount;
        this.amount = amount;
        if (amount.compareTo(BigDecimal.ZERO) == 1){   //Rewards will be saved the database as negatives
            this.amount = amount.multiply(new BigDecimal(-1));
        }
    }

    public ItemRewardModel(HashMap itemTaxRewardHM){
        setModelProperties(itemTaxRewardHM);
    }

    //setter for all of this model's properties
    public ItemRewardModel setModelProperties(HashMap modelDetailHM) {
        setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));

        setTransLineItemId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("TRANSLINEITEMID")));
        setItemTypeId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ITEMTYPEID")));
        setItemId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ITEMID")));

        setAmount(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("AMOUNT")));
        setEligibleAmount(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("ELIGIBLEAMOUNT")));
        setRefundedEligibleAmount(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("REFUNDEDELIGIBLEAMOUNT")));
        setRefundedAmount(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("REFUNDEDAMOUNT")));

        return this;
    }

    //The itemTypeId specifies what type it is (Reward - 30, POS Discount - 7)
    public static ItemRewardModel getItemRewardModel(Integer itemTaxRewardId, Integer terminalId) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> itemTaxRewardList = dm.parameterizedExecuteQuery("data.posapi30.getOneItemRewardById",
                new Object[]{itemTaxRewardId},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        //product list is null or empty, throw exception
        CommonAPI.checkIsNullOrEmptyList(itemTaxRewardList, "Item Reward Not Found", terminalId);
        return ItemRewardModel.createItemRewardModel(itemTaxRewardList.get(0));
     }

    public static ItemRewardModel createItemRewardModel(HashMap itemDiscountHM){
        return new ItemRewardModel(itemDiscountHM);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getTransLineItemId() {
        return transLineItemId;
    }

    public void setTransLineItemId(Integer transLineItemId) {
        this.transLineItemId = transLineItemId;
    }

    public Integer getItemTypeId() {

        if (itemTypeId == null) {
            this.itemTypeId = PosAPIHelper.ObjectType.LOYALTYREWARD.toInt();
        }
        return this.itemTypeId;
    }

    public void setItemTypeId(Integer itemTypeId) {
        this.itemTypeId = itemTypeId;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public LoyaltyRewardModel getReward() {
        return reward;
    }

    public void setReward(LoyaltyRewardModel reward) {
        this.reward = reward;
    }

    public BigDecimal getEligibleAmount() {
        return eligibleAmount;
    }

    public void setEligibleAmount(BigDecimal eligibleAmount) {
        this.eligibleAmount = eligibleAmount;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public BigDecimal getRefundedEligibleAmount() {
        return refundedEligibleAmount;
    }

    public void setRefundedEligibleAmount(BigDecimal refundedEligibleAmount) {
        this.refundedEligibleAmount = refundedEligibleAmount;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public BigDecimal getRefundedAmount() {
        return refundedAmount;
    }

    public void setRefundedAmount(BigDecimal refundedAmount) {
        this.refundedAmount = refundedAmount;
    }

    @JsonIgnore
    public UUID getRewardGuid() {
        return rewardGuid;
    }

    public void setRewardGuid(UUID rewardGuid) {
        this.rewardGuid = rewardGuid;
    }

    /**
     * Overridden toString method for a ItemRewardModel.
     * @return The {@link String} representation of a ItemRewardModel.
     */
    @Override
    public String toString () {

        return String.format("ID: %s, TRANSLINEITEMID: %s, ITEMTYPEID: %s, ITEMID: %s, ELIGIBLEAMOUNT : %s, AMOUNT: %s, " +
                "REFUNDEDELIGIBLEAMOUNT: %s, REFUNDEDAMOUNT: %s, REWARD: %s, REWARDGUID: %s",
                Objects.toString((id != null && id > 0 ? id : "N/A"), "N/A"),
                Objects.toString((transLineItemId != null && transLineItemId > 0 ? transLineItemId : "N/A"), "N/A"),
                Objects.toString((itemTypeId != null && itemTypeId > 0 ? itemTypeId : "N/A"), "N/A"),
                Objects.toString((itemId != null && itemId > 0 ? itemId : "N/A"), "N/A"),
                Objects.toString((eligibleAmount != null ? eligibleAmount.toPlainString() : "N/A"), "N/A"),
                Objects.toString((amount != null ? amount.toPlainString() : "N/A"), "N/A"),
                Objects.toString((refundedEligibleAmount != null ? refundedEligibleAmount.toPlainString() : "N/A"), "N/A"),
                Objects.toString((refundedAmount != null ? refundedAmount.toPlainString() : "N/A"), "N/A"),
                Objects.toString((reward != null ? reward.toString() : "N/A"), "N/A"),
                Objects.toString((rewardGuid != null ? rewardGuid.toString() : "N/A"), "N/A"));

    }

}
