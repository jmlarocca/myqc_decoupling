package com.mmhayes.common.product.models;

//mmhayes dependencies

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.transaction.models.DiscountLineItemModel;
import com.mmhayes.common.transaction.models.DiscountModel;
import com.mmhayes.common.transaction.models.ProductLineItemModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

//API dependencies
//other dependencies

/*
 Last Updated (automatically updated by SVN)
 $Author: jkflanagan $: Author of last commit
 $Date: 2020-12-31 16:39:34 -0500 (Thu, 31 Dec 2020) $: Date of last commit
 $Rev: 13381 $: Revision of last commit
*/
public class ItemDiscountModel {
    private Integer id;
    private Integer transLineItemId;
    private Integer itemTypeId;
    private Integer itemId;
    private Integer tempLinkedLineItemId;
    private Integer linkedPATransLineItemId = null;

    private BigDecimal eligibleAmount = BigDecimal.ZERO;
    private BigDecimal amount;
    private BigDecimal refundedEligibleAmount;
    private BigDecimal refundedAmount;
    private BigDecimal eligibleQuantity;
    private boolean isQcDiscountPlaceHolder = false;
    private boolean isDiscountPlaceHolder = false;
    private boolean isComboDiscount = false;
    private DiscountModel discount;

    public ItemDiscountModel(){

    }

    public ItemDiscountModel(HashMap itemTaxDiscountHM) {
        setModelProperties(itemTaxDiscountHM);
    }

    //setter for all of this model's properties
    public ItemDiscountModel setModelProperties(HashMap modelDetailHM) {
        setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));

        setTransLineItemId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("TRANSLINEITEMID")));
        setItemTypeId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ITEMTYPEID")));
        setItemId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ITEMID")));

        setAmount(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("AMOUNT")));
        setEligibleAmount(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("ELIGIBLEAMOUNT")));
        setRefundedEligibleAmount(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("REFUNDEDELIGIBLEAMOUNT")));
        setRefundedAmount(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("REFUNDEDAMOUNT")));
        setLinkedPATransLineItemId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("LINKEDPATRANSLINEITEMID")));

        return this;
    }

    //The itemTypeId specifies what type it is (Tax - 6, POS Discount - 7)
    public static ItemDiscountModel getItemDiscountModel(Integer itemTaxDiscountId, Integer terminalId) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> itemTaxDiscountList = dm.parameterizedExecuteQuery("data.posapi30.getOneItemDiscountById",
                new Object[]{itemTaxDiscountId},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        //product list is null or empty, throw exception
        CommonAPI.checkIsNullOrEmptyList(itemTaxDiscountList, "Item Tax Discount Not Found", terminalId);
        return ItemDiscountModel.createItemDiscountModel(itemTaxDiscountList.get(0));

    }

    public static ItemDiscountModel createItemDiscountModel(HashMap itemDiscountHM) {
        return new ItemDiscountModel(itemDiscountHM);
    }

    public static ItemDiscountModel createQcDiscountLineItemModel(BigDecimal weightedQcDiscountAmt) throws Exception {
        ItemDiscountModel itemDiscountLineItemModel = new ItemDiscountModel();
        itemDiscountLineItemModel.setDiscount(new DiscountModel());
        itemDiscountLineItemModel.setIsQcDiscountPlaceHolder(true);
        itemDiscountLineItemModel.setAmount(weightedQcDiscountAmt);

        return itemDiscountLineItemModel;
    }

    public static ItemDiscountModel createDiscountLineItemModel(BigDecimal weightedDiscountAmt, DiscountLineItemModel discountLineItemModel, ProductLineItemModel productLineItemModel) throws Exception {
        ItemDiscountModel itemDiscountLineItemModel = new ItemDiscountModel();
        itemDiscountLineItemModel.setIsDiscountPlaceHolder(true);
        itemDiscountLineItemModel.setEligibleAmount(ProductLineItemModel.getAdjValOfExtendedAmountForDiscount(productLineItemModel, false));
        itemDiscountLineItemModel.setItemId(discountLineItemModel.getDiscount().getId());
        itemDiscountLineItemModel.setDiscount(discountLineItemModel.getDiscount());
        itemDiscountLineItemModel.setAmount(weightedDiscountAmt);

        return itemDiscountLineItemModel;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getTransLineItemId() {
        return transLineItemId;
    }

    public void setTransLineItemId(Integer transLineItemId) {
        this.transLineItemId = transLineItemId;
    }

    public Integer getItemTypeId() {

        if (itemTypeId == null) {
            this.itemTypeId = PosAPIHelper.ObjectType.POSDISCOUNT.toInt();
        }
        return this.itemTypeId;
    }

    public void setItemTypeId(Integer itemTypeId) {
        this.itemTypeId = itemTypeId;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public DiscountModel getDiscount() {
        return discount;
    }

    public void setDiscount(DiscountModel discount) {
        this.discount = discount;
    }

    public BigDecimal getEligibleAmount() {
        return eligibleAmount;
    }

    public void setEligibleAmount(BigDecimal eligibleAmount) {
        this.eligibleAmount = eligibleAmount;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public BigDecimal getRefundedEligibleAmount() {
        return refundedEligibleAmount;
    }

    public void setRefundedEligibleAmount(BigDecimal refundedEligibleAmount) {
        this.refundedEligibleAmount = refundedEligibleAmount;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public BigDecimal getRefundedAmount() {
        return refundedAmount;
    }

    public void setRefundedAmount(BigDecimal refundedAmount) {
        this.refundedAmount = refundedAmount;
    }

    @JsonIgnore
    public boolean isQcDiscountPlaceHolder() {
        return isQcDiscountPlaceHolder;
    }

    public void setIsQcDiscountPlaceHolder(boolean isQcDiscountPlaceHolder) {
        this.isQcDiscountPlaceHolder = isQcDiscountPlaceHolder;
    }

    @JsonIgnore
    public boolean isDiscountPlaceHolder() {
        return isDiscountPlaceHolder;
    }

    public void setIsDiscountPlaceHolder(boolean discountPlaceHolder) {
        isDiscountPlaceHolder = discountPlaceHolder;
    }

    public BigDecimal getEligibleQuantity() {
        return eligibleQuantity;
    }

    public void setEligibleQuantity(BigDecimal eligibleQuantity) {
        this.eligibleQuantity = eligibleQuantity;
    }

    @JsonGetter("isComboDiscount")
    public boolean isComboDiscount() {
        return isComboDiscount;
    }

    public void setIsComboDiscount(boolean isComboDiscount) {
        this.isComboDiscount = isComboDiscount;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getTempLinkedLineItemId() {
        return tempLinkedLineItemId;
    }

    public void setTempLinkedLineItemId(Integer tempLinkedLineItemId) {
        this.tempLinkedLineItemId = tempLinkedLineItemId;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getLinkedPATransLineItemId() {
        return linkedPATransLineItemId;
    }

    public void setLinkedPATransLineItemId(Integer linkedPATransLineItemId) {
        this.linkedPATransLineItemId = linkedPATransLineItemId;
    }

    /**
     * Overridden toString method for a ItemDiscountModel.
     * @return The {@link String} representation of a ItemDiscountModel.
     */
    @Override
    public String toString () {

        return String.format("ID: %s, TRANSLINEITEMID: %s, ITEMTYPEID: %s, ITEMID: %s, TEMPLINKEDLINEITEMID: %s, " +
                "LINKEDPATRANSLINEITEMID: %s, ELIGIBLEAMOUNT: %s, AMOUNT:%s, REFUNDEDELIGIBLEAMOUNT: %s, " +
                "REFUNDEDAMOUNT: %s, ELIGIBLEQUANTITY: %s, ISQCDISCOUNTPLACEHOLDER: %s, ISDISCOUNTPLACEHOLDER: %s, " +
                "ISCOMBODISCOUNT: %s, DISCOUNT: %s",
                Objects.toString((id != null && id > 0 ? id : "N/A"), "N/A"),
                Objects.toString((transLineItemId != null && transLineItemId > 0 ? transLineItemId : "N/A"), "N/A"),
                Objects.toString((itemTypeId != null && itemTypeId > 0 ? itemTypeId : "N/A"), "N/A"),
                Objects.toString((itemId != null && itemId > 0 ? itemId : "N/A"), "N/A"),
                Objects.toString((tempLinkedLineItemId != null && tempLinkedLineItemId > 0 ? tempLinkedLineItemId : "N/A"), "N/A"),
                Objects.toString((linkedPATransLineItemId != null && linkedPATransLineItemId > 0 ? linkedPATransLineItemId : "N/A"), "N/A"),
                Objects.toString((eligibleAmount != null ? eligibleAmount.toPlainString() : "N/A"), "N/A"),
                Objects.toString((amount != null ? amount.toPlainString() : "N/A"), "N/A"),
                Objects.toString((refundedEligibleAmount != null ? refundedEligibleAmount.toPlainString() : "N/A"), "N/A"),
                Objects.toString((refundedAmount != null ? refundedAmount.toPlainString() : "N/A"), "N/A"),
                Objects.toString((eligibleQuantity != null ? eligibleQuantity.toPlainString() : "N/A"), "N/A"),
                Objects.toString(isQcDiscountPlaceHolder, "N/A"),
                Objects.toString(isDiscountPlaceHolder, "N/A"),
                Objects.toString(isComboDiscount, "N/A"),
                Objects.toString((discount != null ? discount.toString() : "N/A"), "N/A"));

    }

}
