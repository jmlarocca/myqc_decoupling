package com.mmhayes.common.product.models;

//API dependencies

import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.api.errorhandling.exceptions.MissingDataException;
import com.mmhayes.common.transaction.models.DiscountCalculation;
import com.mmhayes.common.transaction.models.ModifierLineItemModel;
import com.mmhayes.common.transaction.models.ProductLineItemModel;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.StringFunctions;

import java.math.BigDecimal;
import java.util.*;

//other dependencies

/*
Last Updated (automatically updated by SVN)
$Author: eglundin $: Author of last commit
$Date: 2021-09-17 16:25:34 -0400 (Fri, 17 Sep 2021) $: Date of last commit
$Rev: 15401 $: Revision of last commit
*/

public class ProductCalculation {
    private Integer id; //PAPLUID
    private Integer index = null; //keep track of the order of the products
    private ProductModel productModel; //contains all of the product properties
    private BigDecimal linePrice = null; //full price of the whole line
    private BigDecimal quantity = BigDecimal.ZERO; //product line quantity
    private BigDecimal quantityOriginal = BigDecimal.ZERO; //product line quantity original
    private List<ModifierCalculation> modifiers = null;
    private List<HashMap> discountsApplied = new ArrayList<HashMap>(); //holds EXTENDED discount amounts applied
    private List<HashMap> discountTaxes = new ArrayList<HashMap>(); //holds tax related information for discounts
    private List<HashMap> rewardsApplied = new ArrayList<HashMap>(); //holds EXTENDED reward amounts applied
    private List<HashMap> rewardTaxes = new ArrayList<HashMap>(); //holds tax related information for rewards
    private List<Integer> comboOccurrences = new ArrayList<Integer>(); //holds the ID of each combo this product is in
    private Integer comboTransLineItemId = null;
    private Integer comboDetailId = null;
    private BigDecimal basePrice = null;
    private BigDecimal comboPrice = null;
    private BigDecimal upcharge = null;
    private BigDecimal quantityInCombos = BigDecimal.ZERO;
    private Integer originalIndex = null;
    private int roundingMode = BigDecimal.ROUND_HALF_UP;
    private PosAPIHelper.TransactionType transactionTypeEnum = PosAPIHelper.TransactionType.NONE;
    private PrepOptionModel prepOption = null;
    private Integer transactionItemNum = null;

    public ProductCalculation() {

    }

    //clones the product calculation model, for helping with calculations
    public ProductCalculation(ProductCalculation productCalculationReference) {
        this.setId(productCalculationReference.getId());
        this.setIndex(productCalculationReference.getIndex());
        this.setQuantity(productCalculationReference.getQuantity());
        this.setQuantityOriginal(productCalculationReference.getQuantity()); //keep track of the original quantity
        this.setProductModel(productCalculationReference.getProductModel());
        this.setLinePrice(productCalculationReference.getLinePrice());
        this.setTransactionItemNum(productCalculationReference.getTransactionItemNum());

        // Combo related fields
        this.setComboOccurrences(productCalculationReference.getComboOccurrences());
        this.setComboTransLineItemId(productCalculationReference.getComboTransLineItemId());
        this.setComboDetailId(productCalculationReference.getComboDetailId());
        this.setBasePrice(productCalculationReference.getBasePrice());
        this.setComboPrice(productCalculationReference.getComboPrice());
        this.setUpcharge(productCalculationReference.getUpcharge());
        this.setQuantityInCombos(productCalculationReference.getQuantityInCombos());

        //deep copy modifiers...
        if ( productCalculationReference.getModifiers() != null && productCalculationReference.getModifiers().size() > 0 ) {
            List<ModifierCalculation> mods = new ArrayList<>();
            for ( ModifierCalculation modifierCalculationRef : productCalculationReference.getModifiers() ) {
                ModifierCalculation modifierCalculation = new ModifierCalculation( modifierCalculationRef );
                mods.add( modifierCalculation );
            }
            this.setModifiers( mods );
        }

        //deep copy prep option...
        if ( productCalculationReference.getPrepOption() != null ) {
            this.setPrepOption(productCalculationReference.getPrepOption());
        }

        this.setTransactionTypeEnum(productCalculationReference.getTransactionTypeEnum());
    }

    public ProductCalculation(HashMap productCalculationHM, ProductModel productModel, List<ModifierCalculation> modifiers) throws Exception {
        this.setModelProperties(productCalculationHM);
        this.setProductModel(productModel);
        this.setModifiers(modifiers);
    }

    public ProductCalculation(HashMap productCalculationHM, ProductModel productModel, List<ModifierCalculation> modifiers, PosAPIHelper.TransactionType transactionTypeEnum) throws Exception {
        this.setModelProperties(productCalculationHM);
        this.setProductModel(productModel);
        this.setModifiers(modifiers);
        this.setTransactionTypeEnum(transactionTypeEnum);
    }

    public void setModelProperties(HashMap productCalculationHM) throws Exception {
        this.setId(CommonAPI.convertModelDetailToInteger(productCalculationHM.get("ID")));
        this.setIndex(CommonAPI.convertModelDetailToInteger(productCalculationHM.get("INDEX")));
        this.setOriginalIndex(CommonAPI.convertModelDetailToInteger(productCalculationHM.get("ORIGINALINDEX")));
        this.setLinePrice(CommonAPI.convertModelDetailToBigDecimal(productCalculationHM.get("LINEPRICE")));
        this.setTransactionItemNum(CommonAPI.convertModelDetailToInteger(productCalculationHM.get("TRANSACTIONITEMNUM")));

        BigDecimal quantity = CommonAPI.convertModelDetailToBigDecimal(productCalculationHM.get("QUANTITY"));
        this.setQuantity(quantity);
        this.setQuantityOriginal(quantity);

        Integer comboTransLineItemId = CommonAPI.convertModelDetailToInteger(productCalculationHM.get("COMBOTRANSLINEITEMID"));

        if ( comboTransLineItemId != null ) {
            this.setComboTransLineItemId(comboTransLineItemId);
            this.setQuantityInCombos(quantity);
            this.setComboDetailId(CommonAPI.convertModelDetailToInteger(productCalculationHM.get("COMBODETAILID")));
            this.setBasePrice(CommonAPI.convertModelDetailToBigDecimal(productCalculationHM.get("BASEPRICE")));
            this.setComboPrice(CommonAPI.convertModelDetailToBigDecimal(productCalculationHM.get("COMBOPRICE")));
        }

        Integer prepOptionId = CommonAPI.convertModelDetailToInteger(productCalculationHM.get("PREPOPTIONID"));

        if( prepOptionId != null ) {
            PrepOptionModel prepOptionModel = new PrepOptionModel();
            prepOptionModel.setId(prepOptionId);
            prepOptionModel.setPrepOptionSetId(CommonAPI.convertModelDetailToInteger(productCalculationHM.get("PREPOPTIONSETID")));
            prepOptionModel.setPrice(CommonAPI.convertModelDetailToBigDecimal(productCalculationHM.get("PREPOPTIONPRICE")));
            this.setPrepOption(prepOptionModel);
        }
    }

    //returns the discountable amount for the given discount
    public BigDecimal getDiscountableAmount(Integer discountId) {
        //can't be applied to only modifiers, so if it is not eligible on the main product, return zero result
        if ( !this.getProductModel().isDiscountValidForProduct(discountId) ) {
            return BigDecimal.ZERO;
        }

        return this.getCurrentPrice(true, true, discountId, null, this.getTransactionItemNum());
    }

    //returns the discountable amount for the given discount
    public BigDecimal getDiscountableAmount(DiscountCalculation discountCalculation) {
        //can't be applied to only modifiers, so if it is not eligible on the main product, return zero result
        if ( !this.getProductModel().isDiscountValidForProduct(discountCalculation.getId()) ) {
            return BigDecimal.ZERO;
        }

        if (this.getTransactionItemNum().compareTo(discountCalculation.getTransactionItemNum()) > 0){
            return BigDecimal.ZERO;
        }

        return this.getCurrentPrice(true, true, discountCalculation.getId(), null, discountCalculation.getTransactionItemNum());
    }

    public void applyPriceReduction(boolean isDiscount, Integer itemId, BigDecimal amount, BigDecimal eligibleAmount, BigDecimal quantity, String taxIds, boolean singleQuantity) {
        applyPriceReduction(isDiscount, itemId, amount, eligibleAmount, quantity, taxIds, singleQuantity, false);
    }

    //updates the working price(s) and adds the appropriate price reduction records for calculating taxes later
    public void applyPriceReduction(boolean isDiscount, Integer itemId, BigDecimal amount, BigDecimal eligibleAmount, BigDecimal quantity, String taxIds, boolean singleQuantity, boolean isComboDiscount) {
        HashMap priceReductionHM = new HashMap();

        BigDecimal dqrEligibleQuantity = BigDecimal.ZERO; //if no DQR was applied, leave the dqrEligibleQuantity at 0
        if (this.getQuantityOriginal().compareTo(quantity) != 0){
            //if a DQR was applied, keep track of the applied quantity
            dqrEligibleQuantity = quantity;
        }

        if (this.getComboTransLineItemId() != null) {
            //if there are no modifiers, simply update the main product
            if (this.getModifiers() == null || this.getModifiers().size() == 0) {
                //amount here should be 4 decimal amount
                priceReductionHM.put(isDiscount ? "discountId" : "rewardId", itemId);
                priceReductionHM.put("amount", amount);
                priceReductionHM.put("isSingleQuantity", singleQuantity);
                priceReductionHM.put("taxIds", taxIds);
                priceReductionHM.put("dqrEligibleQuantity", dqrEligibleQuantity);
                priceReductionHM.put("isComboDiscount", isComboDiscount);
                this.recordPriceReduction(isDiscount, priceReductionHM);

                return;
            }
        }

        BigDecimal mainProductPrice = BigDecimal.ZERO;
        if (!isDiscount){
            //Rewards: When getting the product price when applying rewards, use the extended amount.
            //Rewards are calculated in LoyaltyRewardApplication.  This will ensure taxes are calculated correctly.
            mainProductPrice = this.getCurrentPrice(true, false, null, null);
        } else {
            mainProductPrice = this.getCurrentPrice(false, false, null, null);
        }

        if(this.getPrepOption() != null) {
            mainProductPrice = mainProductPrice.add(getPrepOption().getPrice());
        }

        mainProductPrice = mainProductPrice.multiply(quantity).setScale(4, this.getRoundingMode());
        BigDecimal mainProductPriceReduction = BigDecimal.ZERO;
        if (eligibleAmount.compareTo(BigDecimal.ZERO) != 0){
            BigDecimal discountRewardPortionForProduct = mainProductPrice.divide(eligibleAmount, 4, this.getRoundingMode());
            if (discountRewardPortionForProduct.compareTo(BigDecimal.ONE) == 1){
                //Not possible for more than 100% to go to this product
                discountRewardPortionForProduct = BigDecimal.ONE;
            }

            //Need to determine what portion of the discount/reward goes to the parent product
            mainProductPriceReduction = discountRewardPortionForProduct.multiply(amount).setScale(4, this.getRoundingMode());
        }

        priceReductionHM.put(isDiscount ? "discountId" : "rewardId", itemId);
        priceReductionHM.put("amount", mainProductPriceReduction);
        priceReductionHM.put("isSingleQuantity", singleQuantity);
        priceReductionHM.put("taxIds", taxIds);
        priceReductionHM.put("dqrEligibleQuantity", dqrEligibleQuantity);
        priceReductionHM.put("isComboDiscount", isComboDiscount);

        this.recordPriceReduction(isDiscount, priceReductionHM);

        for ( ModifierCalculation modifierCalculation : this.getModifiers() ) {
            //if this is a discount and it is not valid (mapped) for this product, just continue
            if ( isDiscount && !modifierCalculation.getProductModel().isDiscountValidForProduct(itemId) ) {
                continue;
            }

            BigDecimal modPrice = modifierCalculation.getCurrentPrice(false, null);
            modPrice = modPrice.multiply(quantity).setScale(4, this.getRoundingMode());

            BigDecimal modifierProductPriceReduction = BigDecimal.ZERO;
            if (eligibleAmount.compareTo(BigDecimal.ZERO) != 0){
                BigDecimal discountRewardPortionForModifier = modPrice.divide(eligibleAmount, 4, this.getRoundingMode());
                if (discountRewardPortionForModifier.compareTo(BigDecimal.ONE) == 1){
                    //Not possible for more than 100% to go to this modifier
                    discountRewardPortionForModifier = BigDecimal.ONE;
                }

                //Need to determine what portion of the discount/reward goes to the modifier product
                modifierProductPriceReduction = discountRewardPortionForModifier.multiply(amount).setScale(4, this.getRoundingMode());
            }

            HashMap modifierPriceReductionHM = new HashMap();
            modifierPriceReductionHM.put(isDiscount ? "discountId" : "rewardId", itemId);
            modifierPriceReductionHM.put("isSingleQuantity", singleQuantity);
            modifierPriceReductionHM.put("amount", modifierProductPriceReduction);
            modifierPriceReductionHM.put("taxIds", taxIds);
            modifierPriceReductionHM.put("dqrEligibleQuantity", dqrEligibleQuantity);
            modifierPriceReductionHM.put("isComboDiscount", isComboDiscount);

            modifierCalculation.recordPriceReduction(isDiscount, modifierPriceReductionHM);
        }
    }

    public void recordPriceReduction(boolean isDiscount, HashMap priceReductionHM) {
        // D-4420/D-4331
        // ensure the reward amount does not exceed the amount for the transaction
        List<HashMap> rewardDiscountHMs = new ArrayList<>();
        rewardDiscountHMs.addAll(this.getDiscountsApplied());
        rewardDiscountHMs.addAll(this.getRewardsApplied());

        BigDecimal rewardDiscountTotal = new BigDecimal(0);
        BigDecimal transactionSubTotal = this.getProductModel().getPrice();
        if (this.getPrepOption() != null) {
            transactionSubTotal = transactionSubTotal.add(this.getPrepOption().getPrice());
        }
        transactionSubTotal = transactionSubTotal.multiply(this.getQuantity());
        BigDecimal originalAmount = new BigDecimal(priceReductionHM.get("amount").toString());

        for(HashMap hm : rewardDiscountHMs) {
            rewardDiscountTotal = rewardDiscountTotal.add(new BigDecimal(hm.get("amount").toString()));
        }
        rewardDiscountTotal = rewardDiscountTotal.add(originalAmount); // add the current reward/discount amount to our total
        if(rewardDiscountTotal.compareTo(transactionSubTotal) == 1) { // rewardDiscountTotal > product cost, that's no good
            rewardDiscountTotal = originalAmount.subtract(rewardDiscountTotal.subtract(transactionSubTotal)); // set the total of the current reward/discount to be the difference to set it as a 100% discount
            priceReductionHM.put("amount", rewardDiscountTotal);
        }
        //record the overall price reduction
        if ( isDiscount ) {
            this.getDiscountsApplied().add(priceReductionHM);
        } else {
            this.getRewardsApplied().add(priceReductionHM);
        }

        //apply itemTaxModel building helper data - ignore if no valid taxes
        if ( priceReductionHM.get("taxIds").toString().equals("") ) {
            return;
        }

        ArrayList<String> taxIds = CommonAPI.convertCommaSeparatedStringToArrayList( priceReductionHM.get("taxIds").toString(), true );

        for ( String taxId : taxIds ) {
            if ( !this.getProductModel().isTaxValidForProduct(Integer.parseInt(taxId)) ) {
                continue;
            }

            HashMap taxHM = new HashMap();
            taxHM.put("id", priceReductionHM.get(isDiscount ? "discountId" : "rewardId"));
            taxHM.put("taxId", taxId);
            taxHM.put("amount", priceReductionHM.get("amount"));

            if ( isDiscount ) {
                this.getDiscountTaxes().add(taxHM);
            } else {
                this.getRewardTaxes().add(taxHM);
            }
        }
    }

    //gets the current price, current discountable amount, or current taxable amount for this product line
    //returns either the individual amount or extended amount based on the getExtendedAmount flag
    //returns either the main/parent product amount or the entire line amount based on the considerModifiers flag
    //set a discountId to get a discountable amount (checks modifiers for mappings)
    //set a taxId to get a taxable amount (checks discounts applied for mappings to the tax)
    public BigDecimal getCurrentPrice(boolean getExtendedAmount, boolean considerModifiersOrPrep, Integer discountId, Integer taxId) {
        return getCurrentPrice(getExtendedAmount, considerModifiersOrPrep, discountId, taxId, null);
    }

    //gets the current price, current discountable amount, or current taxable amount for this product line
    //returns either the individual amount or extended amount based on the getExtendedAmount flag
    //returns either the main/parent product amount or the entire line amount based on the considerModifiers flag
    //set a discountId to get a discountable amount (checks modifiers for mappings)
    //set a taxId to get a taxable amount (checks discounts applied for mappings to the tax)
    //set transactionItemNum to check if discount was applied before (product not valid) or after the product (product valid)
    public BigDecimal getCurrentPrice(boolean getExtendedAmount, boolean considerModifiersOrPrep, Integer discountId, Integer taxId, Integer discountTransactionItemNum) {
        BigDecimal price = this.getProductModel().getPrice().setScale(4, this.getRoundingMode());

        if(this.getComboTransLineItemId() != null) {
            price = (this.getProductModel().getPrice().subtract(this.getBasePrice())).add(this.getComboPrice()).setScale(4, this.getRoundingMode());
        }

        BigDecimal quantity = this.getQuantity().setScale(4, this.getRoundingMode());
        BigDecimal quantityOriginal = this.getQuantityOriginal().setScale(4, this.getRoundingMode());

        BigDecimal currentAmount = price.multiply(quantity).setScale(4, this.getRoundingMode());
        boolean dqrApplied = quantity.compareTo(quantityOriginal) != 0;

        //if looking for a discountable amount and this product is not mapped to the discount, then the amount is zero
        if ( discountId != null && !this.getProductModel().isDiscountValidForProduct(discountId) ) {
            currentAmount = BigDecimal.ZERO;

        } else if (discountId != null && discountTransactionItemNum != null
                && this.getTransactionItemNum().compareTo(discountTransactionItemNum) > 0){
            //if the product came after the discount.transactionItemNum
            currentAmount = BigDecimal.ZERO;

        } else if( discountId != null && considerModifiersOrPrep && this.getPrepOption() != null ) {
            currentAmount = currentAmount.add(this.getPrepOption().getPrice().multiply(quantity).setScale(4, this.getRoundingMode()));

        } else if (discountId != null && this.getProductModel().isWeighted()) {
            currentAmount = price.multiply(quantity).setScale(2, this.getRoundingMode());
        }

        //if looking for a taxable amount and this product is not mapped to the tax, then the amount is zero
        if ( taxId != null && !this.getProductModel().isTaxValidForProduct(taxId) ) {
            currentAmount = BigDecimal.ZERO;

        } else if( taxId != null && considerModifiersOrPrep && this.getPrepOption() != null ) {
            currentAmount = currentAmount.add(this.getPrepOption().getPrice().multiply(quantity).setScale(4, this.getRoundingMode()));
        }

        boolean passesAmountCheck = this.productAmountCheck(getTransactionTypeEnum(), currentAmount);
        if (passesAmountCheck && this.getDiscountsApplied().size() > 0 ) {
            BigDecimal discountAppliedAmount = BigDecimal.ZERO;
            BigDecimal discountAppliedQuantity = BigDecimal.ZERO;

            for (HashMap discountAppliedHM : this.getDiscountsApplied()) {
                BigDecimal discountAmount = CommonAPI.convertModelDetailToBigDecimal(discountAppliedHM.get("amount"));
                BigDecimal quantityOnDiscount = CommonAPI.convertModelDetailToBigDecimal(discountAppliedHM.get("dqrEligibleQuantity"));
                boolean isComboDiscount = CommonAPI.convertModelDetailToBoolean(discountAppliedHM.get("isComboDiscount"));

                //if looking for a current price, or a discountable amount, or if looking for a taxable amount and this discount is mapped to the tax, subtract the discount
                if ((taxId == null || checkTaxMapping(discountAppliedHM, taxId)) && !isComboDiscount) {
                    discountAppliedAmount = discountAppliedAmount.add(discountAmount);
                    discountAppliedQuantity = discountAppliedQuantity.add(quantityOnDiscount);
                }
            }

            if (dqrApplied){
                currentAmount = price.multiply(quantityOriginal).setScale(4, this.getRoundingMode());

                currentAmount = getDqrDiscountableAmount(price, quantity, currentAmount, discountAppliedQuantity);
            } else {

                currentAmount = currentAmount.subtract(discountAppliedAmount);
            }

        }

        passesAmountCheck = this.productAmountCheck(getTransactionTypeEnum(), currentAmount);
        if (passesAmountCheck && this.getRewardsApplied().size() > 0 ) {
            for ( HashMap rewardAppliedHM : this.getRewardsApplied() ) {
                boolean isSingleQuantity = CommonAPI.convertModelDetailToBoolean(rewardAppliedHM.get("isSingleQuantity"));

                //if it is a free product reward, or any other type that is applied to just a single quantity
                if ( !getExtendedAmount && isSingleQuantity ) {
                    continue;
                }

                BigDecimal rewardAmount = CommonAPI.convertModelDetailToBigDecimal(rewardAppliedHM.get("amount"));

                //if looking for a current price, or if looking for a taxable amount and this reward is mapped to the tax, subtract the reward
                if ( taxId == null || checkTaxMapping(rewardAppliedHM, taxId) ) {
                    currentAmount = currentAmount.subtract(rewardAmount);
                }
            }
        }

        if ( considerModifiersOrPrep && this.getModifiers() != null && this.getModifiers().size() > 0 ) {
            for ( ModifierCalculation modifierCalculation : this.getModifiers() ) {
                //skip zero priced modifiers
                if ( modifierCalculation.getProductModel().getPrice() == null || modifierCalculation.getProductModel().getPrice().compareTo(BigDecimal.ZERO) == 0 && (modifierCalculation.getPrepOption() == null || (modifierCalculation.getPrepOption() != null && modifierCalculation.getPrepOption().getPrice().compareTo(BigDecimal.ZERO) == 0)) ) {
                    continue;
                }

                //if looking for a discountable amount and this modifier is not mapped to the discount, skip it
                if ( discountId != null && !modifierCalculation.getProductModel().isDiscountValidForProduct(discountId) ) {
                    continue;
                } else if (discountId != null && modifierCalculation.getTransactionItemNum().compareTo(discountTransactionItemNum) > 0) {
                    currentAmount = BigDecimal.ZERO;
                }

                //if looking for a taxable amount and this modifier is not mapped to the tax, skip it
                if ( taxId != null && !modifierCalculation.getProductModel().isTaxValidForProduct(taxId) ) {
                    continue;
                }

                BigDecimal extendedModifierPrice = modifierCalculation.getCurrentPrice(getExtendedAmount, taxId);
                currentAmount = currentAmount.add(extendedModifierPrice);
            }

            if(taxId == null && discountId == null && this.getPrepOption() != null) {
                currentAmount = currentAmount.add(this.getPrepOption().getPrice().multiply(quantity).setScale(4, this.getRoundingMode()));
            }
        }

        return getExtendedAmount || currentAmount.compareTo(BigDecimal.ZERO) == 0 ? currentAmount : currentAmount.divide(quantity, this.getRoundingMode());
    }

    private boolean checkTaxMapping(HashMap priceReductionHM, Integer taxId) {
        String discountTaxIds = CommonAPI.convertModelDetailToString(priceReductionHM.get("taxIds"));

        ArrayList<String> discountTaxIdArr = CommonAPI.convertCommaSeparatedStringToArrayList(discountTaxIds, true);

        return discountTaxIdArr.contains(taxId.toString());
    }

    //return the taxable amount for the given discount
    public BigDecimal getTaxableAmount(Integer taxId) {
        boolean considerModifiersOrPrep = (this.getModifiers() != null && this.getModifiers().size() > 0) || ( this.getPrepOption() != null );
        return getCurrentPrice(true, considerModifiersOrPrep, null, taxId);
    }

    public void incrementQuantity() {
        this.setQuantity(this.getQuantity().add(BigDecimal.ONE));
    }

    public void decrementQuantity() throws Exception {
        if ( this.getQuantity().compareTo( BigDecimal.ZERO ) == 0 ) {
            throw new MissingDataException("Could not decrement quantity of product, already at 0");
        }

        this.setQuantity(this.getQuantity().subtract(BigDecimal.ONE));
    }

    //increments the count of quantity used in this combo
    public void incrementQuantityInCombo() {
        setQuantityInCombos(this.getQuantityInCombos().add(BigDecimal.ONE));
    }

    public void decrementQuantityInCombo() throws MissingDataException {
        if ( this.getQuantityInCombos().compareTo( BigDecimal.ZERO ) == 0 ) {
            throw new MissingDataException("Could not decrement quantity of product, already at 0");
        }

        setQuantityInCombos( this.getQuantityInCombos().subtract(BigDecimal.ONE) );
    }

    //checks if the quantiy in combos is the same as the quantity overall
    public boolean allQuantityInCombos() {
        return ( getQuantity().compareTo( getQuantityInCombos() ) == 0 );
    }

    //returns the number of a times a product is used for this type of combo
    public BigDecimal getComboOccurrences(Integer comboID) {
        return BigDecimal.valueOf(Collections.frequency(this.getComboOccurrences(), comboID));
    }

    //returns the number of other combos that are using this product besides the given combo
    public BigDecimal getOtherCombosUsingProductCount(Integer currentComboID) {
        ArrayList<Integer> foundComboIds = new ArrayList<>();
        for(Integer comboID: this.getComboOccurrences()) {
            if(!currentComboID.equals(comboID) && !foundComboIds.contains(comboID)) {
                foundComboIds.add(comboID);
            }
        }

        return BigDecimal.valueOf(foundComboIds.size());
    }

    //check if the productLineItemModel has the same modifiers as the productCalculation
    public boolean areModifiersValid(ProductLineItemModel productLineItemModel) {
        if(this.getModifiers() == null && productLineItemModel.getModifiers().size() == 0) {
            return true;
        }

        if(this.getModifiers() == null && productLineItemModel.getModifiers().size() > 0) {
            return false;
        }

        if(this.getModifiers().size() != productLineItemModel.getModifiers().size()) {
            return false;
        }

        boolean foundAllModifiers = true;
        for(ModifierCalculation modifierCalculation : this.getModifiers()) {

            boolean foundMod = false;
            for(ModifierLineItemModel modifierLineItemModel : productLineItemModel.getModifiers()) {

                if(modifierLineItemModel.getProduct().getId().equals(modifierCalculation.getId())) {
                    foundMod = true;
                    break;
                }
            }

            if(!foundMod) {
                foundAllModifiers = false;
                break;
            }
        }
        return foundAllModifiers;
    }

    //check if the productLineItemModel has the same prep option as the productCalculation
    public boolean arePrepOptionsValid(ProductLineItemModel productLineItemModel) {
        if(this.getPrepOption() == null && productLineItemModel.getPrepOption() == null) {
            return true;
        } else if (this.getPrepOption() != null && productLineItemModel.getPrepOption() != null && this.getPrepOption().getId().equals(productLineItemModel.getPrepOption().getId())) {
            return true;
        }

        return false;
    }

    //returns the number of product line items using the same productCalculation based on productID and modifiers
    public Integer countMatchingProductLineItems(List<ProductLineItemModel> productLineItemModelList) {

        Integer productLineItemCount = 0;
        for(ProductLineItemModel productLineItem : productLineItemModelList) {

            //if the productLineItem has the same itemID, the same ProductModel.id and the same modifiers as the ProductCalculation
            if(productLineItem.getItemId().equals( this.getId() ) && productLineItem.getProduct().getId().equals(this.getId()) && this.areModifiersValid(productLineItem)) {
                productLineItemCount ++;
            }
        }

        return productLineItemCount;
    }

    //if duplicate product calculation lines are found check all other combo details in the combo to see if they're a match, if so add the duplicate line's quantity to the current productCalculation line
    // Ex: If you order Wings and a Cheese Pizza and then Wings and a Pepperoni Pizza. And you have a Pizza + Wings combo
    // The Wings are the same products and using the same combo Detail ID but the Cheese Pizza and Pepperoni Pizza are not the same product even though they use the same combo detail ID, so not a duplicate
    public boolean isProductPartOfDuplicateCombo(List<ProductCalculation> productCalculationList, ProductCalculation productToCompare) {
        boolean foundAllDetails = false;

        if(productToCompare.getComboDetailId() != null && productToCompare.getComboDetailId().equals(this.getComboDetailId()) && productToCompare.getId().equals(this.getId())) {

            //get all of the other combo details in the this product's combo
            List<ProductCalculation> otherComboDetailsOfProduct = new ArrayList<>();
            for(ProductCalculation productCalculationThis : productCalculationList) {

                //look for another product that has the same comboTransLineItemID but different comboDetailID
                if(productCalculationThis.getComboTransLineItemId() != null && productCalculationThis.getComboTransLineItemId().equals(this.getComboTransLineItemId()) && !productCalculationThis.getComboDetailId().equals(this.getComboDetailId())) {
                    otherComboDetailsOfProduct.add(productCalculationThis);
                }
            }

            //get all of the other combo details in the product's combo that you're checking against
            List<ProductCalculation> otherComboDetailsOfCompareProduct = new ArrayList<>();
            for(ProductCalculation productCalculationCompare : productCalculationList) {

                //look for another product that has the same comboTransLineItemID but different comboDetailID
                if(productCalculationCompare.getComboTransLineItemId() != null && productCalculationCompare.getComboTransLineItemId().equals(productToCompare.getComboTransLineItemId()) && !productCalculationCompare.getComboDetailId().equals(productToCompare.getComboDetailId())) {
                    otherComboDetailsOfCompareProduct.add(productCalculationCompare);
                }
            }

            //check if the combo details are an exact match for both products
            for(ProductCalculation productCalculationThis: otherComboDetailsOfProduct) {

                boolean found = false;
                for(ProductCalculation productCalculationCompare: otherComboDetailsOfCompareProduct) {

                    if(productCalculationThis.getComboDetailId().equals(productCalculationCompare.getComboDetailId()) && productCalculationThis.getId().equals(productCalculationCompare.getId())) {
                        found = true;
                        break;
                    }
                }
                if(!found) {
                    foundAllDetails = false;
                    break;
                }
            }

        }

        return foundAllDetails;
    }

    /**
     * For Sale transactions check that the product amount is greater than 0
     * For Refund transactions check that the product amount is less than 0
     * @param transactionTypeEnum
     * @param amount
     * @return
     */
    private boolean productAmountCheck(PosAPIHelper.TransactionType transactionTypeEnum, BigDecimal amount) {
        boolean result = false;

        switch (transactionTypeEnum) {
            case REFUND:
                result = amount.compareTo(BigDecimal.ZERO) < 0;
                break;
            default:
                result = amount.compareTo(BigDecimal.ZERO) > 0;
                break;
        }

        return result;
    }

    private BigDecimal getDqrDiscountableAmount(BigDecimal price, BigDecimal quantity, BigDecimal currentAmount, BigDecimal discountAppliedQuantity){
        BigDecimal remainingQtyForDiscount = getQuantityOriginal().subtract(discountAppliedQuantity);

        if (remainingQtyForDiscount.compareTo(BigDecimal.ZERO) == 0){
            //if all quantities were discounted previously
            currentAmount = BigDecimal.ZERO;
        } else if (remainingQtyForDiscount.compareTo(quantity) >= 0){
            //remainingQtyForDiscount covers the entire dqr
            //override the currentAmount to the price * the discount quantity restriction
            currentAmount = price.multiply(quantity).setScale(4, this.getRoundingMode());
        } else if (remainingQtyForDiscount.compareTo(quantity) < 0){
            //remainingQty doesn't cover the entire quantity, only part of it
            BigDecimal discountableQuantity = quantity.subtract(remainingQtyForDiscount);
            currentAmount = price.multiply(discountableQuantity);
        }

        return currentAmount;
    }

    //getters and setters
    public ProductModel getProductModel() {
        return productModel;
    }

    public void setProductModel(ProductModel productModel) {
        this.productModel = productModel;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;

        //update the quantity for the attached modifiers
        if ( this.getModifiers() != null && this.getModifiers().size() > 0 ) {
            for ( ModifierCalculation modifierCalculation : this.getModifiers() ) {
                modifierCalculation.setQuantity( quantity );
            }
        }
    }

    public List<ModifierCalculation> getModifiers() {
        return modifiers;
    }

    public void setModifiers(List<ModifierCalculation> modifiers) {
        this.modifiers = modifiers;
    }

    public BigDecimal getLinePrice() {
        return linePrice;
    }

    public void setLinePrice(BigDecimal linePrice) {
        this.linePrice = linePrice;
    }

    public List<HashMap> getDiscountsApplied() {
        return discountsApplied;
    }

    public void setDiscountsApplied(List<HashMap> discountsApplied) {
        this.discountsApplied = discountsApplied;
    }

    public List<HashMap> getRewardsApplied() {
        return rewardsApplied;
    }

    public void setRewardsApplied(List<HashMap> rewardsApplied) {
        this.rewardsApplied = rewardsApplied;
    }

    public int getRoundingMode() {
        return roundingMode;
    }

    public void setRoundingMode(int roundingMode) {
        this.roundingMode = roundingMode;
    }

    public List<HashMap> getDiscountTaxes() {
        return discountTaxes;
    }

    public void setDiscountTaxes(List<HashMap> discountTaxes) {
        this.discountTaxes = discountTaxes;
    }

    public List<HashMap> getRewardTaxes() {
        return rewardTaxes;
    }

    public void setRewardTaxes(List<HashMap> rewardTaxes) {
        this.rewardTaxes = rewardTaxes;
    }

    public Integer getComboTransLineItemId() {
        return comboTransLineItemId;
    }

    public void setComboTransLineItemId(Integer comboTransLineItemId) {
        this.comboTransLineItemId = comboTransLineItemId;
    }

    public Integer getComboDetailId() {
        return comboDetailId;
    }

    public void setComboDetailId(Integer comboDetailId) {
        this.comboDetailId = comboDetailId;
    }

    public BigDecimal getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(BigDecimal basePrice) {
        this.basePrice = basePrice;
    }

    public BigDecimal getComboPrice() {
        return comboPrice;
    }

    public void setComboPrice(BigDecimal comboPrice) {
        this.comboPrice = comboPrice;
    }

    public BigDecimal getUpcharge() {
        return upcharge;
    }

    public void setUpcharge(BigDecimal upcharge) {
        this.upcharge = upcharge;
    }

    public BigDecimal getQuantityInCombos() {
        return quantityInCombos;
    }

    public void setQuantityInCombos(BigDecimal quantityInCombos) {
        this.quantityInCombos = quantityInCombos;
    }

    public Integer getOriginalIndex() {
        return originalIndex;
    }

    public void setOriginalIndex(Integer originalIndex) {
        this.originalIndex = originalIndex;
    }

    public List<Integer> getComboOccurrences() {
        return comboOccurrences;
    }

    public void setComboOccurrences(List<Integer> comboOccurrences) {
        this.comboOccurrences = comboOccurrences;
    }

    public PosAPIHelper.TransactionType getTransactionTypeEnum() {
        return transactionTypeEnum;
    }

    public void setTransactionTypeEnum(PosAPIHelper.TransactionType transactionTypeEnum) {
        this.transactionTypeEnum = transactionTypeEnum;
    }

    public BigDecimal getQuantityOriginal() {
        return quantityOriginal;
    }

    public void setQuantityOriginal(BigDecimal quantityOriginal) {
        this.quantityOriginal = quantityOriginal;
    }

    public PrepOptionModel getPrepOption() {
        return prepOption;
    }

    public void setPrepOption(PrepOptionModel prepOption) {
        this.prepOption = prepOption;
    }

    public Integer getTransactionItemNum() {
        return transactionItemNum;
    }

    public void setTransactionItemNum(Integer transactionItemNum) {
        this.transactionItemNum = transactionItemNum;
    }

    /**
     * Overridden toString method for a ProductCalculation.
     * @return The {@link String} representation of a ProductCalculation.
     */
    @Override
    public String toString () {

        String modifiersStr = "";
        if (!DataFunctions.isEmptyCollection(modifiers)) {
            modifiersStr += "[";
            for (ModifierCalculation modifierCalculation : modifiers) {
                if (modifierCalculation != null && modifierCalculation.equals(modifiers.get(modifiers.size() - 1))) {
                    modifiersStr += "[" + Collections.singletonList(modifierCalculation).toString() + "]";
                }
                else if (modifierCalculation != null && !modifierCalculation.equals(modifiers.get(modifiers.size() - 1))) {
                    modifiersStr += "[" + Collections.singletonList(modifierCalculation).toString() + "]; ";
                }
            }
            modifiersStr += "]";
        }

        String discountsAppliedStr = "";
        if (!DataFunctions.isEmptyCollection(discountsApplied)) {
            discountsAppliedStr += "[";
            for (HashMap discountAppliedHM : discountsApplied) {
                if (!DataFunctions.isEmptyMap(discountAppliedHM) && discountAppliedHM.equals(discountsApplied.get(discountsApplied.size() - 1))) {
                    discountsAppliedStr += "[" + Collections.singletonList(discountAppliedHM).toString() + "]";
                }
                else if (!DataFunctions.isEmptyMap(discountAppliedHM) && !discountAppliedHM.equals(discountsApplied.get(discountsApplied.size() - 1))) {
                    discountsAppliedStr += "[" + Collections.singletonList(discountAppliedHM).toString() + "]; ";
                }
            }
            discountsAppliedStr += "]";
        }

        String discountTaxesStr = "";
        if (!DataFunctions.isEmptyCollection(discountTaxes)) {
            discountTaxesStr += "[";
            for (HashMap discountTaxHM : discountTaxes) {
                if (!DataFunctions.isEmptyMap(discountTaxHM) && discountTaxHM.equals(discountTaxes.get(discountTaxes.size() - 1))) {
                    discountTaxesStr += "[" + Collections.singletonList(discountTaxHM).toString() + "]";
                }
                else if (!DataFunctions.isEmptyMap(discountTaxHM) && !discountTaxHM.equals(discountTaxes.get(discountTaxes.size() - 1))) {
                    discountTaxesStr += "[" + Collections.singletonList(discountTaxHM).toString() + "]; ";
                }
            }
            discountTaxesStr += "]";
        }

        String rewardsAppliedStr = "";
        if (!DataFunctions.isEmptyCollection(rewardsApplied)) {
            rewardsAppliedStr += "[";
            for (HashMap rewardAppliedHM : rewardsApplied) {
                if (!DataFunctions.isEmptyMap(rewardAppliedHM) && rewardAppliedHM.equals(rewardsApplied.get(rewardsApplied.size() - 1))) {
                    rewardsAppliedStr += "[" + Collections.singletonList(rewardAppliedHM).toString() + "]";
                }
                else if (!DataFunctions.isEmptyMap(rewardAppliedHM) && !rewardAppliedHM.equals(rewardsApplied.get(rewardsApplied.size() - 1))) {
                    rewardsAppliedStr += "[" + Collections.singletonList(rewardAppliedHM).toString() + "]; ";
                }
            }
            rewardsAppliedStr += "]";
        }

        String rewardTaxesStr = "";
        if (!DataFunctions.isEmptyCollection(rewardTaxes)) {
            rewardTaxesStr += "[";
            for (HashMap rewardTaxHM : rewardTaxes) {
                if (!DataFunctions.isEmptyMap(rewardTaxHM) && rewardTaxHM.equals(rewardTaxes.get(rewardTaxes.size() - 1))) {
                    rewardTaxesStr += "[" + Collections.singletonList(rewardTaxHM).toString() + "]";
                }
                else if (!DataFunctions.isEmptyMap(rewardTaxHM) && !rewardTaxHM.equals(rewardTaxes.get(rewardTaxes.size() - 1))) {
                    rewardTaxesStr += "[" + Collections.singletonList(rewardTaxHM).toString() + "]; ";
                }
            }
            rewardTaxesStr += "]";
        }

        String comboOccurrencesStr = "";
        if (!DataFunctions.isEmptyCollection(comboOccurrences)) {
            comboOccurrencesStr = StringFunctions.buildStringFromList(comboOccurrences, ",");
        }

        return String.format("ID: %s, INDEX: %s, PRODUCTMODEL: %s, LINEPRICE: %s, QUANTITY: %s, QUANTITYORIGINAL: %s, " +
                "MODIFIERS: %s, DISCOUNTSAPPLIED: %s, DISCOUNTTAXES: %s, REWARDSAPPLIED: %s, REWARDTAXES: %s, " +
                "COMBOOCCURRENCES: %s, COMBOTRANSLINEITEMID: %s, COMBODETAILID: %s, BASEPRICE: %s, COMBOPRICE: %s, " +
                "UPCHARGE: %s, QUANTITYINCOMBOS: %s, ORIGINALINDEX: %s, ROUNDINGMODE: %s, TRANSACTIONTYPEENUM: %s, " +
                "PREPOPTION: %s",
                Objects.toString((id != null && id > 0 ? id : "N/A"), "N/A"),
                Objects.toString((index != null && index > 0 ? index : "N/A"), "N/A"),
                Objects.toString((productModel != null ? productModel.toString() : "N/A"), "N/A"),
                Objects.toString((linePrice != null ? linePrice.toPlainString() : "N/A"), "N/A"),
                Objects.toString((quantity != null ? quantity.toPlainString() : "N/A"), "N/A"),
                Objects.toString((quantityOriginal != null ? quantityOriginal.toPlainString() : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(modifiersStr) ? modifiersStr : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(discountsAppliedStr) ? discountsAppliedStr : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(discountTaxesStr) ? discountTaxesStr : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(rewardsAppliedStr) ? rewardsAppliedStr : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(rewardTaxesStr) ? rewardTaxesStr : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(comboOccurrencesStr) ? comboOccurrencesStr : "N/A"), "N/A"),
                Objects.toString((comboTransLineItemId != null && comboTransLineItemId > 0 ? comboTransLineItemId : "N/A"), "N/A"),
                Objects.toString((comboDetailId != null && comboDetailId > 0 ? comboDetailId : "N/A"), "N/A"),
                Objects.toString((basePrice != null ? basePrice.toPlainString() : "N/A"), "N/A"),
                Objects.toString((comboPrice != null ? comboPrice.toPlainString() : "N/A"), "N/A"),
                Objects.toString((upcharge != null ? upcharge.toPlainString() : "N/A"), "N/A"),
                Objects.toString((quantityInCombos != null ? quantityInCombos.toPlainString() : "N/A"), "N/A"),
                Objects.toString((originalIndex != null && originalIndex > 0 ? originalIndex : "N/A"), "N/A"),
                Objects.toString(roundingMode, "N/A"),
                Objects.toString((transactionTypeEnum != null ? transactionTypeEnum.getName() : "N/A"), "N/A"),
                Objects.toString((prepOption != null ? prepOption.toString() : "N/A"), "N/A"));

    }

}
