package com.mmhayes.common.product.models;

//other dependencies
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.StringFunctions;

import java.math.BigDecimal;
import java.util.*;

/*
Last Updated (automatically updated by SVN)
$Author: nyu $: Author of last commit
$Date: 2021-08-17 16:55:44 -0400 (Tue, 17 Aug 2021) $: Date of last commit
$Rev: 14953 $: Revision of last commit
*/
public class ModifierCalculation {
    private Integer id; //PAPLUID
    private Integer modIndex = null; //keep track of the order of the products
    private BigDecimal quantity = BigDecimal.ZERO;
    private ProductModel productModel; //contains all of the product properties
    private List<HashMap> discountsApplied = new ArrayList<HashMap>();
    private List<HashMap> discountTaxes = new ArrayList<HashMap>(); //holds tax related information for discounts
    private List<HashMap> rewardsApplied = new ArrayList<HashMap>();
    private List<HashMap> rewardTaxes = new ArrayList<HashMap>(); //holds tax related information for rewards
    private Integer transactionItemNum = null;

    private PrepOptionModel prepOption = null;

    public ModifierCalculation() {

    }

    public ModifierCalculation( ModifierCalculation modifierCalculationReference ) {
        this.setId( modifierCalculationReference.getId() );
        this.setModIndex(modifierCalculationReference.getModIndex());
        this.setQuantity(modifierCalculationReference.getQuantity());
        this.setProductModel( modifierCalculationReference.getProductModel() );
        this.setTransactionItemNum(modifierCalculationReference.getTransactionItemNum());

        if(modifierCalculationReference.getPrepOption() != null) {
            this.setPrepOption( modifierCalculationReference.getPrepOption() );
        }
    }

    public void recordPriceReduction(boolean isDiscount, HashMap priceReductionHM) {
        if ( isDiscount ) {
            this.getDiscountsApplied().add(priceReductionHM);
        } else {
            this.getRewardsApplied().add(priceReductionHM);
        }

        //apply itemTaxModel building helper data - ignore if no valid taxes
        if ( priceReductionHM.get("taxIds").toString().equals("") ) {
            return;
        }

        ArrayList<String> taxIds = CommonAPI.convertCommaSeparatedStringToArrayList( priceReductionHM.get("taxIds").toString(), true );

        for ( String taxId : taxIds ) {
            if ( !this.productModel.isTaxValidForProduct(Integer.parseInt(taxId)) ) {
                continue;
            }

            HashMap taxHM = new HashMap();
            taxHM.put("id", priceReductionHM.get(isDiscount ? "discountId" : "rewardId"));
            taxHM.put("taxId", taxId);
            taxHM.put("amount", priceReductionHM.get("amount"));

            if ( isDiscount ) {
                this.getDiscountTaxes().add(taxHM);
            } else {
                this.getRewardTaxes().add(taxHM);
            }
        }
    }

    public BigDecimal getCurrentPrice(boolean getExtendedAmount, Integer taxId) {
        BigDecimal price = this.getProductModel().getPrice();

        if(this.getPrepOption() != null) {
            price = price.add(this.getPrepOption().getPrice());
        }

        if ( getExtendedAmount ) {
            price = price.multiply( this.getQuantity() );
        }

        if ( this.getDiscountsApplied().size() > 0 ) {
            for ( HashMap discountAppliedHM : this.getDiscountsApplied() ) {
                BigDecimal discountAmount = CommonAPI.convertModelDetailToBigDecimal(discountAppliedHM.get("amount"));

                //if looking for a current price, or a discountable amount, or if looking for a taxable amount and this discount is mapped to the tax, subtract the discount
                if ( taxId == null || checkTaxMapping(discountAppliedHM, taxId) ) {
                    price = price.subtract(discountAmount);
                }
            }
        }

        if ( this.getRewardsApplied().size() > 0 ) {
            for ( HashMap rewardAppliedHM : this.getRewardsApplied() ) {
                boolean isSingleQuantity = CommonAPI.convertModelDetailToBoolean(rewardAppliedHM.get("isSingleQuantity"));

                //if it is a free product reward, or any other type that is applied to just a single quantity
                if ( !getExtendedAmount && isSingleQuantity ) {
                    continue;
                }

                BigDecimal rewardAmount = CommonAPI.convertModelDetailToBigDecimal(rewardAppliedHM.get("amount"));

                //if looking for a current price, or a discountable amount, or if looking for a taxable amount and this reward is mapped to the tax, subtract the reward
                if ( taxId == null || checkTaxMapping(rewardAppliedHM, taxId) ) {
                    price = price.subtract(rewardAmount);
                }
            }
        }

        return price;
    }

    private boolean checkTaxMapping(HashMap priceReductionHM, Integer taxId) {
        String discountTaxIds = CommonAPI.convertModelDetailToString(priceReductionHM.get("taxIds"));

        ArrayList<String> discountTaxIdArr = CommonAPI.convertCommaSeparatedStringToArrayList(discountTaxIds, true);

        return discountTaxIdArr.contains(taxId.toString());
    }

    //getters and setters
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getModIndex() {
        return modIndex;
    }

    public void setModIndex(Integer modIndex) {
        this.modIndex = modIndex;
    }

    public ProductModel getProductModel() {
        return productModel;
    }

    public void setProductModel(ProductModel productModel) {
        this.productModel = productModel;
    }

    public List<HashMap> getDiscountsApplied() {
        return discountsApplied;
    }

    public void setDiscountsApplied(List<HashMap> discountsApplied) {
        this.discountsApplied = discountsApplied;
    }

    public List<HashMap> getRewardsApplied() {
        return rewardsApplied;
    }

    public void setRewardsApplied(List<HashMap> rewardsApplied) {
        this.rewardsApplied = rewardsApplied;
    }


    public List<HashMap> getDiscountTaxes() {
        return discountTaxes;
    }

    public void setDiscountTaxes(List<HashMap> discountTaxes) {
        this.discountTaxes = discountTaxes;
    }

    public List<HashMap> getRewardTaxes() {
        return rewardTaxes;
    }

    public void setRewardTaxes(List<HashMap> rewardTaxes) {
        this.rewardTaxes = rewardTaxes;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public PrepOptionModel getPrepOption() {
        return prepOption;
    }

    public void setPrepOption(PrepOptionModel prepOption) {
        this.prepOption = prepOption;
    }

    public Integer getTransactionItemNum() {
        return transactionItemNum;
    }

    public void setTransactionItemNum(Integer transactionItemNum) {
        this.transactionItemNum = transactionItemNum;
    }

    /**
     * Overridden toString method for a ModifierCalculation.
     * @return The {@link String} representation of a ModifierCalculation.
     */
    @Override
    public String toString () {

        String discountsAppliedStr = "";
        if (!DataFunctions.isEmptyCollection(discountsApplied)) {
            discountsAppliedStr += "[";
            for (HashMap discountAppliedHM : discountsApplied) {
                if (!DataFunctions.isEmptyMap(discountAppliedHM) && discountAppliedHM.equals(discountsApplied.get(discountsApplied.size() - 1))) {
                    discountsAppliedStr += "[" + Collections.singletonList(discountAppliedHM).toString() + "]";
                }
                else if (!DataFunctions.isEmptyMap(discountAppliedHM) && !discountAppliedHM.equals(discountsApplied.get(discountsApplied.size() - 1))) {
                    discountsAppliedStr += "[" + Collections.singletonList(discountAppliedHM).toString() + "]; ";
                }
            }
            discountsAppliedStr += "]";
        }

        String discountTaxesStr = "";
        if (!DataFunctions.isEmptyCollection(discountTaxes)) {
            discountTaxesStr += "[";
            for (HashMap discountTaxHM : discountTaxes) {
                if (!DataFunctions.isEmptyMap(discountTaxHM) && discountTaxHM.equals(discountTaxes.get(discountTaxes.size() - 1))) {
                    discountTaxesStr += "[" + Collections.singletonList(discountTaxHM).toString() + "]";
                }
                else if (!DataFunctions.isEmptyMap(discountTaxHM) && !discountTaxHM.equals(discountTaxes.get(discountTaxes.size() - 1))) {
                    discountTaxesStr += "[" + Collections.singletonList(discountTaxHM).toString() + "]; ";
                }
            }
            discountTaxesStr += "]";
        }
        
        String rewardsAppliedStr = "";
        if (!DataFunctions.isEmptyCollection(rewardsApplied)) {
            rewardsAppliedStr += "[";
            for (HashMap rewardAppliedHM : rewardsApplied) {
                if (!DataFunctions.isEmptyMap(rewardAppliedHM) && rewardAppliedHM.equals(rewardsApplied.get(rewardsApplied.size() - 1))) {
                    rewardsAppliedStr += "[" + Collections.singletonList(rewardAppliedHM).toString() + "]";
                }
                else if (!DataFunctions.isEmptyMap(rewardAppliedHM) && !rewardAppliedHM.equals(rewardsApplied.get(rewardsApplied.size() - 1))) {
                    rewardsAppliedStr += "[" + Collections.singletonList(rewardAppliedHM).toString() + "]; ";
                }
            }
            rewardsAppliedStr += "]";
        }
        
        String rewardTaxesStr = "";
        if (!DataFunctions.isEmptyCollection(rewardTaxes)) {
            rewardTaxesStr += "[";
            for (HashMap rewardTaxHM : rewardTaxes) {
                if (!DataFunctions.isEmptyMap(rewardTaxHM) && rewardTaxHM.equals(rewardTaxes.get(rewardTaxes.size() - 1))) {
                    rewardTaxesStr += "[" + Collections.singletonList(rewardTaxHM).toString() + "]";
                }
                else if (!DataFunctions.isEmptyMap(rewardTaxHM) && !rewardTaxHM.equals(rewardTaxes.get(rewardTaxes.size() - 1))) {
                    rewardTaxesStr += "[" + Collections.singletonList(rewardTaxHM).toString() + "]; ";
                }
            }
            rewardTaxesStr += "]";
        }

        return String.format("ID: %s, MODINDEX: %s, QUANTITY: %s, PRODUCTMODEL: %s, DISCOUNTSAPPLIED: %s, DISCOUNTTAXES: %s, REWARDSAPPLIED: %s, REWARDTAXES: %s, PREPOPTION: %s",
                Objects.toString((id != null && id > 0 ? id : "N/A"), "N/A"),
                Objects.toString((modIndex != null && modIndex > 0 ? modIndex : "N/A"), "N/A"),
                Objects.toString((quantity != null ? quantity.toPlainString() : "N/A"), "N/A"),
                Objects.toString((productModel != null ? productModel.toString() : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(discountsAppliedStr) ? discountsAppliedStr : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(discountTaxesStr) ? discountTaxesStr : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(rewardsAppliedStr) ? rewardsAppliedStr : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(rewardTaxesStr) ? rewardTaxesStr : "N/A"), "N/A"),
                Objects.toString((prepOption != null ? prepOption.toString() : "N/A"), "N/A"));

    }

}
