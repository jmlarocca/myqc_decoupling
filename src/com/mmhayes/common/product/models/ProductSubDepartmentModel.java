package com.mmhayes.common.product.models;

//mmhayes dependencies
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.dataaccess.DataManager;

//API dependencies
import com.mmhayes.common.api.CommonAPI;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionDeserializer;

//other dependencies
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2019-07-02 08:12:14 -0400 (Tue, 02 Jul 2019) $: Date of last commit
 $Rev: 9024 $: Revision of last commit
*/
public class ProductSubDepartmentModel {

    Integer subDepartmentId = null;
    String subDepartmentName = "";

    Integer departmentId = null;
    String departmentName = "";

    public ProductSubDepartmentModel() {

    }

    public ProductSubDepartmentModel(HashMap modelHM) {

          setModelProperties(modelHM);

    }

    //setter for all of this model's properties
    public ProductSubDepartmentModel setModelProperties(HashMap modelDetailHM) {
        setSubDepartmentId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("SUBDEPARTMENTID")));
        setSubDepartmentName(CommonAPI.convertModelDetailToString(modelDetailHM.get("SUBDEPARTMENTNAME")));

        setDepartmentId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("DEPARTMENTID")));
        setDepartmentName(CommonAPI.convertModelDetailToString(modelDetailHM.get("DEPARTMENTNAME")));

        return this;
    }

    public static List<ProductSubDepartmentModel> getAllProductSubDepartments(Integer terminalId){
        List<ProductSubDepartmentModel> productSubDepartmentModelList = new ArrayList<>();

        DataManager dm = new DataManager();

        ArrayList loyaltyProgramList = new ArrayList();

        //get all models in an array list
        ArrayList<HashMap> subDepartmentHMList = dm.parameterizedExecuteQuery("data.posapi30.getAllProductSubDepartments",
                new Object[]{ },
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        //product list is null or empty, throw exception
        //CommonAPI.checkIsNullOrEmptyList(ProductList, "Product List is empty");
        for (HashMap modelHM : subDepartmentHMList) {
            //CommonAPI.checkIsNullOrEmptyObject(productHM.get("ID"), "Product Id could not be found");
            //create a new model and add to the collection
            productSubDepartmentModelList.add(new ProductSubDepartmentModel(modelHM));
        }

        return productSubDepartmentModelList;
    }

    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public Integer getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }

    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getSubDepartmentName() {
        return subDepartmentName;
    }

    public void setSubDepartmentName(String subDepartmentName) {
        this.subDepartmentName = subDepartmentName;
    }

    public Integer getSubDepartmentId() {
        return subDepartmentId;
    }

    public void setSubDepartmentId(Integer subDepartmentId) {
        this.subDepartmentId = subDepartmentId;
    }


}
