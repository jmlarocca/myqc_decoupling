package com.mmhayes.common.product.models;

//MMHayes Dependencies

import com.fasterxml.jackson.annotation.JsonInclude;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.transaction.models.SurchargeDisplayModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

//API Dependencies
//MMHayes Dependencies

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2018-02-12 15:42:40 -0500 (Mon, 12 Feb 2018) $: Date of last commit
 $Rev: 6366 $: Revision of last commit
*/
public class ItemSurchargeModel {
    private Integer id;
    private Integer transLineItemId;
    private Integer itemTypeId;
    private Integer itemId;

    private BigDecimal eligibleAmount = BigDecimal.ZERO;
    private BigDecimal amount;
    private BigDecimal refundedEligibleAmount;
    private BigDecimal refundedAmount;
    private SurchargeDisplayModel surcharge;

    public ItemSurchargeModel(){

    }

    public ItemSurchargeModel(HashMap itemTaxDiscountHM) {
        setModelProperties(itemTaxDiscountHM);
    }

    public static ItemSurchargeModel createItemSurchargeModel(HashMap itemSurchargeHM) {
        return new ItemSurchargeModel(itemSurchargeHM);
    }

    //setter for all of this model's properties
    public ItemSurchargeModel setModelProperties(HashMap modelDetailHM) {
        setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));

        setTransLineItemId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("TRANSLINEITEMID")));
        setItemTypeId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ITEMTYPEID")));
        setItemId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ITEMID")));

        setAmount(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("AMOUNT")));
        setEligibleAmount(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("ELIGIBLEAMOUNT")));
        setRefundedEligibleAmount(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("REFUNDEDELIGIBLEAMOUNT")));
        setRefundedAmount(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("REFUNDEDAMOUNT")));

        return this;
    }

    public static ItemSurchargeModel getItemSurchargeModel(Integer itemTaxDiscountId, Integer terminalId) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> itemSurchargeList = dm.parameterizedExecuteQuery("data.posapi30.getOneItemDiscountById",
                new Object[]{itemTaxDiscountId},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        //product list is null or empty, throw exception
        CommonAPI.checkIsNullOrEmptyList(itemSurchargeList, "Item Surcharge Not Found", terminalId);
        return ItemSurchargeModel.createItemSurchargeModel(itemSurchargeList.get(0));

    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getTransLineItemId() {
        return transLineItemId;
    }

    public void setTransLineItemId(Integer transLineItemId) {
        this.transLineItemId = transLineItemId;
    }

    public Integer getItemTypeId() {

        if (itemTypeId == null) {
            this.itemTypeId = PosAPIHelper.ObjectType.SURCHARGE.toInt();
        }
        return this.itemTypeId;
    }

    public void setItemTypeId(Integer itemTypeId) {
        this.itemTypeId = itemTypeId;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public BigDecimal getEligibleAmount() {
        return eligibleAmount;
    }

    public void setEligibleAmount(BigDecimal eligibleAmount) {
        this.eligibleAmount = eligibleAmount;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public BigDecimal getRefundedEligibleAmount() {
        return refundedEligibleAmount;
    }

    public void setRefundedEligibleAmount(BigDecimal refundedEligibleAmount) {
        this.refundedEligibleAmount = refundedEligibleAmount;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public BigDecimal getRefundedAmount() {
        return refundedAmount;
    }

    public void setRefundedAmount(BigDecimal refundedAmount) {
        this.refundedAmount = refundedAmount;
    }

    public SurchargeDisplayModel getSurcharge() {
        return surcharge;
    }

    public void setSurcharge(SurchargeDisplayModel surcharge) {
        this.surcharge = surcharge;
    }

    /**
     * Overridden toString method for an ItemSurchargeModel.
     * @return The {@link String} representation of an ItemSurchargeModel.
     */
    @Override
    public String toString () {

        return String.format("ID: %s, TRANSLINEITEMID: %s, ITEMTYPEID: %s, ITEMID: %s, ELIGIBLEAMOUNT: %s, AMOUNT: %s, REFUNDEDELIGIBLEAMOUNT: %s, REFUNDEDAMOUNT: %s, SURCHARGE: %s",
                Objects.toString((id != null && id > 0 ? id : "N/A"), "N/A"),
                Objects.toString((transLineItemId != null && transLineItemId > 0 ? transLineItemId : "N/A"), "N/A"),
                Objects.toString((itemTypeId != null && itemTypeId > 0 ? itemTypeId : "N/A"), "N/A"),
                Objects.toString((itemId != null && itemId > 0 ? itemId : "N/A"), "N/A"),
                Objects.toString((eligibleAmount != null ? eligibleAmount.toPlainString() : "N/A"), "N/A"),
                Objects.toString((amount != null ? amount.toPlainString() : "N/A"), "N/A"),
                Objects.toString((refundedEligibleAmount != null ? refundedEligibleAmount.toPlainString() : "N/A"), "N/A"),
                Objects.toString((refundedAmount != null ? refundedAmount.toPlainString() : "N/A"), "N/A"),
                Objects.toString((surcharge != null ? surcharge.toString() : "N/A"), "N/A"));
    }

}
