package com.mmhayes.common.product.models;

//mmhayes dependencies

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionDeserializer;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.loyalty.collections.LoyaltyRewardCollection;
import com.mmhayes.common.loyalty.models.LoyaltyAccrualPolicyLevelModel;
import com.mmhayes.common.loyalty.models.LoyaltyRewardModel;
import com.mmhayes.common.terminal.models.TerminalModel;
import com.mmhayes.common.transaction.collections.SurchargeCollection;
import com.mmhayes.common.transaction.collections.TaxCollection;
import com.mmhayes.common.transaction.models.SurchargeModel;
import com.mmhayes.common.transaction.models.TaxModel;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

//API dependencies
//other dependencies
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2021-06-28 12:21:21 -0400 (Mon, 28 Jun 2021) $: Date of last commit
 $Rev: 14241 $: Revision of last commit
*/

@ApiModel(value = "ProductModel", description = "Model that represents a Product configured in the QC system.  Relates to the QC_PAPlu table.")
@JsonPropertyOrder({"id", "name", "description", "price", "productCode", "subDepartment", "department"})
public class ProductModel {
    private Integer id = null;
    private String name = "";
    private String description = "";
    private BigDecimal price = null;
    private String productCode = "";
    private String subDepartment = "";
    private String department = "";
    private boolean isWellness = false;
    private boolean isModifier = false;
    private boolean isWeighted = false;
    private boolean alreadyFetchedAccrualPolicyLevels = false;
    private boolean isDiningOption = false;
    private boolean inventoryItem = false;
    private boolean fetchedFromDb = false;

    private String discountIds = "";
    private String taxIds = "";
    private List<TaxModel> taxDeletes = new ArrayList<>();

    private Integer subDepartmentId = null;
    private Integer departmentId = null;

    private Integer mappedPosItem = null;
    private List<LoyaltyRewardModel> rewards = new ArrayList<>();
    private List<LoyaltyAccrualPolicyLevelModel> loyaltyAccrualPolicyLevels = new ArrayList<>();

    private List<SurchargeModel> surcharges = new ArrayList<>();

    //default constructor
    public ProductModel() {
    }

    //Constructor- takes in a Hashmap
    public ProductModel(HashMap ProductHM, Integer terminalId) throws Exception {
        setModelProperties(ProductHM);
        getSurchargesForProduct(terminalId);

        if(isDiningOption()) {
            getTaxDeletesForDiningOption(terminalId);
        }
    }

    //setter for all of this model's properties
    public ProductModel setModelProperties(HashMap modelDetailHM) {
        setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));
        setName(CommonAPI.convertModelDetailToString(modelDetailHM.get("NAME")));
        setDescription(CommonAPI.convertModelDetailToString(modelDetailHM.get("DESCRIPTION")));
        setPrice(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("PRICE")));
        setProductCode(CommonAPI.convertModelDetailToString(modelDetailHM.get("PLUCODE")));
        setSubDepartmentId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PASUBDEPTID")));
        setDepartment(CommonAPI.convertModelDetailToString(modelDetailHM.get("DEPARTMENTNAME")));
        setSubDepartment(CommonAPI.convertModelDetailToString(modelDetailHM.get("SUBDEPARTMENTNAME")));
        setWellness(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("WELLNESS"), false));
        setIsModifier(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("ISMODIFIER"), false));
        setIsWeighted(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("SCALEUSED"), false));
        setDiningOption(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("ISDININGOPTION"), false));
        setTaxIds(CommonAPI.convertModelDetailToString(modelDetailHM.get("TAXIDS")));
        setDiscountIds(CommonAPI.convertModelDetailToString(modelDetailHM.get("DISCOUNTIDS")));
        setInventoryItem(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("INVENTORYITEM"), false));
        setDepartmentId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PADEPARTMENTID")));

        setFetchedFromDb(true);

        return this;
    }

    private void getTaxDeletesForDiningOption(Integer terminalId) throws Exception {
        this.setTaxDeletes(TaxCollection.getTaxDeletesForDiningOption(this.getId(), terminalId).getCollection());
    }

    private void getRewardsForProduct(Integer terminalId) throws Exception {
        this.setRewards(LoyaltyRewardCollection.getRewardsForProductId(this.getId(), terminalId).getCollection());
    }

    private void getSurchargesForProduct(Integer terminalId) throws Exception {
        this.setSurcharges(SurchargeCollection.getSurchargesForProductId(this.getId(), terminalId).getCollection());
    }

    //returns true or false depending on if the product is mapped to the given discountId
    public boolean isDiscountValidForProduct(Integer discountId) {
        if ( this.getDiscountIds() == null || this.getDiscountIds().length() == 0 ) {
            return false;
        }

        ArrayList<String> productDiscountMappings = CommonAPI.convertCommaSeparatedStringToArrayList(this.getDiscountIds(), false);

        return ( productDiscountMappings.contains(discountId.toString()) );
    }

    //returns true or false depending on if the product is mapped to the given discountId
    public boolean isTaxValidForProduct(Integer taxId) {
        if ( this.getTaxIds() == null || this.getTaxIds().length() == 0 ) {
            return false;
        }

        ArrayList<String> productTaxMappings = CommonAPI.convertCommaSeparatedStringToArrayList(this.getTaxIds(), false);

        return ( productTaxMappings.contains(taxId.toString()) );
    }

    @JsonIgnore
    public String formatProductNameForLogging(){
        return (this.getName() == null) ? "" : " (" + this.getName().trim() + ")";

    }

    public static ProductModel createProductModel(HashMap modelDetailHM, Integer terminalId) throws Exception {
        ProductModel productModel = new ProductModel(modelDetailHM, terminalId);
        return productModel;
    }

    public static ProductModel getProductModelById(Integer productId, Integer terminalId) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> productList = dm.parameterizedExecuteQuery("data.posapi30.getOneProductById",
                new Object[]{productId},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        //product list is null or empty, throw exception
        CommonAPI.checkIsNullOrEmptyList(productList, "Product Not Found", terminalId);
        return ProductModel.createProductModel(productList.get(0), terminalId);

    }

    public static ProductModel getSimpleProductModelById(Integer productId) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> productList = dm.parameterizedExecuteQuery("data.posapi30.getSimpleProductById",
                new Object[]{productId},
                true
        );

        if ( productList == null || productList.size() != 1 ) {
            Logger.logMessage("Could not find product in ProductModel.getSimpleProductModelByID for ID " + productId, Logger.LEVEL.ERROR);
            return null;
        }

        ProductModel productModel = new ProductModel();
        productModel.setModelProperties( productList.get( 0 ) );

        return productModel;
    }

    public static ProductModel getProductModelByName(String productName, Integer terminalId) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> productList = dm.parameterizedExecuteQuery("data.posapi30.getOneProductByName",
                new Object[]{productName},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        //product list is null or empty, throw exception
        CommonAPI.checkIsNullOrEmptyList(productList, "Product Not Found", terminalId);
        return ProductModel.createProductModel(productList.get(0), terminalId);

    }

    public static ProductModel getProductModelByPluCode(String pluCode, Integer terminalId) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> productList = dm.parameterizedExecuteQuery("data.posapi30.getOneProductByPluCode",
                new Object[]{pluCode},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        //product list is null or empty, throw exception
        CommonAPI.checkIsNullOrEmptyList(productList, "Product Not Found", terminalId);
        return ProductModel.createProductModel(productList.get(0), terminalId);
    }

    public static ProductModel getProductModelBySubDepartmentId(Integer subDepartmentId, Integer terminalId) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> productList = dm.parameterizedExecuteQuery("data.posapi30.getOneProductBySubDepartmentId",
                new Object[]{subDepartmentId},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        //product list is null or empty, throw exception
        CommonAPI.checkIsNullOrEmptyList(productList, "Product Not Found", terminalId);
        return ProductModel.createProductModel(productList.get(0), terminalId);
    }

    public static ProductModel getProductModelBySubDepartmentName(String subDepartmentName, Integer terminalId) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> productList = dm.parameterizedExecuteQuery("data.posapi30.getOneProductBySubDepartmentName",
                new Object[]{subDepartmentName},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        //product list is null or empty, throw exception
        CommonAPI.checkIsNullOrEmptyList(productList, "Product Not Found", terminalId);
        return ProductModel.createProductModel(productList.get(0), terminalId);
    }

    public static ProductModel getProductModelByDepartmentId(Integer departmentId, Integer terminalId) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> productList = dm.parameterizedExecuteQuery("data.posapi30.getOneProductByDepartmentId",
                new Object[]{departmentId},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        //product list is null or empty, throw exception
        CommonAPI.checkIsNullOrEmptyList(productList, "Product Not Found", terminalId);
        return ProductModel.createProductModel(productList.get(0), terminalId);
    }

    public static ProductModel getProductModelByDepartmentName(String departmentName, Integer terminalId) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> productList = dm.parameterizedExecuteQuery("data.posapi30.getOneProductByDepartmentName",
                new Object[]{departmentName},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        //product list is null or empty, throw exception
        CommonAPI.checkIsNullOrEmptyList(productList, "Product Not Found", terminalId);
        return ProductModel.createProductModel(productList.get(0), terminalId);
    }


    //region get Active Products for the Revenue center

    public static ProductModel getActiveProductModelById(Integer productId, TerminalModel terminalModel) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> productList = dm.parameterizedExecuteQuery("data.posapi30.getOneActiveProductById",
                new Object[]{productId, terminalModel.getRevenueCenterId()},
                PosAPIHelper.getLogFileName(terminalModel.getId()),
                true
        );

        //product list is null or empty, throw exception
        CommonAPI.checkIsNullOrEmptyList(productList, "Product Not Found", terminalModel.getId());
        return ProductModel.createProductModel(productList.get(0), terminalModel.getId());

    }

    public static ProductModel getActiveProductModelByName(String productName, TerminalModel terminalModel) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> productList = dm.parameterizedExecuteQuery("data.posapi30.getOneActiveProductByName",
                new Object[]{productName, terminalModel.getRevenueCenterId()},
                PosAPIHelper.getLogFileName(terminalModel.getId()),
                true
        );

        //product list is null or empty, throw exception
        CommonAPI.checkIsNullOrEmptyList(productList, "Product Not Found", terminalModel.getId());
        return ProductModel.createProductModel(productList.get(0), terminalModel.getId());

    }

    public static ProductModel getActiveProductModelByPluCode(String pluCode, TerminalModel terminalModel) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> productList = dm.parameterizedExecuteQuery("data.posapi30.getOneActiveProductByPluCode",
                new Object[]{pluCode, terminalModel.getRevenueCenterId()},
                PosAPIHelper.getLogFileName(terminalModel.getId()),
                true
        );

        //product list is null or empty, throw exception
        CommonAPI.checkIsNullOrEmptyList(productList, "Product Not Found", terminalModel.getId());
        return ProductModel.createProductModel(productList.get(0), terminalModel.getId());
    }

    public static ProductModel getActiveProductModelBySubDepartmentId(Integer subDepartmentId, TerminalModel terminalModel) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> productList = dm.parameterizedExecuteQuery("data.posapi30.getOneActiveProductBySubDepartmentId",
                new Object[]{subDepartmentId, terminalModel.getRevenueCenterId()},
                PosAPIHelper.getLogFileName(terminalModel.getId()),
                true
        );

        //product list is null or empty, throw exception
        CommonAPI.checkIsNullOrEmptyList(productList, "Product Not Found", terminalModel.getId());
        return ProductModel.createProductModel(productList.get(0), terminalModel.getId());
    }

    public static ProductModel getActiveProductModelBySubDepartmentName(String subDepartmentName, TerminalModel terminalModel) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> productList = dm.parameterizedExecuteQuery("data.posapi30.getOneActiveProductBySubDepartmentName",
                new Object[]{subDepartmentName, terminalModel.getRevenueCenterId()},
                PosAPIHelper.getLogFileName(terminalModel.getId()),
                true
        );

        //product list is null or empty, throw exception
        CommonAPI.checkIsNullOrEmptyList(productList, "Product Not Found", terminalModel.getId());
        return ProductModel.createProductModel(productList.get(0), terminalModel.getId());
    }

    public static ProductModel getActiveProductModelByDepartmentId(Integer departmentId, TerminalModel terminalModel) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> productList = dm.parameterizedExecuteQuery("data.posapi30.getActiveOneProductByDepartmentId",
                new Object[]{departmentId, terminalModel.getRevenueCenterId()},
                PosAPIHelper.getLogFileName(terminalModel.getId()),
                true
        );

        //product list is null or empty, throw exception
        CommonAPI.checkIsNullOrEmptyList(productList, "Product Not Found", terminalModel.getId());
        return ProductModel.createProductModel(productList.get(0), terminalModel.getId());
    }

    public static ProductModel getActiveProductModelByDepartmentName(String departmentName, TerminalModel terminalModel) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> productList = dm.parameterizedExecuteQuery("data.posapi30.getOneActiveProductByDepartmentName",
                new Object[]{departmentName, terminalModel.getRevenueCenterId()},
                PosAPIHelper.getLogFileName(terminalModel.getId()),
                true
        );

        //product list is null or empty, throw exception
        CommonAPI.checkIsNullOrEmptyList(productList, "Product Not Found", terminalModel.getId());
        return ProductModel.createProductModel(productList.get(0), terminalModel.getId());
    }

    // For creating the PATransaction when purchasing a Meal Plan, the ProfilePurchaseProduct is the product and the Funding Terminal is the terminal.
    // ProfilePurchaseProduct may not be in Funding Terminal's revenue center so we need to preform check without looking for that
    public static ProductModel getActiveProductModelByIdWithoutTerminal(Integer productId, TerminalModel terminalModel) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> productList = dm.parameterizedExecuteQuery("data.posapi30.getOneActiveProductByIdWithoutTerminal",
                new Object[]{productId, terminalModel.getRevenueCenterId()},
                PosAPIHelper.getLogFileName(terminalModel.getId()),
                true
        );

        //product list is null or empty, throw exception
        CommonAPI.checkIsNullOrEmptyList(productList, "Product Not Found", terminalModel.getId());
        ProductModel productModel = ProductModel.createProductModel(productList.get(0), terminalModel.getId());
        List<LoyaltyRewardModel> rewards = new ArrayList<>();
        productModel.setRewards(rewards);
        productModel.setDiscountIds("");  //ProfilePurchaseProduct can't have loyalty rewards or discounts
        List<SurchargeModel> surcharges = new ArrayList<>();
        productModel.setSurcharges(surcharges);
        return productModel;

    }

    // Get a specific property of the given product
    public static Object getProductDetail(Integer productId, String property) {
        DataManager dm = new DataManager();

        ArrayList<HashMap> productDetails = dm.parameterizedExecuteQuery("data.signage.getProductDetails",
                new Object[]{productId},
                true
        );

        if (productDetails != null && productDetails.size() > 0 && productDetails.get(0) != null) {
            return productDetails.get(0).get(property);
        }
        return null;
    }

    //endregion

    //getter
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getId() {
        return this.id;
    }

    //setter
    public void setId(Integer id) {
        this.id = id;
    }

    //getter
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getName() {
        return this.name;
    }

    //setter
    public void setName(String name) {
        this.name = name;
    }

    //getter
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getDescription() {
        return this.description;
    }

    //setter
    public void setDescription(String description) {
        this.description = description;
    }

    //getter
    //@JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonIgnore
    public BigDecimal getPrice() {
        return this.price;
    }

    //setter
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    //getter
    @ApiModelProperty(value = "PLU of the Product", dataType = "String")
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getProductCode() {
        return this.productCode;
    }

    //setter
    public void setProductCode(String code) {
        this.productCode = code;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getSubDepartment() {
        return subDepartment;
    }

    public void setSubDepartment(String subDepartment) {
        this.subDepartment = subDepartment;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    @JsonIgnore
    public Integer getSubDepartmentId() {
        return subDepartmentId;
    }

    public void setSubDepartmentId(Integer subDepartmentId) {
        this.subDepartmentId = subDepartmentId;
    }

    @JsonIgnore
    public boolean isWellness() {
        return isWellness;
    }

    public void setWellness(boolean isWellness) {
        this.isWellness = isWellness;
    }

    @JsonGetter("isModifier")
    public boolean isModifier() {
        return isModifier;
    }

    public void setIsModifier(boolean modifier) {
        isModifier = modifier;
    }

    @JsonGetter("isWeighted")
    public boolean isWeighted() {
        return isWeighted;
    }

    public void setIsWeighted(boolean weighted) {
        isWeighted = weighted;
    }

    @JsonIgnore
    public Integer getMappedPosItem() {
        return mappedPosItem;
    }

    public void setMappedPosItem(Integer mappedPosItem) {
        this.mappedPosItem = mappedPosItem;
    }

    @JsonIgnore
    public List<LoyaltyRewardModel> getRewards() {
        return rewards;
    }

    public void setRewards(List<LoyaltyRewardModel> rewards) {
        this.rewards = rewards;
    }

    @JsonIgnore
    public String getDiscountIds() {
        return discountIds;
    }

    public void setDiscountIds(String discountIds) {
        this.discountIds = discountIds;
    }

    @JsonIgnore
    public String getTaxIds() {
        return taxIds;
    }

    public void setTaxIds(String taxIds) {
        this.taxIds = taxIds;
    }

    public Integer getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }

    @JsonIgnore
    public List<LoyaltyAccrualPolicyLevelModel> getLoyaltyAccrualPolicyLevels() {
        return loyaltyAccrualPolicyLevels;
    }

    public void setLoyaltyAccrualPolicyLevels(List<LoyaltyAccrualPolicyLevelModel> loyaltyAccrualPolicyLevels) {
        this.loyaltyAccrualPolicyLevels = loyaltyAccrualPolicyLevels;
    }

    @JsonIgnore
    public boolean isAlreadyFetchedAccrualPolicyLevels() {
        return alreadyFetchedAccrualPolicyLevels;
    }

    public void setAlreadyFetchedAccrualPolicyLevels(boolean alreadyFetchedAccrualPolicyLevels) {
        this.alreadyFetchedAccrualPolicyLevels = alreadyFetchedAccrualPolicyLevels;
    }

    @JsonIgnore
    public boolean isDiningOption() {
        return isDiningOption;
    }

    public void setDiningOption(boolean diningOption) {
        isDiningOption = diningOption;
    }

    @JsonIgnore
    public boolean isInventoryItem() {
        return inventoryItem;
    }

    public void setInventoryItem(boolean inventoryItem) {
        this.inventoryItem = inventoryItem;
    }

    @JsonIgnore
    public List<SurchargeModel> getSurcharges() {
        return surcharges;
    }

    public void setSurcharges(List<SurchargeModel> surcharges) {
        this.surcharges = surcharges;
    }

    @JsonIgnore
    public List<TaxModel> getTaxDeletes() {
        return taxDeletes;
    }

    public void setTaxDeletes(List<TaxModel> taxDeletes) {
        this.taxDeletes = taxDeletes;
    }

    @JsonIgnore
    public boolean isFetchedFromDb() {
        return fetchedFromDb;
    }

    public void setFetchedFromDb(boolean fetchedFromDb) {
        this.fetchedFromDb = fetchedFromDb;
    }

    /**
     * Creates a deep copy of the product model
     * @return
     */
    public ProductModel clone(){
        ProductModel clone = null;

        try {
            Gson gson = new Gson();
            String objectString = gson.toJson(this);
            JsonParser jsonParser = new JsonParser();
            JsonElement jsonElement = jsonParser.parse(objectString);
            clone = gson.fromJson(jsonElement, ProductModel.class);
        } catch (Exception e){
            Logger.logMessage("ProductModel.clone: Error cloning product. ", Logger.LEVEL.ERROR);
        }

        return clone;
    }

    /**
     * Overridden toString method for a ProductModel.
     * @return The {@link String} representation of a ProductModel.
     */
    @Override
    public String toString () {
        
        String taxDeletesStr = "";
        if (!DataFunctions.isEmptyCollection(taxDeletes)) {
            taxDeletesStr += "[";
            for (TaxModel tax : taxDeletes) {
                if (tax != null && tax.equals(taxDeletes.get(taxDeletes.size() - 1))) {
                    taxDeletesStr += tax.toString();
                }
                else if (tax != null && !tax.equals(taxDeletes.get(taxDeletes.size() - 1))) {
                    taxDeletesStr += tax.toString() + "; ";
                }
            }
            taxDeletesStr += "]";
        }

        String rewardsStr = "";
        if (!DataFunctions.isEmptyCollection(rewards)) {
            rewardsStr += "[";
            for (LoyaltyRewardModel reward : rewards) {
                if (reward != null && reward.equals(rewards.get(rewards.size() - 1))) {
                    rewardsStr += reward.toString();
                }
                else if (reward != null && !reward.equals(rewards.get(rewards.size() - 1))) {
                    rewardsStr += reward.toString() + "; ";
                }
            }
            rewardsStr += "]";
        }
        
        String loyaltyAccrualPolicyLevelsStr = "";
        if (!DataFunctions.isEmptyCollection(loyaltyAccrualPolicyLevels)) {
            loyaltyAccrualPolicyLevelsStr += "[";
            for (LoyaltyAccrualPolicyLevelModel loyaltyAccrualPolicyLevel : loyaltyAccrualPolicyLevels) {
                if (loyaltyAccrualPolicyLevel != null && loyaltyAccrualPolicyLevel.equals(loyaltyAccrualPolicyLevels.get(loyaltyAccrualPolicyLevels.size() - 1))) {
                    loyaltyAccrualPolicyLevelsStr += loyaltyAccrualPolicyLevel.toString();
                }
                else if (loyaltyAccrualPolicyLevel != null && !loyaltyAccrualPolicyLevel.equals(loyaltyAccrualPolicyLevels.get(loyaltyAccrualPolicyLevels.size() - 1))) {
                    loyaltyAccrualPolicyLevelsStr += loyaltyAccrualPolicyLevel.toString() + "; ";
                }
            }
            loyaltyAccrualPolicyLevelsStr += "]";
        }
        
        String surchargesStr = "";
        if (!DataFunctions.isEmptyCollection(surcharges)) {
            surchargesStr += "[";
            for (SurchargeModel surcharge : surcharges) {
                if (surcharge != null && surcharge.equals(surcharges.get(surcharges.size() - 1))) {
                    surchargesStr += surcharge.toString();
                }
                else if (surcharge != null && !surcharge.equals(surcharges.get(surcharges.size() - 1))) {
                    surchargesStr += surcharge.toString() + "; ";
                }
            }
            surchargesStr += "]";
        }

        return String.format("ID: %s, NAME:%s, DESCRIPTION: %s, PRICE:%s, PRODUCTCODE: %s, SUBDEPARTMENT: %s, " +
                "DEPARTMENT: %s, ISWELLNESS: %s, ISMODIFIER: %s, ISWEIGHTED: %s, ALREADYFETCHEDACCRUALPOLICYLEVELS: %s, " +
                "ISDININGOPTION: %s, INVENTORYITEM: %s, FETCHEDFROMDB: %s, DISCOUNTIDS: %s, TAXIDS: %s, TAXDELETES: %s, " +
                "SUBDEPARTMENTID: %s, DEPARTMENTID: %s, MAPPEDPOSITEM: %s, REWARDS: %s, LOYALTYACCRUALPOLICYLEVELS: %s, " +
                "SURCHARGES: %s",
                Objects.toString((id != null && id > 0 ? id : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(name) ? name : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(description) ? description : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(productCode) ? productCode : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(subDepartment) ? subDepartment : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(department) ? department : "N/A"), "N/A"),
                Objects.toString((price != null ? price.toPlainString() : "N/A"), "N/A"),
                Objects.toString(isWellness, "N/A"),
                Objects.toString(isModifier, "N/A"),
                Objects.toString(isWeighted, "N/A"),
                Objects.toString(alreadyFetchedAccrualPolicyLevels, "N/A"),
                Objects.toString(isDiningOption, "N/A"),
                Objects.toString(inventoryItem, "N/A"),
                Objects.toString(fetchedFromDb, "N/A"),
                Objects.toString((StringFunctions.stringHasContent(discountIds) ? discountIds : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(taxIds) ? taxIds : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(taxDeletesStr) ? taxDeletesStr : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(rewardsStr) ? rewardsStr : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(loyaltyAccrualPolicyLevelsStr) ? loyaltyAccrualPolicyLevelsStr : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(surchargesStr) ? surchargesStr : "N/A"), "N/A"),
                Objects.toString((subDepartmentId != null && subDepartmentId > 0 ? subDepartmentId : "N/A"), "N/A"),
                Objects.toString((departmentId != null && departmentId > 0 ? departmentId : "N/A"), "N/A"),
                Objects.toString((mappedPosItem != null && mappedPosItem > 0 ? mappedPosItem : "N/A"), "N/A"));

    }

}