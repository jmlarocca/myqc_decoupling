package com.mmhayes.common.product.models;

//mmhayes dependencies

import com.fasterxml.jackson.annotation.JsonInclude;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.transaction.models.TaxModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

//API dependencies
//other dependencies

/*
 $Author: jkflanagan $: Author of last commit
 $Date: 2020-12-31 16:39:34 -0500 (Thu, 31 Dec 2020) $: Date of last commit
 $Rev: 13381 $: Revision of last commit
 Notes:
*/
public class ItemTaxModel {

    private Integer id;
    private Integer transLineItemId;
    private Integer itemTypeId;
    private Integer itemId;

    private BigDecimal eligibleAmount = BigDecimal.ZERO;
    private BigDecimal amount;
    private BigDecimal refundedEligibleAmount;
    private BigDecimal refundedAmount;
    private TaxModel tax;

    public ItemTaxModel() {
    }

    public ItemTaxModel(HashMap itemTaxDiscountHM) {
        setModelProperties(itemTaxDiscountHM);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getTransLineItemId() {
        return transLineItemId;
    }

    public void setTransLineItemId(Integer transLineItemId) {
        this.transLineItemId = transLineItemId;
    }

    public void setItemTypeId(Integer itemTypeId) {
        this.itemTypeId = itemTypeId;
    }

    public Integer getItemTypeId() {

        if (itemTypeId == null) {
            this.itemTypeId = PosAPIHelper.ObjectType.TAX.toInt();
        }
        return this.itemTypeId;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public TaxModel getTax() {
        return tax;
    }

    public void setTax(TaxModel tax) {
        this.tax = tax;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public BigDecimal getEligibleAmount() {
        return eligibleAmount;
    }

    public void setEligibleAmount(BigDecimal eligibleAmount) {
        this.eligibleAmount = eligibleAmount;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public BigDecimal getRefundedEligibleAmount() {
        return refundedEligibleAmount;
    }

    public void setRefundedEligibleAmount(BigDecimal refundedEligibleAmount) {
        this.refundedEligibleAmount = refundedEligibleAmount;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public BigDecimal getRefundedAmount() {
        return refundedAmount;
    }

    public void setRefundedAmount(BigDecimal refundedAmount) {
        this.refundedAmount = refundedAmount;
    }

    //setter for all of this model's properties
    public ItemTaxModel setModelProperties(HashMap modelDetailHM) {
        setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));

        setTransLineItemId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("TRANSLINEITEMID")));
        setItemTypeId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ITEMTYPEID")));
        setItemId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ITEMID")));

        setAmount(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("AMOUNT")));
        setEligibleAmount(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("ELIGIBLEAMOUNT")));
        setRefundedEligibleAmount(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("REFUNDEDELIGIBLEAMOUNT")));
        setRefundedAmount(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("REFUNDEDAMOUNT")));

        return this;
    }

    //The itemTypeId specifies what type it is (Tax - 6, POS Discount - 7)
    public static ItemTaxModel getItemTaxDiscountModel(Integer itemTaxDiscountId, String logFileName, Integer terminalId) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> itemTaxDiscountList = dm.parameterizedExecuteQuery("data.posapi30.getOneItemTaxById",
                new Object[]{itemTaxDiscountId},
                logFileName,
                true
        );

        //product list is null or empty, throw exception
        CommonAPI.checkIsNullOrEmptyList(itemTaxDiscountList, "Item Tax Not Found", terminalId);
        return ItemTaxModel.createItemTaxModel(itemTaxDiscountList.get(0));

    }

    public static ItemTaxModel createItemTaxModel(HashMap itemTaxDiscountHM) {
        return new ItemTaxModel(itemTaxDiscountHM);
    }

    /**
     * Overridden toString method for an ItemTaxModel.
     * @return The {@link String} representation of an ItemTaxModel.
     */
    @Override
    public String toString () {

        return String.format("ID: %s, TRANSLINEITEMID: %s, ITEMTYPEID: %s, ITEMID: %s, ELIGIBLEAMOUNT: %s, AMOUNT: %s, REFUNDEDELIGIBLEAMOUNT: %s, REFUNDEDAMOUNT: %s, TAX: %s",
                Objects.toString((id != null && id > 0 ? id : "N/A"), "N/A"),
                Objects.toString((transLineItemId != null && transLineItemId > 0 ? transLineItemId : "N/A"), "N/A"),
                Objects.toString((itemTypeId != null && itemTypeId > 0 ? itemTypeId : "N/A"), "N/A"),
                Objects.toString((itemId != null && itemId > 0 ? itemId : "N/A"), "N/A"),
                Objects.toString((eligibleAmount != null ? eligibleAmount.toPlainString() : "N/A"), "N/A"),
                Objects.toString((amount != null ? amount.toPlainString() : "N/A"), "N/A"),
                Objects.toString((refundedEligibleAmount != null ? refundedEligibleAmount.toPlainString() : "N/A"), "N/A"),
                Objects.toString((refundedAmount != null ? refundedAmount.toPlainString() : "N/A"), "N/A"),
                Objects.toString((tax != null ? tax.toString() : "N/A"), "N/A"));
    }

}
