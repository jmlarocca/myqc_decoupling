package com.mmhayes.common.product.models;

//mmhayes dependencies
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.utils.StringFunctions;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

//API dependencies
import com.fasterxml.jackson.annotation.JsonIgnore;

//other dependencies

/*
 Last Updated (automatically updated by SVN)
 $Author: prsmith $: Author of last commit
 $Date: 2021-01-05 14:06:33 -0500 (Tue, 05 Jan 2021) $: Date of last commit
 $Rev: 13394 $: Revision of last commit
*/
public class PrepOptionModel {

    private Integer id = null;
    private Integer prepOptionSetId = null;
    private BigDecimal price = BigDecimal.ZERO;
    private BigDecimal priceOriginal = BigDecimal.ZERO;
    private String name = "";
    private boolean defaultOption;
    private boolean displayDefaultReceipts;

    public PrepOptionModel() {

    }

    public PrepOptionModel(HashMap modelHM) {

          setModelProperties(modelHM);

    }

    //setter for all of this model's properties
    public PrepOptionModel setModelProperties(HashMap modelDetailHM) {
        setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAPREPOPTIONID")));
        setPrepOptionSetId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAPREPOPTIONSETID")));
        setPrice(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("PRICE")));
        setName(CommonAPI.convertModelDetailToString(modelDetailHM.get("NAME")));
        setDefaultOption(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("DEFAULTOPTION"), false));
        setDisplayDefaultReceipts(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("DISPLAYDEFAULTRECEIPTS"), false));

        return this;
    }

    //setter for all of this model's properties
    public static PrepOptionModel getPrepOptionModel(HashMap modelDetailHM) {
        Integer prepOptionId = CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAPREPOPTIONID"));
        Integer prepOptionSetId = CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAPREPOPTIONSETID"));
        BigDecimal prepOptionPrice = CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("PAPREPOPTIONPRICE"));
        String name = CommonAPI.convertModelDetailToString(modelDetailHM.get("NAME"));
        boolean defaultOption = CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("DEFAULTOPTION"), false);
        boolean displayDefaultReceipts = CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("DISPLAYDEFAULTRECEIPTS"), false);

        if(prepOptionId != null && prepOptionSetId != null && prepOptionPrice != null) {
            PrepOptionModel prepOptionModel = new PrepOptionModel();

            prepOptionModel.setId(prepOptionId);
            prepOptionModel.setPrepOptionSetId(prepOptionSetId);
            prepOptionModel.setPrice(prepOptionPrice);
            prepOptionModel.setName(name);
            prepOptionModel.setDefaultOption(defaultOption);
            prepOptionModel.setDisplayDefaultReceipts(displayDefaultReceipts);

            return prepOptionModel;
        }

        return null;
    }

    public static PrepOptionModel createPrepOptionModel(HashMap modelDetailHM) throws Exception {
        PrepOptionModel prepOptionModel = new PrepOptionModel(modelDetailHM);
        return prepOptionModel;
    }

    public static PrepOptionModel getPrepOptionModelByIdAndSet(Integer prepOptionId, Integer prepOptionSetId, Integer terminalId) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> prepOptionList = dm.parameterizedExecuteQuery("data.posapi30.getOnePrepOptionByIdAndSetId",
                new Object[]{prepOptionId, prepOptionSetId},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        //product list is null or empty, throw exception
        CommonAPI.checkIsNullOrEmptyList(prepOptionList, "Prep Option Not Found", terminalId);
        return PrepOptionModel.createPrepOptionModel(prepOptionList.get(0));

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPrepOptionSetId() {
        return prepOptionSetId;
    }

    public void setPrepOptionSetId(Integer prepOptionSetId) {
        this.prepOptionSetId = prepOptionSetId;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @JsonIgnore
    public BigDecimal getPriceOriginal() {
        return priceOriginal;
    }

    public void setPriceOriginal(BigDecimal priceOriginal) {
        this.priceOriginal = priceOriginal;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonIgnore
    public boolean isDefaultOption() {
        return defaultOption;
    }

    public void setDefaultOption(boolean defaultOption) {
        this.defaultOption = defaultOption;
    }

    @JsonIgnore
    public boolean isDisplayDefaultReceipts() {
        return displayDefaultReceipts;
    }

    public void setDisplayDefaultReceipts(boolean displayDefaultReceipts) {
        this.displayDefaultReceipts = displayDefaultReceipts;
    }

    /**
     * Overridden toString method for a PrepOptionModel.
     * @return The {@link String} representation of a PrepOptionModel.
     */
    @Override
    public String toString () {

        return String.format("ID: %s, PREPOPTIONSETID: %s, PRICE: %s, PRICEORIGINAL: %s, NAME: %s, DEFAULTOPTION: %s, DISPLAYDEFAULTRECEIPTS: %s",
                Objects.toString((id != null && id > 0 ? id : "N/A"), "N/A"),
                Objects.toString((prepOptionSetId != null && prepOptionSetId > 0 ? prepOptionSetId : "N/A"), "N/A"),
                Objects.toString((price != null ? price.toPlainString() : "N/A"), "N/A"),
                Objects.toString((priceOriginal != null ? priceOriginal.toPlainString() : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(name) ? name : "N/A"), "N/A"),
                Objects.toString(defaultOption, "N/A"),
                Objects.toString(displayDefaultReceipts, "N/A"));

    }

}
