package com.mmhayes.common.product.collections;

//mmhayes dependencies
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.dataaccess.DataManager;

//API dependencies
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.product.models.*;

//other dependencies
import java.util.ArrayList;
import java.util.HashMap;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2017-06-16 12:44:46 -0400 (Fri, 16 Jun 2017) $: Date of last commit
 $Rev: 4152 $: Revision of last commit
*/
public class ItemRewardCollection {
    private ArrayList<ItemRewardModel> collection = new ArrayList<>();
    private static DataManager dm = new DataManager();

    //constructor
    public ItemRewardCollection() {

    }

    //The itemTypeId specifies what type it is (Tax - 6, POS Discount - 7)
    public static ItemRewardCollection getAllItemRewards(Integer transLineItemId, Integer terminalId) throws Exception {
        ItemRewardCollection itemRewardCollection = new ItemRewardCollection();


        //get all models in an array list
        ArrayList<HashMap> itemRewardList = dm.parameterizedExecuteQuery("data.posapi30.getAllItemRewards",
                new Object[]{transLineItemId},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        //product list is null or empty, throw exception
        //CommonAPI.checkIsNullOrEmptyList(itemDiscountList, "Item Discount List is empty");
        for (HashMap itemRewardHM : itemRewardList) {
            CommonAPI.checkIsNullOrEmptyObject(itemRewardHM.get("ID"), "Item Reward Id could not be found", terminalId);
            //create a new model and add to the collection
            itemRewardCollection.getCollection().add(ItemRewardModel.createItemRewardModel(itemRewardHM));
        }

        return itemRewardCollection;
    }

    //getter
    public ArrayList<ItemRewardModel> getCollection() {
        return collection;
    }

    //setter
    public void setCollection(ArrayList<ItemRewardModel> collection) {
        this.collection = collection;
    }

}
