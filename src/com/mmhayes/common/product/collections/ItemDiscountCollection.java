package com.mmhayes.common.product.collections;

//mmhayes dependencies
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.dataaccess.DataManager;

//API dependencies
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.product.models.ItemDiscountModel;

//other dependencies
import java.util.ArrayList;
import java.util.HashMap;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2017-06-16 12:44:46 -0400 (Fri, 16 Jun 2017) $: Date of last commit
 $Rev: 4152 $: Revision of last commit
*/
public class ItemDiscountCollection {
    private ArrayList<ItemDiscountModel> collection = new ArrayList<>();
    private static DataManager dm = new DataManager();

    //constructor
    public ItemDiscountCollection() {

    }

    //The itemTypeId specifies what type it is (Tax - 6, POS Discount - 7)
    public static ItemDiscountCollection getAllItemDiscounts(Integer transLineItemId, Integer terminalId) throws Exception {
        ItemDiscountCollection itemDiscountCollection = new ItemDiscountCollection();


        //get all models in an array list
        ArrayList<HashMap> itemDiscountList = dm.parameterizedExecuteQuery("data.posapi30.getAllItemDiscounts",
                new Object[]{transLineItemId},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        //product list is null or empty, throw exception
        //CommonAPI.checkIsNullOrEmptyList(itemDiscountList, "Item Discount List is empty");
        for (HashMap itemDiscountHM : itemDiscountList) {
            CommonAPI.checkIsNullOrEmptyObject(itemDiscountHM.get("ID"), "Item Discount Id could not be found", terminalId);
            //create a new model and add to the collection
            itemDiscountCollection.getCollection().add(ItemDiscountModel.createItemDiscountModel(itemDiscountHM));
        }

        return itemDiscountCollection;
    }

    //getter
    public ArrayList<ItemDiscountModel> getCollection() {
        return collection;
    }

    //setter
    public void setCollection(ArrayList<ItemDiscountModel> collection) {
        this.collection = collection;
    }

}
