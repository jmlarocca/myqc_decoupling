package com.mmhayes.common.product.collections;

//MMHayes dependencies
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.dataaccess.DataManager;

//API dependencies
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.product.models.ItemSurchargeModel;

//Other dependencies
import java.util.ArrayList;
import java.util.HashMap;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2017-06-16 12:44:46 -0400 (Fri, 16 Jun 2017) $: Date of last commit
 $Rev: 4152 $: Revision of last commit
*/
public class ItemSurchargeCollection {
    private ArrayList<ItemSurchargeModel> collection = new ArrayList<>();
    private static DataManager dm = new DataManager();

    //constructor
    public ItemSurchargeCollection() {

    }

    //The itemTypeId specifies what type it is (Tax - 6, POS Discount - 7, etc.)
    public static ItemSurchargeCollection getAllItemSurcharges(Integer transLineItemId, Integer terminalId) throws Exception {
        ItemSurchargeCollection itemSurchargeCollection = new ItemSurchargeCollection();


        //get all models in an array list
        ArrayList<HashMap> itemSurchargeList = dm.parameterizedExecuteQuery("data.posapi30.getAllItemSurcharges",
                new Object[]{transLineItemId},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        //product list is null or empty, throw exception
        for (HashMap itemSurchargeHM : itemSurchargeList) {
            CommonAPI.checkIsNullOrEmptyObject(itemSurchargeHM.get("ID"), "Item Surcharge Id could not be found", terminalId);
            //create a new model and add to the collection
            itemSurchargeCollection.getCollection().add(ItemSurchargeModel.createItemSurchargeModel(itemSurchargeHM));
        }

        return itemSurchargeCollection;
    }

    //getter
    public ArrayList<ItemSurchargeModel> getCollection() {
        return collection;
    }

    //setter
    public void setCollection(ArrayList<ItemSurchargeModel> collection) {
        this.collection = collection;
    }

}
