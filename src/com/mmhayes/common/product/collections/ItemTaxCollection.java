package com.mmhayes.common.product.collections;

//mmhayes dependencies
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.dataaccess.DataManager;

//API dependencies
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.product.models.ItemTaxModel;

//other dependencies
import java.util.ArrayList;
import java.util.HashMap;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2017-06-16 12:44:46 -0400 (Fri, 16 Jun 2017) $: Date of last commit
 $Rev: 4152 $: Revision of last commit
*/
public class ItemTaxCollection {

    private ArrayList<ItemTaxModel> collection = new ArrayList<>();
    private static DataManager dm = new DataManager();

    //constructor
    public ItemTaxCollection() {

    }

    //The itemTypeId specifies what type it is (Tax - 6, POS Discount - 7)
    public static ItemTaxCollection getAllItemTaxes(Integer transLineItemId, Integer terminalId) throws Exception {
        ItemTaxCollection itemTaxCollection = new ItemTaxCollection();


        //get all models in an array list
        ArrayList<HashMap> itemTaxList = dm.parameterizedExecuteQuery("data.posapi30.getAllItemTaxes",
                new Object[]{transLineItemId},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        //product list is null or empty, throw exception
        //CommonAPI.checkIsNullOrEmptyList(itemTaxList, "Item Tax List is empty");
        for (HashMap itemTaxHM : itemTaxList) {
            CommonAPI.checkIsNullOrEmptyObject(itemTaxHM.get("ID"), "Item Tax Discount Id could not be found", terminalId);
            //create a new model and add to the collection
            itemTaxCollection.getCollection().add(ItemTaxModel.createItemTaxModel(itemTaxHM));
        }

        return itemTaxCollection;
    }

    //getter
    public ArrayList<ItemTaxModel> getCollection() {
        return collection;
    }

    //setter
    public void setCollection(ArrayList<ItemTaxModel> collection) {
        this.collection = collection;
    }
}
