package com.mmhayes.common.product.collections;

//mmhayes dependencies
import com.mmhayes.common.product.models.ModifierCalculation;
import com.mmhayes.common.product.models.ProductCalculation;
import com.mmhayes.common.product.models.ProductModel;
import com.mmhayes.common.transaction.collections.DiscountQuantityLevelCollection;
import com.mmhayes.common.transaction.models.*;

//other dependencies
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/*
Last Updated (automatically updated by SVN)
$Author: nyu $: Author of last commit
$Date: 2021-08-17 16:55:44 -0400 (Tue, 17 Aug 2021) $: Date of last commit
$Rev: 14953 $: Revision of last commit
*/

public class ProductCollectionCalculation {
    private List<ProductCalculation> collection = new ArrayList<ProductCalculation>();
    private BigDecimal productSubtotal = BigDecimal.ZERO;

    public ProductCollectionCalculation() {
    }

    public void populateCollection(TransactionModel transactionModel, Integer terminalId, int roundingMode, boolean allowOverrideProductPrices) throws Exception {
        //for each product line, create a new productCalculation object

        for ( int i = 0; i < transactionModel.getProducts().size(); i++ ) {
            ProductLineItemModel productLine = transactionModel.getProducts().get(i);

            Integer productId = productLine.getProduct().getId();
            ProductModel productModel = null;
            if (transactionModel.getProducts().get(i).getProduct() != null && transactionModel.getProducts().get(i).getProduct().isFetchedFromDb()) {
                productModel = transactionModel.getProducts().get(i).getProduct();
            } else {
                productModel = ProductModel.getProductModelById(productId, terminalId);
            }

            //to keep track of the price of the entire line
            BigDecimal productLinePrice = productModel.getPrice();

            if (allowOverrideProductPrices) { //For QCPOS, if any product prices were overridden, take the updated price
                if (productLinePrice == null || (productLine.getAmount() != null && productLine.getAmount().compareTo(productLinePrice) != 0)) {
                    productLinePrice = productLine.getAmount();
                    productModel.setPrice(productLinePrice);
                }
            }

            //set up productCalculation map
            HashMap productHM = new HashMap();
            productHM.put("ID", productId);
            productHM.put("INDEX", i);
            productHM.put("PRODUCTMODEL", productModel);
            productHM.put("QUANTITY", productLine.getQuantity());
            productHM.put("ROUNDINGMODE", roundingMode);
            productHM.put("COMBOTRANSLINEITEMID", productLine.getComboTransLineItemId());
            productHM.put("COMBODETAILID", productLine.getComboDetailId());
            productHM.put("BASEPRICE", productLine.getBasePrice());
            productHM.put("COMBOPRICE", productLine.getComboPrice());
            productHM.put("UPCHARGE", productLine.getUpcharge());
            productHM.put("ORIGINALINDEX", i);
            productHM.put("TRANSACTIONITEMNUM", productLine.getTransactionItemNum());

            if(productLine.getPrepOption() != null) {
                productHM.put("PREPOPTIONID", productLine.getPrepOption().getId());
                productHM.put("PREPOPTIONPRICE", productLine.getPrepOption().getPrice());
                productHM.put("PREPOPTIONSETID", productLine.getPrepOption().getPrepOptionSetId());
            }

            List<ModifierCalculation> modifiers = new ArrayList<ModifierCalculation>();
            if ( productLine.getModifiers() != null && productLine.getModifiers().size() > 0 ) {
                int j = 0;
                for (ModifierLineItemModel modifierLineItemModel : productLine.getModifiers() ) {
                    Integer modifierProductId = modifierLineItemModel.getProduct().getId();

                    ProductModel modifierProductModel = null;
                    if (modifierLineItemModel != null && modifierLineItemModel.getProduct().isFetchedFromDb()) {
                        modifierProductModel = modifierLineItemModel.getProduct();
                    } else {
                        modifierProductModel = ProductModel.getProductModelById(modifierProductId, terminalId);
                    }

                    ModifierCalculation modifierCalculation = new ModifierCalculation();
                    modifierCalculation.setId(modifierProductId);
                    modifierCalculation.setModIndex(j);
                    modifierCalculation.setProductModel(modifierProductModel);
                    modifierCalculation.setQuantity( productLine.getQuantity() );
                    modifierCalculation.setTransactionItemNum(modifierLineItemModel.getTransactionItemNum());

                    if(modifierLineItemModel.getPrepOption() != null) {
                        modifierCalculation.setPrepOption(modifierLineItemModel.getPrepOption());
                    }

                    modifiers.add(modifierCalculation);

                    if(modifierProductModel.getPrice() != null ) {
                        productLinePrice = productLinePrice.add(modifierProductModel.getPrice());
                    }

                    if(modifierCalculation.getPrepOption() != null ) {
                        productLinePrice = productLinePrice.add(modifierCalculation.getPrepOption().getPrice());
                    }


                    j++;
                }
            }

            if(productLine.getPrepOption() != null) {
                productLinePrice = productLinePrice.add(productLine.getPrepOption().getPrice());
            }

            productHM.put("LINEPRICE", productLinePrice);
            this.addToCollection(new ProductCalculation(productHM, productModel, modifiers, transactionModel.getTransactionTypeEnum()), true);
        }
    }

    //returns the a discountCalculation object with all of the details for discountableAmount and per line discountableAmounts
    public DiscountCalculation getDiscountableAmounts(DiscountCalculation discountCalculation) throws Exception {
        if ( this.getCollection() == null || this.getCollection().size() == 0 ) {
            return discountCalculation;
        }

        List<ProductCalculation> eligibleProducts;

        //if the discount has discount quantity levels
        DiscountQuantityLevelCollection discountQuantityLevelCollection = discountCalculation.getDiscountModel().getDiscountQuantityLevelCollection();
        if ( discountQuantityLevelCollection == null || discountQuantityLevelCollection.getCollection() == null || discountQuantityLevelCollection.getCollection().size() ==  0 ) {
            eligibleProducts = this.getCollection();
        } else {
            eligibleProducts = discountQuantityLevelCollection.determineEligibleProducts( this.getCollection(), discountCalculation.getQcDiscountModel(), discountCalculation.getEmployeeId() );
        }

        for ( ProductCalculation productCalculation : eligibleProducts ) {
            this.checkForDiscountsRewardsAlreadyApplied(productCalculation);
            BigDecimal lineDiscountableAmount = productCalculation.getDiscountableAmount(discountCalculation);
            discountCalculation.addLineDiscountable(productCalculation.getIndex(), lineDiscountableAmount);
            discountCalculation.addQuantityLineDiscountable(productCalculation.getIndex(), productCalculation.getQuantity());
        }

        return discountCalculation;
    }

    public TaxCalculation getTaxableAmounts(Integer taxId, Integer terminalId) throws Exception {
        TaxCalculation taxCalculation = new TaxCalculation(taxId, terminalId);

        if ( this.getCollection() == null || this.getCollection().size() == 0 ) {
            return taxCalculation;
        }

        for ( ProductCalculation productCalculation : this.getCollection() ) {
            BigDecimal lineDiscountableAmount = productCalculation.getTaxableAmount(taxId);
            taxCalculation.addLineTaxable(productCalculation.getIndex(), lineDiscountableAmount);
        }

        return taxCalculation;
    }

    public Integer determineNextProductIndex() {
        Integer index = 0;

        for ( ProductCalculation product : this.getCollection() ) {
            if ( product.getIndex() > index ) {
                index = product.getIndex();
            }
        }

        return ( index + 1 );
    }

    public static boolean productsHaveSameModifiers(ProductCalculation product1, ProductCalculation product2) {
        //if they both have no modifiers, they are equivalent
        if ( product1.getModifiers().size() == 0 && product2.getModifiers().size() == 0 ) {
            return true;
        }

        //if one product has more or less modifiers than the other, fail check
        if ( product1.getModifiers().size() != product2.getModifiers().size() ) {
            return false;
        }

        boolean sameModifiers = false;
        for ( int i = 0; i < product1.getModifiers().size(); i++ ) {
            ModifierCalculation prod1mod = product1.getModifiers().get( i );
            ModifierCalculation prod2mod = product2.getModifiers().get( i );

            if ( !prod1mod.getId().equals( prod2mod.getId() ) ) {
                break;
            }

            //on last iteration
            if ( ( i + 1 ) == product1.getModifiers().size() ) {
                sameModifiers = true;
                break;
            }
        }

        return sameModifiers;
    }

    //determine if the two products have the same prep option
    public static boolean productsHaveSamePrepOption(ProductCalculation product1, ProductCalculation product2) {
        if(product1.getPrepOption() == null && product2.getPrepOption() == null) {
            return true;
        } else if (product1.getPrepOption() != null && product2.getPrepOption() != null && product1.getPrepOption().getId().equals(product2.getPrepOption().getId())) {
            return true;
        }

        return false;
    }

    //getters and setters
    public List<ProductCalculation> getCollection() {
        return collection;
    }

    public void setCollection(List<ProductCalculation> collection) {
        this.collection = collection;
    }

    public void addToCollection(ProductCalculation productCalculation, boolean addToSubtotal) {
        //get collection, add to it and set it again
        List<ProductCalculation> productCalculations = this.getCollection();
        productCalculations.add(productCalculation);
        this.setCollection(productCalculations);

        //update subtotal when adding to collection
        if ( addToSubtotal ) {
            addToSubtotal(productCalculation.getLinePrice().multiply(productCalculation.getQuantity()));
        }
    }

    //reorders the product calculation lines so we're adding the split product after it's original product
    public void addToCollectionSplit(ProductCalculation productCalculation, boolean addToSubtotal) {
        List<ProductCalculation> collection = this.getCollection();

        if(productCalculation.getOriginalIndex() == null) {
            return;
        }

        int index =  productCalculation.getOriginalIndex();

        //if splitting the last line then add split product to the end
        if(index + 2 > collection.size()) {
            collection.add(productCalculation);
            if(addToSubtotal) {
                addToSubtotal(productCalculation.getLinePrice().multiply(productCalculation.getQuantity()));
            }
            return;
        }

        //get the product calculation lines before the split
        List<ProductCalculation> productCalculationSplit = collection.subList(0, index +1);
        int afterIndex = index + 2;
        boolean reorderComboLines = false;


        //if the the same product is on the line before this product but it's not a combo product OR the product before is not the same ID as the split product, reorder so combos come first
        if( index+2 < collection.size() && collection.get(index +2).getId().equals(productCalculation.getId()) && collection.get(index + 2).getComboTransLineItemId() != null  ) {
            reorderComboLines = true;
            productCalculationSplit = collection.subList(0, index +3);
        }

        //add the product to the collection
        productCalculationSplit.add(productCalculation);

        //if we need to reorder the lines so this product is after a combo line, change index
        if(reorderComboLines) {
            afterIndex = index + 4;
        }

        //add the product calculation lines after the split to the list
        if(afterIndex <= collection.size()) {
            List<ProductCalculation> productCalculationAfterSplit = collection.subList(afterIndex, collection.size());
            productCalculationSplit.addAll(productCalculationAfterSplit);
        }

        if(addToSubtotal) {
            addToSubtotal(productCalculation.getLinePrice().multiply(productCalculation.getQuantity()));
        }

        //update the collection
        this.setCollection(productCalculationSplit);
    }

    //clean up if there are product Calculations lines with 0 quantity
    public void removeEmptyQuantityProducts() {
        List<Integer> potentialProductCalculationsToRemove = new ArrayList<Integer>();

        //loop over product calculation lines looking for zero quantity
        for ( int p = 0; p < this.getCollection().size(); p++ ) {
            if(this.getCollection().get(p).getQuantity().compareTo(BigDecimal.ZERO) == 0) {
                potentialProductCalculationsToRemove.add(p);
            }
        }

        //remove from list at the found indexes
        for ( int j = potentialProductCalculationsToRemove.size() - 1; j >= 0; j-- ) {
            Integer indexToRemove = potentialProductCalculationsToRemove.get(j);
            this.getCollection().remove(this.getCollection().get(indexToRemove));
        }
    }

    //updates the product indexes after removing duplicates and 0 quantity products for combos
    public void updateProductCollectionIndexes() {

        //loop over product calculation lines looking for zero quantity
        for ( int p = 0; p < this.getCollection().size(); p++ ) {
            this.getCollection().get(p).setIndex(p);

        }
    }

    private void checkForDiscountsRewardsAlreadyApplied(ProductCalculation productCalculation){
        //Retain the discounts applied
        for (ProductCalculation previousProductCalculation : this.getCollection()){
            if (previousProductCalculation.getId().equals(productCalculation.getId()) && previousProductCalculation.getIndex().equals(productCalculation.getIndex())){
                productCalculation.setDiscountsApplied(previousProductCalculation.getDiscountsApplied());
                productCalculation.setRewardsApplied(previousProductCalculation.getRewardsApplied());
            }
        }
    }

    public BigDecimal getProductSubtotal() {
        return productSubtotal;
    }

    public void setProductSubtotal(BigDecimal productSubtotal) {
        this.productSubtotal = productSubtotal;
    }

    public void addToSubtotal(BigDecimal amount) {
        this.setProductSubtotal(this.getProductSubtotal().add(amount));
    }
}
