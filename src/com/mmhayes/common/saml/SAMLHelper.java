package com.mmhayes.common.saml;

//MMHayes Dependencies

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.MMHServletStarter;
import com.onelogin.saml2.Auth;
import com.onelogin.saml2.settings.Saml2Settings;
import com.onelogin.saml2.settings.SettingsBuilder;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.event.KeyValuePair;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.net.URL;
import java.util.*;

//SAML Dependencies
//Other Dependencies


/*
 $Author: ecdyer $: Author of last commit
 $Date: 2020-05-06 13:34:07 -0400 (Wed, 06 May 2020) $: Date of last commit
 $Rev: 11648 $: Revision of last commit
 Notes: Custom helper object that helps with sending and receiving SAML requests and responses
*/
public class SAMLHelper {

    private static DataManager dm = new DataManager();
    private String nameID = null; //for ACS SAMLResponse Handling - this is USUALLY (depending on the name ID configuration properties) the username of the user we want
    private String sessionIndex = null; //for ACS SAMLResponse Handling - the session index provided from IDP - Required for SLO
    private Map<String, List<String>> attributes = null;  //for ACS SAMLResponse Handling - contains any additional attributes that the IdP wants to send back
    private static final String DEFAULT_RELAY_STATE = "../"; //the root of the this projects war context (usually /qc/index.jsp) should be the default relay state
    private static final String SAML_PROP_FILE_NAME = "onelogin.saml.properties"; //for loading the SAML Property File (required for SAML Library)
    private static final String SAML_PROP_DIR_NAME = "config"; //for loading the SAML Property File (required for SAML Library)
    private static final String SAML_SP_ENTITYURL = "onelogin.saml2.sp.entityid";
    private static final String SAML_SP_ACSURL = "onelogin.saml2.sp.assertion_consumer_service.url";
    private static final String SAML_SP_SLSURL = "onelogin.saml2.sp.single_logout_service.url";
    private static final String SAML_SP_X509CERT = "onelogin.saml2.sp.x509cert";
    private static final String SAML_SP_PRIVATE_KEY = "onelogin.saml2.sp.privatekey";
    private static final String SAML_IDP_ENTITYURL = "onelogin.saml2.idp.entityid";
    private static final String SAML_IDP_SLSURL = "onelogin.saml2.idp.single_logout_service.url";
    private static final String SAML_IDP_X509CERT = "onelogin.saml2.idp.x509cert";
    private static final String SAML_IDP_SSOLOGINURL = "onelogin.saml2.idp.single_sign_on_service.url";
    private static final String SAML_DEBUG = "onelogin.saml2.debug";
    private static final String SAML_STRICT = "onelogin.saml2.strict";
    private static final String SAML_LOGOUT_REQUEST_SIGNED = "onelogin.saml2.security.logoutrequest_signed";
    private static final String SAML_SP_NAMEIDFORMAT = "onelogin.saml2.sp.nameidformat";
    //SC2P: add class-level settings
    private Saml2Settings settings = null;
    private java.util.Properties props = null;

    //constructor - by default attempts to load SAML Properties file first and foremost
    public SAMLHelper() throws Exception {

        //SC2P: remove the properties file entirely, instead use class-level variable
        GenerateSamlSettings(MMHServletStarter.getInstanceIdentifier());
        /*
        //get the property file from the application "config" directory and create an apache commons configuration object
        PropertiesConfiguration propertyFileConfig = new PropertiesConfiguration(new File(determineSAMLPropFilePath()));

        //update the properties file from the SAML settings from the QC DB - takes a instanceIdentifier (this is the name of the war that is running. example "qc" or "myqc")
        updateSAMLPropertiesFromDB(propertyFileConfig, MMHServletStarter.getInstanceIdentifier());
        */

        /* //KEEP COMMENT BLOCK - OLD WAY - modified on 8/30/2017 -jrmitaly
        Note that originally the SAML Properties file was stored in the Classpath itself
        However, because we have to modify the SAML Properties file at run time, from values from our Database,
        This caused a huge issue because the MMHTomcatUser in production does not have access to modify anything in the Classpath
        It is a good thing (From a security point of view) that the MMHTomcat user cannot modify anything in the Classpath
        However, this caused us to figure out a different way of dealing with the SAML Properties file
        We instead choose to move the SAML properties file to MMHayes\application\{warName}\config directory

        The following commented out lines was the "original" way this worked
        //PropertiesConfiguration propertyFileConfig = new PropertiesConfiguration(new File(getClass().getClassLoader().getResource(SAML_PROP_FILE_NAME).getFile()));
        //updateSAMLPropertiesFromDB(propertyFileConfig, MMHServletStarter.getInstanceIdentifier());
        */

    }

    //this method sends a request to an Identity Providers (such as ADFS, for example)
    public void sendRequestToIdP(String relayState, HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            //send login request to IdP with auth.login - pass in relayState if it exists
            //SC2P: use the settings when instantiating the Auth class
            Auth auth = new Auth(this.settings, request, response);
            //Auth auth = new Auth(request, response);
            if (relayState != null && !relayState.isEmpty()) {
                auth.login(relayState);
            } else {
                auth.login(DEFAULT_RELAY_STATE);
            }
        } catch (Exception ex) {
            Logger.logException(ex);
            throw ex;
        }
        Logger.logMessage("Location: "+response.getHeader("Location"), Logger.LEVEL.DEBUG);
    }

    //this method responds to requests sent to /sso/saml/acs - Assertion Consumer Service - this is for handling SAML responses from Identity Providers (such as ADFS, for example)
    public void respondToACSRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            //initialize response body that we will send to our client (if needed)
            StringBuilder responseBody = new StringBuilder();

            //process the SAML response
            //SC2P: use the settings when instantiating the Auth class
            Auth auth = new Auth(this.settings, request, response);
            //Auth auth = new Auth(request, response);
            auth.processResponse();

            //grab any potiential errors from processing the SAML response
            List<String> errors = auth.getErrors();

            //no errors found - create a valid session and then redirect user to relayState
            if (errors.isEmpty()) {
                setNameID(auth.getNameId());
                setSessionIndex(auth.getSessionIndex());
                setAttributes(auth.getAttributes());
            } else { //there are errors, log each error and add to the response
                response.setContentType("text/html; charset=UTF-8");
                for (String error : errors) {
                    Logger.logMessage("Error Found in ACS: "+error, Logger.LEVEL.ERROR);
                    responseBody.append("<p>"+error+"</p>");
                }
                String lastErrorReason = auth.getLastErrorReason();
                if (lastErrorReason != null && !lastErrorReason.isEmpty()) {
                    Logger.logMessage("Last Error Reason Found in ACS: "+lastErrorReason, Logger.LEVEL.ERROR);
                    responseBody.append("<br/> <b>Last Error Reason</b> <br/>");
                    responseBody.append("<p>"+lastErrorReason+"</p>");
                }

                //write out responseBody and return to client
                response.setStatus(response.SC_OK);
                response.getWriter().write(responseBody.toString());
                response.getWriter().flush();
                response.getWriter().close();
            }
        } catch (Exception ex) {
            Logger.logException(ex);
            throw ex;
        }
    }

    //this method responds to requests sent to /sso/saml/metadata - response sent should be XML document containing meta data for the service provider (Quickcharge)
    public void respondToMetaDataRequest(HttpServletResponse response) throws Exception {
        try {
            //initialize response body
            StringBuilder responseBody = new StringBuilder();

            //get the SP Meta Data (service provider meta data)
            Auth auth = new Auth();
            Saml2Settings settings = auth.getSettings();
            String metadata = settings.getSPMetadata();

            //determine if there any potiential errors in the meta data
            List<String> errors = Saml2Settings.validateMetadata(metadata);

            //no errors found - need to send back the metadata
            if (errors.isEmpty()) {
                response.setContentType("application/xhtml+xml; charset=UTF-8");
                responseBody.append(metadata);
            } else { //there are errors, log each error and add to the response
                response.setContentType("text/html; charset=UTF-8");
                for (String error : errors) {
                    Logger.logMessage("Error Found in Metadata: "+error, Logger.LEVEL.ERROR);
                    responseBody.append("<p>"+error+"</p>");
                }
            }

            //write out responseBody and return to client
            response.setStatus(response.SC_OK);
            response.getWriter().write(responseBody.toString());
            response.getWriter().flush();
            response.getWriter().close();
        } catch (Exception ex) {
            Logger.logException(ex);
            throw ex;
        }
    }

    //SC2P: pull the settings from the database and hold in memory instead of writing to a file
    private void GenerateSamlSettings(String instanceIdentifier) throws Exception {
        Map<String, Object> samlData = new HashMap<>();
        //get all models in an array list
        ArrayList<HashMap> globalSSOSettingsList = dm.parameterizedExecuteQuery("data.common.getGlobalSSOSettings",
                new Object[]{},
                true
        );
        //automatic funding list should only contain one account which we will use to populate this model
        if (globalSSOSettingsList != null && globalSSOSettingsList.size() == 1) {
            HashMap globalSSOSettingsHM = globalSSOSettingsList.get(0);
            if (globalSSOSettingsHM != null) {
                //use the QC Base URL (something like https://qc.customer.com OR http://qcserver) to determine the SAML Service Provider (SP) URLs
                String samlSPEntityURL = ""; //the service provider "ID"
                String saml_SP_ACS_URL = ""; //assertion consumer service URL - handles SAML Responses
                String saml_SP_SLS_URL = ""; //single log out service URL - for handling Single Log Out
                if (globalSSOSettingsHM.get("QCBASEURL") != null) {
                    String qcBaseURL = globalSSOSettingsHM.get("QCBASEURL").toString();
                    if (instanceIdentifier.toLowerCase().equals("myqc")){
                        //Use MYQCBASEURL if string is not empty
                        if (!globalSSOSettingsHM.get("MYQCBASEURL").toString().trim().isEmpty()) {
                            qcBaseURL = globalSSOSettingsHM.get("MYQCBASEURL").toString();
                        }
                    }
                    if (qcBaseURL != null && !qcBaseURL.isEmpty() && qcBaseURL.substring(qcBaseURL.length() - 1).equals("/")) { //if there is a trailing "/" in the qcBaseURL, remove it
                        qcBaseURL = qcBaseURL.substring(0, qcBaseURL.length() - 1);
                    }
                    //use the qcBaseURL to properly set the SP URLs
                    samlSPEntityURL = qcBaseURL+"/"+instanceIdentifier+"/sso";
                    saml_SP_ACS_URL = qcBaseURL+"/"+instanceIdentifier+"/sso/acs";
                    saml_SP_SLS_URL = qcBaseURL+"/"+instanceIdentifier+"/sso/acls";
                }
                //set properties from DB in the loaded properties file
                samlData.put(SAML_SP_ENTITYURL, new URL(samlSPEntityURL));
                samlData.put(SAML_SP_ACSURL, new URL(saml_SP_ACS_URL));
                samlData.put(SAML_SP_SLSURL, new URL(saml_SP_SLS_URL));
                samlData.put(SAML_SP_X509CERT, globalSSOSettingsHM.get("SSO_SP_X509CERT"));
                samlData.put(SAML_SP_PRIVATE_KEY, globalSSOSettingsHM.get("SSO_SP_PRIVATE_KEY"));
                samlData.put(SAML_IDP_ENTITYURL, new URL(globalSSOSettingsHM.get("SSO_IDP_ENTITYURL").toString()));
                samlData.put(SAML_IDP_SLSURL, globalSSOSettingsHM.get("SSO_IDP_SLSURL"));
                samlData.put(SAML_IDP_X509CERT, globalSSOSettingsHM.get("SSO_IDP_X509CERT"));
                samlData.put(SAML_IDP_SSOLOGINURL, globalSSOSettingsHM.get("SSO_IDP_SSOLOGINURL"));
                //default SSO_DEBUG property to FALSE
                samlData.put(SAML_DEBUG, (globalSSOSettingsHM.get("SSO_DEBUG").toString().equals("true") ? true : false));
                //default SSO_Strict property to TRUE
                samlData.put(SAML_STRICT, (globalSSOSettingsHM.get("SSO_STRICT").toString().equals("false") ? false : true));
                // Set Logout Property - REQUIRED FOR ADFS
                samlData.put(SAML_LOGOUT_REQUEST_SIGNED, (globalSSOSettingsHM.get("SSO_IDP_SLSURL").toString().isEmpty() ? false : true));
            }
            SettingsBuilder builder = new SettingsBuilder();
            //this.settings = builder.fromValues(samlData).build();
            //SC2P: using the Properties object because the version of onelogin is too old to have the values method
            this.props = new Properties();
            for (Map.Entry<String, Object> setting : samlData.entrySet()) {
                this.props.setProperty(setting.getKey(), setting.getValue().toString());
            }
            this.settings = builder.fromProperties(this.props).build();
        }
    }

    //SC2P: don't use methods that involve files
    /*
    //this method updates the SAML properties found in SAML_PROP_FILE_NAME with information from the Quickcharge Database - takes a instanceIdentifier (this is the name of the war that is running. example "qc" or "myqc")
    private void updateSAMLPropertiesFromDB(PropertiesConfiguration propertyFileConfig, String instanceIdentifier) throws Exception {

        if (propertyFileConfig != null)  {

            //get all models in an array list
            ArrayList<HashMap> globalSSOSettingsList = dm.parameterizedExecuteQuery("data.common.getGlobalSSOSettings",
                    new Object[]{},
                    true
            );

            //automatic funding list should only contain one account which we will use to populate this model
            if (globalSSOSettingsList != null && globalSSOSettingsList.size() == 1) {
                HashMap globalSSOSettingsHM = globalSSOSettingsList.get(0);
                if (globalSSOSettingsHM != null) {

                    //use the QC Base URL (something like https://qc.customer.com OR http://qcserver) to determine the SAML Service Provider (SP) URLs
                    String samlSPEntityURL = ""; //the service provider "ID"
                    String saml_SP_ACS_URL = ""; //assertion consumer service URL - handles SAML Responses
                    String saml_SP_SLS_URL = ""; //single log out service URL - for handling Single Log Out
                    if (globalSSOSettingsHM.get("QCBASEURL") != null) {
                        String qcBaseURL = globalSSOSettingsHM.get("QCBASEURL").toString();
                        if(instanceIdentifier.toLowerCase().equals("myqc")){
                            //Use MYQCBASEURL if string is not empty
                            if(!globalSSOSettingsHM.get("MYQCBASEURL").toString().trim().isEmpty()) {
                                qcBaseURL = globalSSOSettingsHM.get("MYQCBASEURL").toString();
                            }
                        }
                        if (qcBaseURL != null && !qcBaseURL.isEmpty() && qcBaseURL.substring(qcBaseURL.length() - 1).equals("/")) { //if there is a trailing "/" in the qcBaseURL, remove it
                            qcBaseURL = qcBaseURL.substring(0, qcBaseURL.length() - 1);
                        }

                        //use the qcBaseURL to properly set the SP URLs
                        samlSPEntityURL = qcBaseURL+"/"+instanceIdentifier+"/sso";
                        saml_SP_ACS_URL = qcBaseURL+"/"+instanceIdentifier+"/sso/acs";
                        saml_SP_SLS_URL = qcBaseURL+"/"+instanceIdentifier+"/sso/acls";
                    }

                    //set properties from DB in the loaded properties file
                    propertyFileConfig.setProperty(SAML_SP_ENTITYURL, samlSPEntityURL);
                    propertyFileConfig.setProperty(SAML_SP_ACSURL, saml_SP_ACS_URL);
                    propertyFileConfig.setProperty(SAML_SP_SLSURL, saml_SP_SLS_URL);
                    propertyFileConfig.setProperty(SAML_SP_X509CERT, globalSSOSettingsHM.get("SSO_SP_X509CERT"));
                    propertyFileConfig.setProperty(SAML_SP_PRIVATE_KEY, globalSSOSettingsHM.get("SSO_SP_PRIVATE_KEY"));
                    propertyFileConfig.setProperty(SAML_IDP_ENTITYURL, globalSSOSettingsHM.get("SSO_IDP_ENTITYURL"));
                    propertyFileConfig.setProperty(SAML_IDP_SLSURL, globalSSOSettingsHM.get("SSO_IDP_SLSURL"));
                    propertyFileConfig.setProperty(SAML_IDP_X509CERT, globalSSOSettingsHM.get("SSO_IDP_X509CERT"));
                    propertyFileConfig.setProperty(SAML_IDP_SSOLOGINURL, globalSSOSettingsHM.get("SSO_IDP_SSOLOGINURL"));

                    //default SSO_DEBUG property to FALSE
                    if (globalSSOSettingsHM.get("SSO_DEBUG").toString().equals("true")) {
                        propertyFileConfig.setProperty(SAML_DEBUG, "true");
                    } else {
                        propertyFileConfig.setProperty(SAML_DEBUG, "false");
                    }

                    //default SSO_Strict property to TRUE
                    if (globalSSOSettingsHM.get("SSO_STRICT").toString().equals("false")) {
                        propertyFileConfig.setProperty(SAML_STRICT, "false");
                    } else {
                        propertyFileConfig.setProperty(SAML_STRICT, "true");
                    }

                    // Set Logout Property - REQUIRED FOR ADFS
                    if(globalSSOSettingsHM.get("SSO_IDP_SLSURL").toString().isEmpty()){
                        propertyFileConfig.setProperty(SAML_LOGOUT_REQUEST_SIGNED, false);
                    } else {
                        propertyFileConfig.setProperty(SAML_LOGOUT_REQUEST_SIGNED, true);
                    }
                }
            }

            //save all the changes made to the property file
            propertyFileConfig.save();
        } else {
            Logger.logMessage("Invalid property file config in updateSAMLPropertiesFromDB.", Logger.LEVEL.ERROR);
        }
    }

    //this method determines the location (on the hard disk) of the SAML Property File - note that the SAML library has to do this as well
    private String determineSAMLPropFilePath() throws Exception {
        String SAMLPropFilePath = null;
        try {
            Logger.logMessage("Attempting to determine the SAML Property File Path...");

            String instanceIdentifier = determineInstanceIdentifierWithoutServlet();

            if (instanceIdentifier != null && !instanceIdentifier.isEmpty()) {

                //determine the "MMHayes Home" path from environment variable MMH_HOME
                String mmhHomePath = null; //usually C:/MMHayes/
                if (System.getenv("MMH_HOME") != null && System.getenv("MMH_HOME").length() > 0) {
                    mmhHomePath = System.getenv("MMH_HOME");
                } else { // otherwise default to c drive
                    mmhHomePath = "C:"+File.separator+"MMHayes"+File.separator;
                }
                Logger.logMessage("MMHayes Home Path determined: "+mmhHomePath);

                //determine the "Application Home" path
                String applicationHomePath = null; //usually C:/MMHayes/application/{warName}/
                if (mmhHomePath != null && !mmhHomePath.isEmpty()) {
                    applicationHomePath = mmhHomePath+"application"+File.separator+instanceIdentifier+File.separator;
                }
                Logger.logMessage("Application Home Path determined: "+applicationHomePath);

                //determine the SAML Property File Path
                if (applicationHomePath != null && !applicationHomePath.isEmpty()) {
                    SAMLPropFilePath = applicationHomePath+SAML_PROP_DIR_NAME+File.separator+SAML_PROP_FILE_NAME;
                }
                Logger.logMessage("SAML Prop File Path determined: "+SAMLPropFilePath);

            }
        } catch (Exception ex) {
            Logger.logException(ex);
            throw ex;
        }
        return SAMLPropFilePath;
    }
    */

    //this method determines the instance identifier (the name of the war - such as "qc" or "myqc") without the need for servlet
    private String determineInstanceIdentifierWithoutServlet() throws Exception {
        String instanceIdentifier = null;
        try {
            Logger.logMessage("Attempting to determine the instance identifier without servlet...");

            //determine the "root" file path of the Java Class loader
            ClassLoader loader = SAMLHelper.class.getClassLoader();
            String javaClassPathRootLocation = loader.getResource("").toString(); //file:/C:/IdeaProjects/Kryptonite_9.0.0/out/artifacts/QC900_war_exploded/WEB-INF/classes/
            Logger.logMessage("Java Class Path Root Location determined: "+javaClassPathRootLocation);

            //split the "root" file path up into parts
            String[] javaClassPathRootLocationParts = javaClassPathRootLocation.split("/");

            //determine what index the "WEB-INF" directory is in the javaClassPathRootLocationParts array
            //NOTE: we are completely relying on the fact that the directory ABOVE this one will be the name of the war (the instance identifier)
            int indexOfWebInf = ArrayUtils.indexOf(javaClassPathRootLocationParts, "WEB-INF");

            //assume that the instance identifier is one index BEFORE the "WEB-INF" directory on the file system
            instanceIdentifier = javaClassPathRootLocationParts[indexOfWebInf-1];

            //for archived wars, there may be a string ".war*" that we will want to remove
            String archivedWarNameClutter = ".war\\*";
            instanceIdentifier = instanceIdentifier.replaceAll(archivedWarNameClutter, "");

            Logger.logMessage("Instance Identifier determined: "+instanceIdentifier);

        } catch (Exception ex) {
            Logger.logException(ex);
            throw ex;
        }

        return instanceIdentifier;
    }

    public String getNameID() {
        return nameID;
    }

    public void setNameID(String nameID) {
        this.nameID = nameID;
    }

    public String getSessionIndex() {
        return sessionIndex;
    }

    public void setSessionIndex(String sessionIndex) {
        this.sessionIndex = sessionIndex;
    }

    public Map<String, List<String>> getAttributes() {
        return attributes;
    }

    public void setAttributes(Map<String, List<String>> attributes) {
        this.attributes = attributes;
    }

}

