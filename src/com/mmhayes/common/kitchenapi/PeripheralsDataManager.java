package com.mmhayes.common.kitchenapi;

//mmhayes dependencies

import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.conn_pool.JDCConnection;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.DynamicSQL;
import com.mmhayes.common.dataaccess.commonMMHFunctions;
import com.mmhayes.common.kitchenPrinters.*;
import com.mmhayes.common.kms.receiptGen.customerReceipt.CustomerReceiptData;
import com.mmhayes.common.kms.receiptGen.errorReceipt.EmailedErrorReceipt;
import com.mmhayes.common.kms.receiptGen.errorReceipt.ErrorReceipt;
import com.mmhayes.common.kms.receiptGen.errorReceipt.PrintedErrorReceipt;
import com.mmhayes.common.printing.PrintJobStatusType;
import com.mmhayes.common.printing.PrintStatusType;
import com.mmhayes.common.receiptGen.Commands.JPOS_COMMAND;
import com.mmhayes.common.receiptGen.Formatter.*;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.receiptGen.Models.CcProcessorReceiptDetail;
import com.mmhayes.common.receiptGen.Models.ReceiptModel;
import com.mmhayes.common.receiptGen.Models.TransactionReceiptModel;
import com.mmhayes.common.receiptGen.ReceiptData.*;
import com.mmhayes.common.receiptGen.ReceiptGen;
import com.mmhayes.common.receiptGen.Renderer.IRenderer;
import com.mmhayes.common.receiptGen.TypeData;
import com.mmhayes.common.transaction.models.TenderLineItemModel;
import com.mmhayes.common.utils.*;
import com.mmhayes.common.xmlapi.XmlRpcUtil;
import jpos.JposException;
import jpos.POSPrinter;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.math.NumberUtils;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

//import javax.comm.SerialPort;

//other dependencies

/*
 $Author: eglundin $: Author of last commit
 $Date: 2019-12-20 15:25:38 -0500 (Fri, 20 Dec 2019) $: Date of last commit
 $Rev: 44790 $: Revision of last commit
 Notes:
*/
public abstract class PeripheralsDataManager extends ReflectiveInvocationHandlerTrackStats {
    public static final String NCR_PRINTER_NAME = "NCR";
    public static String NCR_LOGICAL_PRINTER_NAME = "Printer7197";
    public static final String EPSON_PRINTER_NAME = "Epson";
    public static final String EPSON_LOGICAL_PRINTER_NAME = "EpsonPrinter";
    public static final String SNBC_PRINTER_NAME = "SNBC";
    public static final String SNBC_LOGICAL_PRINTER_NAME = "SNBC_BK-T680_Printer";
    public static final String OTHER_PRINTER_NAME = "GenericPrinter";

    protected DataManager dm;
    protected static boolean isNCRPrinterConnected = false;
    protected static boolean isEpsonPrinterConnected = false;
    private static boolean isGenericPrinterConnected = false;
    protected static boolean isSNBCPrinterConnected = false;
    protected static JPosReceiptPrinter printer = null;
    protected final Object printerLockObject = new Object();
//    private static CashDrawer ncrCashDrawer;
//    private static boolean isNCRDrawerAConnected = false;
//    private static CashDrawer ncrCashDrawerB;
//    private static boolean isNCRDrawerBConnected = false;
//    private static CashDrawer cashDrawer;
//    private static boolean isCDConnected = false;
//    private static CashDrawer cashDrawerB;
//    private static boolean isCDBConnected = false;
//    private static NCRLineDisplay ncrLD;
//    private static boolean isNCRLDConnected = false;
//    private static BarcodeScanner barcodeScannerA;
//    private static boolean isScannerAConnected = false;
//    private static BarcodeScanner barcodeScannerB;
//    private static boolean isScannerBConnected = false;
//    private static BadgeReader badgeRdrA;
//    private static boolean isBadgeRdrAConnected = false;
//    private static BadgeReader badgeRdrB;
//    private static boolean isBadgeRdrBConnected = false;
//    private static Scale scale;
//    private static boolean isScaleConnected = false;
//    private static MagtekIpad ipad;
//    private static boolean isIPadConnected = false;
    protected static final String logFileName = "QCPOS_Periph.log";

    protected static ArrayList badgeRdrIDs = new ArrayList();
    protected static HashMap<Long, Double> scaleThreadIDs = new HashMap<Long, Double>();
    protected static HashMap<Long, Double> barcodeScannerThreadIDs = new HashMap<Long, Double>();
    private static HashMap<Long, Double> ipadThreadIDs = new HashMap<Long, Double>();

    private final static int DETAIL_0_TRANSTYPEID = 0;
    private final static int DETAIL_1_TRAININGTRANSTYPEID = 1;
    private final static int DETAIL_2_INTRAININGMODE = 2;
    private final static int DETAIL_3_ISMERCHANTCOPY = 3;
    private final static int DETAIL_4_RECEIPTHEADER = 4;
    private final static int DETAIL_5_TERMINALID = 5;
    private final static int DETAIL_6_NEWTRANSID = 6;
    private final static int DETAIL_7_ORIGTRANSID = 7;
    private final static int DETAIL_8_CASHIERNAME = 8;
    private final static int DETAIL_9_DATE = 9;
    private final static int DETAIL_10_TIME = 10;
    private final static int DETAIL_11_TRANSARGS = 11;
    private final static int DETAIL_12_SUBTOTAL = 12;
    private final static int DETAIL_13_PURCHASETOTAL = 13;
    private final static int DETAIL_14_CHANGE = 14;
    private final static int DETAIL_15_RECEIPTFOOTER = 15;

    private final static int SHIFTDETAIL_0_AMOUNT = 0;
    private final static int SHIFTDETAIL_1_DISCOUNTTYPEID = 1;
    private final static int SHIFTDETAIL_2_ELIGIBLEAMT = 2;
    private final static int SHIFTDETAIL_3_ISSUBTOTALDISCOUNT = 3;
    private final static int SHIFTDETAIL_4_ITEMNAME = 4;
    private final static int SHIFTDETAIL_5_PAITEMID = 5;
    private final static int SHIFTDETAIL_6_PAITEMTYPEID = 6;
    private final static int SHIFTDETAIL_7_PATENDERTYPEID = 7;
    private final static int SHIFTDETAIL_8_QUANTITY = 8;
    private final static int SHIFTDETAIL_9_TRANSCOUNT = 9;

    protected static final int MAC_ADDRESS_LENGTH = 12;

    protected final BASE64Encoder encoder = new BASE64Encoder();
    protected final BASE64Decoder decoder = new BASE64Decoder();

    protected KitchenPrinterTerminal kitchenPrinterTerminal = null;

    // Email Settings
    protected String emailFromAddress;
    protected String emailServer;
    protected String emailPort;
    protected String emailUser;
    protected String emailPassword;

    /**
     * <p>Constructor for a {@link PeripheralsDataManager}.</p>
     *
     */
    public PeripheralsDataManager () {
        // create a new DataManager
        dm = new DataManager();

    }

    public String getKPLogFileName()
    {
        String kpLogFileName = "KitchenPrinter.log";
        if (kitchenPrinterTerminal != null)
        {
            kpLogFileName = kitchenPrinterTerminal.getKptLog();
        }

        return kpLogFileName;
    }

    //--------------------------------------------------------------------------
    //  region PRINTER METHODS
    //--------------------------------------------------------------------------
    protected static boolean createPrinterIfNeeded()
    {
        if(printer == null)
        {
            Logger.logMessage("createPrinterIfNeeded() - printer is null, calling openConnToPrinter() ", logFileName, Logger.LEVEL.DEBUG);
            openConnToPrinter();
        }

        return  isNCRPrinterConnected || isEpsonPrinterConnected;
    }

    protected static void openConnToPrinter()
    {
        if (MMHProperties.getPeripheralsSetting("ncrReceiptPrinter.connected").toLowerCase().equals("yes") == true)
        {
            if(openConnToPrinter(NCR_PRINTER_NAME, NCR_LOGICAL_PRINTER_NAME))
            {
                isNCRPrinterConnected = true;
            }
            else
            {
                Logger.logMessage("openConnToPrinter("+NCR_PRINTER_NAME+") returned false" , logFileName, Logger.LEVEL.DEBUG);
            }
        }

        if (MMHProperties.getPeripheralsSetting("ncr7199ReceiptPrinter.connected").toLowerCase().equals("yes") == true)
        {
            NCR_LOGICAL_PRINTER_NAME = "Printer7199";

            if(openConnToPrinter(NCR_PRINTER_NAME, NCR_LOGICAL_PRINTER_NAME))
            {
                isNCRPrinterConnected = true;
            }
            else
            {
                Logger.logMessage("openConnToPrinter("+NCR_PRINTER_NAME+") returned false" , logFileName, Logger.LEVEL.DEBUG);
            }
        }

        if (MMHProperties.getPeripheralsSetting("epsonReceiptPrinter.connected").toLowerCase().equals("yes") == true)
        {
            if(openConnToPrinter(EPSON_PRINTER_NAME, EPSON_LOGICAL_PRINTER_NAME))
            {
                isEpsonPrinterConnected = true;
            }
            else
            {
                Logger.logMessage("openConnToPrinter("+EPSON_PRINTER_NAME+") returned false" , logFileName, Logger.LEVEL.DEBUG);
            }
        }

        // We can accommodate other javaPos printers or Esc/Pos printers here
        if(MMHProperties.getPeripheralsSetting("otherPrinter").equals("") == false)
        {
            if (!isNCRPrinterConnected && !isEpsonPrinterConnected)
            {
                String escPosPrinterCfg = MMHProperties.getPeripheralsSetting("otherPrinter");
                if (escPosPrinterCfg != null && escPosPrinterCfg.length() > 1)
                {
                    if (openConnToPrinter(OTHER_PRINTER_NAME, escPosPrinterCfg))
                    {
                        isEpsonPrinterConnected = true;  // we will treat as Epson
                        isGenericPrinterConnected = true;
                    }
                    else
                    {
                        Logger.logMessage("openConnToPrinter(" + OTHER_PRINTER_NAME + ") returned false", logFileName, Logger.LEVEL.DEBUG);

                        // Set printer as null
                        // For Pioneer USB printers using a virtual COM port, we want to try and reconnect later when a print request is sent,
                        // so set it as null
                        printer = null;
                    }
                }
            }
        }

        if (MMHProperties.getPeripheralsSetting("snbcReceiptPrinter.connected").toLowerCase().equals("yes") == true)
        {
            if(openConnToPrinter(SNBC_PRINTER_NAME, SNBC_LOGICAL_PRINTER_NAME))
            {
                isSNBCPrinterConnected = true;
            }
            else
            {
                Logger.logMessage("openConnToPrinter("+SNBC_PRINTER_NAME+") returned false" , logFileName, Logger.LEVEL.DEBUG);
            }
        }
    }

    public static synchronized boolean openConnToPrinter(String sPrinterName, String sLogicalPrinterName)
    {
        Logger.logMessage("PeripheralsDataManager.openConnToPrinter(" + sPrinterName + "," + sLogicalPrinterName + ")", logFileName, Logger.LEVEL.ERROR);

        // Create the printer object
        if(printer == null)
        {
            printer = new JPosReceiptPrinter(sPrinterName, sLogicalPrinterName, "QCPOS_Periph.log");
        }

        // Try to open the printer
        // If we're not successful, set the printer obj to null and return false
        if(!printer.openPrinter())
        {
            try
            {
                printer.closePrinter();
            }
            catch (Exception e)
            {
                Logger.logException("Failed to close printer", logFileName, e);
            }
            printer = null;
            return false;
        }

        return true;
    }

    public boolean closeConnToPrinter()
    {
        // Don't do anything if the printer obj is null
        if (printer == null)
        {
            return false;
        }

        // Otherwise, try to close the connection to the printer
        synchronized (printer)
        {
            return printer.closePrinter();
        }
    }

    public void printNutritionInfo(ArrayList items, ArrayList categoryNames, ArrayList nutriTotals, boolean needsNAFootnote, boolean needsTruncateDigitsFootnote) throws IOException, JposException
    {
        if (printer != null)
        {
            if(isEpsonPrinterConnected)
            {
                printer.epsonPrintNutritionalInfo(items, categoryNames, nutriTotals, needsNAFootnote, needsTruncateDigitsFootnote);
            }
            else if(isNCRPrinterConnected || isSNBCPrinterConnected)
            {
                printer.NCRPrintNutritionalInfo(items, categoryNames, nutriTotals, needsNAFootnote, needsTruncateDigitsFootnote);
            }
        }
        else
        {
            Logger.logMessage("printNutritionInfo - error: printer is null", logFileName, Logger.LEVEL.ERROR);
        }
    }

    public void printChit(ArrayList headerLines, String tranDate, String cashierName, String trxTypeAndID, String trxTime, String terminalID, String orderNumber, String customerName, ArrayList footerLines)
    {
        createPrinterIfNeeded();

        POSPrinter p = null;
        if (printer != null)
        {
            ReceiptGen rg = new ReceiptGen();
            p = printer.getPOSPrinter();
            IRenderer printerRenderer = null;
            if (null != p)
            {
                printerRenderer = rg.createPrinterRenderer(p, printer.getLogicalPrinterName());
            }
            else
            {
                Logger.logMessage("printChit - error Printer is NULL!", logFileName, Logger.LEVEL.ERROR);
            }

            // print the receipt
            if (printerRenderer != null)
            {
                // if begin fails we can't continue
                if (!printerRenderer.begin())
                {
                    Logger.logMessage("printChit - printer.Begin failed, printer may be in error state!", logFileName, Logger.LEVEL.ERROR);
                    return;
                }

                int itemCount = 0;

                if(headerLines != null)
                {
                    for (Object aReceiptHeader : headerLines)
                    {
                        printerRenderer.centerLine(aReceiptHeader.toString());
                    }
                }

                // print the header
                printerRenderer.addBlankLine();
                printerRenderer.add2ColumnLine(tranDate, trxTime);
                printerRenderer.add2ColumnLine("Cashier: " + cashierName, "TID: " + terminalID);
                printerRenderer.add2ColumnLine(trxTypeAndID, "");
                printerRenderer.addBlankLine();

                // Print order number and customer name
                printerRenderer.centerLine("Order Number: " + printerRenderer.large(orderNumber));
                printerRenderer.centerLine("Customer Name: " + printerRenderer.large(customerName));

                printerRenderer.addBlankLine();

                if(null != footerLines)
                {
                    for (Object aReceiptHeader : footerLines)
                    {
                        printerRenderer.centerLine(aReceiptHeader.toString());
                    }
                }

                // finish the receipt
                printerRenderer.addBlankLine();
                printerRenderer.cutPaper();
                printerRenderer.end();
            }
            else
            {
                Logger.logMessage("printChit - error unable to create PrinterRenderer!", logFileName, Logger.LEVEL.ERROR);
            }
        }
    }

    public void printConsolidatedReceipt(ArrayList receiptDetails, boolean printingNutritionalInfo, ArrayList items, ArrayList categoryNames, ArrayList nutriTotals, boolean needsNAFootnote, boolean needsTruncateDigitsFootnote, String emailAddress, boolean printReceipt, ArrayList loyaltySummaryProgramData) throws Exception
    {
        printConsolidatedReceipt(receiptDetails, printingNutritionalInfo, items, categoryNames, nutriTotals, needsNAFootnote, needsTruncateDigitsFootnote, emailAddress, printReceipt, loyaltySummaryProgramData, false, false, "", "", false, false);
    }

    @SuppressWarnings({"unchecked", "MagicNumber"})
    public void printConsolidatedReceipt(ArrayList receiptDetails, boolean printingNutritionalInfo, ArrayList items, ArrayList categoryNames, ArrayList nutriTotals, boolean needsNAFootnote, boolean needsTruncateDigitsFootnote, String emailAddress, boolean printReceipt, ArrayList loyaltySummaryProgramData, boolean printOrderNumber, boolean isGiftReceipt, String suspTransNameLbl, String suspTransName , Boolean printGrossWeight, Boolean printNetWeight) throws Exception
    {
        try
        {
            ReceiptGen rcptGen = new ReceiptGen();
            if(isGiftReceipt)
            {
                rcptGen.setReceiptFormatter(new GiftReceiptFormatter());
            }
            if (rcptGen != null)
            {
                ReceiptData curReceiptData = rcptGen.createReceiptData();
                curReceiptData.setPrintGrossWeight(printGrossWeight);
                curReceiptData.setPrintNetWeight(printNetWeight);
                rcptGen.setReceiptData(curReceiptData);
            }

            // encapsulate the nutrition info
            ArrayList nutritionInfo = new ArrayList();
            if(printingNutritionalInfo)
            {
                nutritionInfo.add(items);
                nutritionInfo.add(categoryNames);
                nutritionInfo.add(nutriTotals);
                nutritionInfo.add(needsNAFootnote);
                nutritionInfo.add(needsTruncateDigitsFootnote);
            }

            // If there's an email address, email the receipt
            if ( (!emailAddress.isEmpty()) && (emailAddress!=null) && (!emailAddress.equals("null")) )
            {
                // If we need to pull email settings, do that
                if (emailServer == null)
                {
                    setEmailSettings();
                }

                //Build render for html receipt
                IRenderer htmlRenderer = rcptGen.createBasicHTMLRenderer();
                if(emailServer.isEmpty())
                {
                    Logger.logMessage("In QC_Globals SMTPSERVER is empty and this is required to send email", logFileName, Logger.LEVEL.TRACE);
                }
                else
                {
                    // Update the receiptDetails so that a CUSTOMER COPY is always emailed
                    receiptDetails.set(3, false);

                    // Try to email the receipt
                    if (htmlRenderer != null && rcptGen.buildFromArrayList(receiptDetails, nutritionInfo, loyaltySummaryProgramData, printOrderNumber, suspTransNameLbl, suspTransName, TypeData.ReceiptType.EMAILEDRECEIPT, htmlRenderer))
                    {
                        if (emailUser.isEmpty())
                        {
                            Logger.logMessage("In PrintConsolidatedReceipt, about to send unauthenticated email receipt to " + emailAddress + " using mail server " + emailServer + " and port " + emailPort + ". ", logFileName, Logger.LEVEL.TRACE);
                            MMHEmail.sendEmail(Integer.parseInt(emailPort), emailServer, emailAddress, emailFromAddress, "Your receipt", htmlRenderer.toString(), "text/html");
                        }
                        else
                        {
                            Logger.logMessage("In PrintConsolidatedReceipt, about to send authenticated email receipt to " + emailAddress + " using mail server " + emailServer + " and port " + emailPort + ". ", logFileName, Logger.LEVEL.TRACE);
                            MMHEmail.sendAuthEmail(Integer.parseInt(emailPort), emailServer, emailUser, emailPassword, emailAddress, emailFromAddress, "Your receipt", htmlRenderer.toString(), "text/html");
                        }
                    }
                }
            }
            else
            {
                // Print the receipt
                String retVal = "";
                POSPrinter ptr = null;
                if (printer != null)
                {
                    if (printReceipt)
                    {
                        ptr = printer.getPOSPrinter();
                        IRenderer printerRenderer = null;
                        if (ptr != null)
                        {
                            printerRenderer = rcptGen.createPrinterRenderer(ptr, printer.getLogicalPrinterName());
                        }

                        // print the receipt
                        if (printerRenderer != null)
                        {
                            rcptGen.buildFromArrayList(receiptDetails, nutritionInfo, loyaltySummaryProgramData, printOrderNumber,
                                    suspTransNameLbl, suspTransName, TypeData.ReceiptType.PRINTEDRECEIPT, printerRenderer);
                            if(printerRenderer.getLastException() != null)
                            {
                                throw printerRenderer.getLastException();
                            }
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            Logger.logException("PeripheralsDataManager.printConsolidatedReceipt error", logFileName, e);
        }
    }

    public boolean printBatchSummary(ArrayList batchSummary) throws JposException {
        boolean result = false;
        Logger.logMessage("PeripheralsDataManager.printBatchSummary()", logFileName, Logger.LEVEL.TRACE);

        createPrinterIfNeeded();

        if (printer != null) {
            synchronized (printer) {
                if (!printer.isInErrorState())
                    result = printer.printBatchSummary(batchSummary);
            }
        }

        return result;
    }

    public void printMissingItemSlip(String pluCode, String timeDate) throws Exception {
        createPrinterIfNeeded();

        if (printer != null) {
            synchronized (printer) {
                if (!printer.isInErrorState())
                    printer.printMissingItemSlip(pluCode, timeDate);
            }
        } else
            Logger.logMessage("printMissingItemSlip - error: printer is null", logFileName, Logger.LEVEL.ERROR);

    }

    public boolean printNoSale(String reasonCode, String date, String cashierName) throws JposException {
        boolean result = false;
        Logger.logMessage("PeripheralsDataManager.printNoSale(" + reasonCode + "," + date + "," + cashierName + ")", logFileName, Logger.LEVEL.DEBUG);
        createPrinterIfNeeded();

        if (printer != null) {
            synchronized (printer) {
                if (!printer.isInErrorState())
                    result = printer.noSaleAdvPrint(reasonCode, date, cashierName);
            }
        } else
            Logger.logMessage("printNoSale - error: printer is null", logFileName, Logger.LEVEL.ERROR);


        return result;
    }

    public boolean printBlindCloseReceipt(String cashierName, String date) throws JposException {
        boolean result = false;
        Logger.logMessage("PeripheralsDataManager.printBlindCloseReceipt(" + cashierName + "," + date + ")", logFileName, Logger.LEVEL.DEBUG);

        createPrinterIfNeeded();

        if (printer != null) {
            synchronized (printer) {
                boolean isInErrorState = printer.isInErrorState();
                if (!isInErrorState) {
                    result = printer.printBlindCloseReceipt(cashierName, date);
                }
            }
        }

        return result;
    }

    public void printCashierEndShiftReceipt(ArrayList cashierZDetails, boolean isXout) throws JposException
    {
        Logger.logMessage("PeripheralsDataManager.printCashierEndShiftReceipt", logFileName, Logger.LEVEL.TRACE);
        String cashierName = cashierZDetails.get(0).toString();
        String transactionDate = cashierZDetails.get(1).toString();
        String terminalName = cashierZDetails.get(2).toString();
        String newTransID = cashierZDetails.get(3).toString();
        String previousTransID = cashierZDetails.get(4).toString();
        String previousZDate = cashierZDetails.get(5).toString();
        String startTransID = cashierZDetails.get(6).toString();
        String startTransDate = cashierZDetails.get(7).toString();
        String totalGrossSum = cashierZDetails.get(8).toString();
        String totalItemDscts = cashierZDetails.get(9).toString();
        String runningTotalOne = cashierZDetails.get(10).toString();
        String totalSubDscts = cashierZDetails.get(11).toString();
        String runningTotalTwo = cashierZDetails.get(12).toString();
        String totalTax = cashierZDetails.get(13).toString();
        String runningTotalThree = cashierZDetails.get(14).toString();
        int customerCount = new Integer(cashierZDetails.get(15).toString()).intValue();
        String totalRA = cashierZDetails.get(16).toString();
        String totalPO = cashierZDetails.get(17).toString();
        String runningTotalFour = cashierZDetails.get(18).toString();
        String totalTender = cashierZDetails.get(19).toString();
        int noSalesQty = new Integer(cashierZDetails.get(20).toString()).intValue();
        int priceOverridesQty = new Integer(cashierZDetails.get(21).toString()).intValue();
        int voidModeQty = new Integer(cashierZDetails.get(22).toString()).intValue();
        String voidModeAmt = cashierZDetails.get(23).toString();
        int refundModeQty = new Integer(cashierZDetails.get(24).toString()).intValue();
        String refundModeAmt = cashierZDetails.get(25).toString();
        int indirectVoidsQty = new Integer(cashierZDetails.get(26).toString()).intValue();
        String indirectVoidsAmt = cashierZDetails.get(27).toString();
        int directVoidsQty = new Integer(cashierZDetails.get(28).toString()).intValue();
        String directVoidsAmt = cashierZDetails.get(29).toString();
        int cancelTransQty = new Integer(cashierZDetails.get(30).toString()).intValue();
        String cancelTransAmt = cashierZDetails.get(31).toString();
        ArrayList taxes = (ArrayList)cashierZDetails.get(32);
        ArrayList itemCouponDscts = (ArrayList)cashierZDetails.get(33);
        ArrayList itemPercentDscts = (ArrayList)cashierZDetails.get(34);
        ArrayList subtotalCouponDscts = (ArrayList)cashierZDetails.get(35);
        ArrayList subtotalPercentDscts = (ArrayList)cashierZDetails.get(36);
        ArrayList paidOut = (ArrayList)cashierZDetails.get(37);
        ArrayList receivedOnAcct = (ArrayList)cashierZDetails.get(38);
        ArrayList tenders = (ArrayList)cashierZDetails.get(39);
        String drawerNum = cashierZDetails.get(40).toString();
        String totalTaxExempt = cashierZDetails.get(41).toString();
        ArrayList taxExempts = (ArrayList)cashierZDetails.get(42);
        ArrayList transCreditRewards = (ArrayList)cashierZDetails.get(43);
        String totalTransCreds = cashierZDetails.get(44).toString();
        ArrayList freeProductRewards = (ArrayList)cashierZDetails.get(45);
        String totalFreeProducts = cashierZDetails.get(46).toString();
        String runningTotalTwoA = cashierZDetails.get(47).toString();
        int numOfflineCreditCardCharges = Integer.parseInt(cashierZDetails.get(48).toString());
        boolean isPrinterInErrorState = false;

        createPrinterIfNeeded();

        if (printer != null)
        {
            synchronized (printer)
            {
                if (!printer.isInErrorState())
                {
                    printer.turnOnTransactionPrinting();

                    printer.printCashierReportHeader(cashierName, transactionDate, terminalName, newTransID, previousTransID, previousZDate, startTransID, startTransDate, drawerNum, isXout);
                    printer.printCashierReportItem("GROSS TOTAL", totalGrossSum);

                    if (!itemCouponDscts.isEmpty())
                    {
                        printer.printNewLine();
                        printer.printCashierReportItemHeading("ITEM COUPON DISCOUNTS");

                        for (Object itemCouponDsct : itemCouponDscts)
                        {
                            ArrayList item = (ArrayList) itemCouponDsct;
                            printer.printCashierReportItem(item.get(SHIFTDETAIL_4_ITEMNAME).toString(), "");
                            printer.printCashierReportSubItemIndented("QTY", item.get(SHIFTDETAIL_8_QUANTITY).toString());
                            printer.printCashierReportSubItemIndented("AMT", item.get(SHIFTDETAIL_0_AMOUNT).toString());
                        }
                    }
                    if (!itemPercentDscts.isEmpty())
                    {
                        printer.printNewLine();
                        printer.printCashierReportItemHeading("ITEM PERCENT DISCOUNTS");

                        for (Object itemPercentDsct : itemPercentDscts)
                        {
                            ArrayList item = (ArrayList) itemPercentDsct;
                            printer.printCashierReportItem(item.get(SHIFTDETAIL_4_ITEMNAME).toString(), "");
                            printer.printCashierReportSubItemIndented("QTY", item.get(SHIFTDETAIL_8_QUANTITY).toString());
                            printer.printCashierReportSubItemIndented("AMT", item.get(SHIFTDETAIL_0_AMOUNT).toString());
                        }
                    }
                    if (!itemCouponDscts.isEmpty() || !itemPercentDscts.isEmpty())
                    {
                        printer.printNewLine();
                        printer.printCashierReportItem("TOTAL ITEM DSCTS", totalItemDscts);
                    }

                    printer.printNewLine();
                    printer.printCashierReportItem("RUNNING TOTAL 1", runningTotalOne);

                    if (!subtotalCouponDscts.isEmpty())
                    {
                        printer.printNewLine();
                        printer.printCashierReportItemHeading("SUBTOTAL COUPON DSCTS & MEAL PLANS");

                        for (Object subtotalCouponDsct : subtotalCouponDscts)
                        {
                            ArrayList item = (ArrayList) subtotalCouponDsct;
                            printer.printCashierReportItem(item.get(SHIFTDETAIL_4_ITEMNAME).toString(), "");
                            printer.printCashierReportSubItemIndented("QTY", item.get(SHIFTDETAIL_8_QUANTITY).toString());
                            printer.printCashierReportSubItemIndented("AMT", item.get(SHIFTDETAIL_0_AMOUNT).toString());
                            printer.printCashierReportSubItemIndented("ELIGIBLE AMT", item.get(SHIFTDETAIL_2_ELIGIBLEAMT).toString());
                        }
                    }
                    if (!subtotalPercentDscts.isEmpty())
                    {
                        printer.printNewLine();
                        printer.printCashierReportItemHeading("SUBTOTAL PERCENT DSCTS & MEAL PLANS");

                        for (Object subtotalPercentDsct : subtotalPercentDscts)
                        {
                            ArrayList item = (ArrayList) subtotalPercentDsct;
                            printer.printCashierReportItem(item.get(SHIFTDETAIL_4_ITEMNAME).toString(), "");
                            printer.printCashierReportSubItemIndented("QTY", item.get(SHIFTDETAIL_8_QUANTITY).toString());
                            printer.printCashierReportSubItemIndented("AMT", item.get(SHIFTDETAIL_0_AMOUNT).toString());
                            printer.printCashierReportSubItemIndented("ELIGIBLE AMT", item.get(SHIFTDETAIL_2_ELIGIBLEAMT).toString());
                        }
                    }
                    if (!subtotalCouponDscts.isEmpty() || !subtotalPercentDscts.isEmpty())
                    {
                        printer.printNewLine();
                        printer.printCashierReportItem("TOTAL SUBTOTAL DSCTS & MEAL PLANS", totalSubDscts);
                    }

                    printer.printNewLine();
                    printer.printCashierReportItem("RUNNING TOTAL 2", runningTotalTwo);

                    if (!transCreditRewards.isEmpty())
                    {
                        printer.printNewLine();
                        printer.printCashierReportItemHeading("TRANSACTION CREDIT REWARDS");
                        ArrayList creditItem;
                        for (int x = 0; x < transCreditRewards.size(); x++)
                        {
                            creditItem = (ArrayList)transCreditRewards.get(x);
                            printer.printCashierReportItem(creditItem.get(SHIFTDETAIL_4_ITEMNAME).toString(), "");
                            printer.printCashierReportSubItemIndented("QTY", creditItem.get(SHIFTDETAIL_8_QUANTITY).toString());
                            printer.printCashierReportSubItemIndented("AMT", creditItem.get(SHIFTDETAIL_0_AMOUNT).toString());
                            printer.printCashierReportSubItemIndented("REWARDABLE AMT", creditItem.get(SHIFTDETAIL_2_ELIGIBLEAMT).toString());
                        }
                        printer.printNewLine();
                        printer.printCashierReportItem("TOTAL TXN CREDIT REWARDS", totalTransCreds);
                    }

                    if (!freeProductRewards.isEmpty())
                    {
                        printer.printNewLine();
                        printer.printCashierReportItemHeading("FREE PRODUCT REWARDS");
                        ArrayList freeItem;
                        for (int x = 0; x < freeProductRewards.size(); x++)
                        {
                            freeItem = (ArrayList)freeProductRewards.get(x);
                            printer.printCashierReportItem(freeItem.get(SHIFTDETAIL_4_ITEMNAME).toString(), "");
                            printer.printCashierReportSubItemIndented("QTY", freeItem.get(SHIFTDETAIL_8_QUANTITY).toString());
                            printer.printCashierReportSubItemIndented("AMT", freeItem.get(SHIFTDETAIL_0_AMOUNT).toString());
                            printer.printCashierReportSubItemIndented("REWARDABLE AMT", freeItem.get(SHIFTDETAIL_2_ELIGIBLEAMT).toString());
                        }
                        printer.printNewLine();
                        printer.printCashierReportItem("TOTAL FREE PRODUCT REWARDS ", totalFreeProducts);
                    }

                    printer.printNewLine();
                    printer.printCashierReportItem("RUNNING TOTAL 2A", runningTotalTwoA);

                    if (!taxExempts.isEmpty())
                    {
                        printer.printNewLine();
                        printer.printCashierReportItemHeading("TAX EXEMPT");

                        for (Object taxExempt : taxExempts)
                        {
                            ArrayList item = (ArrayList) taxExempt;
                            printer.printCashierReportItem(item.get(SHIFTDETAIL_4_ITEMNAME).toString(), "");
                            printer.printCashierReportSubItemIndented("QTY", item.get(SHIFTDETAIL_8_QUANTITY).toString());
                            printer.printCashierReportSubItemIndented("AMT", item.get(SHIFTDETAIL_0_AMOUNT).toString());
                            printer.printCashierReportSubItemIndented("TAXABLE AMT", item.get(SHIFTDETAIL_2_ELIGIBLEAMT).toString());
                        }

                        printer.printCashierReportItem("TOTAL TAX EXEMPT", totalTaxExempt);
                    }
                    if (!taxes.isEmpty())
                    {
                        printer.printNewLine();
                        printer.printCashierReportItemHeading("TAXES");

                        for (Object taxe : taxes)
                        {
                            ArrayList item = (ArrayList) taxe;
                            printer.printCashierReportItem(item.get(SHIFTDETAIL_4_ITEMNAME).toString(), "");
                            printer.printCashierReportSubItemIndented("QTY", item.get(SHIFTDETAIL_8_QUANTITY).toString());
                            printer.printCashierReportSubItemIndented("AMT", item.get(SHIFTDETAIL_0_AMOUNT).toString());
                            printer.printCashierReportSubItemIndented("TAXABLE AMT", item.get(SHIFTDETAIL_2_ELIGIBLEAMT).toString());
                        }
                    }
                    printer.printCashierReportItem("TOTAL TAXES", totalTax);
                    printer.printNewLine();
                    printer.printCashierReportItem("RUNNING TOTAL 3 / NET SALES", runningTotalThree);
                    printer.printCashierReportItem("TRANSACTION COUNT", new Integer(customerCount).toString());

                    if (!receivedOnAcct.isEmpty())
                    {
                        printer.printNewLine();
                        printer.printCashierReportItemHeading("RA");

                        for (Object aReceivedOnAcct : receivedOnAcct)
                        {
                            ArrayList item = (ArrayList) aReceivedOnAcct;
                            printer.printCashierReportItem(item.get(SHIFTDETAIL_4_ITEMNAME).toString(), "");
                            printer.printCashierReportSubItemIndented("QTY", item.get(SHIFTDETAIL_8_QUANTITY).toString());
                            printer.printCashierReportSubItemIndented("AMT", item.get(SHIFTDETAIL_0_AMOUNT).toString());
                        }
                        printer.printCashierReportItem("TOTAL RA", totalRA);
                    }
                    if (!paidOut.isEmpty())
                    {
                        printer.printNewLine();
                        printer.printCashierReportItemHeading("PO");

                        for (Object aPaidOut : paidOut)
                        {
                            ArrayList item = (ArrayList) aPaidOut;
                            printer.printCashierReportItem(item.get(SHIFTDETAIL_4_ITEMNAME).toString(), "");
                            printer.printCashierReportSubItemIndented("QTY", item.get(SHIFTDETAIL_8_QUANTITY).toString());
                            printer.printCashierReportSubItemIndented("AMT", item.get(SHIFTDETAIL_0_AMOUNT).toString());
                        }
                        printer.printCashierReportItem("TOTAL PO", totalPO);
                    }

                    printer.printNewLine();
                    printer.printCashierReportItem("RUNNING TOTAL 4", runningTotalFour);

                    if (!tenders.isEmpty())
                    {
                        printer.printNewLine();
                        printer.printCashierReportItemHeading("TENDERS");

                        String tenderTypeName = "";
                        String tenderTypeTotals = "";
                        for (Object tender : tenders)
                        {
                            ArrayList item = (ArrayList) tender;
                            if (item.size() == 2)
                            {
                                if (tenderTypeName.equals("") == false)
                                {
                                    printer.printCashierReportItem(tenderTypeName + " TOTAL AMOUNT", tenderTypeTotals);
                                    printer.printNewLine();
                                }
                                tenderTypeName = item.get(0).toString();
                                tenderTypeTotals = item.get(1).toString();
                            }
                            else
                            {
                                printer.printCashierReportItem(item.get(SHIFTDETAIL_4_ITEMNAME).toString(), "");
                                printer.printCashierReportSubItemIndented("QTY", item.get(SHIFTDETAIL_8_QUANTITY).toString());
                                printer.printCashierReportSubItemIndented("AMT", item.get(SHIFTDETAIL_0_AMOUNT).toString());
                            }
                        }
                        printer.printCashierReportItem(tenderTypeName + " TOTAL AMOUNT", tenderTypeTotals);
                        printer.printNewLine();
                        printer.printCashierReportItem("TOTAL TENDER", totalTender);
                    }

                    printer.printNewLine();
                    printer.printCashierReportItemHeading("MISC");
                    printer.printCashierReportItem("NO SALES QTY", new Integer(noSalesQty).toString());
                    printer.printCashierReportItem("PRICE OVERRIDES QTY", new Integer(priceOverridesQty).toString());
                    printer.printNewLine();
                    printer.printCashierReportItem("VOID MODE QTY", new Integer(voidModeQty).toString());
                    printer.printCashierReportItem("VOID MODE AMT", voidModeAmt);
                    printer.printNewLine();
                    printer.printCashierReportItem("REFUND MODE QTY", new Integer(refundModeQty).toString());
                    printer.printCashierReportItem("REFUND MODE AMT", refundModeAmt);
                    printer.printNewLine();
                    printer.printCashierReportItem("INDIRECT VOIDS QTY", new Integer(indirectVoidsQty).toString());
                    printer.printCashierReportItem("INDIRECT VOIDS AMT", indirectVoidsAmt);
                    printer.printNewLine();
                    printer.printCashierReportItem("DIRECT VOIDS QTY", new Integer(directVoidsQty).toString());
                    printer.printCashierReportItem("DIRECT VOIDS AMT", directVoidsAmt);
                    printer.printNewLine();
                    printer.printCashierReportItem("CANCEL TRANS QTY", new Integer(cancelTransQty).toString());
                    printer.printCashierReportItem("CANCEL TRANS AMT", cancelTransAmt);
                    printer.printNewLine();
                    printer.printCashierReportItem("*OFFLINE CREDIT CARD CHARGES QTY", new Integer(numOfflineCreditCardCharges).toString());
                    printer.printLeftAlignText("*Represents current number of offline" + "\n" + "credit card charges and is not specific to" + "\n" + "a cashier or shift.");

                    printer.finishPrint();

                    printer.turnOffTransactionPrinting();

                }
            }
        }
        else
            Logger.logMessage("printCashierEndShiftReceipt - error: printer is null", logFileName, Logger.LEVEL.ERROR);

    }

    public void printSummaryEndShift(ArrayList summaryZDetails, Boolean isXClose) throws JposException
    {
        Logger.logMessage("PeripheralsDataManager.printSummaryEndShift", logFileName, Logger.LEVEL.TRACE);
        String cashierNames = summaryZDetails.get(0).toString();
        String transactionDate = summaryZDetails.get(1).toString();
        String terminalName = summaryZDetails.get(2).toString();
        String runningTotalOne = summaryZDetails.get(3).toString();
        String runningTotalTwo = summaryZDetails.get(4).toString();
        String runningTotalThree = summaryZDetails.get(5).toString();
        String runningTotalFour = summaryZDetails.get(6).toString();
        ArrayList itemCouponDscts = (ArrayList)summaryZDetails.get(7);
        ArrayList itemPercentDscts = (ArrayList)summaryZDetails.get(8);
        String totalItemDsctSum = summaryZDetails.get(9).toString();
        ArrayList subtotalCouponDscts = (ArrayList)summaryZDetails.get(10);
        ArrayList subtotalPercentDscts = (ArrayList)summaryZDetails.get(11);
        String totalSubDsctSum = summaryZDetails.get(12).toString();
        ArrayList taxes = (ArrayList)summaryZDetails.get(13);
        String totalTax = summaryZDetails.get(14).toString();
        int transCount = new Integer(summaryZDetails.get(15).toString()).intValue();
        ArrayList ra = (ArrayList)summaryZDetails.get(16);
        String totalRA = summaryZDetails.get(17).toString();
        ArrayList po = (ArrayList)summaryZDetails.get(18);
        String totalPO = summaryZDetails.get(19).toString();
        ArrayList tenders = (ArrayList)summaryZDetails.get(20);
        String totalTender = summaryZDetails.get(21).toString();
        int noSalesQty = new Integer(summaryZDetails.get(22).toString()).intValue();
        int priceOverridesQty = new Integer(summaryZDetails.get(23).toString()).intValue();
        int voidModeQty = new Integer(summaryZDetails.get(24).toString()).intValue();
        String voidModeAmt = summaryZDetails.get(25).toString();
        int refundModeQty = new Integer(summaryZDetails.get(26).toString()).intValue();
        String refundModeAmt = summaryZDetails.get(27).toString();
        int indirectVoidsQty = new Integer(summaryZDetails.get(28).toString()).intValue();
        String indirectVoidsAmt = summaryZDetails.get(29).toString();
        int directVoidsQty = new Integer(summaryZDetails.get(30).toString()).intValue();
        String directVoidsAmt = summaryZDetails.get(31).toString();
        int cancelTransQty = new Integer(summaryZDetails.get(32).toString());
        String cancelTransAmt = summaryZDetails.get(33).toString();
        String grossTotal = summaryZDetails.get(34).toString();
        ArrayList taxDeletes = (ArrayList) summaryZDetails.get(35);
        String totalTaxDelete = summaryZDetails.get(36).toString();

        ArrayList transCreditRewards = (ArrayList)summaryZDetails.get(37);
        String totalTransCreds = summaryZDetails.get(38).toString();
        ArrayList freeProductRewards = (ArrayList)summaryZDetails.get(39);
        String totalFreeProducts = summaryZDetails.get(40).toString();
        String runningTotalTwoA = summaryZDetails.get(41).toString();
        String numOfflineCreditCardCharges = summaryZDetails.get(42).toString();

        createPrinterIfNeeded();

        if (printer != null)
        {
            synchronized (printer)
            {
                if(!printer.isInErrorState())
                {
                    printer.turnOnTransactionPrinting();
                    printer.printSummaryZHeader(cashierNames, transactionDate, terminalName, isXClose);
                    printer.printCashierReportItem("GROSS TOTAL", grossTotal);

                    if (!itemCouponDscts.isEmpty())
                    {
                        printer.printNewLine();
                        printer.printCashierReportItemHeading("ITEM COUPON DISCOUNTS");

                        for (Object itemCouponDsct : itemCouponDscts)
                        {
                            ArrayList item = (ArrayList) itemCouponDsct;
                            printer.printCashierReportItem(item.get(SHIFTDETAIL_4_ITEMNAME).toString(), "");
                            printer.printCashierReportSubItemIndented("QTY", item.get(SHIFTDETAIL_8_QUANTITY).toString());
                            printer.printCashierReportSubItemIndented("AMT", item.get(SHIFTDETAIL_0_AMOUNT).toString());
                        }
                    }
                    if (!itemPercentDscts.isEmpty())
                    {
                        printer.printNewLine();
                        printer.printCashierReportItemHeading("ITEM PERCENT DISCOUNTS");

                        for (Object itemPercentDsct : itemPercentDscts)
                        {
                            ArrayList item = (ArrayList) itemPercentDsct;
                            printer.printCashierReportItem(item.get(SHIFTDETAIL_4_ITEMNAME).toString(), "");
                            printer.printCashierReportSubItemIndented("QTY", item.get(SHIFTDETAIL_8_QUANTITY).toString());
                            printer.printCashierReportSubItemIndented("AMT", item.get(SHIFTDETAIL_0_AMOUNT).toString());
                        }
                    }
                    if (!itemCouponDscts.isEmpty() || !itemPercentDscts.isEmpty())
                    {
                        printer.printNewLine();
                        printer.printCashierReportItem("TOTAL ITEM DSCTS", totalItemDsctSum);
                    }

                    printer.printNewLine();
                    printer.printCashierReportItem("RUNNING TOTAL 1", runningTotalOne);

                    if (!subtotalCouponDscts.isEmpty())
                    {
                        printer.printNewLine();
                        printer.printCashierReportItemHeading("SUBTOTAL COUPON DSCTS & MEAL PLANS");

                        for (Object subtotalCouponDsct : subtotalCouponDscts)
                        {
                            ArrayList item = (ArrayList) subtotalCouponDsct;
                            printer.printCashierReportItem(item.get(SHIFTDETAIL_4_ITEMNAME).toString(), "");
                            printer.printCashierReportSubItemIndented("QTY", item.get(SHIFTDETAIL_8_QUANTITY).toString());
                            printer.printCashierReportSubItemIndented("AMT", item.get(SHIFTDETAIL_0_AMOUNT).toString());
                            printer.printCashierReportSubItemIndented("ELIGIBLE AMT", item.get(SHIFTDETAIL_2_ELIGIBLEAMT).toString());
                        }
                    }
                    if (!subtotalPercentDscts.isEmpty())
                    {
                        printer.printNewLine();
                        printer.printCashierReportItemHeading("SUBTOTAL PERCENT DSCTS & MEAL PLANS");

                        for (Object subtotalPercentDsct : subtotalPercentDscts)
                        {
                            ArrayList item = (ArrayList) subtotalPercentDsct;
                            printer.printCashierReportItem(item.get(SHIFTDETAIL_4_ITEMNAME).toString(), "");
                            printer.printCashierReportSubItemIndented("QTY", item.get(SHIFTDETAIL_8_QUANTITY).toString());
                            printer.printCashierReportSubItemIndented("AMT", item.get(SHIFTDETAIL_0_AMOUNT).toString());
                            printer.printCashierReportSubItemIndented("ELIGIBLE AMT", item.get(SHIFTDETAIL_2_ELIGIBLEAMT).toString());
                        }
                    }
                    if (!subtotalCouponDscts.isEmpty() || !subtotalPercentDscts.isEmpty())
                    {
                        printer.printNewLine();
                        printer.printCashierReportItem("TOTAL SUBTOTAL DSCTS & MEAL PLANS", totalSubDsctSum);
                    }

                    printer.printNewLine();
                    printer.printCashierReportItem("RUNNING TOTAL 2", runningTotalTwo);

                    if (!transCreditRewards.isEmpty())
                    {
                        printer.printNewLine();
                        printer.printCashierReportItemHeading("TRANSACTION CREDIT REWARDS");
                        ArrayList creditItem;
                        for (int x = 0; x < transCreditRewards.size(); x++)
                        {
                            creditItem = (ArrayList)transCreditRewards.get(x);
                            printer.printCashierReportItem(creditItem.get(SHIFTDETAIL_4_ITEMNAME).toString(), "");
                            printer.printCashierReportSubItemIndented("QTY", creditItem.get(SHIFTDETAIL_8_QUANTITY).toString());
                            printer.printCashierReportSubItemIndented("AMT", creditItem.get(SHIFTDETAIL_0_AMOUNT).toString());
                            printer.printCashierReportSubItemIndented("REWARDABLE AMT", creditItem.get(SHIFTDETAIL_2_ELIGIBLEAMT).toString());
                        }
                        printer.printNewLine();
                        printer.printCashierReportItem("TOTAL TXN CREDIT REWARDS", totalTransCreds);
                    }

                    if (!freeProductRewards.isEmpty())
                    {
                        printer.printNewLine();
                        printer.printCashierReportItemHeading("FREE PRODUCT REWARDS");
                        ArrayList freeItem;
                        for (int x = 0; x < freeProductRewards.size(); x++)
                        {
                            freeItem = (ArrayList)freeProductRewards.get(x);
                            printer.printCashierReportItem(freeItem.get(SHIFTDETAIL_4_ITEMNAME).toString(), "");
                            printer.printCashierReportSubItemIndented("QTY", freeItem.get(SHIFTDETAIL_8_QUANTITY).toString());
                            printer.printCashierReportSubItemIndented("AMT", freeItem.get(SHIFTDETAIL_0_AMOUNT).toString());
                            printer.printCashierReportSubItemIndented("REWARDABLE AMT", freeItem.get(SHIFTDETAIL_2_ELIGIBLEAMT).toString());
                        }
                        printer.printNewLine();
                        printer.printCashierReportItem("TOTAL FREE PRODUCT REWARDS ", totalFreeProducts);
                    }

                    printer.printNewLine();
                    printer.printCashierReportItem("RUNNING TOTAL 2A", runningTotalTwoA);

                    //------------------------------------------

                    if (!taxDeletes.isEmpty())
                    {
                        printer.printNewLine();
                        printer.printCashierReportItemHeading("TAX EXEMPT");

                        for (Object taxDel : taxDeletes)
                        {
                            ArrayList item = (ArrayList) taxDel;
                            printer.printCashierReportItem(item.get(SHIFTDETAIL_4_ITEMNAME).toString(), "");
                            printer.printCashierReportSubItemIndented("QTY", item.get(SHIFTDETAIL_8_QUANTITY).toString());
                            printer.printCashierReportSubItemIndented("AMT", item.get(SHIFTDETAIL_0_AMOUNT).toString());
                            printer.printCashierReportSubItemIndented("TAXABLE AMT", item.get(SHIFTDETAIL_2_ELIGIBLEAMT).toString());
                        }
                    }

                    if (!taxes.isEmpty()) {
                        printer.printNewLine();
                        printer.printCashierReportItemHeading("TAXES");

                        for (Object taxe : taxes) {
                            ArrayList item = (ArrayList) taxe;
                            printer.printCashierReportItem(item.get(SHIFTDETAIL_4_ITEMNAME).toString(), "");
                            printer.printCashierReportSubItemIndented("QTY", item.get(SHIFTDETAIL_8_QUANTITY).toString());
                            printer.printCashierReportSubItemIndented("AMT", item.get(SHIFTDETAIL_0_AMOUNT).toString());
                            printer.printCashierReportSubItemIndented("TAXABLE AMT", item.get(SHIFTDETAIL_2_ELIGIBLEAMT).toString());
                        }
                    }

                    printer.printCashierReportItem("TOTAL TAXES", totalTax);
                    printer.printNewLine();
                    printer.printCashierReportItem("RUNNING TOTAL 3 / NET SALES", runningTotalThree);
                    printer.printCashierReportItem("TRANSACTION COUNT", new Integer(transCount).toString());

                    if (!ra.isEmpty())
                    {
                        printer.printNewLine();
                        printer.printCashierReportItemHeading("RA");

                        for (Object aRa : ra)
                        {
                            ArrayList item = (ArrayList) aRa;
                            printer.printCashierReportItem(item.get(SHIFTDETAIL_4_ITEMNAME).toString(), "");
                            printer.printCashierReportSubItemIndented("QTY", item.get(SHIFTDETAIL_8_QUANTITY).toString());
                            printer.printCashierReportSubItemIndented("AMT", item.get(SHIFTDETAIL_0_AMOUNT).toString());
                        }

                        printer.printCashierReportItem("TOTAL RA", totalRA);
                    }
                    if (!po.isEmpty())
                    {
                        printer.printNewLine();
                        printer.printCashierReportItemHeading("PO");

                        for (Object aPo : po)
                        {
                            ArrayList item = (ArrayList) aPo;
                            printer.printCashierReportItem(item.get(SHIFTDETAIL_4_ITEMNAME).toString(), "");
                            printer.printCashierReportSubItemIndented("QTY", item.get(SHIFTDETAIL_8_QUANTITY).toString());
                            printer.printCashierReportSubItemIndented("AMT", item.get(SHIFTDETAIL_0_AMOUNT).toString());
                        }

                        printer.printCashierReportItem("TOTAL PO", totalPO);
                    }

                    printer.printNewLine();
                    printer.printCashierReportItem("RUNNING TOTAL 4", runningTotalFour);

                    if (!tenders.isEmpty())
                    {
                        printer.printNewLine();
                        printer.printCashierReportItemHeading("TENDERS");

                        String tenderTypeName = "";
                        String tenderTypeTotals = "";

                        for (Object tender : tenders)
                        {
                            ArrayList item = (ArrayList) tender;
                            if (item.size() == 2)
                            {
                                if (tenderTypeName.equals("") == false)
                                {
                                    printer.printCashierReportItem(tenderTypeName + " TOTAL AMOUNT", tenderTypeTotals);
                                    printer.printNewLine();
                                }
                                tenderTypeName = item.get(0).toString();
                                tenderTypeTotals = item.get(1).toString();
                            }
                            else
                            {
                                printer.printCashierReportItem(item.get(SHIFTDETAIL_4_ITEMNAME).toString(), "");
                                printer.printCashierReportSubItemIndented("QTY", item.get(SHIFTDETAIL_8_QUANTITY).toString());
                                printer.printCashierReportSubItemIndented("AMT", item.get(SHIFTDETAIL_0_AMOUNT).toString());
                            }
                        }
                        printer.printCashierReportItem(tenderTypeName + " TOTAL AMOUNT", tenderTypeTotals);
                        printer.printNewLine();
                        printer.printCashierReportItem("TOTAL TENDER", totalTender);
                    }

                    printer.printNewLine();
                    printer.printCashierReportItemHeading("MISC");
                    printer.printCashierReportItem("NO SALES QTY", new Integer(noSalesQty).toString());
                    printer.printCashierReportItem("PRICE OVERRIDES QTY", new Integer(priceOverridesQty).toString());
                    printer.printNewLine();
                    printer.printCashierReportItem("VOID MODE QTY", new Integer(voidModeQty).toString());
                    printer.printCashierReportItem("VOID MODE AMT", voidModeAmt);
                    printer.printNewLine();
                    printer.printCashierReportItem("REFUND MODE QTY", new Integer(refundModeQty).toString());
                    printer.printCashierReportItem("REFUND MODE AMT", refundModeAmt);
                    printer.printNewLine();
                    printer.printCashierReportItem("INDIRECT VOIDS QTY", new Integer(indirectVoidsQty).toString());
                    printer.printCashierReportItem("INDIRECT VOIDS AMT", indirectVoidsAmt);
                    printer.printNewLine();
                    printer.printCashierReportItem("DIRECT VOIDS QTY", new Integer(directVoidsQty).toString());
                    printer.printCashierReportItem("DIRECT VOIDS AMT", directVoidsAmt);
                    printer.printNewLine();
                    printer.printCashierReportItem("CANCEL TRANS QTY", new Integer(cancelTransQty).toString());
                    printer.printCashierReportItem("CANCEL TRANS AMT", cancelTransAmt);
                    printer.printNewLine();
                    printer.printCashierReportItem("*OFFLINE CREDIT CARD CHARGES QTY", numOfflineCreditCardCharges);
                    printer.printLeftAlignText("*Represents current number of offline" + "\n" + "credit card charges and is not specific to" + "\n" + "a cashier or shift.");

                    printer.finishPrint();

                    printer.turnOffTransactionPrinting();
                }
            }
        }
        else
        {
            Logger.logMessage("printSummaryEndShift - error: printer is null", logFileName, Logger.LEVEL.ERROR);
        }
    }

    public void printAccountBalances(String availBal, String name, String date, ArrayList loyaltyProgramPtBalances) throws JposException
    {
        if (printer != null)
        {
            synchronized (printer)
            {
                if (!printer.isInErrorState())
                {
                    printer.printAcctBalances(availBal, name, date, loyaltyProgramPtBalances);
                }
            }
        }
    }

    public void cutReceiptPaper() throws JposException {
        Logger.logMessage("PeripheralsDataManager.cutReceiptPaper", Logger.LEVEL.DEBUG);
        if (printer != null) {
            if (!printer.isInErrorState())
                printer.finishPrint();
        }
    }

    public void printUpdateInstructions() throws JposException {
        Logger.logMessage("PeripheralsDataManager.printUpdateInstructions", Logger.LEVEL.DEBUG);

        createPrinterIfNeeded();

        if (printer != null ) {
            if (!printer.isInErrorState())
                printer.printUpdateInstructions();
        } else
            Logger.logMessage("printUpdateInstructions - error: printer is null", logFileName, Logger.LEVEL.ERROR);

    }

    public void printLoyaltySummary(ArrayList programs) throws JposException {
        Logger.logMessage("PeripheralsDataManager.printLoyaltySummary", Logger.LEVEL.DEBUG);
        if (printer != null) {
            synchronized (printer) {
                if (!printer.isInErrorState())
                    printer.printLoyaltySummary(programs);
            }
        } else
            Logger.logMessage("printLoyaltySummary - error: printer is null", logFileName, Logger.LEVEL.ERROR);

    }

    public void emailMerchantLinkReport(ArrayList reportDetails, String emailAddress, int reportType)
    {
        try
        {
            ReceiptGen rcptGen = new ReceiptGen();

            // Create report data
            MerchantLinkReportData reportData = new MerchantLinkReportData();
            reportData.setReportDetails(reportDetails);
            boolean setReportTypeResult = reportData.setReportType(reportType);

            if (!setReportTypeResult)
            {
                Logger.logMessage("EmailMerchantLinkReport - UNKNOWN REPORT TYPE: " + reportType, logFileName, Logger.LEVEL.TRACE);
            }

            // Set report data
            rcptGen.setReportData(reportData);

            // Create formatter
            MerchantLinkReportFormatter reportFormatter = new MerchantLinkReportFormatter();

            // Set formatter
            rcptGen.setReportFormatter(reportFormatter);

            // Create renderer
            IRenderer htmlRenderer = rcptGen.createBasicHTMLRenderer();
            boolean result = rcptGen.buildReport(htmlRenderer);

            if (result == true)
            {
                // If we need to pull email settings, do that
                if (emailServer == null)
                {
                    setEmailSettings();
                }

                // Set email header
                String emailSubjectLine = reportData.getEmailSubjectLine();

                // Email the report
                if (emailUser.isEmpty())
                {
                    Logger.logMessage("In EmailMerchantLinkReport, about to send unauthenticated email report to " + emailAddress + " using mail server " + emailServer + " and port " + emailPort + ". ", logFileName, Logger.LEVEL.TRACE);
                    MMHEmail.sendEmail(Integer.parseInt(emailPort), emailServer, emailAddress, emailFromAddress, emailSubjectLine, htmlRenderer.toString(), "text/html");
                }
                else
                {
                    Logger.logMessage("In EmailMerchantLinkReport, about to send authenticated email report to " + emailAddress + " using mail server " + emailServer + " and port " + emailPort + ". ", logFileName, Logger.LEVEL.TRACE);
                    MMHEmail.sendAuthEmail(Integer.parseInt(emailPort), emailServer, emailUser, emailPassword, emailAddress, emailFromAddress, emailSubjectLine, htmlRenderer.toString(), "text/html");
                }
            }
            else
            {
                Logger.logMessage("Cannot email report, report generation failed!", logFileName, Logger.LEVEL.TRACE);
            }
        }
        catch (Exception ex)
        {
            Logger.logException("EmailMerchantLinkReport error", logFileName, ex);
        }

    }

    public void printMerchantLinkReceipt(ArrayList receiptDetails)
    {
        try
        {
            if (printer == null)
            {
                createPrinterIfNeeded();
            }

            if (printer != null)
            {
                synchronized (printer)
                {
                    Logger.logMessage("PeripheralsDataManager.printMerchantLinkReceipt", logFileName, Logger.LEVEL.TRACE);

                    createPrinterIfNeeded();

                    ReceiptGen rcptGen = new ReceiptGen();

                    // Create report data
                    MerchantLinkReportData reportData = new MerchantLinkReportData();
                    reportData.setReportDetails(receiptDetails);
                    boolean setReportTypeResult = reportData.setReportType(TypeData.MerchantLinkReport.GENERIC_RPT);

                    // Set report data
                    rcptGen.setReportData(reportData);

                    // Create formatter
                    MerchantLinkReportFormatter reportFormatter = new MerchantLinkReportFormatter();

                    // Set formatter
                    rcptGen.setReportFormatter(reportFormatter);

                    String retVal = "";
                    POSPrinter ptr = null;
                    if (printer != null)
                    {
                        ptr = printer.getPOSPrinter();
                        IRenderer printerRenderer = null;
                        if (ptr != null)
                        {
                            // Create renderer
                            printerRenderer = rcptGen.createPrinterRenderer(ptr, printer.getLogicalPrinterName());
                        }

                        // print the receipt
                        if (printerRenderer != null)
                        {
                            rcptGen.buildReport(printerRenderer);
                            if(printerRenderer.getLastException() != null)
                            {
                                throw printerRenderer.getLastException();
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Logger.logException("printMerchantLinkReceipt error", logFileName, ex);
        }
    }

    public void printFreedomPayReceipt(ArrayList receiptContentAL) throws Exception
    {
        try
        {
            ReceiptGen rcptGen = new ReceiptGen();

            // Create and set receipt content
            FreedomPayReceiptData fpRcptData = new FreedomPayReceiptData();
            String receiptContent = receiptContentAL.get(0).toString();
            fpRcptData.setReceiptContent(receiptContent);

            // Assign to ReceiptGen
            if (rcptGen != null)
            {
                rcptGen.setPaymentProcessorRcptData(fpRcptData);
            }

            // Create formatter
            FreedomPayRcptFormatter fpRcptFormatter = new FreedomPayRcptFormatter();

            // Set formatter
            rcptGen.setPaymentProcessorRcptFormatter(fpRcptFormatter);

            // Print the receipt
            String retVal = "";
            POSPrinter ptr = null;
            if (printer != null)
            {
                ptr = printer.getPOSPrinter();
                IRenderer printerRenderer = null;
                if (ptr != null)
                {
                    printerRenderer = rcptGen.createPrinterRenderer(ptr, printer.getLogicalPrinterName());
                }

                // print the receipt
                if (printerRenderer != null)
                {
                    rcptGen.buildPaymentProcessorRcpt(printerRenderer);
                    if(printerRenderer.getLastException() != null)
                    {
                        throw printerRenderer.getLastException();
                    }
                }
            }
        }
        catch (Exception e)
        {
            Logger.logException("PeripheralsDataManager.printFreedomPayReceipt error", logFileName, e);
        }
    }

    public void printSimplifyReceipt(ArrayList receiptContentAL) throws Exception
    {
        try
        {
            ReceiptGen rcptGen = new ReceiptGen();

            // Create and set receipt content
            SimplifyReceiptData smpRcptData = new SimplifyReceiptData();
            smpRcptData.setReceiptInfo(receiptContentAL);

            // Assign to ReceiptGen
            if (rcptGen != null)
            {
                rcptGen.setPaymentProcessorRcptData(smpRcptData);
            }

            // Create formatter
            SimplifyRcptFormatter smpRcptFormatter = new SimplifyRcptFormatter();

            // Set formatter
            rcptGen.setPaymentProcessorRcptFormatter(smpRcptFormatter);

            // Print the receipt
            String retVal = "";
            POSPrinter ptr = null;
            if (printer != null)
            {
                ptr = printer.getPOSPrinter();
                IRenderer printerRenderer = null;
                if (ptr != null)
                {
                    printerRenderer = rcptGen.createPrinterRenderer(ptr, printer.getLogicalPrinterName());
                }

                // print the receipt
                if (printerRenderer != null)
                {
                    rcptGen.buildPaymentProcessorRcpt(printerRenderer);
                    if(printerRenderer.getLastException() != null)
                    {
                        throw printerRenderer.getLastException();
                    }
                }
            }
        }
        catch (Exception e)
        {
            Logger.logException("PeripheralsDataManager.printSimplifyReceipt error", logFileName, e);
        }
    }

    /**
     * <p>Queries the local database to get the printer host record as a {@link HashMap}. </p>
     *
     * @param macAddress The MAC address {@link String} of the printer host record to get from the local database.
     * @return The printer host record from the local database as a {@link HashMap}.
     */
    @SuppressWarnings("unchecked")
    public static HashMap getPHRecFromLocalDB (String macAddress) {
        HashMap phRec = new HashMap();

        try {
            ArrayList<HashMap> queryRes = DataFunctions.purgeAlOfHm(new DataManager().parameterizedExecuteQuery("data.newKitchenPrinter.GetPrinterHostByMacAddress", new Object[]{macAddress}, true));
            if (!DataFunctions.isEmptyCollection(queryRes)) {
                phRec = queryRes.get(0);
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage(String.format("There was a problem trying to get the printer host record with a MAC " +
                    "address of %s in PeripheralsDataManager.getPHRecFromLocalDB",
                    Objects.toString(macAddress, "NULL")), Logger.LEVEL.ERROR);
        }

        return phRec;
    }

    /**
     * <p>Queries the local database to get the printer host record as a {@link HashMap}.</p>
     *
     * @param printerHostID ID of the printer host to get the record for from the local database.
     * @return The printer host record from the local database as a {@link HashMap}.
     */
    @SuppressWarnings("unchecked")
    public static HashMap getPHRecFromLocalDB (int printerHostID) {
        HashMap phRec = new HashMap();

        try {
            ArrayList<HashMap> queryRes = DataFunctions.purgeAlOfHm(new DataManager().parameterizedExecuteQuery("data.newKitchenPrinter.GetPrinterHostByID", new Object[]{printerHostID}, true));
            if (!DataFunctions.isEmptyCollection(queryRes)) {
                phRec = queryRes.get(0);
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage(String.format("There was a problem trying to get the printer host record with an ID " +
                    "of %s in PeripheralsDataManager.getPHRecFromLocalDB",
                    Objects.toString(printerHostID, "NULL")), Logger.LEVEL.ERROR);
        }

        return phRec;
    }

    /**
     * <p>Queries the local database to get the printer record as a {@link HashMap}.</p>
     *
     * @param printerID ID of the printer to get the record for from the local database.
     * @return The printer record from the local database as a {@link HashMap}.
     */
    @SuppressWarnings("unchecked")
    public static HashMap getPtrRecFromLocalDB (int printerID) {
        HashMap phRec = new HashMap();

        try {
            ArrayList<HashMap> queryRes = DataFunctions.purgeAlOfHm(new DataManager().parameterizedExecuteQuery("data.newKitchenPrinter.GetPrinterByID", new Object[]{printerID}, true));
            if (!DataFunctions.isEmptyCollection(queryRes)) {
                phRec = queryRes.get(0);
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage(String.format("There was a problem trying to get the printer record with an ID " +
                            "of %s in PeripheralsDataManager.getPtrRecFromLocalDB",
                    Objects.toString(printerID, "NULL")), Logger.LEVEL.ERROR);
        }

        return phRec;
    }

    /**
     * Determine the MAC address of the printer host that was configured as the primary printer host in QC WEB. If this
     * printer host was configured as a backup then the MAC address of the primary printer host is returned otherwise
     * the MAC address of this printer host is returned.
     *
     * @param macAddress {@link String} MAC address of the printer we want to check for it's primary printer host MAC
     *         address.
     * @return {@link String} The MAC address of the printer host configured as the primary printer host in QC WEB.
     */
    public static String getPrimaryPHMacAddress (String macAddress) {
        String primaryPHMacAddress = "";

        try {
            Object primaryPHMacAddressObj = new DataManager().parameterizedExecuteScalar(
                    "data.kitchenPrinter.getPrimaryPHMACFromMAC", new Object[]{macAddress}, logFileName);

            if ((primaryPHMacAddressObj != null) && (primaryPHMacAddressObj instanceof String)
                    && (!primaryPHMacAddressObj.toString().isEmpty())) {
                primaryPHMacAddress = primaryPHMacAddressObj.toString();
            }
        }
        catch (Exception e) {
            Logger.logException(e, logFileName);
            Logger.logMessage("The printer host running on the machine with a hostname of " +
                    Objects.toString(getPHHostname(), "NULL")+" was unable to determine the MAC address of the " +
                    "printer host that was configured as the primary printer host in QC WEB in " +
                    "PeripheralsDataManger.getPrimaryPHMacAddress", logFileName, Logger.LEVEL.ERROR);
        }

        return primaryPHMacAddress;
    }

    /**
     * Get the hostname of the printer host configured to run on this WAR.
     *
     * @return {@link String} The hostname of the printer host configured to run on this WAR.
     */
    public static String getPHHostname () {
        String hostname = "";

        try {
            if ((InetAddress.getLocalHost().getHostName() != null)
                    && (!InetAddress.getLocalHost().getHostName().isEmpty())) {
                hostname = InetAddress.getLocalHost().getHostName();
            }
        }
        catch (Exception e) {
            Logger.logException(e, logFileName);
            Logger.logMessage("There was a problem trying to hostname for the printer host configured to run on " +
                    "this WAR in PeripheralsDataManager.getPHMacAddress", logFileName, Logger.LEVEL.ERROR);
        }

        return hostname;
    }

    /**
     * Get the hostname of the printer host with the given MAC address.
     *
     * @param macAddress {@link String} MAC address of the printer host we wish to get the hostname of.
     * @return {@link String} Hostname of the printer host with the given MAC address.
     */
    public static String getTerminalHostname(String macAddress) {
        String hostname = "";

        try {
            Object hostnameObj = new DataManager().parameterizedExecuteScalar(
                    "data.kitchenPrinter.getPHHostnameFromMAC", new Object[]{macAddress}, logFileName);

            if ((hostnameObj != null) && (hostnameObj instanceof String) && (!hostnameObj.toString().isEmpty())) {
                hostname = hostnameObj.toString();
            }
        }
        catch (Exception e) {
            Logger.logException(e, logFileName);
            Logger.logMessage("There was a problem trying to hostname for the printer host configured to run on " +
                    "this WAR with the MAC address "+Objects.toString(macAddress, "NULL")+" in " +
                    "PeripheralsDataManager.getPHMacAddress", logFileName, Logger.LEVEL.ERROR);
        }

        return hostname;
    }

    /**
     * Get the MAC address of the terminal this printer host / WAR is running on.
     *
     * @return {@link String} The MAC address of this printer host / WAR.
     */
    public static String getTerminalMacAddress() {
        String macAddress = "";

        try {
            // try to get the MAC address from the app.properties file
            String macAddressFromAppProps = MMHProperties.getCommonAppSetting("site.networkConnCode");
            if ((macAddressFromAppProps != null) && (!macAddressFromAppProps.isEmpty())) {
                macAddress = StringFunctions.decodePassword(macAddressFromAppProps);
            }
            else {
                // if we couldn't get the MAC address from the app.properties file try to get it from all the network
                // interfaces on this machine
                Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
                while (networkInterfaces.hasMoreElements()) {
                    NetworkInterface networkInterface = networkInterfaces.nextElement();

                    // return the hardware address (usually a MAC address) if this machine has one and if it can be
                    // accessed with the current privileges
                    byte[] hardwareAddress = networkInterface.getHardwareAddress();
                    String networkDisplayName = networkInterface.getDisplayName();

                    // try to extract the MAC address from the hardwareAddress
                    if ((hardwareAddress != null) && (hardwareAddress.length > 0)) {
                        StringBuilder stringBuilder = new StringBuilder();
                        for (int i = 0; i < hardwareAddress.length; i++) {
                            stringBuilder.append(String.format("02X%s", hardwareAddress[i],
                                    ((i < hardwareAddress.length) ? "-" : "")));
                        }
                        macAddress = stringBuilder.toString().trim().replaceAll("-", "");
                        // use the first MAC address found
                        if (macAddress.length() == MAC_ADDRESS_LENGTH) {
                            Logger.logMessage("Using the MAC address "+Objects.toString(macAddress, "NULL")+" for the " +
                                    "network interface with a network display name of " +
                                    Objects.toString(networkDisplayName, "NULL")+" in " +
                                    "PeripheralsDataManager.getPHMacAddress", logFileName, Logger.LEVEL.DEBUG);
                            break; // MAC address found no need to keep processing
                        }
                    }
                    else {
                        Logger.logMessage("The hardware address of the network interface with a network " +
                                "display name of "+Objects.toString(networkDisplayName, "NULL")+" in " +
                                "PeripheralsDataManager.getPHMacAddress", logFileName, Logger.LEVEL.ERROR);
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, logFileName);
            Logger.logMessage("There was a problem trying to MAC address for the printer host configured to run on " +
                    "this WAR in PeripheralsDataManager.getPHMacAddress", logFileName, Logger.LEVEL.ERROR);
        }

        return macAddress;
    }

    /**
     * Called through XML RPC to start the printer host on this WAR.
     *
     * @return Whether or not the printer host on this WAR was started successfully.
     */
    public boolean startPrinterHost () {
        boolean printerHostStarted = false;

        try {
            Logger.logMessage("Attempting to start the printer host with hostname "+Objects.toString(getPHHostname(),
                    "NULL")+" in PeripheralsDataManager.startPrinterHost", logFileName, Logger.LEVEL.DEBUG);
            PrinterHostThread.getInstance().startPrinterHost();
            printerHostStarted = true;
        }
        catch (Exception e) {
            Logger.logException(e, logFileName);
            Logger.logMessage("There was a problem trying to start the printer host running on the machine with " +
                            "hostname "+Objects.toString(getPHHostname(), "NULL")+" in PeripheralsDataManager.startPrinterHost",
                    logFileName, Logger.LEVEL.ERROR);
        }

        return printerHostStarted;
    }

    /**
     * Called through XML RPC to stop the printer host on this WAR.
     *
     * @return Whether or not the printer host on this WAR was stopped successfully.
     */
    public boolean stopPrinterHost () {
        boolean printerHostStopped = false;

        try {
            Logger.logMessage("Attempting to stop the printer host running on the machine with with hostname "
                            +Objects.toString(getPHHostname(), "NULL")+" in PeripheralsDataManager.stopPrinterHost",
                    logFileName, Logger.LEVEL.DEBUG);
            PrinterHostThread.getInstance().stopPrinterHost();
            printerHostStopped = true;
        }
        catch (Exception e) {
            Logger.logException(e, logFileName);
            Logger.logMessage("There was a problem trying to stop the printer host running on the machine with " +
                            "hostname "+Objects.toString(getPHHostname(), "NULL")+" in PeripheralsDataManager.stopPrinterHost",
                    logFileName, Logger.LEVEL.ERROR);
        }

        return printerHostStopped;
    }

    /**
     * Called through XML RPC to check whether or not the printer host running on this machine is active.
     *
     * @return Whether or not the printer host running on this machine is active.
     */
    public boolean isPrinterHostActive () {
        boolean isPrinterHostActive = false;

        try {
            Logger.logMessage("Attempting to check if the printer host running on the machine with hostname " +
                    Objects.toString(getPHHostname(), "NULL")+" is active", logFileName, Logger.LEVEL.DEBUG);
            isPrinterHostActive = PrinterHost.getInstance().isPrinterHostActive();
        }
        catch (Exception e) {
            Logger.logException(e, logFileName);
            Logger.logMessage("There was a problem trying to check if the printer host running on the machine " +
                    Objects.toString(getPHHostname(), "NULL")+" was active in " +
                    "PeripheralsDataManager.isPrinterHostActive", logFileName, Logger.LEVEL.ERROR);
        }

        return isPrinterHostActive;
    }

    /**
     * Called through XML RPC to inform this terminal of the MAC address for the machine running the active
     * printer host.
     *
     * @param macAddress {@link String} The MAC address of the machine running the active printer host.
     * @return Whether or not the MAC address for the machine running the active printer host was set on this terminal.
     */
    public boolean setActivePHMacAddress (String macAddress) {
        boolean macAddressSet = false;

        try {
            kitchenPrinterTerminal.setActivePHMacAddress(macAddress);
            macAddressSet = true;
        }
        catch (Exception e) {
            Logger.logException(e, logFileName);
            Logger.logMessage("There was a problem trying to set the MAC address of the machine the active printer " +
                    "host is running on for the terminal with a hostname of "+Objects.toString(getPHHostname(), "NULL") +
                    " in PeripheralsDataManager.setActivePHMacAddress", logFileName, Logger.LEVEL.ERROR);
        }

        return macAddressSet;
    }

    /**
     * Called through XML RPC to obtain the MAC address of the machine running the active printer host as
     * viewed by this terminal.
     *
     * @return {@link String} MAC address of the machine running the active printer host as viewed by this terminal.
     */
    public String getActivePHMacAddress () {
        String activePHMacAddress = "";

        try {
            activePHMacAddress = kitchenPrinterTerminal.getActivePHMacAddress();
        }
        catch (Exception e) {
            Logger.logException(e, logFileName);
            Logger.logMessage("There was a problem trying to get the MAC address of the machine running the active " +
                            "printer host as viewed by the terminal with a hostname of " +
                            Objects.toString(getPHHostname(), "NULL")+" in PeripheralsDataManager.getActivePHMacAddress",
                    logFileName, Logger.LEVEL.ERROR);
        }

        return activePHMacAddress;
    }

//    /**
//     * Called through XML RPC to set the time the active printer host became active as viewed by this terminal.
//     *
//     * @param date {@link Date} The time the active printer host became active as viewed by this terminal.
//     * @return Whether or not the time the active printer host became active as viewed by this terminal was set for this
//     *         terminal.
//     */
//    @SuppressWarnings("UseOfObsoleteDateTimeApi")
//    public boolean setTimePHBecameActive (Date date) {
//        boolean dateSet = false;
//
//        try {
//            kitchenPrinterTerminal.setTimePHBecameActive(date);
//            dateSet = true;
//        }
//        catch (Exception e) {
//            Logger.logException(e, logFileName);
//            Logger.logMessage("There was a problem trying to set the time the active printer host became active as " +
//                    "viewed by the terminal with a hostname of "+Objects.toString(getPHHostname(), "NULL")+" in " +
//                    "PeripheralsDataManager.setTimePHBecameActive", logFileName, Logger.LEVEL.ERROR);
//        }
//
//        return dateSet;
//    }
//
//    /**
//     * Called through XML RPC to obtain the time the active printer host became active as viewed by this terminal.
//     *
//     * @return {@link Date} The time the active printer host became active as viewed by this terminal.
//     */
//    public Date getTimePHBecameActive () {
//        Date timePHBecameActive = null;
//
//        try {
//            timePHBecameActive = kitchenPrinterTerminal.getTimePHBecameActive();
//        }
//        catch (Exception e) {
//            Logger.logException(e, logFileName);
//            Logger.logMessage("There was a problem trying to get the time active printer host became active as viewed " +
//                    "by the terminal with a hostname of "+Objects.toString(getPHHostname(), "NULL")+" in " +
//                    "PeripheralsDataManager.getTimePHBecameActive", logFileName, Logger.LEVEL.ERROR);
//        }
//
//        return timePHBecameActive;
//    }

    /**
     * Updates the LastOnlineOrderCheck field for the MAC address of the printer host configured as the primary
     * within the revenue center.
     *
     * @param macAddress {@link String} MAC address of the currently active printer host.
     * @return Whether or not the LastOnlineOrderCheck field could be updated for the MAC address of the printer host
     *         configured as the primary within the revenue center.
     */
    public boolean updateLastOnlineOrderCheckForMac (String macAddress) {
        boolean updateSuccess = false;

        try {
            if ((StringFunctions.stringHasContent(macAddress))) {
                int printerHostID = 0;
                Object queryRes = dm.parameterizedExecuteScalar("data.kitchenPrinter.getPrinterHostIDFromMacAddress", new Object[]{macAddress}, logFileName);
                if ((queryRes != null) && (!queryRes.toString().isEmpty()) && (NumberUtils.isNumber(queryRes.toString()))) {
                    printerHostID = Integer.parseInt(queryRes.toString());
                }
                if ((printerHostID > 0) && (dm.parameterizedExecuteNonQuery("data.kitchenPrinter.updateLastOnlineOrderCheckForMacAddress", new Object[]{macAddress, printerHostID}, logFileName) > 0)) {
                    updateSuccess = true;
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, logFileName);
            Logger.logMessage("There was a problem trying to update the LastOnlineOrderCheck field for the MAC " +
                    "address of the printer host configured as the primary within the revenue center with the " +
                    "given MAC address of "+Objects.toString(macAddress, "NULL")+" in " +
                    "PeripheralsDataManager.updateLastOnlineOrderCheckForMac");
        }

        return updateSuccess;
    }

    /**
     * <p>Called through XML RPC to update the last time online orders were checked for on the server.</p>
     *
     * @param inSvcPHMac The MAC address {@link String} of the in-service printer host.
     * @return Whether or not the update was successful.
     */
    public boolean updatePHLastOnlineOrderCheck (String inSvcPHMac) {
        boolean updateSuccess = false;

        try {
            if (StringFunctions.stringHasContent(inSvcPHMac)) {
                // update the last time printer host check for online orders on the server
                if (dm.parameterizedExecuteNonQuery("data.kitchenPrinter.updatePHLastOnlineOrderCheck", new Object[]{inSvcPHMac}, logFileName) > 0) {
                    updateSuccess = true;
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, logFileName);
            Logger.logMessage("Unable to set the last time the printer host checked for online orders in " +
                    "PeripheralsDataManager.updatePHLastOnlineOrderCheck", logFileName, Logger.LEVEL.ERROR);
        }

        return updateSuccess;
    }

    /**
     * <p>Called through XML RPC to get the MAC address of the in-service printer host on the server.</p>
     *
     * @param macAddress The MAC address {@link String} for the MAC address of this terminal.
     * @return The MAC address {@link String} of the in-service printer host from the server.
     */
    public String getInServicePrinterHost (String macAddress) {
        String inSvcPHMac = "";

        try {
            Object queryRes = dm.parameterizedExecuteScalar("data.kitchenPrinter.getInServicePH", new Object[]{macAddress}, logFileName);
            if ((queryRes != null) && (StringFunctions.stringHasContent(queryRes.toString()))) {
                inSvcPHMac = queryRes.toString();
            }
        }
        catch (Exception e) {
            Logger.logException(e, logFileName);
            Logger.logMessage("There was a problem trying to get the in service printer host on the server in " +
                    "PeripheralsDataManager.getInServicePrinterHost", logFileName, Logger.LEVEL.ERROR);
        }

        return inSvcPHMac;
    }

    /**
     * Called through XML RPC to get the previous order numbers in the given transaction.
     *
     * @param paTransactionID ID of the transaction to get the previous order numbers within.
     * @return {@link ArrayList} Previous order numbers in the given transaction.
     */
    public ArrayList getPrevOrderNumsInTxn (int paTransactionID) {
        ArrayList res = new ArrayList();

        try {
            res = dm.parameterizedExecuteQuery("data.newKitchenPrinter.GetOldOrderNumsFromTxnID", new Object[]{paTransactionID}, true);
        }
        catch (Exception e) {
            Logger.logException(e, logFileName);
            Logger.logMessage(String.format("There was a problem trying to get the previous order numbers within the " +
                            "transaction with an ID of %s in PeripheralsDataManager.getPrevOrderNumsInTxn",
                    Objects.toString(paTransactionID, "NULL")), logFileName, Logger.LEVEL.ERROR);
        }

        return res;
    }

    /**
     * Called through XML RPC to find previous transactions within the given transaction for open transactions.
     *
     * @param paTransactionID ID of the transaction to get the previous transactions within.
     * @return {@link ArrayList} Previous transactions within in the given transaction.
     */
    @SuppressWarnings("unchecked")
    public ArrayList getTransactionsInChain (int paTransactionID) {
        ArrayList previousTxns = new ArrayList();

        try {
            ArrayList queryRes = dm.parameterizedExecuteQuery("data.newKitchenPrinter.GetTxnsInChain", new Object[]{paTransactionID}, true);
            if ((queryRes != null) && (!queryRes.isEmpty())) {
                queryRes.forEach(hmObj -> {
                    if ((hmObj != null) && (hmObj instanceof HashMap) && (!((HashMap) hmObj).isEmpty())) {
                        HashMap txnInChain = (HashMap) hmObj;
                        int txnID = HashMapDataFns.getIntVal(txnInChain, "PATRANSACTIONID");
                        if (txnID > 0) {
                            previousTxns.add(txnID);
                        }
                    }
                });
            }
        }
        catch (Exception e) {
            Logger.logException(e, logFileName);
            Logger.logMessage(String.format("There was a problem trying to get the previous transactions within the " +
                            "transaction chain for a transaction with an ID of %s in PeripheralsDataManager.getTransactionsInChain",
                    Objects.toString(paTransactionID, "NULL")), logFileName, Logger.LEVEL.ERROR);
        }

        return previousTxns;
    }

    /**
     * Called through XML RPC to find KDS stations that previous transactions within this open transaction were sent to already.
     *
     * @param txnsInChain {@link String} Previous transactions within this open transaction.
     * @return {@link ArrayList} Stations previous transactions within this open transaction had products sent to.
     */
    @SuppressWarnings({"unchecked", "SpellCheckingInspection"})
    public ArrayList getOrderSentToStations (String txnsInChain) {
        ArrayList previousOrders = new ArrayList();

        try {
            ArrayList queryRes = dm.parameterizedExecuteQuery("data.newKitchenPrinter.GetStationsOrderSentTo", new Object[]{txnsInChain}, true);
            if (!DataFunctions.isEmptyCollection(queryRes)) {
                queryRes.forEach(hmObj -> {
                    if ((hmObj != null) && (hmObj instanceof HashMap) && (!((HashMap) hmObj).isEmpty())) {
                        HashMap orderAndStation = (HashMap) hmObj;
                        previousOrders.add(orderAndStation);
                    }
                });
            }
        }
        catch (Exception e) {
            Logger.logException(e, logFileName);
            Logger.logMessage(String.format("There was a problem trying to determine which KDS stations have received " +
                            "orders for the transactions with IDs of %s in PeripheralsDataManager.getOrderSentToStations",
                    Objects.toString(txnsInChain, "NULL")), logFileName, Logger.LEVEL.ERROR);
        }

        return previousOrders;
    }

    /**
     * Called through XML RPC to get print queue details associated with the given printer queue ID.
     *
     * @param paTransactionID The ID of the transaction to get the KDS details for.
     * @return {@link ArrayList} Print queue details associated with the given printer queue ID..
     */
    @SuppressWarnings("unchecked")
    public String getKDSDetails (int paTransactionID) {
        String kdsDetails = "";

        try {
            Object queryRes = dm.parameterizedExecuteScalar("data.kitchenPrinter.GetKDSDetails", new Object[]{paTransactionID});
            if ((queryRes != null) && (!queryRes.toString().isEmpty())) {
                kdsDetails = queryRes.toString();
            }
        }
        catch (Exception e) {
            Logger.logException(e, logFileName);
            Logger.logMessage(String.format("There was a problem trying to get the KDS details associated with " +
                            "the transaction with an ID of %s in PeripheralsDataManager.getKDSDetails",
                    Objects.toString(paTransactionID, "NULL")), logFileName, Logger.LEVEL.ERROR);
        }

        return kdsDetails;
    }

    /**
     * Called through XML RPC to check if the given transaction is open.
     *
     * @param paTransactionID The ID of the transaction to check.
     * @return Whether or not the given transaction is open.
     */
    @SuppressWarnings("unchecked")
    public boolean isTxnOpen (int paTransactionID) {
        boolean isTxnOpen = false;

        try {
            Object queryRes = dm.parameterizedExecuteScalar("data.newKitchenPrinter.GetIsTxnOpen", new Object[]{paTransactionID});
            isTxnOpen = ((queryRes != null) && (!queryRes.toString().isEmpty()) && (queryRes.toString().equalsIgnoreCase("1")));
        }
        catch (Exception e) {
            Logger.logException(e, logFileName);
            Logger.logMessage(String.format("There was a problem trying to determine whether or not the transaction " +
                            "with an ID of %s is open in PeripheralsDataManager.isTxnOpen",
                    Objects.toString(paTransactionID, "NULL")), logFileName, Logger.LEVEL.ERROR);
        }

        return isTxnOpen;
    }

    /**
     * Called through XML RPC to find order numbers that were sent to KDS for the given transaction ID.
     *
     * @param paTransactionID ID of the transaction to check.
     * @return {@link ArrayList} Order numbers that were sent to KDS for the given transaction ID.
     */
    @SuppressWarnings("unchecked")
    public ArrayList getOrderNumsSentToKDSForTxn (int paTransactionID) {
        ArrayList previousOrderNums = new ArrayList();

        try {
            ArrayList queryRes = dm.parameterizedExecuteQuery("data.newKitchenPrinter.GetOrderNumsSentToKDSForTxnID", new Object[]{paTransactionID}, true);
            if ((queryRes != null) && (!queryRes.isEmpty())) {
                queryRes.forEach(hmObj -> {
                    if ((hmObj != null) && (hmObj instanceof HashMap) && (!((HashMap) hmObj).isEmpty())) {
                        HashMap prevOrderNumSentToKDS = (HashMap) hmObj;
                        int orderNum = HashMapDataFns.getIntVal(prevOrderNumSentToKDS, "ORDERNUM");
                        if (orderNum > 0) {
                            previousOrderNums.add(orderNum);
                        }
                    }
                });
            }
        }
        catch (Exception e) {
            Logger.logException(e, logFileName);
            Logger.logMessage(String.format("There was a problem trying to get the order numbers already sent to " +
                            "KDS for the transaction with an ID of %s in PeripheralsDataManager.getOrderNumsSentToKDSForTxn",
                    Objects.toString(paTransactionID, "NULL")), logFileName, Logger.LEVEL.ERROR);
        }

        return previousOrderNums;
    }

    /**
     * Called through XML RPC to find the order number associated with each given transaction ID.
     *
     * @param txnIDs {@link String} ID of the transactions to check.
     * @return {@link String} The order number associated with each given transaction ID.
     */
    @SuppressWarnings("unchecked")
    public ArrayList getOrderNumsForGivenTxnIDs (String txnIDs) {
        ArrayList orderNums = new ArrayList();

        try {
            ArrayList queryRes = dm.parameterizedExecuteQuery("data.kitchenPrinter.GetOrderNumsGivenTxnIDs", new Object[]{txnIDs}, true);
            if (!DataFunctions.isEmptyCollection(queryRes)) {
                queryRes.forEach(hm -> {
                    if ((hm != null) && (hm instanceof HashMap) && (!DataFunctions.isEmptyMap(((HashMap) hm)))) {
                        orderNums.add(hm);
                    }
                });
            }
        }
        catch (Exception e) {
            Logger.logException(e, logFileName);
            Logger.logMessage(String.format("There was a problem trying to get the order number associated with the " +
                            "transactions with a IDs of %s in PeripheralsDataManager.getOrderNumsForGivenTxnIDs",
                    Objects.toString(txnIDs, "NULL")), logFileName, Logger.LEVEL.ERROR);
        }

        return orderNums;
    }

    /**
     * Called through XML RPC to determine the expeditor station ID for the given expeditorPrinterID.
     *
     * @param expeditorPrinterID ID of the printer to get the station ID for.
     * @return {@link String} The station ID for the given expeditorPrinterID.
     */
    public String getExpeditorStationID (int expeditorPrinterID) {
        String expeditorStationID = "";

        try {
            Object queryRes = dm.parameterizedExecuteScalar("data.kitchenPrinter.GetExpeditorPtrStnID", new Object[]{expeditorPrinterID});
            if ((queryRes != null) && (!queryRes.toString().isEmpty())) {
                expeditorStationID = queryRes.toString();
            }
        }
        catch (Exception e) {
            Logger.logException(e, logFileName);
            Logger.logMessage(String.format("There was a problem trying to get the expeditor printer's station ID " +
                            "for the printer with an ID of %s in PeripheralsDataManager.getExpeditorStationID",
                    Objects.toString(expeditorPrinterID, "NULL")), logFileName, Logger.LEVEL.ERROR);
        }

        return expeditorStationID;
    }

    /**
     * Called through XML RPC to determine the remote order station ID for the given remoteOrderPrinterID.
     *
     * @param remoteOrderPrinterID ID of the printer to get the station ID for.
     * @return {@link String} The station ID for the given remoteOrderPrinterID.
     */
    public String getRemoteOrderStationID (int remoteOrderPrinterID) {
        String remoteOrderStationID = "";

        try {
            Object queryRes = dm.parameterizedExecuteScalar("data.kitchenPrinter.GetRemoteOrderPtrStnID", new Object[]{remoteOrderPrinterID});
            if ((queryRes != null) && (!queryRes.toString().isEmpty())) {
                remoteOrderStationID = queryRes.toString();
            }
        }
        catch (Exception e) {
            Logger.logException(e, logFileName);
            Logger.logMessage(String.format("There was a problem trying to get the remote order printer's station ID " +
                            "for the printer with an ID of %s in PeripheralsDataManager.getRemoteOrderStationID",
                    Objects.toString(remoteOrderPrinterID, "NULL")), logFileName, Logger.LEVEL.ERROR);
        }

        return remoteOrderStationID;
    }

    public void printFreedomPayReceipt(ArrayList receiptContentAL, ReceiptModel receiptModel) throws Exception
    {
        try
        {
            Logger.logMessage("Entered PeripheralsDataManager.printFreedomPayReceipt", logFileName, Logger.LEVEL.DEBUG);
            ReceiptGen rcptGen = new ReceiptGen();

            // Create and set receipt content
            FreedomPayReceiptData fpRcptData = new FreedomPayReceiptData();
            String receiptContent = receiptContentAL.get(0).toString();

            String appendedCopyText = "************* CUSTOMER COPY ************\n";
            if (receiptModel.isMerchantCopy()) {
                appendedCopyText = "************* MERCHANT COPY ************\n";
            }
            receiptContent = receiptContent + appendedCopyText;


            fpRcptData.setReceiptContent(receiptContent);

            // Assign to ReceiptGen
            if (rcptGen != null)
            {
                rcptGen.setPaymentProcessorRcptData(fpRcptData);
            }

            // Create formatter
            FreedomPayRcptFormatter fpRcptFormatter = new FreedomPayRcptFormatter();

            // Set formatter
            rcptGen.setPaymentProcessorRcptFormatter(fpRcptFormatter);

            // Print the receipt
            String retVal = "";
            POSPrinter ptr = null;
            if (printer != null)
            {
                ptr = printer.getPOSPrinter();
                IRenderer printerRenderer = null;
                if (ptr != null)
                {
                    printerRenderer = rcptGen.createPrinterRenderer(ptr, printer.getLogicalPrinterName());
                }

                // print the receipt
                if (printerRenderer != null)
                {
                    rcptGen.buildPaymentProcessorRcpt(printerRenderer);
                    if(printerRenderer.getLastException() != null)
                    {
                        throw printerRenderer.getLastException();
                    }
                }
            }
        }
        catch (Exception e)
        {
            Logger.logException("PeripheralsDataManager.printFreedomPayReceipt error", logFileName, e);
        }
    }


    /**
     * <p>Updates the status of a PAPrinterQueue record in the database.</p>
     *
     * @param paPrinterQueueID The ID of PAPrinterQueue record to update.
     * @param printStatusID The ID of the print status to update to.
     * @return Whether or not the records could be updated.
     */
    public boolean updatePrintStatusOfPAPrinterQueue (int paPrinterQueueID, int printStatusID) {

        // make sure there is actually a PAPrinterQueueDetail record to be update
        if (paPrinterQueueID <= 0) {
            Logger.logMessage("The PAPrinterQueueID passed to PeripheralsDataManager.updatePrintStatusOfPAPrinterQueue must be greater " +
                    "than 0, unable to update the print status of the print queue record, now returning false.", Logger.LEVEL.ERROR);
            return false;
        }

        // make sure the print status ID is valid
        if (printStatusID <= 0) {
            Logger.logMessage("The print status ID passed to PeripheralsDataManager.updatePrintStatusOfPAPrinterQueue must be " +
                    "greater than 0, unable to update the print status of the print queue record, now returning false.", Logger.LEVEL.ERROR);
            return false;
        }

        // update the record in the database
        DynamicSQL sql = new DynamicSQL("data.kms.UpdatePAPrinterQueuePrintStatus").addIDList(1, paPrinterQueueID).addIDList(2, printStatusID).setLogFileName(getKPLogFileName());
        if (sql.runUpdate(dm) < 0) {
            Logger.logMessage(String.format("Failed to update the PAPrinterQueue record with an ID of %s to a print status of %s in PeripheralsDataManager.updatePrintStatusOfPAPrinterQueue",
                    Objects.toString(paPrinterQueueID, "N/A"),
                    Objects.toString(printStatusID, "N/A")));
            return false;
        }

        return true;
    }

    /**
     * <p>Updates the status of PAPrinterQueueDetail records in the database.</p>
     *
     * @param paPrinterQueueDetailIDs An {@link ArrayList} of {@link Integer} corresponding to the IDs of PAPrinterQueueDetail records to update.
     * @param printStatusID The ID of the print status to update to.
     * @return Whether or not the records could be updated.
     */
    public boolean updatePrintStatusOfPAPrinterQueueDetails (ArrayList<Integer> paPrinterQueueDetailIDs, int printStatusID) {

        // make sure there are actually PAPrinterQueueDetail records to be updated
        if (DataFunctions.isEmptyCollection(paPrinterQueueDetailIDs)) {
            Logger.logMessage("The PAPrinterQueueDetailIDs passed to PeripheralsDataManager.updatePrintStatusOfPAPrinterQueueDetails can't " +
                    "be null or empty, unable to update the print status of the print queue details, now returning false.", Logger.LEVEL.ERROR);
            return false;
        }

        // make sure the print status ID is valid
        if (printStatusID <= 0) {
            Logger.logMessage("The print status ID passed to PeripheralsDataManager.updatePrintStatusOfPAPrinterQueueDetails must be " +
                    "greater than 0, unable to update the print status of the print queue details, now returning false.", Logger.LEVEL.ERROR);
            return false;
        }

        // update the records in the database
        DynamicSQL sql = new DynamicSQL("data.kms.UpdatePAPrinterQueueDetailsPrintStatus").addLargeIDList(1, paPrinterQueueDetailIDs).addIDList(2, printStatusID).setLogFileName(getKPLogFileName());
        if (sql.runUpdate(dm) < 0) {
            Logger.logMessage(String.format("Failed to update the PAPrinterQueueDetails records with IDs of %s to a print status of %s in PeripheralsDataManager.updatePrintStatusOfPAPrinterQueueDetails",
                    Objects.toString(StringFunctions.buildStringFromList(paPrinterQueueDetailIDs, ","), "N/A"),
                    Objects.toString(printStatusID, "N/A")));
            return false;
        }

        return true;
    }

    /**
     * <p>Waits for the NCR or SNBC printer to finish printing any sale receipts.</p>
     *
     * @param posPrinter The POS printer that will print the receipt and is either a NCR or SNBC.
     */
    private void waitForNCROrSNBC (POSPrinter posPrinter) {

        // make sure the POS printer is valid
        if (posPrinter == null) {
            Logger.logMessage("The POSPrinter passed to PrintedKitchenErrorReceipt.waitForNCROrSNBC can't be null, " +
                    "unable to wait for the NCR or SNBC printer.", Logger.LEVEL.ERROR);
            return;
        }

        // run the check to get the state of the NCR or SNBC printer
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        try {
            Future<Boolean> ncrOrSNBCDoneWithSaleReceipt = executorService.submit(new NCR_SNBC_PrinterCheck(printer, posPrinter, getKPLogFileName()));

            boolean messagePrinted = false;
            while (!ncrOrSNBCDoneWithSaleReceipt.isDone()) {
                if (!messagePrinted) {
                    Logger.logMessage("Now waiting for the NCR or SNBC to finish printing sale receipts.", getKPLogFileName(), Logger.LEVEL.TRACE);
                    messagePrinted = true;
                }
            }

            if ((ncrOrSNBCDoneWithSaleReceipt.get() != null) && (ncrOrSNBCDoneWithSaleReceipt.get())) {
                Logger.logMessage("The NCR or SNBC printer has finished printing any sale receipts and may now print kitchen error receipts.", getKPLogFileName(), Logger.LEVEL.TRACE);
            }

            // shutdown the executor service
            if (!executorService.awaitTermination(500L, TimeUnit.MILLISECONDS)) { // wait a half second for all tasks to terminate, if they don't terminate within that time, forcibly stop the executor service
                executorService.shutdownNow();
            }
        }
        catch (Exception e) {
            Logger.logException(e, getKPLogFileName());
            Logger.logMessage("A problem occurred while trying to wait for the NCR or SNBC printer to finish printing sale receipts in PrintedKitchenErrorReceipt.waitForNCROrSNBC!", getKPLogFileName(), Logger.LEVEL.ERROR);
            executorService.shutdownNow();
        }

    }

    /**
     * <p>Gets the WAR that should be responsible for error receipt printing.</p>
     *
     * @return The name {@link String} of the WAR that should be responsible for error receipt printing.
     */
    private String getErrorReceiptPrintingWAR () {
        // get the WARs on this machine
        ArrayList<String> wars = CommonAPI.getWARsOnThisMachine();

        if ((!DataFunctions.isEmptyCollection(wars)) && (wars.size() == 1)) {
            return wars.get(0);
        }
        else if ((!DataFunctions.isEmptyCollection(wars)) && (wars.size() >= 2)) {
            for (String war : wars) {
                if (StringFunctions.containsIgnoreCase(war, "pos")) {
                    return war;
                }
            }
        }

        Logger.logMessage("Unable to determine which WAR should be in charge of error receipt in " +
                "PeripheralsDataManager.getErrorReceiptPrintingWAR, error receipt will fail to be printed!", Logger.LEVEL.ERROR);

        return null;
    }

    /**
     * <p>Prints the kitchen print jobs locally.</p>
     *
     * @param printJobs An {@link ArrayList} of {@link ArrayList} corresponding to the print jobs to print locally.
     * @return Whether or not the kitchen print jobs could be printed successfully.
     */
    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    public boolean printKitchenPrintJobsLocally (ArrayList<ArrayList> printJobs) {

        // confirm that there are print jobs that need to be printed
        if (DataFunctions.isEmptyCollection(printJobs)) {
            Logger.logMessage("The print jobs passed to PeripheralsDataManager.printKitchenPrintJobsLocally can't be null or " +
                    "empty, unable to print any failed kitchen print jobs, returning false!", getKPLogFileName(), Logger.LEVEL.ERROR);
            return false;
        }

        String thisWAR = commonMMHFunctions.getInstanceName();
        ArrayList<String> posWars = CommonAPI.getWARsOnThisMachine();
        posWars.removeIf(war -> (!war.contains("pos")));

        if ((StringFunctions.stringHasContent(thisWAR)) && (StringFunctions.containsIgnoreCase(thisWAR, "oms")) && (!DataFunctions.isEmptyCollection(posWars))) {
            String errorReceiptPrintingWAR = getErrorReceiptPrintingWAR();
            if ((StringFunctions.stringHasContent(errorReceiptPrintingWAR)) && (!thisWAR.equalsIgnoreCase(errorReceiptPrintingWAR))) {
                XmlRpcUtil xmlRpcUtil = XmlRpcUtil.getInstance();
                Logger.logMessage(String.format("This %s WAR is making a XML RPC call to the %s WAR to print the print jobs locally.",
                        Objects.toString(thisWAR, "N/A"),
                        Objects.toString(errorReceiptPrintingWAR, "N/A")));
                // send the local print jobs to the war running on this machine
                String url = "http://"+ getTerminalHostname(getTerminalMacAddress())+"/"+errorReceiptPrintingWAR+"/QuickCharge/XmlRpcManager";
                Object res = xmlRpcUtil.xmlRpcPost(url, getTerminalMacAddress(), getPHHostname(), "PeripheralsDataManager.printKitchenPrintJobsLocally", new Object[]{((ArrayList) printJobs)});
                boolean printedSuccessfully = xmlRpcUtil.parseBooleanXmlRpcResponse(res, "PeripheralsDataManager.printKitchenPrintJobsLocally");
                boolean printQueueUpdatedSuccessfully = true;
                boolean printQueueDetailsUpdatedSuccessfully = true;
                if (printedSuccessfully) {
                    ArrayList<LocalPrintJob> localPrintJobs = ErrorReceipt.convertToLocalPrintJobs(getKPLogFileName(), dm, printJobs);
                    if (!DataFunctions.isEmptyCollection(localPrintJobs)) {
                        for (LocalPrintJob localPrintJob : localPrintJobs) {
                            if (!updatePrintStatusOfPAPrinterQueue(localPrintJob.getPAPrinterQueueID(), PrintStatusType.PRINTED)) {
                                printQueueUpdatedSuccessfully = false;
                            }
                            Set<Integer> paPrinterQueueDetailIDs = DataFunctions.getUniqueValsAtKeyFromALOfHM(localPrintJob.getDetails(), "PAPRINTERQUEUEDETAILID");
                            if (!DataFunctions.isEmptyCollection(paPrinterQueueDetailIDs)) {
                                if (!updatePrintStatusOfPAPrinterQueueDetails(new ArrayList<>(paPrinterQueueDetailIDs), PrintStatusType.PRINTED)) {
                                    printQueueDetailsUpdatedSuccessfully = false;
                                }
                            }
                        }
                    }
                }
                else {
                    ArrayList<LocalPrintJob> localPrintJobs = ErrorReceipt.convertToLocalPrintJobs(getKPLogFileName(), dm, printJobs);
                    if (!DataFunctions.isEmptyCollection(localPrintJobs)) {
                        for (LocalPrintJob localPrintJob : localPrintJobs) {
                            if (!updatePrintStatusOfPAPrinterQueue(localPrintJob.getPAPrinterQueueID(), PrintStatusType.FAILEDLOCALPRINT)) {
                                printQueueUpdatedSuccessfully = false;
                            }
                            Set<Integer> paPrinterQueueDetailIDs = DataFunctions.getUniqueValsAtKeyFromALOfHM(localPrintJob.getDetails(), "PAPRINTERQUEUEDETAILID");
                            if (!DataFunctions.isEmptyCollection(paPrinterQueueDetailIDs)) {
                                if (!updatePrintStatusOfPAPrinterQueueDetails(new ArrayList<>(paPrinterQueueDetailIDs), PrintStatusType.FAILEDLOCALPRINT)) {
                                    printQueueDetailsUpdatedSuccessfully = false;
                                }
                            }
                        }
                    }
                }
                if (printedSuccessfully) {
                    printedSuccessfully = ((printQueueUpdatedSuccessfully) && (printQueueDetailsUpdatedSuccessfully));
                }

                return printedSuccessfully;
            }
        }

        // try to open a connection to the printer so we can print locally
        createPrinterIfNeeded();

        boolean printJobsFailed = false;

        if (printer != null) {
            synchronized (printerLockObject) { // synchronize on the printer lock object, NOT the printer itself
                if (!printer.isInErrorState()) {
                    ReceiptGen receiptGen = new ReceiptGen();
                    POSPrinter posPrinter = printer.getPOSPrinter();
                    IRenderer printerRenderer = null;
                    if (posPrinter != null) {
                        printerRenderer = receiptGen.createPrinterRenderer(posPrinter, printer.getLogicalPrinterName());
                    }
                    if (printerRenderer != null) {

                        // NCR and SNBC printers take a little longer to finish printing sale receipts, wait for them to finish that process
                        if ((isNCRPrinterConnected) || (isSNBCPrinterConnected)) {
                            waitForNCROrSNBC(posPrinter);
                        }

                        // check that transaction printing mode can be turned off
                        if ((!printerRenderer.end()) && (printerRenderer.getLastException() != null)) {
                            Logger.logMessage(String .format("Unable to turn off transaction printing mode for the renderer, " +
                                            "marking the given print job(s) as print job(s) that failed to print locally due to the error: %s, in " +
                                            "PeripheralsDataManager.printKitchenPrintJobsLocally!",
                                    Objects.toString(printerRenderer.getLastException().getMessage(), "N/A")), getKPLogFileName(), Logger.LEVEL.ERROR);
                            printJobsFailed = true;
                        }

                        // check that transaction printing mode can be turned on
                        if (!printerRenderer.begin()) {
                            Logger.logMessage("Unable to turn on transaction printing mode, the printer may be in an error " +
                                    "state in PeripheralsDataManager.printKitchenPrintJobsLocally, marking the given print job(s) as " +
                                    "print job(s) that failed to print locally!", getKPLogFileName(), Logger.LEVEL.ERROR);
                            printJobsFailed = true;
                        }

                        // determine which print jobs fall under the PAPrinterQueue record
                        PrintedErrorReceipt printedErrorReceipt = new PrintedErrorReceipt(getKPLogFileName(), dm, printJobs, printerRenderer);
                        ArrayList<LocalPrintJob> localPrintJobs = printedErrorReceipt.fixPrintJobs();
                        Map<Integer, Collection<LocalPrintJob>> localPrintJobsForPrintQueueRec = DataFunctions.sortCollectionIntoMapBasedOnProp(localPrintJobs, LocalPrintJob.class, "paPrinterQueueID");
                        HashMap<Integer, Integer> localPrintJobsProcessedForPrintQueueRec = new HashMap<>();
                        HashMap<Integer, Integer> localPrintJobsProcessedSuccessfullyForPrintQueueRec = new HashMap<>();
                        for (LocalPrintJob localPrintJob : localPrintJobs) {
                            if (DataFunctions.isEmptyMap(localPrintJobsProcessedForPrintQueueRec)) {
                                localPrintJobsProcessedForPrintQueueRec.put(localPrintJob.getPAPrinterQueueID(), 1);
                            }
                            else if (localPrintJobsProcessedForPrintQueueRec.containsKey(localPrintJob.getPAPrinterQueueID())) {
                                localPrintJobsProcessedForPrintQueueRec.put(localPrintJob.getPAPrinterQueueID(), localPrintJobsProcessedForPrintQueueRec.get(localPrintJob.getPAPrinterQueueID()) + 1);
                            }
                            else {
                                localPrintJobsProcessedForPrintQueueRec.put(localPrintJob.getPAPrinterQueueID(), 1);
                            }

                            // check whether or not to update the print queue record in the database
                            Collection<LocalPrintJob> localPrintJobsForPrintQueueRecCol = null;
                            if (!DataFunctions.isEmptyMap(localPrintJobsForPrintQueueRec)) {
                                localPrintJobsForPrintQueueRecCol = localPrintJobsForPrintQueueRec.get(localPrintJob.getPAPrinterQueueID());
                            }
                            boolean updatePrintQueueRec = ((!DataFunctions.isEmptyCollection(localPrintJobsForPrintQueueRecCol))
                                    && (localPrintJobsProcessedForPrintQueueRec.get(localPrintJob.getPAPrinterQueueID()) == localPrintJobsForPrintQueueRecCol.size()));

                            if (printedErrorReceipt.buildErrorReceipt(localPrintJob)) {
                                Set<Integer> paPrinterQueueDetailIDs = DataFunctions.getUniqueValsAtKeyFromALOfHM(localPrintJob.getDetails(), "PAPRINTERQUEUEDETAILID");
                                if (!DataFunctions.isEmptyCollection(paPrinterQueueDetailIDs)) {
                                    if (updatePrintStatusOfPAPrinterQueueDetails(new ArrayList<>(paPrinterQueueDetailIDs), PrintStatusType.PRINTED)) {
                                        if (DataFunctions.isEmptyMap(localPrintJobsProcessedSuccessfullyForPrintQueueRec)) {
                                            localPrintJobsProcessedSuccessfullyForPrintQueueRec.put(localPrintJob.getPAPrinterQueueID(), 1);
                                        }
                                        else if ((localPrintJobsProcessedSuccessfullyForPrintQueueRec.containsKey(localPrintJob.getPAPrinterQueueID()))
                                                && (localPrintJobsProcessedSuccessfullyForPrintQueueRec.get(localPrintJob.getPAPrinterQueueID()) != null)) {
                                            localPrintJobsProcessedSuccessfullyForPrintQueueRec.put(localPrintJob.getPAPrinterQueueID(),
                                                    localPrintJobsProcessedSuccessfullyForPrintQueueRec.get(localPrintJob.getPAPrinterQueueID()) + 1);
                                        }
                                        else {
                                            localPrintJobsProcessedSuccessfullyForPrintQueueRec.put(localPrintJob.getPAPrinterQueueID(), 1);
                                        }
                                    }
                                }
                                else {
                                    Logger.logMessage("Unable to determine the IDs of PAPrinterQueueDetail records that need to have their print " +
                                            "status updated in PeripheralsDataManager.printKitchenPrintJobsLocally!", getKPLogFileName(), Logger.LEVEL.ERROR);
                                }
                            }
                            else {
                                Set<Integer> paPrinterQueueDetailIDs = DataFunctions.getUniqueValsAtKeyFromALOfHM(localPrintJob.getDetails(), "PAPRINTERQUEUEDETAILID");
                                if (!DataFunctions.isEmptyCollection(paPrinterQueueDetailIDs)) {
                                    updatePrintStatusOfPAPrinterQueueDetails(new ArrayList<>(), PrintStatusType.FAILEDLOCALPRINT);
                                }
                                else {
                                    Logger.logMessage("Unable to determine the IDs of PAPrinterQueueDetail records that need to have their print " +
                                            "status updated in PeripheralsDataManager.printKitchenPrintJobsLocally!", getKPLogFileName(), Logger.LEVEL.ERROR);
                                }
                            }

                            if (printJobsFailed) {
                                updatePrintStatusOfPAPrinterQueue(localPrintJob.getPAPrinterQueueID(), PrintStatusType.FAILEDLOCALPRINT);
                                Set<Integer> paPrinterQueueDetailIDs = DataFunctions.getUniqueValsAtKeyFromALOfHM(localPrintJob.getDetails(), "PAPRINTERQUEUEDETAILID");
                                if (!DataFunctions.isEmptyCollection(paPrinterQueueDetailIDs)) {
                                    updatePrintStatusOfPAPrinterQueueDetails(new ArrayList<>(), PrintStatusType.FAILEDLOCALPRINT);
                                }
                                else {
                                    Logger.logMessage("Unable to determine the IDs of PAPrinterQueueDetail records that need to have their print " +
                                            "status updated in PeripheralsDataManager.printKitchenPrintJobsLocally!", getKPLogFileName(), Logger.LEVEL.ERROR);
                                }
                            }

                            // update the status of the print queue header record
                            int numLocalPrintJobsProcessedSuccessfully = 0;
                            if ((!DataFunctions.isEmptyMap(localPrintJobsProcessedSuccessfullyForPrintQueueRec))
                                    && (localPrintJobsProcessedSuccessfullyForPrintQueueRec.containsKey(localPrintJob.getPAPrinterQueueID()))
                                    && (localPrintJobsProcessedSuccessfullyForPrintQueueRec.get(localPrintJob.getPAPrinterQueueID()) != null)) {
                                numLocalPrintJobsProcessedSuccessfully = localPrintJobsProcessedSuccessfullyForPrintQueueRec.get(localPrintJob.getPAPrinterQueueID());
                            }
                            if ((updatePrintQueueRec) && (localPrintJobsForPrintQueueRecCol.size() == numLocalPrintJobsProcessedSuccessfully)) {
                                updatePrintStatusOfPAPrinterQueue(localPrintJob.getPAPrinterQueueID(), PrintStatusType.PRINTED);
                            }
                            else if (updatePrintQueueRec) {
                                updatePrintStatusOfPAPrinterQueue(localPrintJob.getPAPrinterQueueID(), PrintStatusType.FAILEDLOCALPRINT);
                            }
                        }

                        // finish rendering the receipt
                        if (!printerRenderer.end()) {
                            Logger.logMessage("Unable to turn off transaction printing mode for the renderer even though the receipt has already been rendered in " +
                                    "PeripheralsDataManager.printKitchenPrintJobsLocally!", getKPLogFileName(), Logger.LEVEL.ERROR);
                        }
                    }
                    else {
                        // can't create a renderer so the receipt can't be printed
                        Logger.logMessage("Failed to create a renderer in PeripheralsDataManager.emailKitchenPrintJobsLocally, as a " +
                                "result the kitchen print jobs can't be printed locally, returning false!", getKPLogFileName(), Logger.LEVEL.ERROR);
                        return false;
                    }
                }
            }
        }

        return true;
    }

    // endregion

    //--------------------------------------------------------------------------
    //region Other
    public void setEmailSettings()
    {
        try
        {
            // Try to get email settings from the database
            ArrayList emailCfgAL = dm.parameterizedExecuteQuery("data.ReceiptGen.EmailCfg", null, "", true, null);
            if (emailCfgAL != null && emailCfgAL.size() > 0)
            {
                HashMap emailCfg = (HashMap)emailCfgAL.get(0);
                emailUser = emailCfg.get("SMTPUSER").toString().trim();
                emailServer = emailCfg.get("SMTPSERVER").toString().trim();
                emailPort = emailCfg.get("SMTPPORT").toString().trim();
                emailPassword = emailCfg.get("SMTPPASSWORD").toString().trim();
                if(emailPassword != "")
                {
                    emailPassword = StringFunctions.decodePassword(emailPassword).trim();
                }
                emailFromAddress = emailCfg.get("SMTPFROMADDRESS").toString().trim();
            }
            else
            {
                //The email settings couldn't be loaded from the database... try pulling them from the app.properties
                emailFromAddress = MMHProperties.getAppSetting("site.communication.email.fromaddress");
                emailServer = MMHProperties.getAppSetting("site.communication.email.smtp.host");
                emailPort = MMHProperties.getAppSetting("site.communication.email.smtp.port");
                emailUser = MMHProperties.getAppSetting("site.communication.email.smtp.user");
                emailPassword = MMHProperties.getAppSetting("site.communication.email.smtp.pass");
            }
        }
        catch (Exception ex)
        {
            Logger.logException("PeripheralsDataManager.getEmailSettingsFromDB error", logFileName, ex);
        }
    }
    //endregion
    //--------------------------------------------------------------------------
}