package com.mmhayes.common.kitchenapi;

//mmhayes dependencies
import com.mmhayes.common.peripherals.printing.MMHPosPrinter;
import com.mmhayes.common.utils.Logger;
import jp.co.epson.uposcommon.EpsonPOSPrinterConst;
import jpos.JposConst;
import jpos.JposException;
import jpos.POSPrinter;
import jpos.POSPrinterConst;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

//other dependencies

/*
 $Author: mebrown $: Author of last commit
 $Date: 2015-03-17 17:45:43 -0400 (Tue, 17 Mar 2015) $: Date of last commit
 $Rev: 9065 $: Revision of last commit
 Notes:
*/
public class JPosReceiptPrinter {
    private POSPrinter ptr;
    private DecimalFormat df;
    private final String logFileName = "QCPOS_Periph.log";
    private String printerName;
    private String logicalPrinterName;
    private boolean bSupportsCutPaperCommand;

    public JPosReceiptPrinter(String sPrinterName, String sLogicalPrinterName, String logFile)
    {
        printerName = sPrinterName;
        logicalPrinterName = sLogicalPrinterName;
        ptr = new MMHPosPrinter(logFile);
        df = new DecimalFormat("#,###,###,##0.00");
        log("Printer constructor finished", Logger.LEVEL.DEBUG);
        bSupportsCutPaperCommand = true;
    }

    public POSPrinter getPOSPrinter() { return ptr; }
    public String getLogicalPrinterName() { return logicalPrinterName; }

    public synchronized boolean openPrinter()  {
        boolean openResult = false;
        try {
            log("Printer: attempting to open('"+logicalPrinterName+"')", Logger.LEVEL.TRACE);
            ptr.open(logicalPrinterName);
            log("Printer: attempting to claim", Logger.LEVEL.DEBUG);
            ptr.claim(0);
            log("Printer: attempting to enable", Logger.LEVEL.DEBUG);
            ptr.setDeviceEnabled(true);
            ptr.printNormal(2, " \n");
            log("Printer: logical name '" + logicalPrinterName + "' open for input", Logger.LEVEL.TRACE);
            openResult = true;

            String deviceSvcDescription = ptr.getDeviceServiceDescription();
            if (deviceSvcDescription.contains("P80") == true) {
                bSupportsCutPaperCommand = false;
                log("Epson Printer: detected printer is TM-P80", Logger.LEVEL.TRACE);
            }

        } catch (JposException ex) {
            openResult = false;
            log("Printer: failed to open; " + ex.getMessage(), Logger.LEVEL.ERROR);
            handleJposException(ex);
        } catch (Exception e) {
            openResult = false;
            Logger.logMessage("Printer: failed to open; " + e.getMessage());
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            log("Printer: " + sw.toString(), Logger.LEVEL.ERROR);
        }
        return openResult;
    }

    private void log(String message, Logger.LEVEL loggingLevel) {
        Logger.logMessage(printerName + " " + message, logFileName, loggingLevel);
    }

    public boolean closePrinter()
    {
        boolean closeResult = false;
        try {
            log("Printer '"+printerName+"' about to close", Logger.LEVEL.TRACE);
            ptr.setDeviceEnabled(false);
            ptr.release();
            ptr.close();
            closeResult =  true;
            log("Printer '"+printerName+"' successfully closed", Logger.LEVEL.TRACE);
        } catch (JposException ex) {
            log("Printer: failed to close; " + ex.getMessage(), Logger.LEVEL.ERROR);
            handleJposException(ex);
        } catch (Exception e) {
            log("Printer: " + e.getMessage(), Logger.LEVEL.ERROR);
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            log("Printer: " + sw.toString(), Logger.LEVEL.ERROR);
        }

        return closeResult;
    }

    public boolean printHeaderLine(String headerLine) throws JposException {
        try {
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|cA" + headerLine + "\n");
        } catch (JposException ex) {
            log("Printer: failed to print header line; " + ex.getMessage(), Logger.LEVEL.ERROR);
            handleJposException(ex);
            throw ex;
        }

        return true;
    }

    public boolean printFooterLine(String footerLine, boolean newLine) throws JposException {
        try {
            if (newLine) {
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\n \n" + "\u001b|cA" + footerLine + "\n");
            } else {
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|cA" + footerLine + "\n");
            }
        } catch (JposException ex) {
            log("Printer: failed to print footer line; " + ex.getMessage(), Logger.LEVEL.ERROR);
            handleJposException(ex);
            throw ex;
        }

        return true;
    }

    public boolean printDate(String date) throws JposException {
        try {
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|cA" + date + "\n");
        } catch (JposException ex) {
            log("Printer: failed to print date; " + ex.getMessage(), Logger.LEVEL.ERROR);
            handleJposException(ex);
            throw ex;
        }

        return true;
    }

    public boolean printTransNumAndType(String transNum, String transType) throws JposException {
        try {
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|cA" + transType + " : " + transNum + "\n \n");
        } catch (JposException ex) {
            log("Printer: failed to print trans num " + transNum + "& type " + transType + "; " + ex.getMessage(), Logger.LEVEL.ERROR);
            handleJposException(ex);
            throw ex;
        }

        return true;
    }

    public boolean printOrigTransNum(String transNum) throws JposException {
        try {
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|cA" + "ORIG: " + transNum + "\n");
        } catch (JposException ex) {
            log("Printer: failed to print orig trans num " + transNum + "; " + ex.getMessage(), Logger.LEVEL.ERROR);
            handleJposException(ex);
            throw ex;
        }

        return true;
    }

    public boolean printRcptIdInfo(String terminalID, String newTransID, String origTransID, String cashierName, String date, String time) throws JposException {
        try {
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\n" + date);
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|rA" + time + "\n");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "Cashier: " + cashierName);
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|rA" + "TID: " + terminalID + "\n");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, newTransID);

            if (!origTransID.equals("")) {
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|rA" + "ORIG : " + origTransID);
            }

            printNewLine();
        } catch (JposException ex) {
            log("Printer state: " + ptr.getState(), Logger.LEVEL.ERROR);
            log("Printer: failed to print rcpt id info for trans # " + newTransID + "; " + ex.getMessage(), Logger.LEVEL.ERROR);
            handleJposException(ex);
            throw ex;
        }

        return true;
    }

    public boolean printSignatureLine() throws JposException {
        try {
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\n \n \n" + "\u001b|cA" + "X_____________________________________");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\n" + "\u001b|cA" + "SIGNATURE");
        } catch (JposException ex) {
            log("Printer: failed to print signature line; " + ex.getMessage(), Logger.LEVEL.ERROR);
            handleJposException(ex);
            throw ex;
        }

        return true;
    }

    public boolean printMerchantCopyLbl() throws JposException {
        try {
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\n" + "\u001b|cA" + "-----     MERCHANT COPY    -----");
        } catch (JposException ex) {
            log("Printer: failed to print merchant copy lbl; " + ex.getMessage(), Logger.LEVEL.ERROR);
            handleJposException(ex);
            throw ex;
        }

        return true;
    }

    public boolean printCustomerCopyLbl() throws JposException {
        try {
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\n" + "\u001b|cA" + "-----     CUSTOMER COPY    -----");
        } catch (JposException ex) {
            log("Printer: failed to print customer copy lbl; " + ex.getMessage(), Logger.LEVEL.ERROR);
            handleJposException(ex);
            throw ex;
        }

        return true;
    }

    public boolean printNewLine() throws JposException {
        try {
            // ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|cA" + "\n \n" + "   ");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT,  "\n \n");
        } catch (JposException ex) {
            log("Printer: failed to print new line; " + ex.getMessage(), Logger.LEVEL.ERROR);
            handleJposException(ex);
            throw ex;
        }

        return true;
    }

    public boolean printItemDiscount(String itemDsctName, double itemDsctAmt, double itemDiscountUnitPrice, int itemDsctQty, int itemDsctTypeID, int transTypeID, double openDsctPercentAmt, String code) throws JposException {
        try {
            int allowedDsctNameLength = 0;
            // Print discount name
            if (code.length() > 2) {
                allowedDsctNameLength = 14 - code.length();
                code = code.substring(0, 2);
            }

            if (itemDsctName.length() > allowedDsctNameLength && code.length() > 0) {
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "   " + code + ": " + itemDsctName.substring(0, allowedDsctNameLength));
            } else if (code.length() == 0) {
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "   " + itemDsctName);
            } else {
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "   " + code + ": " + itemDsctName);
            }

            // Print percent amount for open item discounts
            if (itemDsctTypeID == 1 && openDsctPercentAmt != -1) {
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "(-" + openDsctPercentAmt + "%)");
            }

            // Print discount quantity
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, " (" + itemDsctQty + " at $");

            if (itemDsctTypeID == 1 && openDsctPercentAmt != -1) {
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, df.format(itemDiscountUnitPrice) + ")" + "\n");
            } else {
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, df.format(itemDiscountUnitPrice) + " each)" + "\n");
            }
        } catch(JposException ex) {
            log("Printer: failed to print item discount; " + ex.getMessage(), Logger.LEVEL.ERROR);
            handleJposException(ex);
            throw ex;
        }

        return true;
    }

    public boolean printSubtotalDiscount(String discountName, double discountAmt, double discountUnitPrice, int discountQty, int transTypeID, String code) throws JposException {
        try {
            int allowedDsctNameLength = 0;

            // Print discount name
            if (code.length() > 2) {
                allowedDsctNameLength = 24 - code.length();
                code = code.substring(0, 2);
            }

            // Print code & discount name
            if (discountName.length() > allowedDsctNameLength && code.length() > 0) {
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "   " + code + ": " + discountName.substring(0, allowedDsctNameLength));
            } else if (code.length() == 0) {
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "   " + discountName);
            } else {
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "   " + code + ": " + discountName);
            }

            // Print discount amount
            if (transTypeID == 2 || transTypeID == 3) {
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|rA" + "($" + df.format(discountAmt) + ")" + "\n");
            } else {
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|rA" + "$-" + df.format(discountAmt) + "\n");
            }
        } catch(JposException ex) {
            log("Printer: failed to print subtotal discount; " + ex.getMessage(), Logger.LEVEL.ERROR);
            handleJposException(ex);
            throw ex;
        }

        return true;
    }

    public void printLoyaltyReward(int rewardTypeID, String name, String numPtsUsed, double rwdDollarAmt) throws JposException {
        try {
            // Print reward name
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "   " + name);

            // Print amount
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|rA" + "$-" + df.format(rwdDollarAmt) + "\n");
        } catch (JposException ex) {
            log("Printer: failed to print loyalty reward; " + ex.getMessage(), Logger.LEVEL.ERROR);
            handleJposException(ex);
            throw ex;
        }
    }

    public boolean printTax(String taxName, double taxAmt, int transTypeID, String code) throws JposException {
        try {
            // Print tax name
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "   " + code + ": " + taxName);
            // Print tax amount
            if (transTypeID == 2 || transTypeID == 3) {
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|rA" + "($" + df.format(taxAmt) + ")" + "\n");
            } else {
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|rA" + "$" + df.format(taxAmt) + "\n");
            }
        } catch(JposException ex) {
            log("Printer: failed to print tax; " + ex.getMessage(), Logger.LEVEL.ERROR);
            handleJposException(ex);
            throw ex;
        }

        return true;
    }

    public boolean printSubtotalHeader(float subtotal, int transTypeID) throws JposException {
        try {
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "Subtotal: ");
            if (transTypeID == 2 || transTypeID == 3) {
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|rA" + "($" + df.format(subtotal) + ")" + "\n");
            } else {
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|rA" + "$" + df.format(subtotal) + "\n");
            }
        } catch(JposException ex) {
            log("Printer: failed to print subtotal header; " + ex.getMessage(), Logger.LEVEL.ERROR);
            handleJposException(ex);
            throw ex;
        }

        return true;
    }

    public boolean printSubtotal(double subtotal, int transTypeID) throws JposException {
        try {
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\n" + "Merchandise Subtotal: ");
            if (transTypeID == 2 || transTypeID == 3) {
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|rA" + "($" + df.format(subtotal) + ")" + "\n");
            }
            else {
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|rA" + "$" + df.format(subtotal) + "\n");
            }
        } catch(JposException ex) {
            log("Printer: failed to print subtotal; " + ex.getMessage(), Logger.LEVEL.ERROR);
            handleJposException(ex);
            throw ex;
        }

        return true;
    }

    public boolean printSaleItem(int quantity, double unitPrice, double totalItemPrice, boolean isModifier, String parentName, String comments, String label, String parentLabel, String parentPrice, int transTypeID, double netWeight, String pluCode, int itemTypeID, String taxDsctCodes) throws JposException {
        try {
            if (parentLabel.equalsIgnoreCase("no label")) {
                // Print item name
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, label);
                if (transTypeID == 2 || transTypeID == 3) {
                    // Print total item price
                    ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|rA" + "($" + df.format(totalItemPrice) + ")" + "\n");
                    // Print quantity
                    ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "   " + ((Integer)quantity).toString());
                } else {
                    // Print total item price
                    Logger.logMessage("TOTAL ITEM PRICE: " + totalItemPrice, logFileName, Logger.LEVEL.DEBUG);
                    ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|rA" + "$" + df.format(totalItemPrice) + "\n");
                    // Print quantity
                    ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "   " + ((Integer)quantity).toString());
                }
                if (netWeight > 0) {
                    // Print unit price PER POUND
                    if (!taxDsctCodes.equals("")) {
                        ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, " at $" + df.format(unitPrice) + " /lb" + "\n");
                    } else {
                        ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, " at $" + df.format(unitPrice) + " /lb" + " " + taxDsctCodes + "\n");
                    }
                    // Print net weight
                    ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "   Weight: " + netWeight + " lbs" + "\n");
                } else {
                    // Print unit price
                    if (itemTypeID == 2 && !taxDsctCodes.equals("")) {
                        ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, " at $" + df.format(unitPrice) + " each" + " " + taxDsctCodes + "\n");
                    } else {
                        ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, " at $" + df.format(unitPrice) + " each" + "\n");
                    }
                }
                // Print plu code, if any
                if (itemTypeID == 2 && !pluCode.equals("")) {
                    ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "   Product#: " + pluCode + "\n");
                }
                // Print item comment
                if (comments.length() > 0) {
                    ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "   Cmt: " + comments.toString() + "\n");
                }
            }
            else {
                float parPrice = Float.parseFloat(parentPrice);
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, parentName);
                if (transTypeID == 2 || transTypeID == 3) {
                    ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|rA" + "($" + df.format(totalItemPrice) + ")" + "\n");
                    ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "   -" + ((Integer)quantity).toString());
                    ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, " at $" + df.format(parPrice) + " each" + "\n");
                    ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "   " + parentLabel + "\n");
                    ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "   " + label);
                    ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, " (-" + ((Integer)quantity).toString() + " at " + "$" + df.format(unitPrice) + " each)" + "\n");
                } else {
                    ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|rA" + "$" + df.format(totalItemPrice) + "\n");
                    ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "   " + ((Integer)quantity).toString());
                    ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, " at $" + df.format(parPrice) + " each" + "\n");
                    ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "   " + parentLabel + "\n");
                    ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "   " + label);
                    ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, " (" + ((Integer)quantity).toString() + " at " + "$" + df.format(unitPrice) + " each)" + "\n");
                }
                if (comments.length() > 0) {
                    ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "   Cmt: " + comments.toString() + "\n");
                }
            }
        } catch(JposException ex) {
            log("Printer: failed to print sale item " + label + "; " + ex.getMessage(), Logger.LEVEL.ERROR);
            handleJposException(ex);
            throw ex;
        }

        return true;
    }

    public void printMissingItemSlip(String pluCode, String timeDate) throws Exception {
        try {
            turnOnTransactionPrinting();
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "*****************************************" + "\n\n");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "M I S S I N G   I T E M   S L I P" + "\n");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, timeDate + "\n");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "Barcode: " + pluCode + "\n\n");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "*****************************************" + "\n\n");
            finishPrint();
            turnOffTransactionPrinting();
        } catch (JposException ex) {
            log("Printer: failed to print missing item slip for barcode " + pluCode, Logger.LEVEL.ERROR);
            handleJposException(ex);
            throw ex;
        }
    }

    public boolean printTotalHeader(double purchaseTotal, int transTypeID) throws JposException {
        try {
            if (transTypeID == 2 || transTypeID == 3) {
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\n \n" + "\u001b|rA" + "TOTAL:   ($" + df.format(purchaseTotal) + ")" + "\n");
            } else {
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\n \n" + "\u001b|rA" + "TOTAL:   $" + df.format(purchaseTotal) + "\n");
            }
        } catch(JposException ex) {
            log("Printer: failed to print total header; " + ex.getMessage(), Logger.LEVEL.ERROR);
            handleJposException(ex);
            throw ex;
        }

        return true;
    }

    public boolean printTrainingModeNote() throws JposException {
        ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|cA" + "====== TRAINING MODE ======" + "\n \n");
        return true;
    }

    public boolean printTenderItem(double tenderAmount, String tenderName, int transactionTypeID) throws JposException {
        try {
            if (transactionTypeID == 2) {
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|rA" + "\n" + tenderName + " voided:   ($" + df.format(tenderAmount) + ")");
            } else if (transactionTypeID == 3) {
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|rA" + "\n" + tenderName + " due:   ($" + df.format(tenderAmount) + ")");
            } else {
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|rA" + "\n" + tenderName + " tendered:   $" + df.format(tenderAmount));
            }
        } catch(JposException ex) {
            log("Printer: failed to print tender item; " + ex.getMessage(), Logger.LEVEL.ERROR);
            handleJposException(ex);
            throw ex;
        }
        return true;
    }

    public boolean printTenderComment(String comment) throws JposException {
        try {
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|rA" + "\n" + comment);
        } catch (JposException ex) {
            log("Printer: failed to print tender comment; " + ex.getMessage(), Logger.LEVEL.ERROR);
            handleJposException(ex);
            throw ex;
        }

        return true;
    }

    public boolean printCCInfo(String acctNo, String authCode) throws JposException {
        try {
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|rA" + acctNo);
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|rA" + "\n" + "Auth #: " + authCode);
        } catch (JposException ex) {
            log("Printer: failed to print cc info; " + ex.getMessage(), Logger.LEVEL.ERROR);
            handleJposException(ex);
            throw ex;
        }

        return true;
    }

    public boolean printTotalDetails(double changeDue) throws JposException {
        try {
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|rA" + "\n" + "Change due:   $" + df.format(changeDue) + "\n");
        } catch (JposException ex) {
            log("Printer: failed to print total details; " + ex.getMessage(), Logger.LEVEL.ERROR);
            handleJposException(ex);
            throw ex;
        }

        return true;
    }

    public boolean printTotalTax(float totalTax) throws JposException {
        try {
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "Sales tax: ");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|rA" + "$" + df.format(totalTax) + "\n");
        } catch (JposException ex) {
            log("Printer: failed to print total tax; " + ex.getMessage(), Logger.LEVEL.ERROR);
            handleJposException(ex);
            throw ex;
        }

        return true;
    }

    public boolean finishPrint() throws JposException {
        try {
            printNewLine();
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|5lF");
            if(bSupportsCutPaperCommand)
                ptr.cutPaper(100);
        } catch(JposException ex) {
            log("Printer: failed to cut paper; " + ex.getMessage(), Logger.LEVEL.ERROR);
            handleJposException(ex);
            throw ex;
        }

        return true;
    }

    public boolean printAcctInfo(String[] acctInfo) throws JposException {
        for (String anAcctInfo : acctInfo) {
            if (anAcctInfo.contains("[NAME]:")) {
                printEmployeeName(anAcctInfo.substring(7, anAcctInfo.length()));
            } else if (anAcctInfo.contains("[ACCTNUM]:")) {
                printEmployeeNumber(anAcctInfo.substring(10, anAcctInfo.length()));
            } else if (anAcctInfo.contains("[GLOBALACCTBAL]:")) {
                printGlobalAcctBal(anAcctInfo.substring(16, anAcctInfo.length()));
            } else if (anAcctInfo.contains("[STOREACCTBAL]:")) {
                printStoreAcctBal(anAcctInfo.substring(15, anAcctInfo.length()));
            } else if (anAcctInfo.contains("[BADGE]:")) {
                printBadgeNum(anAcctInfo.substring(8, anAcctInfo.length()));
            } else if (anAcctInfo.contains("[TERMINALPROFILEBALANCETYPE]") == true) {
                printBalanceTypeInfo(anAcctInfo.substring(28, anAcctInfo.length()));
            } else {
                try {
                    ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|rA" + "\n" + anAcctInfo.toString());
                } catch (JposException ex) {
                    log("Printer: failed to print acct info; " + ex.getMessage(), Logger.LEVEL.ERROR);
                    handleJposException(ex);
                    throw ex;
                }
            }
        }

        return true;
    }

    public boolean printEmployeeName(String name) throws JposException {
        try {
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|rA" + "\n" + name);
        } catch(JposException ex) {
            log("Printer: failed to print employee name " + name + "; " + ex.getMessage(), Logger.LEVEL.ERROR);
            handleJposException(ex);
            throw ex;
        }

        return true;
    }

    public boolean printEmployeeNumber(String number) throws JposException {
        try {
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|rA" + "\n" + number);
        } catch(JposException ex) {
            log("Printer: failed to print employee number; " + ex.getMessage(), Logger.LEVEL.ERROR);
            handleJposException(ex);
            throw ex;
        }

        return true;
    }

    public boolean printGlobalAcctBal(String globalBal) throws JposException {
        try {
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|rA" + "\n" + globalBal);
        } catch(JposException ex) {
            log("Printer: failed to print global acct bal; " + ex.getMessage(), Logger.LEVEL.ERROR);
            handleJposException(ex);
            throw ex;
        }

        return true;
    }

    public boolean printStoreAcctBal(String storeBal) throws JposException {
        try {
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|rA" + "\n" + storeBal);
        } catch(JposException ex) {
            log("Printer: failed to print store acct bal; " + ex.getMessage(), Logger.LEVEL.ERROR);
            handleJposException(ex);
            throw ex;
        }

        return true;
    }

    public boolean printBadgeNum(String badgeNum) throws JposException {
        try {
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|rA" + "\n" + badgeNum);
        } catch(JposException ex) {
            log("Printer: failed to print badge num; " + ex.getMessage(), Logger.LEVEL.ERROR);
            handleJposException(ex);
            throw ex;
        }

        return true;
    }

    public boolean printBalanceTypeInfo(String balanceTypeInfo)
    {
        boolean result = true;
        try {
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|rA" + "\n" + balanceTypeInfo);
        } catch(JposException ex) {
            result = false;
            Logger.logMessage("Epson Printer: failed to print receipt balance type info; " + ex.getMessage(), logFileName);
            handleJposException(ex);
        }

        return result;
    }

    public boolean noSalePrint() throws JposException {
        try {
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "*************************" + "\n");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "NO SALE" + "\n");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "*************************" + "\n \n \n \n");
        } catch(JposException ex) {
            log("Printer: failed to print no sale slip; " + ex.getMessage(), Logger.LEVEL.ERROR);
            handleJposException(ex);
            throw ex;
        }

        return true;
    }

    public boolean noSaleAdvPrint(String reason, String date, String cashierName) throws JposException {
        try {
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "*****************************************" + "\n");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "NO SALE" + "\n");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, date + "\n \n");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "REASON CODE:   " + reason + "\n");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "CASHIER:   " + cashierName + "\n");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "*****************************************" + "\n \n \n \n");
            finishPrint();
        } catch(JposException ex) {
            log("Printer: failed to print no sale slip; " + ex.getMessage(), Logger.LEVEL.ERROR);
            handleJposException(ex);
            throw ex;
        }

        return true;
    }

    public void printUpdateInstructions() throws JposException {
        try {
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "*****************************************" + "\n");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "QC POS Update Instructions" + "\n");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "*****************************************" + "\n");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "1. Click on the download link on the update screen to download and save the update" + "\n");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "2. Use the Exit Browser button to close the POS application" + "\n");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "3. Double click on the QCPAUpdate.exe file to install the update" + "\n");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "4. Re-open the POS application and log back in" + "\n");
        } catch (JposException ex) {
            log("Printer: failed to print update instructions", Logger.LEVEL.ERROR);
            handleJposException(ex);
            throw ex;
        }
    }

    public boolean printBlindCloseReceipt(String cashierName, String date) throws JposException {
        try {
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "*****************************************" + "\n\n");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "B L I N D    C L O S E" + "\n");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, date + "\n \n");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, cashierName + "\n");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "*****************************************" + "\n \n \n \n");

            finishPrint();
        } catch(JposException ex) {
            log("Printer: failed to print blind close receipt; " + ex.getMessage(), Logger.LEVEL.ERROR);
            handleJposException(ex);
            throw ex;
        }

        return true;
    }

    public boolean printCashierReportHeader(String cashierName, String date, String terminalID, String newTransId, String prevZId, String prevZDate, String startTransId, String startTransDate, String drawerNum, boolean isXout) throws JposException
    {
        try
        {
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "******************************************" + "\n\n");
            if (isXout)
            {
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "C A S H I E R   X   R E P O R T" + "\n");
            }
            else
            {
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "C A S H I E R   E N D  S H I F T" + "\n");
            }

            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, date + "\n");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, cashierName + "\n");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "Drawer #" + drawerNum + "\n");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "Terminal: " + terminalID + "\n");
            if (isXout == false)
            {
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "#" + newTransId + "\n \n");
            }
            else
            {
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\n");
            }

            if (prevZId.equals("") == false)
            {
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "Previous Z: " + "\n");
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "   #" + prevZId + ", " + prevZDate + "\n");
            }
            else
            {
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "Previous Z: " + "\n");
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "   N/A" + "\n");
            }
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "Start: " + "\n");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "   #" + startTransId + ", " + startTransDate + "\n \n");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "******************************************" + "\n");
        }
        catch(JposException ex)
        {
            log("Printer: failed to print cashier report header; " + ex.getMessage(), Logger.LEVEL.ERROR);
            handleJposException(ex);
            throw ex;
        }

        return true;
    }

    public boolean printSummaryZHeader(String cashierNames, String date, String terminalName, Boolean isXClose) throws JposException {
        try
        {
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "******************************************" + "\n\n");
            if(isXClose){
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "X   R E P O R T   S U M M A R Y" + "\n");
            }else{
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "E N D   S H I F T   S U M M A R Y" + "\n");
            }

            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, date + "\n");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, cashierNames + "\n");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "Terminal: " + terminalName + "\n \n");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "******************************************" + "\n");
        } catch(JposException ex) {
            log("Printer: failed to print summary Z header; " + ex.getMessage(), Logger.LEVEL.ERROR);
            handleJposException(ex);
            throw ex;
        }

        return true;
    }

    public boolean printCashierReportItem(String label, String amount) throws JposException {
        try {
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, label);
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|rA" + amount + "\n");
        } catch(JposException ex) {
            log("Printer: failed to print cashier report item; " + ex.getMessage(), Logger.LEVEL.ERROR);
            handleJposException(ex);
            throw ex;
        }

        return true;
    }

    public boolean printCashierReportSubItem(String label, String amount) throws JposException {
        try {
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "   " + label);
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|rA" + amount + "\n");
        } catch(JposException ex) {
            log("Printer: failed to print cashier report sub item; " + ex.getMessage(), Logger.LEVEL.ERROR);
            handleJposException(ex);
            throw ex;
        }

        return true;
    }

    public boolean printCashierReportSubItemIndented(String label, String amount) throws JposException {
        try {
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "   " + label);
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|rA" + amount + "     " + "\n");
        } catch(JposException ex) {
            log("Printer: failed to print cashier report sub item indented; " + ex.getMessage(), Logger.LEVEL.ERROR);
            handleJposException(ex);
            throw ex;
        }

        return true;
    }

    public boolean printCashierReportItemHeading(String itemHeading) throws JposException {
        try {
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "** " + itemHeading + " **\n");
        } catch(JposException ex) {
            log("Printer: failed to print cashier report item heading; " + ex.getMessage(), Logger.LEVEL.ERROR);
            handleJposException(ex);
            throw ex;
        }

        return true;
    }

    public boolean printLeftAlignText(String text) throws JposException
    {
        try
        {
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, text);
        }
        catch(JposException ex)
        {
            log("JposReceiptPrinter.printLeftAlignText error; " + ex.getMessage(), Logger.LEVEL.ERROR);
            handleJposException(ex);
            throw ex;
        }

        return true;
    }
    public boolean printBatchSummary(ArrayList batchSummary) throws JposException {
        //boolean ccTypeTotalsHeaderPrinted = false;
        int indexOfCCTypeTotals = 0;

        try {
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "*****************************************" + "\n\n");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "B A T C H   S U M M A R Y" + "\n");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, batchSummary.get(11) + "  " + batchSummary.get(12) + "\n");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "Terminal: " + batchSummary.get(13) + "\n");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "Cashier: " + batchSummary.get(14) + "\n");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "Batch #: " + batchSummary.get(0) + "\n \n");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "*****************************************" + "\n");

            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "BATCH COUNT");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|rA" + batchSummary.get(1) + "\n");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "NET BATCH TOTAL");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|rA" + batchSummary.get(2) + "\n \n");

            if (!batchSummary.get(3).equals("-1")) {
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "CREDIT PURCHASE QTY");
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|rA" + batchSummary.get(3) + "\n");
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "CREDIT PURCHASE AMT");
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|rA" + batchSummary.get(4) + "\n \n");

                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "CREDIT RETURN QTY");
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|rA" + batchSummary.get(5) + "\n");
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "CREDIT RETURN AMT");
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|rA" + batchSummary.get(6) + "\n \n");

                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "DEBIT PURCHASE QTY");
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|rA" + batchSummary.get(7) + "\n");
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "DEBIT PURCHASE AMT");
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|rA" + batchSummary.get(8) + "\n \n");

                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "DEBIT RETURN QTY");
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|rA" + batchSummary.get(9) + "\n");
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "DEBIT RETURN AMT");
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|rA" + batchSummary.get(10) + "\n \n");
            }

            indexOfCCTypeTotals = batchSummary.size() - 4;

            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "TOTALS BY CARD TYPE" + "\n");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "     Mastercard: " + batchSummary.get(indexOfCCTypeTotals).toString() + "\n");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "     Visa: " + batchSummary.get(indexOfCCTypeTotals + 1).toString() + "\n");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "     Discover: " + batchSummary.get(indexOfCCTypeTotals + 2).toString() + "\n");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "     Amex: " + batchSummary.get(indexOfCCTypeTotals + 3).toString() + "\n");

            finishPrint();
        } catch(JposException ex) {
            log("Printer: failed to print batch summary; " + ex.getMessage(), Logger.LEVEL.ERROR);
            handleJposException(ex);
            throw ex;
        }

        return true;
    }

    public void printAcctBalances(String availBal, String name, String date, ArrayList loyaltyProgramPointBalances) throws JposException
    {
        try
        {
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "*****************************************" + "\n\n");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "A C C O U N T    S U M M A R Y" + "\n");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, date + "\n");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "Name: " + name + "\n\n");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "*****************************************" + "\n\n");

            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, availBal + "\n");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\n");

            if (loyaltyProgramPointBalances.size() > 0)
            {
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "LOYALTY PROGRAM POINT BALANCES" + "\n");

                for (int x = 0; x < loyaltyProgramPointBalances.size(); x++)
                {
                    ArrayList programPtBal = (ArrayList) loyaltyProgramPointBalances.get(x);

                    String programName = programPtBal.get(0).toString();
                    String programPointBal = programPtBal.get(1).toString();

                    String finalPrintLine = programName + ": " + programPointBal + " points";

                    // Length check - if finalPrintLine is over a certain length, then we need to truncate the program name
                    if (finalPrintLine.length() > 42)
                    {
                        String programPointBalStr = programPointBal + " points" + "..: ";

                        // Value of remainingChars is how long the program name can be
                        int remainingChars = 42 - programPointBalStr.length();

                        if (programName.length() >= remainingChars - 1)
                        {
                            finalPrintLine = programName.substring(0, remainingChars - 1) + "..: ";
                            finalPrintLine += programPointBal + " points" + "\n";
                        }
                    }
                    else
                    {
                        finalPrintLine += "\n";
                    }

                    ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, finalPrintLine);
                }
                
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\n");
            }
            
            finishPrint();
        }
        catch (JposException ex)
        {
            log("Printer: failed to print account balances", Logger.LEVEL.ERROR);
            handleJposException(ex);
            throw ex;
        }
    }
    public void printIntegratedGiftCardBalance(String balance, String date, String headerText, String lastFourCardNum) throws JposException
    {
        try
        {
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "*****************************************" + "\n");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, headerText + "\n");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, date + "\n");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "*****************************************" + "\n\n");

            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "Balance for Card " + lastFourCardNum + ":  $" + balance + "\n");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\n");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\n");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\n");

            finishPrint();
        }
        catch (JposException ex)
        {
            log("Printer: failed to print integrated gift card balance", Logger.LEVEL.ERROR);
            handleJposException(ex);
            throw ex;
        }
    }

    public void NCRPrintNutritionalInfo(ArrayList items, ArrayList categoryNames, ArrayList nutritionTotals, boolean needsNAFootnote, boolean needsTruncateDigitsFootnote) throws IOException, JposException {
        Logger.logMessage("NCR Printer: creating nutritional info image", logFileName);
        int width = 512;
        int height = items.size() * 35 + 200;
        int rowHeight = 35;
        ArrayList item;

        if (needsNAFootnote) {
            height = height + 50;
        }
        if (needsTruncateDigitsFootnote) {
            height = height + 30;
        }

        // Truncate image if necessary; NCR printer only allows up to 12 items
        int numItemsAllowed = items.size();
        int numItemRows = items.size();
        boolean imageNeedsTruncating = false;

        if (items.size() + nutritionTotals.size() - 1 > 15) {
            numItemsAllowed = 14 - nutritionTotals.size();
            numItemRows = numItemsAllowed + 1;
            imageNeedsTruncating = true;
            height = numItemRows * 35 + 200;
        }

        // Initializes a blank picture with a white background
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2d = image.createGraphics();
        Font textFont = new Font("Arial", Font.PLAIN, 18);
        Font textFontBold = new Font("Arial", Font.BOLD, 18);
        g2d.setFont(textFontBold);

        // Set background to be white
        g2d.setColor(Color.white);
        g2d.fillRect(0, 0, width, height);

        // Set font/grid color
        g2d.setColor(Color.black);
        g2d.setStroke(new BasicStroke(2));

        // Print "Nutrition Facts" header
        Font headerFont = new Font("Arial", Font.BOLD, 22);
        g2d.setFont(headerFont);
        g2d.drawString("Nutrition Facts", 0, 20);

        // Print column names
        g2d.setFont(textFontBold);
        g2d.drawString("Qty", 0, 55);
        g2d.drawString("Item Name", 50, 55);
        for (int x = 0; x < categoryNames.size(); x++) {
            item = (ArrayList) categoryNames.get(x);
            g2d.drawString(item.get(0).toString(), (x * 60) + 217, 55);
            g2d.drawString(item.get(1).toString(), (x * 60) + 217, 75);
        }

        // Draw item grid rows
        int rowNum = 0;
        for (int x = 0; x < numItemRows; x++) {
            drawRow(0, 35 * rowNum + 85, g2d);
            rowNum++;
        }

        // Draw grid rows for totals
        int numOfDVRows = nutritionTotals.size() + rowNum;
        drawRowsForTotals(rowNum, numOfDVRows, g2d);
        g2d.drawString("Totals", 150, 35 * (rowNum + 1) + 80);

        // Print nutritional total labels
        rowNum = rowNum + 2;
        for (int x = 1; x < nutritionTotals.size(); x++) {
            item = (ArrayList) nutritionTotals.get(x);
            //g2d.drawString("% Daily Value (" + item.get(0).toString() + " cal)", 0, 35 * rowNum + 80);
            g2d.drawString(item.get(0).toString(), 0, 35 * rowNum + 80);
            rowNum++;
        }

        // Print item nutritional info
        rowNum = 0;
        g2d.setFont(textFont);
        for (int x = 0; x < numItemsAllowed; x++) {
            item = (ArrayList) items.get(x);
            g2d.drawString(item.get(0).toString(), 5, (rowNum * 35) + 114);      // Quantity
            g2d.drawString(item.get(1).toString(), 50, (rowNum * 35) + 114);     // Item name
            g2d.drawString(item.get(2).toString(), 217, (rowNum * 35) + 114);    // #1
            g2d.drawString(item.get(3).toString(), 277, (rowNum * 35) + 114);    // #2
            g2d.drawString(item.get(4).toString(), 337, (rowNum * 35) + 114);    // #3
            g2d.drawString(item.get(5).toString(), 403, (rowNum * 35) + 114);    // #4
            g2d.drawString(item.get(6).toString(), 457, (rowNum * 35) + 114);    // #5

            rowNum++;
        }

        if (imageNeedsTruncating) {
            g2d.drawString("...", 5, (rowNum * 35) + 103);      // Quantity
            g2d.drawString("...", 50, (rowNum * 35) + 103);     // Item name
            g2d.drawString("...", 217, (rowNum * 35) + 103);    // #1
            g2d.drawString("...", 277, (rowNum * 35) + 103);    // #2
            g2d.drawString("...", 337, (rowNum * 35) + 103);    // #3
            g2d.drawString("...", 403, (rowNum * 35) + 103);    // #4
            g2d.drawString("...", 457, (rowNum * 35) + 103);    // #5
        }

        // Print nutritional totals
        if (imageNeedsTruncating) {
            rowNum = numItemsAllowed + 1;
        } else {
            rowNum = numItemsAllowed;
        }
        item = (ArrayList) nutritionTotals.get(0);
        g2d.drawString(item.get(0).toString(), 217, (rowNum * 35) + 112);    // #1
        g2d.drawString(item.get(1).toString(), 277, (rowNum * 35) + 112);    // #2
        g2d.drawString(item.get(2).toString(), 337, (rowNum * 35) + 112);    // #3
        g2d.drawString(item.get(3).toString(), 403, (rowNum * 35) + 112);    // #4
        g2d.drawString(item.get(4).toString(), 457, (rowNum * 35) + 112);    // #5
        rowNum++;
        for (int x = 1; x < nutritionTotals.size(); x++) {
            item = (ArrayList) nutritionTotals.get(x);
            g2d.drawString(item.get(1).toString(), 217, (rowNum * 35) + 112);    // #1
            g2d.drawString(item.get(2).toString(), 277, (rowNum * 35) + 112);    // #2
            g2d.drawString(item.get(3).toString(), 337, (rowNum * 35) + 112);    // #3
            g2d.drawString(item.get(4).toString(), 403, (rowNum * 35) + 112);    // #4
            g2d.drawString(item.get(5).toString(), 457, (rowNum * 35) + 112);    // #5
            rowNum++;
        }

        // Print no data available footnote if necessary
        if (needsNAFootnote) {
            g2d.drawString("*** - signifies that no data is available", 0, rowNum * 35 + 120);
            rowNum++;
        }

        // Print number truncate footnote if necessary
        if (needsTruncateDigitsFootnote) {
            g2d.drawString("### - signifies that the value exceeded the allotted space", 0, rowNum * 35 + 120);
        }

        // Save picture
        String sFileName = "C:/MMHayes/NutritionReceipts/Receipt.bmp";
        File file = new File(sFileName);
        if(!file.createNewFile()) {
            log(" epsonPrintNutritionalInfo createNewFile() failed to create file '" + sFileName + "'", Logger.LEVEL.ERROR);
        }
        ImageIO.write(image, "BMP", file);

        try {
            // Print neatly by setting map mode to ptr_mm_metric
            ptr.setMapMode(POSPrinterConst.PTR_MM_METRIC);

            // Output by the high quality mode
            ptr.setRecLetterQuality(true);

            // Print nutritional info image
            turnOnTransactionPrinting();
            printNewLine();
            ptr.printBitmap(POSPrinterConst.PTR_S_RECEIPT, "C:/MMHayes/NutritionReceipts/Receipt.bmp", POSPrinterConst.PTR_BM_ASIS, POSPrinterConst.PTR_BM_CENTER);
            turnOffTransactionPrinting();

        } catch (JposException ex) {
            log("ReceiptPrinter: print nutritional image error", Logger.LEVEL.ERROR);
            handleJposException(ex);
            throw ex;
        }
        g2d.dispose();
    }

    public void epsonPrintNutritionalInfo(ArrayList items, ArrayList categoryNames, ArrayList nutritionTotals, boolean needsNAFootnote, boolean needsTruncateDigitsFootnote) throws IOException, JposException
    {
        log("Printer: creating nutritional info image", Logger.LEVEL.DEBUG);

        // Generic printer does not print bitmap
        if(printerName.equalsIgnoreCase(PeripheralsDataManager.OTHER_PRINTER_NAME)) {

            // width, align, format 7 columns
            // Qty(3) Desc(15), 5 more columns(6)
            String s =  "Qty Item Name ";
            String s2 = "              ";
            for (int x = 0; x < categoryNames.size(); x++)
            {
                ArrayList item = (ArrayList)categoryNames.get(x);
                s  += item.get(0).toString();
                s2 += item.get(1).toString();
            }

            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, s + "\n");
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, s2 + "\n");

            s = "";
            for (Object item1 : items) {
                ArrayList item = (ArrayList) item1;
                s = String.format("%3s %15s %6s %6s %6s %6s %6s",
                        item.get(0).toString(),  // Quantity
                        item.get(1).toString(),   // Item name
                        item.get(2).toString(),   // #1
                        item.get(3).toString(),   // #2
                        item.get(4).toString(),   // #3
                        item.get(5).toString(),   // #4
                        item.get(6).toString()    // #5
                );
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, s + "\n");
            }


            return;
        }

        int width = 512;
        int height = items.size() * 35 + 200;
        int rowHeight = 35;
        ArrayList item;

        if (needsNAFootnote)
        {
            height = height + 50;
        }
        if (needsTruncateDigitsFootnote)
        {
            height = height + 30;
        }

        // Initializes a blank picture with a white background
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2d = image.createGraphics();
        Font textFont = new Font("Arial", Font.PLAIN, 18);
        Font textFontBold = new Font("Arial", Font.BOLD, 18);
        g2d.setFont(textFontBold);

        // Set background to be white
        g2d.setColor(Color.white);
        g2d.fillRect(0, 0, width, height);

        // Set font/grid color
        g2d.setColor(Color.black);
        g2d.setStroke(new BasicStroke(2));

        // Print "Nutrition Facts" header
        Font headerFont = new Font("Arial", Font.BOLD, 22);
        g2d.setFont(headerFont);
        g2d.drawString("Nutrition Facts", 0, 20);

        // Print column names
        g2d.setFont(textFontBold);
        g2d.drawString("Qty", 0, 55);
        g2d.drawString("Item Name", 50, 55);
        for (int x = 0; x < categoryNames.size(); x++)
        {
            item = (ArrayList)categoryNames.get(x);
            g2d.drawString(item.get(0).toString(), (x * 60) + 217, 55);
            g2d.drawString(item.get(1).toString(), (x * 60) + 217, 75);
        }

        // Draw grid row
        int rowNum = 0;
        for (int x = 0; x < items.size(); x++)
        {
            //drawRow(0, 35 * rowNum + 20, g2d);
            drawRow(0, 35 * rowNum + 85, g2d);
            rowNum++;
        }

        // Draw grid rows for totals
        int numOfDVRows = nutritionTotals.size() + rowNum;
        drawRowsForTotals(rowNum, numOfDVRows, g2d);
        g2d.drawString("Totals", 150, 35 * (rowNum + 1) + 80);

        // Print nutritional total labels
        rowNum = rowNum + 2;
        for (int x = 1; x < nutritionTotals.size(); x++) {
            item = (ArrayList)nutritionTotals.get(x);
            //g2d.drawString("% Daily Value (" + item.get(0).toString() + " cal)", 0, 35 * rowNum + 80);
            g2d.drawString(item.get(0).toString(), 0, 35 * rowNum + 80);
            rowNum++;
        }

        // Print item nutritional info
        rowNum = 0;
        g2d.setFont(textFont);
        for (Object item1 : items) {
            item = (ArrayList) item1;
            g2d.drawString(item.get(0).toString(), 5, (rowNum * 35) + 114);      // Quantity
            g2d.drawString(item.get(1).toString(), 50, (rowNum * 35) + 114);     // Item name
            g2d.drawString(item.get(2).toString(), 217, (rowNum * 35) + 114);    // #1
            g2d.drawString(item.get(3).toString(), 277, (rowNum * 35) + 114);    // #2
            g2d.drawString(item.get(4).toString(), 337, (rowNum * 35) + 114);    // #3
            g2d.drawString(item.get(5).toString(), 403, (rowNum * 35) + 114);    // #4
            g2d.drawString(item.get(6).toString(), 457, (rowNum * 35) + 114);    // #5

            rowNum++;
        }

        // Print nutritional totals
        rowNum = items.size();
        item = (ArrayList)nutritionTotals.get(0);
        g2d.drawString(item.get(0).toString(), 217, (rowNum * 35) + 112);    // #1
        g2d.drawString(item.get(1).toString(), 277, (rowNum * 35) + 112);    // #2
        g2d.drawString(item.get(2).toString(), 337, (rowNum * 35) + 112);    // #3
        g2d.drawString(item.get(3).toString(), 403, (rowNum * 35) + 112);    // #4
        g2d.drawString(item.get(4).toString(), 457, (rowNum * 35) + 112);    // #5
        rowNum++;
        for (int x = 1; x < nutritionTotals.size(); x++)
        {
            item = (ArrayList)nutritionTotals.get(x);
            g2d.drawString(item.get(1).toString(), 217, (rowNum * 35) + 112);    // #1
            g2d.drawString(item.get(2).toString(), 277, (rowNum * 35) + 112);    // #2
            g2d.drawString(item.get(3).toString(), 337, (rowNum * 35) + 112);    // #3
            g2d.drawString(item.get(4).toString(), 403, (rowNum * 35) + 112);    // #4
            g2d.drawString(item.get(5).toString(), 457, (rowNum * 35) + 112);    // #5
            rowNum++;
        }

        // Print no data available footnote if necessary
        if (needsNAFootnote)
        {
            g2d.drawString("*** - signifies that no data is available", 0, rowNum * 35 + 120);
            rowNum++;
        }

        // Print number truncate footnote if necessary
        if (needsTruncateDigitsFootnote)
        {
            g2d.drawString("### - signifies that the value exceeded the allotted space", 0, rowNum * 35 + 120);
        }

        // Save picture
        String sFileName = "C:/MMHayes/NutritionReceipts/Receipt.jpg";
        File file = new File(sFileName);
        if(!file.createNewFile()) {
            log(" epsonPrintNutritionalInfo createNewFile() failed to create file '" + sFileName + "'", Logger.LEVEL.TRACE);
        }
        ImageIO.write(image, "jpg", file);

        try {
            // Print neatly by setting map mode to ptr_mm_metric
            ptr.setMapMode(POSPrinterConst.PTR_MM_METRIC);

            // Output by the high quality mode
            ptr.setRecLetterQuality(true);

            // Multitone printing
            int []pram2 = new int[1];
            pram2[0] = EpsonPOSPrinterConst.PTR_DI_BITMAP_PRINTING_MULTI_TONE;
            ptr.directIO(EpsonPOSPrinterConst.PTR_DI_SET_BITMAP_PRINTING_TYPE, pram2, "");
            ptr.printBitmap(POSPrinterConst.PTR_S_RECEIPT, "C:/MMHayes/NutritionReceipts/Receipt.jpg", ptr.getRecLineWidth(), POSPrinterConst.PTR_BM_CENTER);
        } catch (JposException ex) {
            log("ReceiptPrinter: print nutritional image error", Logger.LEVEL.ERROR);
            handleJposException(ex);
            throw ex;
        }

    }

    public void drawRow(int x, int y, Graphics2D g2d)
    {
        int rowHeight = 35;

        // Draw qty box
        g2d.drawRect(x, y, 35, rowHeight);

        // Draw item name box
        //g2d.drawRect(x + 35, y, 221, rowHeight);
        g2d.drawRect(x + 35, y, 177, rowHeight);

        // Draw category #1 box
        //g2d.drawRect(x + 256, y, 51, rowHeight);
        g2d.drawRect(x + 212, y, 60, rowHeight);

        // Draw category #2 box
        //g2d.drawRect(x + 307, y, 51, rowHeight);
        g2d.drawRect(x + 272, y, 60, rowHeight);

        // Draw category #3 box
        //g2d.drawRect(x + 358, y, 51, rowHeight);
        g2d.drawRect(x + 332, y, 60, rowHeight);

        // Draw category #4 box
        //g2d.drawRect(x + 409, y, 51, rowHeight);
        g2d.drawRect(x + 392, y, 60, rowHeight);

        // Draw category #5 box
        //g2d.drawRect(x + 460, y, 51, rowHeight);
        g2d.drawRect(x + 452, y, 60, rowHeight);
    }

    public void drawRowsForTotals(int rowNum, int numOfDVRows, Graphics g2d)
    {
        int rowHeight = 35;

        for (int x = rowNum; x < numOfDVRows; x++) {
            g2d.drawRect(212, 35 * rowNum + 85, 60, rowHeight);
            g2d.drawRect(272, 35 * rowNum + 85, 60, rowHeight);
            g2d.drawRect(332, 35 * rowNum + 85, 60, rowHeight);
            g2d.drawRect(392, 35 * rowNum + 85, 60, rowHeight);
            g2d.drawRect(452, 35 * rowNum + 85, 60, rowHeight);
            rowNum++;
        }
    }
    public void turnOnTransactionPrinting() throws JposException {
        try {
            ptr.transactionPrint(POSPrinterConst.PTR_S_RECEIPT, POSPrinterConst.PTR_TP_TRANSACTION);
        } catch (JposException ex) {
            log("Printer: unable to turn on transaction printing", Logger.LEVEL.ERROR);
            handleJposException(ex);
            throw ex;
        }
    }

    public void turnOffTransactionPrinting() throws JposException {
        try {
            ptr.transactionPrint(POSPrinterConst.PTR_S_RECEIPT, POSPrinterConst.PTR_TP_NORMAL);
        } catch (JposException ex) {
            log("Printer: unable to turn off transaction printing", Logger.LEVEL.ERROR);
            handleJposException(ex);
            throw ex;
        }
    }

    public void printLoyaltySummary(ArrayList programs) throws JposException {
        try {
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|cA" + "-----     LOYALTY SUMMARY    -----" + "\n");

            for (Object program1 : programs) {
                //ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, programs.get(x));
                ArrayList program = (ArrayList) program1;
                String programName = program.get(0).toString();
                String earnedPts = program.get(1).toString();
                String ptsTotal = program.get(2).toString();
                String redeemedRwds = program.get(3).toString();

                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, programName + "\n");
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "Points Earned: " + earnedPts + "\n");
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "Points Balance: " + ptsTotal + "\n");

                if (!redeemedRwds.equals("")) {
                    ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "Rewards Redeemed: " + redeemedRwds + "\n\n");
                }
            }
        } catch (JposException ex) {
            log("Printer: printLoyaltySummary exception!", Logger.LEVEL.ERROR);
            handleJposException(ex);
            throw ex;
        }
    }

    public boolean isInErrorState()
    {
        boolean isInErrorState = false;

        try {
            int state = ptr.getState();
            String stateMsg = getPrinterStateMsg(state);

            if (stateMsg.toLowerCase().contains("error")) {
                isInErrorState = true;
                log("Printer: state=" + state + ", stateMsg=" + stateMsg, Logger.LEVEL.ERROR);
            }
        } catch (Exception e) {
            log("Printer: getPrinterState exception - " + e.getMessage(), Logger.LEVEL.ERROR);
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            log("Printer: " + sw.toString(), Logger.LEVEL.ERROR);
        }

        return isInErrorState;
    }

    public void handleJposException(JposException ex)
    {
        try {
            // Log what we can about the error and state of the printer
            int errorCode = ex.getErrorCode();
            String errorCodeMsg = getErrorCodeMsg(errorCode);
            int extErrorCode = ex.getErrorCodeExtended();
            String extErrorCodeMsg = getErrorCodeExt(extErrorCode);
            int printerState = ptr.getState();
            String ptrStateMsg = getPrinterStateMsg(printerState);
            log("Printer: " + ex.getMessage() + ", error code=" + ex.getErrorCode() + ", ", Logger.LEVEL.ERROR);
            log("Printer: error code msg = " + errorCodeMsg, Logger.LEVEL.ERROR);
            log("Printer: ext error code=" + extErrorCode, Logger.LEVEL.ERROR);
            log("Printer: ext error code msg=" + extErrorCodeMsg, Logger.LEVEL.ERROR);
            log("Printer: device state=" + printerState + ", " + ptrStateMsg, Logger.LEVEL.ERROR);

            // If freezeEvents is turned on, turn it off
            if (ptr.getFreezeEvents()) {
                ptr.setFreezeEvents(false);
                log("Printer: freeze events turned off", Logger.LEVEL.ERROR);
            }

            // Clear printer's output to cancel transaction mode and avoid queue build up
            ptr.clearOutput();
        } catch (Exception e) {
            log("Printer: handleJposException exception! " + ex.getMessage(), Logger.LEVEL.ERROR);
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ex.printStackTrace(pw);
            log("Printer: " + sw.toString(), Logger.LEVEL.ERROR);
        }
    }

    public String getPrinterStateMsg(int state)
    {
        String ptrState = "";

        try {
            switch (state) {
                case JposConst.JPOS_S_BUSY:
                    ptrState = "Device is busy.";
                    break;
                case JposConst.JPOS_S_IDLE:
                    ptrState = "Device is idle.";
                    break;
                case JposConst.JPOS_S_CLOSED:
                    ptrState = "Device is closed.";
                    break;
                case JposConst.JPOS_S_ERROR:
                    ptrState = "Device is in error state.";
                    break;
                default:
                    ptrState = "Unknown device state.";
                    break;
            }
        } catch (Exception e) {
            log("Printer: getPrinterState exception!" + e.getMessage(), Logger.LEVEL.ERROR);
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            log("Printer: " + sw.toString(), Logger.LEVEL.ERROR);
        }

        return ptrState;
    }

    public String getErrorCodeMsg(int errorCode)
    {
        String errorMsg = "";

        try {
            switch (errorCode) {
                case JposConst.JPOS_E_CLOSED:
                    errorMsg = "An attempt was made to access a closed device.";
                    break;
                case JposConst.JPOS_E_CLAIMED:
                    errorMsg = "An attempt was made to access a Device that is claimed by another Device Control instance.";
                    break;
                case JposConst.JPOS_E_NOTCLAIMED:
                    errorMsg = "Device not claimed.";
                    break;
                case JposConst.JPOS_E_NOSERVICE:
                    errorMsg = "Control cannot communicate with the Service, possibly because of a setup or configuration error.";
                    break;
                case JposConst.JPOS_E_DISABLED:
                    errorMsg = "Cannot perform this operation while the device is disabled.";
                    break;
                case JposConst.JPOS_E_ILLEGAL:
                    errorMsg = "An attempt was made to perform an illegal or unsupported operation with the device.";
                    break;
                case JposConst.JPOS_E_NOHARDWARE:
                    errorMsg = "The device is not connected to the system or is not powered on.";
                    break;
                case JposConst.JPOS_E_OFFLINE:
                    errorMsg = "The device is offline.";
                    break;
                case JposConst.JPOS_E_NOEXIST:
                    errorMsg = "The file name does not exist.";
                    break;
                case JposConst.JPOS_E_EXISTS:
                    errorMsg = "The file name already exists.";
                    break;
                case JposConst.JPOS_E_FAILURE:
                    errorMsg = "The device cannot perform the requested procedure, even though the device is connected to the system, powered on, and online.";
                    break;
                case JposConst.JPOS_E_TIMEOUT:
                    errorMsg = "The service timed out waiting for a response from the device, or the control timed out waiting for a response from the service.";
                    break;
                case JposConst.JPOS_E_BUSY:
                    errorMsg = "The current Device Service state does not allow this request.";
                    break;
                case JposConst.JPOS_E_EXTENDED:
                    errorMsg = "A device category-specific error condition has occurred.";
                    break;
                default:
                    errorMsg = "Error code message not found";
                    break;
            }
        } catch (Exception e) {
            log("Printer: getErrorCodeMsg exception! " + e.getMessage(), Logger.LEVEL.ERROR);
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            log("Printer: " + sw.toString(), Logger.LEVEL.ERROR);
        }

        return errorMsg;
    }

    public String getErrorCodeExt (int extErrorCode)
    {
        String extErrorCodeMsg = "";

        try {
            switch (extErrorCode) {
                case POSPrinterConst.JPOS_EPTR_COVER_OPEN:
                    extErrorCodeMsg = "The printer cover is open.";
                    break;
                case POSPrinterConst.JPOS_EPTR_JRN_EMPTY:
                case POSPrinterConst.JPOS_EPTR_REC_EMPTY:
                    extErrorCodeMsg = "The printer is out of paper.";
                    break;
                case POSPrinterConst.JPOS_EPTR_REC_CARTRIDGE_EMPTY:
                    extErrorCodeMsg = "Printer cartridge is empty.";
                    break;
                case POSPrinterConst.JPOS_EPTR_BADFORMAT:
                    extErrorCodeMsg = "Bad format.";
                    break;
                default:
                    extErrorCodeMsg = "Extended error code not found.";
                    break;
            }
        } catch (Exception e) {
            log("Printer: getErrorCodeExt exception! " + e.getMessage(), Logger.LEVEL.ERROR);
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            log("Printer: " + sw.toString(), Logger.LEVEL.ERROR);
        }
        return extErrorCodeMsg;
    }

    public void clearPrinterOutput()
    {
        try {
            ptr.clearOutput();
        } catch (Exception e) {
            log("Printer: clearPrinterOutput exception! " + e.getMessage(), Logger.LEVEL.ERROR);
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            log("Printer: " + sw.toString(), Logger.LEVEL.ERROR);
        }
    }


    public boolean printSimpleReceiptForKitchen(int terminalID, ArrayList details) {
        boolean retVal= false;

        if(details.size()>1) {
            try {
                DecimalFormat qtyFormat  = new DecimalFormat("##0");

                String printerName = "";
                String transactionID = "";
                String cashierName = "";
                String terminalName = "";
                String date = "";
                String time = "";

                for(int i=0; i<details.size(); i++){
                    ArrayList row = (ArrayList)details.get(i);
                    if(0 == i){ // first row contains header detail
                        transactionID = row.get(0).toString();
                        String sdt = row.get(1).toString();
                        SimpleDateFormat dtFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                        Date dt = dtFormat.parse(sdt);

                        cashierName = row.get(2).toString();
                        terminalName = row.get(3).toString();

                        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
                        SimpleDateFormat stf = new SimpleDateFormat("hh:mm:ss a");
                        date = df.format(dt);
                        time = stf.format(dt);

                    } else {

                        String currentPrinterName = row.get(5).toString();
                        String Name = row.get(2).toString();
                        Double qty =  Double.parseDouble(row.get(0).toString());
                        String s_qty = "    X" + qtyFormat.format(qty);
                        boolean isModifier = Boolean.parseBoolean(row.get(6).toString());

                        switch (s_qty.length())
                        {
                            case 6:  s_qty += "  -"; break;
                            case 7:  s_qty += " -"; break;
                            default:  s_qty += "-"; break;
                        }

                        // print header for each printer
                        if(0 != printerName.compareTo(currentPrinterName)){
                            if(printerName.length()>0)
                                finishPrint();
                            printerName = currentPrinterName;
                            printHeaderForSimpleKitchenReceipt(date, time, cashierName, terminalID, transactionID, terminalName);
                            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|cA" + printerName + "\n\n");
                        }

                        if (isModifier == true) {
                            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "       -> " + Name + "\n");
                        } else {
                            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, s_qty + " " + Name + "\n");
                        }

                    }

                }

                finishPrint();

                retVal = true;

            } catch (JposException ex) {
                retVal = false;
                Logger.logMessage("Printer state: " + ptr.getState(), logFileName);
                Logger.logMessage("Printer: JPosException calling printSimpleReceiptForKitchen(" + terminalID + "); " + ex.getMessage(), logFileName);
                handleJposException(ex);
            }  catch (Exception ex) {
                retVal = false;
                Logger.logMessage("Printer state: " + ptr.getState(), logFileName);
                Logger.logMessage("Printer: exception calling printSimpleReceiptForKitchen(" + terminalID + "); " + ex.getMessage(), logFileName);
            }
        }

        return retVal;
    }

    private void printHeaderForSimpleKitchenReceipt(String date, String time, String cashierName, int terminalID, String transactionID, String terminalName) throws JposException {
        ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\n" + date);
        ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|rA" + time + "\n");
        ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "Cashier: " + cashierName);
        ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|rA" + "TID: " + terminalID + "\n");
        ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "SALE : " + transactionID);
        ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|rA" + terminalName + "\n");
    }

    public void openCashDrawerA()  {
        try {
            ((MMHPosPrinter)ptr).openCashDrawerA();
        }  catch (Exception ex) {
            Logger.logMessage("Printer: exception calling openCashDrawerA(); " + ex.getMessage(), logFileName);
        }
    }


    public void openCashDrawerB()  {
        try {
            ((MMHPosPrinter)ptr).openCashDrawerB();
        }  catch (Exception ex) {
            Logger.logMessage("Printer: exception calling openCashDrawerB(); " + ex.getMessage(), logFileName);
        }
    }



}
