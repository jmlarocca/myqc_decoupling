package com.mmhayes.common.kitchenapi;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.DynamicSQL;
import com.mmhayes.common.kitchenPrinters.*;
import com.mmhayes.common.kitchenPrinters.KDS.KDSEmailAlert;
import com.mmhayes.common.kitchenPrinters.KDS.KDSShareFolderMonitor;
import com.mmhayes.common.printing.PrintStatusType;
import com.mmhayes.common.receiptGen.Formatter.KitchenPrinterReceiptFormatter;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.receiptGen.ReceiptGen;
import com.mmhayes.common.receiptGen.Renderer.IRenderer;
import com.mmhayes.common.receiptGen.TypeData;
import com.mmhayes.common.utils.*;
import com.mmhayes.common.kms.PeripheralsDataManager;
import org.apache.commons.lang.math.NumberUtils;
import redstone.xmlrpc.XmlRpcArray;
import redstone.xmlrpc.XmlRpcStruct;

import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * <p>Data manager for kitchen printer and KDS data.</p>
 *
 */
public class KitchenPrinterDataManager extends ReflectiveInvocationHandlerTrackStats {

    // log file to be used by the KitchenPrinterDataManager
    private static final String KPDM_LOG = "KP_DataManager.log";

    /**
     * <p>Called through XML RPC to start the printer host check on this terminal if it hasn't been started already.</p>
     *
     * @param callerHostname The hostname {@link String} of the terminal that made the XML RPC call.
     * @param healthCheckFrequency How frequently the health check should run on this terminal.
     * @return Whether or not the printer host health check could be started successfully on this terminal.
     */
    public boolean startPrinterHostHealthCheck (String callerHostname, int healthCheckFrequency) {
        boolean success = false;

        try {
            if (!BackofficeChangeUpdater.getInstance().getIsPHHealthCheckRunningOnTerminal()) {
                BackofficeChangeUpdater.getInstance().startPrinterHostHealthCheck(healthCheckFrequency);
                success = true;
            }
            else {
                // the printer host check is already running on the terminal
                success = true;
            }
        }
        catch (Exception e) {
            Logger.logException(e, KPDM_LOG);
            Logger.logMessage(String.format("There was a problem trying to start the printer host health check on the " +
                    "terminal %s from the terminal %s in KitchenPrinterDataManager.startPrinterHostHealthCheck",
                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL"),
                    Objects.toString(callerHostname, "NULL")), KPDM_LOG, Logger.LEVEL.ERROR);
        }

        return success;
    }

    /**
     * <p>Called through XML RPC to stop the printer host check on this terminal if it hasn't been stopped already.</p>
     *
     * @param callerHostname The hostname {@link String} of the terminal that made the XML RPC call.
     * @return Whether or not the printer host health check could be stopped successfully on this terminal.
     */
    public boolean stopPrinterHostHealthCheck (String callerHostname) {
        boolean success = false;

        try {
            if (BackofficeChangeUpdater.getInstance().getIsPHHealthCheckRunningOnTerminal()) {
                BackofficeChangeUpdater.getInstance().stopPrinterHostHealthCheck();
                success = true;
            }
            else {
                // the printer host health check has already been stopped
                success = true;
            }
        }
        catch (Exception e) {
            Logger.logException(e, KPDM_LOG);
            Logger.logMessage(String.format("There was a problem trying to stop the printer host health check on the " +
                    "terminal %s from the terminal %s in KitchenPrinterDataManager.stopPrinterHostHealthCheck",
                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL"),
                    Objects.toString(callerHostname, "NULL")), KPDM_LOG, Logger.LEVEL.ERROR);
        }

        return success;
    }

    /**
     * <p>Called through XML RPC to add the {@link QCPrinterHost} with the given printer host ID to
     * the {@link BackofficeChangeUpdater} {@link ArrayList} of {@link QCPrinterHost}.</p>
     *
     * @param callerHostname The hostname {@link String} of the terminal that made the XML RPC call.
     * @param printerHostID ID of the {@link QCPrinterHost} to add.
     * @return Whether or not the printer host could be added successfully.
     */
    public boolean addPrinterHostToBackofficeUpdaterPrinterHosts (String callerHostname, int printerHostID) {
        boolean success = true;

        try {
            success = BackofficeChangeUpdater.getInstance().addPrinterHost(printerHostID);
        }
        catch (Exception e) {
            Logger.logException(e, KPDM_LOG);
            Logger.logMessage(String.format("There was a problem trying to add the printer host running on the " +
                    "terminal %s to the BackofficeChangeUpdater's list of printer hosts on the terminal %s from the " +
                    "terminal %s in KitchenPrinterDataManager.addPrinterHostToBackofficeUpdaterPrinterHosts",
                    Objects.toString(new QCPrinterHost(PeripheralsDataManager.getPHRecFromLocalDB(printerHostID)).getHostname(), "NULL"),
                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL"),
                    Objects.toString(callerHostname, "NULL")), KPDM_LOG, Logger.LEVEL.ERROR);
        }

        return success;
    }

    /**
     * <p>Called through XML RPC to remove the {@link QCPrinterHost} with the given printer host ID from
     * the {@link BackofficeChangeUpdater} {@link ArrayList} of {@link QCPrinterHost}.</p>
     *
     * @param callerHostname The hostname {@link String} of the terminal that made the XML RPC call.
     * @param printerHostID ID of the {@link QCPrinterHost} to remove.
     * @return Whether or not the printer host could be removed successfully.
     */
    public boolean removePrinterHostFromBackofficeUpdaterPrinterHosts (String callerHostname, int printerHostID) {
        boolean success = true;

        try {
            success = BackofficeChangeUpdater.getInstance().removePrinterHost(printerHostID);
        }
        catch (Exception e) {
            Logger.logException(e, KPDM_LOG);
            Logger.logMessage(String.format("There was a problem trying to remove the printer host running on the " +
                    "terminal %s from the BackofficeChangeUpdater's list of printer hosts on the terminal %s from the " +
                    "terminal %s in KitchenPrinterDataManager.removePrinterHostFromBackofficeUpdaterPrinterHosts",
                    Objects.toString(new QCPrinterHost(PeripheralsDataManager.getPHRecFromLocalDB(printerHostID)).getHostname(), "NULL"),
                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL"),
                    Objects.toString(callerHostname, "NULL")), KPDM_LOG, Logger.LEVEL.ERROR);
        }

        return success;
    }

    /**
     * <p>Called through XML RPC to add the {@link QCKitchenPrinter} with the given printer host ID to
     * the {@link BackofficeChangeUpdater} {@link ArrayList} of {@link QCKitchenPrinter}.</p>
     *
     * @param callerHostname The hostname {@link String} of the terminal that made the XML RPC call.
     * @param printerID ID of the {@link QCKitchenPrinter} to add.
     * @return Whether or not the printer could be added successfully.
     */
    public boolean addPrinterToBackofficeUpdaterPrinters (String callerHostname, int printerID) {
        boolean success = true;

        try {
            success = BackofficeChangeUpdater.getInstance().addPrinter(printerID);
        }
        catch (Exception e) {
            Logger.logException(e, KPDM_LOG);
            Logger.logMessage(String.format("There was a problem trying to add the printer %s to the " +
                    "BackofficeChangeUpdater's list of printers on the terminal %s from the " +
                    "terminal %s in KitchenPrinterDataManager.addPrinterToBackofficeUpdaterPrinters",
                    Objects.toString(new QCKitchenPrinter(PeripheralsDataManager.getPtrRecFromLocalDB(printerID)).getName(), "NULL"),
                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL"),
                    Objects.toString(callerHostname, "NULL")), KPDM_LOG, Logger.LEVEL.ERROR);
        }

        return success;
    }

    /**
     * <p>Called through XML RPC to remove the {@link QCKitchenPrinter} with the given printer host ID from
     * the {@link BackofficeChangeUpdater} {@link ArrayList} of {@link QCKitchenPrinter}.</p>
     *
     * @param callerHostname The hostname {@link String} of the terminal that made the XML RPC call.
     * @param printerID ID of the {@link QCKitchenPrinter} to remove.
     * @return Whether or not the printer could be removed successfully.
     */
    public boolean removePrinterFromBackofficeUpdaterPrinters (String callerHostname, int printerID) {
        boolean success = true;

        try {
            success = BackofficeChangeUpdater.getInstance().removePrinter(printerID);
        }
        catch (Exception e) {
            Logger.logException(e, KPDM_LOG);
            Logger.logMessage(String.format("There was a problem trying to remove the printer %s from the " +
                    "BackofficeChangeUpdater's list of printers on the terminal %s from the " +
                    "terminal %s in KitchenPrinterDataManager.removePrinterFromBackofficeUpdaterPrinters",
                    Objects.toString(new QCKitchenPrinter(PeripheralsDataManager.getPtrRecFromLocalDB(printerID)).getName(), "NULL"),
                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL"),
                    Objects.toString(callerHostname, "NULL")), KPDM_LOG, Logger.LEVEL.ERROR);
        }

        return success;
    }

    /**
     * <p>Called through XML RPC to start the printer host on this terminal.</p>
     *
     * @param callerHostname The hostname {@link String} of the terminal that made the XML RPC call.
     * @return Whether or not the printer host could be successfully started on this terminal.
     */
    public boolean startPrinterHost (String callerHostname) {
        boolean success = false;

        try {
            PrinterHostThread.getInstance().startPrinterHost();

            // wait a little for the printer host to start up
            Thread.sleep(PrinterHostHealthCheck.FIVE_SECONDS);

            // check whether or not the printer host could be started properly
            success = PrinterHost.getInstance().isPrinterHostActive();
        }
        catch (Exception e) {
            Logger.logException(e, KPDM_LOG);
            Logger.logMessage(String.format("There was a problem trying to start the printer host on the terminal %s " +
                    "from the terminal %s in KitchenPrinterDataManager.startPrinterHost",
                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL"),
                    Objects.toString(callerHostname, "NULL")), KPDM_LOG, Logger.LEVEL.ERROR);
        }

        return success;
    }

    /**
     * <p>Called through XML RPC to stop the printer host on this terminal.</p>
     *
     * @param callerHostname The hostname {@link String} of the terminal that made the XML RPC call.
     * @return Whether or not the printer host could be successfully stopped on this terminal.
     */
    public boolean stopPrinterHost (String callerHostname) {
        boolean success = false;

        try {
            PrinterHostThread.getInstance().stopPrinterHost();

            // wait a little for the printer host to stop
            Thread.sleep(PrinterHostHealthCheck.FIVE_SECONDS);

            // check whether or not the printer host could be stopped properly
            success = !PrinterHost.getInstance().isPrinterHostActive();
        }
        catch (Exception e) {
            Logger.logException(e, KPDM_LOG);
            Logger.logMessage(String.format("There was a problem trying to stop the printer host on the terminal %s " +
                    "from the terminal %s in KitchenPrinterDataManager.stopPrinterHost",
                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL"),
                    Objects.toString(callerHostname, "NULL")), KPDM_LOG, Logger.LEVEL.ERROR);
        }

        return success;
    }

    /**
     * <p>Called through XML RPC to determine whether or not the printer host on this terminal is in-service.</p>
     *
     * @param callerHostname The hostname {@link String} of the terminal that made the XML RPC call.
     * @return Whether or not the printer host on this terminal is in-service.
     */
    public boolean isPrinterHostInService (String callerHostname) {
        boolean inService = false;

        try {
            inService = PrinterHost.getInstance().isPrinterHostActive();
        }
        catch (Exception e) {
            Logger.logException(e, KPDM_LOG);
            Logger.logMessage(String.format("There was a problem trying to determine whether or not the printer host " +
                    "on the terminal %s is in-service from the terminal %s in KitchenPrinterDataManager.isPrinterHostInService",
                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL"),
                    Objects.toString(callerHostname, "NULL")), KPDM_LOG, Logger.LEVEL.ERROR);
        }

        return inService;
    }

    /**
     * <p>Called through XML RPC to get the MAC address {@link String} of the terminal running the in-service printer host as perceived by this terminal.</p>
     *
     * @param callerHostname The hostname {@link String} of the terminal that made the XML RPC call.
     * @return The MAC address {@link String} of the terminal running the in-service printer host as perceived by this terminal.
     */
    public String getMacAddressOfInServicePrinterHost (String callerHostname) {
        String macAddressOfInServicePrinterHost = "";

        try {
            macAddressOfInServicePrinterHost = KitchenPrinterTerminal.getInstance().getActivePHMacAddress();
        }
        catch (Exception e) {
            Logger.logException(e, KPDM_LOG);
            Logger.logMessage(String.format("There was a problem trying to get the MAC address of the terminal running " +
                    "the in-service printer host as perceived by this terminal from the terminal %s in " +
                    "KitchenPrinterDataManager.getMacAddressOfInServicePrinterHost",
                    Objects.toString(callerHostname, "NULL")), KPDM_LOG, Logger.LEVEL.ERROR);
        }

        return macAddressOfInServicePrinterHost;
    }

    /**
     * <p>Called through XML RPC to get the time {@link String} the in-service printer host became the in-service printer host on this terminal.</p>
     *
     * @param callerHostname The hostname {@link String} of the terminal that made the XML RPC call.
     * @return The time {@link String} the in-service printer host became the in-service printer host on this terminal.
     */
    public String getTimeInServicePrinterHostBecameInService (String callerHostname) {
        String timeInServicePrinterHostBecameInService = "";

        try {
            timeInServicePrinterHostBecameInService = KitchenPrinterTerminal.getInstance().getTimePHBecameActive().toString();
        }
        catch (Exception e) {
            Logger.logException(e, KPDM_LOG);
            Logger.logMessage(String.format("There was a problem trying to get the time the perceived in-service " +
                    "printer host on the terminal %s began operating as the in-service printer host in " +
                    "from the terminal %s in KitchenPrinterDataManager.getTimeInServicePrinterHostBecameInService",
                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL"),
                    Objects.toString(callerHostname, "NULL")), KPDM_LOG, Logger.LEVEL.ERROR);
        }

        return timeInServicePrinterHostBecameInService;
    }

    /**
     * <p>Called through XML RPC to set the MAC address {@link String} of the terminal running the in-service printer host on this terminal.</p>
     *
     * @param callerHostname The hostname {@link String} of the terminal that made the XML RPC call.
     * @param macAddress The MAC address {@link String} of the in-service printer host.
     * @return Whether or not the MAC address {@link String} of the terminal running the in-service printer host could be set properly on this terminal.
     */
    public boolean setMacAddressOfInServicePrinterHost (String callerHostname, String macAddress) {
        boolean success = false;

        try {
            if (StringFunctions.stringHasContent(macAddress)) {
                KitchenPrinterTerminal.getInstance().setActivePHMacAddress(macAddress);
                success = true;
            }
        }
        catch (Exception e) {
            Logger.logException(e, KPDM_LOG);
            Logger.logMessage(String.format("There was a problem trying to set the MAC address of the terminal running " +
                    "the in-service printer host as %s on the terminal %s from the terminal %s in KitchenPrinterDataManager.setMacAddressOfInServicePrinterHost",
                    Objects.toString(macAddress, "NULL"),
                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL"),
                    Objects.toString(callerHostname, "NULL")), KPDM_LOG, Logger.LEVEL.ERROR);
        }

        return success;
    }

    /**
     * <p>Called through XML RPC to set the time the in-service printer host became the in-service printer host on this terminal.</p>
     *
     * @param callerHostname The hostname {@link String} of the terminal that made the XML RPC call.
     * @param time The time {@link String} the in-service printer host became the in-service printer host.
     * @return Whether or not the time the in-service printer host became the in-service printer host on this terminal.
     */
    public boolean setTimeInServicePrinterHostBecameInService (String callerHostname, String time) {
        boolean success = false;

        try {
            if ((StringFunctions.stringHasContent(time)) && (DataFunctions.isValidLocalDateTimeString(time))) {
                KitchenPrinterTerminal.getInstance().setTimePHBecameActive(LocalDateTime.parse(time));
                success = true;
            }
        }
        catch (Exception e) {
            Logger.logException(e, KPDM_LOG);
            Logger.logMessage(String.format("There was a problem trying to set the time the in-service printer host " +
                    "became the in-service printer host as %s on the terminal %s from the terminal %s in KitchenPrinterDataManager.setTimeInServicePrinterHostBecameInService",
                    Objects.toString(time, "NULL"),
                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL"),
                    Objects.toString(callerHostname, "NULL")), KPDM_LOG, Logger.LEVEL.ERROR);
        }

        return success;
    }

    /**
     * <p>Called through XML RPC to get the status of the printer host configured to run on this terminal.</p>
     *
     * @param callerHostname The hostname {@link String} of the terminal that made the XML RPC call.
     * @return The printer host status {@link String} of the printer host configured to run on this terminal.
     */
    public String getPrinterHostStatus (String callerHostname) {
        String status = "";

        try {
            if (KitchenPrinterTerminal.getInstance().getPrinterHostStatusTracker().containsKey(PeripheralsDataManager.getTerminalMacAddress())) {
                status = KitchenPrinterTerminal.getInstance().getPrinterHostStatusTracker().get(PeripheralsDataManager.getTerminalMacAddress()).getPrinterHostStatus();
            }
        }
        catch (Exception e) {
            Logger.logException(e, KPDM_LOG);
            Logger.logMessage(String.format("There was a problem trying to get the status of the printer host running " +
                    "on the terminal %s from the terminal %s in KitchenPrinterDataManager.getPrinterHostStatus",
                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL"),
                    Objects.toString(callerHostname, "NULL")), KPDM_LOG, Logger.LEVEL.ERROR);
        }

        return status;
    }

    /**
     * <p>Called through XML RPC to set the in-service printer host on the server.</p>
     *
     * @param callerHostname The hostname {@link String} of the terminal that made the XML RPC call.
     * @param macAddress The MAC address {@link String} of the in-service printer host.
     * @return Whether or not the in-service printer host could be set properly on the server.
     */
    public boolean updateInServicePrinterHostOnServer (String callerHostname, String macAddress) {
        boolean success = false;

        try {
            if (StringFunctions.stringHasContent(macAddress)) {
                // get the ID of the in-service printer host
                int printerHostID = 0;
                Object queryRes = new DataManager().parameterizedExecuteScalar("data.kitchenPrinter.getPrinterHostIDFromMacAddress", new Object[]{macAddress}, KPDM_LOG);
                if ((queryRes != null) && (!queryRes.toString().isEmpty()) && (NumberUtils.isNumber(queryRes.toString()))) {
                    printerHostID = Integer.parseInt(queryRes.toString());
                }
                // update the in-service printer host on the server
                if ((printerHostID > 0) && (new DataManager().parameterizedExecuteNonQuery("data.kitchenPrinter.updateInServicePH", new Object[]{macAddress, printerHostID}, KPDM_LOG) > 0)) {
                    success = true;
                    Logger.logMessage(String.format("The in-service printer host has been set as the printer host running on the " +
                            "terminal %s from the terminal %s at %s in KitchenPrinterDataManager.updateInServicePrinterHostOnServer",
                            Objects.toString(PeripheralsDataManager.getTerminalHostname(macAddress), "NULL"),
                            Objects.toString(callerHostname, "NULL"),
                            Objects.toString(LocalDateTime.now(), "NULL")), KPDM_LOG, Logger.LEVEL.DEBUG);
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, KPDM_LOG);
            Logger.logMessage(String.format("There was a problem trying to set the in-service printer host on the server " +
                    "as the printer host running on the terminal %s from the terminal %s in KitchenPrinterDataManager.updateInServicePrinterHostOnServer",
                    Objects.toString(PeripheralsDataManager.getTerminalHostname(macAddress), "NULL"),
                    Objects.toString(callerHostname, "NULL")), KPDM_LOG, Logger.LEVEL.ERROR);
        }

        return success;
    }

    /**
     * <p>Called though XML RPC to update the printers in the {@link PrinterQueueHandler} on this terminal.</p>
     *
     * @param callerHostname The hostname {@link String} of the terminal that made the XML RPC call.
     * @param printerStructs The {@link XmlRpcArray} of {@link XmlRpcStruct} which contain information about the printers currently configured in the backoffice.
     * @return Whether or not the printers on this terminal could be updated properly.
     */
    public boolean updatePrinters (String callerHostname, XmlRpcArray printerStructs) {
        boolean success = false;

        try {
            success = PrinterQueueHandler.getInstance().updatePrinters(printerStructs);
        }
        catch (Exception e) {
            Logger.logException(e, KPDM_LOG);
            Logger.logMessage(String.format("There was a problem trying to update the printers on the terminal %s " +
                    "from the terminal %s in KitchenPrinterDataManager.updatePrinters",
                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL"),
                    Objects.toString(callerHostname, "NULL")), KPDM_LOG, Logger.LEVEL.ERROR);
        }

        return success;
    }

    /**
     * <p>Called through XML RPC to check whether or not the printer host running on this terminal is holding a connection to the desired printer.</p>
     *
     * @param callerHostname The hostname {@link String} of the terminal that made the XML RPC call.
     * @param printerID ID of the printer to check.
     * @return Whether or not the printer host running on this terminal is holding a connection to the desired printer.
     */
    public boolean isPrinterHostHoldingPrinterConnectionOnTerminal (String callerHostname, int printerID) {
        boolean hasConnection = false;
        QCKitchenPrinter printer = null;

        try {
            if ((PrinterQueueHandler.getInstance() != null) && (!DataFunctions.isEmptyMap(PrinterQueueHandler.getInstance().getPrinters()))
                    && (PrinterQueueHandler.getInstance().getPrinters().containsKey(printerID))) {
                printer = PrinterQueueHandler.getInstance().getPrinters().get(printerID);
                hasConnection = printer.getIsPrinterOpen();
            }
        }
        catch (Exception e) {
            Logger.logException(e, KPDM_LOG);
            Logger.logMessage(String.format("There was a problem trying to check whether or not the printer %s " +
                    "could be connected to on the terminal %s from the terminal %s in KitchenPrinterDataManager.isPrinterHostHoldingPrinterConnection",
                    Objects.toString((printer != null ? printer.getName() : "N/A"), "NULL"),
                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL"),
                    Objects.toString(callerHostname, "NULL")), KPDM_LOG, Logger.LEVEL.ERROR);
        }

        return hasConnection;
    }

    /**
     * <p>Called through XML RPC to get the printer host running on this terminal to release it's connection to the given printer.</p>
     *
     * @param callerHostname The hostname {@link String} of the terminal that made the XML RPC call.
     * @param printerID ID of the printer we want the printer host running on this terminal to release it's connection to.
     * @return Whether ot not the printer host running on this terminal was able to release it's connection to the given printer.
     */
    public boolean isPrinterHostConnectionToPrinterReleasedOnTerminal (String callerHostname, int printerID) {
        boolean connectionReleased = false;
        QCKitchenPrinter printer = null;

        try {
            if ((PrinterQueueHandler.getInstance() != null) && (!DataFunctions.isEmptyMap(PrinterQueueHandler.getInstance().getPrinters()))
                    && (PrinterQueueHandler.getInstance().getPrinters().containsKey(printerID))) {
                printer = PrinterQueueHandler.getInstance().getPrinters().get(printerID);
                // close connection to the printer
                connectionReleased = printer.closePrinter();
            }
        }
        catch (Exception e) {
            Logger.logException(e, KPDM_LOG);
            Logger.logMessage(String.format("There was a problem trying to get the printer host running on the terminal %s to release " +
                    "it's connection to the printer %s from the terminal %s in KitchenPrinterDataManager.isPrinterHostConnectionToPrinterReleasedOnTerminal",
                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL"),
                    Objects.toString((printer != null ? printer.getName() : "N/A"), "NULL"),
                    Objects.toString(callerHostname, "NULL")), KPDM_LOG, Logger.LEVEL.ERROR);
        }

        return connectionReleased;
    }

    /**
     * <p>Called by the printer host through XML RPC to get any online orders within the given {@link String} of revenue center IDs.</p>
     *
     * @param callerHostname The hostname {@link String} of the terminal that made the XML RPC call.
     * @param revenueCenterIDs IDs of revenue centers as a {@link String} to check within for any online orders.
     * @param logFile The file name {@link String} to log relevant information to.
     * @return An {@link ArrayList} of any online orders on the server within the given revenue center IDs {@link String}.
     */
    @SuppressWarnings("unchecked")
    public ArrayList kpCheck (String callerHostname, String revenueCenterIDs, String logFile) {
        ArrayList orders = new ArrayList();
        Logger.logMessage(String.format("KitchenPrinterDataManager.kpCheck called for RCs: %s",
                Objects.toString(revenueCenterIDs, "NULL")), logFile, Logger.LEVEL.TRACE);
        try {
            if (StringFunctions.stringHasContent(revenueCenterIDs)) {
                // get the online orders
                ArrayList<HashMap> onlineOrders = new ArrayList<>();
                ArrayList<HashMap> queryRes = new DataManager().parameterizedExecuteQuery("data.newKitchenPrinter.GetOnlineOrdersInTermsOfRevCenters", new Object[]{revenueCenterIDs}, logFile, true);
                if (!DataFunctions.isEmptyCollection(queryRes)) {
                    onlineOrders.addAll(queryRes.stream().filter(hm -> !DataFunctions.isEmptyMap(hm)).collect(Collectors.toList()));
                }

                if (!DataFunctions.isEmptyCollection(onlineOrders)) {
                    Logger.logMessage(String.format("The printer host running on the terminal %s will receive %s online " +
                            "orders found on the server at %s in KitchenPrinterDataManager.kpCheck",
                            Objects.toString(callerHostname, "NULL"),
                            Objects.toString(onlineOrders.size(), "NULL"),
                            Objects.toString(LocalDateTime.now(), "NULL")), logFile, Logger.LEVEL.TRACE);

                    // generate the print jobs details for the online orders
                    for (HashMap onlineOrder : onlineOrders) {
                        int paTxnID = HashMapDataFns.getIntVal(onlineOrder, "PATRANSACTIONID");
                        if (paTxnID > 0) {
                            Logger.logMessage(String.format("Now building the print job details for the transaction with an ID " +
                                    "of %s in KitchenPrinterDataManager.kpCheck", Objects.toString(paTxnID, "NULL")), logFile, Logger.LEVEL.DEBUG);
                        }

                        try {
                            ReceiptGen receiptGen = new ReceiptGen();
                            receiptGen.setReceiptFormatter(new KitchenPrinterReceiptFormatter(new DataManager()));
                            IRenderer htmlRenderer = receiptGen.createBasicHTMLRenderer();
                            receiptGen.setReceiptData(receiptGen.createReceiptData());
                            receiptGen.getReceiptData().setPopupVal(true);
                            if (htmlRenderer != null) {
                                receiptGen.buildFromDBOrderNum(paTxnID, TypeData.ReceiptType.KITCHENPTRRECEIPT, htmlRenderer);
                                ArrayList details = receiptGen.getReceiptData().getReceiptDetails();
                                ArrayList<HashMap> temp = details;
                                for(HashMap H : temp){
                                    if(H.containsKey("LINKEDFROMIDS")){
                                        Logger.logMessage("LINKEDFROMIDS=" + H.get("LINKEDFROMIDS"), logFile, Logger.LEVEL.DEBUG);
                                    }
                                }
                                Logger.logMessage("Got receipt details of size:" + details.size(), logFile, Logger.LEVEL.DEBUG);
                                //orders.add(receiptGen.getReceiptData().getReceiptHeaders());
                                orders.add(details);
                                //orders.add(receiptGen.getReceiptData().getReceiptFooters());
                                Logger.logMessage("Added to orders of size " + orders.size(), logFile, Logger.LEVEL.DEBUG);
//                                orders.add(receiptGen.getReceiptData().getKpJobInfo().getPlainTextDetails());
                            }
                            else {
                                Logger.logMessage(String.format("No valid html renderer was found, unable to create the print job details " +
                                        "for the transaction with an ID of %s in KitchenPrinterDataManager.kpCheck",
                                        Objects.toString(paTxnID, "NULL")), logFile, Logger.LEVEL.ERROR);
                            }
                        }
                        catch (Exception e) {
                            Logger.logException(e, logFile);
                            Logger.logMessage(String.format("A problem occurred while trying to build the print job details for " +
                                            "the transaction with an ID of %s in KitchenPrinterDataManager.kpCheck",
                                    Objects.toString(paTxnID, "NULL")), logFile, Logger.LEVEL.ERROR);
                        }

                        // update the print status of the online order to done so that the printer host doesn't get this same print job again
                        if ((new DataManager().parameterizedExecuteNonQuery("data.newKitchenPrinter.SetPrintQueueStatusToDoneByPATransactionID", new Object[]{paTxnID}, logFile)) > 0) {
                            Logger.logMessage(String.format("The print queue header for the transaction with an ID of %s has had it's " +
                                    "print status updated to DONE at %s in KitchenPrinterDataManager.kpCheck",
                                    Objects.toString(paTxnID, "NULL"),
                                    Objects.toString(LocalDateTime.now(), "NULL")), logFile, Logger.LEVEL.ERROR);
                        }
                        if ((new DataManager().parameterizedExecuteNonQuery("data.newKitchenPrinter.SetPrintQueueDetailStatusToDoneByPATransactionID", new Object[]{paTxnID}, logFile)) > 0) {
                            Logger.logMessage(String.format("The print queue details for the transaction with an ID of %s have had their " +
                                    "print status updated to DONE at %s in KitchenPrinterDataManager.kpCheck",
                                    Objects.toString(paTxnID, "NULL"),
                                    Objects.toString(LocalDateTime.now(), "NULL")), logFile, Logger.LEVEL.ERROR);
                        }
                    }
                }
            }
            else {
                Logger.logMessage("No valid revenue center IDs were found, unable to check for online orders in KitchenPrinterDataManager.kpCheck", logFile, Logger.LEVEL.ERROR);
            }
        }
        catch (Exception e) {
            Logger.logException(e, logFile);
            Logger.logMessage(String.format("A problem occurred while trying to obtain online orders made within the revenue centers " +
                    "with the given IDs of %s, as a result, online orders may not be reaching the printer host running on the terminal %s " +
                    "in KitchenPrinterDataManager.kpCheck",
                    Objects.toString(revenueCenterIDs, "NULL"),
                    Objects.toString(callerHostname, "NULL")), logFile, Logger.LEVEL.ERROR);
        }
        Logger.logMessage("Returning orders of size " + orders.size(), logFile, Logger.LEVEL.DEBUG);
        return orders;
    }


    public ArrayList kpCheckIDs (String callerHostname, String revenueCenterIDs, String logFile) {
        ArrayList orders = new ArrayList();
        Logger.logMessage(String.format("KitchenPrinterDataManager.kpCheck called for RCs: %s",
                Objects.toString(revenueCenterIDs, "NULL")), logFile, Logger.LEVEL.TRACE);
        try {
            if (StringFunctions.stringHasContent(revenueCenterIDs)) {
                // get the online orders
                ArrayList<HashMap> onlineOrders = new ArrayList<>();
                ArrayList<HashMap> queryRes = new DataManager().parameterizedExecuteQuery("data.kms.GetOnlineOrderIDsInTermsOfRevCenters", new Object[]{revenueCenterIDs}, logFile, true);
                if (!DataFunctions.isEmptyCollection(queryRes)) {
                    onlineOrders.addAll(queryRes.stream().filter(hm -> !DataFunctions.isEmptyMap(hm)).collect(Collectors.toList()));
                }

                if (!DataFunctions.isEmptyCollection(onlineOrders)) {
                    Logger.logMessage(String.format("The printer host running on the terminal %s will receive %s online " +
                            "orders found on the server at %s in KitchenPrinterDataManager.kpCheck",
                            Objects.toString(callerHostname, "NULL"),
                            Objects.toString(onlineOrders.size(), "NULL"),
                            Objects.toString(LocalDateTime.now(), "NULL")), logFile, Logger.LEVEL.TRACE);

                    // generate the print jobs details for the online orders
                    for (HashMap onlineOrder : onlineOrders) {
                        int paTxnID = HashMapDataFns.getIntVal(onlineOrder, "PATRANSACTIONID");
                        if (paTxnID > 0) {
                            Logger.logMessage(String.format("Adding transaction with an ID " +
                                    "of %s in KitchenPrinterDataManager.kpCheckIDs", Objects.toString(paTxnID, "NULL")), logFile, Logger.LEVEL.DEBUG);
                            orders.add(paTxnID);
                        }

//                        try {
//                            ReceiptGen receiptGen = new ReceiptGen();
//                            receiptGen.setReceiptFormatter(new KitchenPrinterReceiptFormatter(new DataManager()));
//                            IRenderer htmlRenderer = receiptGen.createBasicHTMLRenderer();
//                            receiptGen.setReceiptData(receiptGen.createReceiptData());
//                            receiptGen.getReceiptData().setPopupVal(true);
//                            if (htmlRenderer != null) {
//                                receiptGen.buildFromDBOrderNum(paTxnID, TypeData.ReceiptType.KITCHENPTRRECEIPT, htmlRenderer);
//                                ArrayList details = receiptGen.getReceiptData().getReceiptDetails();
//                                ArrayList<HashMap> temp = details;
//                                for(HashMap H : temp){
//                                    if(H.containsKey("LINKEDFROMIDS")){
//                                        Logger.logMessage("LINKEDFROMIDS=" + H.get("LINKEDFROMIDS"), logFile, Logger.LEVEL.DEBUG);
//                                    }
//                                }
//                                Logger.logMessage("Got receipt details of size:" + details.size(), logFile, Logger.LEVEL.DEBUG);
//                                //orders.add(receiptGen.getReceiptData().getReceiptHeaders());
//                                orders.add(details);
//                                //orders.add(receiptGen.getReceiptData().getReceiptFooters());
//                                Logger.logMessage("Added to orders of size " + orders.size(), logFile, Logger.LEVEL.DEBUG);
////                                orders.add(receiptGen.getReceiptData().getKpJobInfo().getPlainTextDetails());
//                            }
//                            else {
//                                Logger.logMessage(String.format("No valid html renderer was found, unable to create the print job details " +
//                                        "for the transaction with an ID of %s in KitchenPrinterDataManager.kpCheck",
//                                        Objects.toString(paTxnID, "NULL")), logFile, Logger.LEVEL.ERROR);
//                            }
//                        }
//                        catch (Exception e) {
//                            Logger.logException(e, logFile);
//                            Logger.logMessage(String.format("A problem occurred while trying to build the print job details for " +
//                                    "the transaction with an ID of %s in KitchenPrinterDataManager.kpCheck",
//                                    Objects.toString(paTxnID, "NULL")), logFile, Logger.LEVEL.ERROR);
//                        }
//
                        // update the print status of the online order to done so that the printer host doesn't get this same print job again
                        if ((new DataManager().parameterizedExecuteNonQuery("data.newKitchenPrinter.SetPrintQueueStatusToDoneByPATransactionID", new Object[]{paTxnID}, logFile)) > 0) {
                            Logger.logMessage(String.format("The print queue header for the transaction with an ID of %s has had it's " +
                                    "print status updated to DONE at %s in KitchenPrinterDataManager.kpCheck",
                                    Objects.toString(paTxnID, "NULL"),
                                    Objects.toString(LocalDateTime.now(), "NULL")), logFile, Logger.LEVEL.ERROR);
                        }
                        if ((new DataManager().parameterizedExecuteNonQuery("data.newKitchenPrinter.SetPrintQueueDetailStatusToDoneByPATransactionID", new Object[]{paTxnID}, logFile)) > 0) {
                            Logger.logMessage(String.format("The print queue details for the transaction with an ID of %s have had their " +
                                    "print status updated to DONE at %s in KitchenPrinterDataManager.kpCheck",
                                    Objects.toString(paTxnID, "NULL"),
                                    Objects.toString(LocalDateTime.now(), "NULL")), logFile, Logger.LEVEL.ERROR);
                        }
                    }
                }
            }
            else {
                Logger.logMessage("No valid revenue center IDs were found, unable to check for online orders in KitchenPrinterDataManager.kpCheck", logFile, Logger.LEVEL.ERROR);
            }
        }
        catch (Exception e) {
            Logger.logException(e, logFile);
            Logger.logMessage(String.format("A problem occurred while trying to obtain online orders made within the revenue centers " +
                    "with the given IDs of %s, as a result, online orders may not be reaching the printer host running on the terminal %s " +
                    "in KitchenPrinterDataManager.kpCheck",
                    Objects.toString(revenueCenterIDs, "NULL"),
                    Objects.toString(callerHostname, "NULL")), logFile, Logger.LEVEL.ERROR);
        }
        Logger.logMessage("Returning orders of size " + orders.size(), logFile, Logger.LEVEL.DEBUG);
        return orders;
    }


    /**
     * <p>Called through XML RPC to get the MAC address {@link String} of the printer host that is in-service on the server.</p>
     *
     * @param callerHostname The hostname {@link String} of the terminal that made the XML RPC call.
     * @param callerTerminalID The ID of the terminal that made the XML RPC call.
     * @return The MAC address {@link String} of the printer host that is in-service on the server.
     */
    public String getInServicePrinterHostOnServer (String callerHostname, int callerTerminalID) {
        String macAddress = "";

        try {
            if (callerTerminalID > 0) {
                Object queryRes = new DataManager().parameterizedExecuteScalar("data.kitchenPrinter.getInServicePH", new Object[]{callerTerminalID}, KPDM_LOG);
                if ((queryRes != null) && (StringFunctions.stringHasContent(queryRes.toString()))) {
                    macAddress = queryRes.toString();
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, KPDM_LOG);
            Logger.logMessage(String.format("There was a problem trying to get the MAC address of the terminal running the printer host " +
                    "that is in-service on the server from the terminal %s in KitchenPrinterDataManager.getInServicePrinterHostOnServer",
                    Objects.toString(callerHostname, "NULL")), KPDM_LOG, Logger.LEVEL.ERROR);
        }

        return macAddress;
    }

    /**
     * <p>Called through XML RPC to get the MAC address {@link String} of the terminal running the in-service printer host on this terminal.</p>
     *
     * @param callerHostname The hostname {@link String} of the terminal that made the XML RPC call.
     * @return The MAC address {@link String} of the terminal running the in-service printer host on this terminal.
     */
    public String getInServicePrinterHostOnTerminal (String callerHostname) {
        String macAddress = "";

        try {
            if ((KitchenPrinterTerminal.getInstance() != null) && (StringFunctions.stringHasContent(KitchenPrinterTerminal.getInstance().getActivePHMacAddress()))) {
                macAddress = KitchenPrinterTerminal.getInstance().getActivePHMacAddress();
            }
        }
        catch (Exception e) {
            Logger.logException(e, KPDM_LOG);
            Logger.logMessage(String.format("There was a problem trying to get the MAC address of the terminal running the printer host " +
                    "that is in-service on the terminal %s from the terminal %s in KitchenPrinterDataManager.getInServicePrinterHostOnTerminal",
                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL"),
                    Objects.toString(callerHostname, "NULL")), KPDM_LOG, Logger.LEVEL.ERROR);
        }

        return macAddress;
    }

    /**
     * <p>Called through XML RPC to check whether or not the print job with the given printer queue ID has failed.</p>
     *
     * @param callerHostname The hostname {@link String} of the terminal that made the XML RPC call.
     * @param paPrinterQueueID The ID of the printer queue record to check.
     * @return Whether or not the print job with the given printer queue ID has failed.
     */
    public boolean isKDSPrintJobFailed (String callerHostname, int paPrinterQueueID) {

        int printQueueHeaderPrintStatusID = 0;
        Object queryRes = KitchenPrinterTerminal.getInstance().getDataManager().parameterizedExecuteScalar("data.kitchenPrinter.GetPrintStatusOfPrintQueueHeader", new Object[]{paPrinterQueueID});
        if ((queryRes != null) && (StringFunctions.stringHasContent(queryRes.toString())) && (NumberUtils.isNumber(queryRes.toString()))) {
            printQueueHeaderPrintStatusID = Integer.parseInt(queryRes.toString());
        }

        if ((printQueueHeaderPrintStatusID == PrintStatusType.PRINTED)
                || (printQueueHeaderPrintStatusID == PrintStatusType.ERROR)
                || (printQueueHeaderPrintStatusID == PrintStatusType.FAILEDLOCALPRINT)
                || (printQueueHeaderPrintStatusID == PrintStatusType.DECLINEDLOCALREPRINT)) {
            return true;
        }

        return false;
    }

    /**
     * <p>Called through XML RPC to send an email alert if KDS XML order files have been sitting in the share folder too long.</p>
     *
     * @param terminalMacAddr The MAC address {@link String} of the terminal that invoked this method.
     * @param terminalHostname The hostname {@link String} of the terminal that invoked this method.
     * @param companyName The name {@link String} of the company that wishes to send the email.
     * @param expiredKDSXMLFileJSON A JSON {@link String} containing the KDS XML order files which have been sitting in the share folder too long.
     * @return Whether or not the email alert if KDS XML order files have been sitting in the share folder too long could be sent properly.
     */
    @SuppressWarnings("TypeMayBeWeakened")
    public boolean sendEmailForExpiredKDSXML (String terminalMacAddr, String terminalHostname, String companyName, String expiredKDSXMLFileJSON) {

        // validate the MAC address of the terminal
        if (!StringFunctions.stringHasContent(terminalMacAddr)) {
            Logger.logMessage("Unable to determine the MAC address of the terminal which invoked the method " +
                    "KitchenPrinterDataManager.sendEmailForExpiredKDSXML, unable to send an email which contains KDS XML order " +
                    "files which have been in the file share for too long!", KDSShareFolderMonitor.KDS_SHARE_MONITOR_LOG, Logger.LEVEL.ERROR);
            return false;
        }

        // validate the hostname of the terminal
        if (!StringFunctions.stringHasContent(terminalHostname)) {
            Logger.logMessage("Unable to determine the hostname of the terminal which invoked the method " +
                    "KitchenPrinterDataManager.sendEmailForExpiredKDSXML, unable to send an email which contains KDS XML order " +
                    "files which have been in the file share for too long!", KDSShareFolderMonitor.KDS_SHARE_MONITOR_LOG, Logger.LEVEL.ERROR);
            return false;
        }

        // validate the company name
        if (!StringFunctions.stringHasContent(companyName)) {
            Logger.logMessage("Unable to determine the name of the company who owns the terminal which invoked the method " +
                    "KitchenPrinterDataManager.sendEmailForExpiredKDSXML, unable to send an email which contains KDS XML order " +
                    "files which have been in the file share for too long!", KDSShareFolderMonitor.KDS_SHARE_MONITOR_LOG, Logger.LEVEL.ERROR);
            companyName = "[COMPANY UNKNOWN]";
        }

        // validate the expired XML files JSON
        if (!StringFunctions.stringHasContent(expiredKDSXMLFileJSON)) {
            Logger.logMessage("No KDS XML order files were passed to KitchenPrinterDataManager.sendEmailForExpiredKDSXML, unable to send " +
                    "an email which contains KDS XML order files which have been in the file share for too long!", KDSShareFolderMonitor.KDS_SHARE_MONITOR_LOG, Logger.LEVEL.ERROR);
            return false;
        }

        // convert the JSON String into ArrayList<String>
        Gson gson = new Gson();
        Type strArrayListType = new TypeToken<ArrayList<HashMap<String, String>>>(){}.getType();
        ArrayList<HashMap<String, String>> expiredKDSXMLFilePaths = gson.fromJson(expiredKDSXMLFileJSON, strArrayListType);
        if (DataFunctions.isEmptyCollection(expiredKDSXMLFilePaths)) {
            Logger.logMessage(String.format("Unable to convert the JSON String %s into an ArrayList<String> KitchenPrinterDataManager.sendEmailForExpiredKDSXML, unable to send " +
                    "an email which contains KDS XML order files which have been in the file share for too long!",
                    Objects.toString(expiredKDSXMLFileJSON, "N/A")), KDSShareFolderMonitor.KDS_SHARE_MONITOR_LOG, Logger.LEVEL.ERROR);
            return false;
        }

        // get the email list
        DataManager dataManager = new DataManager();
        int revenueCenterID = KDSEmailAlert.getTerminalsRevenueCenter(terminalMacAddr, terminalHostname, dataManager, KDSShareFolderMonitor.KDS_SHARE_MONITOR_LOG);
        ArrayList<String> emails = new ArrayList<>();
        if (revenueCenterID > 0) {
            emails = KDSEmailAlert.getEmailListForRC(revenueCenterID, dataManager, KDSShareFolderMonitor.KDS_SHARE_MONITOR_LOG);
        }
        if (DataFunctions.isEmptyCollection(emails)) {
            Logger.logMessage("No emails have been found to send the email to in KitchenPrinterDataManager.sendEmailForExpiredKDSXML, unable to send " +
                    "an email which contains KDS XML order files %s which have been in the file share for too long!", KDSShareFolderMonitor.KDS_SHARE_MONITOR_LOG, Logger.LEVEL.ERROR);
            return false;
        }

        boolean dontSpamHelpdesk = false;
        if (StringFunctions.stringHasContent(MMHProperties.getAppSetting("site.kdsFileMonitoring.dontSpamHelpdesk"))
                && (MMHProperties.getAppSetting("site.kdsFileMonitoring.dontSpamHelpdesk").equalsIgnoreCase("yes"))) {
            dontSpamHelpdesk = true;
        }

        if (!dontSpamHelpdesk) {
            // alert MMHayes personnel
            emails.add("QCLevel2@mmhayes.com");
        }

        // get the email configuration information
        HashMap emailConfig = KDSEmailAlert.getEmailConfig(dataManager, KDSShareFolderMonitor.KDS_SHARE_MONITOR_LOG);
        if (DataFunctions.isEmptyMap(emailConfig)) {
            Logger.logMessage(String.format("No back office email configuration has been found in KitchenPrinterDataManager.sendEmailForExpiredKDSXML, unable to send " +
                    "an email which contains KDS XML order files %s which have been in the file share for too long!",
                    Objects.toString(StringFunctions.buildStringFromList(expiredKDSXMLFilePaths, ","), "N/A")), KDSShareFolderMonitor.KDS_SHARE_MONITOR_LOG, Logger.LEVEL.ERROR);
            return false;
        }

        // get the actual email information
        String smtpServer = HashMapDataFns.getStringVal(emailConfig, "SMTPSERVER");
        String smtpUser = HashMapDataFns.getStringVal(emailConfig, "SMTPUSER");
        String smtpPassword = HashMapDataFns.getStringVal(emailConfig, "SMTPPASSWORD");
        if(!smtpPassword.isEmpty()) {
            smtpPassword = StringFunctions.decodePassword(smtpPassword).trim();
        }
        int smtpPort = HashMapDataFns.getIntVal(emailConfig, "SMTPPORT");
        String smtpFromAddress = HashMapDataFns.getStringVal(emailConfig, "SMTPFROMADDRESS");

        // get the email subject line
        String subject = KDSEmailAlert.buildKDSFileShareErrorSubjectLine(terminalHostname, companyName, KDSShareFolderMonitor.KDS_SHARE_MONITOR_LOG);

        // get the email body
        String body = KDSEmailAlert.buildKDSFileShareErrorEmailBody(subject, expiredKDSXMLFilePaths);

        // send the emails
        HashMap<String, Boolean> emailSentResults = new HashMap<>();
        for (String email : emails) {
            if (StringFunctions.stringHasContent(smtpUser)) {
                // send an authenticated email
                Logger.logMessage(String.format("Sending an authenticated email receipt to %s using the mail server %s and port %s in EmailedErrorReceipt.sendEmail.",
                        Objects.toString(email, "N/A"),
                        Objects.toString(smtpServer, "N/A"),
                        Objects.toString(smtpPort, "N/A")), KDSShareFolderMonitor.KDS_SHARE_MONITOR_LOG, Logger.LEVEL.TRACE);
                if (!MMHEmail.sendAuthEmail(smtpPort, smtpServer, smtpUser, smtpPassword, email, smtpFromAddress, subject, body, "text/html")) {
                    // if sending an authenticated email doesn't work try to send an unauthenticated email
                    emailSentResults.put(email, MMHEmail.sendEmail(smtpPort, smtpServer, email, smtpFromAddress, subject, body, "text/html"));
                }
                else {
                    // the authenticatred email was sent successfully
                    emailSentResults.put(email, true);
                }
            }
            else {
                // send an unauthenticated email
                Logger.logMessage(String.format("Sending an unauthenticated email receipt to %s using the mail server %s and port %s in EmailedErrorReceipt.sendEmail.",
                        Objects.toString(email, "N/A"),
                        Objects.toString(smtpServer, "N/A"),
                        Objects.toString(smtpPort, "N/A")), KDSShareFolderMonitor.KDS_SHARE_MONITOR_LOG, Logger.LEVEL.TRACE);
                emailSentResults.put(email, MMHEmail.sendEmail(smtpPort, smtpServer, email, smtpFromAddress, subject, body, "text/html"));
            }
        }

        // check whether or not all emails could be sent successfully
        boolean noFailure = true;
        if (!DataFunctions.isEmptyMap(emailSentResults)) {
            for (Map.Entry<String, Boolean> emailSentResultEntry : emailSentResults.entrySet()) {
                if (!emailSentResultEntry.getValue()) {
                    Logger.logMessage(String.format("Failed to send an email to %s at %s in KitchenPrinterDataManager.sendEmailForExpiredKDSXML",
                            Objects.toString(emailSentResultEntry.getKey(), "N/A"),
                            Objects.toString(LocalDateTime.now().format(DateTimeFormatter.ofPattern(MMHTimeFormatString.YR_MO_DY_HR_MIN_SEC)), "N/A")),
                            KDSShareFolderMonitor.KDS_SHARE_MONITOR_LOG, Logger.LEVEL.ERROR);
                    noFailure = false;
                }
            }
        }

        return noFailure;
    }

    public ArrayList getKOATerminalID (int paTransactionID) {
        DataManager dm = new DataManager();

        ArrayList queryRes = dm.parameterizedExecuteQuery("data.kitchenPrinter.getKOATerminalIDFromTransaction", new Object[]{paTransactionID}, true);

        if (!DataFunctions.isEmptyCollection(queryRes) && queryRes.size() == 1 && !DataFunctions.isEmptyMap((Map) queryRes.get(0))) {
            ArrayList returnList = new ArrayList();
            HashMap map = (HashMap) queryRes.get(0);
            int koaTerminalID = HashMapDataFns.getIntVal(map, "POSKIOSKTERMINALID");
            boolean sendToPrinterHost = HashMapDataFns.getBooleanVal(map, "RECORDTRANSACTIONSLOCALLY");

            returnList.add(koaTerminalID);
            returnList.add(sendToPrinterHost);

            return returnList;
        }

        return null;
    }

}