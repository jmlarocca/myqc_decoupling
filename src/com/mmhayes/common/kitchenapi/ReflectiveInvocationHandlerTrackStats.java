package com.mmhayes.common.kitchenapi;

import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.QCStats;
import redstone.xmlrpc.handlers.ReflectiveInvocationHandler;

/*
 $Author: mebrown $: Author of last commit
 $Date: 2015-02-10 14:58:46 -0500 (Tue, 10 Feb 2015) $: Date of last commit
 $Rev: 8550 $: Revision of last commit
 Notes:
*/

public class ReflectiveInvocationHandlerTrackStats extends ReflectiveInvocationHandler {
    public boolean returnCustomResponseCodes = true;

    public ReflectiveInvocationHandlerTrackStats(){

    }

    public Object invoke(String methodName, java.util.List arguments) throws Throwable {

        // for debug logging trace args
        if(Logger.LEVEL.DEBUG == Logger.currentLogLevel){
            StringBuilder s = new StringBuilder(methodName);
            s.append("(");
            for(Object o : arguments){
                s.append(o.toString());
                s.append(",");
            }
            s.setLength(s.length() - 1);
            s.append(")");
           Logger.logMessage(s.toString(), "", Logger.LEVEL.DEBUG);
        }
        QCStats s = QCStats.Create(this.getClass().getName() + "." + methodName);
        Object o = null;
        try {
            o = super.invoke(methodName, arguments);
        }
        catch (Exception y){
            s.ex(y);
            Logger.logException(methodName, "", y);
        }
        if(Logger.LEVEL.DEBUG == Logger.currentLogLevel){
            StringBuilder s2 = new StringBuilder(methodName);
            s2.append(" returned '");
            if(null == o)
                s2.append("null");
            else
                s2.append(o.toString());
            s2.append("'");
            Logger.logMessage(s2.toString(), "", Logger.LEVEL.DEBUG);
        }
        return s.trackStatsAndBuildResponse(o,returnCustomResponseCodes);
    }

}
