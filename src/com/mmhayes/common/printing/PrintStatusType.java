package com.mmhayes.common.printing;

/*
    Last Updated (automatically updated by SVN)
    $Author: jmkimber $: Author of last commit
    $Date: 2021-07-19 17:07:22 -0400 (Mon, 19 Jul 2021) $: Date of last commit
    $Rev: 14436 $: Revision of last commit
    Notes: Contains the possible print statuses.
*/

import com.mmhayes.common.kitchenPrinters.KitchenPrinterCommon;
import com.mmhayes.common.utils.Logger;

import java.util.Objects;

/**
 * <p>Contains the possible print statuses.</p>
 *
 */
public class PrintStatusType {

    public static final int WAITING = 2;
    public static final int PRINTED = 3;
    public static final int SENT = 4;
    public static final int DONE = 5;
    public static final int ERROR = 6;
    public static final int FAILEDLOCALPRINT = 8;
    public static final int DECLINEDLOCALREPRINT = 9;

    /**
     * <p>Gets the {@link String} value of the print status from the given print status ID.</p>
     *
     * @param printStatusID The print status ID to get the {@link String} value for.
     * @return The {@link String} value of the print status from the given print status ID.
     */
    public static String getFromIntValue (int printStatusID) {

        switch (printStatusID) {
            case 2:
                return "WAITING";
            case 3:
                return "PRINTED";
            case 4:
                return "SENT";
            case 5:
                return "DONE";
            case 6:
                return "ERROR";
            case 8:
                return "FAILEDLOCALPRINT";
            case 9:
                return "DECLINEDLOCALREPRINT";
            default:
                Logger.logMessage(String.format("Encountered an unknown print status ID of %s in PrintStatusType.getFromIntValue",
                        Objects.toString(printStatusID, "N/A")), Logger.LEVEL.ERROR);
                return "";
        }

    }

}