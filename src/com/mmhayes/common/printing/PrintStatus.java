package com.mmhayes.common.printing;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-06-08 11:35:33 -0400 (Mon, 08 Jun 2020) $: Date of last commit
    $Rev: 11927 $: Revision of last commit
    Notes: Contains information about the print status.
*/

import java.time.LocalDateTime;

/**
 * <p>Contains information about the print status.</p>
 *
 */
public class PrintStatus {

    // private member variables of a PrintStatus
    private int printStatusType = 0;
    private LocalDateTime updatedDTM = null;

    /**
     * <p>Constructor for a {@link PrintStatus}.</p>
     *
     */
    public PrintStatus () {}

    /**
     * <p>Sets the printStatusType field for {@link PrintStatus} and returns the {@link PrintStatus} instance.</p>
     *
     * @param printStatusType The print status.
     * @return The {@link PrintStatus} instance with it's printStatusType field set.
     */
    public PrintStatus addPrintStatusType (int printStatusType) {
        this.printStatusType = printStatusType;
        return this;
    }

    /**
     * <p>Sets the updatedDTM field for {@link PrintStatus} and returns the {@link PrintStatus} instance.</p>
     *
     * @param updatedDTM The {@link LocalDateTime} the print job was updated.
     * @return The {@link PrintStatus} instance with it's updatedDTM field set.
     */
    public PrintStatus addUpdatedDTM (LocalDateTime updatedDTM) {
        this.updatedDTM = updatedDTM;
        return this;
    }

    /**
     * <p>Getter for the printStatusType field of the {@link PrintStatus}.</p>
     *
     * @return The printStatusType field of the {@link PrintStatus}.
     */
    public int getPrintStatusType () {
        return printStatusType;
    }

    /**
     * <p>Setter for the printStatusType field of the {@link PrintStatus}.</p>
     *
     * @param printStatusType The printStatusType field of the {@link PrintStatus}.
     */
    public void setPrintStatusType (int printStatusType) {
        this.printStatusType = printStatusType;
    }

    /**
     * <p>Getter for the updatedDTM field of the {@link PrintStatus}.</p>
     *
     * @return The updatedDTM field of the {@link PrintStatus}.
     */
    public LocalDateTime getUpdatedDTM () {
        return updatedDTM;
    }

    /**
     * <p>Setter for the updatedDTM field of the {@link PrintStatus}.</p>
     *
     * @param updatedDTM The updatedDTM field of the {@link PrintStatus}.
     */
    public void setUpdatedDTM (LocalDateTime updatedDTM) {
        this.updatedDTM = updatedDTM;
    }

}