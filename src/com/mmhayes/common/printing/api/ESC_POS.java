package com.mmhayes.common.printing.api;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2021-05-24 12:52:41 -0400 (Mon, 24 May 2021) $: Date of last commit
    $Rev: 14019 $: Revision of last commit
    Notes: Contains ESC-POS constants and methods.
*/

import com.mmhayes.common.utils.DataFunctions;

public class ESC_POS {

//<editor-fold desc="ESC/POS Constants">
    public static final byte[] INIT_PRINTER = new byte[]{0x1B, 0x40};                               // ESC @, initialize the printer
    public static final byte[] LINE_FEED = new byte[]{0x0A};                                        // LF
    public static final byte[] CARRIAGE_RETURN = new byte[]{0x0D};                                  // CR
    public static final byte[] CUT_PAPER = new byte[]{0x1D, 0x56, 0x30};                            // GS V m, full paper cut
    public static final byte[] ENABLE_AUTO_STATUS_BACK = new byte[]{0x1D, 0x61, ((byte) 0x8E)};     // GS a n, passively return the printer's status
    public static final byte[] DISABLE_AUTO_STATUS_BACK = new byte[]{0x1D, 0x61, 0x00};             // GS a n, disable passively returning the printer's status
    public static final byte[] CHECK_DRAWER_KICK_STATUS = new byte[]{0x1D, 0x72, 0x32};             // GS r n, transmit the drawer kick pin status
//</editor-fold>

//<editor-fold desc="Automatic Status Back">
    public static boolean isASBStatus (byte[] resp) {
        return ((resp.length >= 4)
                && (DataFunctions.getBit(resp[0], 0) == 0)
                && (DataFunctions.getBit(resp[0], 1) == 0)
                && (DataFunctions.getBit(resp[0], 4) == 1)
                && (DataFunctions.getBit(resp[0], 7) == 0));
    }

    public static boolean isDrawerKickOutPinHigh (byte b) {
        return DataFunctions.getBit(b, 2) == 1;
    }

    public static boolean isOnline (byte b) {
        return DataFunctions.getBit(b, 3) == 1;
    }

    public static boolean isCoverOpen (byte b) {
        return DataFunctions.getBit(b, 5) == 1;
    }

    public static boolean isPaperBeingFed (byte b) {
        return DataFunctions.getBit(b, 6) == 1;
    }

    public static boolean isWaitingForOnlineRecovery (byte b) {
        return DataFunctions.getBit(b, 0) == 1;
    }

    public static boolean isPaperFeedBeingButtonPushed (byte b) {
        return DataFunctions.getBit(b, 1) == 1;
    }

    public static boolean isRecoverableError (byte b) {
        return DataFunctions.getBit(b, 2) == 1;
    }

    public static boolean isAutocutterError (byte b) {
        return DataFunctions.getBit(b, 3) == 1;
    }

    public static boolean isUnrecoverableError (byte b) {
        return DataFunctions.getBit(b, 5) == 1;
    }

    public static boolean isAutomaticallyRecoverableError (byte b) {
        return DataFunctions.getBit(b, 6) == 1;
    }

    public static boolean isPaperNearEnd (byte b) {
        return ((DataFunctions.getBit(b, 0) == 1) && (DataFunctions.getBit(b, 1) == 1));
    }

    public static boolean isPaperPresent (byte b) {
        return ((DataFunctions.getBit(b, 0) == 2) && (DataFunctions.getBit(b, 3) == 1));
    }
//</editor-fold>

}