package com.mmhayes.common.printing;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cjczyzewski on 6/8/2018.
 */
@Deprecated
public enum PrintJobStatusType {

    QUEUED               (1,  false, false, false), // Queued on the server, will be pulled by the print controller
    WAITING              (2,  false, false, false), // Waiting on terminal to be sent to print controller
    PRINTED              (3,  false, false, false), // Printed locally (not on kitchen printer)
    SENT                 (4,  true,  true,  false), // Sent to the print controller but not printed yet
    DONE                 (5,  true,  true,  true),  // Notified terminal receipt was printed by kitchen
    ERROR                (6,  true,  false, true),  // An error occurred, terminal should print this right away
    INPROGRESS           (7,  false, false, false), // printing of some kind is in progress, don't try to print again until this changes
    FAILEDLOCALPRINT     (8,  false, false, false), // local printing or a KP receipt failed
    DECLINEDLOCALREPRINT (9,  false, false, false), // The user has chosen not to retry printing a failed receipt
    SENTTOKDS            (10, true,  false, false); // Sent to KDS system that does not give further status reports

    private int typeID;
    private boolean requiresUpdateOnTerminal;
    private boolean requiresUpdateOnPrinterHost;
    private boolean requiresDateTimeModified;

    PrintJobStatusType(int typeID, boolean requiresUpdateOnTerminal, boolean requiresUpdateOnPrinterHost, boolean requiresDateTimeModified) {
        setTypeID(typeID);
        setRequiresUpdateOnTerminal(requiresUpdateOnTerminal);
        setRequiresUpdateOnPrinterHost(requiresUpdateOnPrinterHost);
        setRequiresDateTimeModified(requiresDateTimeModified);
    }

    public int getTypeID() {
        return typeID;
    }

    public void setTypeID(int typeID) {
        this.typeID = typeID;
    }

    public boolean isRequiresUpdateOnTerminal() {
        return requiresUpdateOnTerminal;
    }

    public void setRequiresUpdateOnTerminal(boolean requiresUpdateOnTerminal) {
        this.requiresUpdateOnTerminal = requiresUpdateOnTerminal;
    }

    public boolean isRequiresUpdateOnPrinterHost() {
        return requiresUpdateOnPrinterHost;
    }

    public void setRequiresUpdateOnPrinterHost(boolean requiresUpdateOnPrinterHost) {
        this.requiresUpdateOnPrinterHost = requiresUpdateOnPrinterHost;
    }

    public boolean isRequiresDateTimeModified() {
        return requiresDateTimeModified;
    }

    public void setRequiresDateTimeModified(boolean requiresDateTimeModified) {
        this.requiresDateTimeModified = requiresDateTimeModified;
    }

    public static List<PrintJobStatusType> getTypesThatRequireUpdatesOnTerminal() {
        List<PrintJobStatusType> results = new ArrayList<>();
        for (PrintJobStatusType type : PrintJobStatusType.values()) {
            if (type.isRequiresUpdateOnTerminal()) {
                results.add(type);
            }
        }
        return results;
    }

    public static List<PrintJobStatusType> getTypesThatRequireUpdatesOnPrinterHost() {
        List<PrintJobStatusType> results = new ArrayList<>();
        for (PrintJobStatusType type : PrintJobStatusType.values()) {
            if (type.isRequiresUpdateOnPrinterHost()) {
                results.add(type);
            }
        }
        return results;
    }

    public static PrintJobStatusType getFromID(int printStatusID) {
        for (PrintJobStatusType type : PrintJobStatusType.values()) {
            if (type.getTypeID() == printStatusID) {
                return type;
            }
        }
        return null;
    }
}
