package com.mmhayes.common.starbucks.SVdot;

/**
 * $Author: ecdyer $: Author of last commit
 * $Date: 2017-02-09 17:04:46 -0500 (Thu, 09 Feb 2017) $: Date of last commit
 * $Rev: 20495 $: Revision of last commit
 * Notes: Data manager for discount data manager
 */
public class SVdotResponseCode {
    private static String[]responseCodes;

    public static String getResponseMessage(String code){
        if(code == null)
            return "Missing Response Code";
        if(responseCodes == null)
            initResponseCodes();
        String message = "Unknown response code.";
        Integer intCode = Integer.parseInt(code);
        if(intCode >= 0 && intCode < responseCodes.length && responseCodes[intCode] != null)
            message = responseCodes[intCode];
        else
            message = "No description available.";

       return message;
    }

    private static void initResponseCodes(){
        responseCodes = new String[140];
        responseCodes[0] = "Completed OK.";
        responseCodes[1] = "Insufficient funds.";
        responseCodes[2] = "Account closed.";
        responseCodes[3] = "NOT IN USE (REMOVED).";
        responseCodes[4] = "Invalid or Inactive account.";
        responseCodes[5] = "Expired card.";
        responseCodes[6] = "Invalid transaction code.";
        responseCodes[7] = "Invalid merchant.";
        responseCodes[8] = "Already active.";
        responseCodes[9] = "Generic Decline.";
        responseCodes[10] = "Lost or stolen card.";
        responseCodes[11] = "Not lost or stolen.";
        responseCodes[12] = "Invalid transaction format.";

        //unspecified response codes...

        responseCodes[15] = "Bad mag stripe.";
        responseCodes[16] = "Incorrect location.";
        responseCodes[17] = "Max balance exceeded.";
        responseCodes[18] = "Invalid amount.";
        responseCodes[19] = "Invalid clerk.";
        responseCodes[20] = "Invalid password.";
        responseCodes[21] = "Invalid new password.";
        responseCodes[22] = "Exceeded account reloads.";
        responseCodes[23] = "Password retry exceeded.";

        //unspecified response codes...

        responseCodes[26] = "Incorrect transaction version or format number for POS transactions.";
        responseCodes[27] = "Request not permitted by this account.";
        responseCodes[28] = "Request not permitted by this merchant location.";
        responseCodes[29] = "Bad_replay_date.";
        responseCodes[30] = "Bad checksum. The checksum provided is incorrect.";
        responseCodes[31] = "Balance not available (denial).";
        responseCodes[32] = "Account locked.";
        responseCodes[33] = "No previous transaction.";
        responseCodes[34] = "Already reversed.";
        responseCodes[35] = "Generic denial.";
        responseCodes[36] = "Bad authorization code.";
        responseCodes[37] = "Too many transactions requested.";
        responseCodes[38] = "No transactions available/no more transactions available.";
        responseCodes[39] = "Transaction history not available.";
        responseCodes[40] = "New password required.";
        responseCodes[41] = "Invalid status change.";
        responseCodes[42] = "Void of activation after account activity.";
        responseCodes[43] = "No phone service.";
        responseCodes[44] = "Internet access disabled.";
        responseCodes[45] = "Invalid EAN or SCV.";
        responseCodes[46] = "Invalid merchant key.";
        responseCodes[47] = "Promotions for Internet Virtual and Physical cards do not match.";
        responseCodes[48] = "Invalid transaction source.";
        responseCodes[49] = "Account already linked.";
        responseCodes[50] = "Account not in inactive state.";
        responseCodes[51] = "First Data Voice Services returns this response on Internet transactions where the interface input parameter is not valid.";
        responseCodes[52] = "First Data Voice Services returns this response on Internet transactions where they did not receive a response from CLGC.";
        responseCodes[53] = "First Data Voice Services returns this response on Internet transactions where the client certificate is invalid.";
        responseCodes[54] = "Merchant not configured as International although the account requires it.";
        responseCodes[55] = "Invalid currency.";
        responseCodes[56] = "Request not International.";
        responseCodes[57] = "Currency conversion error.";
        responseCodes[58] = "Invalid Expiration Date.";
        responseCodes[59] = "The terminal transaction number did not match (on a void or reversal).";
        responseCodes[60] = "First Data Voice Services added a layer of validation that checks the data they receive from CLGC to make sure it is HTML friendly (i.e. no binary data).";

        //unspecified response codes...

        responseCodes[67] = "Target Embossed Card entered and Transaction Count entered are mismatched.";
        responseCodes[68] = "No Account Link.";
        responseCodes[69] = "Invalid Timezone.";
        responseCodes[70] = "Account On Hold.";
        responseCodes[71] = "Fraud Count Exceeded.";
        responseCodes[72] = "Promo Location Restricted.";
        responseCodes[73] = "Invalid BIN.";
        responseCodes[74] = "Product Code(s) Restricted.";
        responseCodes[75] = "Bad Post Date.";
        responseCodes[76] = "Void Lock Status Error.";
        responseCodes[77] = "Reloadable Already Active.";
        responseCodes[78] = "Account is Purged.";
        responseCodes[79] = "Transaction is declined as the Duplicate transaction.";
        responseCodes[80] = "Bulk Activation Error In Range.";
        responseCodes[81] = "Bulk Activation Not Attempted Because Error In Range.";
        responseCodes[82] = "Bulk Activation Package Amount Not Matched.";
        responseCodes[83] = "Location 0 invalid for third party aggregators.";
        responseCodes[84] = "Account Row Locked Response Code.";
        responseCodes[85] = "Timeout received, but not processed.";
        responseCodes[86] = "Incorrect PVC.";
        responseCodes[87] = "Provisioning limit exceeded, currently 10.";
        responseCodes[88] = "De-provisioning limit reached, current count is 0.";
        responseCodes[89] = "The EAN TYPE is not mentioned in manufacture_fa table.";
        responseCodes[90] = "Field SCV is required in the transaction.";
        responseCodes[91] = "Promo Code is not compatible for consortium code.";
        responseCodes[92] = "Product Restricted Declined.";
        responseCodes[94] = "Account not linked error";
        responseCodes[95] = "Account is in Watch status ";
        responseCodes[96] = "Account is in Fraud status";

        //unspecified response codes...

        responseCodes[127] = "Rule Failure";
        responseCodes[128] = "CED is more than 365 days from transaction date or less than current date.";
        responseCodes[129] = "Already 12 active LTO segment exist for an account.";
        responseCodes[130] = "Expired LTO segment amount is needed for success of transaction.";
        responseCodes[131] = "Transaction is not supported for LTO functionality.";
    }
}
