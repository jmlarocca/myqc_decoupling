package com.mmhayes.common.starbucks.SVdot;

/**
 * $Author: ecdyer $: Author of last commit
 * $Date: 2017-02-09 17:04:46 -0500 (Thu, 09 Feb 2017) $: Date of last commit
 * $Rev: 20495 $: Revision of last commit
 * Notes: Data manager for discount data manager
 */
@SVdotSerializable
public class SVdotFields {
    //required request fields
    public static final String transactionAmountCode = "04";
    @SVdotCode(code = "04")
    public String transactionAmount;
    public static final String localTransactionTimeCode = "12";
    @SVdotCode(code = "12")
    public String localTransactionTime;
    public static final String localTransactionDateCode = "13";
    @SVdotCode(code = "13")
    public String localTransactionDate;
    public static final String terminalTransactionNumberCode = "15";
    @SVdotCode(code = "15")
    public String terminalTransactionNumber;
    public static final String trackIIDataCode = "35";
    @SVdotCode(code = "35")
    public String trackIIData;
    public static final String merchantNumberAndTerminalIDCode = "42";
    @SVdotCode(code = "42")
    public String merchantNumberAndTerminalID;
    public static final String alternateMerchantNumberCode = "44";
    @SVdotCode(code = "44")
    public String alternateMerchantNumber;
    public static final String postDateCode = "53";
    @SVdotCode(code = "53")
    public String postDate;
    public static final String clerkIDCode = "62";
    @SVdotCode(code = "62")
    public String clerkID;
    public static final String embossedCardNumberCode = "70";
    @SVdotCode(code = "70")
    public String embossedCardNumber;
    public static final String localCurrencyCode = "C0";
    @SVdotCode(code = "C0")
    public String localCurrency;
    public static final String transactionCountCode = "CF";
    @SVdotCode(code = "CF")
    public String transactionCount;
    public static final String targetEmbossedCardNumberCode = "ED";
    @SVdotCode(code = "ED")
    public String targetEmbossedCardNumber;

    //Required Response fields

    public static final String systemTraceNumberCode = "11";
    @SVdotCode(code = "11")
    public String systemTraceNumber;
    public static final String authorizationCodeCode = "38";
    @SVdotCode(code = "38")
    public String authorizationCode;
    public static final String responseCodeCode = "39";
    @SVdotCode(code = "39")
    public String responseCode;

    public static final String previousBalanceCode = "75";
    @SVdotCode(code = "75")
    public String previousBalance;
    public static final String newBalanceCode = "76";
    @SVdotCode(code = "76")
    public String newBalance;
    public static final String localLockAmountCode = "78";
    @SVdotCode(code = "78")
    public String localLockAmount;
    public static final String expirationDateCode = "A0";
    @SVdotCode(code = "A0")
    public String expirationDate;
    public static final String cardClassCode = "B0";
    @SVdotCode(code = "B0")
    public String cardClass;
    public static final String originalTransactionRequestCode = "F6";
    @SVdotCode(code = "F6")
    public String originalTransactionRequest;
}
