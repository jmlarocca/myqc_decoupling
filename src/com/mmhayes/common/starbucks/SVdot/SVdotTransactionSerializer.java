package com.mmhayes.common.starbucks.SVdot;

import com.mmhayes.common.starbucks.GiftCardRequestModel;
import com.mmhayes.common.starbucks.StarbucksResponseModel;
import com.mmhayes.common.utils.Logger;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * $Author: ecdyer $: Author of last commit
 * $Date: 2017-02-09 17:04:46 -0500 (Thu, 09 Feb 2017) $: Date of last commit
 * $Rev: 20495 $: Revision of last commit
 * Notes: Data manager for discount data manager
 */
public class SVdotTransactionSerializer {
    public static final String SV = "SV.";
    public static final String VERSION = "4";
    public static final String FORMAT = "0";
    public static final char CHAR_DELIM = 28;//"1C";//field Separator
    public static final String STRING_DELIM = "1C";//field Separator
    public static final String USD = "840";//ISO 4217 for USD
    public static final String TRANSACTION_TYPE_APP_CHECK = "7400";

//    public static String serializeActivation(GiftCardModel card, String MID, String altMID, String terminalNumber){
//
//        return null;
//    }
//
//    public static StarbucksResponseModel deserializeActivation(String payload){
//        StarbucksResponseModel response = new StarbucksResponseModel();
//
//
//        return  response;
//    }

    public static char[] serializeTimoutReversal(String originalPayload){
        //replace the original code with the reversal, and
        if(originalPayload == null || originalPayload.length() < 21){
            Logger.logMessage("Reversal of payload with no data is not possible. Payload: " + originalPayload, Logger.LEVEL.ERROR);
            return null;
        }
        String originalCode = originalPayload.substring(17, 21);
        originalPayload = originalPayload.replace(
                CHAR_DELIM + VERSION + FORMAT + originalCode + CHAR_DELIM,
                CHAR_DELIM + VERSION + FORMAT + "0704" + CHAR_DELIM);
        StringBuilder b = new StringBuilder();
        b.append(originalPayload);
        b.append(CHAR_DELIM);
        b.append("F6");
        b.append(originalCode);
        String newPayload = b.toString();
        Logger.logMessage("Generated reversed payload: " + newPayload);
        return newPayload.toCharArray();
    }

    public static String serializeConnectionCheck(String MID, String altMID, String terminalNumber){
        SVdotFields fields = new SVdotFields();
        //Set the fields that we need for this transaction type
        Date instant = new Date(System.currentTimeMillis());
        SimpleDateFormat time = new SimpleDateFormat("hhmmss");
        SimpleDateFormat date = new SimpleDateFormat("MMddYYYY");
        fields.localTransactionTime = time.format(instant);//12  HHMMSS
        fields.localTransactionDate = date.format(instant);//13  MMDDCCYYY
        fields.alternateMerchantNumber = altMID;//44
        fields.merchantNumberAndTerminalID = MID + terminalNumber;//42

        String payload = addRequestHeader(MID, TRANSACTION_TYPE_APP_CHECK, serializeFields(fields));
        return payload;
    }

    public static String serializeRequest(GiftCardRequestModel card, String transactionTypeID, String MID, String altMID, String terminalNumber){
        SVdotFields fields = new SVdotFields();
        fields.localCurrency = USD;

        if(card.getTransactionAmount() != null){
            //this must be done in the "smallest currency unit" ie penndies
            BigDecimal pennies = card.getTransactionAmount().scaleByPowerOfTen(2);//get it in pennies
            int amount = pennies.intValue();
            fields.transactionAmount = amount + "";
        }

        //Set the fields that we need for this transaction type
        //If the timestamp is not supplied, use right now
        if(card.getTimeStamp() == null || card.getTimeStamp().length() == 0)
            card.setTimeStamp(System.currentTimeMillis() + "");
        Date instant = new Date(Long.parseLong(card.getTimeStamp()));
        SimpleDateFormat time = new SimpleDateFormat("hhmmss");
        SimpleDateFormat date = new SimpleDateFormat("MMddYYYY");
        fields.localTransactionTime = time.format(instant);//12  HHMMSS
        fields.localTransactionDate = date.format(instant);//13  MMDDCCYYY
        //if the post date is not specified, dont use it?
        if(card.getPostDate() != null && card.getPostDate().length() > 0){
            //card.setPostDate(System.currentTimeMillis() + "");
            fields.postDate = date.format(new Date(Long.parseLong(card.getPostDate())));
        }
        fields.alternateMerchantNumber = altMID;//44
        fields.merchantNumberAndTerminalID = MID + terminalNumber;//42
        if(card.getEmbossedCardNumber() != null){
            fields.embossedCardNumber = card.getEmbossedCardNumber();//70
        }else if(card.getTrackIIData() != null){
            String data = card.getTrackIIData();
            if(data.length() >= 39)//start and end sentinals included
                data = data.substring(1, 38);
            fields.trackIIData = data;

        }else{
            throw new RuntimeException("Card not specified");
        }
        String payload = addRequestHeader(MID, transactionTypeID, serializeFields(fields));
        return payload;
    }

    public static StarbucksResponseModel deserializeResponse(char[] payload){
        StarbucksResponseModel response = new StarbucksResponseModel();
        String responseCode = getResponseCode(payload); //this might just always be 0900?
        String payloadString = removeResponseHeader(payload);
        SVdotFields fields = deserializeFields(payloadString);

        response.responseNumber = fields.systemTraceNumber;
        response.authCode = fields.authorizationCode;
        response.responseCode = fields.responseCode;
        response.responseMessage = SVdotResponseCode.getResponseMessage(fields.responseCode);
        response.merchantNumberAndTerminalID = fields.merchantNumberAndTerminalID;
        response.storeNumber = fields.alternateMerchantNumber;
        response.cardNumber = fields.embossedCardNumber;
        response.expirationDate = fields.expirationDate;
        response.cardClass = fields.cardClass;
        response.localCurrency = fields.localCurrency;

        //These fields are in pennies

        if(fields.previousBalance != null)
            response.previousBalance = (Double.parseDouble(fields.previousBalance) / 100) + "";
        if(fields.newBalance != null)
            response.newBalance = (Double.parseDouble(fields.newBalance) / 100) + "";
        if(fields.localLockAmount != null)
            response.lockedAmount = (Double.parseDouble(fields.localLockAmount) / 100) + "";
        return response;
    }

//------------------Private helper methods--------------------------------------\\



    /**
     * Adds the request header to a field payload
     * @param merchantID
     * @param transactionCode
     * @param fieldsPayload
     * @return
     */
    private static String addRequestHeader(String merchantID, String transactionCode, String fieldsPayload){
        return SV + merchantID + CHAR_DELIM + VERSION + FORMAT + transactionCode + fieldsPayload;
    }


    /**
     * removes the response header so that only fields are left
     * @param payload
     * @return
     */
    private static String removeResponseHeader(char[] payload){
        int len = payload.length;
        StringBuilder builder = new StringBuilder();
//        if(payload[0] == '1'){
            for(int i = 7; i < len; i++){
                builder.append(payload[i]);
            }
//        }else if(payload[0] == '4'){
//            for(int i = 6; i < len; i++){
//                builder.append(payload[i]);
//            }
//        }
        return builder.toString();//payload.substring(6, payload.length());
    }

    /**
     * Gets the repsonse code from a payload
     * @param payload
     * @return
     */
    private static String getResponseCode(char[] payload){
//        if(payload[0] == '1'){
            //format 10(FS)0900
            StringBuilder builder = new StringBuilder();
            for(int i = 3; i < 7; i++){
                builder.append(payload[i]);
            }
            return builder.toString();
//        }
//        else if (payload[0] == '4'){
//            //format 100900
//            StringBuilder builder = new StringBuilder();
//            for(int i = 2; i < 6; i++){
//                builder.append(payload[i]);
//            }
//            return builder.toString();
//        }
//        return null;
    }

    private static String serializeFields(SVdotFields model){
        StringBuilder builder = new StringBuilder();
        Field[] fields = SVdotFields.class.getFields();

        for(Field f: fields){
            f.setAccessible(true);
            try {
                if(f.get(model) != null && f.isAnnotationPresent(SVdotCode.class)){
                    //this requires the model's fields to be the same as the SVDot...
                    String code = f.getAnnotation(SVdotCode.class).code();//GiftCardModel.class.getField(f.getName() + "Code").get(model).toString();
                    String data = f.get(model).toString();
                    builder.append(CHAR_DELIM);
                    builder.append(code);//code;
                    builder.append(data);//data
                    Logger.logMessage("Serialized Field: " + code + " with " + data, Logger.LEVEL.DEBUG);
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        return builder.toString();
    }


    /**
     * Transforms the payload fields into a readable format
     * @param payloadFields
     * @return
     */
    private static SVdotFields deserializeFields(String payloadFields){
        String[] fields = payloadFields.split(CHAR_DELIM + "");
        SVdotFields parsed = new SVdotFields();
        for(String field : fields){
            if(field.length() <= 2)
                continue;//the value is blank if it is just a field name or blank
            //IDs are always 2 characters long
            String fieldID = field.substring(0,2);
            String data = field.substring(2, field.length());
            //TODO: use reflection
            switch (fieldID){
                case SVdotFields.transactionAmountCode:
                    parsed.transactionAmount = data;
                    break;
                case SVdotFields.localTransactionTimeCode:
                    parsed.localTransactionTime = data;
                    break;
                case SVdotFields.localTransactionDateCode:
                    parsed.localTransactionDate= data;
                    break;
                case SVdotFields.terminalTransactionNumberCode:
                    parsed.terminalTransactionNumber= data;
                    break;
                case SVdotFields.trackIIDataCode:
                    parsed.trackIIData= data;
                    break;
                case SVdotFields.merchantNumberAndTerminalIDCode:
                    parsed.merchantNumberAndTerminalID= data;
                    break;
                case SVdotFields.alternateMerchantNumberCode:
                    parsed.alternateMerchantNumber= data;
                    break;
                case SVdotFields.postDateCode:
                    parsed.postDate= data;
                    break;
                case SVdotFields.clerkIDCode:
                    parsed.clerkID= data;
                    break;
                case SVdotFields.embossedCardNumberCode:
                    parsed.embossedCardNumber= data;
                    break;
                case SVdotFields.localCurrencyCode:
                    parsed.localCurrency= data;
                    break;
                case SVdotFields.transactionCountCode:
                    parsed.transactionCount= data;
                    break;
                case SVdotFields.targetEmbossedCardNumberCode:
                    parsed.targetEmbossedCardNumber= data;
                    break;

                //responce required fields
                case SVdotFields.systemTraceNumberCode:
                    parsed.systemTraceNumber= data;
                    break;
                case SVdotFields.authorizationCodeCode:
                    parsed.authorizationCode= data;
                    break;
                case SVdotFields.responseCodeCode:
                    parsed.responseCode= data;
                    break;
                case SVdotFields.previousBalanceCode:
                    parsed.previousBalance= data;
                    break;
                case SVdotFields.newBalanceCode:
                    parsed.newBalance= data;
                    break;
                case SVdotFields.localLockAmountCode:
                    parsed.localLockAmount= data;
                    break;
                case SVdotFields.expirationDateCode:
                    parsed.expirationDate= data;
                    break;
                case SVdotFields.cardClassCode:
                    parsed.cardClass= data;
                    break;
                case SVdotFields.originalTransactionRequestCode:
                    parsed.originalTransactionRequest= data;
                    break;
                default:
                    Logger.logMessage("Unhandled SVdot Field: " + fieldID + " with data: " + data);
                    break;
            }
        }
        return parsed;
    }

}
