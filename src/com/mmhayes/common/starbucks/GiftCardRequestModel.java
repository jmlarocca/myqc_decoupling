package com.mmhayes.common.starbucks;

/**
 * $Author: ecdyer $: Author of last commit
 * $Date: 2017-02-09 17:04:46 -0500 (Thu, 09 Feb 2017) $: Date of last commit
 * $Rev: 20495 $: Revision of last commit
 * Notes: Data manager for discount data manager
 */
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.giftcards.GiftCardSetupModel;

import java.math.BigDecimal;

public class GiftCardRequestModel {
    private Integer terminalID;//used to query the DB
    private String clerkID;
    private Integer giftCardSetupID;
    private BigDecimal transactionAmount;//this should come in as "9.99"
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    private String uniqueTransactionId;//used to generate the client Ref
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    private String terminalVersion;//Version on QCPOS running on the Terminal
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    private String trackIIData;//37 Char long ex: 60105 X YYYY YYYY 5=000100040000 V 0 WWZZZZ where the card number is VWWX YYYY YYYY ZZZZ
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    private String embossedCardNumber;//16 digit account number and checksum
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    private String timeStamp;
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    private String postDate;
    private GiftCardSetupModel giftCardSetup;

    public Integer getTerminalID() {
        return terminalID;
    }

    public void setTerminalID(Integer terminalID) {
        this.terminalID = terminalID;
    }

    public String getClerkID() {
        return clerkID;
    }

    public void setClerkID(String clerkID) {
        this.clerkID = clerkID;
    }

    public Integer getGiftCardSetupID() {
        return giftCardSetupID;
    }

    public void setGiftCardSetupID(Integer giftCardSetupID) {
        this.giftCardSetupID = giftCardSetupID;
    }

    public String getUniqueTransactionId() {
        return uniqueTransactionId;
    }

    public void setUniqueTransactionId(String uniqueTransactionId) {
        this.uniqueTransactionId = uniqueTransactionId;
    }

    public BigDecimal getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(BigDecimal transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public String getTrackIIData() {
        return trackIIData;
    }

    public void setTrackIIData(String trackIIData) {
        this.trackIIData = trackIIData;
    }

    public String getEmbossedCardNumber() {
        return embossedCardNumber;
    }

    public void setEmbossedCardNumber(String embossedCardNumber) {
        this.embossedCardNumber = embossedCardNumber;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getPostDate() {
        return postDate;
    }

    public void setPostDate(String postDate) {
        this.postDate = postDate;
    }

    public String getTerminalVersion() {
        return terminalVersion;
    }

    public void setTerminalVersion(String terminalVersion) {
        this.terminalVersion = terminalVersion;
    }

    public GiftCardSetupModel getGiftCardSetup() {
        return giftCardSetup;
    }

    public void setGiftCardSetup(GiftCardSetupModel giftCardSetup) {
        this.giftCardSetup = giftCardSetup;
    }
}
