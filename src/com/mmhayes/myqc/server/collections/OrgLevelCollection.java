package com.mmhayes.myqc.server.collections;

import com.mmhayes.common.api.CommonAuthResource;
import com.mmhayes.common.api.errorhandling.exceptions.InvalidAuthException;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.myqc.server.models.OrgLevelModel;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mmhayes on 9/25/2020.
 */
public class OrgLevelCollection {

    List<OrgLevelModel> collection = new ArrayList<>();
    private static DataManager dm = new DataManager();
    private Integer loggedInEmployeeID = null;

    public OrgLevelCollection(){

    }

    public OrgLevelCollection(HttpServletRequest request) throws Exception{
        setLoggedInEmployeeID(CommonAuthResource.checkRequestHeaderForMMHayesAuth(request));

        populateCollection(request);

    }

    public OrgLevelCollection populateCollection(HttpServletRequest request) throws Exception {

        if(getLoggedInEmployeeID() == null){
            throw new InvalidAuthException();
        }

        ArrayList<HashMap> orgLevelList = dm.parameterizedExecuteQuery("data.orgLevel.getActiveLocations", new Object[]{}, true);

        if(orgLevelList != null && orgLevelList.size() > 0){
            for(HashMap orgLevelHM : orgLevelList){
                // Only return org levels with stores available for the user
                ArrayList<HashMap> orderMethodCounts = dm.parameterizedExecuteQuery("data.ordering.getStoresByOrgLevelId_orderMethodCounts",
                        new Object[] {
                                getLoggedInEmployeeID(),
                                orgLevelHM.get("ORGLEVELPRIMARYID")
                        }, true
                );
                if (orderMethodCounts != null && orderMethodCounts.size() > 0 && orderMethodCounts.get(0) != null) {
                    Object storeCount = orderMethodCounts.get(0).get("STORECOUNT");
                    Object pickupCount = orderMethodCounts.get(0).get("PICKUPCOUNT");
                    Object deliveryCount = orderMethodCounts.get(0).get("DELIVERYCOUNT");
                    if (storeCount instanceof Integer && (Integer)storeCount > 0) {
                        // Set whether the org level has stores available for pickup/delivery for the user
                        boolean pickupAvailable = false;
                        boolean deliveryAvailable = false;
                        if (pickupCount instanceof Integer && (Integer)pickupCount > 0) {
                            pickupAvailable = true;
                        }
                        if (deliveryCount instanceof Integer && (Integer)deliveryCount > 0) {
                            deliveryAvailable = true;
                        }
                        orgLevelHM.put("PICKUPAVAILABLE", pickupAvailable);
                        orgLevelHM.put("DELIVERYAVAILABLE", deliveryAvailable);

                        OrgLevelModel orgLevel = new OrgLevelModel(orgLevelHM);
                        getCollection().add(orgLevel);
                    }
                }
            }
        }

        return this;
    }


    public List<OrgLevelModel> getCollection() {
        return collection;
    }

    public void setCollection(List<OrgLevelModel> collection) {
        this.collection = collection;
    }

    public Integer getLoggedInEmployeeID() {
        return loggedInEmployeeID;
    }

    public void setLoggedInEmployeeID(Integer loggedInEmployeeID) {
        this.loggedInEmployeeID = loggedInEmployeeID;
    }
}
