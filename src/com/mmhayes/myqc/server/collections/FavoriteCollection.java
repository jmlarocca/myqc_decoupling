package com.mmhayes.myqc.server.collections;

//mmhayes dependencies

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.CommonAuthResource;
import com.mmhayes.common.api.ModelPaginator;
import com.mmhayes.common.api.errorhandling.exceptions.InvalidAuthException;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.myqc.server.models.*;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;
import java.math.BigDecimal;
import java.util.*;

//myqc dependencies
//other dependencies

/* Favorite Collection
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2020-07-28 12:12:15 -0400 (Tue, 28 Jul 2020) $: Date of last commit
 $Rev: 50508 $: Revision of last commit

 Notes: Collection of Favorite Models
*/
public class FavoriteCollection {
    List<FavoriteModel> favoriteCollection = new ArrayList<FavoriteModel>();
    List<FavoriteModel> recentCollection = new ArrayList<FavoriteModel>();
    List<FavoriteModel> popularCollection = new ArrayList<FavoriteModel>();
    List<FavoriteModel> favoriteUnavailableCollection = new ArrayList<FavoriteModel>();
    List<FavoriteModel> collection = new ArrayList<FavoriteModel>();
    ModelPaginator modelPaginator = null;
    HashMap collectionHM = new HashMap();
    String terminalID = null; //This is actually representing the TerminalID and NOT the Store (MyQCTerminalID)
    Integer storeID = null;
    private static DataManager dm = new DataManager();
    private Integer loggedInEmployeeID = null;
    ArrayList<String> allProductsInStoreList = new ArrayList<>();
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String allProductsInStore = "";
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String allModifiersInStore = "";

    //constructor - populates the collection
    public FavoriteCollection(HashMap<String, LinkedList> paginationParamsHM, UriInfo uriInfo, HttpServletRequest request) throws Exception {

        //determine the employeeID from the request
        setLoggedInEmployeeID(CommonAuthResource.checkRequestHeaderForMMHayesAuth(request));

        //determine the terminalID from the MyQCTerminalID in request
        String myQCTerminalID = CommonAPI.checkRequestHeaderForStoreID(request).toString();
        String terminalID = FavoriteModel.getTerminalFromMyQCTerminalID(myQCTerminalID);
        setTerminalID(terminalID);
        setStoreID(Integer.parseInt(myQCTerminalID));

        MultivaluedMap<String, String> httpGetParams = uriInfo.getQueryParameters();
        String productList = httpGetParams.get("productList").toString();
        productList = productList.replace("[", "");
        productList = productList.replace("]", "");
        setAllProductsInStore(productList);

        ModelPaginator modelPaginator = new  ModelPaginator(paginationParamsHM);
        setModelPaginator(modelPaginator);
    }


    //convert the product list string into an array
    public ArrayList<String> getProductsSplitToArray() {
        ArrayList<String> productList = new ArrayList<>();
        if(!getAllProductsInStore().equals("")) {
            productList = new ArrayList<String>(Arrays.asList(getAllProductsInStore().split(",")));
        }
        return productList;
    }

    //populates the favorite collection with favorite models
    public FavoriteCollection populateFavoriteCollection(HttpServletRequest request) throws Exception {

        //throw InvalidAuthException if no employeeID or terminalID is found
        if (getLoggedInEmployeeID() == null || getTerminalID() == null) {
            throw new InvalidAuthException();
        }

        //use the store model to get the Store Keypad Model from the cache to get the number of products that can be popular
        StoreModel storeModel = StoreModel.getStoreModel(getStoreID(), request);
        StoreKeypadModel storeKeypadModel = StoreKeypadModel.getStoreKeypadModel(storeModel);
        String popularProducts = storeKeypadModel.getPopularProducts();

        modelPaginator.setModelCount(0);

        //set the modelCount so we can finalize the pagination properties
        ArrayList<HashMap> availableFavorites = getFavoriteAvailableCount();
        if(availableFavorites != null) {
            modelPaginator.setModelCount(availableFavorites.size());
        }

        //determines, sets and validates pagination properties
        modelPaginator.finalizePaginationProperties();

        String searchByFavoriteIds = "";
        if(modelPaginator.getModelCount() > 0) {
            Integer startingIndex = modelPaginator.getStartingModelIndex();
            Integer endingIndex = modelPaginator.getEndingModelIndex();

            if( availableFavorites != null && endingIndex > availableFavorites.size() ) {
                endingIndex = availableFavorites.size();
            }

            List<HashMap> favoriteIDs = availableFavorites.subList(startingIndex, endingIndex);
            List<String> favoriteIDList = new ArrayList<>();
            for(HashMap favoriteHM : favoriteIDs) {
                String favoriteID = CommonAPI.convertModelDetailToString(favoriteHM.get("ORDERINGFAVORITEID"));
                favoriteIDList.add(favoriteID);
            }
            searchByFavoriteIds = String.join(",", favoriteIDList);
        }

        //get a single page of models in an array list
        ArrayList<HashMap> favoriteList = dm.parameterizedExecuteQuery("data.ordering.getEmployeeFavorites",
                new Object[]{
                        searchByFavoriteIds,
                        popularProducts
                },
                true
        );

        //populate this collection from an array list of hashmaps
        if (favoriteList != null && favoriteList.size() > 0) {
            for (HashMap favoriteHM : favoriteList) {

                Integer orderFavoriteID = CommonAPI.convertModelDetailToInteger(favoriteHM.get("ID"));
                Integer modifierID = CommonAPI.convertModelDetailToInteger(favoriteHM.get("MODID"));

                if(modifierID != null) {
                    FavoriteModel existingFavoriteModel = getExistingFavoriteModel(orderFavoriteID, getFavoriteCollection());

                    //if this is the first modifier to be added
                    if(existingFavoriteModel == null) {
                        existingFavoriteModel = new FavoriteModel(favoriteHM);

                        existingFavoriteModel.setAvailable(true);
                        existingFavoriteModel.setModifierModel(favoriteHM);

                        getFavoriteCollection().add(existingFavoriteModel);

                    } else {
                        existingFavoriteModel.setModifierModel(favoriteHM);
                    }

                    continue;
                }

                FavoriteModel favoriteModel = new FavoriteModel(favoriteHM);
                favoriteModel.setAvailable(true);
                getFavoriteCollection().add(favoriteModel);
            }
        }
        return this;
    }

    public FavoriteModel getExistingFavoriteModel(Integer favoriteID, List<FavoriteModel> collection) {
        for(FavoriteModel favoriteModel : collection) {
            if(favoriteModel.getID().equals(favoriteID)) {
                return favoriteModel;
            }
        }
        return null;
    }

    //get the count of favorite products that are available to the currently logged in employee for this terminal/store
    private ArrayList<HashMap> getFavoriteAvailableCount() {
        ArrayList<HashMap> favoriteList = dm.parameterizedExecuteQuery("data.ordering.getEmployeeFavoritesAvailable_count",
                new Object[]{
                        getTerminalID(),
                        getLoggedInEmployeeID(),
                        getAllProductsInStore()
                },
                true
        );

        return favoriteList;
    }

    //populates the unavailable favorite collection with favorite models
    public FavoriteCollection populateUnavailableCollection() throws Exception {

        //throw InvalidAuthException if no employeeID or terminalID is found
        if (getLoggedInEmployeeID() == null || getTerminalID() == null) {
            throw new InvalidAuthException();
        }

        //use the store model to get the Store Keypad Model from the cache to get the number of products that can be popular
        StoreModel storeModel = StoreModel.getStoreModel(getStoreID(), getLoggedInEmployeeID());
        StoreKeypadModel storeKeypadModel = StoreKeypadModel.getStoreKeypadModel(storeModel);
        String popularProducts = storeKeypadModel.getPopularProducts();

        modelPaginator.setModelCount(0);

        //set the modelCount so we can finalize the pagination properties
        ArrayList<HashMap> unavailableFavorites = getFavoriteUnavailableCount();
        if(unavailableFavorites != null) {
            modelPaginator.setModelCount(unavailableFavorites.size());
        }

        //determines, sets and validates pagination properties
        modelPaginator.finalizePaginationProperties();

        String searchByFavoriteIds = "";
        if(modelPaginator.getModelCount() > 0) {
            Integer startingIndex = modelPaginator.getStartingModelIndex();
            Integer endingIndex = modelPaginator.getEndingModelIndex();

            if( unavailableFavorites != null && endingIndex > unavailableFavorites.size() ) {
                endingIndex = unavailableFavorites.size();
            }

            if(startingIndex > endingIndex) {
                return this;
            }

            List<HashMap> favoriteIDs = unavailableFavorites.subList(startingIndex, endingIndex);
            List<String> favoriteIDList = new ArrayList<>();
            for(HashMap favoriteHM : favoriteIDs) {
                String favoriteID = CommonAPI.convertModelDetailToString(favoriteHM.get("ORDERINGFAVORITEID"));
                favoriteIDList.add(favoriteID);
            }
            searchByFavoriteIds = String.join(",", favoriteIDList);
        }

        //get a single page of models in an array list
        ArrayList<HashMap> unavailableFavoriteList = dm.parameterizedExecuteQuery("data.ordering.getEmployeeUnavailableFavorites",
                new Object[]{
                        searchByFavoriteIds,
                        popularProducts
                },
                true
        );

        //populate this collection from an array list of hashmaps
        if (unavailableFavoriteList != null && unavailableFavoriteList.size() > 0) {
            for (HashMap favoriteHM : unavailableFavoriteList) {
                Integer orderFavoriteID = CommonAPI.convertModelDetailToInteger(favoriteHM.get("ID"));
                Integer modifierID = CommonAPI.convertModelDetailToInteger(favoriteHM.get("MODID"));

                if(modifierID != null) {
                    FavoriteModel existingFavoriteModel = getExistingFavoriteModel(orderFavoriteID, getFavoriteUnavailableCollection());

                    //if this is the first modifier to be added
                    if(existingFavoriteModel == null) {
                        existingFavoriteModel = new FavoriteModel(favoriteHM);

                        existingFavoriteModel.setAvailable(false);
                        existingFavoriteModel.setModifierModel(favoriteHM);

                        getFavoriteUnavailableCollection().add(existingFavoriteModel);

                    } else {
                        existingFavoriteModel.setModifierModel(favoriteHM);
                    }

                    continue;
                }

                FavoriteModel favoriteModel = new FavoriteModel(favoriteHM);
                favoriteModel.setAvailable(false);
                getFavoriteUnavailableCollection().add(favoriteModel);
            }
        }
        return this;
    }

    //get the count of favorite products that are not available to the currently logged in employee for this terminal/store
    private ArrayList<HashMap> getFavoriteUnavailableCount() {
        ArrayList<HashMap> orders = getFavoriteAvailableCount();
        String ignoreFavoriteIDs = "";

        if(orders.size() > 0) {
            ArrayList<String> orderStrList = new ArrayList<>();
            for(HashMap order : orders) {
                orderStrList.add(CommonAPI.convertModelDetailToString(order.get("ORDERINGFAVORITEID")));
            }
            ignoreFavoriteIDs = String.join(",", orderStrList);
        }


        ArrayList<HashMap> unavailableFavoriteList = dm.parameterizedExecuteQuery("data.ordering.getEmployeeFavoritesUnavailable_count",
                new Object[]{
                        getTerminalID(),
                        getLoggedInEmployeeID(),
                        ignoreFavoriteIDs
                },
                true
        );

        return unavailableFavoriteList;
    }

    //populates the popular collection with favorite models
    public FavoriteCollection populatePopularCollection(HttpServletRequest request) throws Exception {
        //throw InvalidAuthException if no employeeID or terminalID is found
        if (getLoggedInEmployeeID() == null || getTerminalID() == null) {
            throw new InvalidAuthException();
        }

        //set the modelCount so we can finalize the pagination properties
        modelPaginator.setModelCount(getPopularCount());

        //determines, sets and validates pagination properties
        modelPaginator.finalizePaginationProperties();

        String popularCollectionEndingIndex = determinePopularProductCount(request);

        //get a single page of models in an array list
        ArrayList<HashMap> popularList = dm.parameterizedExecuteQuery("data.ordering.getPopularProducts",
                new Object[]{
                        getTerminalID(),
                        getLoggedInEmployeeID(),
                        modelPaginator.getStartingModelIndex(),
                        popularCollectionEndingIndex,
                        getAllProductsInStore()
                },
                true
        );

        //populate this collection from an array list of hashmaps
        if (popularList != null && popularList.size() > 0) {
            for (HashMap popularHM : popularList) {
                getPopularCollection().add(new FavoriteModel(popularHM));
            }
        }

        return this;
    }

    //get the count of the popular products in the store's revenue center
    private Integer getPopularCount() {
        Integer popularCount = 0;

        ArrayList<HashMap> popularList = dm.parameterizedExecuteQuery("data.ordering.getPopularProducts_count",
                new Object[]{
                        getTerminalID(),
                        getAllProductsInStore()
                },
                true
        );

        if (popularList != null) {
            HashMap popularHM = popularList.get(0);
            popularCount = Integer.parseInt(popularHM.get("COUNT").toString());
        }
        return popularCount;
    }

    public String determinePopularProductCount(HttpServletRequest request) throws Exception{

        Integer storeID = CommonAPI.checkRequestHeaderForStoreID(request);
        StoreModel storeModel = StoreModel.getStoreModel(storeID, request);
        BigDecimal percentOfPopularProds = storeModel.getPercentOfPopularProducts();

        if ( percentOfPopularProds != null ) {
            BigDecimal percentOfPopularProducts = (percentOfPopularProds).divide(BigDecimal.valueOf(100));

            //get number of products in the store
            BigDecimal productsInStore = BigDecimal.valueOf(getProductsSplitToArray().size());

            //get the number of products in the store that can be popular
            BigDecimal popularProductsCount = productsInStore.multiply(percentOfPopularProducts).setScale(0, BigDecimal.ROUND_HALF_UP);

            //if the number of popular products is greater or equal to the number of products that will be shown, then show those products
            // Example: If 350 out of 1000 products are popular but the ending index is 20, return 20 so we only show that many at a time
            if(popularProductsCount.intValue() >= modelPaginator.getEndingModelIndex()) {
                return modelPaginator.getEndingModelIndex().toString();
            }

            //otherwise if the popular products is less than the ending index, the popular product count will be the ending index
            return popularProductsCount.toString();
        }

        return modelPaginator.getEndingModelIndex().toString();
    }

    //getter
    public List<FavoriteModel> getFavoriteCollection() {
        return this.favoriteCollection;
    }

    //getter
    public List<FavoriteModel> getRecentCollection() {
        return this.recentCollection;
    }

    //getter
    public List<FavoriteModel> getPopularCollection() {
        return this.popularCollection;
    }

    //getter
    public List<FavoriteModel> getFavoriteUnavailableCollection() {
        return this.favoriteUnavailableCollection;
    }

    //getter
    public List<FavoriteModel> getCollection() {
        return this.collection;
    }

    //setter
    public void setCollection(List<FavoriteModel> collection) {
        this.collection = collection;
    }

    //getter
    private Integer getLoggedInEmployeeID() {
        return this.loggedInEmployeeID;
    }

    //setter
    private void setLoggedInEmployeeID(Integer loggedInEmployeeID) {
        this.loggedInEmployeeID = loggedInEmployeeID;
    }

    public String getTerminalID() {
        return terminalID;
    }

    public void setTerminalID(String terminalID) {
        this.terminalID = terminalID;
    }

    public HashMap getCollectionHM() {
        return collectionHM;
    }

    public void setCollectionHM(HashMap collectionHM) {
        this.collectionHM = collectionHM;
    }

    public ModelPaginator getModelPaginator() {
        return modelPaginator;
    }

    public void setModelPaginator(ModelPaginator modelPaginator) {
        this.modelPaginator = modelPaginator;
    }

    public String getAllProductsInStore() {
        return allProductsInStore;
    }

    public void setAllProductsInStore(String allProductsInStore) {
        this.allProductsInStore = allProductsInStore;
    }

    public String getAllModifiersInStore() {
        return allModifiersInStore;
    }

    public void setAllModifiersInStore(String allModifiersInStore) {
        this.allModifiersInStore = allModifiersInStore;
    }

    public ArrayList<String> getAllProductsInStoreList() {
        return allProductsInStoreList;
    }

    public void setAllProductsInStoreList(ArrayList<String> allProductsInStoreList) {
        String allProductsInStoreStr = String.join(",", allProductsInStoreList);
        setAllProductsInStore(allProductsInStoreStr);

        this.allProductsInStoreList = allProductsInStoreList;
    }

    public void addProductToProductList(String productID) {
        ArrayList<String> allProductsInStoreList = this.getAllProductsInStoreList();

        allProductsInStoreList.add(productID);

        setAllProductsInStoreList(allProductsInStoreList);
    }

    public Integer getStoreID() {
        return storeID;
    }

    public void setStoreID(Integer storeID) {
        this.storeID = storeID;
    }

}
