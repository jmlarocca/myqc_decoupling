package com.mmhayes.myqc.server.collections;

//mmhayes dependencies

import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.CommonAuthResource;
import com.mmhayes.common.api.errorhandling.exceptions.InvalidAuthException;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.commonMMHFunctions;
import com.mmhayes.myqc.server.models.FeatureModel;
import com.mmhayes.myqc.server.models.HealthyIndicatorFeatureModel;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

//myqc dependencies
//other dependencies

/* Feature Collection
 Last Updated (automatically updated by SVN)
 $Author: ijgerstein $: Author of last commit
 $Date: 2021-09-21 11:07:25 -0400 (Tue, 21 Sep 2021) $: Date of last commit
 $Rev: 58178 $: Revision of last commit

 Notes: Collection of Account Detail Models
*/
public class FeatureCollection {
    List<FeatureModel> collection = new ArrayList<>();
    private static DataManager dm = new DataManager();
    private Integer loggedInEmployeeID = null;
    private String clientVersion = null;
    private static commonMMHFunctions commFunc = new commonMMHFunctions();
    private static final int ACCOUNT_FREEZE_FEATURE_ID = 6;
    private static final int ONLINE_ORDERING_FEATURE_ID = 7;
    private static final int REWARDS_FEATURE_ID = 9;
    private static final int QUICK_PAY_FEATURE_ID = 10;
    private static final int FAQ_FEATURE_ID = 11;
    private static final int FEEDBACK_FEATURE_ID = 12;

    //constructor - populates the collection
    public FeatureCollection(HttpServletRequest request) throws Exception {

        //determine the employeeID from the request
        setLoggedInEmployeeID(CommonAuthResource.checkRequestHeaderForMMHayesAuth(request));

        //grabs the client version from the request header, sets it on the logged in account's record and returns it
        setClientVersion(CommonAPI.checkRequestHeaderForClientVersion(request, getLoggedInEmployeeID()));

        //populate this collection with models
        populateCollection(request, false);

    }

    //constructor - populates the collection if using branding
    public FeatureCollection(HttpServletRequest request, Boolean isUsingBranding) throws Exception {

        //populate this collection with models
        populateCollection(request, isUsingBranding);

    }

    //populates this collection with models
    public FeatureCollection populateCollection(HttpServletRequest request, Boolean isUsingBranding) throws Exception {

        //throw InvalidAuthException if no employeeID is found
        if (!isUsingBranding && getLoggedInEmployeeID() == null) {
            throw new InvalidAuthException();
        }


        ArrayList<HashMap> featureSettings = commFunc.getMyQCConfigurableFeatures(request);

        String showFreeze = "no";
        String showOnlineOrdering = "no";
        String showRewards = "no";
        String showQuickPay = "no";
        String showFAQs = "no";
        String showFeedback = "no";
        for ( HashMap featureSetting : featureSettings ) {
            Integer featureID = CommonAPI.convertModelDetailToInteger(featureSetting.get("MYQCFEATUREID"));
            String featureSettingStr = CommonAPI.convertModelDetailToInteger(featureSetting.get("ACTIVE")) == 1 ? "yes" : "no";

            switch (featureID) {
                case ACCOUNT_FREEZE_FEATURE_ID: showFreeze = featureSettingStr; break;
                case ONLINE_ORDERING_FEATURE_ID: showOnlineOrdering = featureSettingStr; break;
                case REWARDS_FEATURE_ID: showRewards = featureSettingStr; break;
                case QUICK_PAY_FEATURE_ID: showQuickPay = featureSettingStr; break;
                case FAQ_FEATURE_ID: showFAQs = featureSettingStr; break;
                case FEEDBACK_FEATURE_ID: showFeedback = featureSettingStr; break;
            }
        }

        //this is the version of the API and should be incremented for builds!!
        String APIVersion = "6.4.0";

        //client version should always be greater than or the same as the APIVersion
        if ((!isUsingBranding && CommonAPI.verifyVersionIsSameOrGreater(getClientVersion(), APIVersion)) || isUsingBranding) {

            //API Version Feature - Added in version 1.0.0
            FeatureModel apiVersionFeatureModel = new FeatureModel("apiversion", APIVersion, "yes");
            getCollection().add(apiVersionFeatureModel);

            //Self Service Feature - Added in version 1.0.0 - This includes all the original page views (deductions, balances, etc.)
            FeatureModel selfServiceCoreFeatureModel = new FeatureModel("selfservicecore", "N/A", "yes");
            getCollection().add(selfServiceCoreFeatureModel);

            //Account Freeze Feature - Added in version 1.0.3
            FeatureModel accountFreezeFeatureModel = new FeatureModel("accountfreeze", "N/A", showFreeze);
            getCollection().add(accountFreezeFeatureModel);

            //Online Ordering Feature - Added in version 1.1.0
            FeatureModel onlineOrderingFeatureModel = new FeatureModel("onlineordering", "N/A", showOnlineOrdering);
            getCollection().add(onlineOrderingFeatureModel);

            //Loyalty Reward Feature - Added in version 1.3.0
            FeatureModel loyaltyRewardsFeatureModel = new FeatureModel("rewards", "N/A", showRewards);
            getCollection().add(loyaltyRewardsFeatureModel);

            //Account Funding Feature - added in 1.2
            FeatureModel accountFundingFeatureModel = new FeatureModel("funding", "N/A", "yes");
            getCollection().add(accountFundingFeatureModel);

            //Quick Pay Feature - added in 1.3
            FeatureModel quickPayFeatureModel = new FeatureModel("quickpay", "N/A", showQuickPay);
            getCollection().add(quickPayFeatureModel);

            //FAQ Feature - added in 4.0.41
            FeatureModel faqFeatureModel = new FeatureModel("faq", "N/A", showFAQs);
            getCollection().add(faqFeatureModel);

            //Leave Feedback Feature - added in 6.1
            FeatureModel feedbackFeatureModel = new FeatureModel("feedback", "N/A", showFeedback);
            getCollection().add(feedbackFeatureModel);

            HealthyIndicatorFeatureModel healthyIndicatorFeatureModel = new HealthyIndicatorFeatureModel("healthyIndicatorTour", "N/A", "no", showOnlineOrdering);
            getCollection().add(healthyIndicatorFeatureModel);

            //added in 1.4 - had to add for determining which slides should show in the tour when the app is opened before entering the code
            if(isUsingBranding) {
                for ( HashMap featureSetting : featureSettings ) {
                    String name = CommonAPI.convertModelDetailToString(featureSetting.get("NAME"));
                    String using = "yes";
                    if(CommonAPI.convertModelDetailToString(featureSetting.get("ACTIVE")).equals("0")) {
                        using = "no";
                    }
                    FeatureModel brandingFeatureModel = new FeatureModel(name+"Tour", "N/A", using);
                    getCollection().add(brandingFeatureModel);
                }
            }

        } else {
            //API Version Feature - Added in version 1.0.0
            FeatureModel apiVersionFeatureModel = new FeatureModel("apiversion", APIVersion, "no");
            getCollection().add(apiVersionFeatureModel);
        }

        return this;
    }

    public HashMap getHealthyIconInfo() {
        return null;
    }

    //getter
    public List<FeatureModel> getCollection() {
        return this.collection;
    }

    //getter
    private Integer getLoggedInEmployeeID() {
        return this.loggedInEmployeeID;
    }

    //setter
    private void setLoggedInEmployeeID(Integer loggedInEmployeeID) {
        this.loggedInEmployeeID = loggedInEmployeeID;
    }

    //getter
    private String getClientVersion() {
        return this.clientVersion;
    }

    //setter
    private void setClientVersion(String clientVersion) {
        this.clientVersion = clientVersion;
    }


}
