package com.mmhayes.myqc.server.collections;

//mmhayes dependencies

import com.mmhayes.common.api.CommonAuthResource;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.login.LoginSettingsModel;
import com.mmhayes.myqc.server.api.MyQCCache;
import com.mmhayes.myqc.server.models.FAQModel;
import com.mmhayes.myqc.server.models.PrepOptionModel;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

//myqc dependencies
//other dependencies

/* Prep Option Collection
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2020-05-27 12:00:01 -0400 (Wed, 27 May 2020) $: Date of last commit
 $Rev: 49238 $: Revision of last commit

 Notes: Collection of FAQs - A FAQ Set
*/
public class FAQCollection {
    List<FAQModel> collection = new ArrayList<FAQModel>();
    private Integer authenticatedAccountID = null;
    private static DataManager dm = new DataManager();

    //constructor - populates the collection
    public FAQCollection() throws Exception {

        //populate this collection with models
        populateLoginFAQCollection();
    }


    //constructor - populates the collection
    public FAQCollection(Integer faqSetID) throws Exception {

        //populate this collection with models
        populateCollection(faqSetID);
    }

    //populates this collection with models
    public FAQCollection populateCollection(Integer faqSetID) throws Exception {

        //get all models in an array list
        ArrayList<HashMap> faqList = dm.parameterizedExecuteQuery("data.faq.getFAQs",
                new Object[]{
                        faqSetID
                },
                true
        );

        //populate this collection from an array list of hashmaps
        if (faqList != null && faqList.size() > 0) {
            for (HashMap faqHM : faqList) {
                this.getCollection().add( new FAQModel( faqHM ) );
            }
        }

        return this;
    }

    //populates this collection with models
    public FAQCollection populateLoginFAQCollection() throws Exception {
        LoginSettingsModel loginSettingsModel = null;

        //if store is in cache, load from cache
        if ( MyQCCache.checkCacheForLoginSettings() ) {
            loginSettingsModel = MyQCCache.getLoginSettingsCache();
        } else {
            loginSettingsModel = CommonAuthResource.retrieveLoginSettings();
        }

        if(loginSettingsModel.getLoginFAQSetID() == null) {
            return this;
        }

        //get all models in an array list
        ArrayList<HashMap> faqList = dm.parameterizedExecuteQuery("data.faq.getLoginFAQs", new Object[]{loginSettingsModel.getLoginFAQSetID()}, true);

        //populate this collection from an array list of hashmaps
        if (faqList != null && faqList.size() > 0) {
            for (HashMap faqHM : faqList) {
                this.getCollection().add( new FAQModel( faqHM ) );
            }
        }

        return this;
    }


    //getter
    public List<FAQModel> getCollection() {
        return this.collection;
    }

    public Integer getAuthenticatedAccountID() {
        return authenticatedAccountID;
    }

    public void setAuthenticatedAccountID(Integer authenticatedAccountID) {
        this.authenticatedAccountID = authenticatedAccountID;
    }

}
