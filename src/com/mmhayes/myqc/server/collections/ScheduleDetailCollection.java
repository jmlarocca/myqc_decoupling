package com.mmhayes.myqc.server.collections;

//mmhayes dependencies

import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.myqc.server.models.ScheduleDetailModel;
import com.mmhayes.myqc.server.models.ScheduleModel;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

//myqc dependencies
//other dependencies

/* Store Collection
Last Updated (automatically updated by SVN)
$Author: gematuszyk $: Author of last commit
$Date: 2019-04-08 16:16:41 -0400 (Mon, 08 Apr 2019) $: Date of last commit
$Rev: 37702 $: Revision of last commit

Notes: Collection of Store Models
*/
public class ScheduleDetailCollection {
    private List<ScheduleDetailModel> collection = new ArrayList<ScheduleDetailModel>();
    private static DataManager dm = new DataManager();

    //constructor - populates the collection
    public ScheduleDetailCollection() throws Exception {
    }

    public ScheduleDetailCollection(Integer scheduleID, Integer scheduleTypeID, LocalDateTime referenceTime) throws Exception {
        String sqlProp = "data.ordering.getScheduleDetails";

        //for a buffering schedule, grab additional detail information
        if ( scheduleTypeID.equals( ScheduleModel.ORDER_BUFFERING_SCHEDULE ) ) {
            sqlProp = "data.ordering.getBufferScheduleDetails";
        }

        //get all records in an array list
        ArrayList<HashMap> scheduleDetailList = dm.parameterizedExecuteQuery(sqlProp,
            new Object[]{
                scheduleID
            },
            true
        );

        //populate this collection from an array list of hashmaps
        if ( scheduleDetailList == null || scheduleDetailList.size() == 0  ) {
            Logger.logMessage("Could not find schedule details for schedule ID: " + scheduleID, Logger.LEVEL.WARNING);
            return;
        }

        for ( HashMap scheduleDetailHM : scheduleDetailList ) {
            //determine that response is valid
            if ( scheduleDetailHM.get("ID") == null || scheduleDetailHM.get("ID").toString().isEmpty() ) {
                Logger.logMessage("Could not determine scheduleDetailID in ScheduleDetailCollection constructor by ID", Logger.LEVEL.WARNING);
                continue;
            }

            scheduleDetailHM.put("SCHEDULETYPEID", scheduleTypeID);
            ScheduleDetailModel scheduleDetailModel = new ScheduleDetailModel( scheduleDetailHM );

            //check that the schedule detail is open for the reference time based on day of week
            String dayOfWeek = referenceTime.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US).toLowerCase();

            //if this schedule detail is not open for this day of the week, skip considering it, other schedule type's days are checked later
            if ( scheduleTypeID.equals( ScheduleModel.ORDER_BUFFERING_SCHEDULE ) && !scheduleDetailModel.checkDayOfWeek( dayOfWeek ) ) {
                continue;
            }

            getCollection().add( scheduleDetailModel );
        }
    }

    public HashMap<String, ArrayList<String>> getOpenCloseTimes(LocalDateTime referenceTime) {
        HashMap prepTimes = new HashMap();
        ArrayList<String> openTimes = new ArrayList<>();
        ArrayList<String> closeTimes = new ArrayList<>();

        //convert localDateTime to LocalTime for easier comparison
        LocalTime reference = referenceTime.toLocalTime();

        for ( ScheduleDetailModel scheduleDetail : getCollection() ) {
            //check that the schedule detail is open for the reference time based on day of week
            String dayOfWeek = referenceTime.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US).toLowerCase();

            //if this schedule detail is not open for this day of the week, skip considering it
            if ( !scheduleDetail.checkDayOfWeek( dayOfWeek ) ) {
                continue;
            }

            //get the end time as localDateTime for comparison
            LocalTime scheduleDetailEndTime = LocalTime.parse( scheduleDetail.getScheduleEndTime() );

            //when set to 11:59, gets returned as 00:00
            if ( scheduleDetailEndTime.getHour() == 0 && scheduleDetailEndTime.getMinute() == 0 ) {
                scheduleDetailEndTime = LocalTime.MAX;
            }

            //check that the reference time is before schedule detail's end time
            if ( !reference.isBefore( scheduleDetailEndTime ) ) {
                continue;
            }

            //add the scheduleDetail's start time to a list of openTimes
            openTimes.add( scheduleDetail.getScheduleStartTime() );

            //add the scheduleDetail's end time to a list of closeTimes
            closeTimes.add( scheduleDetail.getScheduleEndTime() );
        }

        prepTimes.put( "OPENTIMES", openTimes );
        prepTimes.put( "CLOSETIMES", closeTimes );

        return prepTimes;
    }

    public HashMap getOrderingTimes(LocalDateTime referenceTime) {
        HashMap orderingTimes = new HashMap();

        LocalTime orderingOpenDTM = null;
        LocalTime orderingCloseDTM = null;

        //convert localDateTime to LocalTime for easier comparison
        LocalTime reference = referenceTime.toLocalTime();

        for ( ScheduleDetailModel scheduleDetail : getCollection() ) {
            //check that the schedule detail is open for the reference time based on day of week
            String dayOfWeek = referenceTime.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US).toLowerCase();

            //if this schedule detail is not open for this day of the week, skip considering it
            if ( !scheduleDetail.checkDayOfWeek( dayOfWeek ) ) {
                continue;
            }

            //get the start and end times as localDateTimes for comparison
            LocalTime scheduleDetailStartTime = LocalTime.parse( scheduleDetail.getScheduleStartTime() );
            LocalTime scheduleDetailEndTime = LocalTime.parse( scheduleDetail.getScheduleEndTime() );

            //when set to 11:59, gets returned as 00:00
            if ( scheduleDetailEndTime.getHour() == 0 && scheduleDetailEndTime.getMinute() == 0 ) {
                scheduleDetailEndTime = LocalTime.MAX;
            }

            //check that the reference time falls between the schedule detail's start and end time
            if ( !( reference.isAfter( scheduleDetailStartTime ) && reference.isBefore( scheduleDetailEndTime ) ) ) {
                continue;
            }

            //if the orderingOpen time is not yet set or the start time for this schedule detail is before the current opening time, set this schedule detail's start time as the opening time
            if ( orderingOpenDTM == null || scheduleDetailStartTime.isBefore( orderingOpenDTM ) ) {
                orderingOpenDTM = scheduleDetailStartTime;
            }

            //if the orderingOpen time is not yet set or the end time for this schedule detail is after the current closing time, set this schedule detail's end time as the closing time
            if ( orderingCloseDTM == null || scheduleDetailStartTime.isAfter( orderingCloseDTM ) ) {
                orderingCloseDTM = scheduleDetailEndTime;
            }
        }

        String orderingOpenTime = "";
        if ( orderingOpenDTM != null ) {
            orderingOpenTime = orderingOpenDTM.format(DateTimeFormatter.ofPattern("HH:mm"));
        }

        String orderingCloseTime = "";
        if ( orderingCloseDTM != null ) {
            orderingCloseTime = orderingCloseDTM.format(DateTimeFormatter.ofPattern("HH:mm"));
        }

        orderingTimes.put( "OPENTIME", orderingOpenTime );
        orderingTimes.put( "CLOSETIME", orderingCloseTime );

        return orderingTimes;
    }


    //getter
    public List<ScheduleDetailModel> getCollection() {
        return this.collection;
    }

    public void setCollection(List<ScheduleDetailModel> collection) {
        this.collection = collection;
    }
}
