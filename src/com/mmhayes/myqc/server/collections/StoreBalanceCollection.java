package com.mmhayes.myqc.server.collections;

//mmhayes dependencies
import com.mmhayes.common.api.*;
import com.mmhayes.common.dataaccess.*;
import com.mmhayes.common.utils.*;
import com.mmhayes.common.api.errorhandling.exceptions.*;

//myqc dependencies
import com.mmhayes.myqc.server.models.*;

//other dependencies
import javax.servlet.http.HttpServletRequest;
import java.util.*;

/* Store Balance Collection
 Last Updated (automatically updated by SVN)
 $Author: jmdottavio $: Author of last commit
 $Date: 2016-06-10 08:55:11 -0400 (Fri, 10 Jun 2016) $: Date of last commit
 $Rev: 16513 $: Revision of last commit

 Notes: Collection of Store Balance Models
*/
public class StoreBalanceCollection {
    List<StoreBalanceModel> collection = new ArrayList<StoreBalanceModel>();
    private static DataManager dm = new DataManager();
    private Integer loggedInEmployeeID = null;
    protected ArrayList<HashMap> terminalGroupList = null;

    //constructor - populates the collection
    public StoreBalanceCollection(Integer terminalGroupID, HttpServletRequest request) throws Exception {

        //ensure terminalGroupID was passed in
        if (terminalGroupID == null) {
            throw new MissingDataException("Could not determine Terminal Group.");
        }

        //determine the employeeID from the request
        setLoggedInEmployeeID(CommonAuthResource.checkRequestHeaderForMMHayesAuth(request));

        //populate this collection with models
        populateCollection(terminalGroupID);

    }

    //constructor - populates the collection
    public StoreBalanceCollection(ArrayList<HashMap> terminalGroupList, Integer terminalGroupID, HttpServletRequest request) throws Exception {

        //determine the employeeID from the request
        setLoggedInEmployeeID(CommonAuthResource.checkRequestHeaderForMMHayesAuth(request));

        //populate the collection from the passed in terminal list
        populateCollectionFromList(terminalGroupList, terminalGroupID);

    }

    //populates this collection with models
    public StoreBalanceCollection populateCollection(Integer terminalGroupID) throws Exception {

        //ensure terminalGroupID was passed in
        if (terminalGroupID == null) {
            throw new MissingDataException("Could not determine Terminal Group.");
        }

        //throw InvalidAuthException if no employeeID is found
        if (getLoggedInEmployeeID() == null) {
            throw new InvalidAuthException();
        }

        //get all models in an array list
        ArrayList<HashMap> storeBalanceList = dm.parameterizedExecuteQuery("data.myqc.getEmployeeBalanceForTerminalGroup",
                new Object[]{
                    getLoggedInEmployeeID(),
                     terminalGroupID
                },
                true
        );

        //iterate through list, create models and add them to the collection
        if (storeBalanceList != null && storeBalanceList.size() > 0) {
            for (HashMap storeBalanceHM : storeBalanceList) {
                getCollection().add(new StoreBalanceModel(storeBalanceHM));
            }
        }

        return this;
    }

    //populates this collection with models
    public StoreBalanceCollection populateCollectionFromList(ArrayList<HashMap> terminalGroupList, Integer activeTerminalGroupID) throws Exception {

        //ensure terminalGroupList was passed in and is valid
        if (terminalGroupList == null || terminalGroupList.size() == 0) {
            throw new MissingDataException("Could not determine valid Terminal Group List.");
        }

        //throw InvalidAuthException if no employeeID is found
        if (getLoggedInEmployeeID() == null) {
            throw new InvalidAuthException();
        }

        //iterate through the terminalGroupList and add the records that are in the list that are for the activeTerminalGroupID...
            //...then remove those entries from the list and store the list
        for (Iterator<HashMap> terminalGroupIterator = terminalGroupList.iterator(); terminalGroupIterator.hasNext();) {
            HashMap terminalGroupHM = terminalGroupIterator.next();
            if (terminalGroupHM.get("TERMINALGROUPID") != null && activeTerminalGroupID == Integer.parseInt(terminalGroupHM.get("TERMINALGROUPID").toString())) {
                getCollection().add(new StoreBalanceModel(terminalGroupHM));
                terminalGroupIterator.remove();
            }
        }

        //set what remains of the terminalGroupList so the callee can move to the next activeTerminalGroupID
        setTerminalGroupList(terminalGroupList);

        return this;
    }

    //getter
    public List<StoreBalanceModel> getCollection() {
        return this.collection;
    }

    //getter
    private Integer getLoggedInEmployeeID() {
        return this.loggedInEmployeeID;
    }

    //setter
    private void setLoggedInEmployeeID(Integer loggedInEmployeeID) {
        this.loggedInEmployeeID = loggedInEmployeeID;
    }

    //getter
    protected ArrayList<HashMap> getTerminalGroupList() {
        return this.terminalGroupList;
    }

    //setter
    protected void setTerminalGroupList(ArrayList<HashMap> terminalGroupList) {
        this.terminalGroupList = terminalGroupList;
    }

}
