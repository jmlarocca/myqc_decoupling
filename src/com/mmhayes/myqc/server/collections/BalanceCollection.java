package com.mmhayes.myqc.server.collections;

//mmhayes dependencies
import com.mmhayes.common.api.*;
import com.mmhayes.common.dataaccess.*;
import com.mmhayes.common.utils.*;
import com.mmhayes.common.api.errorhandling.exceptions.*;

//myqc dependencies
import com.mmhayes.myqc.server.models.*;

//other dependencies
import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.math.BigDecimal;

/* Balance Collection
 Last Updated (automatically updated by SVN)
 $Author: prsmith $: Author of last commit
 $Date: 2020-12-04 17:09:42 -0500 (Fri, 04 Dec 2020) $: Date of last commit
 $Rev: 53112 $: Revision of last commit

 Notes: Collection of Balance Models
*/
public class BalanceCollection {
    List<BalanceModel> collection = new ArrayList<BalanceModel>();
    private static DataManager dm = new DataManager();
    private commonMMHFunctions commFunc = new commonMMHFunctions();
    private Integer loggedInEmployeeID = null;

    //constructor - populates the collection
    public BalanceCollection(HttpServletRequest request) throws Exception {

        //determine the employeeID from the request
        setLoggedInEmployeeID(CommonAuthResource.checkRequestHeaderForMMHayesAuth(request));

        //populate this collection with models
        populateCollection(request);

    }

    //populates this collection with models
    public BalanceCollection populateCollection(HttpServletRequest request) throws Exception {

        //throw InvalidAuthException if no employee is found
        if (getLoggedInEmployeeID() == null) {
            throw new InvalidAuthException();
        }

        //get all models in an array list
        ArrayList<HashMap> terminalGroupList = dm.parameterizedExecuteQuery("data.myqc.getEmployeeBalancesByTerminalGroup",
                new Object[]{
                    getLoggedInEmployeeID()
                },
                true
        );

        //iterate through list, create models and add them to the collection
        while (terminalGroupList != null && terminalGroupList.size() > 0) {
            HashMap terminalGroupHM = terminalGroupList.get(0);
            if (terminalGroupHM.get("TERMINALGROUPID") != null && !terminalGroupHM.get("TERMINALGROUPID").toString().isEmpty()) {
                Integer terminalGroupID = Integer.parseInt(terminalGroupHM.get("TERMINALGROUPID").toString());

                //create a storeBalanceCollection and populate the collection for one specific terminalGroupID
                StoreBalanceCollection storeBalanceCollection = new StoreBalanceCollection(terminalGroupList, terminalGroupID, request);

                //get the modified terminalGroupList from the storeBalanceCollection and reset the storeBalanceCollection's terminalGroupList
                terminalGroupList = storeBalanceCollection.getTerminalGroupList();
                storeBalanceCollection.setTerminalGroupList(null);

                //create a new balance model and add to the balance collection
                getCollection().add(new BalanceModel(terminalGroupHM, storeBalanceCollection));
            } else {
                terminalGroupList.remove(0);
            }
        }

        return this;
    }

    //retrieves the global balance information for the logged in employee
    //TODO: (MED) - Method is using an old error handling method -jrmitaly - 6/7/2016
    public HashMap<String, Object> getGlobalBalanceInfo() {
        HashMap<String, Object> globalBalanceHM = new HashMap<String, Object>();
        try {
            if (getLoggedInEmployeeID() != null) {

                //add lastSynced to the globalBalanceHM
                globalBalanceHM.put("lastSynced", commFunc.getLastSyncDateTimeForAccount(getLoggedInEmployeeID()));

                //get logged in employees global balance
                ArrayList<HashMap> globalBalanceList = dm.parameterizedExecuteQuery("data.myqc.getEmployeeGlobalBalance",
                        new Object[]{
                            getLoggedInEmployeeID()
                        },
                        true
                );

                //populate this collection from an array list of hashmaps
                if (globalBalanceList != null && globalBalanceList.size() == 1 && globalBalanceList.get(0) != null) {
                    HashMap<String, Object> tempGlobalBalanceHM = globalBalanceList.get(0);
                    if (tempGlobalBalanceHM != null) {

                        //if global limit is not null and is not empty then set as the "limit"
                        if (tempGlobalBalanceHM.get("GLOBALLIMIT") != null && !tempGlobalBalanceHM.get("GLOBALLIMIT").toString().isEmpty()) {
                            globalBalanceHM.put("limit", new BigDecimal(tempGlobalBalanceHM.get("GLOBALLIMIT").toString()));
                        }

                        //if global balance is not null and is not empty then set as the "balance"
                        if (tempGlobalBalanceHM.get("GLOBALBALANCE") != null && !tempGlobalBalanceHM.get("GLOBALAVAILABLE").toString().isEmpty()) {
                            globalBalanceHM.put("balance", new BigDecimal(tempGlobalBalanceHM.get("GLOBALBALANCE").toString()));
                        }

                        //if global limit and global available are not null and are not empty then set global available as the "avail"
                        if (tempGlobalBalanceHM.get("GLOBALAVAILABLE") != null && !tempGlobalBalanceHM.get("GLOBALAVAILABLE").toString().isEmpty() &&
                                tempGlobalBalanceHM.get("GLOBALLIMIT") != null && !tempGlobalBalanceHM.get("GLOBALLIMIT").toString().isEmpty()) {
                            BigDecimal tmpGlobalLimit = new BigDecimal(tempGlobalBalanceHM.get("GLOBALLIMIT").toString());
                            BigDecimal oneMillion = new BigDecimal(1000000.0000).setScale(4);
                            if (tmpGlobalLimit.equals(oneMillion)) { //if the spending limit is "one million dollars", then set as NULL
                                globalBalanceHM.put("avail", null);
                            } else {
                                globalBalanceHM.put("avail", new BigDecimal(tempGlobalBalanceHM.get("GLOBALAVAILABLE").toString()));
                            }
                        }

                    }
                } else {
                    Logger.logMessage("ERROR: Could not determine valid globalBalanceList in BalanceCollection.getGlobalBalance()", Logger.LEVEL.ERROR);
                }
            } else {
                Logger.logMessage("ERROR: Could not determine valid logged in employee ID in BalanceCollection.getGlobalBalance()", Logger.LEVEL.ERROR);
            }
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
        return globalBalanceHM;
    }

    //Dollar Donations: need global settings for current balance view in MyQC in order to determine whether to allow donations to charity or not as well as text to display
    public ArrayList <HashMap> getDollarDonationSettingsInfo(){
        ArrayList<HashMap> dollarDonationSettings= new ArrayList<>();
        try{
            if (getLoggedInEmployeeID() != null) {
                dollarDonationSettings = dm.parameterizedExecuteQuery("data.globalSettings.getDollarDonationSettingsInfo",
                        new Object[]{getLoggedInEmployeeID()},
                        true
                );
            }
            else{
                Logger.logMessage("ERROR: Could not determine valid logged in employee ID in BalanceCollection.getGlobalBalance()", Logger.LEVEL.ERROR);
            }
        }
        catch (Exception ex){
            Logger.logException(ex);
        }
        return dollarDonationSettings;
    }
    //getter
    public List<BalanceModel> getCollection() {
        return this.collection;
    }

    //getter
    private Integer getLoggedInEmployeeID() {
        return this.loggedInEmployeeID;
    }

    //setter
    private void setLoggedInEmployeeID(Integer loggedInEmployeeID) {
        this.loggedInEmployeeID = loggedInEmployeeID;
    }

}
