package com.mmhayes.myqc.server.collections;

//mmhayes dependencies

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.DynamicSQL;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.myqc.server.models.PrepOptionModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

//myqc dependencies
//other dependencies

/* Prep Option Collection
 Last Updated (automatically updated by SVN)
 $Author: jkflanagan $: Author of last commit
 $Date: 2020-12-16 10:11:28 -0500 (Wed, 16 Dec 2020) $: Date of last commit
 $Rev: 53348 $: Revision of last commit

 Notes: Collection of Prep Options - A Prep Option Set
*/
public class PrepOptionCollection {
    List<PrepOptionModel> collection = new ArrayList<PrepOptionModel>();
    private static DataManager dm = new DataManager();

    //constructor - populates the collection
    public PrepOptionCollection(Integer prepOptionSetID) throws Exception {

        //populate this collection with models
        populateCollection(prepOptionSetID);

    }
    // constructor - populates the collection
    public PrepOptionCollection (Integer productID, Integer keypadID) throws Exception {

        //populate this collection with models
        populateCollection(productID, keypadID);

    }

    //populates this collection with models
    public PrepOptionCollection populateCollection(Integer prepOptionSetID) throws Exception {

        //get all models in an array list
        ArrayList<HashMap> prepOptionList = dm.parameterizedExecuteQuery("data.ordering.getPrepOptions",
                new Object[]{
                        prepOptionSetID
                },
                true
        );

        //populate this collection from an array list of hashmaps
        if (prepOptionList != null && prepOptionList.size() > 0) {
            for (HashMap prepOptionHM : prepOptionList) {
                this.getCollection().add( new PrepOptionModel( prepOptionHM ) );
            }
        }

        //check if there is a default prep option selected, this would be the case where someone deactivates the default prep option in a set
        for(PrepOptionModel prepOptionModel : this.getCollection()) {
           if(prepOptionModel.isDefaultOption()) {
               return this;
           }
        }

        if(this.getCollection().size() > 0) {
            PrepOptionModel prepOptionModelFirst = this.getCollection().get(0);
            prepOptionModelFirst.setDefaultOption(true);
        }

        return this;
    }

    //populates this collection with models
    @SuppressWarnings({"Convert2streamapi", "unchecked"})
    public PrepOptionCollection populateCollection (Integer productID, Integer keypadID) throws Exception {

        // validate the product ID
        if (productID == null || productID <= 0) {
            Logger.logMessage("The product ID passed to PrepOptionCollection.populateCollection must be greater than 0, unable to populate the prep option collection.", Logger.LEVEL.ERROR);
            return null;
        }

        // get the prep option set assigned to product and it's modifiers
        HashMap<Integer, Integer> pluToPrepOptSetMap = new HashMap<>();
        ArrayList<HashMap> queryResults = dm.parameterizedExecuteQuery("data.ordering.getProductToPrepOptionSetMappings", new Object[]{productID}, true);
        if (!DataFunctions.isEmptyCollection(queryResults)) {
            for (HashMap mapping : queryResults) {
                if (!DataFunctions.isEmptyMap(mapping)
                        && mapping.containsKey("PAPLUID")
                        && mapping.get("PAPLUID").toString().matches("\\d+")
                        && mapping.containsKey("PAPREPOPTIONSETID")
                        && mapping.get("PAPREPOPTIONSETID").toString().matches("\\d+")) {
                    pluToPrepOptSetMap.put(new BigDecimal(mapping.get("PAPLUID").toString()).intValue(), new BigDecimal(mapping.get("PAPREPOPTIONSETID").toString()).intValue());
                }
            }
        }

        // get the list of unique prep option set IDs being used by the product and it's modifiers
        List<Integer> uniquePrepOptSetIDs = null;
        if (!DataFunctions.isEmptyMap(pluToPrepOptSetMap)) {
            uniquePrepOptSetIDs = new ArrayList<>(new HashSet<>(pluToPrepOptSetMap.values()));
        }

        // get the prep option models for the default prep option within each prep option set if it should be displayed within MyQC
        ArrayList<HashMap> prepOptionRecordHMs = null;
        if (!DataFunctions.isEmptyCollection(uniquePrepOptSetIDs)) {
            DynamicSQL sql = new DynamicSQL("data.ordering.getDfltPrepOptsInPrepOptSets").addIDList(1, uniquePrepOptSetIDs.stream().map(String::valueOf).collect(Collectors.toList()));
            prepOptionRecordHMs = sql.serialize(dm);
        }

        //convert each prep option record HashMap into a PrepOptionModel and add each PrepOptionModel to the collection
        if (!DataFunctions.isEmptyCollection(prepOptionRecordHMs)) {
            for (HashMap prepOptionRecordHM : prepOptionRecordHMs) {
                if (!DataFunctions.isEmptyMap(prepOptionRecordHM)) {
                    this.getCollection().add(new PrepOptionModel(prepOptionRecordHM));
                }
            }
        }

        return this;
    }

    //getter
    public List<PrepOptionModel> getCollection() {
        return this.collection;
    }

}
