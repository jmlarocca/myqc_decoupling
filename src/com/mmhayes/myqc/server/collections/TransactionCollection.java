package com.mmhayes.myqc.server.collections;

//mmhayes dependencies
import com.mmhayes.common.api.*;
import com.mmhayes.common.dataaccess.*;
import com.mmhayes.common.utils.*;
import com.mmhayes.common.api.errorhandling.exceptions.*;

//myqc dependencies
import com.mmhayes.myqc.server.models.*;

//other dependencies
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.*;

/* Transaction Collection
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2020-04-29 09:51:12 -0400 (Wed, 29 Apr 2020) $: Date of last commit
 $Rev: 48472 $: Revision of last commit

 Notes: Collection of Transaction Models
*/
public class TransactionCollection {
    List<TransactionModel> collection = new ArrayList<TransactionModel>();
    private static DataManager dm = new DataManager();
    private Integer loggedInEmployeeID = null;

    //constructor - populates the collection
    public TransactionCollection(HashMap<String, LinkedList> paginationParamsHM, HttpServletRequest request) throws Exception {

        //determine the employeeID from the request
        setLoggedInEmployeeID(CommonAuthResource.checkRequestHeaderForMMHayesAuth(request));

        //populate this collection with models
        populateCollection(new ModelPaginator(paginationParamsHM));

    }

    //populates this collection with models
    public TransactionCollection populateCollection(ModelPaginator modelPaginator) throws Exception {

        //throw InvalidAuthException if no employeeID is found
        if (getLoggedInEmployeeID() == null) {
            throw new InvalidAuthException();
        }

        //set the modelCount so we can finalize the pagination properties
        modelPaginator.setModelCount(getTransactionCount());

        //determines, sets and validates pagination properties
        modelPaginator.finalizePaginationProperties();

        String sortByColumn = ""; //what column should this result set be sorted by
        if (modelPaginator.getSort().equals("date")) {
            sortByColumn = "TRANSDATE";
        } else {
            Logger.logMessage("ERROR: Invalid sort property in TransactionCollection.populateCollection(). sort property = "+modelPaginator.getSort(), Logger.LEVEL.ERROR);
        }

        //get a single page of models in an array list
        ArrayList<HashMap> transactionList = dm.serializeSqlWithColNames("data.myqc.getEmployeeTransactions",
                new Object[]{
                    getLoggedInEmployeeID().toString(),
                    modelPaginator.getStartingModelIndex().toString(),
                    modelPaginator.getEndingModelIndex().toString(),
                    sortByColumn,
                    modelPaginator.getOrder().toUpperCase()
                },
                false,
                false
        );

        List<String> txnIDs = new ArrayList<>();

        //populate this collection from an array list of hashmaps
        if (transactionList != null && transactionList.size() > 0) {
            for (HashMap transactionHM : transactionList) {
                TransactionModel transactionModel = new TransactionModel(transactionHM);
                String txnID = transactionModel.getID().toString();

                //If this transactionID is already in the list
                if(txnIDs.contains(txnID)) {

                    //Look for duplicate transactionID and add amount to it
                    for(TransactionModel currentTransactionModel : getCollection()) {
                        if(currentTransactionModel.getID().equals(transactionModel.getID())) {
                            BigDecimal amount = transactionModel.getAmt();
                            currentTransactionModel.setAmt(currentTransactionModel.getAmt().add(amount));

                            transactionModel.setDuplicateTxn(true);
                            transactionModel.setID(Integer.parseInt(txnID + "1"));

                            break;
                        }
                    }

                } else {
                    txnIDs.add(txnID);
                }

                getCollection().add(transactionModel);
            }
        }

        return this;
    }

    //get the count of transactions the currently logged in employee has
    private Integer getTransactionCount() {
        Integer transactionCount = 0;
        Object transactionCountObj = dm.parameterizedExecuteScalar("data.myqc.getEmployeeTransactions_count", new Object[] {getLoggedInEmployeeID()});
        if (transactionCountObj != null) {
            transactionCount = Integer.parseInt(transactionCountObj.toString());
        }
        transactionCount = Integer.parseInt(transactionCountObj.toString());
        return transactionCount;
    }

    //getter
    public List<TransactionModel> getCollection() {
        return this.collection;
    }

    //getter
    private Integer getLoggedInEmployeeID() {
        return this.loggedInEmployeeID;
    }

    //setter
    private void setLoggedInEmployeeID(Integer loggedInEmployeeID) {
        this.loggedInEmployeeID = loggedInEmployeeID;
    }

}
