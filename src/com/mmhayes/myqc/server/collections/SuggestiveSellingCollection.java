package com.mmhayes.myqc.server.collections;

//mmhayes dependencies

import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.CommonAuthResource;
import com.mmhayes.common.api.errorhandling.exceptions.InvalidAuthException;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.myqc.server.models.MenuModel;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

//myqc dependencies
//other dependencies

/* Suggestive Selling Collection
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2019-12-16 09:55:58 -0500 (Mon, 16 Dec 2019) $: Date of last commit
 $Rev: 44622 $: Revision of last commit

 Notes: Collection of Menu Models
*/
public class SuggestiveSellingCollection {
    List<MenuModel> collection = new ArrayList<MenuModel>();
    String keypadIDs = "";

    private static DataManager dm = new DataManager();
    private Integer authenticatedAccountID = null;
    private Integer storeID = null;

    //constructor - populates the collection
    public SuggestiveSellingCollection(String keypadID, HttpServletRequest request) throws Exception {

        //determine the authenticated account ID from the request
        setAuthenticatedAccountID(CommonAuthResource.checkRequestHeaderForMMHayesAuth(request));

        //determine the store ID from the request
        setStoreID(CommonAPI.checkRequestHeaderForStoreID(request));

        //throw InvalidAuthException if no authenticatedAccountID is found
        if (getAuthenticatedAccountID() == null) {
            throw new InvalidAuthException();
        }

        setKeypadIDs(keypadID);

        //populate this collection with models
        populateCollection();
    }

    public SuggestiveSellingCollection(Integer upsellProfileID, Integer storeID, Integer employeeID) throws Exception {

        setAuthenticatedAccountID(employeeID);

        setStoreID(storeID);

        List<String> upsellProfileKeypads = getUpsellProfileSuggestiveKeypads(upsellProfileID);
        setKeypadIDs(String.join(",", upsellProfileKeypads));

        //populate this collection with models
        populateCollection();
    }

    //populates this collection with models
    public SuggestiveSellingCollection populateCollection() throws Exception {

        if (getKeypadIDs().equals("")) {
            Logger.logMessage("Could not find suggestive keypads for upsell profile in SuggestiveSellingCollection.populateCollection", Logger.LEVEL.TRACE);
            return this;
        }

        //get all models in an array list
        ArrayList<HashMap> menuList = dm.parameterizedExecuteQuery("data.ordering.getSuggestiveMenuHeaders",
                new Object[]{
                        getKeypadIDs(),
                        getStoreID()
                },
                true
        );

        //populate this collection from an array list of hashmaps
        if (menuList != null && menuList.size() > 0) {
            ArrayList<String> keypadIds = new ArrayList<String>(Arrays.asList(getKeypadIDs().split(",")));
            ArrayList<HashMap> sortedList = new ArrayList<>();
            for(String keypadID : keypadIds) {
                for(HashMap menu : menuList) {
                   String menuID = CommonAPI.convertModelDetailToString(menu.get("ID"));
                    if(menuID.equals(keypadID)) {
                        sortedList.add(menu);
                        break;
                    }
                }
            }

            for(HashMap modelDetailHM : sortedList) {
                if (modelDetailHM.get("ID") != null && !modelDetailHM.get("ID").toString().isEmpty()) {
                    getCollection().add(new MenuModel(modelDetailHM, getAuthenticatedAccountID(), getStoreID(), true));
                } else {
                    Logger.logMessage("ERROR: Could not determine menuID from suggestive selling keypad in SuggestiveSellingCollection.populateCollection", Logger.LEVEL.ERROR);
                }
            }
        }

        return this;
    }

    //populates this collection with models
    public List<String> getUpsellProfileSuggestiveKeypads(Integer upsellProfileID) throws Exception {
        List<String> keypadIDs = new ArrayList<>();

        //get all models in an array list
        ArrayList<HashMap> menuList = dm.parameterizedExecuteQuery("data.ordering.getUpsellProfileSuggestiveKeypads",
                new Object[]{
                        upsellProfileID,
                        getStoreID()
                },
                true
        );

        //populate this collection from an array list of hashmaps
        if (menuList != null && menuList.size() > 0) {
            for(HashMap modelDetailHM : menuList) {
                String keypadID = CommonAPI.convertModelDetailToString(modelDetailHM.get("PAKEYPADID"));
                keypadIDs.add(keypadID);
            }
        } else {
            Logger.logMessage("ERROR: Could not find any keypads for Upsell Profile ID: "+upsellProfileID+". Check keypad revenue centers. In SuggestiveSellingCollection.getUpsellProfileSuggestiveKeypads", Logger.LEVEL.ERROR);
        }

        return keypadIDs;
    }

    //getter
    public List<MenuModel> getCollection() {
        return this.collection;
    }

    //getter
    private Integer getAuthenticatedAccountID() {
        return this.authenticatedAccountID;
    }

    //setter
    private void setAuthenticatedAccountID(Integer authenticatedAccountID) {
        this.authenticatedAccountID = authenticatedAccountID;
    }

    public String getKeypadIDs() {
        return keypadIDs;
    }

    public void setKeypadIDs(String keypadIDs) {
        this.keypadIDs = keypadIDs;
    }

    public Integer getStoreID() {
        return storeID;
    }

    public void setStoreID(Integer storeID) {
        this.storeID = storeID;
    }

}
