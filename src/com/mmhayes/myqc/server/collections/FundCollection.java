package com.mmhayes.myqc.server.collections;

//mmhayes dependencies
import com.mmhayes.common.api.*;
import com.mmhayes.common.dataaccess.*;
import com.mmhayes.common.utils.*;
import com.mmhayes.common.api.errorhandling.exceptions.*;

//myqc dependencies
import com.mmhayes.myqc.server.models.*;

//other dependencies
import javax.servlet.http.HttpServletRequest;
import java.util.*;

/* Fund Collection
 Last Updated (automatically updated by SVN)
 $Author: jrmitaly $: Author of last commit
 $Date: 2016-09-02 17:46:33 -0400 (Fri, 02 Sep 2016) $: Date of last commit
 $Rev: 18205 $: Revision of last commit

 Notes: Collection of Fund Models
*/
public class FundCollection {
    List<FundModel> collection = new ArrayList<FundModel>();
    private static DataManager dm = new DataManager();
    private Integer loggedInEmployeeID = null;

    //constructor - populates the collection
    public FundCollection(HashMap<String, LinkedList> paginationParamsHM, HttpServletRequest request) throws Exception {

        //determine the employeeID from the request
        setLoggedInEmployeeID(CommonAuthResource.checkRequestHeaderForMMHayesAuth(request));

        //populate this collection with models
        populateCollection(new ModelPaginator(paginationParamsHM));

    }

    //populates this collection with models
    public FundCollection populateCollection(ModelPaginator modelPaginator) throws Exception {

        //throw InvalidAuthException if no employeeID is found
        if (getLoggedInEmployeeID() == null) {
            throw new InvalidAuthException();
        }

        //set the modelCount so we can finalize the pagination properties
        modelPaginator.setModelCount(getFundCount());

        //determines, sets and validates pagination properties
        modelPaginator.finalizePaginationProperties();

        String sortByColumn = ""; //what column should this result set be sorted by
        if (modelPaginator.getSort().equals("date")) {
            sortByColumn = "TX.TransactionDate";
        } else {
            Logger.logMessage("ERROR: Invalid sort property in FundCollection.populateCollection(). sort property = "+modelPaginator.getSort(), Logger.LEVEL.ERROR);
        }

        //get a single page of models in an array list
        ArrayList<HashMap> fundList = dm.serializeSqlWithColNames("data.myqc.getEmployeeFunds",
                new Object[]{
                        getLoggedInEmployeeID().toString(),
                        modelPaginator.getStartingModelIndex().toString(),
                        modelPaginator.getEndingModelIndex().toString(),
                        sortByColumn,
                        modelPaginator.getOrder().toUpperCase()
                },
                false,
                false
        );

        //iterate through list, create models and add them to the collection
        if (fundList != null && fundList.size() > 0) {
            for (HashMap fundHM : fundList) {
                getCollection().add(new FundModel(fundHM));
            }
        }

        return this;
    }

    //get the count of fund records (QC Transactions against a special terminal type) the currently logged in employee has
    private Integer getFundCount() {
        Integer fundCount = 0;
        Object fundCountObj = dm.parameterizedExecuteScalar("data.myqc.getEmployeeFunds_count", new Object[] {getLoggedInEmployeeID()});
        fundCount = Integer.parseInt(fundCountObj.toString());
        return fundCount;
    }

    //getter
    public List<FundModel> getCollection() {
        return this.collection;
    }

    //getter
    private Integer getLoggedInEmployeeID() {
        return this.loggedInEmployeeID;
    }

    //setter
    private void setLoggedInEmployeeID(Integer loggedInEmployeeID) {
        this.loggedInEmployeeID = loggedInEmployeeID;
    }

}
