package com.mmhayes.myqc.server.collections;

//mmhayes dependencies

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.CommonAuthResource;
import com.mmhayes.common.api.ModelPaginator;
import com.mmhayes.common.api.errorhandling.exceptions.InvalidAuthException;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.myqc.server.models.FavoriteModel;
import com.mmhayes.myqc.server.models.FavoriteOrderModel;
import com.mmhayes.myqc.server.models.StoreKeypadModel;
import com.mmhayes.myqc.server.models.StoreModel;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;
import java.util.*;

//myqc dependencies
//other dependencies

/* Favorite Order Collection
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2019-02-04 10:19:11 -0500 (Mon, 04 Feb 2019) $: Date of last commit
 $Rev: 36507 $: Revision of last commit

 Notes: Collection of Favorite Order Models
*/
public class FavoriteOrderCollection {
    List<FavoriteOrderModel> collection = new ArrayList<FavoriteOrderModel>();
    List<FavoriteOrderModel> collectionUnavailable = new ArrayList<FavoriteOrderModel>();
    HashMap collectionHM = new HashMap();
    ModelPaginator modelPaginator = null;
    String terminalID = null; //This is actually representing the TerminalID and NOT the Store (MyQCTerminalID)
    private static DataManager dm = new DataManager();
    private Integer loggedInEmployeeID = null;

    ArrayList<String> allProductsInStoreList = new ArrayList<>();
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String allProductsInStore = "";
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String allProductsInStoreWithDiningOptions = "";

    //constructor - populates the collection
    public FavoriteOrderCollection(HashMap<String, LinkedList> paginationParamsHM, UriInfo uriInfo, HttpServletRequest request, boolean populateOrderCollection) throws Exception {

        //determine the employeeID from the request
        setLoggedInEmployeeID(CommonAuthResource.checkRequestHeaderForMMHayesAuth(request));

        //determine the terminalID from the MyQCTerminalID in request
        String myQCTerminalID = CommonAPI.checkRequestHeaderForStoreID(request).toString();
        String terminalID = FavoriteModel.getTerminalFromMyQCTerminalID(myQCTerminalID);
        setTerminalID(terminalID);


        StoreModel storeModel = StoreModel.getStoreModel(Integer.parseInt(myQCTerminalID), request);
        StoreKeypadModel storeKeypadModel = StoreKeypadModel.getStoreKeypadModel(storeModel);
        String productsInStoreWithDiningOptions = storeKeypadModel.getProductsInStoreWithDiningOptions();
        setAllProductsInStoreWithDiningOptions(productsInStoreWithDiningOptions);

        MultivaluedMap<String, String> httpGetParams = uriInfo.getQueryParameters();
        String productList = httpGetParams.get("productList").toString().replace("[", "");
        productList = productList.replace("]", "");
        setAllProductsInStore(productList);

        ModelPaginator modelPaginator = new  ModelPaginator(paginationParamsHM);
        setModelPaginator(modelPaginator);

        if(populateOrderCollection) {
            populateCollection();
        } else {
            populateUnavailableCollection();
        }
    }


    //populates the favorite order collection with favorite order models
    public FavoriteOrderCollection populateCollection() throws Exception {
        //throw InvalidAuthException if no employeeID or terminalID is found
        if (getLoggedInEmployeeID() == null || getTerminalID() == null) {
            throw new InvalidAuthException();
        }

        //get list of validated favorite order ids, pass into query to get details on favorite orders
        ArrayList<HashMap> orders = getFavoriteOrderIDCount();
        String favoriteOrderIDs = "";

        if(orders.size() > 0) {
            ArrayList<String> orderStrList = new ArrayList<>();
            for(HashMap order : orders) {
                orderStrList.add(CommonAPI.convertModelDetailToString(order.get("FAVORITEORDERID")));
            }
            favoriteOrderIDs = String.join(",", orderStrList);
        }

        //set the modelCount so we can finalize the pagination properties
        modelPaginator.setModelCount(orders.size());

        //determines, sets and validates pagination properties
        modelPaginator.finalizePaginationProperties();

        //get a single page of models in an array list
        ArrayList<HashMap> orderList = dm.parameterizedExecuteQuery("data.ordering.getEmployeeFavoriteOrders",
                new Object[]{
                        getTerminalID(),
                        getLoggedInEmployeeID(),
                        modelPaginator.getStartingModelIndex(),
                        modelPaginator.getEndingModelIndex(),
                        favoriteOrderIDs
                },
                true
        );

        //populate this collection from an array list of hashmaps
        if (orderList != null && orderList.size() > 0) {
            for (HashMap orderHM : orderList) {
                getCollection().add(new FavoriteOrderModel(orderHM, true));
            }
        }

        return this;
    }

    //get the count of the available favorite orders in the store's revenue center
    private ArrayList<HashMap> getFavoriteOrderIDCount() {
        ArrayList<HashMap> orders = new ArrayList<>();

        ArrayList<HashMap> orderList = dm.parameterizedExecuteQuery("data.ordering.getEmployeeFavoriteOrder_count",
                new Object[]{
                        getTerminalID(),
                        getLoggedInEmployeeID(),
                        getAllProductsInStoreWithDiningOptions()
                },
                true
        );

        if (orderList != null) {
            orders = orderList;
        }

        return orders;
    }

    //populates the favorite order collection with favorite order models
    public FavoriteOrderCollection populateUnavailableCollection() throws Exception {
        //throw InvalidAuthException if no employeeID or terminalID is found
        if (getLoggedInEmployeeID() == null || getTerminalID() == null) {
            throw new InvalidAuthException();
        }

        //get list of validated favorite order ids, pass that into query to get details on all other favorite orders that those ids for that employee
        ArrayList<HashMap> orders = getFavoriteOrderIDCount();
        String ignoreFavoriteOrderIDs = "";

        if(orders.size() > 0) {
            ArrayList<String> orderStrList = new ArrayList<>();
            for(HashMap order : orders) {
                orderStrList.add(CommonAPI.convertModelDetailToString(order.get("FAVORITEORDERID")));
            }
            ignoreFavoriteOrderIDs = String.join(",", orderStrList);
        }

        //set the modelCount so we can finalize the pagination properties
        modelPaginator.setModelCount(getOrderUnavailableCount(ignoreFavoriteOrderIDs));

        //determines, sets and validates pagination properties
        modelPaginator.finalizePaginationProperties();

        //get a single page of models in an array list
        ArrayList<HashMap> orderList = dm.parameterizedExecuteQuery("data.ordering.getEmployeeFavoriteOrdersUnavailable",
                new Object[]{
                        getTerminalID(),
                        getLoggedInEmployeeID(),
                        modelPaginator.getStartingModelIndex(),
                        modelPaginator.getEndingModelIndex(),
                        ignoreFavoriteOrderIDs
                },
                true
        );

        //populate this collection from an array list of hashmaps
        if (orderList != null && orderList.size() > 0) {
            for (HashMap orderHM : orderList) {
                getCollectionUnavailable().add(new FavoriteOrderModel(orderHM, true));
            }
        }

        return this;
    }

    //get the count of the unavailable orders that are a favorite in the store's revenue center
    private Integer getOrderUnavailableCount(String ignoreFavoriteOrderIDs) {
        Integer unavailableCount = 0;

        ArrayList<HashMap> unavailableList = dm.parameterizedExecuteQuery("data.ordering.getEmployeeFavoriteOrdersUnavailable_count",
                new Object[]{
                        getTerminalID(),
                        getLoggedInEmployeeID(),
                        ignoreFavoriteOrderIDs
                },
                true
        );

        if (unavailableList != null) {
            HashMap unavailableHM = unavailableList.get(0);
            unavailableCount = Integer.parseInt(unavailableHM.get("COUNT").toString());
        }
        return unavailableCount;
    }


    public List<FavoriteOrderModel> getCollection() {
        return collection;
    }

    public void setCollection(List<FavoriteOrderModel> collection) {
        this.collection = collection;
    }

    public List<FavoriteOrderModel> getCollectionUnavailable() {
        return collectionUnavailable;
    }

    public void setCollectionUnavailable(List<FavoriteOrderModel> collectionUnavailable) {
        this.collectionUnavailable = collectionUnavailable;
    }

    //getter
    private Integer getLoggedInEmployeeID() {
        return this.loggedInEmployeeID;
    }

    //setter
    private void setLoggedInEmployeeID(Integer loggedInEmployeeID) {
        this.loggedInEmployeeID = loggedInEmployeeID;
    }

    public String getTerminalID() {
        return terminalID;
    }

    public void setTerminalID(String terminalID) {
        this.terminalID = terminalID;
    }

    public ModelPaginator getModelPaginator() {
        return modelPaginator;
    }

    public void setModelPaginator(ModelPaginator modelPaginator) {
        this.modelPaginator = modelPaginator;
    }

    public String getAllProductsInStore() {
        return allProductsInStore;
    }

    public void setAllProductsInStore(String allProductsInStore) {
        this.allProductsInStore = allProductsInStore;
    }


    public ArrayList<String> getAllProductsInStoreList() {
        return allProductsInStoreList;
    }

    public void setAllProductsInStoreList(ArrayList<String> allProductsInStoreList) {
        String allProductsInStoreStr = String.join(",", allProductsInStoreList);
        setAllProductsInStore(allProductsInStoreStr);

        this.allProductsInStoreList = allProductsInStoreList;
    }

    public HashMap getCollectionHM() {
        return collectionHM;
    }

    public void setCollectionHM(HashMap collectionHM) {
        this.collectionHM = collectionHM;
    }

    public String getAllProductsInStoreWithDiningOptions() {
        return allProductsInStoreWithDiningOptions;
    }

    public void setAllProductsInStoreWithDiningOptions(String allProductsInStoreWithDiningOptions) {
        this.allProductsInStoreWithDiningOptions = allProductsInStoreWithDiningOptions;
    }

}
