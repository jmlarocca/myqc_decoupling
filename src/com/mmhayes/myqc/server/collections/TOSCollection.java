package com.mmhayes.myqc.server.collections;

//mmhayes dependencies

import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.CommonAuthResource;
import com.mmhayes.common.api.errorhandling.exceptions.InvalidAuthException;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.commonMMHFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.myqc.server.models.TOSModel;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

//myqc dependencies
//other dependencies

/* TOS Collection
 Last Updated (automatically updated by SVN)
 $Author: jmdottavio $: Author of last commit
 $Date: 2017-08-21 15:37:14 -0400 (Mon, 21 Aug 2017) $: Date of last commit
 $Rev: 23548 $: Revision of last commit

 Notes: Collection of TOS Models
*/
public class TOSCollection {
    List<TOSModel> collection = new ArrayList<>();
    private static DataManager dm = new DataManager();
    private commonMMHFunctions commFunc = new commonMMHFunctions();
    private Integer loggedInEmployeeID = null;

    public TOSCollection() {

    }

    //constructor - populates the collection
    public TOSCollection(HttpServletRequest request, Integer instanceUserID) throws Exception {

        if (instanceUserID != null) {
            //set the employeeID from the parameter
            setLoggedInEmployeeID(instanceUserID);
        } else {
            //determine the employeeID from the request
            setLoggedInEmployeeID(CommonAuthResource.checkRequestHeaderForMMHayesAuth(request));
        }

        //throw InvalidAuthException if no employeeID is found
        if (getLoggedInEmployeeID() == null) {
            throw new InvalidAuthException();
        }

        //if there is a valid logged in employee, then populate this collection
        populateCollection();

    }

    //populates this collection with models
    public TOSCollection populateCollection() throws Exception {

        //throw InvalidAuthException if no employeeID is found
        if (getLoggedInEmployeeID() == null) {
            throw new InvalidAuthException();
        }

        //TODO: (low) The following only checks for matches on one of each - which could result in missing multiple TOS
        //For example if ALL people in an account group need to sign a form but the user's terminal profile has them sign another but they needed to sign BOTH
        //This is part of the concept of accepting multiple TOS(s).. which we are not supporting in v1.0 of the myQC app
        //Keep on eye on this - no need to worry about in in v1.0 of the myQC app

        //TODO: (LOW) what is this here for? -jrmitaly - 6/7/2016
        HashMap modelDetailHM = new HashMap<String, Object>();

        //determine if any TOS is needed for the logged in employee, if so which ones?
        ArrayList<HashMap> tosList = commFunc.determineAccountTOS(getLoggedInEmployeeID());

        //iterate through list, create models and add them to the collection
        if (tosList != null && tosList.size() > 0) {
            Logger.logMessage("INFO: Found appropriate TOS for ("+getLoggedInEmployeeID()+") - adding TOS Details now.", Logger.LEVEL.DEBUG);
            for (HashMap tosHM : tosList) {
                getCollection().add(new TOSModel(tosHM));
            }
        }

        return this;
    }

    //STATIC - accept TOS for the currently logged in employee
    //TODO: (MED) - Method is using an old error handling method -jrmitaly - 6/7/2016
    public static HashMap<String, String> acceptTOS(Integer tosID, HttpServletRequest request) {
        HashMap<String, String> responseHM = new HashMap<>();
        try {

            //TODO: (MED) Should these SQL calls here be in a SQL batch? -jrmitaly 2/26/2016

            Integer employeeID = CommonAuthResource.checkRequestHeaderForMMHayesAuth(request);
            if (tosID != null && employeeID != null) {

                Integer updateResult = 0;

                if (tosID == 0 || employeeID == 0) { //verify valid tosID and employeeID
                    Logger.logMessage("ERROR: Invalid tosID or employeeID in TOSCollection.acceptTOS - not accepting TOS.", Logger.LEVEL.ERROR);
                } else {
                    //now that the user has accepted the TOS change their account status to "A" (active)
                    updateResult = dm.parameterizedExecuteNonQuery("data.common.activateAccount",
                            new Object[]{
                                    employeeID
                            }
                    );
                }

                //accept was successful
                if (updateResult == 1) {

                    //log success in DB
                    if (CommonAPI.logTOSAcception(tosID, employeeID, null, request)) {
                        Logger.logMessage("Successfully accepted TOS ID ("+tosID+") for account ID: "+employeeID, Logger.LEVEL.TRACE);
                        responseHM.put("status", "success");
                        responseHM.put("details", "Successfully accepted TOS.");
                    } else {
                        Logger.logMessage("ERROR: Could not LOG accepting ("+tosID+") for account ID: "+employeeID, Logger.LEVEL.ERROR);
                        responseHM.put("status", "failed");
                        responseHM.put("details", "Failed to log TOS acception.");
                    }

                    //log account activation to QC_EmployeeChanges table
                    Integer logAccountChangeResult = 0;
                    logAccountChangeResult = dm.parameterizedExecuteNonQuery("data.myqc.recordAccountChangeAsMyQCUser",
                            new Object[]{
                                    employeeID,
                                    "Status",
                                    "Waiting for Signup",
                                    "Active"
                            }
                    );
                    if (logAccountChangeResult == 0) {
                        Logger.logMessage("ERROR: Could not LOG account status change from W to A for account ID: "+employeeID, Logger.LEVEL.ERROR);
                    }

                } else {
                    Logger.logMessage("ERROR: Could not record in DB - failed to accept TOS - in TOSCollection.acceptTOS", Logger.LEVEL.ERROR);
                    responseHM.put("status", "failed");
                    responseHM.put("details", "Failed to accept TOS.");
                }
            } else {
                Logger.logMessage("ERROR: Could not determine tosID or employeeID - failed to accept TOS -  in TOSCollection.acceptTOS", Logger.LEVEL.ERROR);
                responseHM.put("status", "failed");
                responseHM.put("details", "Invalid Access.");
            }
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
            responseHM.put("status", "error");
            responseHM.put("details", commonMMHFunctions.convertExceptionToString(ex));
        }
        return responseHM;
    }

    //getter
    public List<TOSModel> getCollection() {
        return this.collection;
    }

    //getter
    private Integer getLoggedInEmployeeID() {
        return this.loggedInEmployeeID;
    }

    //setter
    private void setLoggedInEmployeeID(Integer loggedInEmployeeID) {
        this.loggedInEmployeeID = loggedInEmployeeID;
    }


    /* commented out and moved to commonMMHFunctions on 11/19/2015 by jrmitaly
    //builds an array list of hashmaps of TOS(s) the user needs to accept
    public ArrayList<HashMap> determineLoggedInEmployeesTOS() {
        ArrayList<HashMap> tosList = null;
        try {
            if (getLoggedInEmployeeID() != null) {
                //only find TOS if the user's account status is "W" (for waiting for approvals)
                if (checkEmployeeAccountStatus().equals("W")) {

                    //first check if there is a match for both spending profile and account group
                    tosList = checkTOSSpendingProfileAndAccountGroup();

                    if (tosList == null) { //no match there, try the next

                        //second check if there is a match for JUST the spending profile
                        tosList = checkTOSSpendingProfile();

                        if (tosList == null) { //no match there, try the next

                            //lastly check if there is a match for JUST the account group
                            tosList = checkTOSPayrollGroup();

                        }
                    }

                    if (tosList == null) { //check one more time now that we have tried "everything" to find a TOS for this employee
                        Logger.logMessage("ERROR: Could not find TOS for user BUT user has an account status of W in TOSCollection.determineLoggedInEmployeesTOS()");
                    }
                }
            }
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
        return tosList;
    }

    //get the account status of the currently logged in employee
    private String checkEmployeeAccountStatus() {
        String accountStatus = null;
        try {
            Object employeeAccountStatusObj = dm.parameterizedExecuteScalar("data.myqc.checkEmployeeAccountStatus", new Object[] {getLoggedInEmployeeID()});
            accountStatus = employeeAccountStatusObj.toString();
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
        return accountStatus;
    }

    //check if there is a TOS match for both this user's account group and spending profile
    private ArrayList<HashMap> checkTOSSpendingProfileAndAccountGroup() {
        ArrayList<HashMap> tosList = null;
        try {
            tosList = dm.parameterizedExecuteQuery("data.myqc.checkTOSSpendingProfileAndAccountGroup",
                new Object[]{
                        getLoggedInEmployeeID().toString()
                },
                true
            );

            //if the size of the tosList is 0, then just set it to null because we didn't find anything
            if (tosList != null && tosList.size() == 0) {
                tosList = null;
            }
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
        return tosList;
    }

    //check if there is a TOS match for JUST this user's spending profile (terminal profile)
    private ArrayList<HashMap> checkTOSSpendingProfile() {
        ArrayList<HashMap> tosList = null;
        try {
            tosList = dm.parameterizedExecuteQuery("data.myqc.checkTOSSpendingProfile",
                    new Object[]{
                            getLoggedInEmployeeID().toString()
                    },
                    true
            );

            //if the size of the tosList is 0, then just set it to null because we didn't find anything
            if (tosList != null && tosList.size() == 0) {
                tosList = null;
            }
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
        return tosList;
    }

    //check if there is a TOS match for JUST this user's account group (payroll group)
    private ArrayList<HashMap> checkTOSPayrollGroup() {
        ArrayList<HashMap> tosList = null;
        try {
            tosList = dm.parameterizedExecuteQuery("data.myqc.checkTOSAccountGroup",
                    new Object[]{
                            getLoggedInEmployeeID().toString()
                    },
                    true
            );

            //if the size of the tosList is 0, then just set it to null because we didn't find anything
            if (tosList != null && tosList.size() == 0) {
                tosList = null;
            }
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
        return tosList;
    }

    */


}
