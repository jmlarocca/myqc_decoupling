package com.mmhayes.myqc.server.collections;

//mmhayes dependencies

import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.errorhandling.exceptions.MissingDataException;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.myqc.server.models.IntervalModel;
import com.mmhayes.myqc.server.models.ScheduleDetailModel;
import com.mmhayes.myqc.server.models.ScheduleModel;
import com.mmhayes.myqc.server.models.StoreModel;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

//myqc dependencies
//other dependencies

/* Interval Collection
Last Updated (automatically updated by SVN)
$Author: gematuszyk $: Author of last commit
$Date: 2020-06-08 14:36:24 -0400 (Mon, 08 Jun 2020) $: Date of last commit
$Rev: 49458 $: Revision of last commit

Notes: Collection of Interval Models
*/
public class IntervalCollection {
    private List<IntervalModel> collection = new ArrayList<IntervalModel>();
    private boolean transactionsFound = true;
    private static DataManager dm = new DataManager();

    //constructor - populates the collection
    public IntervalCollection() throws Exception {

    }

    public IntervalCollection getBufferIntervalCounts(StoreModel storeModel) throws Exception {
        IntervalCollection foundBufferCollection = new IntervalCollection();

        LocalDate date = LocalDate.now();

        //build buffer schedule for the future order date
        if(storeModel.getFutureOrderDate() != null) {
            date = storeModel.getFutureOrderDate();
        }

        //gets the transactions that occurred to today between the buffer schedule start and end times
        ArrayList<HashMap> bufferTransactionList = dm.parameterizedExecuteQuery("data.ordering.getBufferScheduleTransactions",
                new Object[]{
                        storeModel.getID(),
                        date.toString()
                },
                true
        );

        //populate this collection from an array list of hashmaps
        if ( bufferTransactionList == null || bufferTransactionList.size() == 0  ) {
            Logger.logMessage("No transactions were placed during the ordering buffer schedule for store ID: " + storeModel.getID(), Logger.LEVEL.TRACE);
            return foundBufferCollection;
        }

        for ( HashMap bufferHM : bufferTransactionList ) {
            //determine that response is valid
            if ( bufferHM.get("ID") == null || bufferHM.get("ID").toString().isEmpty() ) {
                Logger.logMessage("Could not determine buffer interval ID in IntervalCollection constructor by store ID", Logger.LEVEL.WARNING);
                continue;
            }

            IntervalModel intervalModel = new IntervalModel(bufferHM);

            foundBufferCollection.getCollection().add(intervalModel);
        }

        return foundBufferCollection;

    }

    public void updateIntervalCounts(StoreModel storeModel) throws Exception {

        //get all the buffer intervals that have transactions placed in them
        IntervalCollection foundBufferCollection = getBufferIntervalCounts(storeModel);

        if(foundBufferCollection.getCollection().size() == 0) {
            setTransactionsFound(false);
        }

        //loop over the list of intervals returned from the database
        for(IntervalModel intervalModel : foundBufferCollection.getCollection()) {

            //loop over the full buffer interval list and update it based on the found buffer intervals
            for(IntervalModel currentIntervalModel : getCollection()) {

                if( !(intervalModel.getScheduleDetailID().equals(currentIntervalModel.getScheduleDetailID()) && intervalModel.getIntervalStartTime().equals(currentIntervalModel.getIntervalStartTime()) && intervalModel.getIntervalEndTime().equals(currentIntervalModel.getIntervalEndTime())) ) {
                    continue;
                }

                currentIntervalModel.setPickupTransactionCount(intervalModel.getPickupTransactionCount());
                currentIntervalModel.setDeliveryTransactionCount(intervalModel.getDeliveryTransactionCount());
                currentIntervalModel.setTransactionTotalCount(intervalModel.getTransactionTotalCount());

                break;
            }
        }
    }

    //create a list of 15 minute intervals for the schedule detail's open and close time
    public void createIntervalList(ArrayList<String> openTimes, ArrayList<String> closeTimes) throws Exception {

        for(int i=0; i<openTimes.size(); i++) {

            if( !(openTimes.size() > i && closeTimes.size() > i) || (openTimes.get(i) == null) || (closeTimes.get(i) == null) ) {
                continue;
            }

            LocalTime startTime = LocalTime.parse(CommonAPI.convertToMilitaryTime(openTimes.get(i)));
            LocalTime scheduleEnd = LocalTime.parse(CommonAPI.convertToMilitaryTime(closeTimes.get(i)));

            Integer id = 0;

            while ( !(startTime.compareTo(scheduleEnd) == 0) && !startTime.isAfter(scheduleEnd) && !(startTime.compareTo(LocalTime.parse("00:00")) == 0 && getCollection().size() > 0)) {
                IntervalModel intervalModel = new IntervalModel();
                intervalModel.setLocalStartTime(startTime);

                LocalTime intervalEnd = startTime.plusMinutes(15);
                intervalModel.setLocalEndTime(intervalEnd);

                intervalModel.setID(id);

                getCollection().add(intervalModel);

                id++;
                startTime = startTime.plusMinutes(15);

                if ( getCollection().size() > 100 ) {
                    Logger.logMessage("Too many schedule intervals being created for in IntervalCollection constructor", Logger.LEVEL.ERROR);
                    break;
                }
            }
        }
    }

    //create a list of 15 minute intervals for the schedule detail's open and close time
    public void createBufferIntervalList(ScheduleModel bufferScheduleModel) throws Exception {

        for(ScheduleDetailModel scheduleDetailModel : bufferScheduleModel.getScheduleDetails().getCollection()) {
            LocalTime startTime = LocalTime.parse(scheduleDetailModel.getScheduleStartTime());
            LocalTime scheduleEnd = LocalTime.parse(scheduleDetailModel.getScheduleEndTime());

            Integer id = 0;

            while ( !(startTime.compareTo(scheduleEnd) == 0) && !startTime.isAfter(scheduleEnd) ) {
                IntervalModel intervalModel = new IntervalModel();
                intervalModel.setLocalStartTime(startTime);

                LocalTime intervalEnd = startTime.plusMinutes(15);
                intervalModel.setLocalEndTime(intervalEnd);

                intervalModel.setID(id);
                intervalModel.setScheduleDetailID(scheduleDetailModel.getID());
                intervalModel.setScheduleID(scheduleDetailModel.getScheduleID());
                intervalModel.setScheduleStartTime(scheduleDetailModel.getScheduleStartTime());
                intervalModel.setScheduleEndTime(scheduleDetailModel.getScheduleEndTime());

                getCollection().add(intervalModel);

                id++;
                startTime = startTime.plusMinutes(15);

                if ( getCollection().size() > 100 ) {
                    Logger.logMessage("Too many schedule intervals being created for in IntervalCollection constructor", Logger.LEVEL.ERROR);
                    break;
                }
            }
        }
    }

    public void updateScheduleFromBufferIntervals(List<IntervalModel> bufferIntervals, Integer orderType) throws Exception {

        for(IntervalModel bufferIntervalModel : bufferIntervals) {

            for(IntervalModel intervalModel : getCollection()) {

                if( !(bufferIntervalModel.getIntervalStartTime().equals(intervalModel.getIntervalStartTime()) && bufferIntervalModel.getIntervalEndTime().equals(intervalModel.getIntervalEndTime())) ) {
                    continue;
                }

                //if the buffer interval is closed, set list interval as closed
                if( !bufferIntervalModel.isIntervalOpen() ) {
                    intervalModel.setIntervalOpen(false);

                //if this is a pickup schedule and the buffer interval is closed for pickup, set pickup interval as closed
                } else if(orderType.equals(3) && !bufferIntervalModel.isPickupOpen()) {
                    intervalModel.setPickupOpen(false);

                //if this is a delivery schedule and the buffer interval is closed for delivery, set delivery interval as closed
                } else if(orderType.equals(2) && !bufferIntervalModel.isPickupOpen()) {
                    intervalModel.setPickupOpen(false);
                }

                break;
            }
        }
    }

    //getter
    public List<IntervalModel> getCollection() {
        return this.collection;
    }

    public void setCollection(List<IntervalModel> collection) {
        this.collection = collection;
    }

    public boolean getTransactionsFound() {
        return transactionsFound;
    }

    public void setTransactionsFound(boolean transactionsFound) {
        this.transactionsFound = transactionsFound;
    }

}
