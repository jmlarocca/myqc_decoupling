package com.mmhayes.myqc.server.collections;

//mmhayes dependencies
import com.mmhayes.common.api.*;
import com.mmhayes.common.dataaccess.*;
import com.mmhayes.common.utils.*;
import com.mmhayes.common.api.errorhandling.exceptions.*;

//myqc dependencies
import com.mmhayes.myqc.server.models.*;

//other dependencies
import javax.servlet.http.HttpServletRequest;
import java.util.*;

/* Deduction Collection
 Last Updated (automatically updated by SVN)
 $Author: jrmitaly $: Author of last commit
 $Date: 2016-09-02 17:46:33 -0400 (Fri, 02 Sep 2016) $: Date of last commit
 $Rev: 18205 $: Revision of last commit

 Notes: Collection of Deduction Models
*/
public class DeductionCollection {
    List<DeductionModel> collection = new ArrayList<DeductionModel>();
    private static DataManager dm = new DataManager();
    private Integer loggedInEmployeeID = null;

    //constructor - populates the collection
    public DeductionCollection(HashMap<String, LinkedList> paginationParamsHM, HttpServletRequest request) throws Exception {

        //determine the employeeID from the request
        setLoggedInEmployeeID(CommonAuthResource.checkRequestHeaderForMMHayesAuth(request));

        //populate this collection with models
        populateCollection(new ModelPaginator(paginationParamsHM));

    }

    //populates this collection with models
    public DeductionCollection populateCollection(ModelPaginator modelPaginator) throws Exception {

        //throw InvalidAuthException if no employeeID is found
        if (getLoggedInEmployeeID() == null) {
            throw new InvalidAuthException();
        }

        //set the modelCount so we can finalize the pagination properties
        modelPaginator.setModelCount(getDeductionCount());

        //determines, sets and validates pagination properties
        modelPaginator.finalizePaginationProperties();

        String sortByColumn = ""; //what column should this result set be sorted by
        String additionalSortByColumn = ""; //this is needed because we also want to sort the entire result set by deductions.DEDUCTIONCODE ASC everytime
        if (modelPaginator.getSort().equals("date")) {
            sortByColumn = "PH.ThroughDate";
            additionalSortByColumn = "deductions.PERIODENDDATE";
        } else {
            Logger.logMessage("ERROR: Invalid sort property in DeductionCollection.populateCollection(). sort property = "+modelPaginator.getSort(), Logger.LEVEL.ERROR);
        }

        //get a single page of models in an array list
        ArrayList<HashMap> deductionList = dm.serializeSqlWithColNames("data.myqc.getEmployeeDeductions",
                new Object[]{
                        getLoggedInEmployeeID().toString(),
                        modelPaginator.getStartingModelIndex().toString(),
                        modelPaginator.getEndingModelIndex().toString(),
                        sortByColumn,
                        additionalSortByColumn,
                        modelPaginator.getOrder().toUpperCase()
                },
                false,
                false
        );

        //iterate through list, create models and add them to the collection
        if (deductionList != null && deductionList.size() > 0) {
            for (HashMap deductionHM : deductionList) {
                getCollection().add(new DeductionModel(deductionHM));
            }
        }

        return this;
    }

    //get the count of deductions the currently logged in employee has
    private Integer getDeductionCount() {
        Integer deductionCount = 0;
        Object deductionCountObj = dm.parameterizedExecuteScalar("data.myqc.getEmployeeDeductions_count", new Object[] {getLoggedInEmployeeID()});
        if (deductionCountObj != null) {
            deductionCount = Integer.parseInt(deductionCountObj.toString());
        }
        return deductionCount;
    }

    //getter
    public List<DeductionModel> getCollection() {
        return this.collection;
    }

    //getter
    private Integer getLoggedInEmployeeID() {
        return this.loggedInEmployeeID;
    }

    //setter
    private void setLoggedInEmployeeID(Integer loggedInEmployeeID) {
        this.loggedInEmployeeID = loggedInEmployeeID;
    }

}
