package com.mmhayes.myqc.server.collections;

//mmhayes dependencies


import com.mmhayes.common.account.models.AccountModel;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.CommonAuthResource;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.api.PosApi30.TransactionBuilder;
import com.mmhayes.common.api.errorhandling.exceptions.InvalidAuthException;
import com.mmhayes.common.api.errorhandling.exceptions.MissingDataException;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.commonMMHFunctions;
import com.mmhayes.common.funding.FundingHelper;
import com.mmhayes.common.funding.collections.AccountPaymentMethodCollection;
import com.mmhayes.common.funding.models.AccountPaymentMethodModel;
import com.mmhayes.common.loyalty.models.LoyaltyAccountModel;
import com.mmhayes.common.loyalty.models.LoyaltyRewardModel;
import com.mmhayes.common.product.models.ItemTaxModel;
import com.mmhayes.common.terminal.models.TerminalModel;
import com.mmhayes.common.transaction.models.*;
import com.mmhayes.common.transaction.models.TransactionModel;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.myqc.server.models.*;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

//myqc dependencies
//other dependencies

/* Account Settings Collection
 Last Updated (automatically updated by SVN)
 $Author: ijgerstein $: Author of last commit
 $Date: 2021-06-29 15:14:17 -0400 (Tue, 29 Jun 2021) $: Date of last commit
 $Rev: 56227 $: Revision of last commit

 Notes: Collection of Account Settings functions
*/
public class AccountSettingsCollection {
    List<SpendingProfileModel> spendingProfileCollection = new ArrayList<SpendingProfileModel>();
    List<AccountGroupModel> accountGroupCollection =  new ArrayList<AccountGroupModel>();
    List<AccountTypeModel> accountTypeCollection =  new ArrayList<AccountTypeModel>();
    private static DataManager dm = new DataManager();
    private static commonMMHFunctions comm = new commonMMHFunctions(); //the usual common mmh functions
    private Integer loggedInEmployeeID = null;
    Integer accountGroupID = null;
    Integer spendingProfileID = null;
    Integer accountTypeChange = null;
    Integer originalSpendingProfile = null;
    Integer personID = null;
    Integer orgLevelPrimaryId = null;
    Boolean spendingProfileChanged = false;
    Boolean accountGroupChanged = false;
    Boolean statusChanged = false;
    Boolean needsTOS = false;
    String email = "";
    String lowBalanceThreshold = null;
    String employeeLowBalanceThreshold = null;
    String fundingFee = "";
    String errorDetails = "";
    BigDecimal mealPlanPrice = BigDecimal.ZERO;

    //constructor - populates the collection
    public AccountSettingsCollection(HttpServletRequest request) throws Exception {

        //determine the employeeID from the request
        setLoggedInEmployeeID(CommonAuthResource.checkRequestHeaderForMMHayesAuth(request));

        //populate this collection with models
        populateAccountGroupCollection();
        populateSpendingProfileCollection();
        populateAccountTypeCollection();
    }

    public AccountSettingsCollection(HttpServletRequest request, HashMap accountDetailsHM) throws Exception {
        //determine the employeeID from the request
        setLoggedInEmployeeID(CommonAuthResource.checkRequestHeaderForMMHayesAuth(request));

        if(accountDetailsHM.containsKey("accountGroupID")) {
            setAccountGroupID(CommonAPI.convertModelDetailToInteger(accountDetailsHM.get("accountGroupID")));
        }
        if(accountDetailsHM.containsKey("spendingProfileID")) {
            setSpendingProfileID(CommonAPI.convertModelDetailToInteger(accountDetailsHM.get("spendingProfileID")));
        }

        if(accountDetailsHM.containsKey("spendingProfileChanged")) {
            setSpendingProfileChanged(CommonAPI.convertModelDetailToBoolean(accountDetailsHM.get("spendingProfileChanged")));
        }

        if(accountDetailsHM.containsKey("accountGroupChanged")) {
            setAccountGroupChanged(CommonAPI.convertModelDetailToBoolean(accountDetailsHM.get("accountGroupChanged")));
        }

        if(accountDetailsHM.containsKey("accountTypeID")) {
            setAccountTypeChange(Integer.parseInt(accountDetailsHM.get("accountTypeID").toString()));
        }

        if(accountDetailsHM.containsKey("needsTOS")) {
            setNeedsTOS(Boolean.parseBoolean(accountDetailsHM.get("needsTOS").toString()));
        }

        if(accountDetailsHM.containsKey("statusChanged")) {
            setStatusChanged(true);
        }

        if(accountDetailsHM.containsKey("email")) {
            setEmail(accountDetailsHM.get("email").toString().toLowerCase());
        }

        if(accountDetailsHM.containsKey("lowBalanceThreshold")) {
            setLowBalanceThreshold(accountDetailsHM.get("lowBalanceThreshold").toString());
            setPersonID(Integer.parseInt(accountDetailsHM.get("personID").toString()));
        }

        if(accountDetailsHM.containsKey("originalSpendingProfile")) {
            setOriginalSpendingProfile(Integer.parseInt(accountDetailsHM.get("originalSpendingProfile").toString()));
        }

        if(accountDetailsHM.containsKey("fundingFee") && accountDetailsHM.get("fundingFee") != null) {
            setFundingFee(accountDetailsHM.get("fundingFee").toString());
        }

        if(accountDetailsHM.containsKey("orgLevelPrimaryId") && accountDetailsHM.get("orgLevelPrimaryId") != null){
            setOrgLevelPrimaryId(Integer.parseInt(accountDetailsHM.get("orgLevelPrimaryId").toString()));
        }

        if(getAccountGroupChanged()) {
            checkIfAccountTypeNeedsChange();
        }

        if(accountDetailsHM.containsKey("employeeLowBalanceThreshold")) {
            setEmployeeLowBalanceThreshold(accountDetailsHM.get("employeeLowBalanceThreshold").toString());
        }
    }

    public AccountSettingsCollection(HashMap accountDetailsHM) throws Exception {

        setAccountGroupID(CommonAPI.convertModelDetailToInteger(accountDetailsHM.get("accountGroupID")));
        setOriginalSpendingProfile(Integer.parseInt(accountDetailsHM.get("originalSpendingProfile").toString()));

        getAccountGroupsFromAccountGroup();
        getSpendingProfilesFromAccountGroup();
        getAccountTypeFromAccountGroup();
    }

    public AccountSettingsCollection(HttpServletRequest request, String emptyString) throws Exception {

        //determine the employeeID from the request
        setLoggedInEmployeeID(CommonAuthResource.checkRequestHeaderForMMHayesAuth(request));

    }

    // BEGIN FUNCTIONS ON INITIAL LOAD TO POPULATE Account Group, Spending Profile and Account Type COLLECTIONS

    //populates this collection with models
    public AccountSettingsCollection populateAccountGroupCollection() throws Exception {

        //throw InvalidAuthException if no employeeID is found
        if (getLoggedInEmployeeID() == null) {
            throw new InvalidAuthException();
        }

        //get all models in an array list
        ArrayList<HashMap> accountGroupList = dm.parameterizedExecuteQuery("data.accountSettings.getAccountGroupOptions",
                new Object[]{
                        getLoggedInEmployeeID()
                },
                true
        );

        //iterate through list, create models and add them to the collection
        if (accountGroupList != null && accountGroupList.size() > 0) {
            for (HashMap accountGroupHM : accountGroupList) {
                getAccountGroupCollection().add(new AccountGroupModel(accountGroupHM));
            }
        }

        return this;
    }

    //populates this collection with models
    public AccountSettingsCollection populateSpendingProfileCollection() throws Exception {

        //throw InvalidAuthException if no employeeID is found
        if (getLoggedInEmployeeID() == null) {
            throw new InvalidAuthException();
        }

        //get all models in an array list
        ArrayList<HashMap> spendingProfileList = dm.parameterizedExecuteQuery("data.accountSettings.getSpendingProfileOptions",
                new Object[]{
                    getLoggedInEmployeeID()
                },
                true
        );

        //iterate through list, create models and add them to the collection
        if (spendingProfileList != null && spendingProfileList.size() > 0) {
            for (HashMap spendingProfileHM : spendingProfileList) {
                getSpendingProfileCollection().add(new SpendingProfileModel(spendingProfileHM, getLoggedInEmployeeID()));
            }
        }

        return this;
    }

    //populates this collection with models
    public AccountSettingsCollection populateAccountTypeCollection() throws Exception {

        //throw InvalidAuthException if no employeeID is found
        if (getLoggedInEmployeeID() == null) {
            throw new InvalidAuthException();
        }

        //get all models in an array list
        ArrayList<HashMap> accountTypeList = dm.parameterizedExecuteQuery("data.accountSettings.getAccountTypes",
                new Object[]{
                        getLoggedInEmployeeID()
                },
                true
        );

        //iterate through list, create models and add them to the collection
        if (accountTypeList != null && accountTypeList.size() > 0) {
            for (HashMap accountTypeHM : accountTypeList) {
                getAccountTypeCollection().add(new AccountTypeModel(accountTypeHM));
            }
        }

        return this;
    }

    // BEGIN FUNCTIONS ON CHANGE TO POPULATE Account Group, Spending Profile and Account Type COLLECTIONS

    //populates this collection with models
    public AccountSettingsCollection getAccountGroupsFromAccountGroup() throws Exception {

        //get all models in an array list
        ArrayList<HashMap> accountGroupList = dm.parameterizedExecuteQuery("data.accountSettings.getAccountGroupOptionsFromAccountGroup",
                new Object[]{
                        getAccountGroupID()
                },
                true
        );

        //iterate through list, create models and add them to the collection
        if (accountGroupList != null && accountGroupList.size() > 0) {
            for (HashMap spendingProfileHM : accountGroupList) {
                getAccountGroupCollection().add(new AccountGroupModel(spendingProfileHM));
            }
        }

        return this;
    }

    //populates this collection with models
    public AccountSettingsCollection getSpendingProfilesFromAccountGroup() throws Exception {
        String sql = "data.accountSettings.getSpendingProfilesFromAccountGroup";

        if(getOriginalSpendingProfile() != null && !isFundingTerminalInSpendingProfile()) {
            sql = "data.accountSettings.getSpendingProfilesFromAccountGroupNullPrice";
        }

        //get all models in an array list
        ArrayList<HashMap> spendingProfileList = dm.parameterizedExecuteQuery(sql,
                new Object[]{
                        getAccountGroupID()
                },
                true
        );

        //iterate through list, create models and add them to the collection
        if (spendingProfileList != null && spendingProfileList.size() > 0) {
            for (HashMap spendingProfileHM : spendingProfileList) {
                getSpendingProfileCollection().add(new SpendingProfileModel(spendingProfileHM));
            }
        }

        return this;
    }

    //populates this collection with models
    public AccountSettingsCollection getAccountTypeFromAccountGroup() throws Exception {

        //get all models in an array list
        ArrayList<HashMap> accountTypeList = dm.parameterizedExecuteQuery("data.accountSettings.getAccountTypesFromAccountGroup",
                new Object[]{
                        getAccountGroupID()
                },
                true
        );

        //iterate through list, create models and add them to the collection
        if (accountTypeList != null && accountTypeList.size() > 0) {
            for (HashMap accountTypeHM : accountTypeList) {
                getAccountTypeCollection().add(new AccountTypeModel(accountTypeHM));
            }
        }

        return this;
    }

    //get the user settings for the person account, such as the low balance threshold
    public static String getPersonUserSettings(Integer personID) {
        String lowBalanceThreshold = "-1.00";

        ArrayList<HashMap> lowBalanceThresholdList = dm.parameterizedExecuteQuery("data.accountSettings.getPersonLowBalanceThreshold",
                new Object[]{
                        personID
                },
                true
        );

        if(lowBalanceThresholdList != null && lowBalanceThresholdList.size() > 0) {
            HashMap lowBalanceThresholdHM = lowBalanceThresholdList.get(0);
            if(lowBalanceThresholdHM.get("LOWBALANCETHRESHOLD") != null) {
                lowBalanceThreshold = lowBalanceThresholdHM.get("LOWBALANCETHRESHOLD").toString();
                if(lowBalanceThreshold.equals("")) {
                    lowBalanceThreshold = "null";
                }
            }
        }

        return lowBalanceThreshold;
    }

    // BEING SAVE AND CANCEL FUNCTIONS

    //change back to original account settings
    public boolean cancelAccountSettingChanges() {
        try {

            if(getStatusChanged()) {
                //change status and account group
                if(getAccountGroupID() != null && getAccountTypeChange() == null && getSpendingProfileID() == null) {
                    checkIfAccountGroupNeedsChange();
                    checkIfAccountStatusNeedsChange();

                    //change status and account group and account type
                } else if(getAccountGroupID() != null && getAccountTypeChange() != null && getSpendingProfileID() == null) {
                    updateAccountGroupAccountTypeAndStatus("Active", "A");

                    //change status and account group and spending profile
                } else if(getAccountGroupID() != null && getAccountTypeChange() == null && getSpendingProfileID() != null) {
                    updateAccountGroupSpendingProfileAccountStatus("Active", "A");

                    //change status and spending profile
                } else if(getAccountGroupID() == null && getAccountTypeChange() == null && getSpendingProfileID() != null) {
                    updateSpendingProfileAndStatus("Active", "A");

                    //change status and account group, account type and spending profile
                } else if(getAccountGroupID() != null && getAccountTypeChange() != null && getSpendingProfileID() != null) {
                    updateAccountGroupSpendingProfileAccountTypeAccountStatus("Active", "A");
                }
            } else {
                //change account group and spending profile
                if(getAccountGroupID() != null && getAccountTypeChange() == null && getSpendingProfileID() != null) {
                    updateAccountGroupSpendingProfile();

                    //change account group, account type and spending profile
                } else if(getAccountGroupID() != null && getAccountTypeChange() != null && getSpendingProfileID() != null) {
                    updateAccountGroupSpendingProfileAccountType();

                    //change spending profile
                } else if(getAccountGroupID() == null && getAccountTypeChange() == null && getSpendingProfileID() != null) {
                    updateSpendingProfile();
                }
            }
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
            Logger.logMessage("ERROR: Could not change back account settings in cancelAccountSettingChanges for employeeID: "+getLoggedInEmployeeID() +" in AccountSettingsCollections.cancelAccountSettingChanges()", Logger.LEVEL.ERROR);
        }

        return true;
    }

    //saves the changes to the account settings if there is not TOS required and the spending profile hasn't been changed
    public boolean saveAccountSettingChanges() {
        try {

            //change account group and spending profile
            if(getAccountGroupID() != null && getAccountTypeChange() == null) {
                if(!checkIfAccountGroupNeedsChange()) {  //need to check if it needs to be changed because it could have been changed when changing the spending profile
                    return false;
                }

                //change account group, account type and spending profile
            } else if(getAccountGroupID() != null && getAccountTypeChange() != null) {
                if(!checkIfAccountGroupAccountTypeNeedsChange()) {  //need to check if it needs to be changed because it could have been changed when changing the spending profile
                    return false;
                }
            }

            if(getStatusChanged()) {
                checkIfAccountStatusNeedsChange();
            }

            if(!getEmail().equals("")) {
                updateAccountEmail();
            }

            if(getLowBalanceThreshold() != null) {
                updatePersonLowBalanceThreshold();
            }

            if(getEmployeeLowBalanceThreshold() != null) {
                updateEmployeeAccountLowBalanceThresholdChanges();
                updateEmployeeLowBalanceThreshold();
            }

            if(getOrgLevelPrimaryId() != null){

                if(getOrgLevelPrimaryId().equals(0)){
                    setOrgLevelPrimaryId(null);
                }
                updateEmployeeOrgLevelChanges();
                updateEmployeeOrgLevel();
            }

        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
            Logger.logMessage("ERROR: Could not save account setting changes for employeeID: "+getLoggedInEmployeeID() +" in AccountSettingsCollections.saveAccountSettingChanges()", Logger.LEVEL.ERROR);
            return false;
        }

        return true;
    }


    // BEGIN FUNCTIONS FOR CHECKING IF THERE IS A TOS REQUIRED

    //checks if the account group and spending profile are mapped to a TOS that is not the last TOS the user accepted
    public ArrayList<HashMap> checkIfTOSIsRequired() {
        ArrayList<HashMap> tosList = new ArrayList<>();
        try {
            if (getLoggedInEmployeeID() != null) {

                //first check if there is a match for both spending profile and account group
                tosList = checkTOSSpendingProfileAndAccountGroup();

                if (tosList == null) { //no match there, try the next

                    //second check if there is a match for JUST the spending profile
                    tosList = checkTOSSpendingProfile();

                    if (tosList == null) { //no match there, try the next

                        //check if there is a match for JUST the account group
                        tosList = checkTOSPayrollGroup();
                    }
                }

                //if no TOS is returned, user doesn't have to accept another one
                if(tosList != null && tosList.size() > 0 && !tosList.get(0).get("ID").toString().equals("")) {
                    if(getSpendingProfileChanged()) {
                        setUserToWaitingApproval();
                    }

                } else {
                    tosList = null;
                    if (getSpendingProfileChanged()){
                        updateEmployeeDetailsForLimits();
                    }
                }
            }

        } catch (Exception ex) {
            Logger.logMessage("ERROR: Could not determine is the account is required to accept a new TOS with the Account Group ID: "+ getAccountGroupID() + " and Spending Profile ID: "+ getSpendingProfileID() + " in AccountSettingCollection.checkIfTOSIsRequired()", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }
        return tosList;
    }

    //check if there is a TOS match for both the changed account group and spending profile
    public ArrayList<HashMap> checkTOSSpendingProfileAndAccountGroup() {
        ArrayList<HashMap> tosList = null;
        try {
            tosList = dm.parameterizedExecuteQuery("data.accountSettings.checkTOSSpendingProfileAndAccountGroup",
                    new Object[]{
                            getLoggedInEmployeeID(),
                            getAccountGroupID(),
                            getSpendingProfileID()
                    },
                    true
            );

            //if the size of the tosList is 0, then just set it to null because we didn't find anything
            if (tosList != null && tosList.size() == 0) {
                tosList = null;
            }
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
        return tosList;
    }

    //check if there is a TOS match for JUST this account's (employee's) spending profile (terminal profile)
    public ArrayList<HashMap> checkTOSSpendingProfile() {
        ArrayList<HashMap> tosList = null;
        try {
            tosList = dm.parameterizedExecuteQuery("data.accountSettings.checkTOSSpendingProfile",
                    new Object[]{
                            getLoggedInEmployeeID(),
                            getSpendingProfileID()
                    },
                    true
            );

            //if the size of the tosList is 0, then just set it to null because we didn't find anything
            if (tosList != null && tosList.size() == 0) {
                tosList = null;
            }
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
        return tosList;
    }

    //check if there is a TOS match for JUST this account's (employee's) account group (payroll group)
    public ArrayList<HashMap> checkTOSPayrollGroup() {
        ArrayList<HashMap> tosList = null;
        try {
            tosList = dm.parameterizedExecuteQuery("data.accountSettings.checkTOSAccountGroup",
                    new Object[]{
                            getLoggedInEmployeeID(),
                            getAccountGroupID()
                    },
                    true
            );

            //if the size of the tosList is 0, then just set it to null because we didn't find anything
            if (tosList != null && tosList.size() == 0) {
                tosList = null;
            }
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
        return tosList;
    }

    // BEGIN MEAL PLAN TRANSACTION FUNCTIONS

     public Integer createMealPlanTransaction(MealPlanModel mealPlanModel)  {
         Integer transactionID = null;
        try {
            TransactionModel transactionModel = this.transactionInquiry(mealPlanModel);

            if(!this.getErrorDetails().equals("")) {
                Logger.logMessage(this.getErrorDetails()+" in AccountSettingCollection.createMealPlanTransaction()", Logger.LEVEL.ERROR);
                return transactionID;
            }

            //turn off inquiry flag
            transactionModel.setIsInquiry(false);

            transactionModel.getLoyaltyAccount().setHasFetchedAvailableRewards(false);

            //always a sale, type = 1
            Integer transTypeID = 1;
            transactionModel.setTransactionTypeId(transTypeID);

            //get funding terminal
            Integer terminalID = transactionModel.getTerminalId();
            TerminalModel terminalModel = TerminalModel.createTerminalModel(terminalID, true);

            LocalDateTime orderSubmittedDateTime = LocalDateTime.now();

            //set submitted time string for use in submit transaction later
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
            String submittedTime = orderSubmittedDateTime.format(dtf);

            //datetime of transaction
            transactionModel.setTimeStamp(submittedTime);

            //order type and pickup/delivery note
            String orderType = "NORMAL SALE";
            transactionModel.setOrderType(orderType);

            //set primary employee id
            transactionModel.getLoyaltyAccountInfo(getLoggedInEmployeeID());

            //save transaction
            TransactionBuilder transactionBuilder = TransactionBuilder.createTransactionBuilder(terminalModel, transactionModel, PosAPIHelper.ApiActionType.SALE, PosAPIHelper.TransactionType.SALE);
            transactionBuilder.getTransactionModel().checkTransactionTypeEnum();
            transactionBuilder.buildTransaction();
            transactionBuilder.saveTransaction();

            //send confirmation email
            String emailAddress = "";
            AccountPaymentMethodModel paymentModel = mealPlanModel.getAccountPaymentMethodModel();
            if (paymentModel != null && paymentModel.getPaymentMethodEmailAddress() != null) {
                emailAddress = paymentModel.getPaymentMethodEmailAddress();
            }
            transactionBuilder.sendPurchaseConfirmationEmail(emailAddress);

            transactionID = transactionModel.getId();

        } catch (Exception ex) {
            Logger.logMessage("ERROR: Could not create PATransaction after Meal Plan purchase in AccountSettingCollection.createMealPlanTransaction()", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }

         return transactionID;
    }

    public TransactionModel transactionInquiry(MealPlanModel mealPlanModel) throws Exception {
        com.mmhayes.common.transaction.models.TransactionModel transactionModel = new com.mmhayes.common.transaction.models.TransactionModel();
        transactionModel.setApiActionEnum(PosAPIHelper.ApiActionType.SALE);

        //userID
        Integer userID = CommonAPI.getQCUserID();
        if (userID == null || userID == 0) {
            throw new MissingDataException();
        }
        transactionModel.setUserId(userID);

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
        transactionModel.setTimeStamp(LocalDateTime.now().format(dtf));

        // Funding Terminal ID
        Integer fundingTerminalID = mealPlanModel.getFundingTerminalID();
        if(fundingTerminalID == null) {
            Logger.logMessage("ERROR: Funding Terminal ID was not found in current Spending Profile.", Logger.LEVEL.ERROR);
            this.setErrorDetails("Spending Profile is not configured correctly");
            return transactionModel;
        }

        TerminalModel terminalModel = TerminalModel.createTerminalModel(fundingTerminalID, true);

        // Meal Plan Price
        setMealPlanPrice(mealPlanModel.getPrice());
        if(mealPlanModel.getPrice() == null) {
            this.setErrorDetails("Cannot find Price for Spending Profile");
            return transactionModel;
        }

        List<ProductModel> products = new ArrayList<>();

        Integer profilePurchasePAPluID = mealPlanModel.getProfilePurchasePAPluID();  // Profile Purchase Product
        if(profilePurchasePAPluID == null) {
            Logger.logMessage("ERROR:  Cannot find ProductPurchasePAPluID on Spending Profile the user is switching to.", Logger.LEVEL.ERROR);
            this.setErrorDetails("Spending Profile is not configured correctly");
            return transactionModel;
        }

        ProductModel profilePurchaseProduct = new ProductModel(profilePurchasePAPluID);

        //set the price of the product as the meal plan price
        profilePurchaseProduct.setPrice(CommonAPI.convertModelDetailToBigDecimal(this.getMealPlanPrice()));
        profilePurchaseProduct.setOriginalPrice(CommonAPI.convertModelDetailToBigDecimal(this.getMealPlanPrice()));
        products.add(profilePurchaseProduct);

        transactionModel.setProducts(populateProductsForSubmission(fundingTerminalID, products));

        if(mealPlanModel.getAddFundingTerminalFees()) {
            if(mealPlanModel.getFundingFee() == null) {
                Logger.logMessage("ERROR: Funding Fee was not found on funding terminal even though Add Funding Terminal Fees flag was on for Spending Profile.", Logger.LEVEL.ERROR);
                this.setErrorDetails("Convenience Fee is not configured correctly");
                return transactionModel;
            }

            if(mealPlanModel.getSurchargeID() == null) {
                Logger.logMessage("ERROR: Surcharge was not found on funding terminal even though Add Funding Terminal Fees flag was on for Spending Profile.", Logger.LEVEL.ERROR);
                this.setErrorDetails("Convenience Fee is not configured correctly");
                return transactionModel;
            }

            BigDecimal mealPlanTotal = mealPlanModel.getPrice();
            if(mealPlanModel.getMealPlanTaxModels().size() > 0) {
                for(TaxModel taxModel : mealPlanModel.getMealPlanTaxModels()) {
                    mealPlanTotal = mealPlanTotal.add(mealPlanModel.getPrice().multiply(taxModel.getTaxRate()));
                }
            }

            SurchargeLineItemModel surchargeLineItemModel = new SurchargeLineItemModel();
            surchargeLineItemModel.setId(mealPlanModel.getSurchargeID());
            surchargeLineItemModel.setItemId(mealPlanModel.getSurchargeID());
            surchargeLineItemModel.setEligibleAmount(mealPlanTotal);
            surchargeLineItemModel.setAmount(mealPlanModel.getFundingFee());
            surchargeLineItemModel.setExtendedAmount(mealPlanModel.getFundingFee());
            surchargeLineItemModel.setQuantity(BigDecimal.ONE);

            //validate Surcharge and set SurchargeDisplayModel
            SurchargeModel validatedSurchargeModel = SurchargeModel.getSurchargeModel(surchargeLineItemModel.getItemId(), terminalModel);
            SurchargeDisplayModel surchargeDisplayModel = SurchargeDisplayModel.createSurchargeModel(validatedSurchargeModel);
            surchargeLineItemModel.setSurcharge(surchargeDisplayModel);

            transactionModel.getSurcharges().add(surchargeLineItemModel);
        }

        //set the meal plan product tax on the transaction model
        if(mealPlanModel.getMealPlanTaxModels().size() > 0) {

            BigDecimal subtotal = mealPlanModel.getPrice();

            for(TaxModel taxModel : mealPlanModel.getMealPlanTaxModels()) {
                BigDecimal taxAmount = subtotal.multiply(taxModel.getTaxRate()).setScale(2, BigDecimal.ROUND_HALF_UP);

                TaxLineItemModel taxLineItemModel = new TaxLineItemModel();
                taxLineItemModel.setId(taxModel.getId());
                taxLineItemModel.setAmount(taxAmount);
                taxLineItemModel.setEligibleAmount(subtotal);
                taxLineItemModel.setTax(taxModel);

                transactionModel.getTaxes().add(taxLineItemModel);
            }

            profilePurchaseProduct.setTaxIDs(mealPlanModel.getMealPlanTaxIDs());
            setItemTax(transactionModel, fundingTerminalID);
        }

        if(mealPlanModel.getSurchargeTaxModels().size() > 0) {
            for(TaxModel surchargeTaxModel : mealPlanModel.getSurchargeTaxModels()) {
                BigDecimal taxAmount = mealPlanModel.getFundingFee().multiply(surchargeTaxModel.getTaxRate()).setScale(2, BigDecimal.ROUND_HALF_UP);;

                TaxLineItemModel taxLineItemModel = new TaxLineItemModel();
                taxLineItemModel.setId(surchargeTaxModel.getId());
                taxLineItemModel.setAmount(taxAmount);
                taxLineItemModel.setEligibleAmount(mealPlanModel.getFundingFee());
                taxLineItemModel.setTax(surchargeTaxModel);

                ItemTaxModel itemTaxModel = new ItemTaxModel();
                itemTaxModel.setItemId(taxLineItemModel.getId());
                itemTaxModel.setItemTypeId(taxLineItemModel.getItemTypeId());
                itemTaxModel.setAmount(taxLineItemModel.getAmount());
                itemTaxModel.setEligibleAmount(taxLineItemModel.getEligibleAmount());
                itemTaxModel.setTax(surchargeTaxModel);

                transactionModel.getSurcharges().get(0).getTaxes().add(itemTaxModel);
                transactionModel.getTaxes().add(taxLineItemModel);
            }
        }

        // Funding Terminal CC Tender ID
        Integer tenderID = mealPlanModel.getCCTenderID();
        if(mealPlanModel.getCCTenderID() == null) {
            Logger.logMessage("ERROR: Cannot find Credit Card tenderID for Funding Terminal's Revenue Center.", Logger.LEVEL.ERROR);
            this.setErrorDetails("Spending Profile is not configured correctly");
            return transactionModel;
        }

       //populate tender
        TenderDisplayModel tenderDisplayModel = new TenderDisplayModel();
        tenderDisplayModel.setId(tenderID); //set TenderID as the Credit Card Tender on the Funding Terminal

        AccountModel accountModel = new AccountModel();
        accountModel.setId(this.getLoggedInEmployeeID());

        TenderLineItemModel tenderLineItemModel = new TenderLineItemModel();
        tenderLineItemModel.setTender(tenderDisplayModel);
        tenderLineItemModel.setItemId(tenderID);
        tenderLineItemModel.setItemTypeId(PosAPIHelper.ObjectType.TENDER.toInt());
        tenderLineItemModel.setCreditCardTransInfo(CommonAPI.getCreditCardTransInfo(null, false));

        //set the tender amount to the complete total amount of the meal plan with tax and fee if configured
        BigDecimal total = mealPlanModel.getTotal();

        tenderLineItemModel.setAmount(total);
        tenderLineItemModel.setExtendedAmount(total);
        tenderLineItemModel.setQuantity(BigDecimal.ONE);
        tenderLineItemModel.setAccount(accountModel);

        List<TenderLineItemModel> tenders = new ArrayList<TenderLineItemModel>();
        tenders.add(tenderLineItemModel);
        transactionModel.setTenders(tenders);

        LoyaltyAccountModel loyaltyAccountModel = new LoyaltyAccountModel();
        loyaltyAccountModel.setId(this.getLoggedInEmployeeID());
        transactionModel.setLoyaltyAccount(loyaltyAccountModel);

        transactionModel.setTerminalId(fundingTerminalID);

        TransactionBuilder transactionBuilder = TransactionBuilder.createTransactionBuilder(terminalModel, transactionModel, PosAPIHelper.ApiActionType.SALE);
        transactionBuilder.getTransactionModel().setIsInquiry(true);
        transactionBuilder.buildTransaction();
        transactionBuilder.inquire();

        return transactionBuilder.getTransactionModel();
    }

    private List<ProductLineItemModel> populateProductsForSubmission(Integer terminalID, List<ProductModel> products) throws Exception {
        List<ProductLineItemModel> productLines = new ArrayList<>();

        for (ProductModel product : products) {

            ProductLineItemModel productLineItemModel = new ProductLineItemModel();

            HashMap productHM = new HashMap();
            productHM.put("ID", product.getID());
            productHM.put("NAME", product.getName());
            com.mmhayes.common.product.models.ProductModel productModel = new com.mmhayes.common.product.models.ProductModel(productHM, terminalID);
            List<LoyaltyRewardModel> rewards = new ArrayList<>();
            productModel.setRewards(rewards);
            productModel.setDiscountIds("");
            productLineItemModel.setProduct(productModel);
            productLineItemModel.setItemTypeId(PosAPIHelper.ObjectType.PRODUCT.toInt());
            productLineItemModel.setItemId(product.getID());
            productLineItemModel.setQuantity(BigDecimal.ONE);
            productLineItemModel.setAmount(product.getOriginalPrice() == null ? product.getPrice() : product.getOriginalPrice());

            //add modifiers as their own products
            if (product.getModifiers() != null && product.getModifiers().size() > 0) {
                ArrayList<ModifierLineItemModel> modifiers = new ArrayList<>();

                productLineItemModel.setModifiers(modifiers);
            }

            productLines.add(productLineItemModel);
        }

        return productLines;
    }

    public void setItemTax(TransactionModel transactionModel, Integer terminalID) throws Exception {
        if(transactionModel.getTaxes().size() > 0) {

            ProductLineItemModel productLineItemModel = transactionModel.getProducts().get(0);

            for(TaxLineItemModel taxLineItemModel : transactionModel.getTaxes()) {
                ItemTaxModel itemTaxModel = new ItemTaxModel();
                itemTaxModel.setItemId(taxLineItemModel.getId());
                itemTaxModel.setItemTypeId(taxLineItemModel.getItemTypeId());
                itemTaxModel.setAmount(taxLineItemModel.getAmount());
                itemTaxModel.setEligibleAmount(taxLineItemModel.getEligibleAmount());
                itemTaxModel.setTax(taxLineItemModel.getTax());

                productLineItemModel.getTaxes().add(itemTaxModel);
            }
        }

    }

    // BEGIN HELPER FUNCTIONS

    //get the account's balance, if it has a balance set up and what the name of the Account Funding page should be
    public static MealPlanModel calculateMealPlanAmount(String spendingProfileID, HttpServletRequest request) throws Exception{
        MealPlanModel mealPlanModel = new MealPlanModel();
        mealPlanModel.setChosenSpendingProfileID(Integer.parseInt(spendingProfileID));

        AccountPaymentMethodCollection accountPaymentMethod = new AccountPaymentMethodCollection(request);

        AccountPaymentMethodModel accountPaymentMethodModel = null;
        if(accountPaymentMethod.getCollection().size() > 0) {
            accountPaymentMethodModel = accountPaymentMethod.getCollection().get(0);
        }

        mealPlanModel.setAccountPaymentMethodModel(accountPaymentMethodModel);

        String accountFundingName = "Account Funding";
        boolean doesPayrollGroupAllowFunding = CommonAPI.checkIfPayrollGroupAllowsFunding(request);

        if(!doesPayrollGroupAllowFunding) {
            accountFundingName = "Payment Method";
        }

        mealPlanModel.setAccountFundingName(accountFundingName);


        if(accountPaymentMethodModel != null) {
            Integer employeeID = CommonAuthResource.checkRequestHeaderForMMHayesAuth(request);

            Integer fundingTerminalID = null;
            boolean addFundingFees = false;

            ArrayList<HashMap> fundingTerminalDetails = dm.parameterizedExecuteQuery("data.accountSettings.getFundingTerminalDetails",
                    new Object[]{
                            employeeID
                    },
                    true
            );

            if(fundingTerminalDetails != null && fundingTerminalDetails.size() > 0) {
                HashMap fundingTerminalHM = fundingTerminalDetails.get(0);
                if(fundingTerminalHM.get("FUNDINGTERMINALID") != null && !fundingTerminalHM.get("FUNDINGTERMINALID").toString().equals("")) {
                    fundingTerminalID = CommonAPI.convertModelDetailToInteger(fundingTerminalHM.get("FUNDINGTERMINALID"));
                    mealPlanModel.setFundingTerminalID(fundingTerminalID);
                }

                if(fundingTerminalHM.get("ADDFUNDINGTERMINALFEES") != null && !fundingTerminalHM.get("ADDFUNDINGTERMINALFEES").toString().equals("")) {
                    addFundingFees = CommonAPI.convertModelDetailToBoolean(fundingTerminalHM.get("ADDFUNDINGTERMINALFEES"));
                    mealPlanModel.setAddFundingTerminalFees(addFundingFees);
                }

                if(fundingTerminalHM.get("PASURCHARGEID") != null && !fundingTerminalHM.get("PASURCHARGEID").toString().equals("")) {
                    mealPlanModel.setSurchargeID(CommonAPI.convertModelDetailToInteger(fundingTerminalHM.get("PASURCHARGEID")));
                }

                if(fundingTerminalHM.get("SURCHARGENAME") != null && !fundingTerminalHM.get("SURCHARGENAME").toString().equals("")) {
                    mealPlanModel.setSurchargeName(CommonAPI.convertModelDetailToString(fundingTerminalHM.get("SURCHARGENAME")));
                }

                if(fundingTerminalHM.get("CCTENDERID") != null && !fundingTerminalHM.get("CCTENDERID").toString().equals("")) {
                    mealPlanModel.setCCTenderID(CommonAPI.convertModelDetailToInteger(fundingTerminalHM.get("CCTENDERID")));
                }

                if(mealPlanModel.getAddFundingTerminalFees() && fundingTerminalHM.get("TAXIDS") != null && !fundingTerminalHM.get("TAXIDS").toString().equals("")) {
                    String surchargeTaxIDs = CommonAPI.convertModelDetailToString(fundingTerminalHM.get("TAXIDS"));
                    mealPlanModel.setSurchargeTaxIDs(surchargeTaxIDs);

                    List<String> surchargeTaxes = new ArrayList<String>(Arrays.asList(surchargeTaxIDs.split(",")));

                    for(String tax : surchargeTaxes) {
                        TaxModel taxModel = TaxModel.getTaxModel(Integer.parseInt(tax), fundingTerminalID);

                        if( !taxModel.isRevCenterMapped()) {
                            continue;
                        }

                        mealPlanModel.getSurchargeTaxModels().add(taxModel);
                    }
                }
            }

            BigDecimal mealPlanPrice = BigDecimal.ZERO;

            ArrayList<HashMap> spendingProfileDetails = dm.parameterizedExecuteQuery("data.accountSettings.getSpendingProfileDetails",
                    new Object[]{
                            spendingProfileID
                    },
                    true
            );

            if ( spendingProfileDetails != null && spendingProfileDetails.size() > 0) {
                HashMap spendingProfileHM = spendingProfileDetails.get(0);

                if(spendingProfileHM.get("PRICE") != null && !spendingProfileHM.get("PRICE").toString().equals("")) {
                    mealPlanPrice = CommonAPI.convertModelDetailToBigDecimal(spendingProfileHM.get("PRICE"));
                    mealPlanModel.setPrice(mealPlanPrice);
                }

                if(spendingProfileHM.get("PROFILEPURCHASEPAPLUID") != null && !spendingProfileHM.get("PROFILEPURCHASEPAPLUID").toString().equals("")) {
                    mealPlanModel.setProfilePurchasePAPluID(CommonAPI.convertModelDetailToInteger(spendingProfileHM.get("PROFILEPURCHASEPAPLUID")));
                }

                if(spendingProfileHM.get("TAXIDS") != null && !spendingProfileHM.get("TAXIDS").toString().equals("")) {
                    String taxIDs = CommonAPI.convertModelDetailToString(spendingProfileHM.get("TAXIDS"));
                    mealPlanModel.setMealPlanTaxIDs(taxIDs);

                    List<String> taxes = new ArrayList<String>(Arrays.asList(taxIDs.split(",")));

                    for(String tax : taxes) {
                        if(Integer.parseInt(tax) <= 0) {
                            continue;
                        }

                        TaxModel taxModel = TaxModel.getTaxModel(Integer.parseInt(tax), fundingTerminalID);

                        if( !taxModel.isRevCenterMapped()) {
                            continue;
                        }

                        mealPlanModel.getMealPlanTaxModels().add(taxModel);
                    }
                }
            }

            BigDecimal subtotal = mealPlanPrice;
            BigDecimal taxTotal = BigDecimal.ZERO;
            if(mealPlanModel.getMealPlanTaxModels().size() > 0) {
                for(TaxModel mealPlanTaxModel : mealPlanModel.getMealPlanTaxModels()) {
                    BigDecimal tax = mealPlanPrice.multiply(mealPlanTaxModel.getTaxRate()).setScale(2, BigDecimal.ROUND_HALF_UP);
                    taxTotal = taxTotal.add(tax);
                }
            }

            subtotal = subtotal.add(taxTotal);

            BigDecimal fundingFee = BigDecimal.ZERO;
            if(addFundingFees && accountPaymentMethodModel.getTerminalHasFundingFee()) {
                AccountPaymentMethodModel paymentMethodModel = FundingHelper.determineFundingFee(subtotal, request);
                fundingFee = paymentMethodModel.getFundingFee();

                if(fundingFee.compareTo(BigDecimal.ZERO) > 0) {
                    mealPlanModel.setFundingFee(fundingFee);

                    if(mealPlanModel.getSurchargeTaxModels().size() > 0) {
                        for(TaxModel surchargeTaxModel : mealPlanModel.getSurchargeTaxModels()) {
                            BigDecimal surchargeTax = fundingFee.multiply(surchargeTaxModel.getTaxRate()).setScale(2, BigDecimal.ROUND_HALF_UP);
                            subtotal = subtotal.add(surchargeTax);
                            taxTotal = taxTotal.add(surchargeTax);
                        }
                    }

                    mealPlanModel.setSubtotal(subtotal);
                }
            }

            BigDecimal total = subtotal;

            if(taxTotal.compareTo(BigDecimal.ZERO) > 0) {
                mealPlanModel.setTaxTotal(taxTotal);
            }

            if(fundingFee.compareTo(BigDecimal.ZERO) > 0) {
                total = total.add(fundingFee);
            }

            mealPlanModel.setTotal(total);
        }

        return mealPlanModel;
    }

    //get the account's balance, if it has a balance set up and what the name of the Account Funding page should be
    public static HashMap getAccountBalanceAndPaymentMethod(HttpServletRequest request) throws Exception{
        HashMap result = new HashMap();

        AccountPaymentMethodCollection accountPaymentMethod = new AccountPaymentMethodCollection(request);
        result.put("accountPaymentMethod", accountPaymentMethod);

        String accountFundingName = "Account Funding";
        boolean doesPayrollGroupAllowFunding = hasAbilityToFundAccount(request);
        boolean isPayAsYouGoAllowed = isPayAsYouGoAllowed(request);

        if(!doesPayrollGroupAllowFunding) {
            accountFundingName = "Payment Method";
        }

        result.put("allowMyQCFunding", doesPayrollGroupAllowFunding);
        result.put("accountFundingName", accountFundingName);
        result.put("allowPayAsYouGo", isPayAsYouGoAllowed);

        boolean hasPaymentMethod = accountPaymentMethod.getCollection().size() != 0;
        String errorMsg = "";

        //get user's current balance
        BigDecimal total = CommonAPI.convertModelDetailToBigDecimal("0.01");

        HashMap userBalanceHM = CommonAPI.getUserBalance(request, total);
        BigDecimal userBalance = CommonAPI.convertModelDetailToBigDecimal(userBalanceHM.get("USERBALANCE"));
        String userBalanceType = CommonAPI.convertModelDetailToString(userBalanceHM.get("USERBALANCETYPE"));
        String userBalanceAmtStr = userBalance.setScale(2, BigDecimal.ROUND_FLOOR).toString();

        // If the account doesn't have a balance
        if ( userBalance.compareTo(BigDecimal.ZERO) <= 0 ) {
            userBalance = BigDecimal.ZERO;

            if( Double.parseDouble(userBalanceAmtStr) < 0.00) {
                userBalanceAmtStr = "0.00";
            }

            Integer storeID = CommonAPI.checkRequestHeaderForStoreID(request);
            StoreModel storeModel = StoreModel.getStoreModel(storeID, request);

            // If the account doesn't have a Payment Method
            if( !hasPaymentMethod ) {

                // If store and account group allow use of a credit card
                if( storeModel.isUsesCreditCardAsTender() && isPayAsYouGoAllowed ) {
                    errorMsg = "You must set up a payment method on the "+accountFundingName+ " page before continuing";

                 // Otherwise the store only allows Quickcharge Balance - tell them to setup a payment method so they can add funds
                } else {

                    errorMsg = "You do not have enough available funds to complete a transaction";
                    switch (userBalanceType) {
                        case "store":
                            errorMsg = errorMsg.concat(" at this location. You have $".concat(userBalanceAmtStr).concat(" available to spend in this location."));
                            break;
                        case "global":
                            errorMsg = errorMsg.concat(". You have $".concat(userBalanceAmtStr).concat(" available to spend."));
                            break;
                        case "transaction":
                            errorMsg = errorMsg.concat(" at this location today. You have $".concat(userBalanceAmtStr).concat(" available to spend."));
                            break;
                    }

                    errorMsg += " In order to add funds you must set up a payment method on the "+accountFundingName+" page.";
                }
            }

            result.put("errorMessage", errorMsg);
        }

        result.put("userBalance", userBalance);


        return result;
    }

    //get the account's balance, if it has a balance set up and what the name of the Account Funding page should be
    public static HashMap getAccountBalance(HttpServletRequest request) throws Exception{
        HashMap result = new HashMap();

        //get user's current balance
        BigDecimal userBalance = BigDecimal.ZERO;

        //get the authenticated account (aka logged in user)
        Integer authenticatedAccountID;
        if ( CommonAuthResource.isKOA( request ) ) {
            authenticatedAccountID = CommonAuthResource.getAccountIDFromAccountHeader(request);
        } else {
            authenticatedAccountID = CommonAuthResource.checkRequestHeaderForMMHayesAuth(request);
        }

        if (authenticatedAccountID == null) {
            return null;
        }

        //get logged in employees global balance
        ArrayList<HashMap> globalBalanceList = dm.parameterizedExecuteQuery("data.myqc.getEmployeeBalance",
                new Object[]{
                        authenticatedAccountID
                },
                true
        );

        if(globalBalanceList != null && globalBalanceList.size() > 0) {
            HashMap tempGlobalBalanceHM = globalBalanceList.get(0);
            if (tempGlobalBalanceHM != null) {

                if (tempGlobalBalanceHM.get("GLOBALBALANCE") != null && !tempGlobalBalanceHM.get("GLOBALAVAILABLE").toString().isEmpty()) {
                    userBalance  = new BigDecimal(tempGlobalBalanceHM.get("GLOBALBALANCE").toString());
                }

                if (tempGlobalBalanceHM.get("ACCOUNTTYPEID") != null && !tempGlobalBalanceHM.get("ACCOUNTTYPEID").toString().isEmpty()) {
                    Integer accountTypeID = CommonAPI.convertModelDetailToInteger(tempGlobalBalanceHM.get("ACCOUNTTYPEID"));
                    if(accountTypeID == 2 || accountTypeID == 4) {
                        userBalance = userBalance.multiply(new BigDecimal(-1));
                    }
                }
            }
        }

        result.put("userBalance", userBalance);

        return result;
    }

    //check if the account has the ability to fund, check for funding terminal in spending profile and if My QC Funding is on for the Account Group
    public static boolean hasAbilityToFundAccount(HttpServletRequest request) throws Exception {
        boolean hasAbilityToFund = false;

        Integer employeeID = CommonAuthResource.checkRequestHeaderForMMHayesAuth(request);

        ArrayList<HashMap> fundingList = dm.parameterizedExecuteQuery("data.accountSettings.hasAbilityToFundAccount",
                new Object[]{
                        employeeID
                },
                true
        );

        if(fundingList != null && fundingList.size() > 0) {
            HashMap fundingListHM = fundingList.get(0);
            if(fundingListHM.get("ALLOWMYQCFUNDING") != null && fundingListHM.get("ALLOWMYQCFUNDING").toString().equals("true")) {
                hasAbilityToFund = true;
            }
        }

        return hasAbilityToFund;
    }

    // Check if the Account Group has pay-as-you-go allowed
    public static boolean isPayAsYouGoAllowed(HttpServletRequest request) throws Exception {
        Integer employeeID = CommonAuthResource.checkRequestHeaderForMMHayesAuth(request);
        Object payAsYouGoResult = dm.parameterizedExecuteScalar("data.accountSettings.isPayAsYouGoAllowed",
                new Object[]{
                        employeeID
                }
        );
        if (payAsYouGoResult != null) {
            return (Boolean)payAsYouGoResult;
        }
        return true;
    }

    //checks if there is a funding terminal in the user's spending profile
    public boolean isFundingTerminalInSpendingProfile() {
        boolean isFundingTerminalInSpendingProfile = false;
        try {

            if(getOriginalSpendingProfile() == null) {
                return false;
            }

            ArrayList<HashMap> fundingTerminalID = dm.parameterizedExecuteQuery("data.accountSettings.getFundingTerminalInSpendingProfile",
                    new Object[]{
                            getOriginalSpendingProfile()
                    },
                    true
            );

            if(fundingTerminalID != null && fundingTerminalID.size() > 0) {
                HashMap fundingTerminalHM = fundingTerminalID.get(0);
                if(fundingTerminalHM.containsKey("TERMINALID") && !fundingTerminalHM.get("TERMINALID").toString().equals("")) {
                    isFundingTerminalInSpendingProfile = true;
                }
            }

        } catch (Exception ex) {
            Logger.logMessage("ERROR: Could not determine if there is a funding terminal in the Employee's spending profile for employee ID: "+getLoggedInEmployeeID()+" in AccountSettingCollection.isFundingTerminalInSpendingProfile()", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }
        return isFundingTerminalInSpendingProfile;
    }

    //determines if the employee's account group has the MyQCForceChange bit on or not
    public boolean checkIfForceChangeIsOn() {
        boolean forceChange = false;
        try {
            ArrayList<HashMap> forceChangeList = dm.parameterizedExecuteQuery("data.accountSettings.getForceChangeInfo",
                    new Object[]{
                            getLoggedInEmployeeID()
                    },
                    true
            );

            if(forceChangeList != null && forceChangeList.size() > 0) {
                HashMap forceChangeHM = forceChangeList.get(0);
                if(forceChangeHM.containsKey("FORCECHANGE") && forceChangeHM.get("FORCECHANGE").toString().equals("true")) {
                    forceChange = true;
                }
            }

        } catch (Exception ex) {
            Logger.logMessage("ERROR: Could not determine the Force Change bit was on the employee's payroll group for employee ID: "+getLoggedInEmployeeID()+" in AccountSettingCollection.checkIfForceChangeIsOn()", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }

        return forceChange;
    }

    public void checkIfAccountTypeNeedsChange() {
        try {
            //get all models in an array list
            ArrayList<HashMap> accountTypes = dm.parameterizedExecuteQuery("data.accountSettings.checkIfAccountTypeNeedsChange",
                    new Object[]{
                            getLoggedInEmployeeID(),
                            getAccountGroupID()
                    },
                    true
            );

            if(accountTypes != null && accountTypes.size() > 0) {
                HashMap accountTypeHM = accountTypes.get(0);
                if(accountTypeHM.containsKey("ACCOUNTTYPEID") && !accountTypeHM.get("ACCOUNTTYPEID").equals("")) {
                    setAccountTypeChange(Integer.parseInt(accountTypeHM.get("ACCOUNTTYPEID").toString()));
                }
            }
        } catch (Exception ex) {
            Logger.logMessage("ERROR: Could not determine if the account type needs to be changed from the account group ID: "+getAccountGroupID()+" in AccountSettingCollection.checkIfAccountTypeNeedsChange()", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }
    }

    //if the limits are going to change in real time the TerminalProfileID in QC_Employees has to be changed when the spending profile selector is changed
    //if the spending profile is changing to one that needs a new TOS, can't let the user stay as Active, need to change to Waiting
    public void setUserToWaitingApproval() {
        try {

            //account group changed
            if(getAccountGroupChanged() && getAccountTypeChange() == null && !getSpendingProfileChanged()) {
                updateAccountGroupAndStatus("Waiting for Signup", "W");

                //account group and account type changed
            } else if (getAccountGroupChanged() && getAccountTypeChange() != null && !getSpendingProfileChanged() ) {
                updateAccountGroupAccountTypeAndStatus("Waiting for Signup", "W");

                //spending profile changed
            } else if(!getAccountGroupChanged() && getSpendingProfileChanged()){  //otherwise just update the spending profile and account status
                updateSpendingProfileAndStatus("Waiting for Signup", "W");

                //account group and spending profile changed
            } else if (getAccountGroupChanged() && getAccountTypeChange() == null && getSpendingProfileChanged()) {
                updateAccountGroupSpendingProfileAccountStatus("Waiting for Signup", "W");

                //account group, account type and spending profile changed
            } else if (getAccountGroupChanged() && getAccountTypeChange() != null && getSpendingProfileChanged()) {
                updateAccountGroupSpendingProfileAccountTypeAccountStatus("Waiting for Signup", "W");

            }

        } catch (Exception ex) {
            Logger.logMessage("ERROR: Could not set user to Waiting Approval for EmployeeID : "+ getLoggedInEmployeeID() + " after determining they need to accept another TOS for this spending profile: "+getSpendingProfileID()+" in AccountSettingCollection.setUserToWaitingApproval()", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }
    }

    //update the spending profile so the limits are correct in the Account Settings page-view
    public void updateEmployeeDetailsForLimits() {
        try {

            //this means the user was changed to a spending profile or account group that needed a tos but now it doesn't anymore
            if(getNeedsTOS()) {
                checkIfAccountStatusNeedsChange();
            }

            // if the account group also has been changed, update QC_EmployeeChanges with the changed account group, account type, and spending profile
            if(getAccountGroupChanged()) {

                //if the account group is changed to one that has a different account type then also change the account type
                if(getAccountTypeChange() != null) {
                    updateAccountGroupSpendingProfileAccountType();

                    //otherwise just update the account group and spending profile
                } else {
                    updateAccountGroupSpendingProfile();
                }

            } else {  //otherwise just update the spending profile
                updateSpendingProfile();
            }
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
            Logger.logMessage("ERROR: Could not LOG account spending profile change for account ID: "+getLoggedInEmployeeID(), Logger.LEVEL.ERROR);
        }
    }

    //check if the account group and account type needs to be changed in QC_Employees
    public boolean checkIfAccountGroupAccountTypeNeedsChange() {
        boolean needsChange = true;
        try {
            ArrayList<HashMap> accountSettings = dm.parameterizedExecuteQuery("data.accountSettings.checkIfAccountGroupAccountTypeNeedsChange",
                    new Object[]{
                            getLoggedInEmployeeID(),
                            getAccountGroupID(),
                            getAccountTypeChange()
                    },
                    true
            );

            //if the size of the accountSettings is 0, then update the account group and type
            if (accountSettings != null && accountSettings.size() == 0) {
                updateAccountGroupAndAccountType();
            }
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
            needsChange = false;
        }
        return needsChange;
    }

    //check if the account group needs to be changed in QC_Employees
    public boolean checkIfAccountGroupNeedsChange() {
        boolean needsChange = true;
        try {
            ArrayList<HashMap> accountSettings = dm.parameterizedExecuteQuery("data.accountSettings.checkIfAccountGroupNeedsChange",
                    new Object[]{
                            getLoggedInEmployeeID(),
                            getAccountGroupID()
                    },
                    true
            );

            //if the size of the accountSettings is 0, then update the account group
            if (accountSettings != null && accountSettings.size() == 0) {
                updateAccountGroup();
            }
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
            needsChange = false;
        }
        return needsChange;
    }

    public void checkIfAccountStatusNeedsChange() {
        try {
            ArrayList<HashMap> accountSettings = dm.parameterizedExecuteQuery("data.accountSettings.checkIfAccountStatusNeedsChange",
                    new Object[]{
                            getLoggedInEmployeeID(),
                            "W"
                    },
                    true
            );

            //if the size of the accountSettings is 0, then update the account status back to active
            if (accountSettings != null && accountSettings.size() > 0) {
                updateAccountStatus("Active", "A");
            }
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
    }

    // BEGIN FUNCTIONS FOR UPDATING QC_Employees AND CHECKING QC_EmployeeChanges

    //updates the Account Group, Spending Profile and Account Type in QC_EmployeeChanges and QC_Employees
    public void updateAccountGroupSpendingProfileAccountType() {
        if(updateAccountGroupEmployeeChanges() && updateAccountTypeEmployeeChanges() && updateSpendingProfileEmployeeChanges()) {
            Integer updateAccountDetails = dm.parameterizedExecuteNonQuery("data.accountSettings.updateAccountSpendingProfileAccountGroupAndType",
                    new Object[]{
                            getLoggedInEmployeeID(),
                            getAccountGroupID(),
                            getSpendingProfileID(),
                            getAccountTypeChange()
                    }
            );

            if (updateAccountDetails != 1) {
                Logger.logMessage("Could not update account group, account type and spending profile in AccountSettingCollection.updateAccountGroupSpendingProfileAccountType", Logger.LEVEL.ERROR);
            }
        }
    }

    //updates the Account Group and Spending Profile in QC_EmployeeChanges and QC_Employees
    public void updateAccountGroupSpendingProfile() {
        if(updateAccountGroupEmployeeChanges() && updateSpendingProfileEmployeeChanges()) {
            Integer updateAccountDetails = dm.parameterizedExecuteNonQuery("data.accountSettings.updateAccountSpendingProfileAccountGroup",
                    new Object[]{
                            getLoggedInEmployeeID(),
                            getAccountGroupID(),
                            getSpendingProfileID()
                    }
            );

            if (updateAccountDetails != 1) {
                Logger.logMessage("Could not update account group, account type and spending profile in AccountSettingCollection.updateAccountGroupSpendingProfile", Logger.LEVEL.ERROR);
            }
        }
    }

    //updates the Spending Profile in QC_EmployeeChanges and QC_Employees
    public void updateSpendingProfile() {
        if(updateSpendingProfileEmployeeChanges()) {
            Integer updateAccountSpendingProfile = dm.parameterizedExecuteNonQuery("data.myqc.updateEmployeeSpendingProfile",
                    new Object[]{
                            getLoggedInEmployeeID(),
                            getSpendingProfileID()
                    }
            );

            if (updateAccountSpendingProfile != 1) {
                Logger.logMessage("ERROR: Could not update spending profile in AccountSettingsCollection.updateEmployeeSpendingProfile() - ID = "+getSpendingProfileID(), Logger.LEVEL.ERROR);
            }
        }
    }

    //updates the Account Group, Spending Profile, Account Type and Account Status in QC_EmployeeChanges and QC_Employees
    public void updateAccountGroupSpendingProfileAccountTypeAccountStatus(String status, String statusCharacter) {
        if(updateAccountGroupEmployeeChanges() && updateAccountTypeEmployeeChanges() && updateSpendingProfileEmployeeChanges() && updateAccountStatusEmployeeChanges(status)) {
            Integer updateAccountDetails = dm.parameterizedExecuteNonQuery("data.accountSettings.updateAccountStatusAndAllDetails",
                    new Object[]{
                            getLoggedInEmployeeID(),
                            getAccountGroupID(),
                            getSpendingProfileID(),
                            getAccountTypeChange(),
                            statusCharacter
                    }
            );

            if (updateAccountDetails != 1) {
                Logger.logMessage("Could not update Account Status to Waiting Approval in AccountSettingCollection.updateAccountGroupSpendingProfileAccountTypeAccountStatus", Logger.LEVEL.ERROR);
            }
        }
    }

    //updates the Account Group, Spending Profile and Account Status in QC_EmployeeChanges and QC_Employees
    public void updateAccountGroupSpendingProfileAccountStatus(String status, String statusCharacter) {
        if(updateAccountGroupEmployeeChanges() && updateSpendingProfileEmployeeChanges() && updateAccountStatusEmployeeChanges(status)) {
            Integer updateAccountDetails = dm.parameterizedExecuteNonQuery("data.accountSettings.updateAccountStatusSpendingProfileAccountGroup",
                    new Object[]{
                            getLoggedInEmployeeID(),
                            getAccountGroupID(),
                            getSpendingProfileID(),
                            statusCharacter
                    }
            );

            if (updateAccountDetails != 1) {
                Logger.logMessage("Could not update Account Status to Waiting Approval in AccountSettingCollection.updateAccountGroupSpendingProfileStatus", Logger.LEVEL.ERROR);
            }
        }
    }

    //updates the Spending Profile and Status in QC_EmployeeChanges and QC_Employees
    public void updateSpendingProfileAndStatus(String status, String statusCharacter) {
        if(updateSpendingProfileEmployeeChanges() && updateAccountStatusEmployeeChanges(status)) {
            Integer updateAccountStatus = dm.parameterizedExecuteNonQuery("data.accountSettings.updateAccountStatusAndSpendingProfile",
                    new Object[]{
                            getLoggedInEmployeeID(),
                            getSpendingProfileID(),
                            statusCharacter
                    }
            );

            if (updateAccountStatus != 1) {
                Logger.logMessage("Could not update Account Status to Waiting Approval in AccountSettingCollection.updateSpendingProfileAndStatus", Logger.LEVEL.ERROR);
            }
        }
    }

    //updates the Account Group, Account Type and Status in QC_EmployeeChanges and QC_Employees
    public void updateAccountGroupAccountTypeAndStatus(String status, String statusCharacter) {
        if(updateSpendingProfileEmployeeChanges() && updateAccountStatusEmployeeChanges(status)) {
            Integer updateAccountStatus = dm.parameterizedExecuteNonQuery("data.accountSettings.updateAccountStatusAccountGroupAndType",
                    new Object[]{
                            getLoggedInEmployeeID(),
                            getAccountGroupID(),
                            getAccountTypeChange(),
                            statusCharacter
                    }
            );

            if (updateAccountStatus != 1) {
                Logger.logMessage("Could not update Account Status to Waiting Approval in AccountSettingCollection.updateAccountGroupAccountTypeAndStatus", Logger.LEVEL.ERROR);
            }
        }
    }

    //updates the Account Group and Status in QC_EmployeeChanges and QC_Employees
    public void updateAccountGroupAndStatus(String status, String statusCharacter) {
        if(updateAccountGroupEmployeeChanges() && updateAccountStatusEmployeeChanges(status)) {
            Integer updateAccountStatus = dm.parameterizedExecuteNonQuery("data.accountSettings.updateAccountStatusAccountGroup",
                    new Object[]{
                            getLoggedInEmployeeID(),
                            getAccountGroupID(),
                            statusCharacter
                    }
            );

            if (updateAccountStatus != 1) {
                Logger.logMessage("Could not update Account Status to Waiting Approval in AccountSettingCollection.updateAccountGroupAndStatus", Logger.LEVEL.ERROR);
            }
        }
    }

    //updates the Account Group in QC_EmployeeChanges and QC_Employees
    public void updateAccountGroup() {
        if(updateAccountGroupEmployeeChanges()) {
            Integer updateAccountStatus = dm.parameterizedExecuteNonQuery("data.accountSettings.updateAccountGroup",
                    new Object[]{
                            getLoggedInEmployeeID(),
                            getAccountGroupID()
                    }
            );

            if (updateAccountStatus != 1) {
                Logger.logMessage("Could not update Account Group in AccountSettingCollection.updateAccountGroup", Logger.LEVEL.ERROR);
            }
        }
    }

    //updates the Account Status in QC_EmployeeChanges and QC_Employees
    public void updateAccountStatus(String status, String statusCharacter) {
        if(updateAccountStatusEmployeeChanges(status)) {
            Integer updateAccountStatus = dm.parameterizedExecuteNonQuery("data.accountSettings.updateAccountStatus",
                    new Object[]{
                            getLoggedInEmployeeID(),
                            statusCharacter
                    }
            );

            if (updateAccountStatus != 1) {
                Logger.logMessage("Could not update Account Group in AccountSettingCollection.updateAccountGroup", Logger.LEVEL.ERROR);
            }
        }
    }

    //updates the Account Group and Account Type in QC_EmployeeChanges and QC_Employees
    public void updateAccountGroupAndAccountType() {
        if(updateAccountGroupEmployeeChanges() && updateAccountTypeEmployeeChanges()) {
            Integer updateAccountStatus = dm.parameterizedExecuteNonQuery("data.accountSettings.updateAccountGroupAndAccountType",
                    new Object[]{
                            getLoggedInEmployeeID(),
                            getAccountGroupID(),
                            getAccountTypeChange()
                    }
            );

            if (updateAccountStatus != 1) {
                Logger.logMessage("Could not update Account Group and Account Type in AccountSettingCollection.updateAccountGroup", Logger.LEVEL.ERROR);
            }
        }
    }

    //updates the Account Email in QC_EmployeeChanges and QC_Employees
    public void updateAccountEmail() {
        if(updateAccountEmailEmployeeChanges()) {
            Integer updateAccountStatus = dm.parameterizedExecuteNonQuery("data.accountSettings.updateAccountEmail",
                    new Object[]{
                            getLoggedInEmployeeID(),
                            getEmail()
                    }
            );

            if (updateAccountStatus != 1) {
                Logger.logMessage("Could not update Account Email in AccountSettingCollection.updateAccountEmail", Logger.LEVEL.ERROR);
            }
        }
    }

    //updates the Person's Low Balance Threshold in QC_Person
    public void updatePersonLowBalanceThreshold() {
        if(getLowBalanceThreshold().equals("")) {
            setLowBalanceThreshold(null);
        }

        Integer updateAccountStatus = dm.parameterizedExecuteNonQuery("data.accountSettings.updatePersonLowBalanceThreshold",
                new Object[]{
                        getPersonID(),
                        getLowBalanceThreshold()
                }
        );

        if (updateAccountStatus != 1) {
            Logger.logMessage("Could not update the Low Balance Threshold for PersonID: "+ personID +" in AccountSettingCollection.updatePersonLowBalanceThreshold", Logger.LEVEL.ERROR);
        }
    }

    public void updateEmployeeLowBalanceThreshold(){
        if(getEmployeeLowBalanceThreshold().equals("")) {
            setEmployeeLowBalanceThreshold(null);
        }

        Integer updateAccountStatus = dm.parameterizedExecuteNonQuery("data.accountSettings.updateEmployeeLowBalanceThreshold",
                new Object[]{
                        getLoggedInEmployeeID(),
                        getEmployeeLowBalanceThreshold()
                }
        );

        if (updateAccountStatus != 1) {
            Logger.logMessage("Could not update the Low Balance Threshold for Employee Id: "+ getLoggedInEmployeeID() +" in AccountSettingCollection.EmployeeLowBalanceThreshold", Logger.LEVEL.ERROR);
        }
    }

    public void updateEmployeeOrgLevel(){

        Integer updateAccountStatus = dm.parameterizedExecuteNonQuery("data.accountSettings.updateEmployeeOrgLevel",
                new Object[]{
                        getLoggedInEmployeeID(),
                        getOrgLevelPrimaryId()
                }
        );

        if (updateAccountStatus != 1) {
            Logger.logMessage("Could not update the Low Balance Threshold for Employee Id: "+ getLoggedInEmployeeID() +" in AccountSettingCollection.EmployeeLowBalanceThreshold", Logger.LEVEL.ERROR);
        }
    }

    // BEGIN FUNCTIONS FOR UPDATING QC_EmployeeChanges

    //record changes to account group for an account in the QC_EmployeeChanges table
    public boolean updateAccountGroupEmployeeChanges() {
        try {
            return dm.serializeUpdate("data.myqc.recordAccountGroupChangeAsMyQCUser", new Object[] {getLoggedInEmployeeID(), getAccountGroupID()}) != -1;
        } catch (Exception ex) {
            Logger.logMessage("ERROR: Error recording account change in QC_EmployeeChanges in AccountSettingsCollection.updateAccountGroupEmployeeChanges for account group ID: "+getAccountGroupID(), Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
            return false;
        }
    }

    //record changes to account type for an account in the QC_EmployeeChanges table
    public boolean updateAccountTypeEmployeeChanges() {
        try {
            return dm.serializeUpdate("data.myqc.recordAccountTypeChangeAsMyQCUser", new Object[] {getLoggedInEmployeeID(), getAccountGroupID()}) != -1;
        } catch (Exception ex) {
            Logger.logMessage("ERROR: Error recording account type change in QC_EmployeeChanges in AccountSettingsCollection.updateAccountTypeEmployeeChanges for account ID: "+getLoggedInEmployeeID(), Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
            return false;
        }
    }

    //record changes to spending profile for an account in the QC_EmployeeChanges table
    public boolean updateSpendingProfileEmployeeChanges() {
        try {
            return dm.serializeUpdate("data.myqc.recordSpendingProfileChangeAsMyQCUser", new Object[] {getLoggedInEmployeeID(), getSpendingProfileID()}) != -1;
        } catch (Exception ex) {
            Logger.logMessage("ERROR: Error recording spending profile change in QC_EmployeeChanges in AccountSettingsCollection.updateAccountGroupEmployeeChanges for spending profile ID: "+getSpendingProfileID(), Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
            return false;
        }
    }

    //record changes to account status for an account in the QC_EmployeeChanges table
    public boolean updateAccountStatusEmployeeChanges(String status) {
        try {
            String oldStatus = "Active";
            if(status.equals("Active")) {
                oldStatus = "Waiting for Signup";
            }
            return dm.serializeUpdate("data.myqc.recordAccountStatusChangeAsMyQCUser", new Object[] {getLoggedInEmployeeID(), oldStatus, status}) != -1;
        } catch (Exception ex) {
            Logger.logMessage("ERROR: Error recording account change in QC_EmployeeChanges in AccountSettingsCollection.updateAccountStatusEmployeeChanges for account ID: "+getLoggedInEmployeeID(), Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
            return false;
        }
    }

    //record changes to account group for an account in the QC_EmployeeChanges table
    public boolean updateAccountEmailEmployeeChanges() {
        try {
            return dm.serializeUpdate("data.myqc.recordAccountEmailChangeAsMyQCUser", new Object[] {getLoggedInEmployeeID(), getEmail()}) != -1;
        } catch (Exception ex) {
            Logger.logMessage("ERROR: Error recording account change in QC_EmployeeChanges in AccountSettingsCollection.updateAccountGroupEmployeeChanges for account group ID: "+getAccountGroupID(), Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
            return false;
        }
    }

    //record changes to low balance threshold for an account in the QC_EmployeeChanges
    public boolean updateEmployeeAccountLowBalanceThresholdChanges() {
        try {
            return dm.serializeUpdate("data.myqc.recordLowBalanceThresholdChangeAsMyQCUser", new Object[] {getLoggedInEmployeeID(), getEmployeeLowBalanceThreshold()}) != -1;
        } catch (Exception ex) {
            Logger.logMessage("ERROR: Error recording account change in QC_EmployeeChanges in AccountSettingsCollection.updateAccountGroupEmployeeChanges for account group ID: "+getAccountGroupID(), Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
            return false;
        }
    }

    //record changes to Org Level for an account in the QC_EmployeeChanges
    public boolean updateEmployeeOrgLevelChanges() {
        try {
            return dm.serializeUpdate("data.myqc.recordOrgLevelChangeAsMyQCUser", new Object[] {getLoggedInEmployeeID(), getOrgLevelPrimaryId()}) != -1;
        } catch (Exception ex) {
            Logger.logMessage("ERROR: Error recording account change in QC_EmployeeChanges in AccountSettingsCollection.updateAccountGroupEmployeeChanges for account group ID: "+getAccountGroupID(), Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
            return false;
        }
    }

    //getter
    public List<SpendingProfileModel> getSpendingProfileCollection() {
        return this.spendingProfileCollection;
    }

    //getter
    public List<AccountGroupModel> getAccountGroupCollection() {
        return this.accountGroupCollection;
    }

    //getter
    public List<AccountTypeModel> getAccountTypeCollection() {
        return this.accountTypeCollection;
    }

    //getter
    private Integer getLoggedInEmployeeID() {
        return this.loggedInEmployeeID;
    }

    //setter
    private void setLoggedInEmployeeID(Integer loggedInEmployeeID) {
        this.loggedInEmployeeID = loggedInEmployeeID;
    }

    public Integer getSpendingProfileID() {
        return spendingProfileID;
    }

    public void setSpendingProfileID(Integer spendingProfileID) {
        this.spendingProfileID = spendingProfileID;
    }

    public Integer getAccountGroupID() {
        return accountGroupID;
    }

    public void setAccountGroupID(Integer accountGroupID) {
        this.accountGroupID = accountGroupID;
    }

    public Boolean getSpendingProfileChanged() {
        return spendingProfileChanged;
    }

    public void setSpendingProfileChanged(Boolean spendingProfileChanged) {
        this.spendingProfileChanged = spendingProfileChanged;
    }

    public Boolean getAccountGroupChanged() {
        return accountGroupChanged;
    }

    public void setAccountGroupChanged(Boolean accountGroupChanged) {
        this.accountGroupChanged = accountGroupChanged;
    }

    public Integer getAccountTypeChange() {
        return accountTypeChange;
    }

    public void setAccountTypeChange(Integer accountTypeChange) {
        this.accountTypeChange = accountTypeChange;
    }

    public Boolean getStatusChanged() {
        return statusChanged;
    }

    public void setStatusChanged(Boolean statusChanged) {
        this.statusChanged = statusChanged;
    }

    public Boolean getNeedsTOS() {
        return needsTOS;
    }

    public void setNeedsTOS(Boolean needsTOS) {
        this.needsTOS = needsTOS;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLowBalanceThreshold() {
        return lowBalanceThreshold;
    }

    public void setLowBalanceThreshold(String lowBalanceThreshold) {
        this.lowBalanceThreshold = lowBalanceThreshold;
    }

    public Integer getPersonID() {
        return personID;
    }

    public void setPersonID(Integer personID) {
        this.personID = personID;
    }

    public Integer getOriginalSpendingProfile() {
        return originalSpendingProfile;
    }

    public void setOriginalSpendingProfile(Integer originalSpendingProfile) {
        this.originalSpendingProfile = originalSpendingProfile;
    }

    public BigDecimal getMealPlanPrice() {
        return mealPlanPrice;
    }

    public void setMealPlanPrice(BigDecimal mealPlanPrice) {
        this.mealPlanPrice = mealPlanPrice;
    }

    public String getErrorDetails() {
        return errorDetails;
    }

    public void setErrorDetails(String errorDetails) {
        this.errorDetails = errorDetails;
    }

    public String getFundingFee() {
        return fundingFee;
    }

    public void setFundingFee(String fundingFee) {
        this.fundingFee = fundingFee;
    }

    public String getEmployeeLowBalanceThreshold() {
        return employeeLowBalanceThreshold;
    }

    public void setEmployeeLowBalanceThreshold(String lowBalanceThreshold) {
        this.employeeLowBalanceThreshold = lowBalanceThreshold;
    }

    public Integer getOrgLevelPrimaryId() {
        return orgLevelPrimaryId;
    }

    public void setOrgLevelPrimaryId(Integer orgLevelPrimaryId) {
        this.orgLevelPrimaryId = orgLevelPrimaryId;
    }

}
