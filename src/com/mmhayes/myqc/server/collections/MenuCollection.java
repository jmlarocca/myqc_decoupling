package com.mmhayes.myqc.server.collections;

//mmhayes dependencies
import com.mmhayes.common.api.*;
import com.mmhayes.common.dataaccess.*;
import com.mmhayes.common.utils.*;
import com.mmhayes.common.api.errorhandling.exceptions.*;


//myqc dependencies
import com.mmhayes.myqc.server.models.*;

//other dependencies
import javax.servlet.http.HttpServletRequest;
import java.util.*;

/* Menu Collection
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2019-07-25 12:26:55 -0400 (Thu, 25 Jul 2019) $: Date of last commit
 $Rev: 40257 $: Revision of last commit

 Notes: Collection of Menu Models
*/
public class MenuCollection {
    List<MenuModel> collection = new ArrayList<MenuModel>();
    private static DataManager dm = new DataManager();
    private commonMMHFunctions commFunc = new commonMMHFunctions();
    private Integer authenticatedAccountID = null;
    private Integer storeID = null;

    //constructor - populates the collection
    public MenuCollection(HttpServletRequest request) throws Exception {

        //determine the authenticated account ID from the request
        setAuthenticatedAccountID(CommonAuthResource.checkRequestHeaderForMMHayesAuth(request));

        //determine the store ID from the request
        setStoreID(CommonAuthResource.checkRequestHeaderForMMHayesAuth(request));

        //populate this collection with models
        populateCollection();

    }

    //populates this collection with models
    public MenuCollection populateCollection() throws Exception {

        //throw InvalidAuthException if no authenticatedAccountID is found
        if (getAuthenticatedAccountID() == null) {
            throw new InvalidAuthException();
        }

        //get all models in an array list
        ArrayList<HashMap> menuList = dm.parameterizedExecuteQuery("data.ordering.getAllMenuHeaders",
                new Object[]{},
                true
        );

        //iterate through list, create models and add them to the collection
        if (menuList != null && menuList.size() > 0) {
            for (HashMap menuHM : menuList) {
                if (menuHM.get("ID") != null && !menuHM.get("ID").toString().isEmpty()) {
                    //create a new model and add to the collection
                    getCollection().add(new MenuModel(menuHM, getAuthenticatedAccountID(), getStoreID()));
                }
            }
        }

        return this;
    }

    //getter
    public List<MenuModel> getCollection() {
        return this.collection;
    }

    //getter
    private Integer getAuthenticatedAccountID() {
        return this.authenticatedAccountID;
    }

    //setter
    private void setAuthenticatedAccountID(Integer authenticatedAccountID) {
        this.authenticatedAccountID = authenticatedAccountID;
    }

    public Integer getStoreID() {
        return storeID;
    }

    public void setStoreID(Integer storeID) {
        this.storeID = storeID;
    }

}
