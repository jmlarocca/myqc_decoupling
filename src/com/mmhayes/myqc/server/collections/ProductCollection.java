package com.mmhayes.myqc.server.collections;

//mmhayes dependencies
import com.mmhayes.common.api.*;
import com.mmhayes.common.dataaccess.*;
import com.mmhayes.common.utils.*;
import com.mmhayes.common.api.errorhandling.exceptions.*;

//myqc dependencies
import com.mmhayes.myqc.server.models.*;

//other dependencies
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.*;

/* Product Collection
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2019-06-14 09:14:02 -0400 (Fri, 14 Jun 2019) $: Date of last commit
 $Rev: 39355 $: Revision of last commit

 Notes: Collection of Product Models
*/
public class ProductCollection {
    List<ProductModel> collection = new ArrayList<ProductModel>();
    private static DataManager dm = new DataManager();
    private commonMMHFunctions commFunc = new commonMMHFunctions();
    private Integer authenticatedAccountID = null;
    String message = "";

    //constructor - populates the collection ( do we need this constructor? maybe? -jrmitaly 6/7/2016 )
    public ProductCollection(HttpServletRequest request) {

    }

    /* constructor - populates the collection
        @param products: Array list of Product Models used to populate the collection
        @param request: Standard HTTP request object - used for authentication
    */
    public ProductCollection(List<ProductModel> products, HttpServletRequest request) throws Exception {

        //determine the authenticated account ID from the request
        setAuthenticatedAccountID(CommonAuthResource.checkRequestHeaderForMMHayesAuth(request));

        //populate this collection with models
        populateCollection(products, request);

    }

    public ProductCollection populateCollection(List<ProductModel> products, HttpServletRequest request) throws Exception {

        //throw InvalidAuthException if no authenticatedAccountID is found
        if (getAuthenticatedAccountID() == null) {
            throw new InvalidAuthException();
        }

        //reset collection then rebuild it - one product at a time
        for (ProductModel productModel : products) {

            if (productModel.getID() == null || productModel.getID().toString().isEmpty()) {
                Logger.logMessage("Could not determine Product Model in ProductCollection.populateCollection(products)", Logger.LEVEL.ERROR);
                setMessage("Could not determine product identifier for the order.");
                resetCollection();
                break;
            }

            //retrieve the storeID (MyQCTerminalID) out of the request
            Integer storeID = CommonAPI.checkRequestHeaderForStoreID(request);

            //update product model here from database - one product at a time
            ArrayList<HashMap> singleProductList = dm.parameterizedExecuteQuery("data.ordering.getProduct",
                new Object[]{
                        productModel.getID(),
                        storeID
                },
                true
            );

            //throw exception if no valid response
            if ( singleProductList == null || singleProductList.size() != 1 ) {
                Logger.logMessage("Could not get single Product Model in ProductCollection.populateCollection(products)", Logger.LEVEL.ERROR);
                setMessage("Could not retrieve product details for the order.");
                resetCollection();
                break;
            }

            //get the hashmap of details
            HashMap singleProductHM = singleProductList.get(0);

            //throw exception if no valid response
            if (singleProductHM.get("ID") == null || singleProductHM.get("ID").toString().isEmpty()) {
                Logger.logMessage("Could not determine single Product Model in ProductCollection.populateCollection(products)", Logger.LEVEL.ERROR);
                setMessage("Could not determine product details for the order.");
                resetCollection();
                break;
            }

            //carry quantity information through to the re-populated collection
            if ( productModel.getQuantity() != null && productModel.getQuantity().compareTo(BigDecimal.ZERO) > 0 ) {
                singleProductHM.put("QUANTITY", productModel.getQuantity());
            }

            //create a new model and add to the collection
            ProductModel thisProductModel = new ProductModel(singleProductHM);
            this.addToCollection(thisProductModel);

            //carry modifiers information through to the re-populated collection
            if ( productModel.getModifiers() != null && productModel.getModifiers().size() > 0 ) {
                thisProductModel.setModifiers( productModel.getModifiers() );

                //updates the price of the product by iterating through all the modifiers and adjusting the price accordingly
                thisProductModel.updatePricesBasedOnModifiers((ArrayList<ModifierModel>) productModel.getModifiers(), request);
            }

            //update name from the original product model because it could be a buttonText name from KP
            thisProductModel.setName(productModel.getName());
        }

        return this;
    }

    //getter
    public List<ProductModel> getCollection() {
        return this.collection;
    }

    public void resetCollection() {
        this.collection = new ArrayList<ProductModel>();
    }

    //setter
    public void setCollection(List<ProductModel> collection) {
        this.collection = collection;
    }

    //adder
    public void addToCollection(ProductModel product) {
        this.getCollection().add(product);
    }

    //getter
    private Integer getAuthenticatedAccountID() {
        return this.authenticatedAccountID;
    }

    //setter
    private void setAuthenticatedAccountID(Integer authenticatedAccountID) {
        this.authenticatedAccountID = authenticatedAccountID;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}