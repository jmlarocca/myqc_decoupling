package com.mmhayes.myqc.server.collections;

//mmhayes dependencies

import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.CommonAuthResource;
import com.mmhayes.common.api.errorhandling.exceptions.InvalidAuthException;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.commonMMHFunctions;
import com.mmhayes.myqc.server.models.MenuModel;
import com.mmhayes.myqc.server.models.ModifierModel;
import com.mmhayes.myqc.server.models.PrepOptionModel;
import com.mmhayes.myqc.server.models.ProductFavoriteModel;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

//myqc dependencies
//other dependencies

/* Menu Collection
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2020-10-01 14:52:17 -0400 (Thu, 01 Oct 2020) $: Date of last commit
 $Rev: 51597 $: Revision of last commit

 Notes: Collection of Menu Models
*/
public class ProductFavoriteCollection {
    List<ProductFavoriteModel> collection = new ArrayList<ProductFavoriteModel>();
    private static DataManager dm = new DataManager();
    private commonMMHFunctions commFunc = new commonMMHFunctions();
    private Integer authenticatedAccountID = null;
    private Integer storeID = null;

    //constructor - populates the collection
    public ProductFavoriteCollection(Integer productID, HttpServletRequest request) throws Exception {

        //determine the authenticated account ID from the request
        setAuthenticatedAccountID(CommonAuthResource.checkRequestHeaderForMMHayesAuth(request));

        //determine the store ID from the request
        setStoreID(CommonAPI.checkRequestHeaderForStoreID(request));

        //populate this collection with models
        populateCollection(productID);

    }

    //populates this collection with models
    public ProductFavoriteCollection populateCollection(Integer productID) throws Exception {

        //throw InvalidAuthException if no authenticatedAccountID is found
        if (getAuthenticatedAccountID() == null) {
            throw new InvalidAuthException();
        }

        //get all models in an array list
        ArrayList<HashMap> favoriteList = dm.parameterizedExecuteQuery("data.product.getProductFavorites",
                new Object[]{
                        productID,
                        getAuthenticatedAccountID(),
                        getStoreID()
                },
                true
        );

        //populate this collection from an array list of hashmaps
        if (favoriteList != null && favoriteList.size() > 0) {
            Integer currentFavoriteID = null;
            ProductFavoriteModel currentFavoriteModel = null;

            for(HashMap favoriteHM : favoriteList) {
                Integer favoriteID = CommonAPI.convertModelDetailToInteger(favoriteHM.get("ORDERINGFAVORITEID"));

                Integer modID = CommonAPI.convertModelDetailToInteger(favoriteHM.get("MODID"));
                Integer modPrepOptionSetID = CommonAPI.convertModelDetailToInteger(favoriteHM.get("MODPREPSETID"));
                Integer modPrepOptionID = CommonAPI.convertModelDetailToInteger(favoriteHM.get("MODPREPID"));

                // If new Product Favorite Model
                if(currentFavoriteID == null || !currentFavoriteID.equals(favoriteID)) {
                    if(currentFavoriteModel != null) {
                        this.getCollection().add(currentFavoriteModel);
                    }

                    // Create new Product Favorite Model
                    ProductFavoriteModel favoriteModel = new ProductFavoriteModel(favoriteHM);

                    if(modID != null) {
                        ModifierModel modifierModel = new ModifierModel();
                        modifierModel.setID(modID);
                        modifierModel.setPrepOptionSetID(modPrepOptionSetID);

                        if(modPrepOptionID != null) {
                            PrepOptionModel prepOptionModel = new PrepOptionModel();
                            prepOptionModel.setID(modPrepOptionID);
                            prepOptionModel.setPrepOptionSetID(modPrepOptionSetID);
                            modifierModel.setPrepOption(prepOptionModel);
                        }

                        // Add to Favorite Modifiers Model
                        favoriteModel.getModifiers().add(modifierModel);
                    }

                    // Update current Favorite Model in progress
                    currentFavoriteID = favoriteID;
                    currentFavoriteModel = favoriteModel;

                    // If this modifier is part of the current Favorite Model
                } else {

                    ModifierModel modifierModel = new ModifierModel();
                    modifierModel.setID(modID);
                    modifierModel.setPrepOptionSetID(modPrepOptionSetID);

                    if(modPrepOptionID != null) {
                        PrepOptionModel prepOptionModel = new PrepOptionModel();
                        prepOptionModel.setID(modPrepOptionID);
                        modifierModel.setPrepOption(prepOptionModel);
                    }

                    // Add to Favorite Modifiers Model
                    currentFavoriteModel.getModifiers().add(modifierModel);
                }
            }

            // Add Favorite Model to product
            this.getCollection().add(currentFavoriteModel);
        }
        return this;
    }

    //getter
    public List<ProductFavoriteModel> getCollection() {
        return this.collection;
    }

    //getter
    private Integer getAuthenticatedAccountID() {
        return this.authenticatedAccountID;
    }

    //setter
    private void setAuthenticatedAccountID(Integer authenticatedAccountID) {
        this.authenticatedAccountID = authenticatedAccountID;
    }

    public Integer getStoreID() {
        return storeID;
    }

    public void setStoreID(Integer storeID) {
        this.storeID = storeID;
    }

}
