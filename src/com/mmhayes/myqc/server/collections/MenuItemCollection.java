package com.mmhayes.myqc.server.collections;

//mmhayes dependencies
import com.mmhayes.common.api.*;
import com.mmhayes.common.dataaccess.*;
import com.mmhayes.common.rotations.RotationHandler;
import com.mmhayes.common.utils.*;
import com.mmhayes.common.api.errorhandling.exceptions.*;

//myqc dependencies
import com.mmhayes.myqc.server.api.MyQCCache;
import com.mmhayes.myqc.server.models.*;

//other dependencies
import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.*;

/* Menu Item Collection
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2020-05-20 11:08:56 -0400 (Wed, 20 May 2020) $: Date of last commit
 $Rev: 49132 $: Revision of last commit

 Notes: Collection of Menu Item Models
*/
public class MenuItemCollection {
    List<MenuItemModel> collection = new ArrayList<MenuItemModel>();
    private static DataManager dm = new DataManager();
    private commonMMHFunctions commFunc = new commonMMHFunctions();
    private Integer authenticatedAccountID = null;
    private Integer storeID = null;

    //constructor - populates the collection
    public MenuItemCollection(Integer menuID, boolean isUpsellMenu, Integer employeeID, Integer storeID) throws Exception {

        //set authenticated account ID
        setAuthenticatedAccountID(employeeID);

        //set store ID
        setStoreID(storeID);

        //populate this collection with models
        populateCollection(menuID, isUpsellMenu);

    }

    //populates this collection with models
    public MenuItemCollection populateCollection(Integer menuID, boolean isUpsellMenu) throws Exception {

        //throw InvalidAuthException if no authenticatedAccountID is found
        if (getAuthenticatedAccountID() == null) {
            throw new InvalidAuthException();
        }

        String popularProducts = "";

        if(!isUpsellMenu) {
            //use the store model to get the Store Keypad Model from the cache to get the number of products that can be popular
            StoreModel storeModel = StoreModel.getStoreModel(getStoreID(), getAuthenticatedAccountID());
            StoreKeypadModel storeKeypadModel = StoreKeypadModel.getStoreKeypadModel(storeModel);
            popularProducts = storeKeypadModel.getPopularProducts();

            //check if this keypad is a controller keypad, returns rotating keypad detail if true
            menuID = RotationHandler.checkForRotatingKeypad(getStoreID(), menuID, storeModel.getCurrentStoreDTM(), true);
        } else {

            if(MyQCCache.checkCacheForStore(getStoreID()) ) {
                StoreModel storeModel = MyQCCache.getStoreFromCache( getStoreID(), true );
                StoreKeypadModel storeKeypadModel = StoreKeypadModel.getStoreKeypadModel(storeModel);
                popularProducts = storeKeypadModel.getPopularProducts();
            }

            menuID = RotationHandler.checkForRotatingKeypad(getStoreID(), menuID, LocalDateTime.now(), true);
        }

        //get all models in an array list
        ArrayList<HashMap> menuItemList = dm.parameterizedExecuteQuery("data.ordering.getMenuItems",
                new Object[]{
                    menuID,
                    getStoreID(),
                    getAuthenticatedAccountID(),
                    popularProducts
                },
                true
        );

        //populate this collection from an array list of hashmaps
        if (menuItemList != null && menuItemList.size() > 0) {
            for (HashMap menuItemHM : menuItemList) {
                if (menuItemHM.get("ID") != null && !menuItemHM.get("ID").toString().isEmpty()) {

                    MenuItemModel menuItemModel = new MenuItemModel(menuItemHM);

                    //create a new model and add to the collection
                    getCollection().add(menuItemModel);
                }
            }
        }

        return this;
    }

    //getter
    public List<MenuItemModel> getCollection() {
        return this.collection;
    }

    //getter
    private Integer getAuthenticatedAccountID() {
        return this.authenticatedAccountID;
    }

    //setter
    private void setAuthenticatedAccountID(Integer authenticatedAccountID) {
        this.authenticatedAccountID = authenticatedAccountID;
    }

    public Integer getStoreID() {
        return storeID;
    }

    public void setStoreID(Integer storeID) {
        this.storeID = storeID;
    }
}
