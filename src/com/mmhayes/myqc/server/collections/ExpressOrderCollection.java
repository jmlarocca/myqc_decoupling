package com.mmhayes.myqc.server.collections;

//mmhayes dependencies

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.CommonAuthResource;
import com.mmhayes.common.api.errorhandling.exceptions.InvalidAuthException;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.myqc.server.api.MyQCCache;
import com.mmhayes.myqc.server.models.*;
import org.apache.xpath.operations.Bool;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;

//myqc dependencies
//other dependencies

/* Express Order Collection
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2019-02-04 10:19:11 -0500 (Mon, 04 Feb 2019) $: Date of last commit
 $Rev: 36507 $: Revision of last commit

 Notes: Collection of Express Order Models
*/
public class ExpressOrderCollection {
    List<ExpressOrderModel> collection = new ArrayList<ExpressOrderModel>();
    Integer loggedInEmployeeID = null;
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String storeIDs = "";
    HashMap<Integer, ArrayList<String>> transactionProductsByStore = new HashMap<Integer, ArrayList<String>>();
    ArrayList<String> prepOptionIDList = new ArrayList<>();
    ArrayList<String> prepOptionSetIDList = new ArrayList<>();
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String prepOptionIDs = "";
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String prepOptionSetIDs = "";
    boolean needsUpdate = false;
    LocalDateTime lastCacheUpdate = null;
    HashMap <String, String> prepOptionToPrepOptionSetMappings = new HashMap<>();

    private static DataManager dm = new DataManager();

    //constructor - populates the collection
    public ExpressOrderCollection() {

    }

    //constructor - populates the collection
    public ExpressOrderCollection(String storeIDs, Integer employeeID) throws Exception {

        //set the employeeID
        setLoggedInEmployeeID(employeeID);

        //set list of store IDs
        setStoreIDs(storeIDs);

        populateCollection();
    }

    public static ExpressOrderCollection getExpressReorders(String storeIDs, HttpServletRequest request) throws Exception {

        Integer employeeID = CommonAuthResource.checkRequestHeaderForMMHayesAuth(request);

        //check if all the StoreKeypads and the ExpressReorders are up to date, if not pull new express reorders and update store keypad models
        if (MyQCCache.checkIfExpressReordersAreUpToDate(employeeID, storeIDs)) {
            return MyQCCache.getExpressOrderFromCache(employeeID);
        }

        return new ExpressOrderCollection(storeIDs, employeeID);
    }

    //populates the express order collection with express order models
    public ExpressOrderCollection populateCollection() throws Exception {
        //throw InvalidAuthException if no employeeID or terminalID is found
        if (getLoggedInEmployeeID() == null || getStoreIDs() == null) {
            throw new InvalidAuthException();
        }

        this.getTransactions();

        this.setLastCacheUpdate(LocalDateTime.now());

        MyQCCache.addExpressReordersToCache(this);
        return this;
    }

    //gets a list of the 5 most recent transactions and checks that each transaction has valid products
    public void getTransactions() throws Exception {
        Integer loopCount = 0;
        ArrayList<String> excludeTransactionIDList = new ArrayList<>();

        ArrayList<HashMap> transactionList;

        do {
            //get a list of the 20 most recent transactions
            Object[] params = new Object[]{
                    getLoggedInEmployeeID().toString(),
                    getStoreIDs(),
                    String.join(",", excludeTransactionIDList)
            };

            transactionList = dm.parameterizedExecuteQuery("data.expressorder.getRecentTransactions", params, true);

            //if no transactions found, break out
            if ( transactionList == null || transactionList.size() == 0 ) {
                break;
            }

            for ( HashMap transaction : transactionList ) {
                if ( transaction.get("ID") == null || transaction.get("ID").equals("") || transaction.get("STOREID") == null || transaction.get("STOREID").equals("")) {
                    continue;
                }

                Integer transactionID = CommonAPI.convertModelDetailToInteger(transaction.get("ID"));
                Integer storeID = CommonAPI.convertModelDetailToInteger(transaction.get("STOREID"));

                //don't look for this transaction again if we need to check for more transactions, valid or not doesn't matter
                excludeTransactionIDList.add(transactionID.toString());

                //get transaction information - products and modifiers
                ArrayList<HashMap> transInfo = dm.parameterizedExecuteQuery("data.expressorder.getTransactionDetails",
                        new Object[]{
                                transactionID
                        },
                        true
                );

                //no products found, skip transaction
                if ( transInfo == null || transInfo.size() == 0 ) {
                    continue;
                }

                //check if this transaction is identical to one already in this collection
                if ( this.getCollection().size() > 0 && isOrderAlreadyExpressOrder(transInfo) ) {
                    continue;
                }

                //check if the products and modifiers in the transaction are all valid and available in the transaction's store
                ArrayList<HashMap> transactionResult = validateTransactionProducts(transactionID, storeID);

                //if not the same number of products and modifiers, at least one product or modifier is not currently available
                if ( transactionResult.size() != transInfo.size() ) {
                    continue;
                }

                Integer expressOrderIDInProgress = 0;
                for (HashMap orderHM : transactionResult) {
                    if ( orderHM.get("PATRANSID") == null || orderHM.get("PATRANSID").toString().equals("") ) {
                        continue;
                    }

                    addTransactionProductsByStoreID(storeID, orderHM.get("ID").toString());

                    addPrepOptionsToList(orderHM);

                    //if this PATransLineItem is mapped to a patransactionID that is already an existing ExpressOrderModel, then update the existing model
                    if(Integer.parseInt(orderHM.get("PATRANSID").toString()) == expressOrderIDInProgress) {
                        ExpressOrderModel expressOrderModel = getModelByPATransactionID(CommonAPI.convertModelDetailToInteger(orderHM.get("PATRANSID")));
                        if(expressOrderModel != null) {
                            expressOrderModel.setExpressOrderItems(orderHM);
                        }
                    } else { //otherwise create new ExpressOrderModel
                        expressOrderIDInProgress = CommonAPI.convertModelDetailToInteger(orderHM.get("PATRANSID"));
                        getCollection().add(new ExpressOrderModel(orderHM, getLoggedInEmployeeID()));
                    }
                }

                if ( this.getCollection().size() == 5 ) {
                    break;
                }
            }

            if ( ++loopCount > 5 ) {
                Logger.logMessage("ERROR: Count not determine express reorder transactions in ExpressOrderCollection.getTransaction - tried 5 times.. giving up...", Logger.LEVEL.ERROR);
                return;
            }

            //keep going while the user still has more transactions to consider and not all 5 express orders have been found
        } while ( transactionList.size() == 20 && this.getCollection().size() < 5 );
    }

    public boolean isOrderAlreadyExpressOrder(ArrayList<HashMap> transInfo) {
        Integer transTerminalID = CommonAPI.convertModelDetailToInteger(transInfo.get(0).get("TERMINALID"));
        HashMap<Integer, ExpressOrderItemModel> transOrderItems = new HashMap<>();

        //build expressOrderItemModels for each transLine in transInfo
        for ( HashMap transInfoHM : transInfo ) {
            ExpressOrderItemModel expressOrderItemModel = new ExpressOrderItemModel();

            boolean isModifier = CommonAPI.convertModelDetailToString(transInfoHM.get("ISMODIFIER")).equals("1");
            Integer transLine = CommonAPI.convertModelDetailToInteger(transInfoHM.get("PATRANSLINEITEMID"));
            Integer productID = CommonAPI.convertModelDetailToInteger( transInfoHM.get("PAITEMID") );
            BigDecimal quantity = CommonAPI.convertModelDetailToBigDecimal( transInfoHM.get("QUANTITY") );
            Integer prepOptionID = CommonAPI.convertModelDetailToInteger( transInfoHM.get("PAPREPOPTIONID") );
            Integer paComboID = CommonAPI.convertModelDetailToInteger( transInfoHM.get("PACOMBOID") );

            if ( isModifier ) {
                ModifierModel modifierModel = new ModifierModel();
                modifierModel.setID( productID );

                if(prepOptionID != null) {
                    PrepOptionModel prepOptionModel = new PrepOptionModel();
                    prepOptionModel.setID(prepOptionID);
                    modifierModel.setPrepOption(prepOptionModel);
                }

                transOrderItems.get( transLine ).getModifiers().add(modifierModel);
                continue;
            }

            expressOrderItemModel.setID( productID );
            expressOrderItemModel.setProductID( productID );
            expressOrderItemModel.setLineID( transLine );
            expressOrderItemModel.setQuantity( quantity );

            if(prepOptionID != null) {
                PrepOptionModel prepOptionModel = new PrepOptionModel();
                prepOptionModel.setID(prepOptionID);
                expressOrderItemModel.setPrepOption(prepOptionModel);
            }

            if(paComboID != null) {
                expressOrderItemModel.setPAComboID(paComboID);
            }

            transOrderItems.put( transLine, expressOrderItemModel );
        }

        //set expressOrderItemModels on new ExpressOrderModel
        ExpressOrderModel transExpressOrderModel = new ExpressOrderModel();
        for ( HashMap.Entry<Integer, ExpressOrderItemModel> entry : transOrderItems.entrySet() ) {
            ExpressOrderItemModel expressOrderItemModel = entry.getValue();

            transExpressOrderModel.getItemList().add( expressOrderItemModel );
        }

        try {

            //get comparison string for new express order
            String transInfoComparisonString = transExpressOrderModel.buildComparisonString();

            //check against all existing express orders
            for ( ExpressOrderModel expressOrderModel : this.getCollection() ) {
                //if the terminals are not the same, not a dupe
                if ( !expressOrderModel.getTerminalId().equals( transTerminalID ) ) {
                    continue;
                }

                //if not the same number of products, not a dupe
                if ( expressOrderModel.getItemList().size() != transOrderItems.size() ) {
                    continue;
                }

                String expressOrderComparisonString = expressOrderModel.buildComparisonString();

                //retrieve a composite string that encompasses productIDs, quantities and modifiers, if it's the same for each order, it's a duplicate
                if ( expressOrderComparisonString != null && transInfoComparisonString != null && expressOrderComparisonString.compareTo( transInfoComparisonString ) == 0 ) {
                    return true;
                }
            }

        } catch (Exception ex) {
            Logger.logMessage("ERROR: Count not determine if express reorder transaction is unique in ExpressOrderCollection.isOrderAlreadyExpressOrder", Logger.LEVEL.ERROR);
            return false;
        }

        return false;
    }

    //need to check that the products in the transaction are valid AND are currently available in the store
    public ArrayList<HashMap> validateTransactionProducts(Integer transactionID, Integer storeID) throws Exception {

        ArrayList<String> productsInStoreList = getProductsInStore(storeID);
        String productsInStore = String.join(",", productsInStoreList);

        //if there are no products in the store, then the store is probably not active
        if(productsInStore.equals("")) {
            return new ArrayList<>();
        }

        //validate the products in the transaction, returns empty list if invalid
        ArrayList<HashMap> transLines = dm.parameterizedExecuteQuery("data.expressorder.validateTransaction",
                new Object[]{
                        transactionID,
                        productsInStore
                },
                true
        );

        Integer currentComboTransLineItemID = null;
        Integer replaceComboTransLineItemID = 0;

        //replace the comboTransLineItemId with the temporary id
        for(HashMap transLine : transLines) {
            Integer comboTransLineItemID = CommonAPI.convertModelDetailToInteger(transLine.get("PACOMBOTRANSLINEITEMID"));
            Boolean comboIsActive = CommonAPI.convertModelDetailToBoolean(transLine.get("COMBOACTIVE"));

            // Don't display order with inactive combos.
            if(!comboIsActive){
                return new ArrayList<>();
            }

            if( comboTransLineItemID == null) {
                continue;
            }

            if(currentComboTransLineItemID == null || !currentComboTransLineItemID.equals(comboTransLineItemID)) {
                currentComboTransLineItemID = comboTransLineItemID;
                replaceComboTransLineItemID--;
            }

            transLine.put("PACOMBOTRANSLINEITEMID", replaceComboTransLineItemID);
        }

        return transLines;
    }

    //get the list of all the products in the store
    public ArrayList<String> getProductsInStore(Integer storeID) throws Exception {
        StoreModel storeModel = StoreModel.getStoreModel(storeID, getLoggedInEmployeeID());

        StoreKeypadModel storeKeypadModel = new StoreKeypadModel();

        //if store keypad model is not in cache or is not up-to-date
        if (!MyQCCache.checkIfStoreKeypadCacheNeedsUpdate(storeID)) {
            storeKeypadModel = MyQCCache.getStoreKeypadFromCache( storeID );
        } else {
            storeKeypadModel = new StoreKeypadModel( storeModel );
        }

        return storeKeypadModel.getProductsInStoreWithDiningOptionsList();
    }

    public ExpressOrderModel getModelByPATransactionID(Integer paTransactionID) {
        for(ExpressOrderModel expressOrderModel : getCollection()) {
            if(expressOrderModel.getID().equals(paTransactionID)) {
                return expressOrderModel;
            }
        }
        return null;
    }

    //if getting the Express Reorder Model from the cache... still need to check if the products are active, have inventory, etc...
    public boolean checkTransactionProducts() throws Exception {
        boolean validProducts = true;

        //get the products from the transactions based on the store
        for ( Map.Entry<Integer, ArrayList<String>> entry: getTransactionProductsByStore().entrySet() ) {
            Integer storeID = entry.getKey();
            ArrayList<String> transactionProductsInStoreList = entry.getValue();
            String transactionProductsInStore = String.join(",", transactionProductsInStoreList);

            //check if any of the products in any of the transactions are no longer active, have no inventory, etc
            ArrayList<HashMap> productList = dm.parameterizedExecuteQuery("data.expressorder.checkProductsInTransactionByStore",
                    new Object[]{
                            storeID,
                            transactionProductsInStore
                    },
                    true
            );

            //if there is an invalid product, need to update express reorder model
            if(productList != null && productList.size() > 0) {
                validProducts = false;
                break;
            }

            if(checkForModifierKeypadUpdates(storeID, transactionProductsInStore)) {
                validProducts = false;
                break;
            }
        }

        if(validProducts) {
            validProducts = checkTransactionPrepOptions();
        }

        return validProducts;
    }

    //check if all the prep option and prep option sets in the express reorder products are still active
    public boolean checkTransactionPrepOptions() {

        if(getPrepOptionIDList().size() > 0) {

            //check if the prep options are no longer active
            ArrayList<HashMap> prepOptionList = dm.parameterizedExecuteQuery("data.expressorder.checkActivePrepOptions",
                    new Object[]{
                            getPrepOptionIDs()
                    },
                    true
            );

            //if there is an invalid prep option, need to update express reorder model
            if(prepOptionList != null && prepOptionList.size() > 0) {
                return false;
            }
        }

        if(getPrepOptionSetIDList().size() > 0) {

            //check if the prep options are no longer active
            ArrayList<HashMap> prepOptionSetList = dm.parameterizedExecuteQuery("data.expressorder.checkActivePrepOptionSets",
                    new Object[]{
                            getPrepOptionSetIDs()
                    },
                    true
            );

            //if there is an invalid prep option set, need to update express reorder model
            if(prepOptionSetList != null && prepOptionSetList.size() > 0) {
                return false;
            }
        }

        // get a list of all the prep options mapped to
        if(!getPrepOptionToPrepOptionSetMappings().isEmpty()){
            for(String prepOptionSetID : this.getPrepOptionToPrepOptionSetMappings().keySet()){
                String prepOptionID = this.getPrepOptionToPrepOptionSetMappings().get(prepOptionSetID);

                ArrayList<HashMap> prepOptionMappedToSet = dm.parameterizedExecuteQuery("data.expressorder.checkPrepOptionInPrepOptionSet",
                        new Object[]{
                                prepOptionSetID,
                                prepOptionID
                        },
                        true
                );

                if(prepOptionMappedToSet.size() == 0){
                    return false;
                }
            }
        }

        return true;
    }

    public void addPrepOptionsToList(HashMap orderHM) {

        String prepOptionID = null;
        String prepOptionSetID = null;

        if( (orderHM.containsKey("PAPREPOPTIONID") && !orderHM.get("PAPREPOPTIONID").equals(""))) {
            prepOptionID = CommonAPI.convertModelDetailToString(orderHM.get("PAPREPOPTIONID"));
            addPrepOptionToList(prepOptionID);
        }

        if( (orderHM.containsKey("PAPREPOPTIONSETID") && !orderHM.get("PAPREPOPTIONSETID").equals(""))) {
            prepOptionSetID = CommonAPI.convertModelDetailToString(orderHM.get("PAPREPOPTIONSETID"));
            addPrepOptionSetToList(prepOptionSetID);
        }

        if(prepOptionID != null && prepOptionSetID != null){
            this.addPrepOptionToPrepOptionSetMapping(prepOptionSetID, prepOptionID);
        }

    }

    public boolean checkForModifierKeypadUpdates(Integer storeID, String transactionProductsInStore) throws Exception {
        boolean needsUpdate = false;

        //if now > REFRESH_MODEL_CACHE_SECONDS + lastUpdateCheck, check the lastUpdateDTM for the store
        LocalDateTime now = LocalDateTime.now();
        StoreKeypadModel storeKeypadModel = MyQCCache.getStoreKeypadFromCache(storeID);

        if(storeKeypadModel.getLastUpdateCheck() == null) {
            return false;
        }

        if ( now.isAfter( storeKeypadModel.getLastUpdateCheck().plusSeconds( 300 ) ) ) {

            ArrayList<HashMap> result = dm.parameterizedExecuteQuery("data.order.checkModifierKeypadsForUpdates",
                    new Object[]{
                            transactionProductsInStore,
                            storeKeypadModel.getLastUpdateDTM().toLocalDate().toString()+" "+storeKeypadModel.getLastUpdateDTM().plusSeconds(1).toLocalTime().toString()
                    },
                    true
            );

            //keypads needs to be updated if there are modifier keypad ids returned
            needsUpdate = ( result != null && result.size() > 0);
        }

        return needsUpdate;
    }

    public List<ExpressOrderModel> getCollection() {
        return collection;
    }

    public void setCollection(List<ExpressOrderModel> collection) {
        this.collection = collection;
    }

    //getter
    public Integer getLoggedInEmployeeID() {
        return this.loggedInEmployeeID;
    }

    //setter
    public void setLoggedInEmployeeID(Integer loggedInEmployeeID) {
        this.loggedInEmployeeID = loggedInEmployeeID;
    }

    public String getStoreIDs() {
        return storeIDs;
    }

    public void setStoreIDs(String storeIDs) {
        this.storeIDs = storeIDs;
    }

    public boolean getNeedsUpdate() {
        return needsUpdate;
    }

    public void setNeedsUpdate(boolean needsUpdate) {
        this.needsUpdate = needsUpdate;
    }

    public LocalDateTime getLastCacheUpdate() {
        return lastCacheUpdate;
    }

    public void setLastCacheUpdate(LocalDateTime lastCacheUpdate) {
        this.lastCacheUpdate = lastCacheUpdate;
    }

    public HashMap<Integer, ArrayList<String>> getTransactionProductsByStore() {
        return transactionProductsByStore;
    }

    public void setTransactionProductsByStore(HashMap<Integer, ArrayList<String>> transactionProductsByStore) {
        this.transactionProductsByStore = transactionProductsByStore;
    }

    public ArrayList<String> getTransactionProductsByStoreID(Integer storeID) {
        return getTransactionProductsByStore().get(storeID);
    }

    public void addTransactionProductsByStoreID(Integer storeID, String PAPluID) {
        ArrayList<String> transProductsByStore = getTransactionProductsByStoreID(storeID);
        if(transProductsByStore == null) {
            transProductsByStore = new ArrayList<String>();
        }
        transProductsByStore.add(PAPluID);
        getTransactionProductsByStore().put(storeID, transProductsByStore);
    }

    public ArrayList<String> getPrepOptionIDList() {
        return prepOptionIDList;
    }

    public void setPrepOptionIDList(ArrayList<String> prepOptionIDList) {
        this.prepOptionIDList = prepOptionIDList;
    }

    public ArrayList<String> getPrepOptionSetIDList() {
        return prepOptionSetIDList;
    }

    public void setPrepOptionSetIDList(ArrayList<String> prepOptionSetIDList) {
        this.prepOptionSetIDList = prepOptionSetIDList;
    }

    public String getPrepOptionIDs() {
        return prepOptionIDs;
    }

    public void setPrepOptionIDs() {
        this.prepOptionIDs = String.join(",", getPrepOptionIDList());
    }

    public String getPrepOptionSetIDs() {
        return prepOptionSetIDs;
    }

    public void setPrepOptionSetIDs() {
        this.prepOptionSetIDs = String.join(",", getPrepOptionSetIDList());
    }

    public void addPrepOptionToList(String prepOptionID) {
        ArrayList<String> prepOptions = this.getPrepOptionIDList();

        if(!prepOptions.contains(prepOptionID)) {
            prepOptions.add(prepOptionID);
        }

        setPrepOptionIDList(prepOptions);
        setPrepOptionIDs();
    }

    public void addPrepOptionSetToList(String prepOptionSetID) {
        ArrayList<String> prepOptionSets = this.getPrepOptionSetIDList();

        if(!prepOptionSets.contains(prepOptionSetID)) {
            prepOptionSets.add(prepOptionSetID);
        }

        setPrepOptionSetIDList(prepOptionSets);
        setPrepOptionSetIDs();
    }


    public HashMap<String, String> getPrepOptionToPrepOptionSetMappings() {
        return prepOptionToPrepOptionSetMappings;
    }

    public void setPrepOptionToPrepOptionSetMappings(HashMap<String, String> prepOptionToPrepOptionSetMappings) {
        this.prepOptionToPrepOptionSetMappings = prepOptionToPrepOptionSetMappings;
    }

    public void addPrepOptionToPrepOptionSetMapping(String prepOption, String prepOptionSet){
        this.prepOptionToPrepOptionSetMappings.put(prepOptionSet, prepOption);
    }

}
