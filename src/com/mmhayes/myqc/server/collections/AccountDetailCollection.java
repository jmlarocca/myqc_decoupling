package com.mmhayes.myqc.server.collections;

//mmhayes dependencies
import com.mmhayes.common.api.*;
import com.mmhayes.common.dataaccess.*;
import com.mmhayes.common.utils.*;
import com.mmhayes.common.api.errorhandling.exceptions.*;
import com.mmhayes.common.funding.FundingHelper;

//myqc dependencies
import com.mmhayes.myqc.server.models.*;

//other dependencies
import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.function.BooleanSupplier;

import static com.mmhayes.common.api.CommonAuthResource.getMMHayesAuthHeader;

/* Account Detail Collection
 Last Updated (automatically updated by SVN)
 $Author: mjbatko $: Author of last commit
 $Date: 2021-02-11 10:37:47 -0500 (Thu, 11 Feb 2021) $: Date of last commit
 $Rev: 54203 $: Revision of last commit

 Notes: Collection of Account Detail Models
*/
public class AccountDetailCollection {
    List<AccountDetailModel> collection = new ArrayList<AccountDetailModel>();
    private static DataManager dm = new DataManager();
    private Integer loggedInEmployeeID = null;

    //constructor - populates the collection
    public AccountDetailCollection(HttpServletRequest request) throws Exception {

        //determine the employeeID from the request
        setLoggedInEmployeeID(CommonAuthResource.checkRequestHeaderForMMHayesAuth(request));

        //populate this collection with models
        populateCollection(request);

    }

    //populates this collection with models
    public AccountDetailCollection populateCollection(HttpServletRequest request) throws Exception {

        //throw InvalidAuthException if no employeeID is found
        if (getLoggedInEmployeeID() == null) {
            throw new InvalidAuthException();
        }

        //NOTE: THIS SQL CALL IS NOT USING SQL PARAMETERIZATION - BUT IT'S OKAY IN THIS CASE -jrmitaly 6/17/2015
        //get a single page of models in an array list of linked hashmaps (to maintain the order of how columns are selected in the sql statement)
        ArrayList<HashMap> accountDetailList = dm.serializeSqlLinkedHashmap("data.myqc.getEmployeeAccountDetails",
            new Object[]{
                getLoggedInEmployeeID().toString()
            }
        );

        //if the query did not return any results - throw MissingDataException()
        if (accountDetailList == null || accountDetailList.size() == 0 || accountDetailList.get(0) == null) {
            throw new MissingDataException();
        }

        //if AuthHM's accountModel is null the logged in employee is not mapped to the person set on the DSKey
        String mmhayesAuthHeader = getMMHayesAuthHeader(request);

        Validation requestValidation = new Validation();
        requestValidation.setDSKey(mmhayesAuthHeader);

        HashMap AuthHM = requestValidation.checkDSKeyStatusWithDetails();

        //iterate through hashmap, create models and add them to the collection
        HashMap<String, String> accountDetailHM = accountDetailList.get(0);
        String accountBadge;
        if (accountDetailHM.get("ACCOUNTTYPEID") != null) {
            accountDetailHM.put("ACCOUNTTYPEID", String.valueOf(accountDetailHM.get("ACCOUNTTYPEID")));
        }
        if (accountDetailHM.get("ACCOUNTBADGE") != null) {
            accountBadge = String.valueOf(accountDetailHM.get("ACCOUNTBADGE"));
            accountDetailHM.put("ACCOUNTBADGE", accountBadge);
        } else {
            //if there's no badge number
            throw new MissingDataException();
        }

        if (accountDetailHM.get("BADGENUMPREFIX") != null) {
            String prefix = String.valueOf(accountDetailHM.get("BADGENUMPREFIX"));

            if ( !accountBadge.substring(0, prefix.length()).equals(prefix) ) {
                Logger.logMessage("The badge prefix (" + prefix + ") is not properly configured on the account badge (" + accountBadge + ") for EmployeeID #" + getLoggedInEmployeeID().toString(), Logger.LEVEL.ERROR);
            }

            accountDetailHM.put("BADGENUMPREFIX", prefix);
        }

        if (accountDetailHM.get("BADGENUMSUFFIX") != null) {
            String suffix = String.valueOf(accountDetailHM.get("BADGENUMSUFFIX"));

            if ( !accountBadge.substring(accountBadge.length() - suffix.length()).equals(suffix) ) {
                Logger.logMessage("The badge suffix (" + suffix + ") is not properly configured on the account badge (" + accountBadge + ") for EmployeeID #" + getLoggedInEmployeeID().toString(), Logger.LEVEL.ERROR);
            }

            accountDetailHM.put("BADGENUMSUFFIX", suffix);
        }

        if (accountDetailHM.get("QRCODELENGTH") != null) {
            String qrCodeLength = String.valueOf(accountDetailHM.get("QRCODELENGTH"));
            accountDetailHM.put("QRCODELENGTH", qrCodeLength);
        }

        if(AuthHM.get("accountModel") == null) { //this will force the front end to go to 'Select Student' page
            accountDetailHM.put("EMPLOYEENUMBER", "");

            Integer personID = CommonAPI.convertModelDetailToInteger(AuthHM.get("personID"));
            FundingHelper.determinePersonPaymentMethod(personID, getLoggedInEmployeeID());
        }

        if (accountDetailHM.get("FAQSETID") != null) {
            String faqSetId = CommonAPI.convertModelDetailToString(accountDetailHM.get("FAQSETID"));
            accountDetailHM.put("FAQSETID", faqSetId);
        }


        if (accountDetailHM.get("EMPLOYEELOWBALANCETHRESHOLD") != null) {
            String employeeLowBalanceThreshold = CommonAPI.convertModelDetailToString(accountDetailHM.get("EMPLOYEELOWBALANCETHRESHOLD"));
            accountDetailHM.put("EMPLOYEELOWBALANCETHRESHOLD", employeeLowBalanceThreshold);
        }

        if(accountDetailHM.get("ORGLEVELPRIMARYID") != null){
            String orgLevel = CommonAPI.convertModelDetailToString(accountDetailHM.get("ORGLEVELPRIMARYID"));
            accountDetailHM.put("ORGLEVELPRIMARYID", orgLevel);
        }

        if(accountDetailHM.get("ORGLEVELPRIMARYNAME") != null){
            String orgLevelName = CommonAPI.convertModelDetailToString(accountDetailHM.get("ORGLEVELPRIMARYNAME"));
            accountDetailHM.put("ORGLEVELPRIMARYNAME", orgLevelName);
        }

        for (Map.Entry<String, String> accountDetailEntry : accountDetailHM.entrySet()) {
            AccountDetailModel thisAccountDetailModel = new AccountDetailModel(accountDetailEntry.getKey(), accountDetailEntry.getValue());
            if (!thisAccountDetailModel.getValue().equals("N/A")) { //only add the model to the collection if there is a valid value
                getCollection().add(thisAccountDetailModel);
            }
        }

        if(accountDetailHM.get("FAQSETID") != null && !accountDetailHM.get("FAQSETID").equals("")) {
            Integer faqSetID = CommonAPI.convertModelDetailToInteger(accountDetailHM.get("FAQSETID"));
            FAQCollection faqCollection = new FAQCollection(faqSetID);
            if(faqCollection.getCollection().size() > 0) {
                AccountDetailModel thisAccountDetailModel = new AccountDetailModel("FAQS", faqCollection.getCollection());
                getCollection().add(thisAccountDetailModel);
            }
        }

        return this;
    }

    //STATIC - change account status
    //TODO: (MED) - Method is using an old error handling method -jrmitaly - 6/7/2016
    public static HashMap<String, String> modifyAccountStatus(String changeToAccountStatus, HttpServletRequest request) {
        HashMap<String, String> responseHM = new HashMap<String, String>();
        try {

            Integer accountID = CommonAuthResource.checkRequestHeaderForMMHayesAuth(request);
            if (accountID != null && accountID != 0 ) {

                Integer updateResult = 0;

                //modify the logged in account's (employee's) status
                updateResult = dm.parameterizedExecuteNonQuery("data.myqc.modifyAccountStatus",
                        new Object[]{
                            accountID,
                            changeToAccountStatus
                        }
                );

                if (updateResult == 1) { //freezing the account was successful

                    String accountStatusCurrent = "N/A";
                    String accountStatusNew = "N/A";
                    if (changeToAccountStatus.equals("A")) {
                        accountStatusCurrent = "F"; //TODO: (MED) This is an assumption, should query?
                        accountStatusNew = "A";
                    } else if (changeToAccountStatus.equals("F")) {
                        accountStatusCurrent = "A"; //TODO: (MED) This is an assumption, should query?
                        accountStatusNew = "F";
                    }

                    //log account status change to QC_EmployeeChanges table
                    Integer logAccountChangeResult = 0;
                    logAccountChangeResult = dm.parameterizedExecuteNonQuery("data.myqc.recordAccountChangeAsMyQCUser",
                            new Object[]{
                                accountID,
                                "Status",
                                accountStatusCurrent,
                                accountStatusNew
                            }
                    );
                    if (logAccountChangeResult == 0) {
                        Logger.logMessage("ERROR: Could not LOG account status change from "+accountStatusCurrent+" to "+accountStatusNew+" for account ID: "+accountID, Logger.LEVEL.ERROR);
                    } else {
                        Logger.logMessage("Successfully modified account status to from "+accountStatusCurrent+" to "+accountStatusNew+" for account ID: "+accountID, Logger.LEVEL.ERROR);
                        responseHM.put("status", "success");
                        responseHM.put("details", "Successfully modified account status.");
                    }
                } else {
                    Logger.logMessage("ERROR: Failed to modify account status in AccountDetailCollection.modifyAccountStatus()", Logger.LEVEL.ERROR);
                    responseHM.put("status", "failed");
                    responseHM.put("details", "Failed to modify account status.");
                }
            } else {
                Logger.logMessage("ERROR: Could not determine valid accountID in AccountDetailCollection.modifyAccountStatus()", Logger.LEVEL.ERROR);
                responseHM.put("status", "failed");
                responseHM.put("details", "Invalid Access.");
            }
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
            responseHM.put("status", "error");
            responseHM.put("details", commonMMHFunctions.convertExceptionToString(ex));
        }
        return responseHM;
    }

    //getter
    public List<AccountDetailModel> getCollection() {
        return this.collection;
    }

    //getter
    private Integer getLoggedInEmployeeID() {
        return this.loggedInEmployeeID;
    }

    //setter
    private void setLoggedInEmployeeID(Integer loggedInEmployeeID) {
        this.loggedInEmployeeID = loggedInEmployeeID;
    }

}
