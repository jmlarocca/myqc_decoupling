package com.mmhayes.myqc.server.collections;

//mmhayes dependencies

import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.CommonAuthResource;
import com.mmhayes.common.api.errorhandling.exceptions.InvalidAuthException;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.commonMMHFunctions;
import com.mmhayes.myqc.server.models.MyQCNavModel;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

//myqc dependencies
//other dependencies

/* My QC Nav Collection
 Last Updated (automatically updated by SVN)
 $Author: ijgerstein $: Author of last commit
 $Date: 2021-06-29 15:27:01 -0400 (Tue, 29 Jun 2021) $: Date of last commit
 $Rev: 56230 $: Revision of last commit

 Notes: Collection of My QC Nav Models
*/
public class MyQCNavCollection {
    List<MyQCNavModel> collection = new ArrayList<MyQCNavModel>();
    private static DataManager dm = new DataManager();
    private Integer loggedInEmployeeID = null;
    private static commonMMHFunctions commFunc = new commonMMHFunctions();
    private static final int SPENDING_PROFILE_FEATURE_ID = 2;
    private static final int BALANCE_FEATURE_ID = 3;
    private static final int PURCHASES_FEATURE_ID = 4;
    private static final int DEDUCTIONS_FEATURE_ID = 5;
    private static final int ACCOUNT_FREEZE_FEATURE_ID = 6;
    private static final int ONLINE_ORDERING_FEATURE_ID = 7;
    private static final int ACCOUNT_FUNDING_FEATURE_ID = 8;
    private static final int LOYALTY_FEATURE_ID = 9;
    private static final int QUICK_PAY_FEATURE_ID = 10;
    private static final int FAQ_FEATURE_ID = 11;
    private static final int FEEDBACK_FEATURE_ID = 12;
    private boolean usingPersonAccount = false;

    //constructor - populates the collection
    public MyQCNavCollection(HttpServletRequest request) throws Exception {

        //determine the employeeID from the request
        setLoggedInEmployeeID(CommonAuthResource.checkRequestHeaderForMMHayesAuth(request));

        //populate this collection with models
        populateCollection(request);

    }

    //constructor - populates the collection
    public MyQCNavCollection(HttpServletRequest request, Integer accountID, boolean usingPersonAccount) throws Exception {

        setLoggedInEmployeeID(accountID);

        setUsingPersonAccount(usingPersonAccount);

        //populate this collection with models
        populateCollection(request);

    }

    //populates this collection with models
    public MyQCNavCollection populateCollection(HttpServletRequest request) throws Exception {

        //throw InvalidAuthException if no employee is found
        if (getLoggedInEmployeeID() == null) {
            throw new InvalidAuthException();
        }

        ArrayList<HashMap> featureSettings = commFunc.getMyQCConfigurableFeatures(request);

        boolean showSpendingProfiles = false;
        boolean showBalances = false;
        boolean showPurchases = false;
        boolean showDeductions = false;
        boolean showFreeze = false;
        boolean showOnlineOrdering = false;
        boolean showAccountFunding = false;
        boolean showLoyalty = false;
        boolean showQuickPay = false;
        boolean showFAQ = false;
        boolean showFeedback = false;
        String spendingProfileLabel = "Spending Profile";
        String balanceLabel = "Current Balance";
        String purchasesLabel = "Purchase History";
        String deductionsLabel = "Deduction History";
        String freezeLabel = "Freeze Account";
        String onlineOrderingLabel = "Online Ordering";
        String accountFundingLabel = "Account Funding";
        String loyaltyLabel = "Rewards";
        String quickPayLabel = "Quick Pay";
        String faqLabel = "FAQs";
        String feedbackLabel = "Leave Feedback";

        for ( HashMap featureSetting : featureSettings ) {
            Integer featureID = CommonAPI.convertModelDetailToInteger(featureSetting.get("MYQCFEATUREID"));
            boolean featureActive = CommonAPI.convertModelDetailToInteger(featureSetting.get("ACTIVE")) == 1;
            String featureLabel = CommonAPI.convertModelDetailToString(featureSetting.get("DISPLAYNAME"));
            String overrideDisplayLabel = CommonAPI.convertModelDetailToString(featureSetting.get("OVERRIDEDISPLAYNAME"));
            if(overrideDisplayLabel != null && !overrideDisplayLabel.equals("")) {
                featureLabel = overrideDisplayLabel;
            }

            switch (featureID) {
                case (SPENDING_PROFILE_FEATURE_ID):
                    showSpendingProfiles = featureActive;
                    spendingProfileLabel = featureLabel;
                    break;
                case (BALANCE_FEATURE_ID):
                    showBalances = featureActive;
                    balanceLabel = featureLabel;
                    break;
                case (PURCHASES_FEATURE_ID):
                    showPurchases = featureActive;
                    purchasesLabel = featureLabel;
                    break;
                case (DEDUCTIONS_FEATURE_ID):
                    showDeductions = !this.isUsingPersonAccount() && featureActive;
                    deductionsLabel = featureLabel;
                    break;
                case (ACCOUNT_FREEZE_FEATURE_ID):
                    showFreeze = !this.isUsingPersonAccount() && featureActive;
                    freezeLabel = featureLabel;
                    break;
                case (ONLINE_ORDERING_FEATURE_ID):
                    showOnlineOrdering = featureActive;
                    onlineOrderingLabel = featureLabel;
                    break;
                case (ACCOUNT_FUNDING_FEATURE_ID):
                    showAccountFunding = featureActive;
                    accountFundingLabel = featureLabel;
                    break;
                case (LOYALTY_FEATURE_ID):
                    showLoyalty = !this.isUsingPersonAccount() && featureActive;
                    loyaltyLabel = featureLabel;
                    break;
                case (QUICK_PAY_FEATURE_ID):
                    showQuickPay = !this.isUsingPersonAccount() && featureActive;
                    quickPayLabel = featureLabel;
                    break;
                case (FAQ_FEATURE_ID):
                    showFAQ = !this.isUsingPersonAccount() && featureActive;
                    faqLabel = featureLabel;
                    break;
                case (FEEDBACK_FEATURE_ID):
                    showFeedback = featureActive;
                    feedbackLabel = featureLabel;
                    break;
            }
        }

        //TODO: (MED) Was there supposed to be a MyQCNavModel or something? -jrmitaly 6/7/2016
        HashMap modelDetailHM = new HashMap<String, Object>();

        if ( getStoreCount() > 0 && showOnlineOrdering ) {
            //add Online Ordering to collection
            modelDetailHM.put("id", "order");
            modelDetailHM.put("title", onlineOrderingLabel);
            getCollection().add(new MyQCNavModel(modelDetailHM));
        } else if(showOnlineOrdering){
            modelDetailHM.put("id", "orderTour");
            modelDetailHM.put("title", onlineOrderingLabel);
            getCollection().add(new MyQCNavModel(modelDetailHM));
        }

        //add Rewards to collection
        if ( showLoyalty && getLoyaltyProgramCount() > 0 ) {
            modelDetailHM.put("id", "rewards");
            modelDetailHM.put("title", loyaltyLabel);
            getCollection().add(new MyQCNavModel(modelDetailHM));
        }

        if ( showBalances ) {
            //add Account Balances to collection
            modelDetailHM.put("id", "balances");
            modelDetailHM.put("title", balanceLabel);
            getCollection().add(new MyQCNavModel(modelDetailHM));
        }

        if ( showPurchases ) {
            //add Account Transactions (Purchases) to collection
            modelDetailHM.put("id", "purchases");
            modelDetailHM.put("title", purchasesLabel);
            getCollection().add(new MyQCNavModel(modelDetailHM));
        }

        //add Account Deductions to collection
        if ( getDeductionCount() > 0 && showDeductions ) {
            modelDetailHM.put("id", "deductions");
            modelDetailHM.put("title", deductionsLabel);
            getCollection().add(new MyQCNavModel(modelDetailHM));
        } else if (showDeductions){
            modelDetailHM.put("id", "deductionTour");
            modelDetailHM.put("title", deductionsLabel);
            getCollection().add(new MyQCNavModel(modelDetailHM));
        }

        //add Account Funding to collection
        if ( showAccountFunding || this.isUsingPersonAccount() ) {
            if ( checkIfAccountFundingIsAllowedForAccount() && CommonAPI.checkIfPayrollGroupAllowsFunding(request)) {
                modelDetailHM.put("id", "accountFunding");
                modelDetailHM.put("title", accountFundingLabel);
            } else if (checkIfAccountFundingIsAllowedForAccount() && !CommonAPI.checkIfPayrollGroupAllowsFunding(request)) {
                modelDetailHM.put("id", "accountFunding");
                modelDetailHM.put("title", "Payment Method");
            } else {
                modelDetailHM.put("id", "funding");
                modelDetailHM.put("title", "Funding History");
            }

            getCollection().add(new MyQCNavModel(modelDetailHM));
        }

        if ( showQuickPay && checkIfQuickPayIsAllowedForAccount() ) {
            modelDetailHM.put("id", "quickPay");
            modelDetailHM.put("title", quickPayLabel);
            getCollection().add(new MyQCNavModel(modelDetailHM));
        } else if (showQuickPay) {
            modelDetailHM.put("id", "quickPayTour");
            modelDetailHM.put("title", quickPayLabel);
            getCollection().add(new MyQCNavModel(modelDetailHM));
        }

        if ( showFreeze ) {
            //add Account Transactions (Purchases) to collection
            modelDetailHM.put("id", "freeze");
            modelDetailHM.put("title", freezeLabel);
            getCollection().add(new MyQCNavModel(modelDetailHM));
        }

        //for person accounts include a link to the select account page
        if ( this.isUsingPersonAccount() ) {
            HashMap aliasHM = CommonAPI.getAliases();
            String accountAlias = aliasHM.get("ACCOUNTALIAS").toString();
            modelDetailHM.put("id", "selectAccount");
            modelDetailHM.put("title", "Add/Select " + accountAlias);
            getCollection().add(new MyQCNavModel(modelDetailHM));
        }

        //add Spending Limits to collection
        if( showSpendingProfiles ) {
            modelDetailHM.put("id", "spendinglimits");
            modelDetailHM.put("title", spendingProfileLabel);
            if(this.isUsingPersonAccount()) {
                modelDetailHM.put("title", "Settings");
            }
            getCollection().add(new MyQCNavModel(modelDetailHM));
        }

        if ( showFAQ && checkForFAQSet() ) {
            modelDetailHM.put("id", "faq");
            modelDetailHM.put("title", faqLabel);
            getCollection().add(new MyQCNavModel(modelDetailHM));
        } else if (showFAQ) {
            showFAQ = false;
        }

        //add Leave Feedback to collection
        if (showFeedback) {
            modelDetailHM.put("id", "feedback");
            modelDetailHM.put("title", feedbackLabel);
            getCollection().add(new MyQCNavModel(modelDetailHM));
        }

        //add About Quickcharge to collection
        modelDetailHM.put("id", "about");
        modelDetailHM.put("title", "About Quickcharge");
        getCollection().add(new MyQCNavModel(modelDetailHM));

        //add Online Ordering to collection
        modelDetailHM.put("id", "logout");
        modelDetailHM.put("title", "Log Out");
        getCollection().add(new MyQCNavModel(modelDetailHM));

        return this;
    }

    private Integer getStoreCount() throws Exception {
        Integer storeCount = 0;
        Object storeCountObj = dm.parameterizedExecuteScalar("data.ordering.getStores_count", new Object[] {getLoggedInEmployeeID()});
        if (storeCountObj != null) {
            storeCount = Integer.parseInt(storeCountObj.toString());
        }
        return storeCount;
    }

    //get the count of deductions the currently logged in employee has
    private Integer getDeductionCount() throws Exception {
        Integer deductionCount = 0;
        Object deductionCountObj = dm.parameterizedExecuteScalar("data.myqc.getEmployeeDeductions_count", new Object[] {getLoggedInEmployeeID()});
        if (deductionCountObj != null) {
            deductionCount = Integer.parseInt(deductionCountObj.toString());
        }
        return deductionCount;
    }

    //get the count of fund records (QC Transactions against a special terminal type) the currently logged in employee has
    private Integer getFundCount() throws Exception {
        Integer fundCount = 0;
        Object fundCountObj = dm.parameterizedExecuteScalar("data.myqc.getEmployeeFunds_count", new Object[] {getLoggedInEmployeeID()});
        fundCount = Integer.parseInt(fundCountObj.toString());
        return fundCount;
    }

    private Boolean checkIfAccountFundingIsAllowedForAccount() throws Exception {
        Boolean fundingAllowed = false;

        Object fundingTerminal = dm.parameterizedExecuteScalar("data.myqc.checkIfAccountFundingIsAllowedForAccount", new Object[] {getLoggedInEmployeeID()});

        if ( fundingTerminal != null && !fundingTerminal.toString().equals("")) {
            fundingAllowed = true;
        }

        return fundingAllowed;
    }

    private Integer getLoyaltyProgramCount() throws Exception {
        Object programCountObj = dm.parameterizedExecuteScalar("data.loyalty.getLoyaltyPrograms_count", new Object[] {});

        if ( programCountObj == null ) {
            return 0;
        }

        return CommonAPI.convertModelDetailToInteger(programCountObj);
    }

    private Boolean checkIfQuickPayIsAllowedForAccount() throws Exception {
        Boolean quickPayAllowed = false;

        Object isQuickPayAllowedObj = dm.parameterizedExecuteScalar("data.myqc.checkIfQuickPayIsAllowedForAccount", new Object[] {getLoggedInEmployeeID()});

        if ( isQuickPayAllowedObj != null ) {
            quickPayAllowed = Boolean.parseBoolean(isQuickPayAllowedObj.toString());
        }

        return quickPayAllowed;
    }

    private Boolean checkForFAQSet() throws Exception {
        Boolean faqSetFound = false;

        Object faqSetObj = dm.parameterizedExecuteScalar("data.myqc.checkForFAQSet", new Object[] {getLoggedInEmployeeID()});

        if ( faqSetObj != null && !faqSetObj.toString().equals("") ) {
            faqSetFound = true;
        }

        return faqSetFound;
    }

    //getter
    public List<MyQCNavModel> getCollection() {
        return this.collection;
    }

    //getter
    private Integer getLoggedInEmployeeID() {
        return this.loggedInEmployeeID;
    }

    //setter
    private void setLoggedInEmployeeID(Integer loggedInEmployeeID) {
        this.loggedInEmployeeID = loggedInEmployeeID;
    }

    public boolean isUsingPersonAccount() {
        return usingPersonAccount;
    }

    public void setUsingPersonAccount(boolean usingPersonAccount) {
        this.usingPersonAccount = usingPersonAccount;
    }

    // Get the feedback URL and feedback redirect message from QC_Globals
    public static HashMap<String, String> getFeedbackSettings() {
        HashMap<String, String> feedbackSettings = new HashMap<>();
        ArrayList<HashMap> query = dm.parameterizedExecuteQuery("data.globalSettings.getFeedbackSettings", new Object[]{}, true);
        if (query != null && query.size() > 0 && query.get(0) != null) {
            HashMap<String, String> settings = query.get(0);
            feedbackSettings.put("url", settings.get("FEEDBACKURL"));
            feedbackSettings.put("message", settings.get("FEEDBACKREDIRECTMESSAGE"));
        }
        return feedbackSettings;
    }

}
