package com.mmhayes.myqc.server.collections;

//mmhayes dependencies

import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.CommonAuthResource;
import com.mmhayes.common.api.errorhandling.exceptions.InvalidAuthException;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.commonMMHFunctions;
import com.mmhayes.common.rotations.RotationHandler;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.myqc.server.models.ModifierModel;
import com.mmhayes.myqc.server.models.StoreModel;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

//myqc dependencies
//other dependencies

/* Modifier Collection
 Last Updated (automatically updated by SVN)
 $Author: jkflanagan $: Author of last commit
 $Date: 2020-12-15 09:52:29 -0500 (Tue, 15 Dec 2020) $: Date of last commit
 $Rev: 53306 $: Revision of last commit

 Notes: Collection of Modifier Models
*/
public class ModifierCollection {
    List<ModifierModel> collection = new ArrayList<ModifierModel>();
    private static DataManager dm = new DataManager();
    private commonMMHFunctions commFunc = new commonMMHFunctions();
    private Integer authenticatedAccountID = null;

    //constructor - populates the collection
    public ModifierCollection(HttpServletRequest request) throws Exception {

        //determine the authenticated account ID from the request
        setAuthenticatedAccountID(CommonAuthResource.checkRequestHeaderForMMHayesAuth(request));

        //populate this collection with models
        populateCollection(request);

    }

    /*OVERLOAD - constructor - populates the collection
        @param modifiers: Array list of Modifier Models used to populate the collection
        @param request: Standard HTTP request object - used for authentication
    */
    public ModifierCollection(List<ModifierModel> modifiers, HttpServletRequest request, Integer modifierSetID) throws Exception {

        //determine the authenticated account ID from the request
        setAuthenticatedAccountID(CommonAuthResource.checkRequestHeaderForMMHayesAuth(request));

        //populate this collection with models
        populateCollection(modifiers, request, modifierSetID);

    }

    /*OVERLOAD - constructor - populates the collection
        @param modifiers: Array list of Modifier Models used to populate the collection
        @param request: Standard HTTP request object - used for authentication
    */
    public ModifierCollection(Integer modMenuID, Integer modSetDetailID, HttpServletRequest request) throws Exception {

        //determine the authenticated account ID from the request
        setAuthenticatedAccountID(CommonAuthResource.checkRequestHeaderForMMHayesAuth(request));

        //populate this collection with models
        populateCollection(modMenuID, modSetDetailID, request);

    }

    /*OVERLOAD - constructor - populates the collection
        @param parentProductID: ID of the parent product to which this modifier collection belongs
        @param request: Standard HTTP request object - used for authentication
    */
    @SuppressWarnings("unchecked")
    public ModifierCollection (Integer parentProductID, HttpServletRequest request) throws Exception {

        //validate the ID of the parent product
        if (parentProductID == null || parentProductID <= 0) {
            throw new IllegalArgumentException("The ID of the parent product passed to the ModifierCollection constructor must be greater than 0!");
        }

        //determine the authenticated account ID from the request
        setAuthenticatedAccountID(CommonAuthResource.checkRequestHeaderForMMHayesAuth(request));

        //get the list of default modifiers for the product
        ArrayList<HashMap> defaultModifierHMs = dm.parameterizedExecuteQuery("data.ordering.getDefaultModifiers", new Object[]{parentProductID}, true);

        populateCollection(defaultModifierHMs);
    }

    //populates this collection with models - standard
    public ModifierCollection populateCollection(HttpServletRequest request) throws Exception {

        //throw InvalidAuthException if no authenticatedAccountID is found
        if (getAuthenticatedAccountID() == null) {
            throw new InvalidAuthException();
        }

        //retrieve the storeID (MyQCTerminalID) out of the request
        Integer storeID = CommonAPI.checkRequestHeaderForStoreID(request);

        //get all models in an array list
        ArrayList<HashMap> modifierList = dm.parameterizedExecuteQuery("data.ordering.getAllModifiers",
                new Object[]{
                        storeID
                },
                true
        );

        //iterate through list, create models and add them to the collection
        if (modifierList != null && modifierList.size() > 0) {
            for (HashMap modifierHM : modifierList) {
                if (modifierHM.get("ID") != null && !modifierHM.get("ID").toString().isEmpty()) {
                    //create a new model and add to the collection
                    modifierHM.put("KEYPADID", CommonAPI.convertModelDetailToInteger(modifierHM.get("ID")));
                    getCollection().add(new ModifierModel(modifierHM));
                }
            }
        }

        return this;
    }

    /*OVERLOAD - populates this collection with models
        @param modifiers: Array list of Modifier Models used to populate the collection
    */
    public ModifierCollection populateCollection(List<ModifierModel> modifiers, HttpServletRequest request, Integer modifierSetID ) throws Exception {

        //throw InvalidAuthException if no authenticatedAccountID is found
        if (getAuthenticatedAccountID() == null) {
            throw new InvalidAuthException();
        }

        //populate this collection from passed in array list of modifier models
        if (modifiers != null && modifiers.size() > 0) {

            //reset collection then rebuild it - one modifier at a time
            for (ModifierModel modifierModel : modifiers) {

                if (modifierModel.getID() != null && !modifierModel.getID().toString().isEmpty()) {

                    //retrieve the storeID (MyQCTerminalID) out of the request
                    Integer storeID = CommonAPI.checkRequestHeaderForStoreID(request);

                    //update modifier model here from database - one modifier at a time
                    ArrayList<HashMap> singleModifierList = dm.parameterizedExecuteQuery("data.ordering.getModifier",
                            new Object[]{
                                    modifierModel.getID(),
                                    storeID,
                                    modifierSetID
                            },
                            true
                    );

                    if (singleModifierList != null && singleModifierList.size() == 1) {
                        HashMap singleModifierHM = singleModifierList.get(0);
                        if (singleModifierHM.get("MODIFIERID") != null && !singleModifierHM.get("MODIFIERID").toString().isEmpty()) {
                            //update the modindex from the front
                            //singleModifierHM.put("MODINDEX",modifierModel.getModIndex());

                            ModifierModel modModel = new ModifierModel(singleModifierHM);

                            if(modifierModel.getPrepOption() != null) {
                                modModel.setPrepOptionSetID(modifierModel.getPrepOptionSetID());
                                modModel.setPrepOption(modifierModel.getPrepOption());
                            }

                            modModel.setHasPrinterMappings(modifierModel.getHasPrinterMappings());

                            //create a new model and add to the collection
                            getCollection().add(modModel);
                        }
                    } else {
                        Logger.logMessage("ERROR: Could not determine updated single Modifier Model in ModifierCollection.populateCollection(modifiers)", Logger.LEVEL.ERROR);
                    }

                } else {
                    Logger.logMessage("ERROR: Could not determine Modifier Model in ModifierCollection.populateCollection(modifiers)", Logger.LEVEL.ERROR);
                }

            }

        }

        return this;
    }

    /*OVERLOAD - populates this collection with models
        @param modMenuID: "Modifier Menu" ID (PAKeypadID off of the QC_PAModifierSetDetail table) used to populate the collection
    */
    public ModifierCollection populateCollection(Integer modMenuID, Integer modSetDetailID, HttpServletRequest request) throws Exception {

        //throw InvalidAuthException if no authenticatedAccountID is found
        if (getAuthenticatedAccountID() == null) {
            throw new InvalidAuthException();
        }

        //retrieve the storeID (MyQCTerminalID) out of the request
        Integer storeID = CommonAPI.checkRequestHeaderForStoreID(request);

        //use the storeID to get the store model
        StoreModel storeModel = StoreModel.getStoreModel(storeID, request);

        //check if this modifier keypad is a controller keypad, returns rotating keypad detail if true
        modMenuID = RotationHandler.checkForRotatingKeypad(storeModel.getID(), modMenuID, storeModel.getCurrentStoreDTM(), true);

        //get all models in an array list
        ArrayList<HashMap> modifierList = dm.parameterizedExecuteQuery("data.ordering.getModifiersOnModMenu",
                new Object[]{
                        modMenuID,
                        storeID,
                        modSetDetailID
                },
                true
        );

        //populate this collection from an array list of hashmaps
        if (modifierList != null && modifierList.size() > 0) {
            for (HashMap modifierHM : modifierList) {
                if (modifierHM.get("ID") != null && !modifierHM.get("ID").toString().isEmpty()) {
                    //create a new model and add to the collection
                    modifierHM.put("KEYPADID", CommonAPI.convertModelDetailToInteger(modifierHM.get("ID")));
                    getCollection().add(new ModifierModel(modifierHM));
                }
            }
        }

        return this;
    }

    /*OVERLOAD - populates this collection with models
        @param defaultModifierHMs: An ArrayList of HashMaps corresponding to the default modifier records used to populate the collection
    */
    @SuppressWarnings({"Convert2streamapi", "unchecked"})
    public ModifierCollection populateCollection (List<HashMap> defaultModifierHMs) throws Exception {

        //throw InvalidAuthException if no authenticatedAccountID is found
        if (getAuthenticatedAccountID() == null) {
            throw new InvalidAuthException();
        }

        if (DataFunctions.isEmptyCollection(defaultModifierHMs)) {
            Logger.logMessage("The list of default modifier records passed to ModifierCollection.populate collection can't be null or empty!", Logger.LEVEL.DEBUG);
            return null;
        }

        for (HashMap defaultModifierHM : defaultModifierHMs) {
            if (!DataFunctions.isEmptyMap(defaultModifierHM)) {
                //create a new modifier model and add it to the collection
                getCollection().add(new ModifierModel(defaultModifierHM));
            }
        }

        return this;
    }

    //getter
    public List<ModifierModel> getCollection() {
        return this.collection;
    }

    //setter
    public void setCollection(List<ModifierModel> collection) {
        this.collection = collection;
    }

    //getter
    private Integer getAuthenticatedAccountID() {
        return this.authenticatedAccountID;
    }

    //setter
    private void setAuthenticatedAccountID(Integer authenticatedAccountID) {
        this.authenticatedAccountID = authenticatedAccountID;
    }

}
