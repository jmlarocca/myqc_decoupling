package com.mmhayes.myqc.server.collections;

//mmhayes dependencies
import com.mmhayes.common.api.*;
import com.mmhayes.common.dataaccess.*;
import com.mmhayes.common.utils.*;
import com.mmhayes.common.api.errorhandling.exceptions.*;

//myqc dependencies
import com.mmhayes.myqc.server.models.*;

//other dependencies
import javax.servlet.http.HttpServletRequest;
import java.util.*;

/* Spending Profile Collection
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2018-06-04 10:25:47 -0400 (Mon, 04 Jun 2018) $: Date of last commit
 $Rev: 30195 $: Revision of last commit

 Notes: Collection of Spending Profile Models
*/
public class SpendingProfileCollection {
    List<SpendingProfileModel> collection = new ArrayList<SpendingProfileModel>();
    private static DataManager dm = new DataManager();
    private static commonMMHFunctions comm = new commonMMHFunctions(); //the usual common mmh functions
    private Integer loggedInEmployeeID = null;

    //constructor - populates the collection
    public SpendingProfileCollection(HttpServletRequest request) throws Exception {

        //determine the employeeID from the request
        setLoggedInEmployeeID(CommonAuthResource.checkRequestHeaderForMMHayesAuth(request));

        //populate this collection with models
        populateCollection();

    }

    //populates this collection with models
    public SpendingProfileCollection populateCollection() throws Exception {

        //throw InvalidAuthException if no employeeID is found
        if (getLoggedInEmployeeID() == null) {
            throw new InvalidAuthException();
        }

        //get all models in an array list
        ArrayList<HashMap> spendingProfileList = dm.parameterizedExecuteQuery("data.myqc.getEmployeeSpendingProfiles",
                new Object[]{
                    getLoggedInEmployeeID()
                },
                true
        );

        //iterate through list, create models and add them to the collection
        if (spendingProfileList != null && spendingProfileList.size() > 0) {
            for (HashMap spendingProfileHM : spendingProfileList) {
                getCollection().add(new SpendingProfileModel(spendingProfileHM));
            }
        }

        return this;
    }

    //checks if the logged in employee has more than one spending profile and responds with a Boolean in a hashmap
    //TODO: (MED) - Method is using an old error handling method -jrmitaly - 6/7/2016
    public HashMap<String, Boolean> checkSpendingProfileCount() {
        HashMap<String, Boolean> checkHM = new HashMap<String, Boolean>();
        try {
            Boolean check = false;
            if (getSpendingProfileCount() > 1) {
                check = true;
            }
            checkHM.put("value", check);
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
        return checkHM;
    }

    //checks if the logged in employee has access to the passed in spendingProfileID
    public Boolean validateSpendingProfile(Integer spendingProfileID) {
        Boolean validated = false;

        //run SQL to validate if the logged in employee has access to this spending profile
        ArrayList<HashMap> spendingProfileList = dm.parameterizedExecuteQuery("data.myqc.validateEmployeeSpendingProfile",
                new Object[]{
                    getLoggedInEmployeeID().toString(),
                    spendingProfileID
                },
                true
        );

        //if there is a valid result then the employee has acess
        if (spendingProfileList != null && spendingProfileList.size() > 0) {
            validated = true;
        }

        return validated;
    }

    //updates the logged in employee spending profile using the spendingProfileID in spendingProfileHM
    //TODO: (MED) - Method is using an old error handling method -jrmitaly - 6/7/2016
    public HashMap<String, String> updateEmployeeSpendingProfile(HashMap spendingProfileHM) {
        HashMap<String, String> responseHM = new HashMap<String, String>();
        try {
            Integer employeeID = getLoggedInEmployeeID(); //employee ID (account ID)
            if (employeeID != null) {

                //get spendingProfileID out of spendingProfileHM
                Integer spendingProfileID = null;
                if (spendingProfileHM != null && spendingProfileHM.get("id") != null) {
                    spendingProfileID = Integer.parseInt(spendingProfileHM.get("id").toString());
                } else {
                    Logger.logMessage("ERROR: Could not determine valid spending profile ID in SpendingProfileCollection.updateEmployeeSpendingProfile()", Logger.LEVEL.ERROR);
                    return responseHM;
                }

                //checks if the logged in employee has access to the passed in spendingProfileID
                if (validateSpendingProfile(spendingProfileID) || spendingProfileHM.get("oldSpendingProfileList") != null) {

                    //log spending profile change to QC_EmployeeChanges table
                    Integer logSpendingProfileChangeResult = 0;
                    logSpendingProfileChangeResult = dm.parameterizedExecuteNonQuery("data.myqc.recordSpendingProfileChangeAsMyQCUser",
                            new Object[]{
                                employeeID,
                                spendingProfileID
                            }
                    );
                    if (logSpendingProfileChangeResult == 1) {

                        //prepare sql parameters and then execute update SQL
                        Integer updateResult = dm.parameterizedExecuteNonQuery("data.myqc.updateEmployeeSpendingProfile",
                                new Object[]{
                                        employeeID,
                                        spendingProfileID
                                }
                        );

                        //record success in response
                        if (updateResult == 1) {
                            Logger.logMessage("Successfully modified spending profile for account ID:"+employeeID, Logger.LEVEL.TRACE);
                            responseHM.put("status", "success");
                            responseHM.put("details", "Successfully modified spending profile.");
                        } else {
                            Logger.logMessage("ERROR: Could not update spending profile in SpendingProfileCollection.updateEmployeeSpendingProfile() - ID = "+spendingProfileID, Logger.LEVEL.ERROR);
                            responseHM.put("status", "failed");
                            responseHM.put("details", "Could not modify spending profile.");
                        }
                    } else {
                        Logger.logMessage("ERROR: Could not LOG account spending profile change for account ID: "+employeeID, Logger.LEVEL.ERROR);
                        responseHM.put("status", "failed");
                        responseHM.put("details", "Could not modify spending profile due to not being able to log the change.");
                    }
                } else {
                    Logger.logMessage("ERROR: Could not validate spending profile  ID in SpendingProfileCollection.updateEmployeeSpendingProfile() - ID = "+spendingProfileID, Logger.LEVEL.ERROR);
                    responseHM.put("status", "failed");
                    responseHM.put("details", "Invalid Access.");
                }
            } else {
                Logger.logMessage("ERROR: Could not determine valid logged in employee ID in SpendingProfileCollection.updateEmployeeSpendingProfile()", Logger.LEVEL.ERROR);
                responseHM.put("status", "failed");
                responseHM.put("details", "Invalid Access.");
            }
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
            responseHM.put("status", "error");
            responseHM.put("details", commonMMHFunctions.convertExceptionToString(ex));
        }
        return responseHM;
    }

    //get the count of spending profiles the currently logged in employee has access to
    private Integer getSpendingProfileCount() {
        Integer spendingProfileCount = 0;
        Object spendingProfileCountObj = dm.parameterizedExecuteScalar("data.myqc.getEmployeeSpendingProfiles_count", new Object[] {getLoggedInEmployeeID()});
        if (spendingProfileCountObj != null) {
            spendingProfileCount = Integer.parseInt(spendingProfileCountObj.toString());
        }
        return spendingProfileCount;
    }

    //getter
    public List<SpendingProfileModel> getCollection() {
        return this.collection;
    }

    //getter
    private Integer getLoggedInEmployeeID() {
        return this.loggedInEmployeeID;
    }

    //setter
    private void setLoggedInEmployeeID(Integer loggedInEmployeeID) {
        this.loggedInEmployeeID = loggedInEmployeeID;
    }

}
