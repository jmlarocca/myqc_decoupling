package com.mmhayes.myqc.server.collections;

//mmhayes dependencies
import com.mmhayes.common.api.*;
import com.mmhayes.common.dataaccess.*;
import com.mmhayes.common.api.errorhandling.exceptions.*;

//myqc dependencies
import com.mmhayes.myqc.server.api.MyQCCache;
import com.mmhayes.myqc.server.models.*;

//other dependencies
import javax.servlet.http.HttpServletRequest;
import java.util.*;

/* Store Collection
 Last Updated (automatically updated by SVN)
 $Author: mjbatko $: Author of last commit
 $Date: 2021-02-11 10:37:47 -0500 (Thu, 11 Feb 2021) $: Date of last commit
 $Rev: 54203 $: Revision of last commit

 Notes: Collection of Store Models
*/
public class StoreCollection {
    List<StoreModel> collection = new ArrayList<StoreModel>();
    private static DataManager dm = new DataManager();
    private Integer authenticatedAccountID = null;
    private String futureOrderDate = "";
    private Integer primaryOrgLevelId = null;

    //constructor - populates the collection
    public StoreCollection(HttpServletRequest request) throws Exception {

        //determine the authenticated account ID from the request
        setAuthenticatedAccountID( CommonAuthResource.checkRequestHeaderForMMHayesAuth( request ) );

        //populate this collection with models
        populateCollection();

    }

    //constructor - populates the collection
    public StoreCollection(HttpServletRequest request, String futureDate) throws Exception {

        //determine the authenticated account ID from the request
        setAuthenticatedAccountID( CommonAuthResource.checkRequestHeaderForMMHayesAuth( request ) );

        //set the future date the order will be placed
        setFutureOrderDate(futureDate);

        //populate this collection with models
        populateCollection();

    }

    public StoreCollection(HttpServletRequest request, Integer locationId, String futureDate) throws Exception{

        setAuthenticatedAccountID( CommonAuthResource.checkRequestHeaderForMMHayesAuth( request ) );

        setPrimaryOrgLevelId(locationId);

        //set the future date the order will be placed
        if(futureDate.equals("null")){
            setFutureOrderDate("");
        }
        else{
            setFutureOrderDate(futureDate);
        }

        // use new query
        populateCollection();

    }

    //populates this collection with models
    public StoreCollection populateCollection() throws Exception {

        //throw InvalidAuthException if no authenticatedAccountID is found
        if (getAuthenticatedAccountID() == null) {
            throw new InvalidAuthException();
        }
        // if there is a primary
        String getSQL = "data.ordering.getStores";
        Object[] params = new Object[]{
                getAuthenticatedAccountID()
        };

        if(getPrimaryOrgLevelId() != null){
            getSQL = "data.ordering.getStoresByOrgLevelId";
            params = new Object[]{
                    getAuthenticatedAccountID(),
                    getPrimaryOrgLevelId()
            };
        }

        //get all models in an array list
        ArrayList<HashMap> storeList = dm.parameterizedExecuteQuery(getSQL, params, true);

        //populate this collection from an array list of hashmaps
        if (storeList != null && storeList.size() > 0) {
            for (HashMap storeHM : storeList) {
                Integer storeId = CommonAPI.convertModelDetailToInteger(storeHM.get("ID"), null);

                if ( storeId == null ) {
                    continue;
                }

                if(getFutureOrderDate().equals("")) {
                    this.getCollection().add( new StoreModel( storeHM, getAuthenticatedAccountID() ) );
                } else {
                    this.getCollection().add( new StoreModel( storeHM, getAuthenticatedAccountID(), getFutureOrderDate() ) );
                }
            }
        }

        return this;
    }

    //getter
    public List<StoreModel> getCollection() {
        return this.collection;
    }

    //getter
    private Integer getAuthenticatedAccountID() {
        return this.authenticatedAccountID;
    }

    //setter
    private void setAuthenticatedAccountID(Integer authenticatedAccountID) {
        this.authenticatedAccountID = authenticatedAccountID;
    }

    public String getFutureOrderDate() {
        return futureOrderDate;
    }

    public void setFutureOrderDate(String futureOrderDate) {
        this.futureOrderDate = futureOrderDate;
    }

    public Integer getPrimaryOrgLevelId() {
        return primaryOrgLevelId;
    }

    public void setPrimaryOrgLevelId(Integer primaryOrgLevelId) {
        this.primaryOrgLevelId = primaryOrgLevelId;
    }

}
