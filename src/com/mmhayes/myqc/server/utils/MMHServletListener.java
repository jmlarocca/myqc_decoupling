package com.mmhayes.myqc.server.utils;

//mmhayes dependencies

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.commonMMHFunctions;
import com.mmhayes.common.funding.FundingHelper;
import com.mmhayes.common.utils.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.LoggerContext;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

//other dependencies

/*
 Servlet Context Listener

 Created with IntelliJ IDEA.
 User: jrmitaly
 Date: 11/22/13
 Time: 1:54 PM

 Last Updated (automatically updated by SVN)
 $Author: ijgerstein $: Author of last commit
 $Date: 2021-09-21 11:07:25 -0400 (Tue, 21 Sep 2021) $: Date of last commit
 $Rev: 58178 $: Revision of last commit

Notes: This listener will actually listen for ALL servlets
 */
public class MMHServletListener implements ServletContextListener {

    private ScheduledExecutorService scheduler;
    private DataManager dm;
    private TaskScheduler taskScheduler;

    public static final String WAR_VERSION = "6.4.0";

    public void contextInitialized(ServletContextEvent contextEvent) {
        try {
            // Initialize static information via the servlet starter
            new MMHServletStarter(contextEvent.getServletContext());

            //try to load all properties files
            try {
                setLog4J2Config();
                MMHProperties.loadInstancePropertyFiles(contextEvent.getServletContext());
            } catch (Exception ex) {
                Logger.logException(ex);
                Logger.logMessage("ERROR: Could not retrieve/initialize properties file. Application cannot continue.", Logger.LEVEL.ERROR);
            }

            // Set Java Native Access Path For Passwords handling
            MMHCommonItemsLibrary.setJnaLibraryPath();

            //log My QC API Version.
            String warVersion = WAR_VERSION; // please update the static WAR_VERSION variable up top from now on, using that for an endpoint
            Logger.logMessage("My QC API war version number: "+warVersion, Logger.LEVEL.IMPORTANT);

            //update quickcharge version!
            dm = new DataManager();
            dm.serializeUpdate("data.software.UpdateVersion", new Object[]{warVersion});

            taskScheduler = new TaskScheduler(dm);
            taskScheduler.addRunnable(new FundingHelper(2));//2 is MyQCs application ID

            taskScheduler.initiateScheduledTasks();

        } catch (Exception ex) {
            Logger.logException(ex);
            Logger.logMessage("Critical Error! Could not initialize servlet. Check all logs", Logger.LEVEL.ERROR);
        }
    }

    public void contextDestroyed(ServletContextEvent e) {

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:MM:ss.SSS");
        System.out.println(sdf.format(new Date()) + " MMHServletListener shutdown start.");
        Logger.logMessage("MMH Servlet Listener in contextDestroyed ...", Logger.LEVEL.IMPORTANT);

        try{
            taskScheduler.shutdownScheduledTasks(sdf);

            // No Threads for you!
            Executors.newScheduledThreadPool(0);

            // Close connections
            DataManager.pool.closeConnections();

            // Stop ConnectionReaper thread
            DataManager.pool.stopConnectionReaper();

            // Deregister JDBC driver
            commonMMHFunctions.deregisterJDBCDriver();

            // Stop LoggerThread
            Logger.shutDown();

            Thread.yield();

        } catch (InterruptedException ex) {
            System.out.println(sdf.format(new Date()) + " Error in MMHServletListener.contextDestroyed method.");
            ex.printStackTrace();
        }
        System.out.println(sdf.format(new Date()) + " MMHServletListener shutdown complete.");

    }

    private void setLog4J2Config(){
        //SC2P: Remove all path functions as obsolete
        // /-*
        String configPath = MMHServletStarter.getInstancePropertyLocation()+"/log4j2.xml";
        // Check if there is a file at configPath
        File test = new File(configPath);
        if(test.exists()){
            System.out.println("FILE EXISTS: "+configPath);
            String fileURI = new File(MMHServletStarter.getInstancePropertyLocation()+"/log4j2.xml").toURI().toString();
            System.setProperty("log4j.configurationFile",fileURI);
            System.out.println("Setting log4j.configurationFile: "+fileURI);
            // Load Configuration
            ((org.apache.logging.log4j.core.LoggerContext) LogManager.getContext(false)).reconfigure();
        } else {
            try {
                LoggerContext context = (org.apache.logging.log4j.core.LoggerContext)LogManager.getContext(false);
                URI uri = this.getClass().getClassLoader().getResource("/log4j2_default.xml").toURI();
                System.out.println("DEFAULT: "+uri.toString());
                context.setConfigLocation(uri);
            } catch (URISyntaxException e) {
                System.out.println("Unable to load DEFAULT log4j2_default.xml");
            }
        }
        // *-/
    }

    private ScheduledExecutorService getScheduler() {
        return scheduler;
    }

    private void setScheduler(ScheduledExecutorService scheduler) {
        this.scheduler = scheduler;
    }

}
