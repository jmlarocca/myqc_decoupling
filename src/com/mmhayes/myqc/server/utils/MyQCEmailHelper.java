package com.mmhayes.myqc.server.utils;

import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.CommonAuthResource;
import com.mmhayes.common.api.Validation;
import com.mmhayes.common.api.errorhandling.exceptions.InvalidAuthException;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.commonMMHFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.MMHEmailBuilder;
import com.mmhayes.myqc.server.api.InstanceClient;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;

/*
 * $Author: ecdyer $: Author of last commit
 * $Date: 2017-02-09 17:04:46 -0500 (Thu, 09 Feb 2017) $: Date of last commit
 * $Rev: 20495 $: Revision of last commit
 * Notes: Loader for initializing APPCODE for configurable email - stores APPCODE statically for performance
 */
public class MyQCEmailHelper {

    private static DataManager dm = new DataManager(); //the usual data manager

    public static void initialize(HttpServletRequest request){

        try {
            // Load static AppCode if necessary
            if(MMHEmailBuilder.getAppCode().isEmpty()) {
                // Lookup APP Code
                Logger.logMessage("CommonAPI.getOnGround(): " + CommonAPI.getOnGround(), Logger.LEVEL.DEBUG);
                if(!CommonAPI.getOnGround()){

                    // Check if appcode already exists
                    if(!MMHEmailBuilder.getAppCode().isEmpty()){
                        // already set!
                        return;
                    }

                    // Update gateway use if in cloud
                    InstanceClient instanceClient = new InstanceClient();
                    Validation validation = new Validation();
                    // Prep validation object
                    validation.prepareInstanceToGatewayComm(request);
                    // execute call to gateway
                    Logger.logMessage("About to call in instance client...", Logger.LEVEL.DEBUG);
                    validation = instanceClient.getConfigurableEmailAppCode(validation, request);
                    if(validation.getCodes().isEmpty()){
                        // Log error details
                        Logger.logMessage("Gateway getConfigurableEmailAppCode call NOT successful", Logger.LEVEL.DEBUG);
                    }

                    // Get Data from the validation object
                    ArrayList<HashMap> codes = validation.getCodes();
                    if(codes.isEmpty() || !codes.get(0).containsKey("CODES")){
                        // Log data not found
                        Logger.logMessage("Gateway getConfigurableEmailAppCode call NOT successful. No Data Found.", Logger.LEVEL.DEBUG);
                    }

                    String code = codes.get(0).get("CODES").toString();
                    MMHEmailBuilder.setAppCode(code);
                }
            }
        } catch(Exception ex) {
            Logger.logMessage("Error in method initialize()", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }
    }

    public static boolean sendTOSEmail(int tosID, HttpServletRequest request){

        // If enabled globally, send confirmation email with TOS included
        boolean sendSuccess = false;
        // QUERIES
        final String QUERY_SEND_TOS_ENABLED = "data.globalSettings.getMyQCSendAcceptedTOSEmail";
        final String QUERY_GET_EMPLOYEE_DATA = "data.myqc.sendTOS.getEmployeeData";
        // FIELDS
        final String FIELD_SEND_TOS_ENABLED = "MYQCSENDACCEPTEDTOSEMAIL";
        final String FIELD_EMAIL = "EMAILADDRESS1";

        try {

            //throw InvalidAuthException if no authenticated employee is found
            Integer authenticatedEmployeeID = CommonAuthResource.checkRequestHeaderForMMHayesAuth(request);
            if (authenticatedEmployeeID == null) {
                throw new InvalidAuthException();
            }

            // Determine hostname for image urls
            String requestURL = request.getRequestURL().toString();
            String instanceName = commonMMHFunctions.getInstanceName().equals("qc")?"myqc":commonMMHFunctions.getInstanceName();
            String hostname = requestURL.substring(0, requestURL.indexOf("/" + instanceName + "/"));

            // Check if enabled in Globals
            ArrayList<HashMap> arrayList = dm.parameterizedExecuteQuery(QUERY_SEND_TOS_ENABLED, new Object[]{}, true);
            if(!arrayList.isEmpty()){
                if(arrayList.get(0).containsKey(FIELD_SEND_TOS_ENABLED)){
                    // if NOT enabled return success
                    if(!Boolean.parseBoolean(arrayList.get(0).get(FIELD_SEND_TOS_ENABLED).toString())){
                        // NOT ENABLED
                        Logger.logMessage("Not Sending TOS Accepted email for: "+authenticatedEmployeeID+" Global Setting: MyQC Send Accepted TOS is NOT enabled.", Logger.LEVEL.DEBUG);
                        return true; // Do not send email - check is successful
                    } else {
                        // ENABLED SEND EMAIL
                        Logger.logMessage("Global Setting: MyQC Send Accepted TOS is enabled.", Logger.LEVEL.DEBUG);

                        // Lookup Email Address
                        String emailAddress;
                        arrayList = dm.parameterizedExecuteQuery(QUERY_GET_EMPLOYEE_DATA, new Object[]{authenticatedEmployeeID}, true);
                        if(arrayList.isEmpty()){
                            Logger.logMessage("Not Sending TOS Accepted email for: "+authenticatedEmployeeID+". Cannot locate employee record.", Logger.LEVEL.ERROR);
                            return false;
                        }
                        if(arrayList.get(0).containsKey(FIELD_EMAIL) && !arrayList.get(0).get(FIELD_EMAIL).toString().isEmpty()){
                            emailAddress = arrayList.get(0).get(FIELD_EMAIL).toString();
                        } else {
                            Logger.logMessage("Not Sending TOS Accepted email for: "+authenticatedEmployeeID+". No Email Address found.", Logger.LEVEL.ERROR);
                            return false;
                        }

                        // Build params HashMap
                        HashMap params = new HashMap();
                        params.put("EmployeeID", authenticatedEmployeeID);
                        params.put("TOS_ID", tosID);
                        params.put("HostName", hostname);

                        // Define EmailTypeID
                        int emailTypeID = 5;

                        // Send Email
                        commonMMHFunctions common = new commonMMHFunctions();
                        if(common.sendTemplateEmail(emailAddress, 0, emailTypeID, "", params)){
                            Logger.logMessage("Successfully sent TOS Accepted email to "+emailAddress, Logger.LEVEL.DEBUG);
                            sendSuccess = true;
                        } else {
                            Logger.logMessage("Failed to send TOS Accepted email to "+emailAddress, Logger.LEVEL.ERROR);
                        }
                    }
                }
            }

        } catch(Exception ex){
            Logger.logMessage("Error in method: MyQCEmailHelper.sendTOSEmail()");
            Logger.logException(ex);
        }
        return sendSuccess;
    }
}
