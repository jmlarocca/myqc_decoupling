package com.mmhayes.myqc.server.utils;

import java.util.*;

/**
 * <p>Title: QuickCharge 4</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: MMHayes</p>
 * @author jjohnson
 * @version 4.0
 */

public class DataFunctions {

//	public static ArrayList convertALFieldToInteger(ArrayList list, int i) {
//		if (list == null) return null;
//		Iterator iter = list.iterator();
//		while (iter.hasNext()) {
//			Object item = iter.next();
//			if (item instanceof ArrayList) {
//				ArrayList temp = (ArrayList)item;
//				// Convert field i to Integer
//				temp.set(i,convertToInteger(temp.get(i)));
//			} else if (item instanceof HashMap) {
//				System.out.println("ERROR: Calling convertALFieldToInteger with ArrayList of HashMaps");
//			}
//		}
//		return list;
//	}
//
//	public static ArrayList convertALFieldToInteger(ArrayList list, String fieldName) {
//		if (list == null) return null;
//		Iterator iter = list.iterator();
//		while (iter.hasNext()) {
//			Object item = iter.next();
//			if (item instanceof ArrayList) {
//				System.out.println("ERROR: Calling convertALFieldToInteger with ArrayList of ArrayLists");
//			} else if (item instanceof HashMap) {
//				HashMap temp = (HashMap)item;
//				fieldName = fieldName.toUpperCase();
//				temp.put(fieldName,convertToInteger(temp.get(fieldName)));
//			}
//		}
//		return list;
//	}
//
//	public static ArrayList convertALFieldToBoolean(ArrayList list, int i) {
//		if (list == null) return null;
//		Iterator iter = list.iterator();
//		while (iter.hasNext()) {
//			Object item = iter.next();
//			if (item instanceof ArrayList) {
//				ArrayList temp = (ArrayList)item;
//				// Convert field i to Boolean
//				temp.set(i,convertToBoolean(temp.get(i)));
//			} else if (item instanceof HashMap) {
//				System.out.println("ERROR: Calling convertALFieldToBoolean with ArrayList of HashMaps");
//			}
//		}
//		return list;
//	}
//
//	public static ArrayList convertALFieldToBoolean(ArrayList list, String fieldName) {
//		if (list == null) return null;
//		Iterator iter = list.iterator();
//		while (iter.hasNext()) {
//			Object item = iter.next();
//			if (item instanceof ArrayList) {
//				System.out.println("ERROR: Calling convertALFieldToBoolean with ArrayList of ArrayLists");
//			} else if (item instanceof HashMap) {
//				HashMap temp = (HashMap)item;
//				fieldName = fieldName.toUpperCase();
//				temp.put(fieldName,convertToBoolean(temp.get(fieldName)));
//			}
//		}
//		return list;
//	}

	public static String arrayListToString(ArrayList list) {
		Iterator iter = list.iterator();
		StringBuffer b = new StringBuffer();
		while (iter.hasNext()) {
			Object item = iter.next();
			b.append(item + ",");
		}
		return b.length() > 0 ? b.substring(0, b.length()-1) : b.toString();
	}

	public static ArrayList convertToArrayList(Vector v) {
		if (v == null) return null;
		ArrayList a = new ArrayList(v.size());
		Iterator iter = v.iterator();
		while (iter.hasNext()) {
			Object item = iter.next();
			if (item instanceof Vector) {
				item = convertToArrayList((Vector)item);
			}
			a.add(item);
		}
		return a;
	}

	public static Vector convertToVector(ArrayList a) {
		if (a == null) return null;
		Vector v = new Vector(a.size());
		Iterator iter = a.iterator();
		while (iter.hasNext()) {
			Object item = iter.next();
			if (item instanceof ArrayList) {
				item = convertToVector((ArrayList)item);
			}
			v.addElement(item);
		}
		return v;
	}

	/**
	 * Returns a vector that contains the same objects as the array.
	 * @param anArray  the array to be converted
	 * @return  the new vector; if <code>anArray</code> is <code>null</code>,
	 *				returns <code>null</code>
	 */
	public static Vector convertToVector(Object[] anArray) {
		if (anArray == null)
			return null;

		Vector v = new Vector(anArray.length);
		for (int i=0; i < anArray.length; i++) {
			v.addElement(anArray[i]);
		}
		return v;
	}

	public static ArrayList convertToArrayList(Object[] anArray) {
		if (anArray == null)
			return null;

		ArrayList a = new ArrayList(anArray.length);
		for (int i=0; i < anArray.length; i++) {
			a.add(anArray[i]);
		}
		return a;
	}

	/**
	 * Returns a vector of vectors that contains the same objects as the array.
	 * @param anArray  the double array to be converted
	 * @return the new vector of vectors; if <code>anArray</code> is
	 *				<code>null</code>, returns <code>null</code>
	 */
	public static Vector convertToVector(Object[][] anArray) {
		if (anArray == null)
			return null;

		Vector v = new Vector(anArray.length);
		for (int i=0; i < anArray.length; i++) {
			v.addElement(convertToVector(anArray[i]));
		}
		return v;
	}

	public static ArrayList convertToArrayList(Object[][] anArray) {
		if (anArray == null)
			return null;

		ArrayList a = new ArrayList(anArray.length);
		for (int i=0; i < anArray.length; i++) {
			a.add(convertToArrayList(anArray[i]));
		}
		return a;
	}

	public static Object[] convertToArray(ArrayList a) {
		Object[] ret = new Object[a.size()];
		for (int i = 0; i < a.size(); i++) {
			ret[i] = a.get(i);
		}
		return ret;
	}

	public static Object[][] convertTo2DArray(ArrayList a) {
		if (a == null || a.size() <= 0 || !(a.get(0) instanceof ArrayList)) {
			throw new IllegalArgumentException("ArrayList must be an ArrayList of equal sized ArrayList.");
		}
		Object[][] ret = new Object[a.size()][];
		for (int i = 0; i < a.size() - 1; i++) {
			ret[i] = convertToArray((ArrayList)a.get(i));
		}
		return ret;
	}

	public static double round(double x, int decimals) {  // rounds to the nearest 'decimals' places
		int factor = 1;
		for (int i = 0; i < Math.abs(decimals); i++) factor *= 10;
		if (decimals < 0) return factor * Math.rint(x / factor);
		else return Math.rint(factor * x) / factor;
	}
	public static double floor(double x, int decimals) {  // always rounds down
		int factor = 1;
		for (int i = 0; i < Math.abs(decimals); i++) factor *= 10;
		if (decimals < 0) return factor * Math.floor(x / factor);
		else return Math.floor(factor * x) / factor;
	}

	public static double ceil(double x, int decimals) {  // always rounds up
		int factor = 1;
		for (int i = 0; i < Math.abs(decimals); i++) factor *= 10;
		if (decimals < 0) return factor * Math.ceil(x / factor);
		else return Math.ceil(factor * x) / factor;
	}
	public static Boolean convertToBoolean(Object o) {
		Boolean ret;
		if ((o.toString().compareToIgnoreCase("true") == 0) || (o.toString().compareToIgnoreCase("false") == 0)) {
			ret = new Boolean (Boolean.valueOf(o.toString()).booleanValue());
		} else if (o instanceof Double) {
			ret = new Boolean (Double.valueOf(o.toString()).intValue() == 1);
		} else if (o instanceof Integer) {
			ret = new Boolean (Integer.valueOf(o.toString()).intValue() == 1);
		} else {
			ret = new Boolean (Integer.valueOf(o.toString()).intValue() == 1);
		}
		//if (QCApplet.DEBUG_MODE)
			System.out.println("convertToBoolean(" + o.toString() + ") = " + ret.toString());
		return ret;
	}
	public static Integer convertToInteger(Object o) {
		Integer ret;
		if (o instanceof Double) {
			ret = new Integer (Double.valueOf(o.toString()).intValue());
		} else {
			ret = new Integer (Integer.valueOf(o.toString()).intValue());
			//return o.toString().;
		}
		//if (QCApplet.DEBUG_MODE)
			System.out.println("convertToInteger(" + o.toString() + ") = " + ret.toString());
		return ret;
	}
}