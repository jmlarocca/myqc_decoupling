package com.mmhayes.myqc.server.utils;

//mmhayes dependencies

import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.CommonAuthResource;
import com.mmhayes.common.api.Validation;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.DynamicSQL;
import com.mmhayes.common.dataaccess.commonMMHFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.myqc.server.collections.TOSCollection;
import com.mmhayes.myqc.server.models.TOSModel;
import jcifs.UniAddress;
import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbAuthException;
import jcifs.smb.SmbException;
import jcifs.smb.SmbSession;

import javax.naming.AuthenticationException;
import javax.naming.CommunicationException;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.Duration;
import java.time.Instant;
import java.util.*;

//my qc dependencies
//other dependencies

/*
 Last Updated (automatically updated by SVN)
 $Author: zdhirschman $: Author of last commit
 $Date: 2020-01-14 12:20:03 -0500 (Tue, 14 Jan 2020) $: Date of last commit
 $Rev: 45341 $: Revision of last commit

 Notes: Secure Login class used by QC application instances
*/

public class SecureLogin {

    private static DataManager dm = new DataManager(); //the usual data manager
    private static HashMap<String, HashMap<String, Object>> loginAttempts = new HashMap<>();
    private static ArrayList globalApplicationSettings = dm.resultSetToHashMap(dm.runSql("SELECT * FROM QC_GLOBALS"),true);
    private static final Integer AUTHENTICATE_PROPRIETARY = 1;
    private static final Integer AUTHENTICATE_NT = 2;
    private static final Integer AUTHENTICATE_LDAP = 3;
    private static final String AUTHSUCCESSCODE = "1";
    private static final String LOCKED = "LOCKED";
    private static final String ACTIVE = "ACTIVE";
    private static final String USERID = "USERID";
    private static final String GATEWAYUSERID = "GATEWAYUSERID";
    private static final String INSTANCEUSERID = "INSTANCEUSERID";
    private static final String DIRECTORYUSERNAME = "DIRECTORYUSERNAME";
    private static final String USERHASHEDPASSWORD = "USERPASSWORD";
    private static final String AUTHENTICATIONTYPEID = "AUTHTYPEID";
    private static final String AUTHENTICATIONURL = "AUTHENTICATIONURL";
    private static final String FORCEPASSWORDCHANGE = "FORCEPASSWORDCHANGE";
    private static final String FAILEDLOGINATTEMPTS = "FAILEDLOGINATTEMPTS";
    private static final String DEFAULTLANGUAGEID = "DEFAULTLANGUAGEID";
    private static final String INSTANCEID = "INSTANCEID";
    private static final String LOCATION = "LOCATION";

    //handles all steps of the login process for users logging into an instance (ground)
    public static Validation userLogin(Validation requestValidation, HttpServletRequest request) {
        try {
            //initialize for login process
            HttpSession session = request.getSession();
            requestValidation.setDetails("");
            requestValidation.setErrorDetails("");
            requestValidation.setIsValid(false);

            //check IP blacklist to determine if this IP of this request is allowed to attempt to login
            if (!CommonAPI.checkIPBlackList(CommonAPI.getEndUserIPAddress(requestValidation, request))) {
                Logger.logMessage("Prevented banned IP address from attempting login. IPAddress: " + CommonAPI.getEndUserIPAddress(requestValidation, request), Logger.LEVEL.WARNING);
                requestValidation.reset();
                requestValidation.setDetails("Please contact MMHayes support");
                requestValidation.setFailureCode(Validation.FAILURECODE.BLOCKEDIP.getCodeInt());
                return requestValidation;
            }

            //see if this session has any previous login attempts
            HashMap previousAttempt = loginAttempts.get(session.getId());

            //get user information
            HashMap userHM = getUserInfo(requestValidation);

            //see if this user actually exists
            if (userHM != null) {

                Boolean lockedFlag = Boolean.valueOf(userHM.get("LOCKED").toString());
                Boolean activeFlag = Boolean.valueOf(userHM.get("ACTIVE").toString());
                Integer userID = Integer.parseInt(userHM.get("USERID").toString());
                String hashedPass = userHM.get("USERHASHEDPASSWORD").toString();
                Integer authTypeID = Integer.parseInt(userHM.get("AUTHENTICATIONTYPEID").toString());
                Boolean forcePasswordChange = Boolean.valueOf(userHM.get("USERACCTFORCEPASSWORDCHANGE").toString());
                Integer accountTypeID = Integer.parseInt(userHM.get("ACCOUNTTYPEID").toString());
                if (forcePasswordChange) {
                    requestValidation.setForcePassword(true);
                }

                Boolean giftCardAccount = false;
                if(accountTypeID == 4) {  //cannot allow gift card accounts to log in
                    giftCardAccount = true;
                }

                // Add authTypeID to requestValidation for later use
                HashMap<String, Integer> map = new HashMap<>();
                map.put("AuthType", authTypeID);
                ArrayList<HashMap> data = new ArrayList<>();
                data.add(map);
                requestValidation.setData(data);

                //set the instanceUserID
                requestValidation.setInstanceUserID(userID);

                //check to see if this account has been locked out
                if (lockedFlag) {
                    Logger.logMessage("Attempt to login into locked account - userID: "+userID, Logger.LEVEL.WARNING);
                    requestValidation.setFailureCode(Validation.FAILURECODE.LOCKED.getCodeInt());
                    requestValidation = loginFailed(requestValidation, true, request);
                    requestValidation.setDetails("Your account has been locked.");
                    return requestValidation;
                }

                //check to see if this account has is inactive
                if (!activeFlag) {
                    Logger.logMessage("Attempt to login into an inactive account - userID: "+userID, Logger.LEVEL.WARNING);
                    requestValidation.setFailureCode(Validation.FAILURECODE.INACTIVE.getCodeInt());
                    requestValidation = loginFailed(requestValidation, true, request);
                    return requestValidation;
                }

                //password handling
                Boolean isPassVerified;

                //determine if the password provided is correct (either QC or Single Sign Auth)
                if (authTypeID.equals(AUTHENTICATE_PROPRIETARY)) {
                    isPassVerified = verifyPassword(requestValidation.getLoginPassword(), hashedPass);
                } else {
                    isPassVerified = verifySingleSignOn(userHM.get("LOGINNAME").toString(), requestValidation.getLoginPassword(), authTypeID);
                }

                //is the password provided correct?
                if (isPassVerified) {
                    //sets trust for this session and validation object, records successful login, reset failed login attempts
                    requestValidation = loginSuccess(requestValidation, request);

                    if(giftCardAccount) {
                        requestValidation.setDetails("gift-card");
                    }
                } else {
                    //bad LoginPassword, so fail the request
                    requestValidation.setFailureCode(Validation.FAILURECODE.PASSWORD.getCodeInt());
                    requestValidation = loginFailed(requestValidation, true, request);
                }
            } else {
                //bad LoginName, so fail the request
                requestValidation.setFailureCode(Validation.FAILURECODE.USERNAME.getCodeInt());
                requestValidation = loginFailed(requestValidation, false, request);
            }
        } catch (Exception ex) {
            requestValidation.handleGenericException(ex, "SecureLogin.userLogin");
        } finally {
            // Clear the plain text password
            requestValidation.setLoginPassword("");
        }
        return requestValidation;
    }

    //sets trust for this session and validation object, records successful login, reset failed login attempts
    private static Validation loginSuccess(Validation requestValidation, HttpServletRequest request) {
        try {
            //set default success details for the requestValidation
            requestValidation.setIsValid(true);
            requestValidation.setDetails("success");

            //store instanceUserID
            Integer instanceUserID = requestValidation.getInstanceUserID();

            //get request information
            String userRequestIP = CommonAPI.getEndUserIPAddress(requestValidation, request);
            HttpSession session = request.getSession();

            //remove loginAttempts information from hash map for this session id
            if (loginAttempts.get(session.getId()) != null) {
                loginAttempts.remove(session.getId());
            }

            //insert a new history record
            String historySQL=dm.getSQLProperty("data.validation.recordAttempt", new Object[]{});
            ArrayList<String> historyValues = new ArrayList<>();
            historyValues.add(0, AUTHSUCCESSCODE);
            historyValues.add(1, requestValidation.getLoginName());

            //determine employee id / user id
            Boolean MyQCUser = (instanceUserID < 0);
            if (MyQCUser) {
                Integer employeeID = instanceUserID*-1;
                historyValues.add(2, employeeID.toString());
                historyValues.add(3, null);
            } else {
                Integer quickChargeUserID = instanceUserID;
                historyValues.add(2, null);
                historyValues.add(3, quickChargeUserID.toString());
            }

            historyValues.add(4, CommonAPI.getEndUserIPAddress(requestValidation, request));
            historyValues.add(5, CommonAPI.getEndUserUserAgent(requestValidation, request));
            historyValues.add(6, null); //not an invite
            dm.insertPreparedStatement(historySQL, historyValues);

            //reset failed login attempts
            String resetFailedLoginSqlProp;
            String failedLoginSQLUserID;
            if (MyQCUser) {
                resetFailedLoginSqlProp = "data.validation.updateEmployeeFailedLoginAttempt";
                //the instance user id in this case is negative, so we have to make it positive for SQL
                Integer positiveInstanceUserID = instanceUserID*-1;
                failedLoginSQLUserID = positiveInstanceUserID.toString();
            } else {
                resetFailedLoginSqlProp = "data.validation.updateManagerFailedLoginAttempt";
                failedLoginSQLUserID = instanceUserID.toString();
            }
            String resetFailedLoginSQL=dm.getSQLProperty(resetFailedLoginSqlProp, new Object[]{});
            ArrayList<String> resetFailedLoginValues = new ArrayList<>();
            resetFailedLoginValues.add(0, "0"); //set back to zero
            resetFailedLoginValues.add(1, failedLoginSQLUserID);
            dm.setAndUpdatePreparedStatement(resetFailedLoginSQL, resetFailedLoginValues);

            //create DSKey for now verified user
            String sessionIndex = "";
            if(session.getAttribute("QC_SESSIONINDEX")!= null){
                sessionIndex = session.getAttribute("QC_SESSIONINDEX").toString();
            }
            if(!sessionIndex.isEmpty()){
                requestValidation.createDSKey(sessionIndex);
            } else {
                requestValidation.createDSKey();
            }

            //if prepare the instanceUserIDForTOS
            Integer instanceUserIDForTOS = null;
            if (instanceUserID < 0) { //if negative make positive
                instanceUserIDForTOS = instanceUserID*-1;
            }

            //set TOS for now verified user
            TOSCollection tosCollection = new TOSCollection(request, instanceUserIDForTOS);
            requestValidation.setTOS(tosCollection.getCollection());

            if (requestValidation.getIsValid()) {
                Logger.logMessage("Successfully logged in " + requestValidation.getLoginName() + ". UserID: "+instanceUserID.toString(), Logger.LEVEL.IMPORTANT);
            }
        } catch (Exception ex) {
            requestValidation.handleGenericException(ex, "SecureLogin.loginSuccess");
        }
        return requestValidation;
    }

    //handles timeouts, lockouts and IP address blacklisting when a user fails to login
    private static Validation loginFailed(Validation requestValidation, Boolean validUser, HttpServletRequest request) {
        try {
            // Set login request flag to false indicating a failed attempt
            requestValidation.setIsValid(false);

            // Set the reason code for the failure
            int failureReasonCode = (requestValidation.fetchFailureCode() != null ? requestValidation.fetchFailureCode() : 2);

            // Set the details displayed to the user for the failure code
            switch (failureReasonCode) {
                case 2:
                    requestValidation.setDetails("Login Error");
                    break;
                case 5:
                    requestValidation.setDetails("Account Locked");
                    break;
                case 6:
                    requestValidation.setDetails("Contact Support Regarding Your Account");
                    break;
                case 7:
                    requestValidation.setDetails("Account Inactive");
                    break;
                case 3:
                case 4:
                case 10:
                default:
                    requestValidation.setDetails("Login Failure");
                    break;
            }

            // Log the failure
            Logger.logMessage("Login failed for \"" + requestValidation.getLoginName() +
                    "\". IP Address: " + CommonAPI.getEndUserIPAddress(requestValidation, request) +
                    " Reason Code: " + Integer.toString(failureReasonCode) + "-" + requestValidation.getDetails(),
                    Logger.LEVEL.WARNING);

            // DETERMINE USER AUTH TYPE
            boolean isQuickChargeAuthType = true;
            if(requestValidation.getData() != null && !requestValidation.getData().isEmpty()){
                ArrayList<HashMap> data = requestValidation.getData();
                for(HashMap hm : data){
                    if(hm.containsKey("AuthType")){
                        isQuickChargeAuthType = (Integer.parseInt(hm.get("AuthType").toString()) == 1);
                        break;
                    }
                }
            }

            // Insert a new history record
            ArrayList<String> historyValues = new ArrayList<>();
            historyValues.add(0, Integer.toString(failureReasonCode));
            historyValues.add(1, requestValidation.getLoginName());

            //determine employee id / user id
            Boolean MyQCUser = false;
            if (validUser) {
                MyQCUser = (requestValidation.getInstanceUserID() < 0);
                if (MyQCUser) {
                    Integer employeeID = requestValidation.getInstanceUserID()*-1;
                    historyValues.add(2, employeeID.toString());
                    historyValues.add(3, null);
                } else {
                    Integer quickChargeUserID = requestValidation.getInstanceUserID();
                    historyValues.add(2, null);
                    historyValues.add(3, quickChargeUserID.toString());
                }
            } else {
                historyValues.add(2, null);
                historyValues.add(3, null);
            }

            historyValues.add(4, CommonAPI.getEndUserIPAddress(requestValidation, request));
            historyValues.add(5, CommonAPI.getEndUserUserAgent(requestValidation, request));
            historyValues.add(6, null); //not an invite
            // ONLY FOR QuickCharge AuthType - See B-06042
            if(isQuickChargeAuthType){
                dm.insertPreparedStatement(dm.getSQLProperty("data.validation.recordAttempt", new Object[] {}), historyValues);
            }

            //START - Timeout Handling
            // ONLY FOR QuickCharge AuthType - See B-06042
            if(isQuickChargeAuthType){
                //create a new loginAttempt for THIS failed login attempt (aka the currentAttempt)
                HashMap<String, Object> currentAttempt = new HashMap<>();
                currentAttempt.put("lastFailedTime", new Date());

                //see if this user's session has a had a previous login attempt
                HttpSession session = request.getSession();
                HashMap previousAttempt = loginAttempts.get(session.getId());

                //init timeout checking
                Integer previousTimeOut = 1; //default to one second
                Integer failedAttempts = 0; //default to zero attempts
                Integer allowedAttemptsBeforeTimeout = CommonAPI.getFailedLoginTimeoutLimit(); //allowed attempts
                Integer timeoutSeconds = CommonAPI.getFailedLoginTimeoutSeconds();

                if (previousAttempt != null) { //this session HAS tried before

                    //get the total failed login attempts from the previous attempt
                    failedAttempts = (Integer) previousAttempt.get("failedAttempts");

                    //increment the failed attempts
                    failedAttempts++;

                    //if this session has failed to login one too many times get the current time out they are facing
                    if (failedAttempts >= allowedAttemptsBeforeTimeout && allowedAttemptsBeforeTimeout != 0 && timeoutSeconds != 0) {
                        previousTimeOut = (Integer) previousAttempt.get("currentTimeOut");
                    }
                } else { //this session HAS NOT tried before
                    failedAttempts = 1;
                }

                //determine what the timeout should be for this session
                int currentTimeout = timeoutSeconds * previousTimeOut;

                //update the currentAttempt and add to the static loginAttempts hashmap
                currentAttempt.put("currentTimeOut", currentTimeout);
                currentAttempt.put("failedAttempts", failedAttempts);
                currentAttempt.put("createdTime", Instant.now());

                //purge loginAttempts
                purgeLoginAttempts(requestValidation, CommonAPI.getSessionTimeoutMinutes(), request);

                //put the currentAttempt into loginAttempts
                loginAttempts.put(session.getId(), currentAttempt);
            }
            //END - Timeout Handling

            //START - Lockout Handling

            //ensures the login name attempting to login is actually a valid user as we can't lock out users that don't exist
            // ONLY FOR QuickCharge AuthType - See B-06042
            if (validUser && isQuickChargeAuthType) {

                String failedLoginSqlProp;
                if (MyQCUser) { failedLoginSqlProp = "data.validation.getFailedEmployeeAttempts"; } else { failedLoginSqlProp = "data.validation.getFailedManagerAttempts"; }

                //this overloaded dm.serializeWithColNames utilizes parameterized queries
                ArrayList failedLoginAttemptsList  = dm.serializeSqlWithColNames(failedLoginSqlProp,
                        new Object[]{
                                requestValidation.getLoginName()
                        },
                        false,
                        true
                );

                if (failedLoginAttemptsList != null && failedLoginAttemptsList.size() == 1) {

                    HashMap failedLoginAttemptsHM = (HashMap)failedLoginAttemptsList.get(0);
                    Integer failedLoginAttempts = Integer.parseInt(failedLoginAttemptsHM.get("FAILEDLOGINATTEMPTS").toString());
                    Integer userID = Integer.parseInt(failedLoginAttemptsHM.get("USERID").toString());
                    String failedLoginAttemptSqlProp;
                    if (MyQCUser) { failedLoginAttemptSqlProp = "data.validation.updateEmployeeFailedLoginAttempt"; } else { failedLoginAttemptSqlProp = "data.validation.updateManagerFailedLoginAttempt"; }

                    //update failed login attempts in DB
                    failedLoginAttempts++; //increment the total failed login attempts for this user
                    String failedLoginSQL=dm.getSQLProperty(failedLoginAttemptSqlProp, new Object[]{});
                    ArrayList<String> failedLoginValues = new ArrayList<>();
                    failedLoginValues.add(0, failedLoginAttempts.toString());
                    failedLoginValues.add(1, userID.toString());
                    dm.setAndUpdatePreparedStatement(failedLoginSQL, failedLoginValues);

                    //get how many tries any one username can fail before they become locked
                    Integer failedLoginLimit = CommonAPI.getFailedLoginLimit();

                    //lock this user's account if they have failed one too many times..
                    if ((failedLoginLimit != 0) && (failedLoginAttempts >= failedLoginLimit)) {
                        String lockSQLProp;
                        if (MyQCUser) { lockSQLProp = "data.validation.lockEmployeeAccount"; } else { lockSQLProp = "data.validation.lockManagerAccount"; }
                        //lock the user's account in DB
                        String lockSQL=dm.getSQLProperty(lockSQLProp, new Object[]{});
                        ArrayList<String> lockAccountValues = new ArrayList<>();
                        lockAccountValues.add(0, userID.toString());
                        dm.setAndUpdatePreparedStatement(lockSQL, lockAccountValues);
                        Logger.logMessage("Locked account with userID of "+userID, Logger.LEVEL.WARNING);
                    }
                } else {
                    Logger.logMessage("While attempting to check failed login attempts - Could not find a LoginName "+ requestValidation.getLoginName(), Logger.LEVEL.WARNING);
                }
            }
            //END - Lockout Handling

            //START - IP Address Blacklisting

            //checks the user's IP to see if it should be black listed
            // ONLY FOR QuickCharge AuthType - See B-06042
            if(isQuickChargeAuthType){
                requestValidation = CommonAPI.checkIPAddress(requestValidation, CommonAPI.getEndUserIPAddress(requestValidation, request));
            }

            //END - IP Address Blacklisting

        } catch (Exception ex) {
            requestValidation.handleGenericException(ex, "SecureLogin.loginFailed");
        } finally {
            requestValidation.setFailureCode(0);
        }
        return requestValidation;
    }

    //gets QC user (employee ONLY) information
    private static HashMap getUserInfo(Validation requestValidation) {
        return getUserInfo(requestValidation, false);
    }

    //gets QC user (employee ONLY) information
    private static HashMap getUserInfo(Validation requestValidation, Boolean checkForSSOAuthType) {
        HashMap userHM = null;

        try {

            String loginName = requestValidation.getLoginName();

            //choose lookup SQL string based on checkForSSOAuthType
            String employeeLookUpSQLPropName;
            if (checkForSSOAuthType) {
                employeeLookUpSQLPropName = "data.validation.getEmployeeInfoSSOAuthType";
            } else {
                employeeLookUpSQLPropName = "data.validation.getEmployeeInfo";
            }

            ArrayList employeeList = getUserInfoWithDynamicColumn(employeeLookUpSQLPropName,false,loginName, checkForSSOAuthType);

            //see if this user is an employee
            if (employeeList != null && employeeList.size() == 1) {
                userHM = (HashMap)employeeList.get(0);
                userHM.put("USERID", Integer.parseInt(userHM.get("EMPLOYEEID").toString())*-1); //negative employee ID
                userHM.put("USERHASHEDPASSWORD", userHM.get("USERACCTPASS").toString());

                //if the accounts status is "I" (inactive) then the user cannot be active - added 2/23/2016 by jrmitaly
                if (userHM.get("AIP").toString().equals("I")) {
                    userHM.put("ACTIVE", 0);
                } else {
                    userHM.put("ACTIVE", userHM.get("USERACCTACTIVE").toString());
                }

                userHM.put("ISMANAGER", false);
                userHM.put("LOGINNAME", userHM.get("USERACCTLOGIN").toString());
            }

            return userHM;
        } catch (Exception ex) {
            userHM = null;
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ex.printStackTrace(pw); //allows stack trace as a string -> sw.toString()
            String errorDetails = "Error in SecureLogin.getUserInfo - Exception: " + sw.toString();
            Logger.logMessage(errorDetails, Logger.LEVEL.ERROR); //log details
            return userHM;
        }
    }

    //OVERLOAD - gets QC user (employee ONLY) information by instance user ID rather than loginName
    private static HashMap getUserInfo(Validation requestValidation, String instanceUserID) {
        HashMap userHM = null;
        try {

            Integer positiveInstanceUserID;

            //the employee ID can be stored in the validation object as negative, so we need to make it positive to run SQL against it
            if (Integer.parseInt(instanceUserID) < 0) {
                positiveInstanceUserID = Integer.parseInt(instanceUserID)*-1;
            } else {
                positiveInstanceUserID = Integer.parseInt(instanceUserID);
            }

            //this overloaded dm.serializeWithColNames utilizes parameterized queries
            ArrayList employeeList = dm.serializeSqlWithColNames("data.validation.getEmployeeInfoByID",
                    new Object[]{
                            positiveInstanceUserID.toString()
                    },
                    false,
                    true
            );

            //see if this user is an employee
            if (employeeList != null && employeeList.size() == 1) {
                userHM = (HashMap)employeeList.get(0);
                userHM.put("USERID", Integer.parseInt(userHM.get("EMPLOYEEID").toString())*-1); //negative employee ID
                userHM.put("USERHASHEDPASSWORD", userHM.get("USERACCTPASS").toString());

                //if the accounts status is "I" (inactive) then the user cannot be active - added 2/23/2016 by jrmitaly
                if (userHM.get("AIP").toString().equals("I")) {
                    userHM.put("ACTIVE", 0);
                } else {
                    userHM.put("ACTIVE", userHM.get("USERACCTACTIVE").toString());
                }

                userHM.put("ISMANAGER", false);
                userHM.put("LOGINNAME", userHM.get("USERACCTLOGIN").toString());
            }

            return userHM;
        } catch (Exception ex) {
            userHM = null;
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ex.printStackTrace(pw); //allows stack trace as a string -> sw.toString()
            String errorDetails = "Error in SecureLogin.getUserInfo - Exception: " + sw.toString();
            Logger.logMessage(errorDetails, Logger.LEVEL.ERROR); //log details
            return userHM;
        }
    }

    //handles proprietary authentication verification
    private static Boolean verifyPassword(String plainTextPass, String hashedPass) {
        try {
            Logger.logMessage("Verifying password ...", Logger.LEVEL.DEBUG);
            return commonMMHFunctions.verifyPassword(plainTextPass, hashedPass);
        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ex.printStackTrace(pw); //allows stack trace as a string -> sw.toString()
            String errorDetails = "Error in SecureLogin.verifyPassword - Exception: " + sw.toString();
            Logger.logMessage(errorDetails, Logger.LEVEL.ERROR); //log details
            return false;
        }
    }

    //handles LDAP and NT authentication verification
    private static Boolean verifySingleSignOn(String username, String password, int authTypeId) {
        Logger.logMessage("verifySingleSignOn called with username: " + username, Logger.LEVEL.DEBUG);

        if (globalApplicationSettings != null && globalApplicationSettings.size() == 1) {
            HashMap globalApplicationSettingsHM = (HashMap)globalApplicationSettings.get(0);

            if (password.trim().length() == 0) {
                return false;
            }
            String domain;
            if (username.indexOf("\\") > 0) {
                domain = username.substring(0,username.indexOf("\\"));
                username = username.substring(domain.length() + 1);
            } else {
                domain = globalApplicationSettingsHM.get("AUTHENTICATIONURL").toString();
                if (domain.indexOf("//") >= 0) {
                    domain = domain.substring(domain.indexOf("//") + 2);
                }
            }
            String controller = globalApplicationSettingsHM.get("AUTHENTICATIONURL").toString();
            boolean UseLDAPs = controller.toUpperCase().indexOf("LDAPS://") == 0;
            if (controller.indexOf("//") >= 0) {
                controller = controller.substring(controller.indexOf("//") + 2);
            }
            Logger.logMessage("Controller: " + controller + "; Domain: " + domain + "; User: " + username, Logger.LEVEL.DEBUG);
            if (authTypeId == AUTHENTICATE_LDAP) {
                try {
                    // Set up the environment for creating the initial context
                    Hashtable env = new Hashtable();
                    env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
                    env.put(Context.PROVIDER_URL, "ldap://" + controller); // "ldap://mmhayes.com");

                    // Specify SSL
                    if (UseLDAPs) {
                        env.put(Context.SECURITY_PROTOCOL, "ssl");
                    }

                    // Authenticate as S. User and password "mysecret"
                    env.put(Context.SECURITY_AUTHENTICATION, "simple");
                    env.put(Context.SECURITY_PRINCIPAL, domain + "\\" + username);
                    env.put(Context.SECURITY_CREDENTIALS, password);

                    // Create the initial context
                    Logger.logMessage("About to reach out to connect via LDAP with username: " + username, Logger.LEVEL.DEBUG);
                    DirContext ctx = new InitialDirContext(env);
                    return true;
                } catch (CommunicationException comEx) {
                    Logger.logMessage("Communication_exception: " + comEx.getMessage(), Logger.LEVEL.ERROR);
                    comEx.printStackTrace();
                    return false;
                } catch (AuthenticationException authEx) {
                    Logger.logMessage("User logon failed for " + username, Logger.LEVEL.ERROR);
                    //authEx.printStackTrace();
                    Logger.logMessage("Authentication_exception: " + authEx.getMessage(), Logger.LEVEL.ERROR);
                    authEx.printStackTrace();
                    return false;
                } catch (NamingException nameEx) {
                    Logger.logMessage("Naming_exception: " + nameEx.toString(), Logger.LEVEL.ERROR);
                    nameEx.printStackTrace();
                    return false;
                }
            } else if (authTypeId == AUTHENTICATE_NT) {
                try {
                    InetAddress ip = InetAddress.getByName(controller); // ip address of your windows controller
                    UniAddress myDomain = new UniAddress(ip);
                    Logger.logMessage("NT logon - IP Address for auth " + ip, Logger.LEVEL.DEBUG);
                    Logger.logMessage("NT logon - Domain for Smb Session login " + myDomain, Logger.LEVEL.DEBUG);
                    NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication(domain, username, password);
                    SmbSession.logon(myDomain, auth);
                    return true;
                } catch (SmbAuthException ex) {
                    Logger.logMessage("User logon failed for " + username + " " + ex.getMessage(), Logger.LEVEL.ERROR);
                    //ex.printStackTrace();
                    return false;
                } catch (UnknownHostException ex) {
                    Logger.logMessage("UnknownHostException: " + ex.getMessage(), Logger.LEVEL.ERROR);
                    return false;
                } catch (SmbException ex) {
                    Logger.logMessage("SmbException: " + ex.getMessage(), Logger.LEVEL.ERROR);
                    return false;
                } catch (Exception ex) {
                    Logger.logMessage("GeneralException: " + ex.getMessage(), Logger.LEVEL.ERROR);
                    return false;
                }
            } else {
                return false;
            }
        } else {
            Logger.logMessage("SecureLogin.verifySingleSignOn failed due to an invalid global application settings list.", Logger.LEVEL.ERROR);
            return false;
        }
    }

    //removes entries from the static loginAttempts hashmap that are older than maxEntryLengthMins from now
    private static Validation purgeLoginAttempts(Validation requestValidation, Integer maxEntryLengthMins, HttpServletRequest request) {
        try {
            //iterate through loginAttempts and remove old entries
            Iterator loginAttemptIterator = loginAttempts.entrySet().iterator();
            while (loginAttemptIterator.hasNext()) {

                //get the created time for each loginAttempt
                Map.Entry pairs = (Map.Entry)loginAttemptIterator.next();
                HashMap currentLoginAttempt = (HashMap) pairs.getValue();
                Instant createdTime = (Instant) currentLoginAttempt.get("createdTime");


                //add maxEntryLengthMins minutes to key to create a checkTime Instant
                Instant checkTime = createdTime.plus(Duration.ofMinutes(maxEntryLengthMins));

                //get the currentTime
                Instant currentTime = Instant.now();

                //if the current time is after the checkTime then dispose of this loginAttempt
                if (currentTime.isAfter(checkTime)) {
                    loginAttemptIterator.remove();
                    Logger.logMessage("Purged entry from loginAttempts static hashmap.", Logger.LEVEL.DEBUG);
                }
            }
        } catch (Exception ex) {
            requestValidation.handleGenericException(ex, "SecureLogin.purgeLoginAttempts");
        }
        return requestValidation;
    }

    //attempt to update ground user's password after they have been flagged for force password change
    public static Validation attemptUpdate(Validation requestValidation, HashMap clientUpdateHM, HttpServletRequest request) {
        try {
            //get client login name from login screen
            String loginName = clientUpdateHM.get("loginName").toString();

            //set the name so getUserInfo() can work
            requestValidation.setLoginName(loginName);

            //hashmap of user data
            HashMap userHM = getUserInfo(requestValidation);

            Integer userID = CommonAuthResource.checkRequestHeaderForMMHayesAuth(request);

            Boolean isManager = Boolean.valueOf( userHM.get("ISMANAGER").toString() );

            requestValidation.setInstanceUserID(userID);

            String resetPass = clientUpdateHM.get("resetPass").toString();
            String confirmResetPass = clientUpdateHM.get("confirmResetPass").toString();
            String hashedPass;

            //if the passwords are equal...
            if (resetPass.equals(confirmResetPass)) {

                //then determine if this password meets this application's password criteria
                String passStrength = CommonAPI.getPassStrength();
                Boolean passCriteriaMet;
                commonMMHFunctions commFunc = new commonMMHFunctions();
                if (passStrength.equals("loose")) {
                    passCriteriaMet = commFunc.validatePasswordLoose(resetPass);
                } else {
                    passCriteriaMet = commFunc.validatePasswordStrict(resetPass);
                }

                //if the password criteria has been met then hash the password
                if (passCriteriaMet) {
                    hashedPass = commonMMHFunctions.hashPassword(resetPass);
                } else {
                    requestValidation.reset();
                    requestValidation.setDetails("An error has occurred. Please contact MMHayes support if the error continues.");
                    Logger.logMessage("Password does not meet criteria ("+passStrength+") in ResetPassword.attemptResetSuccess", Logger.LEVEL.WARNING);
                    return requestValidation;
                }
            } else {
                requestValidation.reset();
                requestValidation.setDetails("An error has occurred. Please contact MMHayes support if the error continues.");
                Logger.logMessage("Passwords are not equal in ResetPassword.attemptResetSuccess", Logger.LEVEL.WARNING);
                return requestValidation;
            }

            if ( !hashedPass.equals("") ) {
                //sql string for updating password
                String updateUserPassSQL;
                String UserIDStr;

                //tells which DB to use, managers go in QC_QuickChargeUsers otherwise QC_Employees
                if (isManager) {
                    updateUserPassSQL=dm.getSQLProperty("data.validation.updateQCUserPass", new Object[]{});
                } else {
                    updateUserPassSQL=dm.getSQLProperty("data.validation.updateEmployeeUserPass", new Object[]{});

                    if (userID < 0) { //to deal with negative user IDs in employees table
                        userID = userID * -1;
                    }
                }

                UserIDStr = userID.toString();
                ArrayList<String> updateUserPassValues = new ArrayList<>();
                updateUserPassValues.add(0, hashedPass);
                updateUserPassValues.add(1, UserIDStr);
                dm.setAndUpdatePreparedStatement(updateUserPassSQL, updateUserPassValues);


                //record in QC_EmployeeChanges that password was updated -jrmitaly - 6/20/2016
                Integer logAccountChangeResult;
                logAccountChangeResult = dm.parameterizedExecuteNonQuery("data.myqc.recordAccountChangeAsMyQCUser",
                        new Object[]{
                            UserIDStr,
                            "User Password",
                            "********",
                            "********"
                        }
                );
                if (logAccountChangeResult == 0) {
                    Logger.logMessage("Could not LOG password change for account ID: "+UserIDStr, Logger.LEVEL.ERROR);
                } else {
                    Logger.logMessage("Successfully updated password for UserID: "+userID, Logger.LEVEL.IMPORTANT);
                    requestValidation.setForcePassword(false);
                    requestValidation.setIsValid(true);
                    requestValidation.setDetails( "Successfully updated password for " + requestValidation.getLoginName() );
                }
            }


        } catch (Exception ex) {
            requestValidation.handleGenericException(ex, "SecureLogin.attemptUpdate");
        }
        return requestValidation;
    }

    public static Validation verifyUserPasswordForReAuth(HttpServletRequest request, String plainTextPassword, Validation requestValidation) {
        boolean passwordVerified = false;
        requestValidation.setIsValid(false);

        try {
            Integer userID = CommonAuthResource.checkRequestHeaderForMMHayesAuth(request);

            //if the mmhayesAuthHeader is valid, verify and set the reauth status
            if (userID != null) {
                //this overloaded dm.serializeWithColNames utilizes parameterized queries
                ArrayList ResultList = dm.parameterizedExecuteQuery("data.validation.getEmployeeInfoByID",
                        new Object[]{
                                userID,
                        },
                        true
                );

                if (ResultList != null && ResultList.size() == 1) { //verified DSKeyReAuthStatusList
                    HashMap ResultHM = (HashMap) ResultList.get(0);

                    if ( ResultHM.containsKey("USERACCTPASS") && ResultHM.get("USERACCTPASS") != null ) {
                        passwordVerified = commonMMHFunctions.verifyPassword(plainTextPassword, ResultHM.get("USERACCTPASS").toString());
                    } else {
                        Logger.logMessage("USERACCTPASS not returned in SecureLogin.verifyUserPasswordForReAuth()", Logger.LEVEL.ERROR);
                        requestValidation.setErrorDetails("Error - Could not verify password.");
                    }
                } else {
                    Logger.logMessage("No results returned in SecureLogin.verifyUserPasswordForReAuth()", Logger.LEVEL.ERROR);
                    requestValidation.setErrorDetails("Error - Could not verify password.");
                }
            }
        } catch (Exception ex) {
            Logger.logException(ex);
            Logger.logMessage("Could not verify user's password in SecureLogin.verifyUserPasswordForReAuth()", Logger.LEVEL.ERROR);
            requestValidation.setErrorDetails("Error - Could not verify password.");
        }

        if ( passwordVerified ) {
            Logger.logMessage("User password successfully verified.", Logger.LEVEL.DEBUG);
            requestValidation.setIsValid(true);
            requestValidation.setDetails("User password successfully verified.");
        }

        return requestValidation;
    }

    //creates a validated sessions for SSO - WARNING this method should be called SAFELY as it allows logging in without a password
    public static Validation createValidatedSessionForSSONameID(String nameID, String sessionIndex, HttpServletRequest request) {
        Logger.logMessage("Attempting to createValidatedSessionForSSONameID for nameID: "+nameID, Logger.LEVEL.DEBUG);
        Validation ssoValidation = new Validation();

        //WARNING: this method should be called SAFELY as it allows logging in without a password

        try {

            //set the default requestValidation object with the nameID as the loginName
            ssoValidation.setValidationType(3); //only check "account access" (QC_Employees)
            ssoValidation.setLoginName(nameID); //the login name should directly match the IdP Provided nameID

            //initialize for login process
            //HttpSession session = request.getSession();
            ssoValidation.setIsValid(false);

            //check IP blacklist to determine if this IP of this request is allowed to attempt to login
            if (!CommonAPI.checkIPBlackList(CommonAPI.getEndUserIPAddress(ssoValidation, request))) {
                Logger.logMessage("Prevented banned IP address from attempting login. IPAddress: " + CommonAPI.getEndUserIPAddress(ssoValidation, request), Logger.LEVEL.WARNING);
                ssoValidation.reset();
                ssoValidation.setDetails("Please contact MMHayes support");
                ssoValidation.setFailureCode(Validation.FAILURECODE.BLOCKEDIP.getCodeInt());
                return ssoValidation;
            }

            HashMap userHM = getUserInfo(ssoValidation, true);

            //see if this user actually exists
            if (userHM != null) {

                Boolean lockedFlag = Boolean.valueOf(userHM.get("LOCKED").toString());
                Boolean activeFlag = Boolean.valueOf(userHM.get("ACTIVE").toString());
                Integer userID = Integer.parseInt(userHM.get("USERID").toString());
                String hashedPass = userHM.get("USERHASHEDPASSWORD").toString();
                Integer authTypeID = Integer.parseInt(userHM.get("AUTHENTICATIONTYPEID").toString());
                Boolean isManager = Boolean.valueOf( userHM.get("ISMANAGER").toString() );

                // Add authTypeID to requestValidation for later use
                HashMap<String, Integer> map = new HashMap<>();
                map.put("AuthType", authTypeID);
                ArrayList<HashMap> data = new ArrayList<>();
                data.add(map);
                ssoValidation.setData(data);

                //set the instanceUserID
                ssoValidation.setInstanceUserID(userID);

                //check to see if this account has been locked out
                if (lockedFlag) {
                    Logger.logMessage("Attempt to login into locked account - userID: "+userID, Logger.LEVEL.WARNING);
                    ssoValidation.setFailureCode(Validation.FAILURECODE.LOCKED.getCodeInt());
                    ssoValidation = loginFailed(ssoValidation, true, request);
                    ssoValidation.setDetails("Your account has been locked.");
                    return ssoValidation;
                }

                //check to see if this account has is inactive
                if (!activeFlag) {
                    Logger.logMessage("Attempt to login into an inactive account - userID: "+userID, Logger.LEVEL.WARNING);
                    ssoValidation.setFailureCode(Validation.FAILURECODE.INACTIVE.getCodeInt());
                    ssoValidation = loginFailed(ssoValidation, true, request);
                    return ssoValidation;
                }

                //sets trust for this session and validation object, records successful login, reset failed login attempts
                request.getSession().setAttribute("QC_SESSIONINDEX", sessionIndex);
                ssoValidation = loginSuccess(ssoValidation, request);

                //remove TOS HTML content if exists - too much to pass in a URL
                List tosList = ssoValidation.getTOS();
                if (tosList != null) {
                    for (Object tosModel : tosList) { //iterate through all TOS Models in the TOS List...
                        if (tosModel instanceof TOSModel) { //ensure this object really is a TOSModel before attempting to do anything
                            Logger.logMessage("In createValidatedSessionForSSONameID... removing TOS Contents from TOS ID#: "+((TOSModel) tosModel).getID());

                            //remove the TOS content as it's too much to pass in the URL in the redirect in the SSOAuthHandler
                            ((TOSModel) tosModel).setContent("");
                        }
                    }
                }

            } else {
                //bad LoginName, so fail the request
                ssoValidation.setFailureCode(Validation.FAILURECODE.USERNAME.getCodeInt());
                ssoValidation = loginFailed(ssoValidation, false, request);
                Logger.logMessage("While attempting to handle SSO... Could not find a valid, SSO Authentication type, QC account (employee) for credential: "+ssoValidation.getLoginName(), Logger.LEVEL.ERROR);
            }

        } catch (Exception ex) {
            Logger.logException(ex);
            throw ex;
        }
        return ssoValidation;
    }

    private static ArrayList getUserInfoWithDynamicColumn(String getSql, boolean isManagerFlag, String loginName, boolean checkForSSOAuthType){
        DynamicSQL dynamicSQL = new DynamicSQL(getSql).addValue(1,loginName).addValue(2,"%\\"+loginName);
        String ssoLoginNameTypeID = "1"; // Default to finding the LoginID or the UserAcctLogin
        if(checkForSSOAuthType){
            try{
                ssoLoginNameTypeID = new DynamicSQL("data.validation.getGlobalSSOLoginNameTypeID").serialize(dm).get(0).get("SSOLOGINNAMETYPEID").toString();
            }
            catch(Exception e){
                Logger.logException(e);
            }
        }
        Logger.logMessage("Checking User Info... ssoLoginNameTypeID: "+ ssoLoginNameTypeID, Logger.LEVEL.DEBUG);

        switch(ssoLoginNameTypeID){
            case "2":
                String validationColumn = (isManagerFlag? "ACCOUNTNUMBER" : "EMPLOYEENUMBER");
                dynamicSQL.addTableORColumnName(3,validationColumn);
                break;
            case "3":
                dynamicSQL.addTableORColumnName(3,"EMAILADDRESS1");
                break;
            default:
                String validationUserNameColumn = (isManagerFlag? "LOGINID" : "USERACCTLOGIN");
                dynamicSQL.addTableORColumnName(3,validationUserNameColumn);
                break;
        }

        return dynamicSQL.serialize(dm);

    }

}
