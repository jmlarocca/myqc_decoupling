package com.mmhayes.myqc.server.utils;

import java.io.PrintStream;
import java.io.PrintWriter;


/**
 * <p>Title: QuickCharge 4</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: MMHayes</p>
 * @author jjohnson
 * @version 4.0
 */

public class QCException extends Exception {
	private Exception inner;
	private String message;

	public QCException(Exception innerException) {
		inner = innerException;
	}
	public QCException(Exception innerException, String message) {
		inner = innerException;
		this.message = message;
	}
	public String getLocalizedMessage() {
		return inner.getLocalizedMessage();
	}
	public void printStackTrace(PrintWriter parm1) {
		inner.printStackTrace(parm1);
	}
	public Throwable fillInStackTrace() {
		return inner.fillInStackTrace();
	}
	public String getMessage() {
		if (message != null) return message;
		return inner.getMessage();
	}
	public void printStackTrace() {
		inner.printStackTrace();
	}
	public void printStackTrace(PrintStream parm1) {
		inner.printStackTrace(parm1);
	}
	public String toString() {
		if (message != null) return message + " - " + inner.toString();
		return inner.toString();
	}

}