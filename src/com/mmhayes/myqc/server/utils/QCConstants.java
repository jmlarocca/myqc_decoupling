package com.mmhayes.myqc.server.utils;

/**
 * <p>Title: QuickCharge 4</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: MMHayes</p>
 * @author jjohnson
 * @version 4.0
 */

public final class QCConstants {

  // PA ITEM TYPES (from QC_PAItemType)
  public static final int PAITEMTYPE_KEYPAD = 1;
  public static final int PAITEMTYPE_PLU = 2;
  public static final int PAITEMTYPE_TENDER = 3;
  public static final int PAITEMTYPE_SUBDEPARTMENT = 4;
  public static final int PAITEMTYPE_DEPARTMENT = 5;
  public static final int PAITEMTYPE_TAX = 6;
  public static final int PAITEMTYPE_PADISCOUNTS = 7;
  public static final int PAITEMTYPE_PAFUNCTIONS = 8;
  public static final int PAITEMTYPE_PAPRICELEVELS = 9;
  public static final int PAITEMTYPE_PAPAIDOUT = 10;
  public static final int PAITEMTYPE_RECEIVEDACCOUNT = 11;
  public static final int PAITEMTYPE_TAXDELETE = 12;
  public static final int PAITEMTYPE_TARES = 13;
  public static final int PAITEMTYPE_ITSTRING = 14;
  public static final int PAITEMTYPE_ITFORM = 15;
  public static final int PAITEMTYPE_ITBADGE = 16;
  public static final int PAITEMTYPE_ITDATE = 17;
  public static final int PAITEMTYPE_ITDURATION = 18;
  public static final int PAITEMTYPE_ITFLOAT = 19;
  public static final int PAITEMTYPE_ITHIDDEN = 20;
  public static final int PAITEMTYPE_ITINTEGER = 21;
  public static final int PAITEMTYPE_ITLIST = 22;
  public static final int PAITEMTYPE_ITTIME = 23;
  public static final int PAITEMTYPE_ROTATION = 24;

  // Terminal Types
  public static final int TERMINALTYPE_ADVANCED_PAYMENTS_TERMINAL = 1;
  public static final int TERMINALTYPE_KRONOS_TERMINAL = 2;
  public static final int TERMINALTYPE_PC_ENTRY_TERMINAL = 3;
  public static final int TERMINALTYPE_CASH_REGISTER_TERMINAL = 4;
  public static final int TERMINALTYPE_POS_ANYWHERE_TERMINAL = 5;

  // Terminal Models
  public static final int TERMINALMODEL_365Retail = 26;
  public static final int TERMINALMODEL_TamRetail = 27;
  public static final int TERMINALMODEL_Halifax = 0;
  public static final int TERMINALMODEL_USATech = 31;

  public QCConstants()
  {
  }

}