package com.mmhayes.myqc.server.api;

//mmhayes dependencies
import com.mmhayes.common.api.*;
import com.mmhayes.common.dataaccess.*;
import com.mmhayes.common.utils.*;

//javax.ws.rs dependencies
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

//other dependencies
import java.util.*;
import org.reflections.Reflections;

/*
 REST API Application

 Created with IntelliJ IDEA.
 User: jrmitaly
 Date: 10/23/17
 Time: 11:46 AM

 Last Updated (automatically updated by SVN)
 $Author: jrmitaly $: Author of last commit
 $Date: 2017-10-24 10:46:29 -0400 (Tue, 24 Oct 2017) $: Date of last commit
 $Rev: 24822 $: Revision of last commit

Notes: This class disables "auto discovery" in Jersey - and as such we need to define all the classes involved in this class
*/

//extend Application (Java RS class) to help register classes for the REST API (jersey, jaxrs) (was previously done using auto discovery, which is not an option with archived wars)
@ApplicationPath("/api")
public class RestAPIApplication extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        try {

            //determine all classes in the server (project specific) "resources" package
            Reflections serverResourceReflections = new Reflections("com.mmhayes.myqc.server.api.resources");
            Set<Class<? extends Object>> restAPIClasses = serverResourceReflections.getSubTypesOf(Object.class);

            //determine all classes in the server (project specific) "filters" package
            Reflections serverFilterReflections = new Reflections("com.mmhayes.myqc.server.api.filters");
            restAPIClasses.addAll(serverFilterReflections.getSubTypesOf(Object.class));

            //determine all classes in the server (project specific) "exceptions" package
            Reflections commonErrorHandlingExceptionReflection = new Reflections("com.mmhayes.common.api.errorhandling.exceptions");
            restAPIClasses.addAll(commonErrorHandlingExceptionReflection.getSubTypesOf(Object.class));

            //determine all classes in the server (project specific) "mappers" package
            Reflections commonErrorHandlingMappersReflection = new Reflections("com.mmhayes.common.api.errorhandling.mappers");
            restAPIClasses.addAll(commonErrorHandlingMappersReflection.getSubTypesOf(Object.class));

            /*  //EXAMPLE ONLY: how to manually add classes that Jersey (REST API) needs to be aware of
            Set<Class<?>> restAPIClasses = new HashSet<Class<?>>();
            restAPIClasses.add(restAPIClasses.class);
            */

            return restAPIClasses;
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
            throw ex;
        }
    }

}