package com.mmhayes.myqc.server.api.resources;

//mmhayes dependencies

import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.myqc.server.collections.FavoriteCollection;
import com.mmhayes.myqc.server.collections.FavoriteOrderCollection;
import com.mmhayes.myqc.server.collections.ProductFavoriteCollection;
import com.mmhayes.myqc.server.models.*;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

//myqc dependencies
//javax.ws.rs dependencies
//other dependencies

/*
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2020-10-01 14:52:17 -0400 (Thu, 01 Oct 2020) $: Date of last commit
 $Rev: 51597 $: Revision of last commit

 Notes: Resource for Favorite Products, Orders and My Quick Picks Page
*/

//REST Resource for /{qc instance}/api/ordering/
@Path("favorite")
public class FavoriteResource {

    @POST
    @Path("/details")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    //Determines if the product with the exact modifiers is a favorite
    public ProductFavoriteModel getFavoriteProductDetails(ProductFavoriteModel productFavoriteModel, @Context HttpServletRequest request) throws Exception{
        FavoriteModel favoriteModel = new FavoriteModel(productFavoriteModel, request);
        return favoriteModel.determineIfFavoriteIsProduct(productFavoriteModel);
    }

    @POST
    @Path("/modifiers")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    //Determines if the product is already a favorite with different modifiers
    public ArrayList<HashMap> getFavoriteProductWithModsDetails(ProductFavoriteModel productFavoriteModel, @Context HttpServletRequest request) throws Exception{
        FavoriteModel favoriteModel = new FavoriteModel(request, productFavoriteModel);
        return favoriteModel.isProductAlreadyFavorite();
    }

    @POST
    @Path("/product/active")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    //Create the product as a favorite
    public Integer createFavoriteProduct(ProductFavoriteModel productFavoriteModel, @Context HttpServletRequest request) throws Exception{
        FavoriteModel favoriteModel = new FavoriteModel(request, productFavoriteModel);
        return favoriteModel.activateFavorite();
    }

    @POST
    @Path("/product/inactive/{favoriteID}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    //Remove the product as a favorite
    public Integer removeFavoriteProduct(@PathParam("favoriteID") Integer favoriteID, @Context HttpServletRequest request) throws Exception{
        return FavoriteModel.inactivateFavorite(favoriteID);
    }

    @POST
    @Path("/all/products")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    //Retrieves all the products in the current store, need to check if product is in a currently active keypad
    public ArrayList<String> getAllProductsInStoreKeypads(@Context HttpServletRequest request) throws Exception {
        return StoreKeypadModel.getProductsInStoreKeypadModel(request);
    }

    @GET
    @Path("/items/list")
    @Produces(MediaType.APPLICATION_JSON)
    //Retrieves product-favorite models in a favorite collection
    public List<FavoriteModel> getLoggedInEmployeesFavoriteProducts(@Context UriInfo uriInfo, @Context HttpServletRequest request) throws Exception {
        FavoriteCollection favoriteCollection = new FavoriteCollection(CommonAPI.populatePaginationParamsFromURI(uriInfo), uriInfo, request);
        favoriteCollection.populateFavoriteCollection(request);
        return favoriteCollection.getFavoriteCollection();
    }

    @GET
    @Path("/popular/list")
    @Produces(MediaType.APPLICATION_JSON)
    //Retrieves product-favorite models in a popular collection
    public List<FavoriteModel> getLoggedInEmployeesPopularProducts(@Context UriInfo uriInfo, @Context HttpServletRequest request) throws Exception {
        FavoriteCollection favoriteCollection = new FavoriteCollection(CommonAPI.populatePaginationParamsFromURI(uriInfo), uriInfo, request);
        favoriteCollection.populatePopularCollection(request);
        return favoriteCollection.getPopularCollection();
    }

    @GET
    @Path("/items/unavailable/list")
    @Produces(MediaType.APPLICATION_JSON)
    //Retrieves product-favorite models in a unavailable collection
    public List<FavoriteModel> getLoggedInEmployeesUnavailableProducts(@Context UriInfo uriInfo, @Context HttpServletRequest request) throws Exception {
        FavoriteCollection favoriteCollection = new FavoriteCollection(CommonAPI.populatePaginationParamsFromURI(uriInfo), uriInfo, request);
        favoriteCollection.populateUnavailableCollection();
        return favoriteCollection.getFavoriteUnavailableCollection();
    }

    @POST
    @Path("/order/activate")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createFavoriteOrder(HashMap modelDetailHM, @Context HttpServletRequest request) throws Exception {
            FavoriteOrderModel favoriteOrderModel = new FavoriteOrderModel(modelDetailHM);
            return Response.ok().entity(favoriteOrderModel.saveRecords(request)).build();
    }

    @POST
    @Path("/order/activate/cart")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createFavoriteOrderFromCart(HashMap body, @Context HttpServletRequest request) throws Exception {
            FavoriteOrderModel favoriteOrderModel = new FavoriteOrderModel();
            return Response.ok().entity(favoriteOrderModel.saveFavoriteBeforeTransaction(body, request)).build();
    }

    @GET
    @Path("/order/is-favorite/{transactionID}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response isFavoriteOrder(@PathParam("transactionID") long transactionId, @Context HttpServletRequest req) throws Exception {
        FavoriteOrderModel favoriteOrderModel = new FavoriteOrderModel();
        return Response.ok().entity(favoriteOrderModel.isFavorite(req, transactionId)).build();
    }

    @POST
    @Path("/order/is-favorite/cart")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response isFavoriteOrder(HashMap products, @Context HttpServletRequest req) throws Exception {
        FavoriteOrderModel favoriteOrderModel = new FavoriteOrderModel(req);
        return Response.ok().entity(favoriteOrderModel.isFavorite(products)).build();
    }

    @PUT
    @Path("/order/{favoriteOrderId}/toggle")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response toggleFavoriteOrder(@PathParam("favoriteOrderId") int favoriteOrderId,@Context HttpServletRequest req) throws Exception {
        FavoriteOrderModel favoriteOrderModel = new FavoriteOrderModel(favoriteOrderId);
        if(favoriteOrderModel.isActive()) {
            favoriteOrderModel.setInactivatedDTM(new Timestamp(new Date().getTime()));
        } else {
            favoriteOrderModel.setInactivatedDTM(null);
        }
        favoriteOrderModel.setActive(!favoriteOrderModel.isActive());
        favoriteOrderModel.updateActiveStatus();
        return Response.ok(favoriteOrderModel.toString()).build();
    }

    @GET
    @Path("/orders/list")
    @Produces(MediaType.APPLICATION_JSON)
    //Retrieves product-favorite order models in a favorite order collection
    public List<FavoriteOrderModel> getLoggedInEmployeesFavoriteOrders(@Context UriInfo uriInfo, @Context HttpServletRequest request) throws Exception {
        FavoriteOrderCollection favoriteOrderCollection = new FavoriteOrderCollection(CommonAPI.populatePaginationParamsFromURI(uriInfo), uriInfo, request, true);
        return favoriteOrderCollection.getCollection();
    }

    @GET
    @Path("/orders/unavailable/list")
    @Produces(MediaType.APPLICATION_JSON)
    //Retrieves product-favorite order models in a favorite unavailable order collection
    public List<FavoriteOrderModel> getLoggedInEmployeesFavoriteUnavailableOrders(@Context UriInfo uriInfo, @Context HttpServletRequest request) throws Exception {
        FavoriteOrderCollection favoriteOrderCollection = new FavoriteOrderCollection(CommonAPI.populatePaginationParamsFromURI(uriInfo), uriInfo, request, false);
        return favoriteOrderCollection.getCollectionUnavailable();
    }

    @POST
    @Path("/order/{favoriteOrderID}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    //Remove the product as a favorite
    public FavoriteOrderModel getFavoriteOrderDetails(@PathParam("favoriteOrderID") Integer favoriteOrderID, @Context HttpServletRequest request) throws Exception{
        return new FavoriteOrderModel(favoriteOrderID, request);
    }

    @POST
    @Path("/order/name")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    //Update the name of the favorite order on the favorite order view
    public Integer updateFavoriteOrderName(HashMap orderDetails, @Context HttpServletRequest request) throws Exception {
        FavoriteOrderModel favoriteOrderModel = new FavoriteOrderModel(request);
        return favoriteOrderModel.updateOrderName(orderDetails);
    }

    @GET
    @Path("/product")
    @Produces(MediaType.APPLICATION_JSON)
    //Retrieves product-favorite order models in a favorite unavailable order collection
    public List<FavoriteOrderModel> getProductFavorites(@Context UriInfo uriInfo, @Context HttpServletRequest request) throws Exception {
        FavoriteOrderCollection favoriteOrderCollection = new FavoriteOrderCollection(CommonAPI.populatePaginationParamsFromURI(uriInfo), uriInfo, request, false);
        return favoriteOrderCollection.getCollectionUnavailable();
    }

    @GET
    @Path("/product/{productID}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<ProductFavoriteModel> getProductFavorites(@PathParam("productID") Integer productID, @Context HttpServletRequest request) throws Exception {
        ProductFavoriteCollection productFavoriteCollection = new ProductFavoriteCollection(productID, request);
        return productFavoriteCollection.getCollection();
    }
}
