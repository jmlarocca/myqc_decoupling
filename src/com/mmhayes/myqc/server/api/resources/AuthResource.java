package com.mmhayes.myqc.server.api.resources;

//mmhayes dependencies
import com.mmhayes.common.api.*;
import com.mmhayes.common.api.errorhandling.exceptions.InvalidAuthException;
import com.mmhayes.common.dataaccess.*;
import com.mmhayes.common.login.LoginSettingsModel;
import com.mmhayes.common.utils.*;

//my qc dependencies
import com.mmhayes.myqc.server.api.InstanceClient;
import com.mmhayes.myqc.server.api.MyQCCache;
import com.mmhayes.myqc.server.collections.TOSCollection;
import com.mmhayes.myqc.server.models.AccountModel;
import com.mmhayes.myqc.server.models.TOSModel;
import com.mmhayes.myqc.server.utils.*;

//javax.ws.rs dependencies
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Context;

//other dependencies
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;

/*
 Last Updated (automatically updated by SVN)
 $Author: mjbatko $: Author of last commit
 $Date: 2021-06-30 16:15:44 -0400 (Wed, 30 Jun 2021) $: Date of last commit
 $Rev: 56261 $: Revision of last commit

 Notes: All notes for pages go here
*/

//REST Resource for /{my qc instance}/api/auth/
@Path("auth")
public class AuthResource {

    protected commonMMHFunctions commDM = new commonMMHFunctions();
    private static DataManager dm = new DataManager();

//START - Login Functionality API Endpoints

    @POST
    @Path("/login")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    //this method attempts to authenticate any client
    public Validation instanceAuth(Validation requestValidation, @Context HttpServletRequest request) {
        try {
            if (CommonAPI.getOnGround()) {  //if the user is on the ground, then login using the local login class
                requestValidation = SecureLogin.userLogin(requestValidation, request);
            } else { //if the application is in the cloud, then we have to ask a gateway
                InstanceClient instanceClient = new InstanceClient();
                requestValidation = instanceClient.gatewayAuth(requestValidation, request);
            }
        } catch (Exception ex) {
            requestValidation.handleGenericException(ex, "AuthResource.instanceAuth");
        }
        return requestValidation;
    }

    /* My QC App does not need to validate Quickcharge users (managers) JUST Quickcharge accounts (employees -jrmitaly 11/23/2015
    @POST
    @Path("/login/users")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    //this method attempts to authenticate JUST Quickcharge users (managers)
    public Validation instanceAuthForQCUsers(Validation requestValidation, @Context HttpServletRequest request) {
        try {
            if (CommonAPI.getOnGround()) {  //if the user is on the ground, then login using the local login class
                requestValidation.setValidationType(2); //JUST Quickcharge User (manager, QC_QuickchargeUsers - validationType of 2)
                requestValidation = SecureLogin.userLogin(requestValidation, request);
            } else { //if the application is in the cloud, then we have to ask a gateway
                InstanceClient instanceClient = new InstanceClient();
                requestValidation = instanceClient.gatewayAuth(requestValidation, request);
            }
        } catch (Exception ex) {
            requestValidation.handleGenericException(ex, "AuthResource.instanceAuth");
        }
        return requestValidation;
    }
    */

    @POST
    @Path("/login/accounts")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    //this method attempts to authenticate JUST Quickcharge accounts (employees)
    public Validation instanceAuthForQCAccounts(Validation requestValidation, @Context HttpServletRequest request) {
        try {
            if (CommonAPI.getOnGround()) {  //if the user is on the ground, then login using the local login class
                requestValidation.setValidationType(3); //JUST Quickcharge Account (employee, QC_Employees, - validationType of 3)
                requestValidation = SecureLogin.userLogin(requestValidation, request);
            } else { //if the application is in the cloud, then we have to ask a gateway

                InstanceClient instanceClient = new InstanceClient();
                if( !requestValidation.getLoginName().contains("@") ) {
                    Validation validationDetails = instanceClient.getInstanceDetails(requestValidation, request);

                    String baseLoginName = requestValidation.getLoginName();
                    String instanceAlias = validationDetails.getInstanceAlias();
                    requestValidation.setLoginName(baseLoginName+"@"+instanceAlias);
                }

                requestValidation = instanceClient.gatewayAuth(requestValidation, request);
            }
        } catch (Exception ex) {
            requestValidation.handleGenericException(ex, "AuthResource.instanceAuth");
        }
        return requestValidation;
    }

    @GET
    @Path("/login/settings")
    @Produces(MediaType.APPLICATION_JSON)
    //this method gets settings for the login page (method is safe, no data in, no sensitive data out)
    public LoginSettingsModel getLoginSettings() {
        //if store is in cache, load from cache
        if ( MyQCCache.checkCacheForLoginSettings() ) {
            return MyQCCache.getLoginSettingsCache();
        }

        LoginSettingsModel loginSettingsModel = CommonAuthResource.retrieveLoginSettings();
        loginSettingsModel.getAccountCreationModel().setGuestLicenseAvailable(AccountModel.guestLicensesAreAvailable());
        loginSettingsModel.getAccountCreationModel().setEmployeeLicenseAvailable(AccountModel.employeeLicensesAreAvailable());

        MyQCCache.setLoginSettingsCache( loginSettingsModel );

        return loginSettingsModel;
    }

    @OPTIONS
    @Path("/validate")
    //this method handles pre-flight requests for CORS for /validate endpoint
    public Response preFlight_authByGSKeyValidation(@Context HttpServletRequest request) {
        //enable CORS for this end point
        return Response.ok()
                .header("Access-Control-Allow-Methods", "POST")
                .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
                .header("Access-Control-Request-Headers", "application/json")
                .header("Access-Control-Allow-Credentials", true)
                //.header("Access-Control-Max-Age", "1209600")  //header for telling the browser to cache the pre-flight CORS check (14 days)
                .allow("OPTIONS").build();
    }

    @POST
    @Path("/validate")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    //this method attempts to validate a GSKey and instanceUserID against the gateway
    public Response authByGSKeyValidation(HashMap validateHM, @Context HttpServletRequest request) {

        //create a new validation object (to respond to the client with)
        Validation requestValidation = new Validation();

        try {
            //get data out of the validateHM passed in
            if (validateHM.get("gskey") != null && validateHM.get("instanceUserID") != null) {

                //set the passed in GSKey and instanceUserID
                requestValidation.setGSKey(validateHM.get("gskey").toString());
                requestValidation.setInstanceUserID(Integer.parseInt(validateHM.get("instanceUserID").toString()));
                Integer instanceUserTypeID = CommonAPI.convertModelDetailToInteger(validateHM.get("instanceUserTypeID"));
                requestValidation.setInstanceUserTypeID(instanceUserTypeID);

            }

            //create a new client so we can verify if this GSKey is legitimate, if it is a session will be created from the client
            InstanceClient instanceClient = new InstanceClient();
            Validation responseValidation = instanceClient.gatewayAuth(requestValidation, request);

            //enable CORS for this end point
            return Response.ok()
                    .entity(responseValidation)
                    .header("Access-Control-Allow-Methods", "GET")
                    .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
                    .header("Access-Control-Request-Headers", "application/json")
                    .header("Access-Control-Allow-Credentials", true)
                    .allow("OPTIONS").build();
        } catch (Exception ex) {
            requestValidation.handleGenericException(ex, "AuthResource.authByGSKeyValidation");
            return Response.ok()
                    .entity(requestValidation)
                    .header("Access-Control-Allow-Methods", "GET")
                    .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
                    .header("Access-Control-Request-Headers", "application/json")
                    .header("Access-Control-Allow-Credentials", true)
                    .allow("OPTIONS").build();
        }
    }

    @POST
    @Path("/logout")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    //this method expires the DSKey in the DB. DSKey is provided in the X-MMHayes-Auth custom header
    public HashMap logoutByDSKey(@Context HttpServletRequest request) {
        return CommonAuthResource.expireDSKeyForMMHayesAuth(request);
    }

    @GET
    @Path("/login/check")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    //this method verifies a DSKey and responds if it's valid or not
    public HashMap verifyDSKey(@Context HttpServletRequest request) throws Exception {
        return CommonAPI.verifyDSKey(request, true);
    }

//END - Login Functionality API Endpoints

//START - Force Password Change Functionality API Endpoints

    @POST
    @Path("/update")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    //this method attempts a password reset from the provided forcePWChangeHM hashmap - for force password change
    public Validation attemptUpdate(HashMap forcePWChangeHM, @Context HttpServletRequest request) throws Exception {

        //create a new validation object (to respond to the client with)
        Validation requestValidation = new Validation();

        if(CommonAuthResource.checkRequestHeaderForMMHayesAuth(request) != null) { //validate session
            try {
                if (CommonAPI.getOnGround()) {
                    requestValidation = SecureLogin.attemptUpdate(requestValidation, forcePWChangeHM, request);
                } else { //if the application is in the cloud, then we have to ask a gateway
                    InstanceClient instanceClient = new InstanceClient();
                    requestValidation = instanceClient.attemptUpdate(requestValidation, forcePWChangeHM, request);
                }
            } catch (Exception ex) {
                requestValidation.handleGenericException(ex, "AuthResource.attemptUpdate");
            }
        } else {
            Logger.logMessage("ERROR: Could not validate session on AuthResource.attemptUpdate", Logger.LEVEL.ERROR);
            requestValidation.setErrorDetails("Invalid Access");
        }
        return requestValidation;

    }

//END - Force Password Change Functionality API Endpoints

//START - ReAuth Verify Password Functionality API Endpoints

    @POST
    @Path("/reauth")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    //this method attempts a verify a user's password - for re-authentication on order submission
    public Validation reAuthBeforePurchase(String userPassword, @Context HttpServletRequest request) throws Exception {
        //create a new validation object (to respond to the client with)
        Validation requestValidation = new Validation();
        if(CommonAuthResource.checkRequestHeaderForMMHayesAuth(request) != null) { //validate session
            try {
                if (CommonAPI.getOnGround()) {
                    requestValidation = SecureLogin.verifyUserPasswordForReAuth(request, userPassword, requestValidation);
                } else { //if the application is in the cloud, then we have to ask a gateway
                    InstanceClient instanceClient = new InstanceClient();
                    requestValidation.setLoginPassword(userPassword);
                    requestValidation = instanceClient.gatewayReAuth(requestValidation, request);
                }
            } catch (Exception ex) {
                requestValidation.handleGenericException(ex, "AuthResource.reAuthBeforePurchase");
            }
        } else {
            Logger.logMessage("ERROR: Could not validate session on AuthResource.reAuthBeforePurchase", Logger.LEVEL.ERROR);
            requestValidation.setErrorDetails("Invalid Access");
        }
        return requestValidation;

    }

    @GET
    @Path("/reauth/off")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    //this method attempts a verify a user's password - for re-authentication on order submission
    //this method verifies a DSKey and responds if it's valid or not
    public HashMap unsetDSKeyReAuthFlag(@Context HttpServletRequest request) throws Exception {
        return CommonAuthResource.unsetDSKeyReAuthFlag(request);
    }

//END - ReAuth Verify Password Functionality API Endpoints

//START - Session Timeout and Extension Functionality API Endpoints

    @GET
    @Path("/extend/check")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    //this method attempts a verify a user's password - for re-authentication on order submission
    //this method verifies a DSKey and responds if it's valid or not
    public boolean checkSessionForExtension(@Context HttpServletRequest request) throws Exception {
        //get custom header X-MMHayes-Auth from request object
        String mmhayesAuthHeader = request.getHeader("X-MMHayes-Auth");

        //if the mmhayesAuthHeader is valid, verify the contents of it (Device Session Key)
        if (mmhayesAuthHeader == null) {
            Logger.logMessage("ERROR: Could not find X-MMHayes-Auth in the request header CommonAuthResource.checkRequestHeaderForMMHayesAuth", Logger.LEVEL.ERROR);
            throw new InvalidAuthException();
        }

        return CommonAuthResource.checkSessionValidForExtenstion(mmhayesAuthHeader);
    }

    @POST
    @Path("/extend")
    @Consumes(MediaType.WILDCARD)
    @Produces(MediaType.APPLICATION_JSON)
    public HashMap extendSession(@Context HttpServletRequest request) throws Exception {
        return CommonAuthResource.extendSession(request);
    }


//END - Session Timeout and Extension Functionality API Endpoints

//START - Check Account License for Availability API Endpoints

    @POST
    @Path("/licenses")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    //this method attempts a license availability check before the TOS is displayed
    public boolean getLicenseAvailability(@Context HttpServletRequest request) throws Exception {
        Integer userID = CommonAuthResource.checkRequestHeaderForMMHayesAuth(request);
        return commDM.getEmployeeLicenseAvailabilityByUserID(userID);
    }

//END - Check Account License for Availability API Endpoints

//START - SSO Endpoints

    @POST
    @Path("/login/verifysso")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public HashMap verifySSOLogin(HashMap data, @Context HttpServletRequest request) throws Exception {
        HashMap resultHM = CommonAPI.verifyDSKey(request);
        String tosID = data.get("tosID").toString();

        //return failure if no valid result from verifying DS key
        if ( resultHM == null || !resultHM.containsKey("result") ) {
            resultHM = new HashMap();
            resultHM.put("errorDetails", "An error occurred while attempting to verify the user.");
            Logger.logMessage("Error verifying DS key for SSO in AuthResource.verifySSOLogin", Logger.LEVEL.DEBUG);
            return resultHM;
        }

        //return failure if bad dskey
        if ( resultHM.get("result").equals("no") ) {
            resultHM.put("errorDetails", "Invalid authentication provided.");
            Logger.logMessage("Attempt to verify SSO login failed due to bad DS Key in AuthResource.verifySSOLogin", Logger.LEVEL.DEBUG);
            return resultHM;
        }

        //if tos needs to be accepted, check settings and get TOS data
        if ( tosID != null && tosID.length() > 0 ) {
            boolean validLicenses;
            boolean importAllInactive = CommonAPI.getImportAllInactive();

            if ( importAllInactive ) {
                Integer userID = CommonAPI.convertModelDetailToInteger(data.get("userID"));
                validLicenses = commDM.getEmployeeLicenseAvailabilityByUserID(userID);
            } else {
                validLicenses = true;
            }

            if ( !validLicenses ) {
                resultHM.put("result", "no");
                resultHM.put("errorDetails", "Cannot display Terms of Service agreement due to insufficient licenses. Please contact your supervisor for more information.");
                return resultHM;
            }

            //get TOS title and content, build TOS model, return it in resultHM
            ArrayList<HashMap> result = dm.parameterizedExecuteQuery("data.common.getTOS",
                new Object[]{
                    tosID
                },
                true
            );

            if (result == null || result.size() != 1) {
                resultHM.put("result", "no");
                resultHM.put("errorDetails", "Could not retrieve TOS content.");
                return resultHM;
            }

            //TOS is always returned as a collection of models
            TOSCollection tosCollection = new TOSCollection();
            TOSModel tosModel = new TOSModel(result.get(0));
            tosCollection.getCollection().add(tosModel);
            resultHM.put("tos", tosCollection.getCollection());
        }

        return resultHM;
    }

//END - SSO Endpoints

//START - Pass Through Check Error Endpoints

    @GET
    @Path("/error")
    @Produces(MediaType.APPLICATION_JSON)
    public String forceAuthError() throws Exception{
        // Method throws exception and cient code checks for either a JSON error or
        // HTML IIS error to check state of IIS HTML ERROR PASSTHROUGH setting. Check [B-03947] for additional details.
        // IF JSON is NOT received then the UI will show warning msg and recommended fix (run batch script)
        Logger.logMessage("NOTE: The following error is triggered to monitor application configuration and is NOT an actual failure.", Logger.LEVEL.ERROR);
        throw new NotAuthorizedException("InvalidAuthException");
    }

    @GET
    @Path("/errorHTML")
    @Produces(MediaType.TEXT_HTML)
    public String forceHTMLError() throws Exception{
        // TEST METHOD for returning HTML for testing /errorAPI
        return "<html><head></head><body>hi</body></html>";
    }

//END   - Pass Through Check Error Endpoints

}
