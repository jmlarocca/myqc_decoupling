package com.mmhayes.myqc.server.api.resources;

//mmhayes dependencies
import com.mmhayes.common.account.collections.AccountPersonCollection;
import com.mmhayes.common.account.models.AccountPersonModel;
import com.mmhayes.common.api.*;

//my qc dependencies
import com.mmhayes.myqc.server.collections.AccountSettingsCollection;
import com.mmhayes.myqc.server.collections.TOSCollection;
import com.mmhayes.myqc.server.collections.AccountDetailCollection;
import com.mmhayes.myqc.server.models.AccountModel;
import com.mmhayes.myqc.server.models.AccountSettingsDetailModel;
import com.mmhayes.myqc.server.models.MealPlanModel;

//javax.ws.rs dependencies
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Context;

//other dependencies
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;

/*
 Last Updated (automatically updated by SVN)
 $Author: ijgerstein $: Author of last commit
 $Date: 2021-06-29 15:36:13 -0400 (Tue, 29 Jun 2021) $: Date of last commit
 $Rev: 56235 $: Revision of last commit

 Notes: All notes for pages go here
*/

//REST Resource for /{qc instance}/api/account/
@Path("account")
public class AccountResource {

    @POST
    @Path("/tos/accept/{tosID}")
    @Produces(MediaType.APPLICATION_JSON)
    //Retrieves StoreBalanceModel models in a StoreBalance collection
    public HashMap<String, String> acceptTOS(@PathParam("tosID") Integer tosID, @Context HttpServletRequest request) {
        return TOSCollection.acceptTOS(tosID, request);
    }

    @POST
    @Path("/freeze")
    @Produces(MediaType.APPLICATION_JSON)
    //"freezes" an account by modifying the logged in account's status
    public HashMap<String, String> freezeAccount(@Context HttpServletRequest request) {
        return AccountDetailCollection.modifyAccountStatus("F", request);
    }

    @POST
    @Path("/unfreeze")
    @Produces(MediaType.APPLICATION_JSON)
    //"unfreezes" an account by modifying the logged in account's status
    public HashMap<String, String> unfreezeAccount(@Context HttpServletRequest request) {
        return AccountDetailCollection.modifyAccountStatus("A", request);
    }

    @POST
    @Path("/duplicate/name")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    //checks for duplicate account names (username or email) for account creation
    public HashMap checkDuplicateAccountName(HashMap accountNameInfo, @Context HttpServletRequest request) {
        return AccountModel.checkDuplicateAccountNames(accountNameInfo, request);
    }

    @GET
    @Path("/sendAnotherInvite/{employeeID}")
    @Produces(MediaType.APPLICATION_JSON)
    //sends another invite to the employee if they have not excepted theirs yet
    public Boolean sendAnotherInvite(@PathParam("employeeID") String employeeID) {
        return AccountModel.sendInviteForGatewayEmailAcct(employeeID);
    }

    @POST
    @Path("/duplicate/number")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    //checks for duplicate account numbers or badge numbers for account creation
    public HashMap checkDuplicateAcctNumber(HashMap acctNumberInfo) {
        return AccountModel.checkDuplicateAccountNumber(acctNumberInfo);
    }

    @POST
    @Path("/tos/status")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    //checks if the TOS is required and if there are available licenses for account creation
    public HashMap checkTOSandAccountStatus(HashMap accountTypeInfo) {
        return AccountModel.determineAccountStatusAndLicense(accountTypeInfo);
    }

    @GET
    @Path("/tos/getAccepted")
    @Produces(MediaType.APPLICATION_JSON)
    // Gets the latest TOS that was accepted by this user
    public HashMap getLastAcceptedTOS(@Context HttpServletRequest request) {
        return AccountModel.getLastAcceptedTOS(request);
    }

    @POST
    @Path("/createAccount")
    @Produces(MediaType.APPLICATION_JSON)
    //creates the account for username or email
    public HashMap<String, String> createAccountInMyQC(HashMap accountInfo, @Context HttpServletRequest request) {
        return AccountModel.createAccountInMyQC(accountInfo, request);
    }

    @GET
    @Path("/createAccount/available/guest")
    @Produces(MediaType.APPLICATION_JSON)
    public HashMap<String, Boolean> guestAccountsAreAvailable() throws Exception {
        HashMap<String, Boolean> guestsAvailable = new HashMap();
        guestsAvailable.put("accountCreationAllowed", AccountModel.guestLicensesAreAvailable());

        return guestsAvailable;
    }

    @POST
    @Path("/person/create")
    @Produces(MediaType.APPLICATION_JSON)
    //creates the account for username or email
    public HashMap<String, String> createPersonAccountInMyQC(HashMap accountInfo, @Context HttpServletRequest request) {
        return AccountModel.createPersonAccountInMyQC(accountInfo, request);
    }

    @GET
    @Path("/force/change")
    @Produces(MediaType.APPLICATION_JSON)
    //checks the Force setting on the account group of the employee account
    public boolean checkIfForceChangeIsON(@Context HttpServletRequest request) throws Exception {
        AccountSettingsCollection accountSettingsCollection = new AccountSettingsCollection(request, "");
        return accountSettingsCollection.checkIfForceChangeIsOn();
    }

    @GET
    @Path("/settings")
    @Produces(MediaType.APPLICATION_JSON)
    //Retrieves spending profile, account group and account type details for account settings
    public AccountSettingsDetailModel getAccountSettingsDetails(@Context HttpServletRequest request) throws Exception {
        return new AccountSettingsDetailModel(request);
    }

    @POST
    @Path("/check/tos")
    @Produces(MediaType.APPLICATION_JSON)
    //Retrieves spending profile, account group and account type details for account settings
    public ArrayList<HashMap> checkIfTOSIsRequired(HashMap accountDetailsHM, @Context HttpServletRequest request) throws Exception {
        AccountSettingsCollection accountSettingsCollection = new AccountSettingsCollection(request, accountDetailsHM);
        return accountSettingsCollection.checkIfTOSIsRequired();
    }

    @POST
    @Path("/settings/changed")
    @Produces(MediaType.APPLICATION_JSON)
    //Retrieves spending profile, account group and account type details for account settings
    public AccountSettingsDetailModel getAccountSettingsDetailsFromAccountGroup(HashMap accountDetailsHM, @Context HttpServletRequest request) throws Exception {
        return new AccountSettingsDetailModel(accountDetailsHM, request);
    }

    @POST
    @Path("/settings/cancel")
    @Produces(MediaType.APPLICATION_JSON)
    //Retrieves spending profile, account group and account type details for account settings
    public boolean getCancelAccountSettingChanges(HashMap accountDetailsHM, @Context HttpServletRequest request) throws Exception {
        AccountSettingsCollection accountSettingsCollection = new AccountSettingsCollection(request, accountDetailsHM);
        return accountSettingsCollection.cancelAccountSettingChanges();
    }

    @POST
    @Path("/settings/save")
    @Produces(MediaType.APPLICATION_JSON)
    //Retrieves spending profile, account group and account type details for account settings
    public boolean saveAccountSettingChanges(HashMap accountDetailsHM, @Context HttpServletRequest request) throws Exception {
        AccountSettingsCollection accountSettingsCollection = new AccountSettingsCollection(request, accountDetailsHM);
        return accountSettingsCollection.saveAccountSettingChanges();
    }

    @GET
    @Path("/user/settings/{personID}")
    @Produces(MediaType.APPLICATION_JSON)
    //Retrieves user settings details for person account
    public String getPersonUserSettings(@PathParam("personID") Integer personID, @Context HttpServletRequest request) throws Exception {
        return AccountSettingsCollection.getPersonUserSettings(personID);
    }

    @POST
    @Path("/person/password")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    //Checks if the password of the person account is the same as the password saved in the global settings
    public boolean checkPersonAccountPassword(HashMap passwordHM, @Context HttpServletRequest request) throws Exception {
        return AccountModel.getPersonAccountPassword(passwordHM);
    }

    @POST
    @Path("/person/employees/{personID}")
    @Produces(MediaType.APPLICATION_JSON)
    //Returns the employee Ids that are associated with the person Id
    public ArrayList<AccountPersonModel> getEmployeeAssociations(@PathParam("personID") String personID, @Context HttpServletRequest request) throws Exception {
        AccountPersonCollection accountPersonCollection = new AccountPersonCollection(personID);
        return accountPersonCollection.getCollection();
    }

    @POST
    @Path("/person/relationships")
    @Produces(MediaType.APPLICATION_JSON)
    //Gets relationship options for selector on 'Add Account to Manage' page
    public ArrayList<HashMap> getPersonRelationshipTypes(@Context HttpServletRequest request) throws Exception {
        AccountPersonCollection accountPersonCollection = new AccountPersonCollection();
        return accountPersonCollection.getRelationships();
    }

    @POST
    @Path("/person/mapping/find")
    @Produces(MediaType.APPLICATION_JSON)
    //Determines if the Name and StudentID match a QC_Employee and if a mapping exists
    public AccountPersonModel findPersonAccountMapping(HashMap accountInfo, @Context HttpServletRequest request) throws Exception {
        AccountPersonModel accountPersonModel = new AccountPersonModel();
        return accountPersonModel.findEmployee(accountInfo);
    }

    @POST
    @Path("/person/add")
    @Produces(MediaType.APPLICATION_JSON)
    //Creates an new employee to person mapping or reactivates and existing mapping
    public Integer addPersonAccount(HashMap accountInfo, @Context HttpServletRequest request) throws Exception {
        AccountPersonModel accountPersonModel = new AccountPersonModel();
        return accountPersonModel.employeePersonMapping(accountInfo);
    }

    @GET
    @Path("/person/update/dsKey/{employeeID}")
    @Produces(MediaType.APPLICATION_JSON)
    //Adds the employeeID to the DSKey record in QC_DeviceSessionKey
    public Integer updatePersonAccountDSKey(@PathParam("employeeID") String employeeID, @Context HttpServletRequest request) throws Exception {
        return CommonAPI.updatePersonAccountDSKey(employeeID, request);
    }

    @POST
    @Path("/mealPlan/transaction")
    @Produces(MediaType.APPLICATION_JSON)
    //Creates a PATransaction when a meal plan is purchased on the Spending Profile in Account Settings
    public Integer createMealPlanTransaction(MealPlanModel mealPlanModel, @Context HttpServletRequest request) throws Exception {
         AccountSettingsCollection accountSettingsCollection = new AccountSettingsCollection(request);
         return accountSettingsCollection.createMealPlanTransaction(mealPlanModel);
    }

    @GET
    @Path("/balance/details")
    @Produces(MediaType.APPLICATION_JSON)
    //Retrieves the account's balance and payment method
    public HashMap getAccountBalanceAndPaymentMethodDetails(@Context HttpServletRequest request) throws Exception {
        return AccountSettingsCollection.getAccountBalanceAndPaymentMethod(request);
    }

    @GET
    @Path("/balance")
    @Produces(MediaType.APPLICATION_JSON)
    //Retrieves the account's balance and payment method
    public HashMap getAccountBalance(@Context HttpServletRequest request) throws Exception {
        return AccountSettingsCollection.getAccountBalance(request);
    }

    @GET
    @Path("/sso/extend/dsKey")
    @Produces(MediaType.APPLICATION_JSON)
    //Creates a PATransaction when a meal plan is purchased on the Spending Profile in Account Settings
    public Integer extendSSODSKeyForFingerprint(@Context HttpServletRequest request) throws Exception {
        return CommonAPI.updateSSODSKeyForFingerprintAuth(request);
    }

    @POST
    @Path("/create/person")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    //Creates a gateway person account with the MyQC instance ID
    public Validation createGatewayPersonAccountInQCWeb(HashMap accountHM, @Context HttpServletRequest request) throws Exception {
        return AccountModel.createGatewayPersonAccount(accountHM, request);
    }

    @GET
    @Path("/calculate/mealPlan/{spendingProfileID}")
    @Produces(MediaType.APPLICATION_JSON)
    //Retrieves the account's balance and payment method
    public MealPlanModel getMealPlanDetails(@PathParam("spendingProfileID") String spendingProfileID, @Context HttpServletRequest request) throws Exception {
        return AccountSettingsCollection.calculateMealPlanAmount(spendingProfileID, request);
    }

    @POST
    @Path("/registerDevice")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    // Registers a device in the database if not already registered
    public Boolean registerDevice(HashMap registrationInfo, @Context HttpServletRequest request) throws Exception {
        return AccountModel.registerDevice(registrationInfo, request);
    }

}
