package com.mmhayes.myqc.server.api.resources;

//mmhayes dependencies
import com.mmhayes.common.api.*;
import com.mmhayes.common.donation.collections.DonationCollection;
import com.mmhayes.common.donation.models.DonationModel;
import com.mmhayes.common.loyalty.collections.LoyaltyPointCollection;
import com.mmhayes.common.loyalty.collections.LoyaltyTransactionPointHistoryCollection;
import com.mmhayes.common.loyalty.collections.LoyaltyRewardCollection;
import com.mmhayes.common.loyalty.models.LoyaltyPointModel;
import com.mmhayes.common.loyalty.models.LoyaltyRewardModel;
import com.mmhayes.common.loyalty.models.LoyaltyTransactionPointHistoryModel;

//javax.ws.rs dependencies
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Context;

//other dependencies
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.UriInfo;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by admin on 1/19/2017.
 */
@Path("loyalty")
public class LoyaltyResource {

    @GET
    @Path("/programs")
    @Produces(MediaType.APPLICATION_JSON)
    //retrieve a summary of loyalty programs and point totals for the user
    public List<LoyaltyPointModel> getLoyaltyProgramsForAccount(@Context HttpServletRequest request) throws Exception {
        Integer accountID = CommonAuthResource.checkRequestHeaderForMMHayesAuth(request);
        LoyaltyPointCollection LPC = LoyaltyPointCollection.getAllLoyaltyPointsForEmployee(accountID, true);
        return LPC.getCollection();
    }

    @GET
    @Path("/rewards/{loyaltyProgramID}")
    @Produces(MediaType.APPLICATION_JSON)
    //retrieve all rewards associated with the program and in the user's spending profile
    public List<LoyaltyRewardModel> getLoyaltyProgramRewards(@PathParam("loyaltyProgramID") Integer loyaltyProgramID, @Context HttpServletRequest request) throws Exception {
        Integer accountID = CommonAuthResource.checkRequestHeaderForMMHayesAuth(request);
        LoyaltyRewardCollection LRC = new LoyaltyRewardCollection();
        return LRC.getLoyaltyRewardsForProgramByRevenueCenter(accountID,loyaltyProgramID).getCollection();
    }

    @GET
    @Path("/history/{loyaltyProgramID}")
    @Produces(MediaType.APPLICATION_JSON)
    //retrieve history records for the user associated with the given loyalty program
    public List<LoyaltyTransactionPointHistoryModel> getLoyaltyProgramHistory(@Context UriInfo uriInfo, @PathParam("loyaltyProgramID") Integer loyaltyProgramID, @Context HttpServletRequest request) throws Exception {
        Integer accountID = CommonAuthResource.checkRequestHeaderForMMHayesAuth(request);
        HashMap<String, LinkedList> paginationParamsHM = CommonAPI.populatePaginationParamsFromURI(uriInfo);
        LoyaltyTransactionPointHistoryCollection loyaltyPointCollection = new LoyaltyTransactionPointHistoryCollection();
        return loyaltyPointCollection.getLoyaltyProgramPointsHistoryForEmployee(paginationParamsHM, accountID, loyaltyProgramID).getCollection();
    }

    @GET
    @Path("/donations")
    @Produces(MediaType.APPLICATION_JSON)
    //retrieve a summary of donations for the user
    public List<DonationModel> getLoyaltyDonationsForAccount(@Context HttpServletRequest request) throws Exception {
        Integer accountID = CommonAuthResource.checkRequestHeaderForMMHayesAuth(request);
        DonationCollection donationCollection = DonationCollection.getAllDonationsByEmployeeId(accountID, null);
        return donationCollection.getCollection();
    }
}
