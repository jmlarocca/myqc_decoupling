package com.mmhayes.myqc.server.api.resources;

//mmhayes dependencies

import com.mmhayes.common.funding.FundingHelper;
import com.mmhayes.common.funding.models.AccountAutoFundingModel;
import com.mmhayes.common.funding.models.AccountFundingDetailModel;
import com.mmhayes.common.funding.models.AccountPaymentMethodModel;
import com.mmhayes.common.funding.models.FundingModel;
import com.mmhayes.myqc.server.utils.MyQCEmailHelper;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.math.BigDecimal;
import java.util.HashMap;

//myqc dependencies
//javax.ws.rs dependencies
//other dependencies

/*
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2020-08-18 15:25:03 -0400 (Tue, 18 Aug 2020) $: Date of last commit
 $Rev: 50946 $: Revision of last commit

 Notes: Resource for Account Funding requests - contains end points for generic funding AND handles "Account Funding" requests for My QC application.
 This resource may be used by clients that are NOT served up by this war. Proper session (DSKey) should be required for communication!
*/

//REST Resource for /{myqc instance}/api/funding/
@Path("funding")
public class FundingResource {

    @POST
    @Path("/fund/{fundAmount}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    //funds QC account by charging the default payment method on file for the account
    public FundingModel fundAccount(@PathParam("fundAmount") BigDecimal amount, @Context HttpServletRequest request) throws Exception {
        MyQCEmailHelper.initialize(request);
        FundingModel fundingModel = new FundingModel(request);
        return fundingModel.chargeThenFundAccount(amount, fundingModel.getAccountPaymentMethod(), "manual");
    }

    @GET
    @Path("/fee/{fundAmount}")
    @Produces(MediaType.APPLICATION_JSON)
    //Retrieves a single Menu model (which contain MenuItemCollection which contain MenuItemModels)
    public AccountPaymentMethodModel determineFundingFee(@PathParam("fundAmount") BigDecimal fundAmount, @Context HttpServletRequest request) throws Exception {
        return FundingHelper.determineFundingFee(fundAmount, request);
    }

    @GET
    @Path("/account/detail")
    @Produces(MediaType.APPLICATION_JSON)
    //responds with funding amounts and payment method details for the authenticated account
    public AccountFundingDetailModel getFundingAccountDetails(@Context HttpServletRequest request) throws Exception {
        MyQCEmailHelper.initialize(request);
        return new AccountFundingDetailModel(request);
    }

    @GET
    @Path("/account/store/balance/info")
    @Produces(MediaType.APPLICATION_JSON)
    //TODO: This method relies on one "My QC Funding" terminal type per spending profile. One day it should support multiple -jmd 8/27/18
    //responds with the funding store balance (a.k.a terminal group) balance information - for a SINGLE "My QC Funding" terminal
    public HashMap getFundingStoreBalanceInfo(@Context HttpServletRequest request) throws Exception {
        return FundingHelper.determineFundingStoreBalanceInfo(request);
    }

    @POST
    @Path("/account/payment/default/create")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    //creates a new default payment method for the account (will inactivate any existing defaults)
    public AccountPaymentMethodModel createNewDefaultPaymentMethodForAccount(AccountPaymentMethodModel accountPaymentMethodModel, @Context HttpServletRequest request) throws Exception {
        MyQCEmailHelper.initialize(request);
        return FundingHelper.createNewDefaultPaymentMethodForAccount(accountPaymentMethodModel, request);
    }

    @POST
    @Path("/account/payment/default/inactivate")
    @Consumes(MediaType.APPLICATION_JSON)
    //inactivates default payment method on the account and disables automatic reloads for the account
    public void inactivateDefaultPaymentMethodForccount(@Context HttpServletRequest request) throws Exception {
        MyQCEmailHelper.initialize(request);
        FundingHelper.inactivateDefaultPaymentMethodForAccount(true, request);
    }

    @POST
    @Path("/auto/reload")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    //modifies automatic reloads for the authenticated account
    public AccountAutoFundingModel modifyAutomaticReload(AccountAutoFundingModel accountAutoFundingModel, @Context HttpServletRequest request) throws Exception {
        MyQCEmailHelper.initialize(request);
        return FundingHelper.modifyOrEnableAutomaticReload(accountAutoFundingModel, request);
    }

    @POST
    @Path("/auto/reload/enable")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    //enables automatic reloads for the authenticated account
    public AccountAutoFundingModel enableAutomaticReload(AccountAutoFundingModel accountAutoFundingModel, @Context HttpServletRequest request) throws Exception {
        MyQCEmailHelper.initialize(request);
        return FundingHelper.modifyOrEnableAutomaticReload(accountAutoFundingModel, request);
    }

    @POST
    @Path("/auto/reload/disable")
    @Consumes(MediaType.APPLICATION_JSON)
    //disables (cancels) automatic reloads for the authenticated account
    public void disableAutomaticReload(@Context HttpServletRequest request) throws Exception {
        MyQCEmailHelper.initialize(request);
        FundingHelper.disableAutomaticReload(request);
    }

    @POST
    @Path("/charge/{chargeAmount}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    //Charges the default payment method on file for the account
    public FundingModel chargeAccount(@PathParam("chargeAmount") BigDecimal amount, @Context HttpServletRequest request) throws Exception {
        MyQCEmailHelper.initialize(request);
        FundingModel fundingModel = new FundingModel(request);
        fundingModel.setFundingTransaction(false);
        fundingModel.getAccountPaymentMethod().setTerminalHasFundingFee(false); //fee is already calculated into charge amount
        return fundingModel.chargeAccount(amount, fundingModel.getAccountPaymentMethod(), "manual");
    }

    @POST
    @Path("/refund")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    //Refunds the default payment method on file for the account
    public FundingModel refundAccount(FundingModel fundingModel, @Context HttpServletRequest request) throws Exception {
        MyQCEmailHelper.initialize(request);
        // Create new FundingModel for private fields that weren't sent in charge request
        FundingModel refundFundingModel = new FundingModel(request);
        // Update new FundingModel with processedAmount and processorRefNum
        refundFundingModel.setProcessedAmount(fundingModel.getProcessedAmount());
        refundFundingModel.setProcessorRefNum(fundingModel.getProcessorRefNum());
        return refundFundingModel.refundChargedAmount(request);
    }

    @GET
    @Path("/auto/minutes")
    @Produces(MediaType.APPLICATION_JSON)
    //Retrieve the number of minutes until the funding executor will run again
    public long determineMinutesUntilFundingExecutor(@Context HttpServletRequest request) throws Exception {
        return FundingHelper.getMinutesSinceFundingExecutorRan();
    }
}