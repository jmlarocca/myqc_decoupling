package com.mmhayes.myqc.server.api.resources;

//mmhayes dependencies

import com.mmhayes.common.account.models.AccountModel;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.CommonAuthResource;
import com.mmhayes.common.api.Validation;
import com.mmhayes.common.api.errorhandling.exceptions.InvalidAuthException;
import com.mmhayes.common.donation.collections.DonationCollection;
import com.mmhayes.common.donation.models.DonationModel;
import com.mmhayes.common.receiptGen.ReceiptGen;
import com.mmhayes.common.receiptGen.Renderer.IRenderer;
import com.mmhayes.common.receiptGen.TypeData;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.myqc.server.api.MyQCCache;
import com.mmhayes.myqc.server.collections.*;
import com.mmhayes.myqc.server.models.*;
import com.mmhayes.myqc.server.utils.MMHServletListener;
import com.mmhayes.myqc.server.utils.MyQCEmailHelper;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

//myqc dependencies
//javax.ws.rs dependencies
//other dependencies

/*
 Last Updated (automatically updated by SVN)
 $Author: ijgerstein $: Author of last commit
 $Date: 2021-05-03 15:30:58 -0400 (Mon, 03 May 2021) $: Date of last commit
 $Rev: 55344 $: Revision of last commit

 Notes: Resource for My Quickcharge (myqc) specific requests
*/

//REST Resource for /{qc instance}/api/myqc/
@Path("myqc")
public class MyQCResource {

    @GET
    @Path("/version")
    @Produces(MediaType.APPLICATION_JSON)
    //Retrieves the version of the MyQC WAR
    public String getAppVersion (@Context HttpServletRequest request) {
        return "{\"app-version\":\"" + MMHServletListener.WAR_VERSION + "\"}";
    }

    @GET
    @Path("/nav")
    @Produces(MediaType.APPLICATION_JSON)
    //Retrieves my quickcharge navigation models in a my quickcharge navigation collection
    public List<MyQCNavModel> getMyQCNav(@Context HttpServletRequest request) throws Exception {
        HashMap DSKeyHM = CommonAuthResource.determineAuthDetailsFromAuthHeader(request);

        MyQCNavCollection myQCNavCollection;

        if ( DSKeyHM.containsKey("personID") && DSKeyHM.get("personID") != null && !DSKeyHM.get("personID").toString().equals("") && DSKeyHM.containsKey("accountModel") && DSKeyHM.get("accountModel") != null ) {
            com.mmhayes.common.account.models.AccountModel accountModel = (AccountModel) DSKeyHM.get("accountModel");
            myQCNavCollection = new MyQCNavCollection(request, accountModel.getId(), true);
        } else {
            myQCNavCollection = new MyQCNavCollection(request);
        }

        return myQCNavCollection.getCollection();
    }

    @GET
    @Path("/transactions")
    @Produces(MediaType.APPLICATION_JSON)
    //Retrieves transactions models in a transaction collection
    public List<TransactionModel> getLoggedInEmployeesTransactions(@Context UriInfo uriInfo, @Context HttpServletRequest request) throws Exception {
        HashMap<String, LinkedList> paginationParamsHM = paginationParamsHM = CommonAPI.populatePaginationParamsFromURI(uriInfo);
        TransactionCollection transactionCollection = new TransactionCollection(paginationParamsHM, request);
        return transactionCollection.getCollection();
    }

    @GET
    @Path("/transactions/receipt/{transactionID}")
    @Produces(MediaType.APPLICATION_JSON)
    //Retrieves transactions models in a transaction collection
    public HashMap getReceiptByTransactionID(@PathParam("transactionID") Integer transactionID, @Context HttpServletRequest request) throws Exception {
        if ( CommonAuthResource.checkRequestHeaderForMMHayesAuth(request) == null ) {
            throw new InvalidAuthException();
        }

        HashMap result = new HashMap();
        ReceiptGen rg = new ReceiptGen();

        //Set renderer
        IRenderer htmlRenderer = rg.createBasicHTMLRenderer();

        //Set receipt data object
        rg.setReceiptData(rg.createReceiptData());
        rg.getReceiptData().setPopupVal(true);
        rg.getReceiptData().setReceiptTypeID(TypeData.ReceiptType.POPUPRECEIPT);
        rg.getReceiptData().setDisplayWellnessIcons(HealthyIndicatorFeatureModel.showHealthyIndicatorsOnReceipts());

        //Build and generate the receipt
        if (htmlRenderer != null && rg.buildFromDBOrderNum(transactionID, TypeData.ReceiptType.POPUPRECEIPT, htmlRenderer))
        {
            result.put("content", htmlRenderer.toString());
            return result;
        }

        return new HashMap();
    }


    @GET
    @Path("/deductions")
    @Produces(MediaType.APPLICATION_JSON)
    //Retrieves deductions models in a deduction collection
    public List<DeductionModel> getLoggedInEmployeesDeductions(@Context UriInfo uriInfo, @Context HttpServletRequest request) throws Exception {
        HashMap<String, LinkedList> paginationParamsHM = paginationParamsHM = CommonAPI.populatePaginationParamsFromURI(uriInfo);
        DeductionCollection deductionCollection = new DeductionCollection(paginationParamsHM, request);
        return deductionCollection.getCollection();
    }

    @GET
    @Path("/funds")
    @Produces(MediaType.APPLICATION_JSON)
    //Retrieves fund models in a fund collection
    public List<FundModel> getLoggedInEmployeesFunds(@Context UriInfo uriInfo, @Context HttpServletRequest request) throws Exception {
        HashMap<String, LinkedList> paginationParamsHM = paginationParamsHM = CommonAPI.populatePaginationParamsFromURI(uriInfo);
        FundCollection fundCollection = new FundCollection(paginationParamsHM, request);
        return fundCollection.getCollection();
    }

    @GET
    @Path("/accountdetails")
    @Produces(MediaType.APPLICATION_JSON)
    //Retrieves account detail models in a account detail collection
    public List<AccountDetailModel> getLoggedInEmployeesAccountDetails(@Context HttpServletRequest request) throws Exception {
        AccountDetailCollection accountDetailCollection = new AccountDetailCollection(request);
        return accountDetailCollection.getCollection();
    }

    @GET
    @Path("/spendingprofiles")
    @Produces(MediaType.APPLICATION_JSON)
    //Retrieves spending profiles models in a spending profile collection
    public List<SpendingProfileModel> getLoggedInEmployeesSpendingProfiles(@Context HttpServletRequest request) throws Exception {
        SpendingProfileCollection spendingProfileCollection = new SpendingProfileCollection(request);
        return spendingProfileCollection.getCollection();
    }

    @POST
    @Path("/spendingprofiles")
    @Consumes(MediaType.WILDCARD)
    @Produces(MediaType.APPLICATION_JSON)
    //Updates the logged in employee's spending profile to the spending profile ID provided in spendingProfileHM
    public HashMap<String, String> updateLoggedInEmployeesSpendingProfile(HashMap spendingProfileHM, @Context HttpServletRequest request) throws Exception {
        SpendingProfileCollection spendingProfileCollection = new SpendingProfileCollection(request);
        return spendingProfileCollection.updateEmployeeSpendingProfile(spendingProfileHM);
    }

    @GET
    @Path("/spendingprofiles/check")
    @Produces(MediaType.APPLICATION_JSON)
    //Checks if the logged in employee has more than one spending profile and responds with a Boolean in an object
    public HashMap<String, Boolean> getLoggedInEmployeesSpendingProfilesCount(@Context HttpServletRequest request) throws Exception {
        SpendingProfileCollection spendingProfileCollection = new SpendingProfileCollection(request);
        return spendingProfileCollection.checkSpendingProfileCount();
    }

    @GET
    @Path("/account/settings")
    @Produces(MediaType.APPLICATION_JSON)
    //Retrieves spending profile, account group and account type details for account settings
    public AccountSettingsDetailModel getAccountSettingsDetails(@Context HttpServletRequest request) throws Exception {
        return new AccountSettingsDetailModel(request);
    }

    @GET
    @Path("/donations")
    @Produces(MediaType.APPLICATION_JSON)
    //retrieve a summary of monetary donations for the user
    public List<DonationModel> getMonetaryDonationsForAccount(@Context HttpServletRequest request) throws Exception {
        Integer accountID = CommonAuthResource.checkRequestHeaderForMMHayesAuth(request);
        DonationCollection donationCollection = DonationCollection.getAllMonetaryDonationsByEmployeeId(accountID, null);
        return donationCollection.getCollection();
    }

    @GET
    @Path("/balances")
    @Produces(MediaType.APPLICATION_JSON)
    //Retrieves Balance models in a Balance collection
    public List<BalanceModel> getLoggedInEmployeeBalancesByTerminalGroup(@Context HttpServletRequest request) throws Exception {
        BalanceCollection balanceCollection = new BalanceCollection(request);
        return balanceCollection.getCollection();
    }

    @GET
    @Path("/balances/dollarDonationSettings")
    @Produces(MediaType.APPLICATION_JSON)
    //Retrieves dollar donation settings from the global settings and spending profile of the user
    public ArrayList<HashMap> getDollarDonationSettingsInfo(@Context HttpServletRequest request) throws Exception {
        BalanceCollection balanceCollection = new BalanceCollection(request);
        return balanceCollection.getDollarDonationSettingsInfo();
    }

    @GET
    @Path("/balances/{terminalGroupID}")
    @Produces(MediaType.APPLICATION_JSON)
    //Retrieves StoreBalanceModel models in a StoreBalance collection
    public List<StoreBalanceModel> getLoggedInEmployeeBalanceForTerminalGroup(@PathParam("terminalGroupID") Integer terminalGroupID, @Context HttpServletRequest request) throws Exception {
        StoreBalanceCollection storeBalanceCollection = new StoreBalanceCollection(terminalGroupID, request);
        return storeBalanceCollection.getCollection();
    }

    @GET
    @Path("/balances/global")
    @Produces(MediaType.APPLICATION_JSON)
    //Retrieves global balance information for the logged in employee
    public HashMap<String, Object> getLoggedInEmployeeGlobalBalanceInfo(@Context HttpServletRequest request) throws Exception {
        BalanceCollection balanceCollection = new BalanceCollection(request);
        return balanceCollection.getGlobalBalanceInfo();
    }

    //TODO: (HIGH) This end point is piggy backing off of the balances collection and model
    @GET
    @Path("/limits")
    @Produces(MediaType.APPLICATION_JSON)
    //Retrieves Balance models in a Balance collection
    public List<BalanceModel> getLoggedInEmployeeLimitsByTerminalGroup(@Context HttpServletRequest request) throws Exception {
        BalanceCollection balanceCollection = new BalanceCollection(request);
        return balanceCollection.getCollection();
    }

    //TODO: (HIGH) This end point is piggy backing off of the balances collection and model
    @GET
    @Path("/limits/{terminalGroupID}")
    @Produces(MediaType.APPLICATION_JSON)
    //Retrieves StoreBalanceModel models in a StoreBalance collection
    public List<StoreBalanceModel> getLoggedInEmployeeLimitByTerminalGroup(@PathParam("terminalGroupID") Integer terminalGroupID, @Context HttpServletRequest request) throws Exception {
        StoreBalanceCollection storeBalanceCollection = new StoreBalanceCollection(terminalGroupID, request);
        return storeBalanceCollection.getCollection();
    }

    //TODO: (HIGH) This end point is piggy backing off of the balances collection and model
    @GET
    @Path("/limits/global")
    @Produces(MediaType.APPLICATION_JSON)
    //Retrieves global limits information for the logged in employee
    public HashMap<String, Object> getLoggedInEmployeeGlobalLimitInfo(@Context HttpServletRequest request) throws Exception {
        BalanceCollection balanceCollection = new BalanceCollection(request);
        return balanceCollection.getGlobalBalanceInfo();
    }

    @GET
    @Path("/check/features")
    @Produces(MediaType.APPLICATION_JSON)
    //allows clients to check which features are available in this version of the API
    public List<FeatureModel>checkFeatures(@Context HttpServletRequest request) throws Exception {
        FeatureCollection featureCollection = new FeatureCollection(request);
        return featureCollection.getCollection();
    }

    @GET
    @Path("/check/tourFeatures")
    @Produces(MediaType.APPLICATION_JSON)
    //allows clients to check which features are available in this version of the API
    public List<FeatureModel> checkTourFeatures(@Context HttpServletRequest request) throws Exception {
        FeatureCollection featureCollection = new FeatureCollection(request, true);
        return featureCollection.getCollection();
    }

    @GET
    @Path("/check/validUser")
    @Produces(MediaType.APPLICATION_JSON)
    //allows clients to check which features are available in this version of the API
    public Integer validUser(@Context HttpServletRequest request) throws Exception {
        return CommonAuthResource.checkRequestHeaderForMMHayesAuth(request);
    }

    @GET
    @Path("/locationSettings")
    @Produces(MediaType.APPLICATION_JSON)
    //Retrieves account detail models in a account detail collection
    public LocationSettingsModel getLocationFilterSettings(@Context HttpServletRequest request) throws Exception {
        return new LocationSettingsModel();
    }

    @GET
    @Path("/orgLevels")
    @Produces(MediaType.APPLICATION_JSON)
    //Retrieves account detail models in a account detail collection
    public List<OrgLevelModel> getOrgLevels(@Context HttpServletRequest request) throws Exception {
        return new OrgLevelCollection(request).getCollection();
    }



    @GET
    @Path("/check/conn")
    @Produces(MediaType.APPLICATION_JSON)
    //checks if any connection to the server is available
    public String checkConnection(@Context HttpServletRequest request) throws Exception {
        return "Successful connection test!";
    }

    @POST
    @Path("/error/log")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    //logs all front end errors caught by window.onerror
    public void logError(HashMap errorHM, @Context HttpServletRequest request) throws Exception {
        Logger.logMessage("ERROR: Front end error occurred.", Logger.LEVEL.TRACE);
        Logger.logMessage("Error Message " + errorHM.get("msg").toString(), Logger.LEVEL.TRACE );

        String url = CommonAPI.convertModelDetailToString(errorHM.get("url"));
        if ( url != null && !url.equals("") ) {
            Logger.logMessage("Error URL " + url, Logger.LEVEL.TRACE );
        }

        String line = CommonAPI.convertModelDetailToString(errorHM.get("line"));
        if ( line != null && !line.equals("") ) {
            Logger.logMessage("Error Line " + line, Logger.LEVEL.TRACE );
        }

        String col = CommonAPI.convertModelDetailToString(errorHM.get("col"));
        if ( col != null && !col.equals("") ) {
            Logger.logMessage("Error Col " + col, Logger.LEVEL.TRACE );
        }

        String error = CommonAPI.convertModelDetailToString(errorHM.get("error"));
        if ( error != null && !error.equals("") ) {
            Logger.logMessage("Error " + error, Logger.LEVEL.TRACE );
        }
    }

    @POST
    @Path("/tos/send/{tosID}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public boolean sendTOSEmail(@Context HttpServletRequest request, @PathParam("tosID") Integer tosID){
        // SEND Email if configured
        MyQCEmailHelper.initialize(request);
        return MyQCEmailHelper.sendTOSEmail(tosID, request);
    }

    @GET
    @Path("/check/cache")
    @Produces(MediaType.APPLICATION_JSON)
    public HashMap checkCache(@Context HttpServletRequest request) throws Exception {
        CommonAuthResource.determineAuthDetailsFromAuthHeader(request);

        return MyQCCache.getCache();
    }

    @POST
    @Path("/qrcode")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    //Checks if the logged in employee has more than one spending profile and responds with a Boolean in an object
    public long generateQRCode(HashMap badgeSettings, @Context HttpServletRequest request) throws Exception {
        return CommonAPI.generateQRCodeBadgeNumber(badgeSettings, request);
    }

    @GET
    @Path("/inactivate/qrcode")
    @Produces(MediaType.APPLICATION_JSON)
    public Integer inactivateQRCodeBadgeNumbers(@Context HttpServletRequest request) throws Exception {
        return CommonAPI.inactivateQRCodeBadgeNumber(request);
    }

    @GET
    @Path("/order/token")
    @Produces(MediaType.APPLICATION_JSON)
    public Validation getCurrentOrderToken(@Context HttpServletRequest request) throws Exception {
        return MyQCCache.buildCurrentOrderToken(request);
    }

    @GET
    @Path("/login/faq")
    @Produces(MediaType.APPLICATION_JSON)
    public List<FAQModel> getLoginFAQs() throws Exception {
        FAQCollection faqCollection = new FAQCollection();
        return faqCollection.getCollection();
    }

    @GET
    @Path("/feedbackSettings")
    @Produces(MediaType.APPLICATION_JSON)
    public HashMap<String, String> getFeedbackSettings() throws Exception {
        return MyQCNavCollection.getFeedbackSettings();
    }

}

