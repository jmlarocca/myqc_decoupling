package com.mmhayes.myqc.server.api.resources;

//mmhayes dependencies

import com.mmhayes.common.transaction.models.TransactionModel;
import com.mmhayes.myqc.server.collections.*;
import com.mmhayes.myqc.server.models.*;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

//myqc dependencies
//javax.ws.rs dependencies
//other dependencies

/*
 Last Updated (automatically updated by SVN)
 $Author: ejwhitton $: Author of last commit
 $Date: 2021-09-02 11:15:25 -0400 (Thu, 02 Sep 2021) $: Date of last commit
 $Rev: 57854 $: Revision of last commit

 Notes: Resource for Online Ordering (ordering) specific requests
*/

//REST Resource for /{qc instance}/api/ordering/
@Path("ordering")
public class OrderingResource {

    @GET
    @Path("/stores")
    @Produces(MediaType.APPLICATION_JSON)
    //Retrieves Store models in a Store collection
    public List<StoreModel> getStores(@Context HttpServletRequest request) throws Exception {
        StoreCollection storeCollection = new StoreCollection(request);
        return storeCollection.getCollection();
    }

    @GET
    @Path("/stores/{futureOrderDate}")
    @Produces(MediaType.APPLICATION_JSON)
    //Retrieves Store models in a Store collection
    public List<StoreModel> getStoresByFutureDate(@PathParam("futureOrderDate") String futureOrderDate, @Context HttpServletRequest request) throws Exception {
        StoreCollection storeCollection = new StoreCollection(request, futureOrderDate);
        return storeCollection.getCollection();
    }

    @GET
    @Path("/stores/location/{locationId}/{futureOrderDate}")
    @Produces(MediaType.APPLICATION_JSON)
    //Retrieves Store models in a Store collection
    public List<StoreModel> getStores(@PathParam("locationId") String locationId, @PathParam("futureOrderDate") String futureOrderDate, @Context HttpServletRequest request) throws Exception {
        StoreCollection storeCollection = new StoreCollection(request, Integer.parseInt(locationId), futureOrderDate);
        return storeCollection.getCollection();
    }

    @GET
    @Path("/menu")
    @Produces(MediaType.APPLICATION_JSON)
    //Retrieves ALL Menu models (which contain MenuItemCollection which contain MenuItemModels) in Menu Collection
    public List<MenuModel> getAllMenus(@Context HttpServletRequest request) throws Exception {
        MenuCollection menuCollection = new MenuCollection(request);
        return menuCollection.getCollection();
    }

    @GET
    @Path("/menu/{PAKeypadID}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    //Retrieves a single Menu model (which contain MenuItemCollection which contain MenuItemModels)
    public MenuModel getMenuDetails(@PathParam("PAKeypadID") Integer menuID, @Context HttpServletRequest request) throws Exception {
        return new MenuModel(menuID, request);
    }

    @POST
    @Path("/menu/{PAKeypadID}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    //Retrieves a single Menu model (which contain MenuItemCollection which contain MenuItemModels)
    public MenuModel getMenu(@PathParam("PAKeypadID") Integer menuID, @Context HttpServletRequest request) throws Exception {
        return new MenuModel(menuID, request);
    }

    @GET
    @Path("/modmenu/{modDetailSetID}")
    @Produces(MediaType.APPLICATION_JSON)
    //Retrieves a single Mod Menu model (which contain ModifierCollection which contain ModifierModels)
    public ModMenuModel getModeMnu(@PathParam("modDetailSetID") Integer modDetailSetID, @Context HttpServletRequest request) {
        return new ModMenuModel(modDetailSetID, request);
    }

    /* Leave for reference on how to consume a JSON object - the trick was setting a "dummy constructor" on the ProductModel -jrmitaly - 2/16/2016
    @POST
    @Path("/product/{PAPluID}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    //Updates the passed in productModel - updates price based on modifiers
    public ProductModel updateProduct(@PathParam("PAPluID") Integer productID, ProductModel productModel, @Context HttpServletRequest request) {
        return productModel;
    }
    */

    @POST
    @Path("/product/{PAPluID}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    //Updates the passed in productModel - updates price based on modifiers
    public ProductModel updateProduct(@PathParam("PAPluID") Integer productID, ArrayList<ModifierModel> productModifiers, @Context HttpServletRequest request) throws Exception {
        ProductModel productModel = ProductModel.getProductModel(productID, request);
        productModel.updatePricesBasedOnModifiers(productModifiers, request);
        return productModel;
    }

    @GET
    @Path("/product/{PAPluID}")
    @Produces(MediaType.APPLICATION_JSON)
    //Retrieves a single Product model (which contain ModifierCollection which contain ModifierModels)
    public ProductModel getProduct(@PathParam("PAPluID") Integer productID, @Context HttpServletRequest request) throws Exception {
        return ProductModel.getProductModel(productID, request);
    }

    @POST
    @Path("/product/nutrition")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    //Retrieves the nutrition information and totals for a product and modifiers if they are present
    public NutritionModel getNutritionInfo(HashMap productInfo, @Context HttpServletRequest request) throws Exception {
        return new NutritionModel(productInfo, request);
    }

    @GET
    @Path("/product/prepOption/{prepOptionSetID}")
    @Produces(MediaType.APPLICATION_JSON)
    //Retrieves the prep option information from the product using the product or keypad button's prepOptionSetID
    public List<PrepOptionModel> getPrepOptionSet(@PathParam("prepOptionSetID") Integer prepOptionSetID, @Context HttpServletRequest request) throws Exception {
        PrepOptionCollection prepOptionCollection = new PrepOptionCollection(prepOptionSetID);
        return prepOptionCollection.getCollection();
    }


    @POST
    @Path("/cart/nutrition")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    //Retrieves the nutrition information and totals for a product and modifiers if they are present
    public NutritionModel getNutritionTotals(ArrayList<HashMap> productInfo, @Context HttpServletRequest request) throws Exception {
        return new NutritionModel(productInfo);
    }

    @POST
    @Path("/order/review")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    //Calculates and finalizes order details for customer review
    public OrderModel reviewOrder(OrderModel orderModel, @Context HttpServletRequest request) throws Exception{
        orderModel.reviewOrder(request);

        return orderModel;
    }

    @POST
    @Path("/order/submit")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    //Calculates and finalizes order details for customer review
    public OrderModel submitOrder(OrderModel orderModel, @Context HttpServletRequest request) throws Exception{
        orderModel.submitOrder(request);
        return orderModel;
    }

    @POST
    @Path("/order/inquire")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    //Calculates and finalizes order details for customer review
    public TransactionModel transactionInquiry(OrderModel orderModel, @Context HttpServletRequest request) throws Exception{
        TransactionModel transactionModel = orderModel.transactionInquiry(request);
        return transactionModel;
    }

    @GET
    @Path("/cart/{transactionID}")
    @Produces(MediaType.APPLICATION_JSON)
    //Builds a cartModel based on the given transactionID, for ordering again
    public CartModel orderAgain(@PathParam("transactionID") Integer transactionID, @Context HttpServletRequest request) throws Exception {
        CartModel cartModel = new CartModel(transactionID, request);
        return cartModel;
    }

    @GET
    @Path("/express/cart/{transactionID}")
    @Produces(MediaType.APPLICATION_JSON)
    //Builds a cartModel based on the given transactionID, for ordering again
    public CartModel expressOrderAgain(@PathParam("transactionID") Integer transactionID, @Context HttpServletRequest request) throws Exception {
        CartModel cartModel = new CartModel(transactionID, true, request);
        return cartModel;
    }

    @GET
    @Path("/buildTimePicker/{storeID}/{orderType}")
    @Produces(MediaType.APPLICATION_JSON)
    //Builds a cartModel based on the given transactionID, for ordering again
    public TimePickerModel buildTimePicker(@PathParam("storeID") Integer storeID, @PathParam("orderType") String orderType, @Context HttpServletRequest request) throws Exception {
        StoreModel storeModel = StoreModel.getStoreModel(storeID, request);
        TimePickerModel timePickerModel = new TimePickerModel(storeModel, orderType);
        return timePickerModel;
    }

    @GET
    @Path("/store/{storeID}")
    @Produces(MediaType.APPLICATION_JSON)
    //Builds a cartModel based on the given transactionID, for ordering again
    public StoreModel getTerminalInformation(@PathParam("storeID") Integer storeID, @Context HttpServletRequest request) throws Exception {
        return new StoreModel(storeID, request);
    }

    @POST
    @Path("/buildTimePicker")
    @Produces(MediaType.APPLICATION_JSON)
    //Builds a cartModel based on the given transactionID, for ordering again
    public TimePickerModel buildTimePickerByFutureOrderDate(HashMap storeDetailsHM, @PathParam("orderType") String orderType, @Context HttpServletRequest request) throws Exception {
        return new TimePickerModel(storeDetailsHM, request);
    }

    @POST
    @Path("/dining/option")
    @Produces(MediaType.APPLICATION_JSON)
    //Gets relationship options for selector on 'Add Account to Manage' page
    public ArrayList<HashMap> getDiningOptionsList(@Context HttpServletRequest request) throws Exception {
        return OrderModel.getDiningOptions(request);
    }

    @POST
    @Path("/express/reorder")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    //Calculates and finalizes order details for customer review
    public OrderModel buildExpressReorder(OrderModel orderModel, @Context HttpServletRequest request) throws Exception{
        orderModel.expressReorder(request);
        return orderModel;
    }

    @GET
    @Path("/express/orders/{storeIDs}")
    @Produces(MediaType.APPLICATION_JSON)
    //Returns the top 5 most recent transaction this account made at all stores
    public List<ExpressOrderModel> get5MostRecentTransactionsFromStores(@PathParam("storeIDs") String storeIDs, @Context HttpServletRequest request) throws Exception {
        ExpressOrderCollection expressOrderCollection = ExpressOrderCollection.getExpressReorders(storeIDs, request);
        return expressOrderCollection.getCollection();
    }

    @GET
    @Path("/suggestive/selling/{keypadID}")
    @Produces(MediaType.APPLICATION_JSON)
    //Returns the top 5 most recent transaction this account made at all stores
    public List<MenuModel> loadSuggestiveSellingKeypad(@PathParam("keypadID") String keypadID, @Context HttpServletRequest request) throws Exception {
        SuggestiveSellingCollection suggestiveSellingCollection = new SuggestiveSellingCollection(keypadID, request);
        return suggestiveSellingCollection.getCollection();
    }

    @GET
    @Path("/combo/{comboID}")
    @Produces(MediaType.APPLICATION_JSON)
    //Retrieves a single Product model (which contain ModifierCollection which contain ModifierModels)
    public ComboModel getCombo(@PathParam("comboID") Integer comboID, @Context HttpServletRequest request) throws Exception {
        return new ComboModel(comboID);
    }

    @POST
    @Path("/donation/submit")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    //Calculates and finalizes order details for customer review
    public OrderModel submitDonation(OrderModel orderModel, @Context HttpServletRequest request) throws Exception{
        orderModel.submitDonation(request);
        return orderModel;
    }

    @POST
    @Path("/dollarDonation/submit")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    //Calculates and finalizes order details for customer review (dollar donations in balances view)
    public OrderModel submitDollarDonation(OrderModel orderModel, @Context HttpServletRequest request) throws Exception{
        orderModel.submitDollarDonation(request);
        return orderModel;
    }

    @GET
    @Path("/product/code/{PLUCode}")
    @Produces(MediaType.APPLICATION_JSON)
    //Retrieves a single Product model (which contain ModifierCollection which contain ModifierModels)
    public ProductModel getProduct(@PathParam("PLUCode") String PLUCode, @Context HttpServletRequest request) throws Exception {
        return new ProductModel(PLUCode, request);
    }

    @GET
    @Path("/product/{productID}/default/modifiers")
    @Produces(MediaType.APPLICATION_JSON)
    // Retrieves the default modifiers for a product with the given ID
    public List<ModifierModel> getDefaultModifiersForProduct (@PathParam("productID") Integer productID, @Context HttpServletRequest request) throws Exception {
        ModifierCollection modifierCollection = new ModifierCollection(productID, request);
        return modifierCollection.getCollection();
    }

    @GET
    @Path("/product/{productID}/keypad/{keypadID}/default/prepOptions")
    @Produces(MediaType.APPLICATION_JSON)
    // Retrieves the default prep option for a product as well as the product's default modifiers
    public List<PrepOptionModel> getDefaultPrepOptionsForProduct (@PathParam("productID") Integer productID, @PathParam("keypadID") Integer keypadID, @Context HttpServletRequest request) throws Exception {
        PrepOptionCollection prepOptionCollection = new PrepOptionCollection(productID, keypadID);
        return prepOptionCollection.getCollection();
    }

}
