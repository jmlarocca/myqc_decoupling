package com.mmhayes.myqc.server.api.resources;

//mmhayes dependencies
import com.mmhayes.myqc.server.models.*;
import com.mmhayes.common.api.errorhandling.exceptions.InvalidAuthException;
import com.mmhayes.common.utils.*;
import com.mmhayes.common.api.*;

//myqc dependencies
import com.mmhayes.myqc.server.models.*;
import com.mmhayes.myqc.server.collections.*;

//javax.ws.rs dependencies
import javax.ws.rs.*;
import javax.ws.rs.core.*;

//stripe dependencies
import com.stripe.Stripe;
import com.stripe.model.Charge;

//other dependencies
import javax.servlet.http.HttpServletRequest;
import java.util.*;


/*
 Last Updated (automatically updated by SVN)
 $Author: jrmitaly $: Author of last commit
 $Date: 2017-01-24 17:39:55 -0500 (Tue, 24 Jan 2017) $: Date of last commit
 $Rev: 20286 $: Revision of last commit

 Notes: Resource for Recharge Account (Account Funding) specific requests
*/

//REST Resource for /{qc instance}/api/recharge/
@Path("recharge")
public class RechargeResource {

    @POST
    @Path("/test")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    //TODO: ONLY FOR PROOF OF CONCEPT - remove once proven
    public HashMap updateProduct(String stripeSingleUseToken, @Context HttpServletRequest request) throws Exception {
        //TODO: ONLY FOR PROOF OF CONCEPT - remove once proven
        HashMap response = new HashMap<String,String>();
        if (stripeSingleUseToken != null) {

            //make a request to Stripe API to charge the users carge
            Stripe.apiKey = "sk_test_k9yCO9KZ4M7pgiAKSpoZTgID";
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("amount", 1000);
            params.put("currency", "usd");
            params.put("description", "Example charge");
            params.put("source", stripeSingleUseToken);

            Charge charge = Charge.create(params);

            response.put("status", "PASS");
            response.put("responseCode", "200");
            response.put("responseMessage", "successfully found token and charged");
        } else {
            response.put("status", "FAIL");
            response.put("responseCode", "500");
            response.put("responseMessage", "failed could not find token or failed to charge");
        }
        return response;
    }

}
