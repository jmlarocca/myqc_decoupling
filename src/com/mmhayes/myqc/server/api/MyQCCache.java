package com.mmhayes.myqc.server.api;

//mmhayes dependencies

import com.mmhayes.common.api.CommonAuthResource;
import com.mmhayes.common.api.PosApi30.PosAPIModelCache;
import com.mmhayes.common.api.Validation;
import com.mmhayes.common.login.LoginSettingsModel;
import com.mmhayes.common.transaction.models.ModifierLineItemModel;
import com.mmhayes.common.transaction.models.ProductLineItemModel;
import com.mmhayes.common.transaction.models.TransactionModel;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.myqc.server.collections.ExpressOrderCollection;
import com.mmhayes.myqc.server.models.*;

import javax.servlet.http.HttpServletRequest;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;

//javax.ws.rs dependencies
//other dependencies

/*
 Last Updated (automatically updated by SVN)
 $Author: ijgerstein $: Author of last commit
 $Date: 2021-09-28 10:43:14 -0400 (Tue, 28 Sep 2021) $: Date of last commit
 $Rev: 58300 $: Revision of last commit

Notes: This client is for instances authenticating against the gateway
*/

public class MyQCCache {
    private static final long REFRESH_MODEL_CACHE_SECONDS = 300;
    private static final long REFRESH_SETTINGS_CACHE_SECONDS = 300;
    private static final long TRANSACTION_CACHE_SETTING_MINUTES = 30;
    private static HashMap<Integer, StoreModel> storeCache = new HashMap<Integer, StoreModel>();
    private static HashMap<Integer, ScheduleModel> scheduleCache = new HashMap<Integer, ScheduleModel>();
    private static LoginSettingsModel loginSettingsCache = null;
    private static LocalDateTime loginSettingsLastCheck = null;
    private static HashMap<Integer, StoreKeypadModel> keypadCache = new HashMap<Integer, StoreKeypadModel>();
    private static HashMap<Integer, ExpressOrderCollection> expressOrderCache = new HashMap<Integer, ExpressOrderCollection>();
    private static HashMap<Integer, HashMap<String, TransactionCacheModel>> transactionCache = new
            HashMap<Integer, HashMap<String, TransactionCacheModel>>();
    private static HashMap<Integer, ProductModel> productCache = new HashMap<Integer, ProductModel>();

    public static HashMap getCache() {
        HashMap<String, Object> cacheHM = new HashMap();

        cacheHM.put( "StoreCache", getStoreCache() );
        cacheHM.put( "ScheduleCache", getScheduleCache() );
        cacheHM.put( "LoginSettings", getLoginSettingsCache() );
        cacheHM.put( "KeypadCache", getKeypadCache() );
        cacheHM.put( "ExpressReorderCache", getExpressReorderCache() );
        cacheHM.put( "ProductCache", getProductCache() );

        return cacheHM;
    }

    public static HashMap<Integer, StoreModel> getStoreCache() {
        return storeCache;
    }

    public static void setStoreCache(HashMap<Integer, StoreModel> storeCache) {
        MyQCCache.storeCache = storeCache;
    }

    public static HashMap<Integer, ScheduleModel> getScheduleCache() {
        return scheduleCache;
    }

    public static void setScheduleCache(HashMap<Integer, ScheduleModel> scheduleCache) {
        MyQCCache.scheduleCache = scheduleCache;
    }

    public static HashMap<Integer, StoreKeypadModel> getKeypadCache() {
        return keypadCache;
    }

    public static void setKeypadCache(HashMap<Integer, StoreKeypadModel> keypadCache) {
        MyQCCache.keypadCache = keypadCache;
    }

    //BEGIN STORE CACHE
    public static boolean checkCacheForStore(Integer storeId) throws Exception {
        boolean check = storeCache.containsKey(storeId);

        if ( !check ) {
            Logger.logMessage("Store " + storeId + " does not exist in storeCache", Logger.LEVEL.DEBUG);
        } else {
            Logger.logMessage("Store " + storeId + " exists in storeCache", Logger.LEVEL.DEBUG);

            //if now > REFRESH_MODEL_CACHE_SECONDS + lastUpdateCheck, check the lastUpdateDTM for the store
            LocalDateTime now = LocalDateTime.now();
            StoreModel storeModel = storeCache.get(storeId);

            if ( now.isAfter( storeModel.getLastUpdateCheck().plusSeconds( REFRESH_MODEL_CACHE_SECONDS ) ) ) {
                Logger.logMessage("Store" + storeId + " needs to be checked for updates in cache", Logger.LEVEL.DEBUG);

                //if the lastUpdateDTM (in db) is after lastUpdateDTM (cached), then return false
                if ( storeModel.checkForUpdates() ) {
                    Logger.logMessage("Store" + storeId + " needs to be updated, returning failed cache check", Logger.LEVEL.DEBUG);
                    return false;
                } else {
                    Logger.logMessage("Store" + storeId + " does not need to be updated, returning successful cache check", Logger.LEVEL.DEBUG);
                    storeModel.setLastUpdateCheck( now );
                }
            }
        }

        return check;
    }

    public static StoreModel getStoreFromCache(Integer storeId, boolean forMenuItems) throws Exception {
        StoreModel storeModel = storeCache.get( storeId );

        return storeModel;
    }

    public static StoreModel getStoreFromCache(Integer storeId, LocalDate futureOrderDate) throws Exception {
        StoreModel storeModel = storeCache.get( storeId );

        //set the future order date if there is one for the current store time
        if( futureOrderDate != null ) {
            storeModel.setFutureOrderDate(futureOrderDate);
        }

        //need to update the store's current time
        storeModel.setCurrentTime();

        //update the current schedules
        storeModel.determineAndSetStoreOrderingAvailability();

        //if the store is using buffering, check the buffering schedule
        if ( storeModel.getUsingBuffering() ) {
            storeModel.setBuffering();
        }

        //update the wait time for each store
        storeModel.setWaitTimes();

        return storeModel;
    }

    //abstract getStoreFromCache to pass in futureOrderDate
    public static StoreModel getStoreFromCache(Integer storeId) throws Exception {
        return getStoreFromCache(storeId, null);
    }

    public static void addStoreToCache(StoreModel storeModel) throws Exception {
        if ( storeModel == null ) {
            Logger.logMessage("StoreModel sent to cache was null", Logger.LEVEL.ERROR);
            return;
        }

        Integer storeId = storeModel.getID();
        Logger.logMessage("Adding store " + storeModel.getHeader1() + "(" + storeId + ") to storeCache", Logger.LEVEL.DEBUG);

        MyQCCache.storeCache.put(storeId, storeModel);
    }

    //express reorder cache needs to be updated ifa menu keypad, home keypad or dining option keypad was changed
    public static void storeKeypadPopularProductsNeedsUpdate(Integer storeID) throws Exception {
        StoreKeypadModel storeKeypadModel = MyQCCache.getStoreKeypadFromCache(storeID);
        storeKeypadModel.setPopularProductsNeedsUpdate(true);
    }

    //END STORE CACHE

    //BEGIN SCHEDULE CACHE

    public static boolean checkCacheForSchedule(Integer scheduleId) throws Exception {
        boolean check = scheduleCache.containsKey(scheduleId);

        if ( !check ) {
            Logger.logMessage("Schedule " + scheduleId + " does not exist in scheduleCache", Logger.LEVEL.DEBUG);
        } else {
            Logger.logMessage("Schedule " + scheduleId + " exists in scheduleCache", Logger.LEVEL.DEBUG);

            //if now > REFRESH_MODEL_CACHE_SECONDS + lastUpdateCheck, check the lastUpdateDTM for the store
            LocalDateTime now = LocalDateTime.now();
            ScheduleModel scheduleModel = scheduleCache.get( scheduleId );

            if ( now.isAfter( scheduleModel.getLastUpdateCheck().plusSeconds( REFRESH_MODEL_CACHE_SECONDS ) ) ) {
                Logger.logMessage("Schedule " + scheduleId + " needs to be checked for updates in cache", Logger.LEVEL.DEBUG);

                //if the lastUpdateDTM (in db) is after lastUpdateDTM (cached), then return false
                if ( scheduleModel.checkForUpdates() ) {
                    Logger.logMessage("Schedule " + scheduleId + " needs to be updated, returning failed cache check", Logger.LEVEL.DEBUG);
                    return false;
                } else {
                    Logger.logMessage("Schedule " + scheduleId + " does not need to be updated, returning successful cache check", Logger.LEVEL.DEBUG);
                    scheduleModel.setLastUpdateCheck( now );
                }
            }
        }

        return check;
    }

    public static ScheduleModel getScheduleFromCache(Integer scheduleId) {
        ScheduleModel scheduleModel = scheduleCache.get( scheduleId );

        return scheduleModel;
    }

    public static void addScheduleToCache(ScheduleModel scheduleModel) throws Exception {
        if ( scheduleModel == null ) {
            Logger.logMessage("ScheduleModel sent to cache was null", Logger.LEVEL.ERROR);
            return;
        }

        Integer scheduleId = scheduleModel.getID();
        Logger.logMessage("Adding schedule " + scheduleModel.getName() + "(" + scheduleId + ") to scheduleCache", Logger.LEVEL.DEBUG);

        MyQCCache.scheduleCache.put(scheduleId, scheduleModel);
    }

    //END SCHEDULE CACHE

    //START LOGIN SETTINGS CACHE

    public static boolean checkCacheForLoginSettings() {
        boolean check = ( getLoginSettingsCache() != null );

        if ( !check ) {
            Logger.logMessage("Login Settings are not set in cache", Logger.LEVEL.DEBUG);
        } else {
            Logger.logMessage("Login Settings are set in cache", Logger.LEVEL.DEBUG);

            //if there is no reference time, return false to refresh the settings
            if ( getLoginSettingsLastCheck() == null ) {
                return false;
            }

            LocalDateTime now = LocalDateTime.now();

            if ( now.isAfter( getLoginSettingsLastCheck().plusSeconds( REFRESH_SETTINGS_CACHE_SECONDS ) ) ) {
                Logger.logMessage("Login Settings need to be updated in cache", Logger.LEVEL.DEBUG);
                return false;
            }
        }

        return check;
    }

    public static LoginSettingsModel getLoginSettingsCache() {
        return loginSettingsCache;
    }

    public static void setLoginSettingsCache(LoginSettingsModel loginSettingsCache) {
        setLoginSettingsLastCheck( LocalDateTime.now() );
        MyQCCache.loginSettingsCache = loginSettingsCache;
    }

    public static LocalDateTime getLoginSettingsLastCheck() {
        return loginSettingsLastCheck;
    }

    public static void setLoginSettingsLastCheck(LocalDateTime loginSettingsLastCheck) {
        MyQCCache.loginSettingsLastCheck = loginSettingsLastCheck;
    }

    //END LOGIN SETTINGS CACHE

    //START KEYPAD CACHE

    public static boolean checkCacheForStoreKeypad(Integer storeID) throws Exception {
        boolean check = keypadCache.containsKey(storeID);

        if ( !check ) {
            Logger.logMessage("Keypad Cache does not exist for StoreID " + storeID + " in keypadCache", Logger.LEVEL.DEBUG);
        } else {
            Logger.logMessage("Keypad Cache exists for StoreID " + storeID + " keypadCache", Logger.LEVEL.DEBUG);

            //if now > REFRESH_MODEL_CACHE_SECONDS + lastUpdateCheck, check the lastUpdateDTM for the store
            LocalDateTime now = LocalDateTime.now();
            StoreKeypadModel storeKeypadModel = keypadCache.get( storeID );

            if(storeKeypadModel.getLastUpdateCheck() == null) {
                return false;
            }

            if ( now.isAfter( storeKeypadModel.getLastUpdateCheck().plusSeconds( REFRESH_MODEL_CACHE_SECONDS ) ) ) {
                Logger.logMessage("Keypad Cache needs to be checked for updates in cache for StoreID " + storeID, Logger.LEVEL.DEBUG);

                //if the lastUpdateDTM (in db) is after lastUpdateDTM (cached), then return false
                if ( storeKeypadModel.checkForUpdates() ) {
                    PosAPIModelCache.buildRotationCache(storeID, true);
                    Logger.logMessage("Keypad Cache needs to be updated, returning failed cache check for StoreID " + storeID, Logger.LEVEL.DEBUG);
                    return false;
                } else {
                    Logger.logMessage("Keypad Cache does not need to be updated, returning successful cache check for StoreID " + storeID, Logger.LEVEL.DEBUG);
                    storeKeypadModel.setLastUpdateCheck( now );
                }
            }
        }

        return check;
    }

    public static StoreKeypadModel getStoreKeypadFromCache(Integer storeId) throws Exception {
        return keypadCache.get( storeId );
    }

    public static void addStoreKeypadToCache(StoreKeypadModel storeKeypadModel) throws Exception {
        if ( storeKeypadModel == null ) {
            Logger.logMessage("StoreKeypadModel sent to cache was null", Logger.LEVEL.ERROR);
            return;
        }

        Integer storeID = storeKeypadModel.getID();
        Logger.logMessage("Adding keypad model cache for store ID " + storeID + " to keypadCache", Logger.LEVEL.DEBUG);

        MyQCCache.keypadCache.put(storeID, storeKeypadModel);
    }

    // if store keypad model is not in cache OR
    // the store keypad model is in the cache but the home keypad of the store was updated OR
    // the store keypad model is in the cache and there is a dining option keypad on the store model and the store model's dining option keypad doesnt match the cache OR
    // the store keypad model is in the cache but a rotating keypad has changed,
    // then update store keypad model
    public static boolean checkIfStoreKeypadCacheNeedsUpdate(Integer storeId) throws Exception {
        boolean storeKeypadInCache = MyQCCache.checkCacheForStoreKeypad( storeId );

        StoreModel storeModel = MyQCCache.getStoreFromCache(storeId);
        StoreKeypadModel storeKeypadModel = getStoreKeypadFromCache(storeId);

        if(!storeKeypadInCache || (storeKeypadModel != null && !storeKeypadModel.getHomeKeypadID().equals(storeModel.getHomeKeypadID()))) {
           return true;

        //if the store keypad exists in the cache AND ( (store keypad's dining option isn't null and the store model's dining option isn't null and the store keypads dining option doesn't equal the store model's dining option keypad) OR (store keypad's dining option equals null and the store model's dining option doesn't equal null)  OR (store keypad's dining option doesn't equal null and the store model's dining options equals null))... then update cache
        } else if ( storeKeypadModel != null && ((storeKeypadModel.getDiningOptionKeypadID() != null && storeModel.getDiningOptionKeypadID() != null && !storeKeypadModel.getDiningOptionKeypadID().equals(storeModel.getDiningOptionKeypadID()) || (storeKeypadModel.getDiningOptionKeypadID() == null  && storeModel.getDiningOptionKeypadID() != null) || (storeKeypadModel.getDiningOptionKeypadID() != null  && storeModel.getDiningOptionKeypadID() == null) )) ) {
            return true;

        // If the store keypad exists in the cache but a rotating keypad has changed, then update cache
        } else if (storeKeypadModel != null && storeKeypadModel.checkForRotationUpdates(storeModel.getCurrentStoreDTM())) {
            return true;
        }

        return false;
    }

    //END KEYPAD CACHE

    //START EXPRESS REORDER CACHE

    public static HashMap<Integer, HashMap> getExpressReorderCache() {
        HashMap<Integer, HashMap> newExpressReorderCache = new HashMap<>();

        //have to pass back as a HashMap, ExpressReorderCollection class was causing serialization error
        for (Map.Entry<Integer, ExpressOrderCollection> entry : expressOrderCache.entrySet()) {
            ExpressOrderCollection value = entry.getValue();
            HashMap expressHM = new HashMap();
            expressHM.put("collection", value.getCollection());
            expressHM.put("storeIDs", value.getStoreIDs());
            expressHM.put("needsUpdate", value.getNeedsUpdate());
            expressHM.put("transactionProductsByStore", value.getTransactionProductsByStore());
            newExpressReorderCache.put(value.getLoggedInEmployeeID(), expressHM);
        }

        return newExpressReorderCache;
    }

    //get the express reorder cache based on employeeID
    public static ExpressOrderCollection getExpressOrderFromCache(Integer employeeID) throws Exception {
        return expressOrderCache.get( employeeID );
    }

    //add the express reorder collection to the cache based on employeeID
    public static void addExpressReordersToCache(ExpressOrderCollection expressOrderCollection) throws Exception {
        if ( expressOrderCollection == null ) {
            Logger.logMessage("Express Order Collection sent to cache was null", Logger.LEVEL.ERROR);
            return;
        }

        Integer employeeID = expressOrderCollection.getLoggedInEmployeeID();
        Logger.logMessage("Adding express order collection for employee ID " + employeeID + " to expressReorderCache", Logger.LEVEL.DEBUG);

        MyQCCache.expressOrderCache.put(employeeID, expressOrderCollection);
    }

    //return the express reorder cache based on the employeeID
    public static boolean checkCacheForExpressReorders(Integer employeeID) throws Exception {
        return expressOrderCache.containsKey( employeeID );
    }

    //check if the express order needs to be updated if the user placed a transaction
    public static boolean checkExpressReorderCacheNeedsUpdate(Integer employeeID) throws Exception {

        ExpressOrderCollection expressOrderCollection = getExpressOrderFromCache( employeeID );

        if(expressOrderCollection != null && expressOrderCollection.getNeedsUpdate()) {
            Logger.logMessage("Express Reorder Cache needs to be updated, returning failed cache check", Logger.LEVEL.DEBUG);
            return true;
        }

        Logger.logMessage("Express Reorder Cache doesn't need to be updated, returning successful cache check", Logger.LEVEL.DEBUG);
        return false;
    }

    //check that the products in the transaction are still active, have inventory, etc. since we don't have LastUpdateDTM on product
    public static boolean checkProductsInTransaction(Integer employeeID) throws Exception {
        ExpressOrderCollection expressOrderCollection = getExpressOrderFromCache( employeeID );

        if(expressOrderCollection != null && expressOrderCollection.checkTransactionProducts()) {
            Logger.logMessage("Express Reorder Cache doesn't need to be updated, products are all valid, returning successful cache check", Logger.LEVEL.DEBUG);
            return true;
        }

        Logger.logMessage("Express Reorder Cache needs to be updated, products are invalid, returning failed cache check", Logger.LEVEL.DEBUG);
        return false;
    }

    //express reorder cache needs to be updated ifa menu keypad, home keypad or dining option keypad was changed
    public static void expressReorderCacheNeedsUpdate(Integer employeeID) throws Exception {
        ExpressOrderCollection expressOrderCollection = MyQCCache.getExpressOrderFromCache(employeeID);
        expressOrderCollection.setNeedsUpdate(true);
    }

    //if express orders exists in cache AND cache doesn't need update from placing a transaction AND the products in the transaction are still valid (active, have inventory, etc)
    public static boolean checkIfExpressReordersAreUpToDate(Integer employeeID, String storeIDs) throws Exception {
        if( !(MyQCCache.checkCacheForExpressReorders(employeeID) && !MyQCCache.checkExpressReorderCacheNeedsUpdate(employeeID) && MyQCCache.checkProductsInTransaction(employeeID) )) {
            return false;
        }

        ExpressOrderCollection expressOrderCollection = getExpressOrderFromCache( employeeID );
        List<String> storeIDList = new ArrayList<String>(Arrays.asList(storeIDs.split(",")));

        for ( Map.Entry<Integer, ArrayList<String>> entry: expressOrderCollection.getTransactionProductsByStore().entrySet() ) {
            Integer storeID = entry.getKey();

            //check if the store is even in the list of available stores
            if( !storeIDList.contains(storeID.toString()) ) {
                return false;
            }

            // Check if the store keypad cache has been updated since the last express reorder cache update
            StoreKeypadModel storeKeypadModel = getStoreKeypadFromCache(storeID);
            if (storeKeypadModel == null || !storeKeypadModel.getLastCacheUpdate().isBefore(expressOrderCollection.getLastCacheUpdate())) {
                return false;
            }

            //check if the store keypad cache needs update for the transactions in the express reorder collection
            if(MyQCCache.checkIfStoreKeypadCacheNeedsUpdate(storeID)) {
                return false;
            }
        }

        return true;
    }

    //sets the express reorder as a favorite order in the cache when it's created from the Select Store page
    public static void createExpressFavoriteOrderInCache(Integer employeeID, Integer transactionID, String favoriteOrderName, Integer favoriteOrderID) throws Exception {

        ExpressOrderCollection expressOrderCollection = getExpressOrderFromCache(employeeID);

        if(expressOrderCollection != null) {

            for(ExpressOrderModel expressOrderModel : expressOrderCollection.getCollection()) {

                if( !expressOrderModel.getID().equals(transactionID) ) {
                    continue;
                }

                expressOrderModel.setFavoriteOrderName(favoriteOrderName);
                expressOrderModel.setFavoriteOrderID(favoriteOrderID);
                expressOrderModel.setFavoriteOrderActive(true);

                break;
            }
        }
    }

    //update the favorite order status of an express reorder if it is changed on the Select Store page
    public static void updateExpressFavoriteOrderInCache(Integer employeeID, Integer favoriteOrderID, boolean status) throws Exception {
        if(employeeID != null && checkCacheForExpressReorders(employeeID)) {

            ExpressOrderCollection expressOrderCollection = getExpressOrderFromCache(employeeID);

            for(ExpressOrderModel expressOrderModel : expressOrderCollection.getCollection()) {

                if( !(expressOrderModel.getFavoriteOrderID() != null && expressOrderModel.getFavoriteOrderID().equals(favoriteOrderID)) ) {
                    continue;
                }

                expressOrderModel.setFavoriteOrderActive(status);
                break;
            }
        }
    }

    //update the favorite order status of an express reorder if it is changed on the Select Store page
    public static void updateExpressFavoriteOrderNameInCache(Integer employeeID, Integer favoriteOrderID, String name) throws Exception {
        if(employeeID != null && checkCacheForExpressReorders(employeeID)) {

            ExpressOrderCollection expressOrderCollection = getExpressOrderFromCache(employeeID);

            for(ExpressOrderModel expressOrderModel : expressOrderCollection.getCollection()) {

                if( !(expressOrderModel.getFavoriteOrderID() != null && expressOrderModel.getFavoriteOrderID().equals(favoriteOrderID)) ) {
                    continue;
                }

                expressOrderModel.setFavoriteOrderName(name);
                break;
            }
        }
    }

    //END EXPRESS REORDER CACHE

    //START TRANSACTION CACHE

    public static HashMap<Integer, HashMap<String, TransactionCacheModel>> getTransactionCache() {
        return transactionCache;
    }

    public static HashMap<String, TransactionCacheModel> getEmployeeTransactionsFromCache(Integer employeeID) throws Exception {
        return transactionCache.get( employeeID );
    }

    // Generate a random token to correspond to the user's transaction
    public static Validation buildCurrentOrderToken(HttpServletRequest request) throws Exception {
        // Can't pass back token as string, use Validation object to send back token
        Validation validation = new Validation();

        Integer employeeID = CommonAuthResource.checkRequestHeaderForMMHayesAuth(request);

        Instant now = Instant.now();
        String orderToken = new BigInteger(80, new SecureRandom())+BigInteger.valueOf(now.getEpochSecond()).subtract
                (new BigInteger(8, new SecureRandom())).toString(64);

        validation.setDSKey(orderToken);

        HashMap<String, TransactionCacheModel> employeeTransactions = getEmployeeTransactionsFromCache( employeeID );

        // If the employee hasn't made any transactions yet the token is valid
        if( employeeTransactions == null || employeeTransactions.size() == 0 ) {
            return validation;
        }

        // If the token doesn't exist in the employee's transactions the token is valid
        if(!employeeTransactions.containsKey(orderToken)) {
            return validation;
        }

        boolean validToken = false;
        int loopCount = 0;

        // We only want to enter while loop if the first token created is a duplicate
        while (!validToken) {
            orderToken = new BigInteger(130, new SecureRandom())+BigInteger.valueOf(now.getEpochSecond()).subtract
                    (new BigInteger(8, new SecureRandom())).toString(64);

            if(!employeeTransactions.containsKey(orderToken)) {
                validToken = true;
            }

            if(loopCount > 50) {
                validation.setDSKey(orderToken);
            }

            loopCount++;
        }

        return validation;
    }


    //add the transaction to the cache based on employeeID
    public static void addTransactionToCache(Integer employeeID, String token, TransactionModel transactionModel)
            throws Exception {
        if ( transactionModel == null ) {
            Logger.logMessage("Transaction Cache Model sent to cache was null", Logger.LEVEL.ERROR);
            return;
        }

        Logger.logMessage("Adding transaction cache model for employee ID " + employeeID + " to transactionCache",
                Logger.LEVEL.DEBUG);

        TransactionCacheModel transactionCacheModel = new TransactionCacheModel();

        transactionCacheModel.setID(transactionModel.getId());
        transactionCacheModel.setTimeStamp(transactionModel.getTimeStamp());
        transactionCacheModel.setOrderNumber(transactionModel.getOrderNumber());
        transactionCacheModel.setTimeCreated(LocalDateTime.now());

        HashMap<String, TransactionCacheModel> employeeTransactions = getEmployeeTransactionsFromCache( employeeID );

        if( employeeTransactions == null || employeeTransactions.size() == 0 ) {
            employeeTransactions = new HashMap<>();
        }

        employeeTransactions.put(token, transactionCacheModel);

        MyQCCache.transactionCache.put(employeeID, employeeTransactions);

        cleanUpTransactionCache(employeeID);
    }

    // Check if the transaction is in the cache based on their order number
    public static TransactionCacheModel checkTransactionInCache(Integer employeeID, String token) throws
            Exception {

        HashMap<String, TransactionCacheModel> employeeTransactions = getEmployeeTransactionsFromCache( employeeID );

        if( employeeTransactions == null || employeeTransactions.size() == 0 ) {
            return null;
        }

        if(employeeTransactions.containsKey(token)) {
            return employeeTransactions.get(token);
        }

        return null;
    }

    // Check if the transaction is in the cache based on their order number
    public static void cleanUpTransactionCache(Integer employeeID) throws Exception {

        HashMap<String, TransactionCacheModel> employeeTransactions = getEmployeeTransactionsFromCache( employeeID );

        if( employeeTransactions == null || employeeTransactions.size() == 0 ) {
            return;
        }

        ArrayList<String> transactionsToRemove = new ArrayList<>();

        for ( HashMap.Entry<String, TransactionCacheModel> entry : employeeTransactions.entrySet() ) {
            TransactionCacheModel transactionCacheModel = entry.getValue();
            LocalDateTime now = LocalDateTime.now();

            if(transactionCacheModel.getTimeCreated().until(now, ChronoUnit.DAYS) > 30) {
                transactionsToRemove.add(entry.getKey());
            }
        }

        for(String token : transactionsToRemove) {
            employeeTransactions.remove(token);
        }
    }

    //END TRANSACTION CACHE

    //BEGIN PRODUCT CACHE

    public static HashMap<Integer, ProductModel> getProductCache() {
        return productCache;
    }

    public static void setProductCache(HashMap<Integer, ProductModel> productCache) {
        MyQCCache.productCache = productCache;
    }

    public static void addProductToCache(ProductModel productModel) throws Exception {
        if ( productModel == null ) {
            Logger.logMessage("ProductModel sent to cache was null", Logger.LEVEL.ERROR);
            return;
        }

        Integer productId = productModel.getID();
        Logger.logMessage("Adding product " + productModel.getName() + "(" + productId + ") to productCache", Logger.LEVEL.DEBUG);

        MyQCCache.productCache.put(productId, productModel);
    }

    public static boolean checkCacheForProduct(Integer productId) throws Exception {
        boolean check = productCache.containsKey(productId);

        if ( !check ) {
            Logger.logMessage("Product " + productId + " does not exist in productCache", Logger.LEVEL.DEBUG);
        } else {
            Logger.logMessage("Product " + productId + " exists in productCache", Logger.LEVEL.DEBUG);

            //if now > REFRESH_MODEL_CACHE_SECONDS + lastUpdateCheck, check the lastUpdateDTM for the store
            LocalDateTime now = LocalDateTime.now();
            ProductModel productModel = productCache.get(productId);

            if ( now.isAfter( productModel.getLastUpdateCheck().plusSeconds( REFRESH_MODEL_CACHE_SECONDS ) ) ) {
                Logger.logMessage("Product " + productId + " needs to be checked for updates in cache", Logger.LEVEL.DEBUG);

                //if the lastUpdateDTM (in db) is after lastUpdateDTM (cached), then return false
                if ( productModel.checkForUpdates() ) {
                    Logger.logMessage("Product " + productId + " needs to be updated, returning failed cache check", Logger.LEVEL.DEBUG);
                    return false;
                } else {
                    Logger.logMessage("Product " + productId + " does not need to be updated, returning successful cache check", Logger.LEVEL.DEBUG);
                    productModel.setLastUpdateCheck( now );
                }
            }
        }

        return check;
    }

    public static ProductModel getProductFromCache(Integer productId) throws Exception {
        return productCache.get( productId );
    }


    //END PRODUCT CACHE

}