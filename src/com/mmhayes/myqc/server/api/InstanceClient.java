package com.mmhayes.myqc.server.api;

//mmhayes dependencies

import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.CommonAuthResource;
import com.mmhayes.common.api.Validation;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.commonMMHFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.myqc.server.collections.TOSCollection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.HashMap;

//javax.ws.rs dependencies
//other dependencies

/*
 Last Updated (automatically updated by SVN)
 $Author: ijgerstein $: Author of last commit
 $Date: 2021-06-29 15:29:39 -0400 (Tue, 29 Jun 2021) $: Date of last commit
 $Rev: 56234 $: Revision of last commit

Notes: This client is for instances authenticating against the gateway
*/

public class InstanceClient {
    private static Client validationClient = ClientBuilder.newClient(); //creates a validationClient for all requests from the instance
    //private static Client validationClient = createSSLClient();
    private static DataManager dm = new DataManager(); //the usual data manager
    private Validation responseValidation; //initialize response object
    private String validationTarget = "N/A"; //default target to "N/A"
    private String validationPath = "N/A"; //default path to "N/A"

    //this method sends a request to the gateway to have the gateway determine if the validation object passed is truly valid
    public Validation gatewayAuth(Validation requestValidation, HttpServletRequest request) {
        try { //send request - requestValidation
            //determine the validation target from a gatewayID
            Integer gatewayID = CommonAPI.getGatewayID();
            if (!gatewayID.equals(0)) {

                //determine validation target from database
                Object gatewayIdObj = dm.getSingleField("data.validation.getGatewayLocation",new Object[]{gatewayID});
                if (gatewayIdObj != null && !gatewayIdObj.toString().equals("false")) {
                    validationTarget = gatewayIdObj.toString();
                } else {
                    String errorDetails = "ERROR: Error in requestValidation InstanceClient.gatewayAuth - NO VALID GATEWAY FOUND IN DB";
                    Logger.logMessage(errorDetails + " - invalid gatewayID in instance DB ", Logger.LEVEL.ERROR); //log details
                    requestValidation.setErrorDetails(errorDetails); //set error details
                    return requestValidation;
                }

                //this being hardcoded here keeps the actual path of the API out of the database
                validationPath = "auth/validate";

                //prepare requestValidation for secure communication
                requestValidation.prepareInstanceToGatewayComm(request);

                //adding DEBUG logs here because sometimes Jersey swallows our exception and we can't determine what is going on in production -jrmitaly 1/26/2018
                Logger.logMessage("validationTarget: "+validationTarget, Logger.LEVEL.DEBUG);
                Logger.logMessage("validationPath: "+validationPath, Logger.LEVEL.DEBUG);

                //make the request
                responseValidation = securePostRequest(requestValidation);

            } else {
                String errorDetails = "ERROR: Error in requestValidation InstanceClient.gatewayAuth";
                Logger.logMessage(errorDetails + " - invalid gatewayID set in application properties", Logger.LEVEL.ERROR); //log details
                requestValidation.setErrorDetails(errorDetails); //set error details
                return requestValidation;
            }
        } catch (Exception ex) {
            Logger.logMessage("ERROR: Exception in requestValidation in InstanceClient.gatewayAuth", Logger.LEVEL.ERROR);
            Logger.logMessage("INFO: validationTarget: "+validationTarget, Logger.LEVEL.ERROR);
            Logger.logMessage("INFO: validationPath: "+validationPath, Logger.LEVEL.ERROR);
            requestValidation.handleGenericException(ex, "InstanceClient.gatewayAuth");
            return requestValidation;
        }

        try { //handle response - responseValidation
            Logger.logMessage("INFO: Handling response from gateway host: " + responseValidation.getGatewayHostName(), Logger.LEVEL.DEBUG);
            if (responseValidation.verifyHash()) { //verify the response - make sure the gateway sent it
                if (responseValidation.getIsValid()) { //check to see if the validation object is valid now..
                    if (responseValidation.getInstanceUserID() != -1) { //check to see if the user has more than one instance
                        if (CommonAPI.isInstanceUserActive(responseValidation)) { //check to see if the user is active
                            //check to see if responseValidation.data is populated with an instanceUserTypeID
                            Integer instanceUserTypeID = responseValidation.fetchInstanceUserTypeID();
                            if ( instanceUserTypeID != null ) {
                                responseValidation.createDSKey(instanceUserTypeID);
                            } else {
                                //create DSKey for now verified user
                                responseValidation.createDSKey();
                            }

                            //check TOS and account type only for employee accounts
                            if ( instanceUserTypeID == null || instanceUserTypeID == 1 ) {
                                //if prepare the instanceUserIDForTOS
                                Integer instanceUserIDForTOS = null;
                                if (responseValidation.getInstanceUserID() < 0) { //if negative make positive
                                    instanceUserIDForTOS = responseValidation.getInstanceUserID()*-1;
                                }

                                //set TOS for now verified user
                                TOSCollection tosCollection = new TOSCollection(request, instanceUserIDForTOS);
                                responseValidation.setTOS(tosCollection.getCollection());

                                String accountType = CommonAPI.checkForGiftCardAccountType(instanceUserIDForTOS);
                                if(!accountType.equals("") && accountType.equals("4")) {
                                    responseValidation.setDetails("gift-card");
                                }
                            }

                        } else {
                            responseValidation.setDetails("Your account may have been inactivated.  Please contact your Administrator or Manager.");
                            responseValidation.setErrorDetails("Your account may have been inactivated.  Please contact your Administrator or Manager.");
                            Logger.logMessage("ERROR: Tried to log user in - but they were not valid!", Logger.LEVEL.ERROR);
                            Logger.logMessage("DETAILS: "+responseValidation.getDetails(), Logger.LEVEL.ERROR); //log details
                            Logger.logMessage("ERROR DETAILS: "+responseValidation.getErrorDetails(), Logger.LEVEL.ERROR);//log error details
                        }
                    } else {
                        responseValidation.setDetails("Please login through MMHayes gateway.");
                    }
                } else {
                    Logger.logMessage("ERROR: Tried to log user in - but they were not valid!", Logger.LEVEL.ERROR);
                    Logger.logMessage("DETAILS: "+responseValidation.getDetails(), Logger.LEVEL.ERROR); //log details
                    Logger.logMessage("ERROR DETAILS: "+responseValidation.getErrorDetails(), Logger.LEVEL.ERROR); //log error details
                }
            } else {
                Logger.logMessage("ERROR: Error verifying hash in InstanceClient.gatewayAuth!", Logger.LEVEL.ERROR);
                Logger.logMessage("DETAILS: "+responseValidation.getDetails(), Logger.LEVEL.ERROR); //log details
                Logger.logMessage("ERROR DETAILS: "+responseValidation.getErrorDetails(), Logger.LEVEL.ERROR); //log error details
            }
            return responseValidation;
        } catch (Exception ex) {
            Logger.logMessage("ERROR: Exception in responseValidation in InstanceClient.gatewayAuth", Logger.LEVEL.ERROR);
            Logger.logMessage("INFO: validationTarget: "+validationTarget, Logger.LEVEL.ERROR);
            Logger.logMessage("INFO: validationPath: "+validationPath, Logger.LEVEL.ERROR);
            responseValidation.handleGenericException(ex, "InstanceClient.gatewayAuth");
            return responseValidation;
        }
    }

    //this method sends a request to the gateway to update a user's password (by loginName) - for force password change
    public Validation attemptUpdate(Validation requestValidation, HashMap forcePWChangeHM, HttpServletRequest request) {
        try { //send request

            //get client credentials
            HttpSession session = request.getSession();
            Integer instanceUserID = CommonAuthResource.checkRequestHeaderForMMHayesAuth(request);
            String loginName = forcePWChangeHM.get("loginName").toString();
            String resetPass = forcePWChangeHM.get("resetPass").toString();
            String confirmResetPass = forcePWChangeHM.get("confirmResetPass").toString();
            Integer instanceUserTypeID = CommonAPI.convertModelDetailToInteger(forcePWChangeHM.get("instanceUserTypeID"));

            //return if passwords don't match
            if (!resetPass.equals(confirmResetPass)) {
                String errorDetails = "ERROR: Passwords do not match";
                Logger.logMessage(errorDetails + " - inside InstanceClient.AttemptUpdate", Logger.LEVEL.ERROR);//log details
                requestValidation.setErrorDetails(errorDetails); //set error details
                return requestValidation;
            }

            //test password strength against criteria
            String passStrength = CommonAPI.getPassStrength();
            Boolean passCriteriaMet = false;
            commonMMHFunctions commFunc = new commonMMHFunctions();
            if (passStrength.equals("loose")) {
                passCriteriaMet = commFunc.validatePasswordLoose(resetPass);
            } else {
                passCriteriaMet = commFunc.validatePasswordStrict(resetPass);
            }

            //if password criteria not met then return
            if (!passCriteriaMet) {
                String errorDetails = "ERROR: Password criteria not met";
                Logger.logMessage(errorDetails + " - inside InstanceClient.AttemptUpdate", Logger.LEVEL.ERROR); //log details
                requestValidation.setErrorDetails(errorDetails); //set error details
                return requestValidation;
            }

            //set the instanceUserId as negative for employees
            instanceUserID = instanceUserID * -1;

            //if the instanceUserTypeID is 3 set the instanceUserID as positive for person accounts
            if(instanceUserTypeID != null && instanceUserTypeID == 3 && instanceUserID < 0) {
                instanceUserID = instanceUserID * -1;
            }

            //set instanceUserID and loginName
            Integer gatewayID = CommonAPI.getGatewayID();
            requestValidation.setInstanceUserID(instanceUserID);
            requestValidation.setLoginName(loginName);

            //set the validation object to isValid and build a hash
            requestValidation.setIsValid(true);
            requestValidation.buildHash();

            //generic data object takes in an arraylist of hashmaps
            ArrayList<HashMap> data = new ArrayList<HashMap>();
            HashMap userData = new HashMap();
            userData.put("RESETPASS", resetPass);
            data.add(userData);
            requestValidation.setData( data );

            requestValidation.setInstanceUserTypeID(instanceUserTypeID);

            if (!gatewayID.equals(0)) {

                //determine validation target from database
                validationTarget = dm.getSingleField("data.validation.getGatewayLocation",new Object[]{gatewayID}).toString();

                //this being hardcoded here keeps the actual path of the API out of the database
                validationPath = "auth/update/attempt";

                //prepare requestValidation for secure communication
                requestValidation.prepareInstanceToGatewayComm(request);

                //adding DEBUG logs here because sometimes Jersey swallows our exception and we can't determine what is going on in production -jrmitaly 1/26/2018
                Logger.logMessage("validationTarget: "+validationTarget, Logger.LEVEL.DEBUG);
                Logger.logMessage("validationPath: "+validationPath, Logger.LEVEL.DEBUG);

                //make the request to gateway
                responseValidation = securePostRequest(requestValidation);

            } else {
                String errorDetails = "ERROR: Error in requestValidation InstanceClient.attemptUpdate";
                Logger.logMessage(errorDetails + " - invalid gatewayID set in application properties", Logger.LEVEL.ERROR); //log details
                requestValidation.setErrorDetails(errorDetails); //set error details
                return requestValidation;
            }
        } catch (Exception ex) {
            Logger.logMessage("ERROR: Exception in requestValidation in InstanceClient.attemptUpdate", Logger.LEVEL.ERROR);
            Logger.logMessage("INFO: validationTarget: "+validationTarget, Logger.LEVEL.ERROR);
            Logger.logMessage("INFO: validationPath: "+validationPath, Logger.LEVEL.ERROR);
            requestValidation.handleGenericException(ex, "InstanceClient.attemptUpdate");
            return requestValidation;
        }

        try { //handle response - responseValidation
            Logger.logMessage("INFO: Handling response from gateway host: "+responseValidation.getGatewayHostName(), Logger.LEVEL.DEBUG);
            if (responseValidation.verifyHash()) { //verify the response - make sure the gateway sent it
                if (!responseValidation.getIsValid()) { //check to see if the validation object is valid now..
                    Logger.logMessage("ERROR: Tried to force password change - but they were not valid!", Logger.LEVEL.ERROR);
                    Logger.logMessage("DETAILS: "+responseValidation.getDetails(), Logger.LEVEL.ERROR); //log details
                    Logger.logMessage("ERROR DETAILS: "+responseValidation.getErrorDetails(), Logger.LEVEL.ERROR); //log error details
                }
            } else {
                Logger.logMessage("ERROR: Error verifying hash in InstanceClient.attemptUpdate!", Logger.LEVEL.ERROR);
                Logger.logMessage("DETAILS: "+responseValidation.getDetails(), Logger.LEVEL.ERROR); //log details
                Logger.logMessage("ERROR DETAILS: "+responseValidation.getErrorDetails(), Logger.LEVEL.ERROR); //log error details
            }
            return responseValidation;
        } catch (Exception ex) {
            Logger.logMessage("ERROR: Exception in responseValidation in InstanceClient.attemptUpdate", Logger.LEVEL.ERROR);
            Logger.logMessage("INFO: validationTarget: "+validationTarget, Logger.LEVEL.ERROR);
            Logger.logMessage("INFO: validationPath: "+validationPath, Logger.LEVEL.ERROR);
            requestValidation.handleGenericException(ex, "InstanceClient.attemptUpdate");
            return responseValidation;
        }
    }

    //this method sends a request to the gateway
    public Validation gatewayReAuth(Validation requestValidation, HttpServletRequest request) {
        try { //send request - requestValidation
            //determine the validation target from a gatewayID
            Integer gatewayID = CommonAPI.getGatewayID();
            if (!gatewayID.equals(0)) {

                //get custom header X-MMHayes-Auth from request object
                String mmhayesAuthHeader = request.getHeader("X-MMHayes-Auth");

                //if the mmhayesAuthHeader is valid, verify the contents of it (Device Session Key)
                if (mmhayesAuthHeader == null) {
                    String errorDetails = "ERROR: checking request headers InstanceClient.gatewayReAuth";
                    Logger.logMessage(errorDetails + " - could not find X-MMHayes-Auth in the request header!", Logger.LEVEL.ERROR); //log details
                    requestValidation.setErrorDetails(errorDetails); //set error details
                    return requestValidation;
                }

                //determine gatewayUserID from DSKey
                Object gatewayUserIDObj = dm.getSingleField("data.validation.getGatewayUserByDSKey",new Object[]{mmhayesAuthHeader});
                if (gatewayUserIDObj != null) {
                    requestValidation.setGatewayUserID(Integer.parseInt(gatewayUserIDObj.toString()));
                } else {
                    String errorDetails = "ERROR: Error in requestValidation InstanceClient.gatewayReAuth";
                    Logger.logMessage(errorDetails + " - NO GATEWAY USER ID ON DEVICE SESSION KEY ", Logger.LEVEL.ERROR); //log details
                    requestValidation.setErrorDetails(errorDetails); //set error details
                    return requestValidation;
                }

                //determine validation target from database
                Object gatewayIdObj = dm.getSingleField("data.validation.getGatewayLocation",new Object[]{gatewayID});
                if (gatewayIdObj != null && !gatewayIdObj.toString().equals("false")) {
                    validationTarget = gatewayIdObj.toString();
                } else {
                    String errorDetails = "ERROR: Error in requestValidation InstanceClient.gatewayReAuth - NO VALID GATEWAY FOUND IN DB";
                    Logger.logMessage(errorDetails + " - invalid gatewayID in instance DB ", Logger.LEVEL.ERROR);//log details
                    requestValidation.setErrorDetails(errorDetails); //set error details
                    return requestValidation;
                }

                //this being hardcoded here keeps the actual path of the API out of the database
                validationPath = "auth/reauth";

                //prepare requestValidation for secure communication
                requestValidation.prepareInstanceToGatewayComm(request);

                //adding DEBUG logs here because sometimes Jersey swallows our exception and we can't determine what is going on in production -jrmitaly 1/26/2018
                Logger.logMessage("validationTarget: "+validationTarget, Logger.LEVEL.DEBUG);
                Logger.logMessage("validationPath: "+validationPath, Logger.LEVEL.DEBUG);

                //make the request
                responseValidation = securePostRequest(requestValidation);

            } else {
                String errorDetails = "ERROR: Error in requestValidation InstanceClient.gatewayReAuth";
                Logger.logMessage(errorDetails + " - invalid gatewayID set in application properties", Logger.LEVEL.ERROR); //log details
                requestValidation.setErrorDetails(errorDetails); //set error details
                return requestValidation;
            }
        } catch (Exception ex) {
            Logger.logMessage("ERROR: Exception in requestValidation in InstanceClient.gatewayReAuth", Logger.LEVEL.ERROR);
            Logger.logMessage("INFO: validationTarget: "+validationTarget, Logger.LEVEL.ERROR);
            Logger.logMessage("INFO: validationPath: "+validationPath, Logger.LEVEL.ERROR);
            requestValidation.handleGenericException(ex, "InstanceClient.gatewayReAuth");
            return requestValidation;
        }

        try { //handle response - responseValidation
            Logger.logMessage("INFO: Handling response from gateway host: "+responseValidation.getGatewayHostName(), Logger.LEVEL.DEBUG);
            if (responseValidation.verifyHash()) { //verify the response - make sure the gateway sent it
                if (!responseValidation.getIsValid()) { //check to see if the validation object is valid now..
                    Logger.logMessage("ERROR: Tried to re-authenticate user but was unable!", Logger.LEVEL.ERROR);
                    Logger.logMessage("DETAILS: "+responseValidation.getDetails(), Logger.LEVEL.ERROR);//log details
                    Logger.logMessage("ERROR DETAILS: "+responseValidation.getErrorDetails(), Logger.LEVEL.ERROR);//log error details
                }
            } else {
                Logger.logMessage("ERROR: Error verifying hash in InstanceClient.gatewayReAuth!", Logger.LEVEL.ERROR);
                Logger.logMessage("DETAILS: "+responseValidation.getDetails(), Logger.LEVEL.ERROR);//log details
                Logger.logMessage("ERROR DETAILS: "+responseValidation.getErrorDetails(), Logger.LEVEL.ERROR);//log error details
            }
            return responseValidation;
        } catch (Exception ex) {
            Logger.logMessage("ERROR: Exception in responseValidation in InstanceClient.gatewayReAuth", Logger.LEVEL.ERROR);
            Logger.logMessage("INFO: validationTarget: "+validationTarget, Logger.LEVEL.ERROR);
            Logger.logMessage("INFO: validationPath: "+validationPath, Logger.LEVEL.ERROR);
            responseValidation.handleGenericException(ex, "InstanceClient.gatewayAuth");
            return responseValidation;
        }
    }

    //this method determines if the loginName passed in the requestValidation is valid for use
    public Validation checkIfGatewayUserExists(Validation requestValidation, HttpServletRequest request) {
        try { //send request - requestValidation

            //determine the validation target from a gatewayID
            Integer gatewayID = CommonAPI.getGatewayID();
            if (!gatewayID.equals(0)) {

                //determine validation target from database
                validationTarget = dm.getSingleField("data.validation.getGatewayLocation",new Object[]{gatewayID}).toString();

                //this being hardcoded here keeps the actual path of the API out of the database
                validationPath = "account/exists";

                //prepare requestValidation for secure communication
                requestValidation.prepareInstanceToGatewayComm(request);

                //adding DEBUG logs here because sometimes Jersey swallows our exception and we can't determine what is going on in production -jrmitaly 1/26/2018
                Logger.logMessage("validationTarget: "+validationTarget, Logger.LEVEL.DEBUG);
                Logger.logMessage("validationPath: "+validationPath, Logger.LEVEL.DEBUG);

                //make the request
                responseValidation = securePostRequest(requestValidation);

            } else {
                String errorDetails = "Error in requestValidation InstanceClient.removeInstanceAccess";
                Logger.logMessage(errorDetails + " - invalid gatewayID set in application properties", Logger.LEVEL.ERROR); //log details
                requestValidation.setErrorDetails(errorDetails); //set error details
                return requestValidation;
            }
        } catch (Exception ex) {
            Logger.logMessage("ERROR: Exception in requestValidation in InstanceClient.removeInstanceAccess", Logger.LEVEL.ERROR);
            Logger.logMessage("INFO: validationTarget: "+validationTarget, Logger.LEVEL.ERROR);
            Logger.logMessage("INFO: validationPath: "+validationPath, Logger.LEVEL.ERROR);
            requestValidation.handleGenericException(ex, "InstanceClient.removeInstanceAccess");
            return requestValidation;
        }

        try { //handle response - responseValidation
            return checkGatewayResponseValidationObject(responseValidation, false);
        } catch (Exception ex) {
            Logger.logMessage("ERROR: Exception in responseValidation in InstanceClient.removeInstanceAccess", Logger.LEVEL.ERROR);
            Logger.logMessage("INFO: validationTarget: "+validationTarget, Logger.LEVEL.ERROR);
            Logger.logMessage("INFO: validationPath: "+validationPath, Logger.LEVEL.ERROR);
            requestValidation.handleGenericException(ex, "InstanceClient.removeInstanceAccess");
            return responseValidation;
        }
    }

    //this method gets gateway user information by sending an instanceUserID to the gateway
    public Validation getInstanceDetails(Validation requestValidation, HttpServletRequest request) {
        try { //send request - requestValidation

            //determine the validation target from a gatewayID
            Integer gatewayID = CommonAPI.getGatewayID();
            if (!gatewayID.equals(0)) {

                //determine validation target from database
                validationTarget = dm.getSingleField("data.validation.getGatewayLocation",new Object[]{gatewayID}).toString();

                //this being hardcoded here keeps the actual path of the API out of the database
                validationPath = "account/details";

                //prepare requestValidation for secure communication
                requestValidation.prepareInstanceToGatewayComm(request);

                //adding DEBUG logs here because sometimes Jersey swallows our exception and we can't determine what is going on in production -jrmitaly 1/26/2018
                Logger.logMessage("validationTarget: "+validationTarget, Logger.LEVEL.DEBUG);
                Logger.logMessage("validationPath: "+validationPath, Logger.LEVEL.DEBUG);

                //make the request
                responseValidation = securePostRequest(requestValidation);

            } else {
                String errorDetails = "Error in requestValidation InstanceClient.getInstanceDetails";
                Logger.logMessage(errorDetails + " - invalid gatewayID set in application properties", Logger.LEVEL.ERROR); //log details
                requestValidation.setErrorDetails(errorDetails); //set error details
                return requestValidation;
            }
        } catch (Exception ex) {
            Logger.logMessage("ERROR: Exception in requestValidation in InstanceClient.getInstanceDetails", Logger.LEVEL.ERROR);
            Logger.logMessage("INFO: validationTarget: "+validationTarget, Logger.LEVEL.ERROR);
            Logger.logMessage("INFO: validationPath: "+validationPath, Logger.LEVEL.ERROR);
            requestValidation.handleGenericException(ex, "InstanceClient.getInstanceDetails");
            return requestValidation;
        }

        try { //handle response - responseValidation
            return checkGatewayResponseValidationObject(responseValidation, false);
        } catch (Exception ex) {
            Logger.logMessage("ERROR: Exception in responseValidation in InstanceClient.getInstanceDetails", Logger.LEVEL.ERROR);
            Logger.logMessage("INFO: validationTarget: "+validationTarget, Logger.LEVEL.ERROR);
            Logger.logMessage("INFO: validationPath: "+validationPath, Logger.LEVEL.ERROR);
            requestValidation.handleGenericException(ex, "InstanceClient.getInstanceDetails");
            return responseValidation;
        }
    }

    //this method sends a request to the gateway to create a new gateway account
    public Validation createGatewayAccount(Validation requestValidation, HttpServletRequest request) {
        try { //send request - requestValidation

            //determine the validation target from a gatewayID
            Integer gatewayID = CommonAPI.getGatewayID();
            if (!gatewayID.equals(0)) {

                //determine validation target from database
                validationTarget = dm.getSingleField("data.validation.getGatewayLocation",new Object[]{gatewayID}).toString();

                //this being hardcoded here keeps the actual path of the API out of the database
                validationPath = "account/new";

                //prepare requestValidation for secure communication
                requestValidation.prepareInstanceToGatewayComm(request);

                //adding DEBUG logs here because sometimes Jersey swallows our exception and we can't determine what is going on in production -jrmitaly 1/26/2018
                Logger.logMessage("validationTarget: "+validationTarget, Logger.LEVEL.DEBUG);
                Logger.logMessage("validationPath: "+validationPath, Logger.LEVEL.DEBUG);

                //make the request
                responseValidation = securePostRequest(requestValidation);
            } else {
                String errorDetails = "Error in requestValidation InstanceClient.createGatewayAccount";
                Logger.logMessage(errorDetails + " - invalid gatewayID set in application properties.", Logger.LEVEL.ERROR); //log details
                requestValidation.setErrorDetails(errorDetails); //set error details
                return requestValidation;
            }
        } catch (Exception ex) {
            Logger.logMessage("ERROR: Exception in requestValidation in InstanceClient.createGatewayAccount", Logger.LEVEL.ERROR);
            Logger.logMessage("INFO: validationTarget: "+validationTarget, Logger.LEVEL.ERROR);
            Logger.logMessage("INFO: validationPath: "+validationPath, Logger.LEVEL.ERROR);
            requestValidation.handleGenericException(ex, "InstanceClient.createGatewayAccount");
            return requestValidation;
        }

        try { //handle response - responseValidation
            return checkGatewayResponseValidationObject(responseValidation, true);
        } catch (Exception ex) {
            Logger.logMessage("ERROR: Exception in responseValidation in InstanceClient.createGatewayAccount", Logger.LEVEL.ERROR);
            Logger.logMessage("INFO: validationTarget: "+validationTarget, Logger.LEVEL.ERROR);
            Logger.logMessage("INFO: validationPath: "+validationPath, Logger.LEVEL.ERROR);
            requestValidation.handleGenericException(ex, "InstanceClient.createGatewayAccount");
            return responseValidation;
        }
    }


    //this method checks the response validation object for errors or other issues before returning to the client
    private Validation checkGatewayResponseValidationObject(Validation responseValidation, Boolean validate) throws Exception {
        Logger.logMessage("INFO: Handling response from gateway host: "+responseValidation.getGatewayHostName(), Logger.LEVEL.TRACE);
        if (!responseValidation.verifyHash()) { //verify hash - checks against instance
            Logger.logMessage("ERROR: Error verifying hash in InstanceClient.checkGatewayResponseValidationObject!", Logger.LEVEL.ERROR);
            Logger.logMessage("DETAILS: "+responseValidation.getDetails(), Logger.LEVEL.ERROR); //log details
            Logger.logMessage("ERROR DETAILS: "+responseValidation.getErrorDetails(), Logger.LEVEL.ERROR); //log error details
        }
        if (validate) {
            if (!responseValidation.getIsValid()) { //check to see if the validation object is valid now..
                Logger.logMessage("ERROR: Tried to validate request - but it was not valid! In InstanceClient.checkGatewayResponseValidationObject", Logger.LEVEL.ERROR);
                Logger.logMessage("DETAILS: " + responseValidation.getDetails(), Logger.LEVEL.ERROR); //log details
                Logger.logMessage("ERROR DETAILS: " + responseValidation.getErrorDetails(), Logger.LEVEL.ERROR); //log error details
            }
        }
        return responseValidation;
    }

    //this method sends a POST request using the secure Validation object and returns the response as a secure Validation object
    //TODO: (HIGH) needs SSL !! See createSSLClient method
    private Validation securePostRequest(Validation requestValidation) {

        //FOR TESTING - sends requests through fiddler (or any proxy really) -jrmitaly
        //System.setProperty("http.proxyHost", "10.1.246.11");
        //System.setProperty("http.proxyPort", "8888");

        return validationClient.target(validationTarget)
                .path(validationPath)
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(requestValidation, MediaType.APPLICATION_JSON), Validation.class);
    }

    public Validation getConfigurableEmailAppCode(Validation requestValidation, HttpServletRequest request){

        try {
            //determine the validation target from a gatewayID
            Integer gatewayID = CommonAPI.getGatewayID();
            if (!gatewayID.equals(0)) {

                //determine validation target from database
                validationTarget = dm.getSingleField("data.validation.getGatewayLocation", new Object[]{gatewayID }).toString();

                //this being hardcoded here keeps the actual path of the API out of the database
                validationPath = "auth/code/instance/"+CommonAPI.getInstanceID().toString();

                //prepare requestValidation for secure communication
                requestValidation.prepareInstanceToGatewayComm(request);

                //make the request
                responseValidation = securePostRequest(requestValidation);

            } else {
                String errorDetails = "Error in InstanceClient.getConfigurableEmailAppCode";
                Logger.logMessage(errorDetails, Logger.LEVEL.ERROR); //log details
                requestValidation.setErrorDetails(errorDetails); //set error detailsrequestValidation.setData(new ArrayList<>());
                requestValidation.setData(new ArrayList<>());
                return requestValidation;
            }
        } catch (Exception ex) {
            Logger.logMessage("ERROR: Exception in InstanceClient.getConfigurableEmailAppCode", Logger.LEVEL.ERROR);
            Logger.logMessage("INFO: validationTarget: "+validationTarget, Logger.LEVEL.ERROR);
            Logger.logMessage("INFO: validationPath: "+validationPath, Logger.LEVEL.ERROR);
            requestValidation.handleGenericException(ex, "InstanceClient.getConfigurableEmailAppCode");
            return requestValidation;
        }

        try { //handle response - responseValidation
            return checkGatewayResponseValidationObject(responseValidation, false);
        } catch (Exception ex) {
            Logger.logMessage("ERROR: Exception in responseValidation in InstanceClient.getConfigurableEmailAppCode", Logger.LEVEL.ERROR);
            Logger.logMessage("INFO: validationTarget: "+validationTarget, Logger.LEVEL.ERROR);
            Logger.logMessage("INFO: validationPath: "+validationPath, Logger.LEVEL.ERROR);
            requestValidation.handleGenericException(ex, "InstanceClient.getConfigurableEmailAppCode");
            return responseValidation;
        }

    }


    //creates a REST client for all requests from the instance - SSL Enabled
    //Commented out due to issues with SSL in the cloud -jrmitaly 1/19/2015
    //See: https://www13.v1host.com/MMHayes/story.mvc/Summary?oidToken=Story%3A8001
    /*private static Client createSSLClient() {
        Client client = null;
        String trustStorePath = "", keyStorePath="", appPath="";

        try {
            System.setProperty("jsse.enableSNIExtension", "false"); //testing this to fix "already connected" error -jrmitaly
            if (MMHServletStarter.isDevEnv()) { //we are in dev environment
                //do not remove commented out lines below
                //use the following two lines in order to test SSL within dev environments -jrmitaly 1/16/2015
                //trustStorePath = "C:/IdeaProjects/Libraries/JDK/java 8/jre/lib/security/cacerts";
                //keyStorePath = "C:/IdeaProjects/Libraries/JDK/java 8/jre/lib/security/MMHayes.jks";

                //do not use SSL for dev environment as tomcat seems to freak out when the same tomcat is trying to use the same cert for two servers -jrmitaly 1/16/2015
                client = ClientBuilder.newBuilder().build();
                return client;
            } else { //we are NOT in a dev environment so we need to use SSL
                if (System.getenv("MMH_HOME") != null && System.getenv("MMH_HOME").toString().length() > 0) {
                    appPath = System.getenv("MMH_HOME").toString();
                    trustStorePath = appPath+"/java/jre/lib/security/cacerts";
                    keyStorePath = appPath+"/java/jre/lib/security/MMHayes.jks";

                    //for testing - C:\MMHayes\java\jre\lib\security\
                    trustStorePath = "C:/MMHayes/java/jre/lib/security/cacerts";
                    keyStorePath = "C:/MMHayes/java/jre/lib/security/MMHCloud.jks";
                    Logger.logMessage("INFO: appPath = "+appPath);
                    Logger.logMessage("INFO: trustStorePath = "+trustStorePath);
                    Logger.logMessage("INFO: keyStorePath = "+keyStorePath);
                }  else {
                    Logger.logMessage("ERROR: Error in InstanceClient.createSSLClient - Cannot determine MMH_Home environment variable!");
                }
            }

            SslConfigurator sslConfig = SslConfigurator.newInstance()
                    .trustStoreFile(trustStorePath)
                    .trustStorePassword("changeit")
                    .keyStoreFile(keyStorePath)
                    .keyPassword("kronites");
            SSLContext sslContext = sslConfig.createSSLContext();
            client = ClientBuilder.newBuilder().sslContext(sslContext).build();
        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ex.printStackTrace(pw); //allows stack trace as a string -> sw.toString()
            Logger.logMessage("ERROR: Error in InstanceClient.createSSLClient - Exception thrown: " + sw.toString());
        }
        return client;

    }*/
}