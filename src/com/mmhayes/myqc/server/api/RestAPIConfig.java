package com.mmhayes.myqc.server.api;

//mmhayes dependencies
import com.mmhayes.common.api.*;
import com.mmhayes.common.dataaccess.*;
import com.mmhayes.common.utils.*;

//javax.ws.rs dependencies
import org.glassfish.jersey.server.ResourceConfig;

/*
 REST API Config

 Created with IntelliJ IDEA.
 User: jrmitaly
 Date: 4/18/16
 Time: 11:46 AM

 Last Updated (automatically updated by SVN)
 $Author: jrmitaly $: Author of last commit
 $Date: 2017-10-23 11:47:44 -0400 (Mon, 23 Oct 2017) $: Date of last commit
 $Rev: 24782 $: Revision of last commit

Notes: For registering packages/classes to use with REST (registering components in Jersey/JaxRS)
*/

//extend ResourceConfig to configure the REST API (jersey, jaxrs) (was previously done in web.xml)
public class RestAPIConfig extends ResourceConfig {

    //register REST API Components
    public RestAPIConfig() {
        try {
            //register the PROJECT SPECIFIC "api" resources
            packages("com.mmhayes.myqc.server.api.resources");

            //register the PROJECT SPECIFIC "api" filters
            packages("com.mmhayes.myqc.server.api.filters");

            //register the COMMON "api" error handling classes
            packages("com.mmhayes.common.api.errorhandling");

            //register the COMMON "api" custom exceptions (checked exceptions)
            packages("com.mmhayes.common.api.errorhandling.exceptions");

            //register the COMMON "api" exception mappers
            packages("com.mmhayes.common.api.errorhandling.mappers");

            //register the COMMON "api" JSON mappers
            packages("com.mmhayes.common.api.json.mappers");

            //register the COMMON "api" JSON mappers
            packages("com.mmhayes.common.api.json.modules");

            //register the COMMON "api" JSON mappers
            packages("com.mmhayes.common.api.json.serializers");

            /* LEAVE THE FOLLOWING LINE UNLESS YOU KNOW WHAT YOU ARE DOING - see notes below -jrmitaly 6/6/2016
            //this needs to be added due to an issue in Jersey/Jackon - relating to them already have a mapper for the JsonProcessingException
            see: http://stackoverflow.com/questions/24397647/jersey-exception-mappers-not-working-when-jackson-deserialization-fails
            see: https://github.com/FasterXML/jackson-jaxrs-providers/issues/22

            Basically, I would leave this line here until they fix it in Jersey and then upgrade Jersey
            */
            //register(com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider.class);
            register(com.mmhayes.common.api.providers.CustomJsonProvider.class);

        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
            throw ex;
        }
    }
}