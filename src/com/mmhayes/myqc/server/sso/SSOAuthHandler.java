package com.mmhayes.myqc.server.sso;

//MMHayes Dependencies

import com.google.gson.Gson;
import com.mmhayes.common.api.Validation;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.myqc.server.utils.SecureLogin;
import com.onelogin.saml2.Auth;
import com.onelogin.saml2.settings.Saml2Settings;
import org.owasp.esapi.ESAPI;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//Other Dependencies

/*
 $Author: jmdottavio $: Author of last commit
 $Date: 2018-03-30 16:45:03 -0400 (Fri, 30 Mar 2018) $: Date of last commit
 $Rev: 28686 $: Revision of last commit
 Notes: For dealing with authentication related to SSO (called from SSO "helpers" such as the SAMLHelper)
*/
public class SSOAuthHandler {

    private static DataManager dm = new DataManager();
    private static final String DEFAULT_RELAY_STATE = "../"; //the root of the this projects war context (like /qc/index.jsp) should be the default relay state
    private static final String SSO_VALIDATION_GET_PARAM_KEY = "ssoValidation"; //name of HTTP GET parameter for the ssoValidation JSON object
    private static final String REDIRECT_LOGOUT = "../#logout-page";

    //handles SSO Response (ex. SAML Response) from an IdP - this generally means the user is authenticated and ready to create a session in our application
    public void handleIdPResponse(String nameID, String sessionIndex, Map<String, List<String>> attributes, HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            Logger.logMessage("Handling IdP Response for nameID: "+nameID, Logger.LEVEL.DEBUG);

            //NOTE: I am not sure what we will need SSO attributes we are passing in for exactly. It is a good idea to have them ready to go in this method though - jrmitaly 8/11/2017

            //create a Validation object that contains a DSKey, TOS Information, and more
            Validation ssoValidation = SecureLogin.createValidatedSessionForSSONameID(nameID, sessionIndex, request);

            //as long we found some ssoValidation object, send it with the redirect, let it handle the errors inside it if there are any
            if (ssoValidation != null) {
                //now that the session has been created, relay the user to the application they are trying to access
                String relayState = request.getParameter("RelayState"); //RelayState is the relayState that we sent to the IdP originally OR it defaults
                relayState = ESAPI.encoder().decodeFromURL(relayState); //URL decode the relay state
                if (relayState != null && !relayState.isEmpty()) {
                    Logger.logMessage("Redirecting using the GET param relay state...", Logger.LEVEL.DEBUG);
                    response.sendRedirect(relayState+"?"+SSO_VALIDATION_GET_PARAM_KEY+"="+new Gson().toJson(ssoValidation));
                } else {
                    Logger.logMessage("Redirecting using the default relay state...", Logger.LEVEL.DEBUG);
                    response.sendRedirect(DEFAULT_RELAY_STATE+"?"+SSO_VALIDATION_GET_PARAM_KEY+"="+new Gson().toJson(ssoValidation));
                }
            } else {
                Logger.logMessage("Could not find valid ssoValidation object in handleIdPResponse in SSOAuthHandler...", Logger.LEVEL.ERROR);
            }

        } catch (Exception ex) {
            Logger.logException(ex);
            throw ex;
        }
    }

    //this method sends a request to an Identity Providers (such as ADFS, for example)
    public void sendLogoutRequestToIdP(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String redirectPath = REDIRECT_LOGOUT;
        try {
            //send logout request to IdP with auth.login
            Auth auth = new Auth(request, response);
            // IF NO SLO URL defined then skip logout
            if(auth.getSLOurl() == null){
                response.sendRedirect(redirectPath);
                return;
            }

            String nameID = "";
            String sessionIndex = "";
            //String dsKey = request.getHeader("x-mmhayes-auth");
            String dsKey = request.getParameter("dskey");
            if(dsKey != null){
                ArrayList<HashMap> result = dm.parameterizedExecuteQuery("data.validation.getIDPSLSParameters",new Object[]{dsKey}, true);
                if(result.get(0).containsKey("USERACCTLOGIN")){
                    nameID = result.get(0).get("USERACCTLOGIN").toString();
                }
                if(result.get(0).containsKey("IDPSESSIONKEY")){
                    sessionIndex = result.get(0).get("IDPSESSIONKEY").toString();
                }
            }

            // execute Single Log Out
            if(!nameID.equals("") && !sessionIndex.equals("")){
                // Get SAML Settings
                Saml2Settings settings = auth.getSettings();
                // Cache sp name id format
                String spNameIDFormat = settings.getSpNameIDFormat();
                // clear SpNameIDFormat for ADFS compatibility
                settings.setSpNameIDFormat(null);
                // Call logout
                auth.logout(redirectPath, nameID, sessionIndex, false);
                // Reset SPNameIDFormat for next Login request
                settings.setSpNameIDFormat(spNameIDFormat);
            } else {
                // no session so got to logout
                response.sendRedirect(redirectPath);
            }
        } catch (Exception ex) {
            Logger.logMessage("Error in method: sendLogoutRequestToIdP()", Logger.LEVEL.ERROR);
            Logger.logException(ex);
            response.sendRedirect(redirectPath);
        }
    }

    public void handleIDPInitiatedLogout(HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            //initialize response body that we will send to our client (if needed)
            StringBuilder responseBody = new StringBuilder();

            //process the SAML response
            Auth auth = new Auth(request, response);
            auth.processSLO();

            //grab any potiential errors from processing the SAML response
            List<String> errors = auth.getErrors();

            //no errors found - create a valid session and then redirect user to relayState
            if (errors.isEmpty()) {
                //String sessionIndex = auth.getLastMessageId();
                String lastRequest = auth.getLastRequestXML();
                if(lastRequest == null){
                    response.sendRedirect(REDIRECT_LOGOUT);
                } else {
                    int posStart = lastRequest.indexOf("<samlp:SessionIndex>");
                    int posEnd = lastRequest.indexOf("</samlp:SessionIndex>");
                    String sessionIndex = lastRequest.substring(posStart+20, posEnd);
                    // Expire DSKEYS
                    if(!sessionIndex.isEmpty()){
                        ArrayList<HashMap> result = dm.parameterizedExecuteQuery("data.validation.getEmployeeIDByIDPSessionKey", new Object[]{sessionIndex}, true);
                        if(!result.isEmpty() && result.get(0).containsKey("EMPLOYEEID")){
                            String employeeID = result.get(0).get("EMPLOYEEID").toString();
                            int updateCount = dm.parameterizedExecuteNonQuery("data.validation.expireDSKeyByEmployeeID", new Object[]{employeeID});
                            if(updateCount > 0){
                                Logger.logMessage("Successfully logged out SSO IDPSessionKey: "+sessionIndex+" out of MyQuickCharge. Expired "+updateCount+" dskeys via IDP Initiated Logout.", Logger.LEVEL.TRACE);
                            } else {
                                Logger.logMessage("Unable to complete IDP Initiated Logout for EmployeeID: "+employeeID+" No matching DeviceSessionKey records found.", Logger.LEVEL.ERROR);
                            }
                        } else {
                            Logger.logMessage("Unable to complete IDP Initiated Logout. Unable to find DeviceSessionKey record for SSO session: "+sessionIndex, Logger.LEVEL.ERROR);
                        }
                    }
                }
            } else { //there are errors, log each error and add to the response
                response.setContentType("text/html; charset=UTF-8");
                for (String error : errors) {
                    Logger.logMessage("Error Found in ACS: "+error, Logger.LEVEL.ERROR);
                    responseBody.append("<p>"+error+"</p>");
                }
                String lastErrorReason = auth.getLastErrorReason();
                if (lastErrorReason != null && !lastErrorReason.isEmpty()) {
                    Logger.logMessage("Last Error Reason Found in ACS: "+lastErrorReason, Logger.LEVEL.ERROR);
                    responseBody.append("<br/> <b>Last Error Reason</b> <br/>");
                    responseBody.append("<p>"+lastErrorReason+"</p>");
                }

                //write out responseBody and return to client
                response.setStatus(response.SC_OK);
                response.getWriter().write(responseBody.toString());
                response.getWriter().flush();
                response.getWriter().close();
            }
        } catch (Exception ex) {
            Logger.logException(ex);
            throw ex;
        }
    }
}
