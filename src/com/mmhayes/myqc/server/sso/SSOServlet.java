package com.mmhayes.myqc.server.sso;

//MMHayes Dependencies

import com.mmhayes.common.saml.SAMLHelper;
import com.mmhayes.common.utils.Logger;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

//Other Dependencies

/*
 $Author: ecdyer $: Author of last commit
 $Date: 2017-11-02 15:55:45 -0400 (Thu, 02 Nov 2017) $: Date of last commit
 $Rev: 25147 $: Revision of last commit
 Notes: For sending and receiving SAML requests
*/
public class SSOServlet extends HttpServlet implements Servlet {

    private SAMLHelper samlHelper = null; //custom helper object to help assist with serializing and de-serializing SAML

    //runs when the servlet starts up for the first time
    public void init() {
        try {
            Logger.logMessage("Initializing SAML Servlet...", Logger.LEVEL.IMPORTANT);

            //create a new SAMLHelper and set it
            setSamlHelper(new SAMLHelper());

        } catch (Exception ex) {
            Logger.logException(ex);
        }
    }

    //runs when GET requests are sent to the servlet (/sso/*)
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            handleServletRequest("GET",request,response);
        } catch (ServletException | IOException ex) {
            Logger.logException(ex);
            throw ex;
        } catch (Exception ex) {
            Logger.logException(ex);
            final String SAML_SERVLET_UNHANDLED_EXCEPTION = "An unhandled exception occured in SAML Servlet. Please review logs.";
            throw new ServletException(SAML_SERVLET_UNHANDLED_EXCEPTION);
        }
    }

    //runs when POST requests are sent to the servlet (/sso/*)
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            handleServletRequest("POST",request,response);
        } catch (ServletException | IOException ex) {
            Logger.logException(ex);
            throw ex;
        } catch (Exception ex) {
            Logger.logException(ex);
            final String SAML_SERVLET_UNHANDLED_EXCEPTION = "An unhandled exception occured in SAML Servlet. Please review logs.";
            throw new ServletException(SAML_SERVLET_UNHANDLED_EXCEPTION);
        }
    }

    //handles both GET and POST requests for this servlet, determines what to do based on requested URI
    private void handleServletRequest(String requestMethod, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String completeRequestedURL = getCompleteRequestedURL(request);
        String relayState = request.getParameter("refPage"); //where to redirect when task is completed
        Logger.logMessage("Handling "+requestMethod+" request to SAML Servlet to completeRequestedURL: "+completeRequestedURL, Logger.LEVEL.DEBUG);  //log what URI this servlet is handling
        SSOAuthHandler ssoAuthHandler = new SSOAuthHandler();

        //determine how to handle the request based on the completeRequestedURL
        if (completeRequestedURL.endsWith("/sso") || completeRequestedURL.endsWith("/sso/") || completeRequestedURL.endsWith("/sso/login")) {

            getSamlHelper().sendRequestToIdP(relayState, request, response);

        } else if (completeRequestedURL.endsWith("/acs")) {

            getSamlHelper().respondToACSRequest(request, response);
            //get the SAML Response Information from the SAMLHelper and pass it to the SSOAuthHandler
            ssoAuthHandler.handleIdPResponse(getSamlHelper().getNameID(), getSamlHelper().getSessionIndex(),  getSamlHelper().getAttributes(), request, response);

        } else if (completeRequestedURL.endsWith("/sls")) {

            // ENDPOINT FOR SINGLE LOGOUT SERVICE ie SLS
            ssoAuthHandler.sendLogoutRequestToIdP(request, response);

        } else if (completeRequestedURL.endsWith("/acls")) {

            // ENDPOINT FOR SINGLE LOGOUT SERVICE ie SLS
            ssoAuthHandler.handleIDPInitiatedLogout(request,response);

        } else if (completeRequestedURL.endsWith("/metadata")) {

            getSamlHelper().respondToMetaDataRequest(response);

        } else {
            Logger.logMessage("Could not determine how to handle the completeRequestedURL: "+completeRequestedURL, Logger.LEVEL.ERROR);
        }
    }

    //this method returns the ENTIRE url WITHOUT parameters (ex. http://server/qc/sso/) from a request object
    private String getCompleteRequestedURL(HttpServletRequest request) throws Exception {
        String completeRequestedURL = null;

        //if we have a valid request passed in...
        if (request != null && request.getRequestURL() != null) {

            //String requestedURI = request.getRequestURI(); //requested path in the URL (ex. /qc/sso/)
            completeRequestedURL = request.getRequestURL().toString();
            String lastCharOfCompleteRequestedURL = completeRequestedURL.substring(completeRequestedURL.length() - 1);

            //if there is a trailing "/" in the URL, remove it
            if (lastCharOfCompleteRequestedURL.equals("/")) {
                completeRequestedURL = completeRequestedURL.substring(0, completeRequestedURL.length() - 1);
            }

        }

        //if we couldn't determine a valid completeRequestedURL, error out
        if (completeRequestedURL == null) {
            Logger.logMessage("Could not determine completeRequestedURL in SSO Servlet. Fatal error.", Logger.LEVEL.ERROR);
            throw new RuntimeException();
        }

        return completeRequestedURL;
    }

    public SAMLHelper getSamlHelper() {
        return samlHelper;
    }

    public void setSamlHelper(SAMLHelper samlHelper) {
        this.samlHelper = samlHelper;
    }

}
