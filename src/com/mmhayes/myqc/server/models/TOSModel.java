package com.mmhayes.myqc.server.models;

//mmhayes dependencies

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.utils.Logger;

import java.util.HashMap;

//other dependencies

/* TOS Model
 Last Updated (automatically updated by SVN)
 $Author: ecdyer $: Author of last commit
 $Date: 2017-08-02 08:58:52 -0400 (Wed, 02 Aug 2017) $: Date of last commit
 $Rev: 23136 $: Revision of last commit

 Notes: Model for a single TOS
*/
public class TOSModel {
    Integer id = null; //TOSID
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String title = "N/A"; //Title of the TOS - Could be "Terms of Service" or "Privacy Policy"
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String content = "N/A"; //HTML content of TOS

    //constructor - takes hashmap that get sets to this models properties
    public TOSModel(HashMap modelDetailHM) {
        try {
            setModelProperties(modelDetailHM);
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
    }

    //setter for all of this model's properties
    public TOSModel setModelProperties(HashMap modelDetailHM) {
        try {
            if (modelDetailHM.get("ID") != null) {
                setID(Integer.parseInt(modelDetailHM.get("ID").toString()));
            }
            if (modelDetailHM.get("TITLE") != null) {
                setTitle(modelDetailHM.get("TITLE").toString());
            }
            if (modelDetailHM.get("CONTENT") != null) {
                setContent(modelDetailHM.get("CONTENT").toString());
            }
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
        return this;
    }

    //getter
    public Integer getID() {
        return this.id;
    }

    //setter
    public void setID(Integer id) {
        this.id = id;
    }

    //getter
    public String getTitle() {
        return title;
    }

    //setter
    public void setTitle(String title) {
        this.title = title;
    }

    //getter
    public String getContent() {
        return content;
    }

    //setter
    public void setContent(String content) {
        this.content = content;
    }

}
