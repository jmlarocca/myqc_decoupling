package com.mmhayes.myqc.server.models;

//mmhayes dependencies

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.dataaccess.DataManager;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

//javax.ws.rs dependencies
//myqc dependencies
//other dependencies

/* Prep Option Model
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2020-05-16 16:27:44 -0400 (Sat, 16 May 2020) $: Date of last commit
 $Rev: 49042 $: Revision of last commit

 Notes: Model for FAQs
*/
public class FAQModel {
    private static DataManager dm = new DataManager();
    Integer id = null; //ID
    boolean linkToAccountFunding = false;
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String question = "";
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String answer = "";

    //constructor - takes hashmap that get sets to this models properties
    public FAQModel(HashMap modelDetailHM) throws Exception {
        setModelProperties(modelDetailHM);
    }

    //setter for all of the favorite order model's for the My Quick Picks page
    public FAQModel setModelProperties(HashMap modelDetailHM) throws Exception{
        setID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("FAQID")));

        setLinkToAccountFunding(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("ADDACCOUNTFUNDINGLINK")));

        setQuestion(CommonAPI.convertModelDetailToString(modelDetailHM.get("QUESTION")));
        setAnswer(CommonAPI.convertModelDetailToString(modelDetailHM.get("ANSWER")));

        return this;
    }

    //getter
    public Integer getID() {
        return this.id;
    }

    //setter
    public void setID(Integer id) {
        this.id = id;
    }

    public boolean isLinkToAccountFunding() {
        return linkToAccountFunding;
    }

    public void setLinkToAccountFunding(boolean linkToAccountFunding) {
        this.linkToAccountFunding = linkToAccountFunding;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}

