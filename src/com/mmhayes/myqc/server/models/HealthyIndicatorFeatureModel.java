package com.mmhayes.myqc.server.models;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.dataaccess.DataManager;

import java.util.ArrayList;
import java.util.HashMap;

public class HealthyIndicatorFeatureModel extends FeatureModel {

    HashMap<String, Boolean> indicators = new HashMap();
    ArrayList<String> icons = new ArrayList<>();
    static DataManager dm = new DataManager();
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String available = "no"; //yes, no, partial
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String version = "N/A"; //N/A means there are no "sub versions" for this feature
    String showOnlineOrdering;

    public HealthyIndicatorFeatureModel(String id, String version, String available, String showOnlineOrdering) {
        super(id, version, showOnlineOrdering);
        this.showOnlineOrdering = showOnlineOrdering;
        setHealthyIndicators();
    }

    @Override
    public String getVersion() {
        return version;
    }

    @Override
    public String getAvailable() {
        return available;
    }

    private void setHealthyIndicators() {
        if(showOnlineOrdering == "yes") {
            ArrayList<HashMap> query = dm.parameterizedExecuteQuery("data.globalSettings.getHealthyIconAvailability", new Object[]{}, true);
            if (query.size() != 0) {
                indicators = query.get(0);
                for (String key : indicators.keySet()) {
                    Boolean available = indicators.get(key);
                    if (available) {
                        icons.add(key);
                    }
                }
                // Now turn the query into a string of available icons
                String retValue = "";
                for (int i = 0; i < icons.size(); i++) {
                    retValue += icons.get(i);
                    if (i != icons.size() - 1) {
                        retValue += ", ";
                    }
                }
                version = retValue;
                if (icons.size() > 0) {
                    available = "yes";
                }
            } else {
                available = "no";
            }
        }
    }

    // Whether to display healthy indicators on MyQC receipts
    public static boolean showHealthyIndicatorsOnReceipts() {
        Object result = dm.parameterizedExecuteScalar("data.globalSettings.getWellnessOnReceipts", new Object[]{});
        if (result != null) {
            return (boolean)result;
        }
        return false;
    }

}
