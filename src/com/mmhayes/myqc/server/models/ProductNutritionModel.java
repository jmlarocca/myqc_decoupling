package com.mmhayes.myqc.server.models;

//mmhayes dependencies

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.utils.Logger;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;

//myqc dependencies
//other dependencies

/* Modifier Model
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2018-03-23 12:40:54 -0400 (Fri, 23 Mar 2018) $: Date of last commit
 $Rev: 28380 $: Revision of last commit

 Notes: Model for "Modifier Products" - Products (a.k.a PLUs, Items, etc.) that have the IsModifier flag turned on
*/
public class ProductNutritionModel {
    private int pluID = 0;
    private String quantity = "";
    private String productName = "";
    private String nutritionInfo1 = "";
    private String nutritionInfo2 = "";
    private String nutritionInfo3 = "";
    private String nutritionInfo4 = "";
    private String nutritionInfo5 = "";

    //dummy constructor - needed to consume ANY JSON object via REST (Jersey/Jackson style)
    public ProductNutritionModel() {

    }

    public BigDecimal calculateNutritionTotal(String nutritionAmtForQtyOne)
    {
        BigDecimal nutritionTotal = new BigDecimal(nutritionAmtForQtyOne);

        // The product's total nutritional value is (nutritionalValue * productQuantity)
        nutritionTotal = nutritionTotal.multiply(new BigDecimal(quantity));

        return nutritionTotal;
    }

    //getter
    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getPluID() {
        return pluID;
    }

    public void setPluID(Integer pluID) {
        this.pluID = pluID;
    }

    public String getNutritionInfo1() {
        return nutritionInfo1;
    }

    public void setNutritionInfo1(String nutritionInfo1) {
        if(nutritionInfo1.equals("***") || nutritionInfo1.equals("")) {
            this.nutritionInfo1 = nutritionInfo1;
        } else {
            this.nutritionInfo1 = new BigDecimal(nutritionInfo1).setScale(1, RoundingMode.CEILING).toPlainString();

            if(this.nutritionInfo1.substring(this.nutritionInfo1.indexOf('.')+1, this.nutritionInfo1.length()).equals("0")) {
                this.nutritionInfo1 = new BigDecimal(nutritionInfo1).setScale(0, RoundingMode.CEILING).toPlainString();
            }
        }

    }

    public String getNutritionInfo2() {
        return nutritionInfo2;
    }

    public void setNutritionInfo2(String nutritionInfo2) {
        if(nutritionInfo2.equals("***") || nutritionInfo2.equals("")) {
            this.nutritionInfo2 = nutritionInfo2;
        } else {
            this.nutritionInfo2 = new BigDecimal(nutritionInfo2).setScale(1, RoundingMode.CEILING).toPlainString();

            if(this.nutritionInfo2.substring(this.nutritionInfo2.indexOf('.')+1, this.nutritionInfo2.length()).equals("0")) {
                this.nutritionInfo2 = new BigDecimal(nutritionInfo2).setScale(0, RoundingMode.CEILING).toPlainString();
            }
        }
    }

    public String getNutritionInfo3() {
        return nutritionInfo3;
    }

    public void setNutritionInfo3(String nutritionInfo3) {
        if(nutritionInfo3.equals("***") || nutritionInfo3.equals("")) {
            this.nutritionInfo3 = nutritionInfo3;
        } else {
            this.nutritionInfo3 = new BigDecimal(nutritionInfo3).setScale(1, RoundingMode.CEILING).toPlainString();

            if(this.nutritionInfo3.substring(this.nutritionInfo3.indexOf('.')+1, this.nutritionInfo3.length()).equals("0")) {
                this.nutritionInfo3 = new BigDecimal(nutritionInfo3).setScale(0, RoundingMode.CEILING).toPlainString();
            }
        }
    }

    public String getNutritionInfo4() {
        return nutritionInfo4;
    }

    public void setNutritionInfo4(String nutritionInfo4) {
        if(nutritionInfo4.equals("***") || nutritionInfo4.equals("")) {
            this.nutritionInfo4 = nutritionInfo4;
        } else {
            if(new BigDecimal(nutritionInfo4).setScale(1, RoundingMode.CEILING).scale() <= 0) {
                this.nutritionInfo4 = new BigDecimal(nutritionInfo4).setScale(0, RoundingMode.HALF_UP).toPlainString();
            } else {
                this.nutritionInfo4 = new BigDecimal(nutritionInfo4).setScale(1, RoundingMode.CEILING).toPlainString();

                if(this.nutritionInfo4.substring(this.nutritionInfo4.indexOf('.')+1, this.nutritionInfo4.length()).equals("0")) {
                    this.nutritionInfo4 = new BigDecimal(nutritionInfo4).setScale(0, RoundingMode.CEILING).toPlainString();
                }
            }

        }
    }

    public String getNutritionInfo5() {
        return nutritionInfo5;
    }

    public void setNutritionInfo5(String nutritionInfo5) {
        if(nutritionInfo5.equals("***") || nutritionInfo5.equals("")) {
            this.nutritionInfo5 = nutritionInfo5;
        } else {
            this.nutritionInfo5 = new BigDecimal(nutritionInfo5).setScale(1, RoundingMode.CEILING).toPlainString();

            if(this.nutritionInfo5.substring(this.nutritionInfo5.indexOf('.')+1, this.nutritionInfo5.length()).equals("0")) {
                this.nutritionInfo5 = new BigDecimal(nutritionInfo5).setScale(0, RoundingMode.CEILING).toPlainString();
            }
        }
    }

}
