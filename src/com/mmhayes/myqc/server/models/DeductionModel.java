package com.mmhayes.myqc.server.models;

//mmhayes dependencies

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.dataaccess.commonMMHFunctions;
import com.mmhayes.common.utils.Logger;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;

//other dependencies

/* Deductions Model
 Last Updated (automatically updated by SVN)
 $Author: ecdyer $: Author of last commit
 $Date: 2017-08-02 08:58:52 -0400 (Wed, 02 Aug 2017) $: Date of last commit
 $Rev: 23136 $: Revision of last commit

 Notes: Model for Employee Deductions
*/
public class DeductionModel {
    private commonMMHFunctions commFunc = new commonMMHFunctions();
    Integer id = null;
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String code = "N/A";
    BigDecimal amt = null;
    String date = null;
    private LocalDateTime dateTime = null; //also switch getters and setters to make public - 6/15/2015 -jrmitaly

    //constructor - takes hashmap that get sets to this models properties
    public DeductionModel(HashMap modelDetailHM) {
        try {
            setModelProperties(modelDetailHM);
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
    }

    //setter for all of this model's properties
    public DeductionModel setModelProperties(HashMap modelDetailHM) {
        try {

            if (modelDetailHM.get("DEDUCTIONID") != null) {
                setID(Integer.parseInt(modelDetailHM.get("DEDUCTIONID").toString()));
            }
            if (modelDetailHM.get("DEDUCTIONCODE") != null) {
                setCode(modelDetailHM.get("DEDUCTIONCODE").toString());
            }
            if (modelDetailHM.get("DEDUCTIONAMOUNT") != null) {
                setAmt(new BigDecimal(modelDetailHM.get("DEDUCTIONAMOUNT").toString()));
            }
            if (modelDetailHM.get("PERIODENDDATE") != null) {
                try {

                    //specify in and out formats for formatting modelDetailHM.get("PERIODENDDATE")
                    String dateTimeInFormat = "yyyy-MM-dd HH:mm:ss.S";
                    String dateTimeOutFormat = "M/dd/yyyy h:mm:ss a"; //old way was - M/dd/yyyy H:mm:ss - jrmitaly 6/23/2015

                    //format modelDetailHM.get("PERIODENDDATE") and set to dateTimeStr
                    String dateTimeStr = commFunc.formatDateTime(modelDetailHM.get("PERIODENDDATE").toString(),dateTimeInFormat,dateTimeOutFormat);

                    //parse dateTimeStr to LocalDateTime and set that to this.dateTime
                    DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(dateTimeOutFormat);
                    setDateTime(LocalDateTime.parse(dateTimeStr, dateTimeFormatter));

                    //set this.date
                    //setDate(dateTimeStr.split(" ")[0]); //for just the date
                    setDate(dateTimeStr); //for date and time

                } catch (Exception ex) {
                    Logger.logMessage("ERROR: Issue with date formatting in DeductionModel.setModel()", Logger.LEVEL.ERROR);
                    Logger.logException(ex); //always log exceptions
                }
            }
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
        return this;
    }

    //getter
    public Integer getID() {
        return this.id;
    }

    //setter
    public void setID(Integer id) {
        this.id = id;
    }

    //getter
    public String getCode() {
        return code;
    }

    //setter
    public void setCode(String code) {
        this.code = code;
    }

    //getter
    public BigDecimal getAmt() {
        return amt;
    }

    //setter
    public void setAmt(BigDecimal amt) {
        this.amt = amt;
    }

    //getter
    public String getDate() {
        return date;
    }

    //setter
    public void setDate(String date) {
        this.date = date;
    }

    //getter - PRIVATE SO WE DON'T RETURN WITH API, switch to public to return - 6/15/2015 -jrmitaly
    private LocalDateTime getDateTime() {
        return dateTime;
    }

    //setter - PRIVATE SO WE DON'T RETURN WITH API, switch to public to return - 6/15/2015 -jrmitaly
    private void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

}
