package com.mmhayes.myqc.server.models;

//mmhayes dependencies

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.dataaccess.commonMMHFunctions;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;

//other dependencies

/* Transactions Model
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2020-04-29 09:51:12 -0400 (Wed, 29 Apr 2020) $: Date of last commit
 $Rev: 48472 $: Revision of last commit

 Notes: Model for QC Transactions
*/
public class TransactionModel {
    private commonMMHFunctions commFunc = new commonMMHFunctions();
    Integer id = null;
    Integer paTransactionID = null;
    Integer transTypeID = null;
    Integer refundID = null;
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String store = "N/A";
    BigDecimal amt = null;
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String date = null;
    Integer PAOrderTypeID = null;
    boolean terminalInProfile = false;
    boolean showReceiptsOnMyQC = false;
    boolean enableFavorites = false;
    boolean duplicateTxn = false;
    private LocalDateTime dateTime = null; //also switch getters and setters to make public - 6/12/2015 -jrmitaly

    //constructor - takes hashmap that get sets to this models properties
    public TransactionModel(HashMap modelDetailHM) throws Exception {
        setModelProperties(modelDetailHM);
    }

    //setter for all of this model's properties
    public TransactionModel setModelProperties(HashMap modelDetailHM) throws Exception {
        if (modelDetailHM.get("TRANSID") != null) {
            setID(Integer.parseInt(modelDetailHM.get("TRANSID").toString()));
        }
        if (modelDetailHM.get("PATRANSID") != null && !modelDetailHM.get("PATRANSID").toString().equals("")) {
            setPATransactionID(Integer.parseInt(modelDetailHM.get("PATRANSID").toString()));
        }
        if (modelDetailHM.get("TRANSSTORE") != null) {
            setStore(modelDetailHM.get("TRANSSTORE").toString());
        }
        if (modelDetailHM.get("TRANSAMOUNT") != null) {
            setAmt(new BigDecimal(modelDetailHM.get("TRANSAMOUNT").toString()));
        }
        if (modelDetailHM.get("TRANSDATE") != null) {

                //specify in and out formats for formatting modelDetailHM.get("TRANSDATE")
                String dateTimeInFormat = "yyyy-MM-dd HH:mm:ss.S";
                String dateTimeOutFormat = "M/dd/yyyy h:mm:ss a"; //old way was - M/dd/yyyy H:mm:ss - jrmitaly 6/23/2015

                //format modelDetailHM.get("TRANSDATE") and set to dateTimeStr
                String dateTimeStr = commFunc.formatDateTime(modelDetailHM.get("TRANSDATE").toString(),dateTimeInFormat,dateTimeOutFormat);

                //parse dateTimeStr to LocalDateTime and set that to this.dateTime
                DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(dateTimeOutFormat);
                setDateTime(LocalDateTime.parse(dateTimeStr, dateTimeFormatter));

                //set this.date
                //setDate(dateTimeStr.split(" ")[0]); //for just the date
                setDate(dateTimeStr); //for date and time
        }

        setPAOrderTypeID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAORDERTYPEID")));

        if ( modelDetailHM.get("TERMINALINPROFILE") != null && modelDetailHM.get("TERMINALINPROFILE").toString().equals("1")) {
            setTerminalInProfile(true);
        }

        if ( modelDetailHM.get("SHOWRECEIPTSONMYQC") != null && CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("SHOWRECEIPTSONMYQC")) ) {
            setShowReceiptsOnMyQC(true);
        }

        if ( modelDetailHM.get("ENABLEFAVORITES") != null && !modelDetailHM.get("ENABLEFAVORITES").toString().equals("") ) {
            setEnableFavorites(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("ENABLEFAVORITES")));
        }

        if ( modelDetailHM.get("TRANSTYPEID") != null && !modelDetailHM.get("TRANSTYPEID").toString().equals("") ) {
            setTransTypeID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("TRANSTYPEID")));
        }

        if ( modelDetailHM.get("REFUNDID") != null && !modelDetailHM.get("REFUNDID").toString().equals("") ) {
            setRefundID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("REFUNDID")));
        }

        return this;
    }

    //getter
    public Integer getID() {
        return this.id;
    }

    //setter
    public void setID(Integer id) {
        this.id = id;
    }

    //getter
    public Integer getPATransactionID() {
        return this.paTransactionID;
    }

    //setter
    public void setPATransactionID(Integer paTransactionID) {
        this.paTransactionID = paTransactionID;
    }

    //getter
    public String getStore() {
        return store;
    }

    //setter
    public void setStore(String store) {
        this.store = store;
    }

    //getter
    public BigDecimal getAmt() {
        return amt;
    }

    //setter
    public void setAmt(BigDecimal amt) {
        this.amt = amt;
    }

    //getter
    public String getDate() {
        return date;
    }

    //setter
    public void setDate(String date) {
        this.date = date;
    }

    public Integer getPAOrderTypeID() {
        return PAOrderTypeID;
    }

    public void setPAOrderTypeID(Integer PAOrderTypeID) {
        this.PAOrderTypeID = PAOrderTypeID;
    }

    //getter - PRIVATE SO WE DON'T RETURN WITH API, switch to public to return - 6/12/2015 -jrmitaly
    private LocalDateTime getDateTime() {
        return dateTime;
    }

    //setter - PRIVATE SO WE DON'T RETURN WITH API, switch to public to return - 6/12/2015 -jrmitaly
    private void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public boolean isTerminalInProfile() {
        return terminalInProfile;
    }

    public void setTerminalInProfile(boolean terminalInProfile) {
        this.terminalInProfile = terminalInProfile;
    }

    public boolean isShowReceiptsOnMyQC() {
        return showReceiptsOnMyQC;
    }

    public void setShowReceiptsOnMyQC(boolean showReceiptsOnMyQC) {
        this.showReceiptsOnMyQC = showReceiptsOnMyQC;
    }

    public boolean isEnableFavorites() {
        return enableFavorites;
    }

    public void setEnableFavorites(boolean enableFavorites) {
        this.enableFavorites = enableFavorites;
    }

    public Integer getTransTypeID() {
        return transTypeID;
    }

    public void setTransTypeID(Integer transTypeID) {
        this.transTypeID = transTypeID;
    }

    public Integer getRefundID() {
        return refundID;
    }

    public void setRefundID(Integer refundID) {
        this.refundID = refundID;
    }

    public boolean isDuplicateTxn() {
        return duplicateTxn;
    }

    public void setDuplicateTxn(boolean duplicateTxn) {
        this.duplicateTxn = duplicateTxn;
    }

}
