package com.mmhayes.myqc.server.models;


//mmhayes dependencies

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.account.models.AccountModel;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.CommonAuthResource;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.api.PosApi30.TransactionBuilder;
import com.mmhayes.common.api.errorhandling.exceptions.InvalidAuthException;
import com.mmhayes.common.api.errorhandling.exceptions.InvalidVoucherException;
import com.mmhayes.common.api.errorhandling.exceptions.MissingDataException;
import com.mmhayes.common.api.errorhandling.exceptions.TransactionIndividualLimitException;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.donation.collections.DonationCollection;
import com.mmhayes.common.donation.models.DonationLineItemModel;
import com.mmhayes.common.donation.models.DonationModel;
import com.mmhayes.common.funding.collections.AccountPaymentMethodCollection;
import com.mmhayes.common.funding.collections.FundingAmountCollection;
import com.mmhayes.common.funding.models.*;
import com.mmhayes.common.login.LoginModel;
import com.mmhayes.common.loyalty.models.LoyaltyAccountModel;
import com.mmhayes.common.loyalty.models.LoyaltyPointModel;
import com.mmhayes.common.product.models.ItemTaxModel;
import com.mmhayes.common.terminal.models.TerminalModel;
import com.mmhayes.common.transaction.models.*;
import com.mmhayes.common.transaction.models.TransactionModel;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.voucher.models.VoucherCodeModel;
import com.mmhayes.myqc.server.api.MyQCCache;
import com.mmhayes.myqc.server.collections.ProductCollection;
import org.owasp.esapi.ESAPI;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

//myqc dependencies
//other dependencies


/* Order Model
 Last Updated (automatically updated by SVN)
 $Author: ijgerstein $: Author of last commit
 $Date: 2021-09-20 10:51:48 -0400 (Mon, 20 Sep 2021) $: Date of last commit
 $Rev: 58158 $: Revision of last commit

 Notes: Model for order object
*/
public class OrderModel {
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String type = ""; //delivery or pickup order
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String location = ""; //delivery location
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String time = ""; //delivery or pickup selected time, such as 1:45 PM or ASAP
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String phone = ""; //customer phone number
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String mobilePhone = ""; //customer mobile phone number
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String comments = ""; //order comments
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String errorDetails = ""; //holds any error message
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String personName = ""; //holds the user's name
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String orderNumber = ""; //holds the order number to be displayed back to the user after successful submit
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String readyTime = ""; //after successful submit tells the user when the order will be delivered or ready to pick up
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String submittedTime = ""; //from the client - the time their phone/browser says it put in the order
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String estimatedOrderTime = ""; //the time the order will be ready
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String diningOptionPAPluID = ""; //the product id chosen in the Dining Options selector
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String creditCardFeeLabel = ""; //the product id chosen in the Dining Options selector
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String token = ""; // the token corresponding to the current transaction
    String diningOptionPAPluName = ""; //the name of the product in the Dining Options selector
    boolean forceReAuth = true; //setting to force user to reauth before purchasing, default to true
    boolean advanceScreen = false; //setting determine whether to advance to the review screen or not - some errors need to be displayed on the review screen, others need to stop on the receive page
    boolean useMealPlanDiscount = true; //setting to determine whether to allow the use of a subtotal coupon discount
    Integer reAuthTypeID = 0; //to show rauth at all, and if so, whether or not to show the checkbox
    BigDecimal subtotal = null; //order subtotal; before taxes, fees & subtotal discount
    BigDecimal subtotalDiscount = null; //order discount from subtotal
    BigDecimal rewardsTotal = null; //total amount of discount from rewards
    BigDecimal surchargeTotal = null; //total amount of fee from surcharges
    BigDecimal tax = null; //order tax
    BigDecimal deliveryFee = null; //order delivery fee
    BigDecimal deliveryMinimum = null; //order delivery minimum
    BigDecimal creditCardFee = null; //credit card fee
    BigDecimal total = null; //order total after taxes, fees and discounts
    Integer transactionID = null; //PATransaction ID returned by POS API after order is submitted
    Integer reorderTransactionID = 0; //PATransactionID used to determine if the express reorder needs to get details from the transactionID or the last transaction in the store
    Integer personID = null; //personID passed in to determine the person name for the receipt
    List<ProductModel> products = new ArrayList<>(); //this Array List represents a ProductCollection which contains a list of ProductModels
    List<LoyaltyRewardLineItemModel> rewards = new ArrayList<>(); //contains all rewards that are to be applied on the transaction
    List<ComboLineItemModel> combos = new ArrayList<>(); //contains all combos that are to be applied on the transaction
    List<SurchargeLineItemModel> itemSurcharges = new ArrayList<>(); //list of item surcharges for the transaction to list out each one
    List<DonationLineItemModel> donations = new ArrayList<>(); //list of donations for the transaction
    List<LoyaltyPointModel> loyaltyPointsEarned = new ArrayList<>(); //list of loyalty points that were earned on the transaction
    List<DonationModel> employeeDonations = new ArrayList<>(); //list of all donations available for the employee
    ArrayList<HashMap> selectedDiscountList = new ArrayList<>(); //contains overall discount data
    LocalDate futureOrderDate = null; //the date the user can choose to place an order in the future
    PrepOptionModel prepOption = null; //contains prep option details for the product
    AccountFundingDetailModel accountFundingDetailModel = null; //contains details about manual funding
    BigDecimal userBalance = null; //contains user's balance for manual funding
    BigDecimal customFundingAmount = null; //contains user's balance for manual funding
    boolean ignoreInventoryCounts = false;
    boolean isUsingMobile = false; //setting to determine if saving the phone numbers as the mobile phone number or not
    boolean useCreditCardAsTender = false; //setting to determine if using a credit card or quickcharge tender
    boolean useDiningOptions = false; //setting to determine if using a credit card or quickcharge tender
    boolean expressReorder = false; //setting to determine if this an express reorder
    boolean chargeAtPurchaseTime = true; //setting to determine if the user will be charged at purchase time or pickup time - sale vs open transaction
    boolean grabAndGo = false; //setting to determine if this a grab and go transaction and should be counted towards buffering
    String voucherCode = null; //voucher code to apply to the transaction
    BigDecimal voucherTotal = null; //total amount from applied voucher
    private Integer authenticatedAccountID = null;
    DataManager dm = new DataManager();

    public OrderModel() {

    }

    //**** START ORDER REVIEW ****//

    //reviews the order and calculates and sets the order total
    public void reviewOrder(HttpServletRequest request) throws Exception {
        //determine the authenticated account ID from the request
        setAuthenticatedAccountID(CommonAuthResource.checkRequestHeaderForMMHayesAuth(request));

        Integer storeID = CommonAPI.checkRequestHeaderForStoreID(request);

        if ( getAuthenticatedAccountID() == null ) {
            Logger.logMessage("ERROR: Could not determine authenticated accountID in reviewOrder()", Logger.LEVEL.ERROR);
            throw new InvalidAuthException("An error occurred while validating your order, please try again.");
        }

        if(this.getDiningOptionPAPluID() != null && !this.getDiningOptionPAPluID().equals("") && !isExpressReorder()) {
            addEatInTakeOutProduct(storeID);
        }

        //if the person account is managing the employee, set the person account's name as the PersonName
        if(this.getPersonID() != null) {
            String personName = determinePersonAccountName();
            setPersonName(personName);
        }

        if ( getProducts().size() == 0 ) {
            setErrorDetails("No products found on order");
            return;
        }

        //ensure the time of the order is valid for the given store
        if ( !validateOrderTime(storeID, request) ) {
            return;
        }

        //get inventory related information for the products
        ProductCollection productCollection = new ProductCollection(getProducts(), request);

        //check for errors
        if ( productCollection.getMessage().length() > 0 ) {
            setErrorDetails(productCollection.getMessage());
            return;
        }


        //check inventory
        if ( !this.checkInventoryValidForOrder(request, productCollection) ) {
            return;
        }

        //ensure all products are set to their original price - this is a secondary check as this is dealt with on the front now from 2.0.38 onward - 11/12/18 JMD
        for ( int i = 0; i < this.getProducts().size(); i++ ) {
            this.getProducts().get( i ).setPrice( productCollection.getCollection().get( i ).getOriginalPrice() );
        }

        TransactionModel transactionModel = this.transactionInquiry(request);

        //look through combos and get subtotal
        BigDecimal comboSubtotal = BigDecimal.ZERO;
        for(ComboLineItemModel comboLineItemModel : transactionModel.getCombos()) {
            BigDecimal comboAmount = comboLineItemModel.getAmount().multiply(comboLineItemModel.getQuantity());
            comboSubtotal = comboSubtotal.add(comboAmount);
        }

        //look through products and set subtotal
        BigDecimal productSubtotal = BigDecimal.ZERO;
        for (ProductLineItemModel productLineItemModel : transactionModel.getProducts() ) {

            if(productLineItemModel.getComboTransLineItemId() != null) {
                continue;
            }

            productSubtotal = productSubtotal.add(productLineItemModel.getExtendedAmount().setScale(2, BigDecimal.ROUND_HALF_UP));

            if ( productLineItemModel.getModifiers() != null && productLineItemModel.getModifiers().size() > 0 ) {
                for (ModifierLineItemModel modifierLineItemModel : productLineItemModel.getModifiers() ) {

                    if ( modifierLineItemModel.getAmount() == null || modifierLineItemModel.getAmount().compareTo(BigDecimal.ZERO) == 0 ) {
                        continue;
                    }

                    if(productLineItemModel.getProduct().isWeighted()){
                        productSubtotal = productSubtotal.add(modifierLineItemModel.getAmount().setScale(2, BigDecimal.ROUND_HALF_UP));
                    }
                    else{
                        productSubtotal = productSubtotal.add( modifierLineItemModel.getExtendedAmount().setScale(2, BigDecimal.ROUND_HALF_UP));
                    }
                }
            }

            // If the product is weighted, add the prep option total after the extended amount has been calculated with the base price.
            if(productLineItemModel.getPrepOption() != null && productLineItemModel.getProduct().isWeighted()){
               productSubtotal = productSubtotal.add(productLineItemModel.getPrepOption().getPrice());
            }

        }

        productSubtotal = productSubtotal.add(comboSubtotal);
        setSubtotal(productSubtotal);

        //for each TransactionModel -> Discounts line, sum the total discount amounts
        BigDecimal discountTotal = BigDecimal.ZERO;
        for (DiscountLineItemModel discountLineItemModel : transactionModel.getDiscounts() ) {
            if( discountLineItemModel.getDiscount().isSubTotalDiscount() ) {
                discountTotal = discountTotal.add(BigDecimal.valueOf(-1).multiply(discountLineItemModel.getAmount()));
            }
        }
        setSubtotalDiscount(discountTotal.setScale(2, BigDecimal.ROUND_HALF_UP));

        //for each TransactionModel -> Rewards line, sum the total reward amounts
        BigDecimal rewardTotal = BigDecimal.ZERO;
        for (LoyaltyRewardLineItemModel loyaltyRewardLineItemModel : transactionModel.getRewards() ) {
            rewardTotal = rewardTotal.add(BigDecimal.valueOf(-1).multiply(loyaltyRewardLineItemModel.getAmount()));
        }
        setRewardsTotal(rewardTotal.setScale(2, BigDecimal.ROUND_HALF_UP));

        //for each TransactionModel -> Taxes line, sum the total tax amounts
        BigDecimal taxTotal = BigDecimal.ZERO;
        for (TaxLineItemModel taxLineItemModel : transactionModel.getTaxes() ) {
            if( !taxLineItemModel.getItemTypeId().equals(PosAPIHelper.ObjectType.TAX_DELETE.toInt())) {
                taxTotal = taxTotal.add(taxLineItemModel.getAmount().setScale(2, BigDecimal.ROUND_HALF_UP));
            }
        }
        setTax(taxTotal.setScale(2, BigDecimal.ROUND_HALF_UP));

        // For each product in the TransactionModel, set the total tax applied on the parent product (for purchase restriction calculations)
        for (int i = 0; i < transactionModel.getProducts().size(); i++) {
            ProductLineItemModel productLineItemModel = transactionModel.getProducts().get(i);
            String taxIds = productLineItemModel.getProduct().getTaxIds();
            ArrayList<String> taxIdsArray = new ArrayList<>();
            if (taxIds != null && taxIds.length() > 0) {
                taxIdsArray = CommonAPI.convertCommaSeparatedStringToArrayList(taxIds, true);
            }

            BigDecimal productTaxTotal = BigDecimal.ZERO;
            for (ItemTaxModel itemTaxModel : productLineItemModel.getTaxes()) {
                // Only add to the tax total if the tax ID is mapped to the parent product
                for (String parentTaxId : taxIdsArray) {
                    if (Integer.parseInt(parentTaxId) == itemTaxModel.getItemId()) {
                        productTaxTotal = productTaxTotal.add(itemTaxModel.getAmount().setScale(4, BigDecimal.ROUND_HALF_UP));
                        break;
                    }
                }
            }

            this.getProducts().get(i).setTaxTotal(productTaxTotal.setScale(2, BigDecimal.ROUND_HALF_UP));
        }

        //create a list of Item Surcharge Models, combine multiple surcharges of the same ID as one surcharge model
        BigDecimal surchargeTotal = setItemSurchargeAmounts(transactionModel);
        setSurchargeTotal(surchargeTotal.setScale(2, BigDecimal.ROUND_HALF_UP));

        BigDecimal total = determineOrderTotal();

        // Set voucher total
        for (TenderLineItemModel tender : transactionModel.getTenders()) {
            if (tender.getVoucherCode() != null) {
                setVoucherTotal(tender.getAmount());
                break;
            }
        }

        // Calculate the voucher-adjusted total
        BigDecimal nonVoucherTotal = total;
        if (nonVoucherTotal != null && this.getVoucherTotal() != null) {
            nonVoucherTotal = nonVoucherTotal.subtract(this.getVoucherTotal()).setScale(2, BigDecimal.ROUND_HALF_UP);
        }

        if (getUseCreditCardAsTender() && nonVoucherTotal != null && nonVoucherTotal.compareTo(BigDecimal.ZERO) > 0) {
            FundingModel fundingModel = new FundingModel(getAuthenticatedAccountID(), false);

            //if using the merchant account of the online ordering store, update payment processor details
            if(transactionModel.getTerminal().getUseOnlineOrderingFunding()) {
                fundingModel = updatePaymentMethodForOnlineOrdering(fundingModel, transactionModel.getTerminalId());
            }

            if(fundingModel != null) {
                BigDecimal totalWithEligibleSurcharges = determineCreditCardSurchargeTotal();
                BigDecimal paymentFee = fundingModel.getAccountPaymentMethod().calculatePaymentFee(totalWithEligibleSurcharges).getFundingFee();

                if(paymentFee != null) {
                    setCreditCardFee(paymentFee);
                    total = total.add(paymentFee);
                    setCreditCardFeeLabel(fundingModel.getAccountPaymentMethod().getSurchargeName());
                }
            }
        }

        if ( total == null || !getErrorDetails().equals("") ) {
            return;
        }

        setTotal(total);

        // If credit card as a tender is not selected
        if( !getUseCreditCardAsTender() ) {
            checkUserBalance(request, storeID);

        } else if ( getType().equals("delivery") && getDeliveryMinimum() != null && total.compareTo(getDeliveryMinimum()) == -1) { //check delivery minimum against final total
            setErrorDetails("You did not reach the $"+ getDeliveryMinimum().setScale(2, BigDecimal.ROUND_CEILING) +" minimum amount required for delivery. You can go back to add items.");
            setAdvanceScreen(true);
        }

        determineOrderReAuthStatus(request);
    }

    //determines the final order total after discount, taxes, fees
    private BigDecimal determineOrderTotal() throws Exception {
        BigDecimal total = getSubtotal();

        //return error if there is no total at this point
        if (total == null) {
            Logger.logMessage("ERROR: Could not determine order subtotal.", Logger.LEVEL.ERROR);
            setErrorDetails("An error occurred while calculating your order total.");
            return null;
        }

        if ( getSubtotalDiscount() != null ) {
            total = total.subtract(getSubtotalDiscount());
        }

        if ( getTax() != null )  {
            total = total.add(getTax());
        }

        if ( getDeliveryFee() != null )  {
            total = total.add(getDeliveryFee());
        }

        if ( getRewardsTotal() != null ) {
            total = total.subtract(getRewardsTotal());
        }

        if ( getSurchargeTotal() != null ) {
            total = total.add(getSurchargeTotal());
        }

        return total.setScale(2, BigDecimal.ROUND_HALF_EVEN);
    }

    //gets the user's balance and checks against the order total, setting error details if necessary
    private void checkUserBalance(HttpServletRequest request, Integer storeID) throws Exception {
        BigDecimal total = getTotal();

        //check delivery minimum against final total
        if ( !isChargeAtPurchaseTime() && getType().equals("delivery") && getDeliveryMinimum() != null && total.compareTo(getDeliveryMinimum()) == -1) {
            setErrorDetails("You did not reach the $"+ getDeliveryMinimum().setScale(2, BigDecimal.ROUND_CEILING) +" minimum amount required for delivery. You can go back to add items.");
            setAdvanceScreen(true);
        }

        if( !isChargeAtPurchaseTime() )  {
            return;
        }

        //get user's current balance
        HashMap userBalanceHM = CommonAPI.getUserBalance(request, total);
        BigDecimal userBalance = CommonAPI.convertModelDetailToBigDecimal(userBalanceHM.get("USERBALANCE"));

        if ( userBalance.compareTo(BigDecimal.ZERO) < 0 ) {
            userBalance = BigDecimal.ZERO;
        }

        //check delivery minimum against final total
        if ( getType().equals("delivery") && getDeliveryMinimum() != null && total.compareTo(getDeliveryMinimum()) == -1) {
            setErrorDetails("You did not reach the $"+ getDeliveryMinimum().setScale(2, BigDecimal.ROUND_CEILING) +" minimum amount required for delivery. You can go back to add items.");
            setAdvanceScreen(true);
        }

        if ( getErrorDetails().equals("") ) {
            setAdvanceScreen(true);
        }

        //check total against user's current balance
        BigDecimal requiredFunds = total;
        if (this.getVoucherTotal() != null) {
            requiredFunds = requiredFunds.subtract(this.getVoucherTotal());
        }
        if ( requiredFunds.compareTo(userBalance) == 1 && getErrorDetails().length() == 0 ) {
            String userBalanceType = CommonAPI.convertModelDetailToString(userBalanceHM.get("USERBALANCETYPE"));
            String userBalanceAmtStr = userBalance.setScale(2, BigDecimal.ROUND_FLOOR).toString();

            String errorMsg = "You do not have enough available funds to complete this transaction.";

            if( Double.parseDouble(userBalanceAmtStr) < 0.00) {
                userBalanceAmtStr = "0.00";
            }

            StoreModel storeModel = StoreModel.getStoreModel(storeID, request);
            BigDecimal maxOverdraftAmount = storeModel.getMaxOverdraftAmount() == null ? BigDecimal.ZERO : storeModel.getMaxOverdraftAmount();

            BigDecimal actualUserBalance = CommonAPI.convertModelDetailToBigDecimal(userBalanceHM.get("USERBALANCE"));

            //check and auto fund the account if possible, otherwise show error
            if(checkForAutoOrManualFunding(requiredFunds, actualUserBalance, maxOverdraftAmount) && !userBalanceType.equals("transaction") && !userBalanceType.equals("store")) {
                return;
            } else if(getUserBalance() != null && getUserBalance().compareTo(BigDecimal.ZERO) == 0) {  //if the user balance is 0 or not null
                return;
            }  else {

                if ( userBalanceType.equals("store") ) {
                    errorMsg = errorMsg.concat(" You have $".concat(userBalanceAmtStr).concat(" available to spend in this location."));
                } else if ( userBalanceType.equals("global") ) {
                    errorMsg = errorMsg.concat(" You have $".concat(userBalanceAmtStr).concat(" available to spend."));
                } else if ( userBalanceType.equals("transaction") ) {
                    errorMsg = errorMsg.concat(" You have $".concat(userBalanceAmtStr).concat(" available to spend in this location today."));
                }

                errorMsg = errorMsg.concat(" You can go back to your cart to remove items or cancel your order.");

                //check delivery minimum against final total
                if ( getType().equals("delivery") && getDeliveryMinimum() != null && userBalance.compareTo(getDeliveryMinimum()) == -1) {
                    errorMsg = "You do not have enough available funds to order delivery from this store. Your remaining available is $" + userBalance.setScale(2, BigDecimal.ROUND_FLOOR) + ", but the delivery minimum is $" + getDeliveryMinimum().setScale(2, BigDecimal.ROUND_CEILING);
                }

                setErrorDetails(errorMsg);
            }
        }
    }

    //check if the account could be auto funded enough to pay for the order total or can be manual funded
    public boolean checkForAutoOrManualFunding(BigDecimal orderTotal, BigDecimal userBalance, BigDecimal maxOverdraftAmount) {
        try {
            AccountFundingDetailModel accountFundingDetailModel = new AccountFundingDetailModel(getAuthenticatedAccountID());

            // If funding is not allowed, return false
            if( !accountFundingDetailModel.isAccountFundingAllowed() ) {
                return false;
            }

            AccountAutoFundingModel accountFundingModel = new AccountAutoFundingModel(getAuthenticatedAccountID());

            //if the account is set up for auto reloads
            if(accountFundingModel.getReloadPaymentMethodID() != null) {

                BigDecimal amountAfterReload = userBalance.subtract(orderTotal);

                if(maxOverdraftAmount.compareTo(BigDecimal.ZERO) > 0) {
                    maxOverdraftAmount = maxOverdraftAmount.multiply(BigDecimal.valueOf(-1));
                }

                //if the order total is less than or equal to the amount after the auto reload, go through with the auto reload
                if(amountAfterReload.compareTo(maxOverdraftAmount) >= 0) {
                    return true;
                }
            }

            AccountPaymentMethodCollection accountPaymentMethodCollection = new AccountPaymentMethodCollection(getAuthenticatedAccountID());

            AccountPaymentMethodModel accountPaymentMethodModel = null;
            if(accountPaymentMethodCollection.getCollection().size() > 0) {
                accountPaymentMethodModel = accountPaymentMethodCollection.getCollection().get(0);
            }

            //if the account has a payment method
            if(accountPaymentMethodModel != null) {

                accountFundingDetailModel.setAccountPaymentMethods(accountPaymentMethodCollection.getCollection());

                //check if there are any funding amounts that are great enough to complete the order
                List<FundingAmountModel> fundingAmounts = new ArrayList<>();
                for(FundingAmountModel fundingAmountModel: new FundingAmountCollection(getAuthenticatedAccountID()).getCollection()) {
                    BigDecimal addedFundingAmount = userBalance.add(fundingAmountModel.getAmount());
                    if( addedFundingAmount.compareTo(orderTotal) >= 0 ) {
                        fundingAmounts.add(fundingAmountModel);
                    }
                }

                accountFundingDetailModel.setFundingAmounts(fundingAmounts);

                //if no valid funding amounts have been found, return the amount that is enough to pay for the order
                if(fundingAmounts.size() == 0) {
                    BigDecimal customFundingAmount = orderTotal.subtract(userBalance);
                    setCustomFundingAmount(customFundingAmount);
                }

                setAccountFundingDetailModel(accountFundingDetailModel);
                setUserBalance(userBalance);

            } else {
                setUserBalance(userBalance);
            }


        } catch (Exception e) {
            Logger.logMessage("Error attempting to check if account can be automatically funded before order submission.. in OrderModel.checkForAutoReloadFunding!", Logger.LEVEL.ERROR);
        }

        return false;
    }

    //validate the given Time, submittedTime and orderType on the orderModel against the store's schedules
    private boolean validateOrderTime(Integer storeID, HttpServletRequest request) throws Exception {
        ArrayList<String> openTimes, closeTimes;
        LocalTime orderingOpenTime, orderingCloseTime;
        String orderingOpen, orderingClose;

        DateTimeFormatter dtformatter = DateTimeFormatter.ofPattern("h:mm a");

        boolean validOrderTime = false;
        boolean roundTimeUpBeforeCheck = false;

        //create new store model
        StoreModel storeModel = StoreModel.getStoreModel(storeID, request);

        //if there is no valid store model returned, it could be the store has been removed from the user's spending profile
        if ( storeModel == null || storeModel.getID() == null ) {
            Logger.logMessage("Invalid store model returned when checking for a valid order time for storeID: " + storeID, Logger.LEVEL.DEBUG);
            return false;
        }

        Integer prepTime = storeModel.getPickUpPrepTime();

        //check order type
        if ( getType().equals("pickup") ) {
            //ordering time for checking against submitted time
            orderingOpen = storeModel.getPickUpOrderingOpenTime();
            orderingClose = storeModel.getPickUpOrderingCloseTime();

            if ( orderingOpen.length() == 0 || orderingClose.length() == 0 ) {
                Logger.logMessage("ERROR: no ordering times returned for order with order type: " + getType(), Logger.LEVEL.ERROR);
                setErrorDetails("This store is not currently accepting pickup orders.");
                if(!storeModel.isPrinterHostOnline()) {
                    setErrorDetails("This store is not currently accepting orders.");
                }
                return false;
            }

            orderingOpenTime = LocalTime.parse(orderingOpen, dtformatter);
            orderingCloseTime = LocalTime.parse(orderingClose, dtformatter);

            openTimes = storeModel.getPickUpOpenTimes();
            closeTimes = storeModel.getPickUpCloseTimes();

        } else {
            prepTime += storeModel.getDeliveryPrepTime();

            //get delivery ordering times
            orderingOpen = storeModel.getDeliveryOrderingOpenTime();
            orderingClose = storeModel.getDeliveryOrderingCloseTime();

            if ( orderingOpen.length() == 0 || orderingClose.length() == 0 ) {
                Logger.logMessage("ERROR: no ordering times returned for order with order type: " + getType(), Logger.LEVEL.ERROR);
                setErrorDetails("This store is not currently accepting delivery orders.");
                if(!storeModel.isPrinterHostOnline()) {
                    setErrorDetails("This store is not currently accepting orders.");
                }
                return false;
            }

            orderingOpenTime = LocalTime.parse(orderingOpen, dtformatter);
            orderingCloseTime = LocalTime.parse(orderingClose, dtformatter);

            openTimes = storeModel.getDeliveryOpenTimes();
            closeTimes = storeModel.getDeliveryCloseTimes();

        }

        if ( orderingCloseTime == null || orderingOpenTime == null ) {
            Logger.logMessage("ERROR: invalid time found attempting to check submitted time in validateOrderTime for storeID: " + storeID + " and order type: " + getType(), Logger.LEVEL.ERROR);
            setErrorDetails("An error occurred while processing your order, please try again later.");
            return false;
        }

        LocalDateTime orderSubmittedDateTime = LocalDateTime.now();

        //adjust for the terminal timezone offset if applicable
        if ( storeModel.getTimezoneOffset() != null && storeModel.getTimezoneOffset() != 0 ) {
            orderSubmittedDateTime = orderSubmittedDateTime.plusHours(storeModel.getTimezoneOffset());
        }

        LocalTime orderSubmittedTime = orderSubmittedDateTime.toLocalTime();

        //set submitted time string for use in submit transaction later
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
        setSubmittedTime(orderSubmittedDateTime.format(dtf));

        //check submitted time is between orderingOpenTime and orderingCloseTime
        if ( orderingOpenTime.compareTo(orderSubmittedTime) > 0 || orderingCloseTime.compareTo(orderSubmittedTime) < 0 || openTimes == null || openTimes.size() == 0 || closeTimes == null || closeTimes.size() == 0) {
            Logger.logMessage("ERROR: bad ordering time or no times returned for order with order type: " + getType(), Logger.LEVEL.ERROR);
            setErrorDetails("This store is not currently accepting " + this.getType() + " orders.");
            return false;
        }

        //open and close times have to be the same size
        if ( openTimes.size() != closeTimes.size() ) {
            Logger.logMessage("ERROR: number of open and close times do not match", Logger.LEVEL.ERROR);
            setErrorDetails("An error occurred while processing your order, please try again later");
            return false;
        }

        LocalTime ASAP;
        if(getType().equals("pickup") || getType().equals("notChosen")) {
            ASAP = storeModel.determineASAPTime(storeModel.getPickUpOpenTimes(), storeModel.getPickUpCloseTimes(), storeModel.getPickUpPrepTime());
        } else {
            Integer deliveryPrepTime = storeModel.getPickUpPrepTime() + storeModel.getDeliveryPrepTime();
            ASAP = storeModel.determineASAPTime(storeModel.getDeliveryOpenTimes(), storeModel.getDeliveryCloseTimes(), deliveryPrepTime);
        }
        String ASAPStr = "ASAP (" + CommonAPI.convertFromMilitaryTime(ASAP.toString()) + ")";

        if(isExpressReorder() && getTime().equals("")) {
            setTime(ASAPStr);
        } else if (!getTime().equals("") && !getTime().toUpperCase().contains("ASAP")) {
            // If a time interval was selected that is now before ASAP, set time to ASAP
            LocalTime chosen = LocalTime.parse(formatASAPTime(ESAPI.encoder().decodeForHTML(getTime())));
            if (chosen.isBefore(ASAP.withSecond(0).withNano(0))) {
                setTime(ASAPStr);
            }
        }

        //order time has to be within an interval of store open/close times
        for ( int i = 0; i < openTimes.size(); i++ ) {
            String readyTime;
            LocalTime open = LocalTime.parse(openTimes.get(i), dtformatter);
            LocalTime close = LocalTime.parse(closeTimes.get(i), dtformatter);

            if ( !this.getTime().toLowerCase().contains(("ASAP").toLowerCase()) ) {  //if the time does not contain 'ASAP'
                readyTime = this.getTime().trim();
                roundTimeUpBeforeCheck = true;
            } else {

                if( getFutureOrderDate() != null ) {
                    orderSubmittedTime = open;
                }

                //if the store isn't open yet then the prep time has to be applied to the open time
                if ( orderSubmittedTime.compareTo(open) < 0 ) {
                    readyTime = open.plusMinutes(prepTime).format(DateTimeFormatter.ofPattern("h:mm a"));
                } else {
                    if ( orderSubmittedTime.plusMinutes(prepTime).getHour() == 0 ) {
                        orderSubmittedTime = LocalTime.MAX;
                    } else {
                        orderSubmittedTime = orderSubmittedTime.plusMinutes(prepTime);
                    }
                    readyTime = orderSubmittedTime.format(DateTimeFormatter.ofPattern("h:mm a"));
                }
            }
            readyTime =  ESAPI.encoder().decodeForHTML(readyTime);
            LocalTime orderReady = LocalTime.parse(readyTime, dtformatter);

            if ( close.plusMinutes(prepTime).getHour() == 0 ) {
                close = LocalTime.MAX;
            } else {
                close = close.plusMinutes(prepTime);

                if ( roundTimeUpBeforeCheck ) {
                    Integer mins = close.getMinute();
                    Integer hours = close.getHour();

                    if ( hours == 23 && mins > 45 ) {
                        close = LocalTime.MAX;
                    } else if ( mins > 45 ) {
                        close = close.withMinute(0);
                        close = close.plusHours(1);
                    } else if ( mins > 30 ) {
                        close = close.withMinute(45);
                    } else if ( mins > 15 ) {
                        close = close.withMinute(30);
                    } else if ( mins > 0 ) {
                        close = close.withMinute(15);
                    }
                }
            }

            //if the prep time is 0 then the open time will be the same as the order ready, should not be configured this way but still need to handle scenario
            if( prepTime == 0 && open.compareTo(orderReady) == 0 && close.compareTo(orderReady) >= 0 ) {
                validOrderTime = true;
                this.setReadyTime(readyTime);
                break;
            }

            if ( open.compareTo(orderReady) == -1 && close.compareTo(orderReady) >= 0 ) {
                validOrderTime = true;
                this.setReadyTime(readyTime);
                break;
            }
        }

        if ( getReadyTime().equals("") ) {
            setErrorDetails("This store is not currently accepting " + this.getType() + " orders.");
        }

        //check buffering interval again, other transactions could have been made which would change the interval's open status
        String chosenTime = ESAPI.encoder().decodeForHTML(getTime());
        String readyByTime = formatASAPTime(chosenTime);

        LocalDateTime preparingStartTime = storeModel.setPreparingOrderStartTime(readyByTime, orderSubmittedDateTime, getFutureOrderDate(), prepTime);

        //if the store isn't using buffering OR the store is using buffering and the interval is still valid...set EstimatedOrderTime to the time the cooks will get the order
        if(!storeModel.getUsingBuffering() || getGrabAndGo() || (storeModel.getUsingBuffering() && storeModel.checkInterval(preparingStartTime, getType()))) {
            LocalDateTime estimatedOrderTime = formatEstimatedOrderTime(preparingStartTime, orderSubmittedDateTime, readyByTime);
            this.setEstimatedOrderTime(estimatedOrderTime.format(dtf));

        } else {
            Logger.logMessage("ERROR: The order submitted at "+getSubmittedTime()+" is no longer in an open ordering interval." + getType(), Logger.LEVEL.ERROR);
            setErrorDetails("We're sorry your order can no longer be placed at the requested time. Please select another time and try again.");
            return false;
        }

        return validOrderTime;
    }

    public LocalDateTime formatEstimatedOrderTime(LocalDateTime preparingStartTime, LocalDateTime orderSubmittedDateTime, String pickUpDeliveryTime) {
        LocalTime estimatedOrderTime = preparingStartTime.toLocalTime();

        //set TransactionDate (submittedTime) to the pickup or delivery time
        LocalTime orderTime = LocalTime.parse(pickUpDeliveryTime);
        LocalDateTime transactionDate = LocalDateTime.of(orderSubmittedDateTime.toLocalDate(), orderTime);

        if( getFutureOrderDate() != null ) {
            transactionDate = LocalDateTime.of(getFutureOrderDate(), orderTime);
        }

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
        setSubmittedTime(transactionDate.format(dtf));

        //get the transactionDate
        String submittedTimeStr = getSubmittedTime().replace(" ", "T");
        LocalDateTime submittedTime = LocalDateTime.parse(submittedTimeStr);
        LocalTime transactionTime = submittedTime.toLocalTime();
        transactionTime = transactionTime.withSecond(0);
        transactionTime = transactionTime.withNano(0);

        //if the estimatedOrderTime is the same as the transactionTime add 10 seconds to transactionDate
        if(estimatedOrderTime.compareTo(transactionTime) == 0 && isChargeAtPurchaseTime()){
            return submittedTime.plusSeconds(10);

        } else {
            //format the estimatedOrderTime for the database
            LocalDateTime time = preparingStartTime.withHour(preparingStartTime.getHour());
            time = time.withMinute(preparingStartTime.getMinute());
            time = time.withSecond(0);
            time = time.withNano(0);
            return time;
        }
    }

    public String formatASAPTime(String chosenTime) {
        if ( this.getTime().toLowerCase().contains(("ASAP").toLowerCase()) ) {
            chosenTime = this.getTime().substring(this.getTime().indexOf("(") + 1, this.getTime().indexOf(")"));
            if((chosenTime.indexOf(":")) == (1)) {
                chosenTime = "0" + chosenTime;
            }
        }
        return CommonAPI.convertToMilitaryTime(chosenTime);
    }

    //builds a unique list of products, with modifiers listed as separate individual products, for discount & tax calculation
    private boolean checkInventoryValidForOrder(HttpServletRequest request, ProductCollection productCollection) throws Exception {
        HashMap<String, ProductModel> productListHM = new HashMap<>();

        //setup productListHM
        for ( ProductModel productModel : productCollection.getCollection() ) {

            //if product is valid -- add to productListHM
            if ( productModel.getID() == null || productModel.getID().toString().isEmpty() ) {
                Logger.logMessage("Could not determine productID in OrderModel.buildProductListHM", Logger.LEVEL.ERROR);
                setErrorDetails("Could not determine product details.");
                break;
            }

            //if product in the productListHM already (two distinct line items), just update the quantity
            if ( productListHM.containsKey(productModel.getID().toString())) {
                ProductModel existingProductModel = productListHM.get(productModel.getID().toString());

                //have to create a new model in order to update quantity, so orderModel products are not affected by the change
                ProductModel duplicateProductModel = new ProductModel(existingProductModel);

                BigDecimal updatedQuantity = existingProductModel.getQuantity().add(productModel.getQuantity());

                //fail if the store and product are tracking inventory and the product does not have enough inventory to fulfill the order
                if ( !getIgnoreInventoryCounts() && existingProductModel.getIsInventoryItem() && (existingProductModel.getInvCurrent() == null || existingProductModel.getInvCurrent().compareTo(updatedQuantity) < 0 ) ){
                    Logger.logMessage("Product " + productModel.getName() + " does not have enough inventory to be ordered in OrderModel.buildProductListHM", Logger.LEVEL.ERROR);
                    String grammar = productModel.getInvCurrent().compareTo(BigDecimal.ONE) == 0 ? "is" : "are";
                    setErrorDetails("Not enough inventory available for product " + productModel.getName() + " - you ordered "+updatedQuantity.toString()+", but only "+productModel.getInvCurrent().setScale(0, BigDecimal.ROUND_HALF_EVEN).toString()+ " " + grammar + " available.");
                    break;
                }

                //update the quantity on duped model
                duplicateProductModel.setQuantity(updatedQuantity);

                //put the duplicated model
                productListHM.put(existingProductModel.getID().toString(), duplicateProductModel);
            } else {
                //fail if the store and product are tracking inventory and the product does not have enough inventory to fulfill the order
                if ( !getIgnoreInventoryCounts() && productModel.getIsInventoryItem() && (productModel.getInvCurrent() == null || productModel.getInvCurrent().compareTo(productModel.getQuantity()) < 0) ) {
                    Logger.logMessage("Product " + productModel.getName() + " does not have enough inventory to be ordered in OrderModel.buildProductListHM", Logger.LEVEL.ERROR);
                    String grammar = productModel.getInvCurrent().compareTo(BigDecimal.ONE) == 0 ? "is" : "are";
                    setErrorDetails("Not enough inventory available for product " + productModel.getName() + " - you ordered "+productModel.getQuantity().toString()+", but only "+productModel.getInvCurrent().setScale(0,BigDecimal.ROUND_HALF_EVEN).toString()+ " " + grammar + " available.");
                    break;
                }

                //if not in list, check inv and add to productListHM
                productListHM.put(productModel.getID().toString(), productModel);
            }

            //if product has modifiers
            if ( productModel.getModifiers() != null && productModel.getModifiers().size() > 0 ) {
                //NOTE modifiers will have their own mapping information for both discounts and taxes, which is why they are treated as
                //separate individual products. Their prices and mappings will dictate amounts added to either discountable or
                //taxable amounts

                ArrayList<ModifierModel> productModifiers = (ArrayList<ModifierModel>) productModel.getModifiers();

                //add all modifiers on this product to productListHM
                for ( ModifierModel mod : productModifiers ) {

                    //if mod is not in productListHM - create new ProductModel from ModifierModel data and add to productListHM
                    if ( !productListHM.containsKey(mod.getID().toString())) {
                        ProductModel modToProductModel = ProductModel.getProductModel(mod.getID(), request);

                        //fail if the store and product are tracking inventory and the product does not have enough inventory to fulfill the order
                        if ( !getIgnoreInventoryCounts() && modToProductModel.getIsInventoryItem() && (modToProductModel.getInvCurrent() == null || modToProductModel.getInvCurrent().compareTo(productModel.getQuantity()) < 0) ) {
                            Logger.logMessage("Product " + modToProductModel.getName() + " does not have enough inventory to be ordered in OrderModel.buildProductListHM", Logger.LEVEL.ERROR);
                            String grammar = modToProductModel.getInvCurrent().compareTo(BigDecimal.ONE) == 0 ? "is" : "are";
                            setErrorDetails("Not enough inventory available for product " + modToProductModel.getName() + " - you ordered "+productModel.getQuantity().toString()+", but only "+modToProductModel.getInvCurrent().setScale(0, BigDecimal.ROUND_HALF_EVEN).toString()+ " " + grammar + " available.");
                            break;
                        }

                        modToProductModel.setQuantity(productModel.getQuantity());

                        //add to productListHM
                        productListHM.put(mod.getID().toString(), modToProductModel);
                    } else {
                        ProductModel existingProductModel = productListHM.get(mod.getID().toString());

                        //have to create a new model in order to update quantity, so orderModel products are not affected by the change
                        ProductModel duplicateProductModel = new ProductModel(existingProductModel);

                        BigDecimal updatedQuantity = existingProductModel.getQuantity().add(productModel.getQuantity());

                        //fail if the store and product are tracking inventory and the product does not have enough inventory to fulfill the order
                        if ( !getIgnoreInventoryCounts() && existingProductModel.getIsInventoryItem() && (existingProductModel.getInvCurrent() == null || existingProductModel.getInvCurrent().compareTo(updatedQuantity) < 0 )) {
                            Logger.logMessage("Product " + existingProductModel.getName() + " does not have enough inventory to be ordered in OrderModel.buildProductListHM", Logger.LEVEL.ERROR);
                            String grammar = existingProductModel.getInvCurrent().compareTo(BigDecimal.ONE) == 0 ? "is" : "are";
                            setErrorDetails("Not enough inventory available for product " + existingProductModel.getName() + " - you ordered "+updatedQuantity.toString()+", but only "+existingProductModel.getInvCurrent().setScale(0, BigDecimal.ROUND_HALF_EVEN).toString()+ " " + grammar + " available.");
                            break;
                        }

                        //update the quantity on duped model
                        duplicateProductModel.setQuantity(updatedQuantity);

                        //put the duplicated model
                        productListHM.put(existingProductModel.getID().toString(), duplicateProductModel);
                    }
                }
            }

            //add to subtotal with updated product price information
            if ( productModel.getPrice() == null || productModel.getQuantity() == null ) {
                Logger.logMessage("ERROR: Could not determine product total price.", Logger.LEVEL.ERROR);
                setErrorDetails("Could not determine product prices in order.");
                break;
            }
        }

        return getErrorDetails().equals("");
    }

    public void determineOrderReAuthStatus(HttpServletRequest request) throws Exception {
        boolean reAuthStatus = CommonAPI.getUserReAuthStatus(request);

        setForceReAuth(reAuthStatus);

        if ( getReAuthTypeID() == 0 ) {
            Integer reAuthTypeID = CommonAPI.getTerminalReAuthType(request);

            setReAuthTypeID(reAuthTypeID);
        }
    }

    //**** END ORDER REVIEW ****//

    //**** START ORDER INQUIRE ****//

    public TransactionModel transactionInquiry(HttpServletRequest request) throws Exception {
        com.mmhayes.common.transaction.models.TransactionModel transactionModel = new com.mmhayes.common.transaction.models.TransactionModel();
        transactionModel.setApiActionEnum(PosAPIHelper.ApiActionType.SALE);

        this.setAuthenticatedAccountID(CommonAuthResource.checkRequestHeaderForMMHayesAuth(request));

        //userID
        Integer userID = CommonAPI.getQCUserID();
        if (userID == null || userID == 0) {
            throw new MissingDataException("The My Quickcharge Access Profile is not configured correctly.");
        }
        transactionModel.setUserId(userID);

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
        transactionModel.setTimeStamp(LocalDateTime.now().format(dtf));

        Integer storeID = CommonAPI.checkRequestHeaderForStoreID(request);
        Integer terminalID = CommonAPI.getUserTerminalID(storeID);

        Integer tenderID = CommonAPI.getQuickchargeTenderID(storeID);
        if(this.getUseCreditCardAsTender()) { //if "Credit Card" option is selected in Payment Method selector get the Store's Credit Card Tender
            tenderID = CommonAPI.getCreditCardTenderID(storeID, this.getAuthenticatedAccountID());

            if(tenderID == null) {  //if there is no Credit Card Tender on the store then send back error to front end
                Logger.logMessage("ERROR: Could not find Credit Card Tender for Store ID: " + storeID, Logger.LEVEL.ERROR);
                setErrorDetails("This store is not configured correctly to use Credit Cards as a Payment Method");
                return transactionModel;
            }
        }

        StoreModel storeModel = StoreModel.getStoreModel(storeID, request);
        setChargeAtPurchaseTime(storeModel.isChargeAtPurchaseTime());

        //populate tender
        TenderDisplayModel tenderDisplayModel = new TenderDisplayModel();
        tenderDisplayModel.setId(tenderID); //always QC type ID

        AccountModel accountModel = new AccountModel();
        accountModel.setId(this.getAuthenticatedAccountID());

        TenderLineItemModel tenderLineItemModel = new TenderLineItemModel();
        tenderLineItemModel.setTender(tenderDisplayModel);
        tenderLineItemModel.setItemId(tenderID);
        tenderLineItemModel.setItemTypeId(PosAPIHelper.ObjectType.TENDER.toInt());
        tenderLineItemModel.setAmount(BigDecimal.ZERO);
        tenderLineItemModel.setQuantity(BigDecimal.ONE);
        tenderLineItemModel.setAccount(accountModel);

        if(this.getUseCreditCardAsTender()) {
            String creditCardTransInfo = getCreditCardTransInfo(request);
            tenderLineItemModel.setCreditCardTransInfo(creditCardTransInfo);
        }

        //set the tender on the transaction model if it is a sale transaction
        if(isChargeAtPurchaseTime()) {
            List<TenderLineItemModel> tenders = new ArrayList<TenderLineItemModel>();
            tenders.add(tenderLineItemModel);

            // Set the voucher code
            if (this.getVoucherCode() != null) {
                TenderLineItemModel voucherTender = new TenderLineItemModel();
                VoucherCodeModel voucher = new VoucherCodeModel();
                voucher.setCode(this.getVoucherCode());
                voucherTender.setVoucherCode(voucher);
                tenders.add(voucherTender);
            }

            transactionModel.setTenders(tenders);
        }

        LoyaltyAccountModel loyaltyAccountModel = new LoyaltyAccountModel();
        loyaltyAccountModel.setId(this.getAuthenticatedAccountID());
        transactionModel.setLoyaltyAccount(loyaltyAccountModel);

        TerminalModel terminalModel = TerminalModel.createTerminalModel(terminalID, true);
        transactionModel.setTerminalId(terminalID);
        transactionModel.setProducts(populateProductsForSubmission(terminalID));

        terminalModel.setChargeAtTimeOfPurchase(isChargeAtPurchaseTime());

        transactionModel.setUseMealPlanDiscount( this.isUseMealPlanDiscount() );

        if ( this.getRewards() != null && this.getRewards().size() > 0 ) {
            transactionModel.setRewards(this.getRewards());
        }

        if ( this.getCombos() != null && this.getCombos().size() > 0 ) {
            transactionModel.setCombos(this.getCombos());
        }

        TransactionBuilder transactionBuilder = TransactionBuilder.createTransactionBuilder(terminalModel, transactionModel, PosAPIHelper.ApiActionType.SALE);
        transactionBuilder.getTransactionModel().setIsInquiry(true);
        transactionBuilder.buildTransaction();
        transactionBuilder.inquire();

        return transactionBuilder.getTransactionModel();
    }

    public TransactionModel transactionInquiryDonation(HttpServletRequest request, Integer storeID, Integer terminalID, boolean isLoyalty) throws Exception {
        com.mmhayes.common.transaction.models.TransactionModel transactionModel = new com.mmhayes.common.transaction.models.TransactionModel();
        transactionModel.setApiActionEnum(PosAPIHelper.ApiActionType.SALE);
        this.setAuthenticatedAccountID(CommonAuthResource.checkRequestHeaderForMMHayesAuth(request));

        //userID
        Integer userID = CommonAPI.getQCUserID();
        if (userID == null || userID == 0) {
            throw new MissingDataException("The My Quickcharge Access Profile is not configured correctly.");
        }
        transactionModel.setUserId(userID);

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
        transactionModel.setTimeStamp(LocalDateTime.now().format(dtf));

        Integer tenderID = CommonAPI.getQuickchargeTenderID(storeID);
        TenderDisplayModel tenderDisplayModel = new TenderDisplayModel();
        tenderDisplayModel.setId(tenderID); //always QC type ID

        AccountModel accountModel = new AccountModel();
        accountModel.setId(this.getAuthenticatedAccountID());

        TenderLineItemModel tenderLineItemModel = new TenderLineItemModel();
        tenderLineItemModel.setTender(tenderDisplayModel);
        tenderLineItemModel.isQuickChargeTender();
        tenderLineItemModel.setItemId(tenderID);
        tenderLineItemModel.setItemTypeId(PosAPIHelper.ObjectType.TENDER.toInt());
        tenderLineItemModel.setQuantity(BigDecimal.ONE);
        //this should always happen after donationlineitemmodel is set
        tenderLineItemModel.setAmount(this.getDonations().get(0).getAmount()); //the amount sent in the json request is stored in the donations line item first, setting it in tender as well
        tenderLineItemModel.setAccount(accountModel);

        //set the tender on the transaction model if it is a sale transaction
        if(isChargeAtPurchaseTime()) {
            List<TenderLineItemModel> tenders = new ArrayList<TenderLineItemModel>();
            tenders.add(tenderLineItemModel);
            transactionModel.setTenders(tenders);
        }

        LoyaltyAccountModel loyaltyAccountModel = new LoyaltyAccountModel();
        loyaltyAccountModel.setId(this.getAuthenticatedAccountID());
        transactionModel.setLoyaltyAccount(loyaltyAccountModel);

        TerminalModel terminalModel = TerminalModel.createTerminalModel(terminalID, true);
        transactionModel.setTerminalId(terminalID);

        terminalModel.setChargeAtTimeOfPurchase(isChargeAtPurchaseTime());

        // Creating a Donation Transaction
        terminalModel.setLoginModel(new LoginModel());
        terminalModel.getLoginModel().setUserId(userID);
        transactionModel.setDrawerNum(1);
        transactionModel.setUseMealPlanDiscount( this.isUseMealPlanDiscount() );
        TransactionBuilder transactionBuilder;
        if(isLoyalty){ //NOTE: maybe set the isLoyalty boolean to instead be an integer identifier grabbed from an enum? just in case more donation types added
            transactionModel.setTransactionTypeId(PosAPIHelper.TransactionType.LOYALTY_ADJUSTMENT.toInt());
            transactionModel.setLoyaltyAdjustmentTypeId(PosAPIHelper.LoyaltyAdjustmentType.POINTS_DONATION.toInt());
            transactionBuilder = TransactionBuilder.createTransactionBuilder(terminalModel, transactionModel, PosAPIHelper.ApiActionType.LOYALTY_ADJUSTMENT);
        }
        else{ // Dollar Donation case
            transactionModel.setTransactionTypeId(PosAPIHelper.TransactionType.SALE.toInt());
            transactionBuilder = TransactionBuilder.createTransactionBuilder(terminalModel, transactionModel, PosAPIHelper.ApiActionType.SALE);
            if (terminalModel.getTypeId() == PosAPIHelper.TerminalType.POS_TERMINAL.toInt()) {
                transactionModel.setTransReferenceNumber("DONATION-" + transactionModel.getTimeStamp());
            }
        }
        transactionBuilder.getTransactionModel().setIsInquiry(true);
        transactionBuilder.buildTransaction();
        transactionBuilder.inquire();

        return transactionBuilder.getTransactionModel();
    }

    public String getCreditCardTransInfo(HttpServletRequest request) throws Exception {
        AccountPaymentMethodCollection accountPaymentMethod = new AccountPaymentMethodCollection(request);
        if(accountPaymentMethod.getCollection() == null || accountPaymentMethod.getCollection().size() == 0) {
            Logger.logMessage("ERROR: Could not find Payment Method for account.", Logger.LEVEL.ERROR);
            setErrorDetails("Missing or invalid Payment Method");
            this.setUseCreditCardAsTender(false);
            return "Invalid";
        }

        AccountPaymentMethodModel accountPaymentMethodModel = accountPaymentMethod.getCollection().get(0);
        return CommonAPI.getCreditCardTransInfo(accountPaymentMethodModel, false);
    }

    //**** END ORDER INQUIRE ****//

    //**** START ORDER SUBMIT ****//

    public OrderModel submitOrder(HttpServletRequest request) throws Exception {
        //determine the authenticated account ID from the request
        setAuthenticatedAccountID(CommonAuthResource.checkRequestHeaderForMMHayesAuth(request));

        if ( getAuthenticatedAccountID() == null ) {
            Logger.logMessage("ERROR: Could not determine authenticated accountID in OrderModel.submitOrder()", Logger.LEVEL.ERROR);
            throw new InvalidAuthException("An error occurred while validating your order.");
        }

        //get myqc terminal
        Integer storeID = CommonAPI.checkRequestHeaderForStoreID(request);

        if ( !validateOrderTime(storeID, request) ) {
            return this;
        }

        TransactionModel transactionModel = this.transactionInquiry(request);

        //turn off inquiry flag
        transactionModel.setIsInquiry(false);

        transactionModel.getLoyaltyAccount().setHasFetchedAvailableRewards(false);

        // Setting the email on the transaction model for the PATransaction record
        if( transactionModel.getLoyaltyAccount() != null && !transactionModel.getLoyaltyAccount().getEmail().equals("") ) {
            transactionModel.setEmail(transactionModel.getLoyaltyAccount().getEmail());
        }

        //sale, type = 1
        Integer transTypeID = 1;
        transactionModel.setTransactionTypeId(transTypeID);

        //if not charging at time of purchase, open transaction
        if( !isChargeAtPurchaseTime() ) {
            transactionModel.setTransactionTypeId(15);
            transactionModel.setTransactionType("OPEN");
            transactionModel.setApiActionEnum(PosAPIHelper.ApiActionType.OPEN);
            transactionModel.setCreateOpen(true); //this will mark the transaction model that it is a new Open Transaction
        }

        //get actual terminal
        Integer terminalID = transactionModel.getTerminalId();
        TerminalModel terminalModel = TerminalModel.createTerminalModel(terminalID, true);

        //datetime of transaction
        transactionModel.setTimeStamp(getSubmittedTime());

        //time the order is ready for pickup or delivery
        transactionModel.setEstimatedOrderTime(getEstimatedOrderTime());

        //set address if delivery
        if(this.getType().equals("delivery")) {
            transactionModel.setAddress(this.getLocation());
        }

        //transComment
        String comment =  ESAPI.encoder().decodeForHTML(this.getComments());
        transactionModel.setTransComment(comment);

        //phone number
        String phone = this.getPhone();
        String mobilePhone = this.getMobilePhone();
        if(this.isUsingMobile()) {
            phone = mobilePhone;
        }
        transactionModel.setPhone(phone);
        updateEmployeePhoneNumber();

        //order type and pickup/delivery note
        String orderType = this.getType();
        transactionModel.setOrderType(orderType);

        String note;

        if ( orderType.equals("delivery") ) {
            note = "Has to be delivered at ";
        } else {
            note = "Will be picked up at ";
        }

        note = note.concat(this.getReadyTime());

        if(!this.getDiningOptionPAPluName().equals("")) {
            note += ". \\nFor "+this.getDiningOptionPAPluName()+".";
        }

        transactionModel.setPickUpDeliveryNote(note);

        //user's name
        String name = ESAPI.encoder().decodeForHTML(getPersonName());
        transactionModel.setName(name);

        transactionModel.setGrabAndGoTransaction(getGrabAndGo());

        TransactionCacheModel transactionCacheModel = MyQCCache.checkTransactionInCache(getAuthenticatedAccountID(), getToken());

        // Calculate the voucher-adjusted total
        BigDecimal nonVoucherTotal = this.getTotal();
        if (this.getVoucherTotal() != null) {
            nonVoucherTotal = nonVoucherTotal.subtract(this.getVoucherTotal());
        }

        FundingModel fundingModel = null;
        if (transactionCacheModel == null && this.getUseCreditCardAsTender() && nonVoucherTotal.compareTo(BigDecimal.ZERO) > 0) {
            fundingModel = chargeCreditCard(transactionModel, terminalModel, nonVoucherTotal);
        }

        TransactionBuilder transactionBuilder = new TransactionBuilder();

        try {

            if ( !this.getErrorDetails().equals("") ) {
                Logger.logMessage("ERROR: Error found processing order: ".concat(this.getErrorDetails()), Logger.LEVEL.ERROR);
                return this;
            }

            //create transaction builder based on sale or open transaction
            if( !isChargeAtPurchaseTime() ) {
                transactionBuilder = TransactionBuilder.createTransactionBuilder(terminalModel, transactionModel, PosAPIHelper.ApiActionType.OPEN, PosAPIHelper.TransactionType.OPEN);
            } else {
                // Set the total on the primary (non-voucher) tender
                if (nonVoucherTotal.compareTo(BigDecimal.ZERO) == 0) {
                    // Remove the tender since the amount is 0
                    transactionModel.getTenders().remove(0);
                } else {
                    transactionModel.getTenders().get(0).setAmount(nonVoucherTotal);
                    transactionModel.getTenders().get(0).setExtendedAmount(nonVoucherTotal);
                }
                transactionBuilder = TransactionBuilder.createTransactionBuilder(terminalModel, transactionModel, PosAPIHelper.ApiActionType.SALE, PosAPIHelper.TransactionType.SALE);
            }

            if(transactionCacheModel != null) {
                this.setOrderNumber(transactionCacheModel.getOrderNumber());
                this.setTransactionID(transactionCacheModel.getID());
                return this;
            }

            //save transaction
            transactionBuilder.getTransactionModel().checkTransactionTypeEnum();
            transactionBuilder.buildTransaction();
            transactionBuilder.saveTransaction();

        } catch (Exception ex) {
            setErrorDetails("There was an issue creating the transaction. Please contact support or try again later.");
            Logger.logException("ERROR: There was an issue saving the transaction in OrderModel.submitOrder", "", ex);

            // Set error details for an invalid voucher
            if (ex instanceof InvalidVoucherException) {
                String invalidVoucherDetails = ((InvalidVoucherException)ex).getDetails();
                if (invalidVoucherDetails != null && !invalidVoucherDetails.equals("")) {
                    setErrorDetails("There was an issue creating the transaction. " + invalidVoucherDetails);
                }
            }

            // If the tender was a Credit Card, attempt to refund account (this needs to be changed to void once that is tested)
            if(fundingModel != null && fundingModel.getProcessorRefNum() != null) {
                Logger.logMessage("ProcessorRefNum found on FundingModel, attempting to refund Credit Card charge", Logger.LEVEL.DEBUG);

                try {

                    fundingModel.refundChargedAmount(fundingModel.getProcessedAmount(), getAuthenticatedAccountID());

                    if(fundingModel.getRefundedAmount().compareTo(fundingModel.getProcessedAmount()) != 0) {
                        setErrorDetails("There was an issue creating the transaction. Your credit card was charged but we were unable to process the refund. Please contact support.");
                        Logger.logMessage("Unable to fully refund account ID: "+ getAuthenticatedAccountID()+" after failure to create transaction in OrderModel.submitOrder.", Logger.LEVEL.ERROR);
                    }

                } catch (Exception refundEx) {
                    setErrorDetails("There was an issue creating the transaction. Your credit card was charged but we were unable to process the refund. Please contact support.");
                    Logger.logException("Unable to process the refund for account ID: "+ getAuthenticatedAccountID()+" after failure to create transaction in OrderModel.submitOrder.", "", refundEx);
                }
            }

            return this;
        }

        // Set the loyalty points that were earned on the transaction and the previous points totals
        this.setLoyaltyPointsEarned(transactionModel.getLoyaltyPointsEarned());
        for (LoyaltyPointModel loyaltyEarned : this.getLoyaltyPointsEarned()) {
            int programID = loyaltyEarned.getLoyaltyProgram().getId();
            for (LoyaltyPointModel loyaltyTotal : transactionModel.getLoyaltyAccount().getLoyaltyPoints()) {
                if (programID == loyaltyTotal.getLoyaltyProgram().getId()) {
                    loyaltyEarned.setOriginalPoints(loyaltyTotal.getPoints() - loyaltyEarned.getPoints());
                    break;
                }
            }
        }

        // Set the donations available for the employee
        DonationCollection donationCollection = DonationCollection.getAllDonationsByEmployeeId(this.getAuthenticatedAccountID(), null);
        this.setEmployeeDonations(donationCollection.getCollection());

        //only send the purchase confirmation email if it is a sale transaction
        if(isChargeAtPurchaseTime() && transactionModel.getId() != null) {
            transactionBuilder.sendPurchaseConfirmationEmail();
        }

        //set order number based on transactionID
        this.setOrderNumber(transactionModel.getOrderNumber());
        this.setTransactionID(transactionModel.getId());

        if ( MyQCCache.checkCacheForExpressReorders( getAuthenticatedAccountID() ) ) {
            MyQCCache.expressReorderCacheNeedsUpdate( getAuthenticatedAccountID() );
        }

        if ( MyQCCache.checkCacheForStoreKeypad( storeID ) ) {
            MyQCCache.storeKeypadPopularProductsNeedsUpdate( storeID );
        }

        MyQCCache.addTransactionToCache(getAuthenticatedAccountID(), getToken(), transactionModel);

        return this;
    }

    private List<ProductLineItemModel> populateProductsForSubmission(Integer terminalID) throws Exception {
        Integer productIndex = 1;
        List<ProductLineItemModel> productLines = new ArrayList<>();

        for (ProductModel product : this.getProducts()) {
            //if any product has an error - the order is not able to be completed, error out
            if ( !this.getErrorDetails().equals("") ) {
                break;
            }

            ProductLineItemModel productLineItemModel = new ProductLineItemModel();

            HashMap productHM = new HashMap();
            productHM.put("ID", product.getID());
            productHM.put("NAME", product.getName());
            com.mmhayes.common.product.models.ProductModel productModel = new com.mmhayes.common.product.models.ProductModel(productHM, terminalID);
            productLineItemModel.setProduct(productModel);
            productLineItemModel.setItemTypeId(PosAPIHelper.ObjectType.PRODUCT.toInt());
            productLineItemModel.setItemId(product.getID());
            productLineItemModel.setQuantity(product.getQuantity());
            productLineItemModel.setAmount(product.getOriginalPrice() == null ? product.getPrice() : product.getOriginalPrice());
            productLineItemModel.setKeypadId(product.getKeypadID());

            boolean scaleUsed = product.getScaleUsed() == null ? false : product.getScaleUsed();
            productLineItemModel.getProduct().setIsWeighted(scaleUsed);

            //add modifiers as their own products
            if (product.getModifiers() != null && product.getModifiers().size() > 0) {
                ArrayList<ModifierLineItemModel> modifiers = new ArrayList<>();

                for (ModifierModel mod : product.getModifiers()) {
                    //if any product has an error - the order is not able to be completed, error out
                    if ( !this.getErrorDetails().equals("") ) {
                        break;
                    }

                    ModifierLineItemModel modifierLineItemModel = new ModifierLineItemModel();

                    HashMap modProductHM = new HashMap();
                    modProductHM.put("ID", mod.getID());
                    modProductHM.put("NAME", mod.getName());
                    modProductHM.put("PRICE", mod.getPrice());
                    com.mmhayes.common.product.models.ProductModel modProductModel = new com.mmhayes.common.product.models.ProductModel(modProductHM, terminalID);

                    modifierLineItemModel.setProduct(modProductModel);
                    modifierLineItemModel.setQuantity(product.getQuantity());
                    modifierLineItemModel.setAmount(mod.getPrice());
                    modifierLineItemModel.setKeypadId(mod.getKeypadID());

                    if(mod.getPrepOption() != null) {
                        com.mmhayes.common.product.models.PrepOptionModel prepOptionModel = new com.mmhayes.common.product.models.PrepOptionModel();
                        prepOptionModel.setId(mod.getPrepOption().getID());
                        prepOptionModel.setPrepOptionSetId(mod.getPrepOption().getPrepOptionSetID());
                        prepOptionModel.setPrice(mod.getPrepOption().getPrice());
                        modifierLineItemModel.setPrepOption(prepOptionModel);

                        modifierLineItemModel.setAmount(modifierLineItemModel.getAmount().add(mod.getPrepOption().getPrice()));
                    }

                    modifiers.add(modifierLineItemModel);
                }

                productLineItemModel.setModifiers(modifiers);
            }

            if(product.getPrepOption() != null) {
                com.mmhayes.common.product.models.PrepOptionModel prepOptionModel = new com.mmhayes.common.product.models.PrepOptionModel();
                prepOptionModel.setId(product.getPrepOption().getID());
                prepOptionModel.setPrepOptionSetId(product.getPrepOption().getPrepOptionSetID());
                prepOptionModel.setPrice(product.getPrepOption().getPrice());
                productLineItemModel.setPrepOption(prepOptionModel);

                productLineItemModel.setBasePrice(product.getPrice());
                if(!productLineItemModel.getProduct().isWeighted()){
                    productLineItemModel.setAmount(productLineItemModel.getAmount().add(product.getPrepOption().getPrice()));
                }
            }

            if(product.getComboTransLineItemId() != null) {
                productLineItemModel.setComboTransLineItemId(product.getComboTransLineItemId());
                productLineItemModel.setComboDetailId(product.getComboDetailId());
                productLineItemModel.setBasePrice(product.getBasePrice());
                productLineItemModel.setComboPrice(product.getComboPrice());
                productLineItemModel.setUpcharge(product.getUpcharge());
            }

            productLines.add(productLineItemModel);
            productIndex = productIndex + 1;
        }

        return productLines;
    }

    //adds the associated dining option product to the current list of products
    public void addEatInTakeOutProduct(Integer storeID) {
        String diningOptionPAPluID = this.getDiningOptionPAPluID();

        try {
            if (diningOptionPAPluID != null && !diningOptionPAPluID.equals("")) {
                Integer productID = Integer.parseInt(diningOptionPAPluID);
                ProductModel productModel = ProductModel.getProductModel(productID, storeID); //create ProductModel

                productModel.setQuantity(BigDecimal.ONE); //set quantity to 1

                List<ProductModel> productList = new ArrayList<>();
                productList.add(productModel);

                List<ProductModel> originalProductList = this.getProducts();
                for(int i=0; i<originalProductList.size(); i++) {
                   productList.add(originalProductList.get(i));
                }

                this.setProducts(productList);
            }
        } catch (Exception ex) {
            Logger.logException(ex);
        }
    }

    //updating the employee's phone or mobile number when placing the order
    public void updateEmployeePhoneNumber() {
        String phoneNumber = this.getPhone();
        String mobilePhone = this.getMobilePhone().replace("-","");
        String updateSQL = "data.ordering.updateEmployeePhoneNumber";
        String fieldName = "Phone";

        if(phoneNumber.equals("") && !mobilePhone.equals("")) {
            updateSQL = "data.ordering.updateEmployeeMobilePhoneNumber";
            phoneNumber = mobilePhone;
            fieldName = "Mobile Phone";
        }

        String oldPhoneNumber = getOldAccountPhoneNumber(fieldName);

        if(!oldPhoneNumber.equals(phoneNumber)) {
            //set the employee's phone or mobile number in QC_Employees
            Integer updateResult = dm.parameterizedExecuteNonQuery(updateSQL,
                    new Object[]{
                            getAuthenticatedAccountID(),
                            phoneNumber
                    }
            );

            if (updateResult != 1) {
                Logger.logMessage("Could not update Employee phone or mobile phone number for EmployeeID: "+getAuthenticatedAccountID()+" in OrderModel.updateEmployeePhoneNumber.", Logger.LEVEL.ERROR);
            } else {

                //log the update to the employee's phone number in the QC_EmployeeChanges table
                Integer logAccountChangeResult = 0;
                logAccountChangeResult = dm.parameterizedExecuteNonQuery("data.myqc.recordAccountChangeAsMyQCUser",
                        new Object[]{
                                getAuthenticatedAccountID(),
                                fieldName,
                                oldPhoneNumber,
                                phoneNumber
                        }
                );
                if (logAccountChangeResult == 0) {
                    Logger.logMessage("ERROR: Could not LOG account phone change from "+oldPhoneNumber+" to "+phoneNumber+" for account ID: "+getAuthenticatedAccountID(), Logger.LEVEL.ERROR);
                } else {
                    Logger.logMessage("Successfully modified account phone to from "+oldPhoneNumber+" to "+phoneNumber+" for account ID: "+getAuthenticatedAccountID(), Logger.LEVEL.DEBUG);
                }
            }
        }
    }

    //get the employee's old phone number or old mobile phone number
    public String getOldAccountPhoneNumber(String fieldName) {
        String oldPhoneNumber = "";
        String selectSQL = "data.ordering.getEmployeePhoneNumber";

        if(fieldName.equals("Mobile Phone")) {
            selectSQL = "data.ordering.getEmployeeMobilePhoneNumber";
        }

        //return the employee's current phone number or mobile phone number
        Object oldPhoneNumberObj = dm.getSingleField(selectSQL,
                new Object[]{
                        getAuthenticatedAccountID()
                },
                false, true
        );

        if (oldPhoneNumberObj != null && !oldPhoneNumberObj.toString().equals("")) {
            oldPhoneNumber = oldPhoneNumberObj.toString();
        }

        return oldPhoneNumber;
    }

    //returns the products from the dining options keypad for the store
    public static ArrayList<HashMap> getDiningOptions(HttpServletRequest request) throws  Exception{
        //get myqc terminal
        Integer storeID = CommonAPI.checkRequestHeaderForStoreID(request);

        //use the store model to get the Store Keypad Model from the cache to get the number of products that can be popular
        StoreModel storeModel = StoreModel.getStoreModel(storeID, request);
        StoreKeypadModel storeKeypadModel = StoreKeypadModel.getStoreKeypadModel(storeModel);

        return storeKeypadModel.getDiningOptionProducts();
    }

    //if using credit card as a tender, charge account before completing transaction
    public FundingModel chargeCreditCard(TransactionModel transactionModel, TerminalModel terminalModel, BigDecimal total) throws Exception {
        FundingModel fundingModel = null;

        for (TenderLineItemModel tenderLineItemModel : transactionModel.getTenders()) {

            //if the tender is a credit card tender, charge account
            if (tenderLineItemModel.getTender() != null && tenderLineItemModel.getTender().getName() != null &&
                    tenderLineItemModel.getTender().getTenderTypeId().equals(2)) {

                fundingModel = new FundingModel(tenderLineItemModel.getEmployeeId(), false);
                fundingModel.setFundingTransaction(false);

                Integer surchargeID = terminalModel.getPASurchargeID();

                //update funding model with the online ordering terminal's payment processor
                if(terminalModel.getUseOnlineOrderingFunding()) {
                    fundingModel = updatePaymentMethodForOnlineOrdering(fundingModel, terminalModel.getId());
                } else {
                    surchargeID = fundingModel.getPaymentProcessorModel().getPASurchargeID();
                }

                if(fundingModel != null) {

                    //subtract the funding fee from the total or it will be added twice
                    if(this.getCreditCardFee() != null) {
                        BigDecimal totalWithoutFee = total.subtract(this.getCreditCardFee());

                        //if the total without the fee is greater than 0
                        if(totalWithoutFee.compareTo(BigDecimal.ZERO) > 0) {

                            //if the payment method is stripe and that the total with the fee is less than Stripe's $0.50 minimum, don't charge and show error
                            if(fundingModel.getAccountPaymentMethod().getPaymentProcessorID().equals(1) && total.compareTo(BigDecimal.valueOf(0.50)) < 0) {
                                Logger.logMessage("The $0.50 minimum charge amount for Stripe was not met.", Logger.LEVEL.TRACE);
                                setErrorDetails("You do not reach the $0.50 minimum required to use a Credit Card as the Payment Method");

                            //otherwise charge the account for the total without the fee, the fee will get added on in chargeAccount()
                            } else {
                                setSurcharge(transactionModel, surchargeID);
                                fundingModel.getAccountPaymentMethod().setTerminalHasFundingFee(false);
                                fundingModel.chargeAccount(total, fundingModel.getAccountPaymentMethod(), "manual");

                                // Update CreditCardTransInfo with the requestID
                                updateCreditCardTransInfo(fundingModel, tenderLineItemModel);
                            }
                        }

                    //if the total is greater than 0
                    } else if(total.compareTo(BigDecimal.ZERO) > 0) {

                        //if the payment method is stripe and that the total is less than Stripe's $0.50 minimum, don't charge and show error
                        if(fundingModel.getAccountPaymentMethod().getPaymentProcessorID().equals(1) && total.compareTo(BigDecimal.valueOf(0.50)) < 0) {
                            Logger.logMessage("The $0.50 minimum charge amount for Stripe was not met.", Logger.LEVEL.TRACE);
                            setErrorDetails("You do not reach the $0.50 minimum required to use a Credit Card as a Payment Method");

                        //otherwise charge the account for the total
                        } else {
                            fundingModel.chargeAccount(total, fundingModel.getAccountPaymentMethod(), "manual");

                            // Update CreditCardTransInfo with the requestID
                            updateCreditCardTransInfo(fundingModel, tenderLineItemModel);
                        }
                    }
                }
            }
        }

        return fundingModel;
    }

    // Update CreditCardTransInfo on the tender with the requestID
    public void updateCreditCardTransInfo(FundingModel fundingModel, TenderLineItemModel tenderLineItemModel) {
        String creditCardTransInfo = tenderLineItemModel.getCreditCardTransInfo();
        String requestId = fundingModel.getProcessorRefNum();

        creditCardTransInfo += "RequestId=" + requestId + ";isEcommerceCharge=1;";

        tenderLineItemModel.setCreditCardTransInfo(creditCardTransInfo);

        // Adding Payment Method ID to tenderLineItemModel - Is already encoded in creditCardTransInfo
        tenderLineItemModel.setPaymentMethodTypeId(fundingModel.getAccountPaymentMethod().getPaymentMethodTypeID());
    }

    //if using the merchant account of the online ordering terminal, update account payment method
    public FundingModel updatePaymentMethodForOnlineOrdering(FundingModel fundingModel, Integer terminalID) {
        DataManager dm = new DataManager();

        //get the online ordering terminal merchant account details
        ArrayList<HashMap> terminalDetailsList = dm.parameterizedExecuteQuery("data.ordering.getOnlineOrderingFundingDetails",
                new Object[]{
                        terminalID
                },
                true
        );

        if(terminalDetailsList != null && terminalDetailsList.size() > 0) {
            HashMap terminalDetails = terminalDetailsList.get(0);

            //update funding fee percentage amount
            fundingModel.getAccountPaymentMethod().setFundingPercentageFeeAmount(CommonAPI.convertModelDetailToBigDecimal(terminalDetails.get("FUNDINGPERCENTAGEFEEAMOUNT")));

            //update funding fee flat amount
            fundingModel.getAccountPaymentMethod().setFundingFlatFeeAmount(CommonAPI.convertModelDetailToBigDecimal(terminalDetails.get("FUNDINGFLATFEEAMOUNT")));

            //update calculation method
            fundingModel.getAccountPaymentMethod().setFundingFeeCalculationMethodID(CommonAPI.convertModelDetailToInteger(terminalDetails.get("FUNDINGFEECALCULATIONMETHODID")));

            //update surcharge name
            fundingModel.getAccountPaymentMethod().setSurchargeName(CommonAPI.convertModelDetailToString(terminalDetails.get("SURCHARGENAME")));

            //update 'terminal has funding fee' flag
            boolean hasFundingFee = terminalDetails.containsKey("TERMINALHASFUNDINGFEE") && CommonAPI.convertModelDetailToInteger(terminalDetails.get("TERMINALHASFUNDINGFEE")) == 1;
            fundingModel.getAccountPaymentMethod().setTerminalHasFundingFee(hasFundingFee);

            //update payment processor details based on payment processor type
            if(terminalDetails.get("FUNDINGPAYMENTPROCESSORID") != null && !terminalDetails.get("FUNDINGPAYMENTPROCESSORID").toString().equals("")) {
                Integer fundingPaymentProcessorID = CommonAPI.convertModelDetailToInteger(terminalDetails.get("FUNDINGPAYMENTPROCESSORID"));

                //if the current payment processor is Stripe and the Online Ordering Terminal's merchant account is using Stripe
                if( fundingModel.getAccountPaymentMethod().getPaymentProcessorID().equals(1) ) {

                    if( fundingPaymentProcessorID.equals(1) ) {

                        String publicAPIKey = CommonAPI.convertModelDetailToString(terminalDetails.get("PAYMENTPROCESSORPUBLICAPIKEY"));
                        String privateAPIKey = CommonAPI.convertModelDetailToString(terminalDetails.get("PAYMENTPROCESSORPRIVATEAPIKEY"));

                        //update the default funding terminal merchant account with the store's merchant account details
                        fundingModel.getAccountPaymentMethod().updatePaymentMethodDetails(publicAPIKey, privateAPIKey, "", "");
                        return fundingModel;

                    } else {
                        Logger.logMessage("ERROR: Default funding terminal in the account's Spending Profile is set to a different Payment Processor, Stripe, than the store for terminalID: "+terminalID+" in OrderModel.updatePaymentMethodForOnlineOrdering", Logger.LEVEL.ERROR);
                        setErrorDetails("This store is not configured correctly to use Credit Cards as a Payment Method");
                    }
                }

                //if the current payment processor is FreedomPay and the Online Ordering Terminal's merchant account is using FreedomPay
                if( fundingModel.getAccountPaymentMethod().getPaymentProcessorID().equals(6) ) {

                    if( fundingPaymentProcessorID.equals(6) ) {

                        String storeNum = CommonAPI.convertModelDetailToString(terminalDetails.get("PAYMENTPROCESSORSTORENUM"));
                        String terminalNum = CommonAPI.convertModelDetailToString(terminalDetails.get("PAYMENTPROCESSORTERMINALNUM"));

                        //update the default funding terminal merchant account with the store's merchant account details
                        fundingModel.getAccountPaymentMethod().updatePaymentMethodDetails("", "", storeNum, terminalNum);
                        return fundingModel;

                    } else {
                        Logger.logMessage("ERROR: Default funding terminal in the account's Spending Profile is set to a different Payment Processor, FreedomPay, than the store for terminalID: "+terminalID+" in OrderModel.updatePaymentMethodForOnlineOrdering", Logger.LEVEL.ERROR);
                        setErrorDetails("This store is not configured correctly to use Credit Cards as a Payment Method");
                    }
                }

            } else {
                Logger.logMessage("ERROR: Could not retrieve payment processor details from online ordering terminal in OrderModel.updatePaymentMethodForOnlineOrdering", Logger.LEVEL.ERROR);
                setErrorDetails("This store is not configured correctly to use Credit Cards as a Payment Method");
            }
        }
        return null;
    }

    public void setSurcharge(TransactionModel transactionModel, Integer surchargeID) throws Exception {
        if(surchargeID == null) {
            Logger.logMessage("ERROR: Could not find valid surcharge on terminal for recording Credit Card transaction with Fee in OrderModel.setSurcharge", Logger.LEVEL.ERROR);
            setErrorDetails("This store is not configured correctly to use Credit Cards as a Payment Method");

        } else {
            //create and set the SurchargeLineItemModel
            SurchargeLineItemModel surchargeLineItemModel = new SurchargeLineItemModel();

            surchargeLineItemModel.setId(surchargeID);
            surchargeLineItemModel.setItemId(surchargeID);
            surchargeLineItemModel.setItemTypeId(PosAPIHelper.ObjectType.SURCHARGE.toInt());

            BigDecimal eligibleSurchargeSubtotalTotal = determineCreditCardSurchargeTotal();

            surchargeLineItemModel.setEligibleAmount(eligibleSurchargeSubtotalTotal);
            surchargeLineItemModel.setAmount(getCreditCardFee());
            surchargeLineItemModel.setExtendedAmount(getCreditCardFee());
            surchargeLineItemModel.setQuantity(BigDecimal.ONE);

            //validate Surcharge and set SurchargeDisplayModel
            SurchargeModel validatedSurchargeModel = SurchargeModel.getSurchargeModel(surchargeLineItemModel.getItemId(), transactionModel.getTerminal());
            SurchargeDisplayModel surchargeDisplayModel = SurchargeDisplayModel.createSurchargeModel(validatedSurchargeModel);
            surchargeLineItemModel.setSurcharge(surchargeDisplayModel);

            //set SurchargeLineItemModel on transactionModel
            transactionModel.getSurcharges().add(surchargeLineItemModel);
        }
    }

    //set the list of surcharges for the review page, have to use Hashmap because SurchargeModels did not return
    public BigDecimal setItemSurchargeAmounts(TransactionModel transactionModel) {
        BigDecimal surchargeTotal = BigDecimal.ZERO;

        //loop over all the item surcharge in the transaction
        for( SurchargeLineItemModel surchargeLineItemModel: transactionModel.getSurcharges() ) {
            if(surchargeLineItemModel.getSurcharge().getApplicationTypeId().equals(2)) {

                //check if this surcharge model is already being used, if found then add amount to existing model
                boolean surchargeFound = false;
                for(SurchargeLineItemModel surchargeLine: getItemSurcharges()) {

                    if(surchargeLine.getSurcharge().getId().equals(surchargeLineItemModel.getSurcharge().getId())) {
                        BigDecimal surchargeTotalAmount = surchargeLine.getExtendedAmount().add(surchargeLineItemModel.getExtendedAmount());
                        surchargeLine.setExtendedAmount(surchargeTotalAmount);
                        surchargeFound = true;
                        break;
                    }
                }

                if(!surchargeFound) {
                    getItemSurcharges().add(surchargeLineItemModel);
                }

                //determine the total surcharge amount for the order total
                surchargeTotal = surchargeTotal.add(surchargeLineItemModel.getExtendedAmount().setScale(2, BigDecimal.ROUND_HALF_UP));
            }
        }

        return surchargeTotal;
    }

    //determine what the subtotal credit card fee surcharge should be calculate on
    public BigDecimal determineCreditCardSurchargeTotal() {
        BigDecimal surchargeTotal = BigDecimal.ZERO;

        //only add surcharges with 'Eligible For Subtotal Surcharge' to total for surcharge calculation
        for(SurchargeLineItemModel surchargeLine: getItemSurcharges()) {

            if( surchargeLine.getSurcharge().getEligibleForSubtotalSurcharge()) {
                surchargeTotal = surchargeTotal.add(surchargeLine.getExtendedAmount().setScale(2, BigDecimal.ROUND_HALF_UP));
            }
        }

        BigDecimal total = getSubtotal();

        //return error if there is no total at this point
        if (total == null) {
            Logger.logMessage("ERROR: Could not determine credit card surcharge total.", Logger.LEVEL.ERROR);
            setErrorDetails("An error occurred while calculating the credit card surcharge total.");
            return null;
        }

        if ( getSubtotalDiscount() != null ) {
            total = total.subtract(getSubtotalDiscount());
        }

        if ( getTax() != null )  {
            total = total.add(getTax());
        }

        if ( getDeliveryFee() != null )  {
            total = total.add(getDeliveryFee());
        }

        if ( getRewardsTotal() != null ) {
            total = total.subtract(getRewardsTotal());
        }

        if ( surchargeTotal != null ) {
            total = total.add(surchargeTotal);
        }

        return total.setScale(2, BigDecimal.ROUND_HALF_EVEN);
    }

    public OrderModel expressReorder(HttpServletRequest request) throws Exception{
        //determine the authenticated account ID from the request
        setAuthenticatedAccountID(CommonAuthResource.checkRequestHeaderForMMHayesAuth(request));

        Integer storeID = CommonAPI.checkRequestHeaderForStoreID(request);

        if ( getAuthenticatedAccountID() == null ) {
            Logger.logMessage("ERROR: Could not determine authenticated accountID in reviewOrder()", Logger.LEVEL.ERROR);
            throw new InvalidAuthException("An error occurred while validating your order, please try again.");
        }

        setExpressReorder(true);

        //if there is no reorder transactionID then coming from the favorite order view and need to get last transaction details for express reorder
        if(getReorderTransactionID() == null || getReorderTransactionID() == 0) {

            //get the online ordering terminal merchant account details
            ArrayList<HashMap> prevTransactionList = dm.parameterizedExecuteQuery("data.expressorder.getPreviousTransactionDetails",
                    new Object[]{
                            getAuthenticatedAccountID(),
                            storeID
                    },
                    true
            );

            if(prevTransactionList != null && prevTransactionList.size() > 0) {
                HashMap prevTransaction = prevTransactionList.get(0);
                if(prevTransaction.get("PHONE") != null) {
                    String phoneNumber = prevTransaction.get("PHONE").toString();
                    setPhone(phoneNumber);
                }

                if(prevTransaction.get("TENDERTYPE") != null && !prevTransaction.get("TENDERTYPE").toString().equals("")) {
                    Integer tenderType = Integer.parseInt(prevTransaction.get("TENDERTYPE").toString());
                    if(tenderType.equals(2)) {
                        setUseCreditCardAsTender(true);
                    }
                }

                if(prevTransaction.get("DININGOPTIONPAPLUID") != null && !prevTransaction.get("DININGOPTIONPAPLUID").toString().equals("")) {
                    if(getType().equals("pickup")) {
                        boolean diningOptionFound = false;
                        Integer diningOptionPAPluID = CommonAPI.convertModelDetailToInteger(prevTransaction.get("DININGOPTIONPAPLUID"));

                        //use the store model to get the Store Keypad Model from the cache to get the number of products that can be popular
                        StoreModel storeModel = StoreModel.getStoreModel(storeID, request);
                        StoreKeypadModel storeKeypadModel = StoreKeypadModel.getStoreKeypadModel(storeModel);

                        //if the store has a dining option keypad and valid dining option products in the keypad, check if any of the products in the favorite order are a dining option product
                        if(storeKeypadModel.getDiningOptionKeypadID() != null && storeKeypadModel.getDiningOptionProducts() != null && storeKeypadModel.getDiningOptionProducts().size() > 0) {
                            for(ProductModel productModel : getProducts()) {

                                for(HashMap diningOption : storeKeypadModel.getDiningOptionProducts()) {

                                    //if found the dining option product then break out of loop, there can only be one dining option
                                    if(productModel.getID().equals(CommonAPI.convertModelDetailToInteger(diningOption.get("ID")))) {
                                        diningOptionFound = true;
                                        break;
                                    }
                                }
                                if(diningOptionFound) {
                                    break;
                                }
                            }
                        }

                        if(!diningOptionFound) {
                            ProductModel diningOptionProductModel = new ProductModel(diningOptionPAPluID);
                            diningOptionProductModel.setQuantity(BigDecimal.ONE);

                            List<ProductModel> products = new ArrayList<>();
                            products.add(diningOptionProductModel);
                            products.addAll(getProducts());
                            setProducts(products);
                        }

                        setDiningOptionPAPluID(prevTransaction.get("DININGOPTIONPAPLUID").toString());
                        setDiningOptionPAPluName(prevTransaction.get("DININGOPTIONPAPLUNAME").toString());
                    }
                }
            }
        } else if (getUseDiningOptions()) {

            //use the store model to get the Store Keypad Model from the cache to get the number of products that can be popular
            StoreModel storeModel = StoreModel.getStoreModel(storeID, request);
            StoreKeypadModel storeKeypadModel = StoreKeypadModel.getStoreKeypadModel(storeModel);

            //if the store has a dining option keypad and valid dining option products in the keypad, check if any of the products in the favorite order are a dining option product
            if(storeKeypadModel.getDiningOptionKeypadID() != null && storeKeypadModel.getDiningOptionProducts() != null && storeKeypadModel.getDiningOptionProducts().size() > 0) {

                for(ProductModel productModel : getProducts()) {

                    for(HashMap diningOption : storeKeypadModel.getDiningOptionProducts()) {

                        //if found the dining option product then break out of loop, there can only be one dining option
                        if(productModel.getID().equals(CommonAPI.convertModelDetailToInteger(diningOption.get("ID")))) {
                            setDiningOptionPAPluID(productModel.getID().toString());
                            setDiningOptionPAPluName(productModel.getName());
                            break;
                        }
                    }

                    if(!getDiningOptionPAPluName().equals("")) {
                        break;
                    }
                }
            }
        }

        reviewOrder(request);

        return this;
    }

    private String determinePersonAccountName() {
        String personName = "";

        personName = dm.parameterizedExecuteScalar("data.accounts.getPersonAccountName", new Object[]{getPersonID()}).toString();

        return personName;
    }

    //**** START DONATION SUBMIT ****//

    public OrderModel submitDonation(HttpServletRequest request) throws Exception {
        //determine the authenticated account ID from the request
        setAuthenticatedAccountID(CommonAuthResource.checkRequestHeaderForMMHayesAuth(request));

        if ( getAuthenticatedAccountID() == null ) {
            Logger.logMessage("ERROR: Could not determine authenticated accountID in OrderModel.submitDonation()", Logger.LEVEL.ERROR);
            throw new InvalidAuthException("An error occurred while validating your order.");
        }

        if(!this.getErrorDetails().equals("")) {
            return this;
        }

        //get myqc terminal
        DonationModel donation = this.getDonations().get(0).getDonation();
        Integer storeID = CommonAPI.getMyQCTerminalID(donation.getTerminalId());

        //Build transactionModel
        TransactionModel transactionModel = this.transactionInquiryDonation(request, storeID, donation.getTerminalId(), true);

        //set donations on Transaction Model
        transactionModel.setDonations(populateDonationsForSubmission());

        //turn off inquiry flag
        transactionModel.setIsInquiry(false);

        transactionModel.getLoyaltyAccount().setHasFetchedAvailableRewards(false);

        //get actual terminal
        TerminalModel terminalModel = TerminalModel.createTerminalModel(transactionModel.getTerminalId(), true);

        terminalModel.setLoginModel(new LoginModel());
        terminalModel.getLoginModel().setUserId(transactionModel.getUserId());
        transactionModel.setDrawerNum(1);

        //create new store model
        StoreModel storeModel = StoreModel.getStoreModel(storeID, request);
        LocalDateTime donationSubmittedDateTime = LocalDateTime.now();

        //adjust for the terminal timezone offset if applicable
        if ( storeModel.getTimezoneOffset() != null && storeModel.getTimezoneOffset() != 0 ) {
            donationSubmittedDateTime = donationSubmittedDateTime.plusHours(storeModel.getTimezoneOffset());
        }

        //datetime of transaction
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");

        transactionModel.setTimeStamp(donationSubmittedDateTime.format(dtf));

        //user's name
        String name = getPersonName();
        transactionModel.setName(name);

        if ( !this.getErrorDetails().equals("") ) {
            Logger.logMessage("ERROR: Error found processing order: ".concat(this.getErrorDetails()), Logger.LEVEL.ERROR);
            return this;
        }

        TransactionBuilder transactionBuilder = TransactionBuilder.createTransactionBuilder(terminalModel, transactionModel, PosAPIHelper.ApiActionType.LOYALTY_ADJUSTMENT, PosAPIHelper.TransactionType.LOYALTY_ADJUSTMENT);

        //save transaction
        transactionBuilder.getTransactionModel().checkTransactionTypeEnum();
        transactionBuilder.buildTransaction();
        transactionBuilder.saveTransaction();

        this.setTransactionID(transactionModel.getId());

        return this;
    }

    public OrderModel submitDollarDonation(HttpServletRequest request) throws Exception {
        //determine the authenticated account ID from the request
        setAuthenticatedAccountID(CommonAuthResource.checkRequestHeaderForMMHayesAuth(request));

        if ( getAuthenticatedAccountID() == null ) {
            Logger.logMessage("ERROR: Could not determine authenticated accountID in OrderModel.submitDollarDonation()", Logger.LEVEL.ERROR);
            throw new InvalidAuthException("An error occurred while validating your order.");
        }

        if(!this.getErrorDetails().equals("")) {
            return this;
        }

        //get myqc terminal
        DonationModel donation = this.getDonations().get(0).getDonation();
        Integer storeID = donation.getTerminalId();

        //backend validation, check if user has enough available balance in the store to make donation.
        HashMap thisUserBalance = getUserBalanceForDonation(storeID, this.getDonations().get(0).getAmount(), getAuthenticatedAccountID());
        int isDonationGreaterThanAvailableBalance = this.getDonations().get(0).getAmount().compareTo((BigDecimal) thisUserBalance.get("USERBALANCE")); //value of 1 means donation is greater than available balance
        if((isDonationGreaterThanAvailableBalance==1)){
            throw new TransactionIndividualLimitException("The amount to donate exceeds the purchase limit for this terminal/store");
        };
        //Build transactionModel
        TransactionModel transactionModel = this.transactionInquiryDonation(request, storeID, donation.getTerminalId(), false);

        //set donations on Transaction Model
        transactionModel.setDonations(populateDonationsForSubmission());

        //turn off inquiry flag
        transactionModel.setIsInquiry(false);

        transactionModel.getLoyaltyAccount().setHasFetchedAvailableRewards(false);

        //Dollar Donation Adjustment Transaction Type
        transactionModel.setTransactionTypeId(PosAPIHelper.TransactionType.SALE.toInt());

        //get actual terminal
        TerminalModel terminalModel = TerminalModel.createTerminalModel(transactionModel.getTerminalId(), true);

        terminalModel.setLoginModel(new LoginModel());
        terminalModel.getLoginModel().setUserId(transactionModel.getUserId());
        transactionModel.setDrawerNum(1);
       if( transactionModel.getLoyaltyAccount() != null && !transactionModel.getLoyaltyAccount().getEmail().equals("") ) {
            transactionModel.setEmail(transactionModel.getLoyaltyAccount().getEmail());
       }

        //create new store model
        StoreModel storeModel = StoreModel.getStoreModel(storeID, request);
        LocalDateTime donationSubmittedDateTime = LocalDateTime.now();

        //adjust for the terminal timezone offset if applicable
        if ( storeModel.getTimezoneOffset() != null && storeModel.getTimezoneOffset() != 0 ) {
            donationSubmittedDateTime = donationSubmittedDateTime.plusHours(storeModel.getTimezoneOffset());
        }

        //datetime of transaction
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");

        transactionModel.setTimeStamp(donationSubmittedDateTime.format(dtf));

        //user's name
        String name = getPersonName();
        transactionModel.setName(name);

        if ( !this.getErrorDetails().equals("") ) {
            Logger.logMessage("ERROR: Error found processing order: ".concat(this.getErrorDetails()), Logger.LEVEL.ERROR);
            return this;
        }

        TransactionBuilder transactionBuilder = TransactionBuilder.createTransactionBuilder(terminalModel, transactionModel, PosAPIHelper.ApiActionType.SALE, PosAPIHelper.TransactionType.SALE);

        //save transaction
        transactionBuilder.getTransactionModel().checkTransactionTypeEnum();
        transactionBuilder.buildTransaction();
        transactionBuilder.saveTransaction();


        this.setTransactionID(transactionModel.getId());
        transactionBuilder.sendPurchaseConfirmationEmail(transactionModel.getEmail());

        return this;
    }

    //CommonAPI code copied and altered. params and methods changed to fit donation validation usage.
    public static HashMap getUserBalanceForDonation(Integer terminalID, BigDecimal orderTotal, Integer authenticatedAccountID) throws Exception {
        HashMap userBalanceHM = new HashMap();
        DataManager dm = new DataManager();
        try {
            ArrayList<HashMap> result = null;
            if ( CommonAPI.isLocalWar() ) {
                result = dm.parameterizedExecuteQuery("data.posanywhere.PersonTerminalLookupOffline",
                        new Object[]{
                                terminalID,
                                Double.parseDouble(orderTotal.toString()),
                                authenticatedAccountID
                        },
                        true
                );
            } else {
                result = dm.parameterizedExecuteQuery("data.posanywhere.PersonTerminalLookupBP",
                        new Object[]{
                                terminalID,
                                Double.parseDouble(orderTotal.toString()),
                                authenticatedAccountID

                        },
                        true
                );
            }


            //should only be one result
            if (result != null && result.size() == 1) {
                HashMap resultHM = result.get(0);

                userBalanceHM = CommonAPI.calculateUserCurrentBalance(resultHM);

                BigDecimal userBalance = CommonAPI.convertModelDetailToBigDecimal(userBalanceHM.get("USERBALANCE"));
                if (userBalance.compareTo(orderTotal) < 0) {
                    //This is at best mostly redundant but it is what the POS does
                    ArrayList<HashMap> response = null;

                    if ( CommonAPI.isLocalWar() ) {
                        response = dm.parameterizedExecuteQuery("data.posanywhere.PersonTerminalLookupOffline",
                                new Object[]{
                                        terminalID,
                                        Double.parseDouble(orderTotal.toString()),
                                        authenticatedAccountID

                                },
                                true
                        );
                    } else {
                        response = dm.parameterizedExecuteQuery("data.posanywhere.PersonTerminalLookup",
                                new Object[]{
                                        terminalID,
                                        Double.parseDouble(orderTotal.toString()),
                                        authenticatedAccountID

                                },
                                true
                        );
                    }

                    if (response != null && response.size() == 1) {
                        HashMap responseHM = response.get(0);

                        userBalanceHM = CommonAPI.calculateUserCurrentBalance(resultHM);
                    }
                }
            } else {
                Logger.logMessage("Could not determine user's balance in OrderModel.getUserBalanceForDonation method", Logger.LEVEL.ERROR);
            }
        } catch (Exception ex) {
            throw new MissingDataException("Could not determine account balance. Purchase limit may be incorrectly configured.");
        }
        return userBalanceHM;
    }

    private List<DonationLineItemModel> populateDonationsForSubmission() throws Exception {
        Integer donationIndex = 1;
        List<DonationLineItemModel> donationLines = new ArrayList<>();

        for (DonationLineItemModel donationLineItemModel : this.getDonations()) {

            donationLineItemModel.setItemTypeId(PosAPIHelper.ObjectType.DONATION.toInt());
            donationLineItemModel.setItemId(donationLineItemModel.getDonation().getId());
            donationLineItemModel.setQuantity(BigDecimal.ONE);
            donationLineItemModel.setEmployeeId(getAuthenticatedAccountID());

            if(donationLineItemModel.getPoints() != null && donationLineItemModel.getPoints() > 0) {
                donationLineItemModel.setPoints(donationLineItemModel.getPoints() * -1);
            }

            donationLines.add(donationLineItemModel);
            donationIndex = donationIndex + 1;
        }

        return donationLines;
    }

    //getter
    private Integer getAuthenticatedAccountID() {
        return this.authenticatedAccountID;
    }

    //setter
    private void setAuthenticatedAccountID(Integer authenticatedAccountID) {
        this.authenticatedAccountID = authenticatedAccountID;
    }

    //getter
    public String getType() { return type; }

    //setter
    public void setType(String type) { this.type = type; }

    //getter
    public String getLocation() { return location; }

    //setter
    public void setLocation(String location) { this.location = location; }

    //getter
    public String getTime() { return time; }

    //setter
    public void setTime(String time) { this.time = time; }

    //getter
    public String getPhone() { return phone; }

    //setter
    public void setPhone(String phone) { this.phone = phone; }

    //getter
    public String getMobilePhone() { return mobilePhone; }

    //setter
    public void setMobilePhone(String mobilePhone) { this.mobilePhone = mobilePhone; }

    //getter
    public String getComments() { return comments; }

    //setter
    public void setComments(String comments) { this.comments = comments; }

    //getter
    public String getErrorDetails() { return errorDetails; }

    //setter
    public void setErrorDetails(String errorDetails) { this.errorDetails = errorDetails; }

    //getter
    public String getPersonName() { return personName; }

    //setter
    public void setPersonName(String personName) { this.personName = personName; }

    //getter
    public String getOrderNumber() { return orderNumber; }

    //setter
    public void setOrderNumber(String orderNumber) { this.orderNumber = orderNumber; }

    //getter
    public String getReadyTime() { return readyTime; }

    //setter
    public void setReadyTime(String readyTime) { this.readyTime = readyTime; }

    //getter
    public String getSubmittedTime() { return submittedTime; }

    //setter
    public void setSubmittedTime(String submittedTime) { this.submittedTime = submittedTime; }

    //getter
    public BigDecimal getSubtotal() { return subtotal; }

    //setter
    public void setSubtotal(BigDecimal subtotal) { this.subtotal = subtotal; }

    //getter
    public BigDecimal getSubtotalDiscount() { return subtotalDiscount; }

    //setter
    public void setSubtotalDiscount(BigDecimal subtotalDiscount) { this.subtotalDiscount = subtotalDiscount; }

    //getter
    public BigDecimal getTax() { return tax; }

    //setter
    public void setTax(BigDecimal tax) { this.tax = tax; }

    //getter
    public BigDecimal getDeliveryFee() { return deliveryFee; }

    //setter
    public void setDeliveryFee(BigDecimal deliveryFee) { this.deliveryFee = deliveryFee; }

    //getter
    public BigDecimal getDeliveryMinimum() { return deliveryMinimum; }

    //setter
    public void setDeliveryMinimum(BigDecimal deliveryMinimum) { this.deliveryMinimum = deliveryMinimum; }

    //getter
    public BigDecimal getTotal() { return total; }

    //setter
    public void setTotal(BigDecimal total) { this.total = total; }

    //getter
    public ArrayList<HashMap> getSelectedDiscountList() {
        return selectedDiscountList;
    }

    //setter
    public void setSelectedDiscountList(ArrayList<HashMap> selectedDiscountList) {
        this.selectedDiscountList = selectedDiscountList;
    }

    //getter
    public List<ProductModel> getProducts() {
        return products;
    }

    //setter
    public void setProducts(List<ProductModel> products) {
        this.products = products;
    }

    //getter
    public boolean getForceReAuth() { return forceReAuth; }

    //setter
    public void setForceReAuth(boolean forceReAuth) { this.forceReAuth = forceReAuth; }

    //getter
    public boolean getAdvanceScreen() { return advanceScreen; }

    //setter
    public void setAdvanceScreen(boolean advanceScreen) { this.advanceScreen = advanceScreen; }

    //getter
    public Integer getReAuthTypeID() { return reAuthTypeID; }

    //setter
    public void setReAuthTypeID(Integer reAuthTypeID) { this.reAuthTypeID = reAuthTypeID; }

    //getter
    public boolean getIgnoreInventoryCounts() {
        return ignoreInventoryCounts;
    }

    //setter
    public void setIgnoreInventoryCounts(boolean ignoreInventoryCounts) {
        this.ignoreInventoryCounts = ignoreInventoryCounts;
    }

    public String getEstimatedOrderTime() {
        return estimatedOrderTime;
    }

    public void setEstimatedOrderTime(String estimatedOrderTime) {
        this.estimatedOrderTime = estimatedOrderTime;
    }

    public BigDecimal getRewardsTotal() {
        return rewardsTotal;
    }

    public void setRewardsTotal(BigDecimal rewardsTotal) {
        this.rewardsTotal = rewardsTotal;
    }

    public List<LoyaltyRewardLineItemModel> getRewards() {
        return rewards;
    }

    public void setRewards(List<LoyaltyRewardLineItemModel> rewards) {
        this.rewards = rewards;
    }

    public Integer getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(Integer transactionID) {
        this.transactionID = transactionID;
    }

    public List<ComboLineItemModel> getCombos() {
        return combos;
    }

    public void setCombos(List<ComboLineItemModel> combos) {
        this.combos = combos;
    }

    public boolean isUsingMobile() {
        return isUsingMobile;
    }

    public void setUsingMobile(boolean usingMobile) {
        isUsingMobile = usingMobile;
    }

    public String getDiningOptionPAPluID() {
        return diningOptionPAPluID;
    }

    public void setDiningOptionPAPluID(String diningOptionPAPluID) {
        this.diningOptionPAPluID = diningOptionPAPluID;
    }

    public String getDiningOptionPAPluName() {
        return diningOptionPAPluName;
    }

    public void setDiningOptionPAPluName(String diningOptionPAPluName) {
        this.diningOptionPAPluName = diningOptionPAPluName;
    }

    public boolean isUseMealPlanDiscount() {
        return useMealPlanDiscount;
    }

    public void setUseMealPlanDiscount(boolean useMealPlanDiscount) {
        this.useMealPlanDiscount = useMealPlanDiscount;
    }

    public boolean getUseCreditCardAsTender() {
        return useCreditCardAsTender;
    }

    public void setUseCreditCardAsTender(boolean useCreditCardAsTender) {
        this.useCreditCardAsTender = useCreditCardAsTender;
    }

    public BigDecimal getCreditCardFee() {
        return creditCardFee;
    }

    public void setCreditCardFee(BigDecimal creditCardFee) {
        this.creditCardFee = creditCardFee;
    }

    public boolean isExpressReorder() {
        return expressReorder;
    }

    public void setExpressReorder(boolean expressReorder) {
        this.expressReorder = expressReorder;
    }

    public Integer getReorderTransactionID() {
        return reorderTransactionID;
    }

    public void setReorderTransactionID(Integer reorderTransactionID) {
        this.reorderTransactionID = reorderTransactionID;
    }

    public boolean getUseDiningOptions() {
        return useDiningOptions;
    }

    public void setUseDiningOptions(boolean useDiningOptions) {
        this.useDiningOptions = useDiningOptions;
    }

    public String getCreditCardFeeLabel() {
        return creditCardFeeLabel;
    }

    public void setCreditCardFeeLabel(String creditCardFeeLabel) {
        this.creditCardFeeLabel = creditCardFeeLabel;
    }

    public BigDecimal getSurchargeTotal() {
        return surchargeTotal;
    }

    public void setSurchargeTotal(BigDecimal surchargeTotal) {
        this.surchargeTotal = surchargeTotal;
    }

    public LocalDate getFutureOrderDate() {
        return futureOrderDate;
    }

    public void setFutureOrderDate(String futureOrderDate) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        this.futureOrderDate = LocalDate.parse(futureOrderDate, formatter);
    }

    public boolean isChargeAtPurchaseTime() {
        return chargeAtPurchaseTime;
    }

    public void setChargeAtPurchaseTime(boolean chargeAtPurchaseTime) {
        this.chargeAtPurchaseTime = chargeAtPurchaseTime;
    }

    //getter
    public List<SurchargeLineItemModel> getItemSurcharges() {
        return itemSurcharges;
    }

    //setter
    public void setItemSurcharges(List<SurchargeLineItemModel> itemSurcharges) {
        this.itemSurcharges = itemSurcharges;
    }


    public PrepOptionModel getPrepOption() {
        return prepOption;
    }

    public void setPrepOption(PrepOptionModel prepOption) {
        this.prepOption = prepOption;
    }

    public Integer getPersonID() {
        return personID;
    }

    public void setPersonID(Integer personID) {
        this.personID = personID;
    }

    public AccountFundingDetailModel getAccountFundingDetailModel() {
        return accountFundingDetailModel;
    }

    public void setAccountFundingDetailModel(AccountFundingDetailModel accountFundingDetailModel) {
        this.accountFundingDetailModel = accountFundingDetailModel;
    }

    public BigDecimal getUserBalance() {
        return userBalance;
    }

    public void setUserBalance(BigDecimal userBalance) {
        this.userBalance = userBalance;
    }

    public BigDecimal getCustomFundingAmount() {
        return customFundingAmount;
    }

    public void setCustomFundingAmount(BigDecimal customFundingAmount) {
        this.customFundingAmount = customFundingAmount;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public List<DonationLineItemModel> getDonations() {
        return donations;
    }

    public void setDonations(List<DonationLineItemModel> donations) {
        this.donations = donations;
    }

    public boolean getGrabAndGo() {
        return grabAndGo;
    }

    public void setGrabAndGo(boolean grabAndGo) {
        this.grabAndGo = grabAndGo;
    }

    public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    public BigDecimal getVoucherTotal() {
        return voucherTotal;
    }

    public void setVoucherTotal(BigDecimal voucherTotal) {
        this.voucherTotal = voucherTotal;
    }

    public List<LoyaltyPointModel> getLoyaltyPointsEarned() {
        return loyaltyPointsEarned;
    }

    public void setLoyaltyPointsEarned(List<LoyaltyPointModel> loyaltyPointsEarned) {
        this.loyaltyPointsEarned = loyaltyPointsEarned;
    }

    public List<DonationModel> getEmployeeDonations() {
        return employeeDonations;
    }

    public void setEmployeeDonations(List<DonationModel> employeeDonations) {
        this.employeeDonations = employeeDonations;
    }

}