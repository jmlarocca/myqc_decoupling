package com.mmhayes.myqc.server.models;

//mmhayes dependencies

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.dataaccess.commonMMHFunctions;
import com.mmhayes.common.utils.Logger;

//other dependencies


/* Feature Model
 Last Updated (automatically updated by SVN)
 $Author: ecdyer $: Author of last commit
 $Date: 2017-08-02 08:58:52 -0400 (Wed, 02 Aug 2017) $: Date of last commit
 $Rev: 23136 $: Revision of last commit

 Notes: Model for Employee Account Details
*/
public class FeatureModel {
    private commonMMHFunctions commFunc = new commonMMHFunctions();
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String id = "N/A"; //name of the feature itself
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String version = "N/A"; //N/A means there are no "sub versions" for this feature
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String available = "no"; //yes, no, partial

    //constructor - takes strings and sets them to the model
    public FeatureModel(String id, String version, String available) {
        try {
            setModelProperties(id, version, available);
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
    }

    //setter for all of this model's properties
    public FeatureModel setModelProperties(String id, String version, String available) {
        try {

            if (id != null && !id.isEmpty()) {
                setID(id);
            }

            if (version != null && !version.isEmpty()) {
                setVersion(version);
            }

            if (available != null && !available.isEmpty()) {
                setAvailable(available);
            }

        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
        return this;
    }

    //getter
    public String getID() {
        return this.id;
    }

    //setter
    public void setID(String id) {
        this.id = id;
    }

    //getter
    public String getVersion() {
        return this.version;
    }

    //setter
    public void setVersion(String version) {
        this.version = version;
    }

    //getter
    public String getAvailable() {
        return this.available;
    }

    //setter
    public void setAvailable(String available) {
        if (available.equals("")) {
            available = "N/A";
        }
        this.available = available;
    }

}
