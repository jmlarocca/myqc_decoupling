package com.mmhayes.myqc.server.models;

//mmhayes dependencies

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.utils.Logger;

import java.util.HashMap;

//other dependencies

/* My QC Nav Model
 Last Updated (automatically updated by SVN)
 $Author: ecdyer $: Author of last commit
 $Date: 2017-08-02 08:58:52 -0400 (Wed, 02 Aug 2017) $: Date of last commit
 $Rev: 23136 $: Revision of last commit

 Notes: Model for the front-end navigation menu for the my qc application
*/
public class MyQCNavModel {
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String id = null;
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String title = "N/A";

    //constructor - takes hashmap that get sets to this models properties
    public MyQCNavModel(HashMap modelDetailHM) {
        try {
            setModelProperties(modelDetailHM);
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
    }

    //setter for all of this model's properties
    public MyQCNavModel setModelProperties(HashMap modelDetailHM) {
        try {
            if (modelDetailHM.get("id") != null) {
                setID(modelDetailHM.get("id").toString());
            }
            if (modelDetailHM.get("title") != null) {
                setTitle(modelDetailHM.get("title").toString());
            }
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
        return this;
    }

    //getter
    public String getID() {
        return this.id;
    }

    //setter
    public void setID(String id) {
        this.id = id;
    }

    //getter
    public String getTitle() {
        return title;
    }

    //setter
    public void setTitle(String title) {
        this.title = title;
    }

}
