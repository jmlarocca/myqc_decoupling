package com.mmhayes.myqc.server.models;

//mmhayes dependencies

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.commonMMHFunctions;
import com.mmhayes.common.funding.models.AccountPaymentMethodModel;
import com.mmhayes.common.transaction.models.TaxModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

//other dependencies

/* Meal Plan Model
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2019-06-03 11:44:15 -0400 (Mon, 03 Jun 2019) $: Date of last commit
 $Rev: 38998 $: Revision of last commit

 Notes: Model for Meal Plans
*/
public class MealPlanModel {
    private static DataManager dm = new DataManager();
    private commonMMHFunctions commFunc = new commonMMHFunctions();
    private Integer loggedInEmployeeID = null;
    Integer fundingTerminalID = null;
    Integer profilePurchasePAPluID = null;
    Integer surchargeID = null;
    Integer chosenSpendingProfileID = null;
    Integer CCTenderID = null;
    BigDecimal price = BigDecimal.ZERO;
    BigDecimal subtotal = BigDecimal.ZERO;
    BigDecimal total = BigDecimal.ZERO;
    BigDecimal taxTotal = null;
    BigDecimal fundingFee = null;
    AccountPaymentMethodModel accountPaymentMethodModel = null;
    Boolean addFundingTerminalFees = null;
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String mealPlanTaxIDs = "";
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String surchargeTaxIDs = "";
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String accountFundingName = "";
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String surchargeName = "";
    List<TaxModel> mealPlanTaxModels = new ArrayList<>();
    List<TaxModel> surchargeTaxModels = new ArrayList<>();

    //constructor - standard
    public MealPlanModel() {

    }

    public Integer getLoggedInEmployeeID() {
        return loggedInEmployeeID;
    }

    public void setLoggedInEmployeeID(Integer loggedInEmployeeID) {
        this.loggedInEmployeeID = loggedInEmployeeID;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getFundingTerminalID() {
        return fundingTerminalID;
    }

    public void setFundingTerminalID(Integer fundingTerminalID) {
        this.fundingTerminalID = fundingTerminalID;
    }

    public Integer getProfilePurchasePAPluID() {
        return profilePurchasePAPluID;
    }

    public void setProfilePurchasePAPluID(Integer profilePurchasePAPluID) {
        this.profilePurchasePAPluID = profilePurchasePAPluID;
    }

    public Integer getSurchargeID() {
        return surchargeID;
    }

    public void setSurchargeID(Integer surchargeID) {
        this.surchargeID = surchargeID;
    }

    public String getMealPlanTaxIDs() {
        return mealPlanTaxIDs;
    }

    public void setMealPlanTaxIDs(String mealPlanTaxIDs) {
        this.mealPlanTaxIDs = mealPlanTaxIDs;
    }

    public String getSurchargeTaxIDs() {
        return surchargeTaxIDs;
    }

    public void setSurchargeTaxIDs(String surchargeTaxIDs) {
        this.surchargeTaxIDs = surchargeTaxIDs;
    }

    public BigDecimal getTaxTotal() {
        return taxTotal;
    }

    public void setTaxTotal(BigDecimal taxTotal) {
        this.taxTotal = taxTotal;
    }

    public BigDecimal getFundingFee() {
        return fundingFee;
    }

    public void setFundingFee(BigDecimal fundingFee) {
        this.fundingFee = fundingFee;
    }

    public Boolean getAddFundingTerminalFees() {
        return addFundingTerminalFees;
    }

    public void setAddFundingTerminalFees(Boolean addFundingTerminalFees) {
        this.addFundingTerminalFees = addFundingTerminalFees;
    }

    public AccountPaymentMethodModel getAccountPaymentMethodModel() {
        return accountPaymentMethodModel;
    }

    public void setAccountPaymentMethodModel(AccountPaymentMethodModel accountPaymentMethodModel) {
        this.accountPaymentMethodModel = accountPaymentMethodModel;
    }

    public String getAccountFundingName() {
        return accountFundingName;
    }

    public void setAccountFundingName(String accountFundingName) {
        this.accountFundingName = accountFundingName;
    }

    public BigDecimal getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(BigDecimal subtotal) {
        this.subtotal = subtotal;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public Integer getChosenSpendingProfileID() {
        return chosenSpendingProfileID;
    }

    public void setChosenSpendingProfileID(Integer chosenSpendingProfileID) {
        this.chosenSpendingProfileID = chosenSpendingProfileID;
    }

    public List<TaxModel> getMealPlanTaxModels() {
        return mealPlanTaxModels;
    }

    public void setMealPlanTaxModels(List<TaxModel> mealPlanTaxModels) {
        this.mealPlanTaxModels = mealPlanTaxModels;
    }

    public List<TaxModel> getSurchargeTaxModels() {
        return surchargeTaxModels;
    }

    public void setSurchargeTaxModels(List<TaxModel> surchargeTaxModels) {
        this.surchargeTaxModels = surchargeTaxModels;
    }

    public Integer getCCTenderID() {
        return CCTenderID;
    }

    public void setCCTenderID(Integer CCTenderID) {
        this.CCTenderID = CCTenderID;
    }

    public String getSurchargeName() {
        return surchargeName;
    }

    public void setSurchargeName(String surchargeName) {
        this.surchargeName = surchargeName;
    }

}
