package com.mmhayes.myqc.server.models;

//mmhayes dependencies

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.dataaccess.commonMMHFunctions;
import com.mmhayes.common.utils.Logger;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;

//other dependencies

/* Fund Model
 Last Updated (automatically updated by SVN)
 $Author: jmdottavio $: Author of last commit
 $Date: 2018-10-23 16:48:52 -0400 (Tue, 23 Oct 2018) $: Date of last commit
 $Rev: 34134 $: Revision of last commit

 Notes: Model for Employee Funds (QC Transactions on against a special terminal type)
*/
public class FundModel {
    private commonMMHFunctions commFunc = new commonMMHFunctions();
    Integer id = null;
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String comment = "N/A";
    BigDecimal amt = null;
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String date = null;
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String fundedBy = "N/A";
    BigDecimal fundingFee = null;
    private LocalDateTime dateTime = null; //also switch getters and setters to make public - 6/12/2015 -jrmitaly

    //constructor - takes hashmap that get sets to this models properties
    public FundModel(HashMap modelDetailHM) {
        try {
            setModelProperties(modelDetailHM);
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
    }

    //setter for all of this model's properties
    public FundModel setModelProperties(HashMap modelDetailHM) {
        try {
            if (modelDetailHM.get("TRANSID") != null) {
                setID(Integer.parseInt(modelDetailHM.get("TRANSID").toString()));
            }
            if (modelDetailHM.get("TRANSCOMMENT") != null) {
                setComment(modelDetailHM.get("TRANSCOMMENT").toString());
            }
            if (modelDetailHM.get("TRANSAMOUNT") != null) {
                setAmt(new BigDecimal(modelDetailHM.get("TRANSAMOUNT").toString()));
            }
            if (modelDetailHM.get("TRANSFEE") != null) {
                setFundingFee(new BigDecimal(modelDetailHM.get("TRANSFEE").toString()));
            }
            if (modelDetailHM.get("FUNDEDBY") != null) {
                setFundedBy(modelDetailHM.get("FUNDEDBY").toString());
            }
            if (modelDetailHM.get("TRANSDATE") != null) {
                try {

                    //specify in and out formats for formatting modelDetailHM.get("TRANSDATE")
                    String dateTimeInFormat = "yyyy-MM-dd HH:mm:ss.S";
                    String dateTimeOutFormat = "M/dd/yyyy h:mm:ss a"; //old way was - M/dd/yyyy H:mm:ss - jrmitaly 6/23/2015

                    //format modelDetailHM.get("TRANSDATE") and set to dateTimeStr
                    String dateTimeStr = commFunc.formatDateTime(modelDetailHM.get("TRANSDATE").toString(),dateTimeInFormat,dateTimeOutFormat);

                    //parse dateTimeStr to LocalDateTime and set that to this.dateTime
                    DateTimeFormatter transactionDateTimeFormatter = DateTimeFormatter.ofPattern(dateTimeOutFormat);
                    setDateTime(LocalDateTime.parse(dateTimeStr, transactionDateTimeFormatter));

                    //set this.date
                    //setDate(dateTimeStr.split(" ")[0]); //for just the date
                    setDate(dateTimeStr); //for date and time

                } catch (Exception ex) {
                    Logger.logMessage("ERROR: Issue with date formatting in FundModel.setModel()", Logger.LEVEL.ERROR);
                    Logger.logException(ex); //always log exceptions
                }
            }
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
        return this;
    }

    //getter
    public Integer getID() {
        return this.id;
    }

    //setter
    public void setID(Integer id) {
        this.id = id;
    }

    //getter
    public String getComment() {
        return comment;
    }

    //setter
    public void setComment(String comment) {
        this.comment = comment;
    }

    //getter
    public BigDecimal getAmt() {
        return amt;
    }

    //setter
    public void setAmt(BigDecimal amt) {
        this.amt = amt;
    }

    //getter
    public String getDate() {
        return date;
    }

    //setter
    public void setDate(String date) {
        this.date = date;
    }

    //getter - PRIVATE SO WE DON'T RETURN WITH API, switch to public to return - 6/12/2015 -jrmitaly
    private LocalDateTime getDateTime() {
        return dateTime;
    }

    //setter - PRIVATE SO WE DON'T RETURN WITH API, switch to public to return - 6/12/2015 -jrmitaly
    private void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public String getFundedBy() {
        return fundedBy;
    }

    public void setFundedBy(String fundedBy) {
        this.fundedBy = fundedBy;
    }

    public BigDecimal getFundingFee() {
        return fundingFee;
    }

    public void setFundingFee(BigDecimal fundingFee) {
        this.fundingFee = fundingFee;
    }
}
