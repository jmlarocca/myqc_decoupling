package com.mmhayes.myqc.server.models;

//mmhayes dependencies

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.utils.Logger;
import org.apache.xpath.operations.Mod;

import java.math.BigDecimal;
import java.util.*;

//javax.ws.rs dependencies
//myqc dependencies
//other dependencies

/* Express Order Item Model
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2019-03-13 17:18:12 -0400 (Wed, 13 Mar 2019) $: Date of last commit
 $Rev: 37153 $: Revision of last commit

 Notes: Model for express order items (a.k.a products and products with modifiers)
*/
public class ExpressOrderItemModel {
    private Integer id = null;
    private Integer lineID = null;
    private Integer productID = null;
    private Integer keypadID = null;
    private Integer modifierSetID = null;
    private Integer paComboID = null;
    private Integer comboTransLineItemId = null;
    private Integer comboDetailId = null;
    private Integer subDeptID = null;
    private Integer departmentID = null;
    private BigDecimal basePrice = null;
    private BigDecimal comboPrice = null;
    private BigDecimal upcharge = null;
    private BigDecimal quantity = BigDecimal.ONE;
    private BigDecimal price = BigDecimal.ZERO;
    private BigDecimal originalPrice = BigDecimal.ZERO;
    private BigDecimal fixedTareWeight = null; //Fixed Tare Weight for product
    private Boolean scaleUsed = false;
    private Boolean autoShowModSet = false;
    private List<ModifierModel> modifiers = new ArrayList<>(); //this Array List represents a ModifierCollection which contains a list of ModifierModels
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    private String name = "";
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String image = ""; //for setting the image in the product view
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    private String comboName = "";
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    private String tareName = ""; // Tare assigned to product.
    PrepOptionModel prepOption = null;
    Boolean hasPrinterMappings = true; //flag that determines if this item has Kitchen Printer Mappings

    //dummy constructor - needed to consume ANY JSON object via REST (Jersey/Jackson style)
    public ExpressOrderItemModel() {

    }

    //constructor - takes hashmap that get sets to this models properties
    public ExpressOrderItemModel(HashMap modelDetailHM) throws Exception {
        setModelProperties(modelDetailHM);
    }


    //setter for all of the favorite order model's for the My Quick Picks page
    public ExpressOrderItemModel setModelProperties(HashMap modelDetailHM) throws Exception{
        setID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));
        setLineID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("LINEID")));
        setProductID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));
        setKeypadID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAKEYPADID")));
        setQuantity(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("QUANTITY")));
        setPrice(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("PRICE")));
        setOriginalPrice(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("PRICE")));
        setName(CommonAPI.convertModelDetailToString(modelDetailHM.get("PRODUCTNAME")));
        setScaleUsed(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("SCALEUSED")));
        setAutoShowModSet(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("AUTOSHOWMODSET")));
        setModifierSetID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("MODIFIERSETID")));
        setImage(CommonAPI.convertModelDetailToString(modelDetailHM.get("IMAGE")));
        setSubDeptID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PASUBDEPTID")));
        setDepartmentID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PADEPARTMENTID")));
        setHasPrinterMappings(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("HASPRINTERMAPPINGS"), true));

        setComboName(CommonAPI.convertModelDetailToString(modelDetailHM.get("COMBONAME")));

        setPAComboID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PACOMBOID")));
        setComboTransLineItemId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PACOMBOTRANSLINEITEMID")));
        setComboDetailId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PACOMBODETAILID")));
        setComboPrice(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("COMBOPRICE")));
        setBasePrice(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("BASEPRICE")));

        setFixedTareWeight(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("FIXEDTAREWEIGHT")));
        setTareName(CommonAPI.convertModelDetailToString(modelDetailHM.get("TARENAME")));

        if( (modelDetailHM.containsKey("PAPREPOPTIONID") && !modelDetailHM.get("PAPREPOPTIONID").equals("")) ||
                (modelDetailHM.containsKey("PAPREPOPTIONSETID") && !modelDetailHM.get("PAPREPOPTIONSETID").equals(""))) {

            PrepOptionModel prepOptionModel = getPrepOptionModel(modelDetailHM);
            setPrepOption(prepOptionModel);

            BigDecimal price = this.getOriginalPrice().add(prepOptionModel.getPrice());
            setPrice(price);
        }

        //if the keypadID wasn't saved on the transaction line and the line is from a combo, use the comboID and comboDetailID to determine which keypad it belongs to
        if(getPAComboID() != null && getKeypadID() == null) {
            ComboModel comboModel = new ComboModel(getPAComboID());

            //find which comdo detail is assoicated with the product line
            for(ComboDetailModel comboDetailModel : comboModel.getComboDetails()) {
                if(getComboDetailId() != null && getComboDetailId().equals(comboDetailModel.getID())) {
                    setKeypadID(comboDetailModel.getKeypadId());
                    break;
                }
            }
        }

        return this;
    }

    public void updateModelModifiers(HashMap modelDetailHM) throws Exception {

        Integer modifierCount = getModifiers().size();
        modelDetailHM.put("MODINDEX", modifierCount + 1);
        modelDetailHM.put("HASNUTRITION", false);
        modelDetailHM.put("KEYPADID", CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAKEYPADID")));
        modelDetailHM.put("MODNAME", CommonAPI.convertModelDetailToString(modelDetailHM.get("PRODUCTNAME")));
        modelDetailHM.put("MODIFIERID", CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));

        ModifierModel modifierModel = new ModifierModel(modelDetailHM);

        if( (modelDetailHM.containsKey("PAPREPOPTIONID") && !modelDetailHM.get("PAPREPOPTIONID").equals("")) ||
                (modelDetailHM.containsKey("PAPREPOPTIONSETID") && !modelDetailHM.get("PAPREPOPTIONSETID").equals(""))) {

            PrepOptionModel prepOptionModel = getPrepOptionModel(modelDetailHM);
            modifierModel.setPrepOption(prepOptionModel);

            BigDecimal price = this.getOriginalPrice().add(prepOptionModel.getPrice());
            setPrice(price);
        }

        getModifiers().add(modifierModel);

        updateItemPriceBasedOnModifiers();

    }

    //updates the price of the product by iterating through all the modifiers and adjusting the price accordingly
    public void updateItemPriceBasedOnModifiers() {
        try {

            setPrice(getOriginalPrice()); //reset the price to original product price before we add all modifier prices

            if(getPrepOption() != null) {
                BigDecimal currentProductPrice = this.getPrice();
                BigDecimal newProductPrice = currentProductPrice.add(getPrepOption().getPrice());
                setPrice(newProductPrice);
            }

            for (ModifierModel modifierModel : getModifiers()) {
                if (modifierModel.getID() != null && !modifierModel.getID().toString().isEmpty()) {

                    //if this modifier really has a price
                    if (modifierModel.getPrice() != null && !modifierModel.getID().toString().isEmpty()) {
                        //expanded out to show what is going on... valid one liner... this.getPrice().add(modifierModel.getPrice()
                        BigDecimal currentProductPrice = this.getPrice();
                        BigDecimal newProductPrice = currentProductPrice.add(modifierModel.getPrice());

                        if(modifierModel.getPrepOption() != null) {
                            newProductPrice = newProductPrice.add(modifierModel.getPrepOption().getPrice());
                        }

                        setPrice(newProductPrice);
                    }

                } else {
                    Logger.logMessage("ERROR: Could not determine Modifier Model in ExpressOrderItemModel.updateItemPriceBasedOnModifiers()", Logger.LEVEL.ERROR);
                }
            }

        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
    }

    public PrepOptionModel getPrepOptionModel(HashMap modelDetailHM) throws Exception {

        Integer prepOptionSetID = CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAPREPOPTIONSETID"));
        Integer prepOptionID = CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAPREPOPTIONID"));

        PrepOptionModel prepOptionModel = new PrepOptionModel();

        if(prepOptionSetID != null && prepOptionID == null) {

            prepOptionModel.getDefaultPrepOptionModel(prepOptionSetID);

        } else {
            BigDecimal prepPrice = CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("PREPPRICE"), BigDecimal.ZERO);
            String prepName = CommonAPI.convertModelDetailToString(modelDetailHM.get("PREPNAME"));
            boolean displayDefault = CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("DISPLAYDEFAULTMYQC"), false);
            boolean defaultOption = CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("DEFAULTOPTION"), false);

            prepOptionModel.setID(prepOptionID);
            prepOptionModel.setPrepOptionSetID(prepOptionSetID);
            prepOptionModel.setPrice(prepPrice);
            prepOptionModel.setName(prepName);
            prepOptionModel.setDisplayDefault(displayDefault);
            prepOptionModel.setDefaultOption(defaultOption);
        }

        return prepOptionModel;
    }


    @JsonIgnore
    public List<Integer> getSortedModifierIDs() {
        ArrayList<Integer> sortedModifierIDs = new ArrayList<Integer>();

        List<ModifierModel> modifierModels = new ArrayList<ModifierModel>( this.getModifiers() );

        Collections.sort( modifierModels, new Comparator<ModifierModel>() {
            public int compare(ModifierModel o1, ModifierModel o2) {
                try {
                    return o1.getID() < o2.getID() ? -1 : o1.getID() > o2.getID() ? 1 : 0;
                } catch (Exception ex) {
                     return -1;
                }
            }
        });

        for ( ModifierModel modifierModel : modifierModels ) {
            sortedModifierIDs.add(modifierModel.getID());
        }

        return sortedModifierIDs;
    }

    @JsonIgnore
    public List<ModifierModel> getSortedModifierModels() {
        List<ModifierModel> sortedModifierModels = new ArrayList<>();

        List<ModifierModel> modifierModels = new ArrayList<ModifierModel>( this.getModifiers() );

        Collections.sort( modifierModels, new Comparator<ModifierModel>() {
            public int compare(ModifierModel o1, ModifierModel o2) {
                try {
                    return o1.getID() < o2.getID() ? -1 : o1.getID() > o2.getID() ? 1 : 0;
                } catch (Exception ex) {
                    return -1;
                }
            }
        });

        sortedModifierModels = modifierModels;

        return sortedModifierModels;
    }

    //getter
    public Integer getID() {
        return this.id;
    }

    //setter
    public void setID(Integer id) {
        this.id = id;
    }

    public Integer getProductID() {
        return productID;
    }

    public void setProductID(Integer productID) {
        this.productID = productID;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public List<ModifierModel> getModifiers() {
        return modifiers;
    }

    public void setModifiers(List<ModifierModel> modifiers) {
        this.modifiers = modifiers;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name.replace("\\r", " ");
    }

    public BigDecimal getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(BigDecimal originalPrice) {
        this.originalPrice = originalPrice;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public Boolean getScaleUsed() {
        return scaleUsed;
    }

    public void setScaleUsed(Boolean scaleUsed) {
        this.scaleUsed = scaleUsed;
    }

    public PrepOptionModel getPrepOption() {
        return prepOption;
    }

    public void setPrepOption(PrepOptionModel prepOption) {
        this.prepOption = prepOption;
    }

    public Integer getLineID() {
        return lineID;
    }

    public void setLineID(Integer lineID) {
        this.lineID = lineID;
    }

    public Integer getKeypadID() {
        return keypadID;
    }

    public void setKeypadID(Integer keypadID) {
        this.keypadID = keypadID;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Boolean getAutoShowModSet() {
        return autoShowModSet;
    }

    public void setAutoShowModSet(Boolean autoShowModSet) {
        this.autoShowModSet = autoShowModSet;
    }

    public Integer getComboTransLineItemId() {
        return comboTransLineItemId;
    }

    public void setComboTransLineItemId(Integer comboTransLineItemId) {
        this.comboTransLineItemId = comboTransLineItemId;
    }

    public Integer getComboDetailId() {
        return comboDetailId;
    }

    public void setComboDetailId(Integer comboDetailId) {
        this.comboDetailId = comboDetailId;
    }

    public BigDecimal getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(BigDecimal basePrice) {
        this.basePrice = basePrice;
    }

    public BigDecimal getComboPrice() {
        return comboPrice;
    }

    public void setComboPrice(BigDecimal comboPrice) {
        this.comboPrice = comboPrice;
    }

    public BigDecimal getUpcharge() {
        return upcharge;
    }

    public void setUpcharge(BigDecimal upcharge) {
        this.upcharge = upcharge;
    }

    public String getComboName() {
        return comboName;
    }

    public void setComboName(String comboName) {
        this.comboName = comboName;
    }

    public Integer getPAComboID() {
        return paComboID;
    }

    public void setPAComboID(Integer paComboID) {
        this.paComboID = paComboID;
    }

    public Integer getSubDeptID() {
        return subDeptID;
    }

    public void setSubDeptID(Integer subDeptID) {
        this.subDeptID = subDeptID;
    }

    public Integer getDepartmentID() {
        return departmentID;
    }

    public void setDepartmentID(Integer departmentID) {
        this.departmentID = departmentID;
    }

    public BigDecimal getFixedTareWeight(){
        return this.fixedTareWeight;
    }

    public void setFixedTareWeight(BigDecimal fixedTareWeight){
        this.fixedTareWeight = fixedTareWeight;
    }

    public String getTareName(){
        return this.tareName;
    }

    public void setTareName(String tareName){
        this.tareName = tareName;
    }

    public Boolean getHasPrinterMappings() {
        return hasPrinterMappings;
    }

    public void setHasPrinterMappings(Boolean hasPrinterMappings) {
        this.hasPrinterMappings = hasPrinterMappings;
    }

    public Integer getModifierSetID() {
        return modifierSetID;
    }

    public void setModifierSetID(Integer modifierSetID) {
        this.modifierSetID = modifierSetID;
    }
}

