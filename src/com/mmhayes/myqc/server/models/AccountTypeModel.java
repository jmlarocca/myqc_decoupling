package com.mmhayes.myqc.server.models;

//mmhayes dependencies

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.dataaccess.commonMMHFunctions;
import com.mmhayes.common.utils.Logger;

import java.util.HashMap;

//other dependencies

/* Account Type Model
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2018-06-15 13:06:35 -0400 (Fri, 15 Jun 2018) $: Date of last commit
 $Rev: 30521 $: Revision of last commit

 Notes: Model for Account Types
*/
public class AccountTypeModel {
    private commonMMHFunctions commFunc = new commonMMHFunctions();
    Integer id = null;
    Integer accountGroupID = null;
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String value = "N/A";

    //constructor - takes hashmap that get sets to this models properties
    public AccountTypeModel(HashMap modelDetailHM) {
        try {
            setModelProperties(modelDetailHM);
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
    }

    //setter for all of this model's properties
    public AccountTypeModel setModelProperties(HashMap modelDetailHM) {
        try {
            if (modelDetailHM.get("ACCOUNTTYPEID") != null) {
                setID(Integer.parseInt(modelDetailHM.get("ACCOUNTTYPEID").toString()));
            }
            if (modelDetailHM.get("NAME") != null) {
                setValue(modelDetailHM.get("NAME").toString());
            }
            if (modelDetailHM.get("PAYROLLGROUPID") != null) {
                setAccountGroupID(Integer.parseInt(modelDetailHM.get("PAYROLLGROUPID").toString()));
            }
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
        return this;
    }

    //getter
    public Integer getID() {
        return this.id;
    }

    //setter
    public void setID(Integer id) {
        this.id = id;
    }

    //getter
    public String getValue() {
        return value;
    }

    //setter
    public void setValue(String value) {
        this.value = value;
    }

    //getter
    public Integer getAccountGroupID() {
        return this.accountGroupID;
    }

    //setter
    public void setAccountGroupID(Integer accountGroupID) {
        this.accountGroupID = accountGroupID;
    }

}
