package com.mmhayes.myqc.server.models;

//mmhayes dependencies

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.CommonAuthResource;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.myqc.server.collections.FavoriteCollection;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

//myqc dependencies
//other dependencies

/**
 * Created by admin on 10/18/2016.
 */
public class CartModel {
    List<ProductModel> products = new ArrayList<>();
    StoreModel storeModel = null;
    Integer storeID = null;
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String message = "";
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String orderType = "";
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String phone = "";  //for express reorder
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String comments = "";  //for express reorder
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String location = "";  //for express reorder
    Integer tenderTypeID = null; //for express reorder
    boolean useDiningOptions = false; //for express reorder
    boolean expressReorder = false; //flag that determines if this is an express reorder
    private static DataManager dm = new DataManager();
    private Integer authenticatedAccountID = null;

    //Constructor that takes a transactionID - for reordering
    public CartModel(Integer transactionID, HttpServletRequest request) throws Exception {
        setModelProperties(transactionID, request);
    }

    //Constructor that takes a transactionID - for reordering
    public CartModel(Integer transactionID, boolean expressOrder, HttpServletRequest request) throws Exception {
        setExpressReorder(expressOrder);
        setModelProperties(transactionID, request);
    }

    //START Reordering Functionality

    //attempt to re-build the cart from a given transactionID
    private void setModelProperties(Integer transactionID, HttpServletRequest request) throws Exception {
        //determine the authenticated account ID from the request
        setAuthenticatedAccountID(CommonAuthResource.checkRequestHeaderForMMHayesAuth(request));

        if ( getAuthenticatedAccountID() == null ) {
            Logger.logMessage("ERROR: Could not determine authenticated accountID in CartModel(transactionID)", Logger.LEVEL.ERROR);
            return;
        }

        //get transaction header details - terminalID, storeID, orderTypeID
        HashMap transactionDetailHM = getTransactionDetails(transactionID);

        //ensure valid transaction details, user can see the store and that the store is open
        if ( transactionDetailHM == null || transactionDetailHM.size() == 0 || !verifyStoreDetails(transactionDetailHM, request) ) {
            return;
        }

        setPhone(CommonAPI.convertModelDetailToString(transactionDetailHM.get("PHONE")));
        setTenderTypeID(CommonAPI.convertModelDetailToInteger(transactionDetailHM.get("PATENDERTYPEID")));
        setUseDiningOptions(CommonAPI.convertModelDetailToBoolean(transactionDetailHM.get("PROMPTFORDININGOPTIONS")));
        setComments(CommonAPI.convertModelDetailToString(transactionDetailHM.get("TRANSCOMMENT")));
        setLocation(CommonAPI.convertModelDetailToString(transactionDetailHM.get("ADDRESS")));

        //store is valid, check and set the products related to the transaction
        validateTransactionProducts(transactionID, transactionDetailHM);
    }

    //get out all relevant transaction header details, like the storeID and orderTypeID
    private HashMap getTransactionDetails(Integer transactionID) throws Exception  {
        ArrayList<HashMap> transactionDetails = dm.parameterizedExecuteQuery("data.ordering.getTransactionDetails",
                new Object[]{
                        transactionID
                },
                true
        );

        if ( transactionDetails == null || transactionDetails.size() != 1 || transactionDetails.get(0) == null ) {
            Logger.logMessage("ERROR: Unable to retrieve transaction data for transactionID: ".concat(transactionID.toString()), Logger.LEVEL.ERROR);
            setMessage("Error retrieving transaction details");
            return new HashMap();
        }

        return transactionDetails.get(0);
    }

    //all checking and setting related to the user and store is done here - store in spending profile, store open, setting storeID, storeModel, order type
    private boolean verifyStoreDetails(HashMap transactionDetailHM, HttpServletRequest request) throws Exception {
        Integer terminalID = CommonAPI.convertModelDetailToInteger(transactionDetailHM.get("TERMINALID"));

        Object spendingProfileIndex = dm.parameterizedExecuteScalar("data.ordering.determineStoreInSpendingProfile",
                new Object[]{
                        getAuthenticatedAccountID(),
                        terminalID
                }
        );

        if ( spendingProfileIndex == null || !(CommonAPI.convertModelDetailToInteger(spendingProfileIndex) > 0) ) {
            setMessage("Selected store is not currently available.");
            Logger.logMessage("ERROR: Store no longer in user's spending profile OR store is not active in CartModel.verifyStoreDetails", Logger.LEVEL.ERROR);
            return false;
        }

        //get out orderTypeID ( 2 - delivery,  3 - pickup )
        Integer orderTypeID = CommonAPI.convertModelDetailToInteger(transactionDetailHM.get("PAORDERTYPEID"));

        //we are now preventing
        if ( orderTypeID == 1 ) {
            setMessage("Selected order is not available for reordering, try a different order.");
            Logger.logMessage("Order could not be reordered because it was a POS transaction", Logger.LEVEL.TRACE);
            return false;
        }

        //set the storeID and storeModel for this cartModel
        setStoreID(CommonAPI.convertModelDetailToInteger(transactionDetailHM.get("MYQCTERMINALID")));
        setStoreModel(StoreModel.getStoreModel(getStoreID(), request));

        //ensure store is currently open to reorder from, if not then return incomplete cart object
        return determineStoreOpen(orderTypeID);
    }

    /*determines if the user would currently see the store on the store list view based on ordering schedules
    different from OrderModel.validateOrderTime because it considers both pickup and delivery and returns true if either is open
    sets the orderType parameter on the CartModel*/
    private boolean determineStoreOpen(Integer orderTypeID) throws Exception  {
        boolean pickupValid = false;
        boolean deliveryValid = false;
        boolean pickupOpen = false;
        boolean deliveryOpen = false;

        LocalTime orderTime = getStoreModel().getCurrentStoreDTM().toLocalTime();
        DateTimeFormatter dtformatter = DateTimeFormatter.ofPattern("h:mm a");

        String pickUpOrderingOpen = getStoreModel().getPickUpOrderingOpenTime();
        String pickUpOrderingClose = getStoreModel().getPickUpOrderingCloseTime();

        String deliveryOrderingOpen = getStoreModel().getDeliveryOrderingOpenTime();
        String deliveryOrderingClose = getStoreModel().getDeliveryOrderingCloseTime();

        if ( pickUpOrderingOpen.length() > 0 && pickUpOrderingClose.length() > 0 ) {
            pickupValid = true;
        }

        if ( deliveryOrderingOpen.length() > 0 && deliveryOrderingClose.length() > 0 ) {
            deliveryValid = true;
        }

        if ( pickupValid ) {
            LocalTime pickUpOrderingOpenTime = LocalTime.parse(pickUpOrderingOpen, dtformatter);
            LocalTime pickUpOrderingCloseTime = LocalTime.parse(pickUpOrderingClose, dtformatter);

            //check submitted time is between orderingOpenTime and orderingCloseTime
            if ( pickUpOrderingOpenTime.compareTo(orderTime) < 0 && pickUpOrderingCloseTime.compareTo(orderTime) > 0 ) {
                pickupOpen = true;
            }
        }

        if ( deliveryValid ) {
            LocalTime deliveryOrderingOpenTime = LocalTime.parse(deliveryOrderingOpen, dtformatter);
            LocalTime deliveryOrderingCloseTime = LocalTime.parse(deliveryOrderingClose, dtformatter);

            //check submitted time is between orderingOpenTime and orderingCloseTime
            if ( deliveryOrderingOpenTime.compareTo(orderTime) < 0 && deliveryOrderingCloseTime.compareTo(orderTime) > 0 ) {
                deliveryOpen = true;
            }
        }

        if ( !pickupOpen && !deliveryOpen ) {
            setMessage("Store is not currently accepting orders.");
            return false;
        }

        if ( orderTypeID == 2 ) {
            if ( deliveryOpen ) {
                setOrderType("delivery");
            } else {
                setOrderType("pickup");
            }
        } else {
            if ( pickupOpen ) {
                setOrderType("pickup");
            } else {
                setOrderType("delivery");
            }
        }

        return true;
    }

    //retrieve transLines based on transactionID
    private ArrayList<HashMap> getTransLines(Integer transactionID) throws Exception  {
        ArrayList<HashMap> transLines = dm.parameterizedExecuteQuery("data.ordering.getTransLines",
                new Object[]{
                        transactionID
                },
                true
        );

        if ( transLines == null || transLines.size() == 0 ) {
            setMessage("Error retrieving transaction details");
            return new ArrayList<>();
        }

        return transLines;
    }


    //validates and sets the products on the cart for reordering the given transactionID
    private void validateTransactionProducts(Integer transactionID, HashMap transactionDetailHM) throws Exception {
        ArrayList<HashMap> transLines = getTransLines(transactionID);

        Integer revenueCenterID = CommonAPI.convertModelDetailToInteger(transactionDetailHM.get("REVENUECENTERID"));
        Integer defaultLocationID = CommonAPI.convertModelDetailToInteger(transactionDetailHM.get("DEFAULTLOCATIONID"));
        Integer terminalID = CommonAPI.convertModelDetailToInteger(transactionDetailHM.get("TERMINALID"));

        boolean considerRCInventory = false;

        if ( defaultLocationID != null && defaultLocationID > 0 ) {
            considerRCInventory = true;
        }

        List<ProductModel> transactionProducts = new ArrayList<>();

        StoreKeypadModel storeKeypadModel = StoreKeypadModel.getStoreKeypadModel(getStoreModel());
        ArrayList<String> productsInStore = storeKeypadModel.getProductsInStoreWithDiningOptionsList();

        List<ProductModel> productModels = buildProductModels(transLines, terminalID);

        for(ProductModel productModel : productModels) {

            if ( productModel.getKeypadID() == null ) {
                Logger.logMessage("No keypad was found to determine where this product came from for Product: " + productModel.getName(), Logger.LEVEL.DEBUG);
            }

            String message = validateProduct(productModel.getID(), revenueCenterID, considerRCInventory, false, productsInStore, productModel.getDiningOptionProduct());

            //put name into the error about not being able to retrieve details, for better UX
            if ( message.length() > 0 && !message.contains(productModel.getName()) ) {
                message = "Could not find information for Product <b>" + productModel.getName() + "</b>, please select manually";
            }

            if(productModel.getPrepOption() != null && message.length() == 0) {
                message = validatePrepOption(productModel);
            }

            //make the new product model and set parameters
            ProductModel validProductModel = new ProductModel(productModel.getID(), getStoreID(), false);
            validProductModel.setQuantity(productModel.getQuantity());
            validProductModel.setKeypadID(productModel.getKeypadID());
            validProductModel.setName(productModel.getName());
            validProductModel.setDiningOptionProduct(productModel.getDiningOptionProduct());
            validProductModel.setModifiers(productModel.getModifiers());
            validProductModel.setPrepOption(productModel.getPrepOption());

            if (validProductModel.getPrepOption() != null && validProductModel.getPrice() != null) {
                BigDecimal newPrice = validProductModel.getPrice().add(validProductModel.getPrepOption().getPrice());
                validProductModel.setPrice(newPrice);
                validProductModel.setPrepOptionSetID(validProductModel.getPrepOption().getPrepOptionSetID());
            }

            validProductModel.setComboTransLineItemId(productModel.getComboTransLineItemId());
            validProductModel.setComboDetailId(productModel.getComboDetailId());
            validProductModel.setComboPrice(productModel.getComboPrice());
            validProductModel.setComboName(productModel.getComboName());
            validProductModel.setComboActive(productModel.getComboActive());
            validProductModel.setBasePrice(productModel.getBasePrice());
            validProductModel.setUpcharge(productModel.getUpcharge());

            //message will be set if there are any errors from validating the product
            if(!validProductModel.getDiningOptionProduct() || (validProductModel.getDiningOptionProduct() && isExpressReorder())) {
                validProductModel.setMessage(message);
            }

            // if there is a combo on this item and it's not active, error.
            /*if(validProductModel.getComboTransLineItemId() != null && !validProductModel.getComboActive()){
                validProductModel.setMessage("Combo <b>"+validProductModel.getComboName()+"</b> is not eligible for reorder at this time.");
                Logger.logMessage(validProductModel.getMessage());
            } else{
                validProductModel.setMessage("Combo <b>"+validProductModel.getComboName()+"</b> is not eligible for reorder at this time.");
                Logger.logMessage(validProductModel.getMessage());
            }*/

            if ( validProductModel.getMessage().equals("") ) {

                //modifiers
                if(validProductModel.getModifierSetID() == null && validProductModel.getModifiers() != null) {
                    validProductModel.setMessage("Product <b>"+validProductModel.getName()+"</b>'s options have changed, please select manually.");
                    Logger.logMessage("Attempting to reorder transaction: "+transactionID.toString()+", product with ID: " + validProductModel.getID().toString() + " has modifiers, but no modSet in CartModel.validateTransactionProducts", Logger.LEVEL.WARNING);

                } else if (validProductModel.getModifierSetID() != null && validProductModel.getModifiers() != null) {

                    List<String> modifierIDs = new ArrayList<>();
                    List<String> modifierKeypads = new ArrayList<>();

                    for(ModifierModel modifierModel : validProductModel.getModifiers()) {
                        modifierIDs.add(modifierModel.getID().toString());
                        if(modifierModel.getKeypadID() != null) {
                            modifierKeypads.add(modifierModel.getKeypadID().toString());
                        }
                    }

                    if ( modifierIDs.size() != modifierKeypads.size() ) {
                        Logger.logMessage("Attempting to reorder transaction: " + transactionID.toString() + ", could not validate number of keypads on modifiers for product: " + validProductModel.getID().toString() + " in CartModel.validateTransactionProducts due to missing saved keypadIDs", Logger.LEVEL.WARNING);
                    }

                    if ( validProductModel.getMessage().equals("") && !validateModifiersOnModSet(productModel.getModSetDetailID(), validProductModel.getModifiers(), modifierKeypads, modifierIDs) ) {
                        validProductModel.setMessage("Product <b>"+validProductModel.getName()+"</b>'s options have changed, please select manually.");
                        Logger.logMessage("Attempting to reorder transaction: " + transactionID.toString() + ", could not validate modifiers on mod set for product: " + validProductModel.getID().toString() + " in CartModel.validateTransactionProducts", Logger.LEVEL.WARNING);
                    }

                    if ( validProductModel.getMessage().equals("") ) {
                        for ( ModifierModel modifierModel : validProductModel.getModifiers() ) {

                            if ( modifierModel.getKeypadID() == null ) {
                                Logger.logMessage("Attempting to reorder transaction: " + transactionID.toString() + ", could not validate modifier: " + modifierModel.getID() + " for product: " + validProductModel.getID().toString() + " in CartModel.validateTransactionProducts due to missing saved keypadID", Logger.LEVEL.WARNING);
                            }

                            String modMessage = validateProduct(modifierModel.getID(), revenueCenterID, considerRCInventory, true, productsInStore, false);

                            if ( !modMessage.equals("") ) {
                                validProductModel.setMessage("Product <b>"+validProductModel.getName()+"</b>'s options have changed, please select manually.");
                                Logger.logMessage("Attempting to reorder transaction: " + transactionID.toString() + ", could not validate modifier: " + modifierModel.getID() + " for product: " + validProductModel.getID().toString() + " in CartModel.validateTransactionProducts", Logger.LEVEL.WARNING);
                                break;
                            }

                            ProductModel modProductModel = ProductModel.getProductModel(modifierModel.getID(), getStoreID());

                            modifierModel.setName(modProductModel.getName());
                            modifierModel.setPrice(modProductModel.getOriginalPrice());
                            if(modProductModel.getHasNutrition() == null){
                                modifierModel.setHasNutrition(false);
                            }
                            else{
                                modifierModel.setHasNutrition(modProductModel.getHasNutrition());
                            }

                        }

                        validProductModel.updateProductPriceBasedOnModifiers();
                    }
                }
            }

            transactionProducts.add(validProductModel);
        }

        if ( transactionProducts.size() == 0 ) {
            setMessage("No products were found to be eligible for reorder at this time, please select manually.");
        }

        setProducts(transactionProducts);
    }

    public List<ProductModel> buildProductModels(ArrayList<HashMap> transLines, Integer terminalID) throws Exception {
        List<ProductModel> products = new ArrayList<>();

        Integer activeTransLineItemID = null;
        Integer activeComboTransLineItemID = null;
        Integer comboTransLineItemID = 0;

        for ( HashMap transLine : transLines ) {
            Integer transLineItemID = CommonAPI.convertModelDetailToInteger(transLine.get("PATRANSLINEITEMID"));

            if(activeTransLineItemID == null || !activeTransLineItemID.equals(transLineItemID)) {
                activeTransLineItemID = transLineItemID;

                Integer productID = CommonAPI.convertModelDetailToInteger(transLine.get("PRODUCTID"));
                Integer keypadID = CommonAPI.convertModelDetailToInteger(transLine.get("KEYPADID"));
                Integer prepOptionID = CommonAPI.convertModelDetailToInteger(transLine.get("PAPREPOPTIONID"));
                Integer prepOptionSetID = CommonAPI.convertModelDetailToInteger(transLine.get("PAPREPOPTIONSETID"));
                Integer modifierSetID = CommonAPI.convertModelDetailToInteger(transLine.get("MODIFIERSETID"));
                Integer comboID = CommonAPI.convertModelDetailToInteger(transLine.get("COMBOID"));
                BigDecimal quantity = CommonAPI.convertModelDetailToBigDecimal(transLine.get("QUANTITY"), null);
                String productName = CommonAPI.convertModelDetailToString(transLine.get("NAME"));

                boolean isDiningOptionProduct = validateEatInTakeOutProduct(productID, terminalID);

                //check if the product is an eat in or take out product, if it is the product won't be added to the transactionModel
                if ( !isExpressReorder() && isDiningOptionProduct ) {
                    continue;
                }

                if ( keypadID == null ) {
                    Logger.logMessage("No keypad was found to determine where this product came from for Product: " + productName, Logger.LEVEL.DEBUG);
                }

                //make the new product model and set parameters
                ProductModel productModel = new ProductModel();
                productModel.setID(productID);
                productModel.setQuantity(quantity);
                productModel.setKeypadID(keypadID);
                productModel.setName(productName);
                productModel.setModSetDetailID(modifierSetID);

                productModel.setTransLineItemID(transLineItemID);

                PrepOptionModel prepOptionModel = new PrepOptionModel();

                if(prepOptionID != null && prepOptionSetID != null) {
                    prepOptionModel = new PrepOptionModel(prepOptionID, prepOptionSetID);
                    productModel.setPrepOption(prepOptionModel);
                } else if (prepOptionSetID != null) {
                    prepOptionModel.getDefaultPrepOptionModel(prepOptionSetID);
                    productModel.setPrepOption(prepOptionModel);
                }

                if(isDiningOptionProduct) {
                    productModel.setDiningOptionProduct(true);
                }

                setModifierModel(productModel, transLine);

                if(comboID != null) {

                    if(activeComboTransLineItemID == null || !activeComboTransLineItemID.equals(comboID)) {
                        activeComboTransLineItemID = comboID;
                        comboTransLineItemID--;
                    }

                    setCombo(productModel, transLine, comboTransLineItemID);
                }

                products.add(productModel);

            } else if (activeTransLineItemID .equals(transLineItemID)) {

                ProductModel productModel = null;

                for(ProductModel productModelExisting : products) {
                    if(productModelExisting.getTransLineItemID().equals(transLineItemID)) {
                        productModel = productModelExisting;
                        break;
                    }
                }

                if(productModel == null) {
                    continue;
                }

                setModifierModel(productModel, transLine);
            }
        }

        return products;
    }

    private void setModifierModel(ProductModel productModel, HashMap transLine) throws Exception{

        Integer modifierID = CommonAPI.convertModelDetailToInteger(transLine.get("MODID"));
        Integer modifierKeypadID = CommonAPI.convertModelDetailToInteger(transLine.get("MODKEYPADID"));

        Integer modifierPrepOptionID = CommonAPI.convertModelDetailToInteger(transLine.get("MODPREPOPTIONID"));
        Integer modifierPrepOptionSetID = CommonAPI.convertModelDetailToInteger(transLine.get("MODPREPOPTIONSETID"));

        if(modifierID != null) {
            ModifierModel modifierModel = new ModifierModel();

            modifierModel.setID(modifierID);
            modifierModel.setKeypadID(modifierKeypadID);


            if(modifierPrepOptionID != null) {
                PrepOptionModel prepOptionModel = new PrepOptionModel(modifierPrepOptionID, modifierPrepOptionSetID);
                modifierModel.setPrepOption(prepOptionModel);
            }

            List<ModifierModel> modifiers = productModel.getModifiers();
            if(modifiers == null) {
                modifiers = new ArrayList<>();
                modifiers.add(modifierModel);
                productModel.setModifiers(modifiers);
            } else if (!containsModifier(modifiers, modifierModel)) {
                modifiers.add(modifierModel);
                productModel.setModifiers(modifiers);
            }
        }
    }

    private boolean containsModifier(List<ModifierModel> modifiers, ModifierModel modifierModel) {
        if(modifiers.size() == 0) {
            return true;
        }

        for(ModifierModel modifier : modifiers) {
            if(modifier.getID().equals(modifierModel.getID()) && modifier.getKeypadID().equals(modifierModel.getKeypadID()) && containsPrep(modifier.getPrepOption(), modifierModel.getPrepOption())) {
               return true;
            }
        }

        return false;
    }

    private boolean containsPrep(PrepOptionModel modPrep, PrepOptionModel prepOptionModel) {
        return modPrep == null && prepOptionModel == null || modPrep != null && prepOptionModel != null && modPrep.getID().equals(prepOptionModel.getID()) && modPrep.getPrepOptionSetID().equals(prepOptionModel.getPrepOptionSetID()) && modPrep.getPrice().equals(prepOptionModel.getPrice()) && modPrep.getSortOrder().equals(prepOptionModel.getSortOrder()) && modPrep.getName().equals(prepOptionModel.getName());
    }

    private void setCombo(ProductModel productModel, HashMap transLine, Integer comboTransLineItemID) throws Exception{

        productModel.setComboTransLineItemId(comboTransLineItemID);
        productModel.setComboDetailId(CommonAPI.convertModelDetailToInteger(transLine.get("PACOMBODETAILID")));

        productModel.setComboPrice(CommonAPI.convertModelDetailToBigDecimal(transLine.get("COMBOPRICE")));

        productModel.setComboActive(CommonAPI.convertModelDetailToBoolean(transLine.get("COMBOACTIVE")));
        productModel.setComboName(CommonAPI.convertModelDetailToString(transLine.get("COMBONAME")));

        BigDecimal basePrice = CommonAPI.convertModelDetailToBigDecimal(transLine.get("BASEPRICE"));
        BigDecimal productPrice = CommonAPI.convertModelDetailToBigDecimal(transLine.get("AMOUNT"));

        BigDecimal upcharge = basePrice.subtract(productPrice);

        productModel.setBasePrice(basePrice);
        productModel.setUpcharge(upcharge);

    }

    //returns blank string if product is valid, based on active, disabled, mapped, etc criteria
    private String validateProduct(Integer productID, Integer revenueCenterID, boolean considerRCInventory, boolean modifier, ArrayList<String> productsInStore, boolean isDiningOptionProduct) throws Exception {
        String message = "";
        boolean productError = false;

        HashMap productDetails = getProductValidationDetails(productID, revenueCenterID);

        if ( productDetails.containsKey("message") ) {
            message = productDetails.get("message").toString();
        } else {
            boolean isActive = CommonAPI.convertModelDetailToBoolean(productDetails.get("ACTIVE"));
            boolean isInvProduct = CommonAPI.convertModelDetailToBoolean(productDetails.get("INVENTORYITEM"));
            boolean isModifier = CommonAPI.convertModelDetailToBoolean(productDetails.get("ISMODIFIER"));
            boolean productDisabled = CommonAPI.convertModelDetailToBoolean(productDetails.get("ITEMDISABLED"));
            boolean priceOpen = CommonAPI.convertModelDetailToBoolean(productDetails.get("PRICEOPEN"));
            boolean pricePreset = CommonAPI.convertModelDetailToBoolean(productDetails.get("PRICEPRESET"));
            boolean onlineOrderingAvail = CommonAPI.convertModelDetailToBoolean(productDetails.get("ONLINEORDERINGAVAIL"));
            Integer mappedID = CommonAPI.convertModelDetailToInteger(productDetails.get("MAPPEDID"));
            BigDecimal invCurrent = CommonAPI.convertModelDetailToBigDecimal(productDetails.get("INVCURRENT"));
            String name = CommonAPI.convertModelDetailToString(productDetails.get("NAME"));
            boolean foundInStore = true;

            if ( !modifier ) { //if the product is not a modifier, loop through all the products in the store to check if it is in an active keypad
                foundInStore = false;
                for(String productInStore: productsInStore) {
                    if(productID.toString().equals(productInStore)) {
                        foundInStore = true;
                        break;
                    }
                }
            }

            if ( !isActive ) {
                productError = true;
                Logger.logMessage("Product <b>"+name+" is not eligible for reorder due to being inactive.", Logger.LEVEL.DEBUG);
            }

            if ( productDisabled ) {
                productError = true;
                Logger.logMessage("Product <b>"+name+"</b> is not eligible for reorder due to being disabled.", Logger.LEVEL.DEBUG);
            }

            if (priceOpen && !pricePreset) {
                productError = true;
                Logger.logMessage("Product <b>"+name+"</b> is not eligible for reorder due to price open and preset settings.", Logger.LEVEL.DEBUG);
            }

            if ( mappedID == null || mappedID <= 0 ) {
                productError = true;
                Logger.logMessage("Product <b>"+name+"</b> is not eligible for reorder due to not being mapped.", Logger.LEVEL.DEBUG);
            }

            if ( modifier && !isModifier ) {
                productError = true;
                Logger.logMessage("Product <b>"+name+"</b> is not eligible for reorder due to bad isModifier setting.", Logger.LEVEL.DEBUG);
            }

            if ( !getStoreModel().getIgnoreInventoryCounts() && considerRCInventory && isInvProduct && ( invCurrent == null || invCurrent.compareTo(BigDecimal.ZERO) <= 0 ) ) {
                productError = true;
                Logger.logMessage("Product <b>"+name+"</b> is not eligible for reorder due to lack of inventory.", Logger.LEVEL.DEBUG);
            }

            if ( !onlineOrderingAvail ) {
                productError = true;
                Logger.logMessage("Product <b>"+name+"</b> is not eligible for reorder because it has been marked unavailable for online ordering.", Logger.LEVEL.DEBUG);
            }

            if ( !foundInStore && !isDiningOptionProduct ) {
                productError = true;
                Logger.logMessage("Product <b>"+name+"</b> is not eligible for reorder because it is currently not available on a menu for online ordering.", Logger.LEVEL.DEBUG);
            }

            if ( productError ) {
                message = "Product <b>"+name+"</b> is not eligible for reorder at this time.";
            }
        }
        return message;
    }

    //retrieves all relevant info for the product to be validated, based on productID and rev center
    private HashMap getProductValidationDetails(Integer productID, Integer revenueCenterID) throws Exception {
        HashMap productDetailHM = new HashMap();

        ArrayList<HashMap> productDetails = dm.parameterizedExecuteQuery("data.ordering.getProductValidForOrderDetails",
                new Object[]{
                        productID,
                        revenueCenterID
                },
                true
        );

        if ( productDetails == null || productDetails.size() == 0 ) {
            productDetailHM.put("message", "Could not retrieve product details");
        } else {
            productDetailHM = productDetails.get(0);
        }

        return productDetailHM;
    }

    //check if the product is the Eat In or Take Out Product for this terminal
    private boolean validateEatInTakeOutProduct(Integer productID, Integer terminalID) {
        ArrayList<HashMap> eatInTakeOutProductDetails = dm.parameterizedExecuteQuery("data.ordering.getEatInTakeOutProductFromTerminal",
                new Object[]{
                        productID,
                        terminalID
                },
                true
        );

        if ( eatInTakeOutProductDetails != null && eatInTakeOutProductDetails.size() > 0 ) {
            return true;
        }

        return false;
    }

    //returns true or false depending on validity of all modifiers in a given modifier set
    private boolean validateModifiersOnModSet(Integer modifierSetID, List<ModifierModel> modifiers, List<String> modifierKeypads, List<String> modIDs) throws Exception {
        ArrayList<HashMap> modSetDetails = dm.parameterizedExecuteQuery("data.ordering.getModSetDetails",
                new Object[] {
                        modifierSetID
                },
                true
        );

        if ( modSetDetails == null || modSetDetails.size() == 0 ) {
            Logger.logMessage("No Mod Set Details found for Mod Set ID: " + modifierSetID, Logger.LEVEL.DEBUG);
            return false;
        }

        ArrayList<String> foundModifiers = new ArrayList<>();
        ArrayList<String> foundKeypads = new ArrayList<>();

        //go through each mod set detail and figure out which modifiers are from each keypad and compare to min/max
        for ( HashMap modSetDetail : modSetDetails ) {
            Integer keypadID = CommonAPI.convertModelDetailToInteger(modSetDetail.get("PAKEYPADID"));

            //if this keypad was not included in the last order, it has been added to the mod set since, we will allow this
            if ( !modifierKeypads.contains( keypadID.toString() ) ) {
                modifierKeypads.remove( keypadID.toString() );
                Logger.logMessage( "Reorder - keypad: " + keypadID + " was not used last time for this product on mod set: " + modifierSetID, Logger.LEVEL.DEBUG );
            }

            //get how many modifiers on the product are from this particular keypad
            HashMap keypadCountHM = countNumProductsFromKeypad(keypadID, modifiers);

            //if any error in counting, message will be set
            if ( keypadCountHM == null || !keypadCountHM.get("message").equals("") ) {
                Logger.logMessage( "Reorder Failed - Could not count valid modifiers on keypad: " + keypadID, Logger.LEVEL.DEBUG );

                if ( keypadCountHM != null && keypadCountHM.containsKey("message") ) {
                    Logger.logMessage( "Reorder - Message is: " + keypadCountHM.get("message").toString(), Logger.LEVEL.DEBUG );
                }

                return false;
            }

            foundKeypads.add( keypadID.toString() );
            Integer keypadCount = CommonAPI.convertModelDetailToInteger(keypadCountHM.get("count"));

            Integer min = CommonAPI.convertModelDetailToInteger(modSetDetail.get("MINQUANTITY"), null, true);
            Integer max = CommonAPI.convertModelDetailToInteger(modSetDetail.get("MAXQUANTITY"), null, true);

            //max quantity is not required - set to over 9000 if null
            if ( max == null ) {
                max = 9999;
            }

            //any error related to min or max, fail product
            if ( keypadCount < min || keypadCount > max ) {
                Logger.logMessage( "Reorder Failed - Modifiers on keypad count is: " + keypadCount + " for keypad:" + keypadID + ", which is below the minimum of: " + min + ", or above the maximum of: " + max, Logger.LEVEL.DEBUG );
                return false;
            }

            ArrayList<String> detailMods = (ArrayList<String>) keypadCountHM.get("foundModifiers");

            //any modifiers found are added to the total modifier list for the product, to ensure we find them all
            for ( String mod : detailMods ) {
                Logger.logMessage( "Reorder - Adding modifier: " + mod + " to foundModifiers list", Logger.LEVEL.LUDICROUS );
                foundModifiers.add(mod);
            }
        }

        for ( String modifier : modIDs ) {
            if ( !foundModifiers.contains(modifier) ) {
                Logger.logMessage( "Reorder Failed - foundModifiers does not contain modifier: " + modifier, Logger.LEVEL.DEBUG );
                return false;
            }
        }

        return true;
    }

    private HashMap countNumProductsFromKeypad(Integer keypadID, List<ModifierModel> modifiers) throws Exception {
        HashMap returnHM = new HashMap();
        Integer count = 0;
        String message = "";

        ArrayList<HashMap> keypadDetails = dm.parameterizedExecuteQuery("data.ordering.getKeypadDetails",
                new Object[] {
                        keypadID
                },
                true
        );

        if ( keypadDetails == null && keypadDetails.size() == 0 ) {
            returnHM.put("message", "no keypad details found");
            return returnHM;
        }

        List keypadItems = new ArrayList<Integer>();
        List keypadItemNames = new ArrayList<String>();
        for ( HashMap keyPadDetail : keypadDetails ) {
            Integer itemID = CommonAPI.convertModelDetailToInteger(keyPadDetail.get("PAKEYPADITEMID"));
            String itemName = CommonAPI.convertModelDetailToString(keyPadDetail.get("NAME"));
            Logger.logMessage( "Reorder - found keypad detail: " + itemID + " (" + itemName + ")", Logger.LEVEL.LUDICROUS );

            keypadItems.add(itemID);
            keypadItemNames.add(itemName);
        }

        ArrayList<String> foundModifiers = new ArrayList<>();
        ArrayList<String> foundNoneModifiers = new ArrayList<>();

        for ( ModifierModel modifier : modifiers ) {
            Integer modifierID = modifier.getID();

            Logger.logMessage("Reorder - searching keypad: " + keypadID + " for modifier: " + modifierID, Logger.LEVEL.DEBUG);
            if ( (keypadItems.contains( modifierID ) && keypadID.equals(modifier.getKeypadID())) || (keypadItems.contains( modifierID ) && modifier.getKeypadID() == null) ) {
                int index = keypadItems.indexOf( modifierID );
                Logger.logMessage("Reorder - found modifier: " + modifierID + " (" + keypadItemNames.get(index) + ") for keypad: " + keypadID, Logger.LEVEL.DEBUG);
                if ( keypadItemNames.get(index).equals("*-----") ) {
                    Logger.logMessage("Reorder - modifier is a none modifier, not counting it in keypad item search", Logger.LEVEL.DEBUG);
                    foundNoneModifiers.add( modifierID.toString() );
                    continue;
                }

                count++;
                foundModifiers.add(modifierID.toString());
            }
        }

        //if no non-none products are found, then add the none products
        if ( count == 0 && foundNoneModifiers.size() > 0 ) {
            Logger.logMessage("Reorder - no non-NONE products were found for keypad: " + keypadID + ", but found at least 1 NONE modifier, setting count to 1", Logger.LEVEL.DEBUG);
            count = 1;
            foundModifiers.add(foundNoneModifiers.get(0));
        }

        returnHM.put("message", message);
        returnHM.put("count", count);
        returnHM.put("foundModifiers", foundModifiers);

        return returnHM;
    }

    public String validatePrepOption(ProductModel productModel) {
        String message = "";
        boolean prepOptionError = false;

        PrepOptionModel prepOptionModel = productModel.getPrepOption();

        if ( prepOptionModel.getID() == null ) {
            Logger.logMessage("Product <b>"+productModel.getName()+" is not eligible for reorder due to the prep option not being in the prep option set ID: "+ prepOptionModel.getPrepOptionSetID() +" anymore.", Logger.LEVEL.DEBUG);
            return  "Product <b>"+productModel.getName()+"</b> is not eligible for reorder at this time.";
        }

        ArrayList<HashMap> prepDetails = dm.parameterizedExecuteQuery("data.ordering.getPrepOptionDetails",
                new Object[] {
                        prepOptionModel.getID(),
                        prepOptionModel.getPrepOptionSetID()
                },
                true
        );

        boolean isPrepActive = false;
        boolean isPrepSetActive = false;

        for(HashMap prepDetailsHM : prepDetails) {
            boolean prepStatus = CommonAPI.convertModelDetailToBoolean(prepDetailsHM.get("PREPSTATUS"), false);
            boolean prepSetStatus = CommonAPI.convertModelDetailToBoolean(prepDetailsHM.get("PREPSETSTATUS"), false);

            if(prepStatus) {
                isPrepActive = true;
            }

            if(prepSetStatus) {
                isPrepSetActive = true;
            }
        }

        String name = prepOptionModel.getName();

        if ( !isPrepActive ) {
            prepOptionError = true;
            Logger.logMessage("Prep Option <b>"+name+" is not eligible for reorder due to being inactive.", Logger.LEVEL.DEBUG);
        }

        if ( !isPrepSetActive ) {
            prepOptionError = true;
            Logger.logMessage("Prep Option <b>"+name+" in Prep Option Set ID: "+prepOptionModel.getPrepOptionSetID()+" is not eligible for reorder due to the set being inactive.", Logger.LEVEL.DEBUG);
        }

        if ( prepOptionError ) {
            message = "Prep Option <b>"+name+"</b> is not eligible for reorder at this time.";
        }

        return message;
    }

    //END Reordering Functionality

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public List<ProductModel> getProducts() {
        return products;
    }

    public void setProducts(List<ProductModel> products) {
        this.products = products;
    }

    public StoreModel getStoreModel() {
        return storeModel;
    }

    public void setStoreModel(StoreModel storeModel) {
        this.storeModel = storeModel;
    }

    public Integer getStoreID() {
        return storeID;
    }

    public void setStoreID(Integer storeID) {
        this.storeID = storeID;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getAuthenticatedAccountID() {
        return authenticatedAccountID;
    }

    public void setAuthenticatedAccountID(Integer authenticatedAccountID) {
        this.authenticatedAccountID = authenticatedAccountID;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean isUseDiningOptions() {
        return useDiningOptions;
    }

    public void setUseDiningOptions(boolean useDiningOptions) {
        this.useDiningOptions = useDiningOptions;
    }

    public Integer getTenderTypeID() {
        return tenderTypeID;
    }

    public void setTenderTypeID(Integer tenderTypeID) {
        this.tenderTypeID = tenderTypeID;
    }

    public boolean isExpressReorder() {
        return expressReorder;
    }

    public void setExpressReorder(boolean expressReorder) {
        this.expressReorder = expressReorder;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

}
