package com.mmhayes.myqc.server.models;

//mmhayes dependencies

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;
import com.mmhayes.myqc.server.collections.PrepOptionCollection;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

//myqc dependencies
//other dependencies

/* Modifier Model
 Last Updated (automatically updated by SVN)
 $Author: skaugustine $: Author of last commit
 $Date: 2021-02-02 08:16:55 -0500 (Tue, 02 Feb 2021) $: Date of last commit
 $Rev: 54052 $: Revision of last commit

 Notes: Model for "Modifier Products" - Products (a.k.a PLUs, Items, etc.) that have the IsModifier flag turned on
*/
public class ModifierModel {
    Integer id = null; //ID of the "modifier product" (PAPluID on QC_PAPlu)
    Integer keypadID = null;
    Integer prepOptionSetID = null;

    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String name = "N/A"; //Name of the "modifier product"
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String description = "N/A"; //Description of the "modifier product"
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String taxIDs = "N/A"; //Comma separated list of Tax IDs for purchase restrictions

    BigDecimal price = null; //Price of the "modifier product"
    BigDecimal nutritionInfo1 = null; //Product nutrition info 1
    BigDecimal nutritionInfo2 = null; //Product nutrition info 2
    BigDecimal nutritionInfo3 = null; //Product nutrition info 3
    BigDecimal nutritionInfo4 = null; //Product nutrition info 4
    BigDecimal nutritionInfo5 = null; //Product nutrition info 5

    Boolean hasNutrition = false;
    Boolean vegan = false;
    Boolean vegetarian = false;
    Boolean glutenFree = false;
    Boolean healthy = false;
    Boolean defaultModifier = false;
    Boolean hasPrinterMappings = true;
    ArrayList<HashMap> nutritionInfo = new ArrayList<>();
    List<PrepOptionModel> prepOptions = new ArrayList<>();  //prep options that could be selected for modifier
    PrepOptionModel prepOption = null; //the one selected prep option
    PrepOptionModel defaultPrepOption = null; //the one selected prep option

    //dummy constructor - needed to consume ANY JSON object via REST (Jersey/Jackson style)
    public ModifierModel() {

    }

    //constructor - takes hashmap that get sets to this models properties
    public ModifierModel(HashMap modelDetailHM) {
        try {
            setModelProperties(modelDetailHM);
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
    }

    //setter for all of this model's properties
    public ModifierModel setModelProperties(HashMap modelDetailHM) {
        try {

            setID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("MODIFIERID")));

            setName(CommonAPI.convertModelDetailToString(modelDetailHM.get("MODNAME")));

            setPrice(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("PRICE")));

            setDescription(CommonAPI.convertModelDetailToString(modelDetailHM.get("MODDESC")));

            setTaxIDs(CommonAPI.convertModelDetailToString(modelDetailHM.get("TAXIDS")));

            setKeypadID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("KEYPADID")));

            setNutritionInfo1(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("NUTRITIONINFO1")));

            setNutritionInfo2(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("NUTRITIONINFO2")));

            setNutritionInfo3(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("NUTRITIONINFO3")));

            setNutritionInfo4(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("NUTRITIONINFO4")));

            setNutritionInfo5(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("NUTRITIONINFO5")));

            setGlutenFree(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("GLUTENFREE")));

            setVegan(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("VEGAN")));

            setVegetarian(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("VEGETARIAN")));

            setHealthy(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("WELLNESS")));

            setDefaultModifier(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("DEFAULTMODIFIER")));

            setHasPrinterMappings(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("HASPRINTERMAPPINGS"), true));

            setPrepOptionSetID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PREPOPTIONSETID")));

        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
        return this;
    }

    public static ModifierModel createModifierModel(HashMap modifierHM) throws Exception {
        HashMap modifier = new HashMap();

        modifier.put("ID", modifierHM.get("id") );
        modifier.put("NAME", modifierHM.get("name") );
        modifier.put("PRICE", modifierHM.get("price") );
        modifier.put("MODINDEX", modifierHM.get("modIndex") );
        modifier.put("HASNUTRITION", modifierHM.get("hasNutrition") );
        modifier.put("KEYPADID", modifierHM.get("keypadID") );
        modifier.put("TAXIDS", modifierHM.get("taskIDs") );

        return new ModifierModel(modifier);
    }

    //getter
    public Integer getID() {
        return this.id;
    }

    //setter
    public void setID(Integer id) {
        this.id = id;
    }

    //getter
    public String getName() {
        return name;
    }

    //setter
    public void setName(String name) {
        this.name = name.replace("\\r", " ");
    }

    //getter
    public BigDecimal getPrice() {
        return price;
    }

    //setter
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Boolean getHasNutrition() {
        return hasNutrition;
    }

    public void setHasNutrition(Boolean hasNutrition) {
        this.hasNutrition = hasNutrition;
    }

    public Integer getKeypadID() {
        return keypadID;
    }

    public void setKeypadID(Integer keypadID) {
        this.keypadID = keypadID;
    }

    public Boolean getVegan() {
        return vegan;
    }

    public void setVegan(Boolean vegan) {
        this.vegan = vegan;
    }

    public Boolean getVegetarian() {
        return vegetarian;
    }

    public void setVegetarian(Boolean vegetarian) {
        this.vegetarian = vegetarian;
    }

    public Boolean getGlutenFree() {
        return glutenFree;
    }

    public void setGlutenFree(Boolean glutenFree) {
        this.glutenFree = glutenFree;
    }

    public Boolean getHealthy() {
        return healthy;
    }

    public void setHealthy(Boolean healthy) {
        this.healthy = healthy;
    }

    public List<PrepOptionModel> getPrepOptions() {
        return prepOptions;
    }

    public void setPrepOptions(List<PrepOptionModel> prepOptions) {
        this.prepOptions = prepOptions;
    }

    public Integer getPrepOptionSetID() {
        return prepOptionSetID;
    }

    public void setPrepOptionSetID(Integer prepOptionSetID) {
        this.prepOptionSetID = prepOptionSetID;
    }

    public Boolean getDefaultModifier() {
        return defaultModifier;
    }

    public void setDefaultModifier(Boolean defaultModifier) {
        this.defaultModifier = defaultModifier;
    }

    public PrepOptionModel getPrepOption() {
        return prepOption;
    }

    public void setPrepOption(PrepOptionModel prepOption) {
        this.prepOption = prepOption;
    }

    public PrepOptionModel getDefaultPrepOption() {
        return defaultPrepOption;
    }

    public void setDefaultPrepOption(PrepOptionModel defaultPrepOption) {
        this.defaultPrepOption = defaultPrepOption;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getHasPrinterMappings() {
        return hasPrinterMappings;
    }

    public void setHasPrinterMappings(Boolean hasPrinterMappings) {
        this.hasPrinterMappings = hasPrinterMappings;
    }

    public BigDecimal getNutritionInfo1() {
        return nutritionInfo1;
    }

    public void setNutritionInfo1(BigDecimal nutritionInfo1) {
        this.nutritionInfo1 = nutritionInfo1;
    }

    public BigDecimal getNutritionInfo2() {
        return nutritionInfo2;
    }

    public void setNutritionInfo2(BigDecimal nutritionInfo2) {
        this.nutritionInfo2 = nutritionInfo2;
    }

    public BigDecimal getNutritionInfo3() {
        return nutritionInfo3;
    }

    public void setNutritionInfo3(BigDecimal nutritionInfo3) {
        this.nutritionInfo3 = nutritionInfo3;
    }

    public BigDecimal getNutritionInfo4() {
        return nutritionInfo4;
    }

    public void setNutritionInfo4(BigDecimal nutritionInfo4) {
        this.nutritionInfo4 = nutritionInfo4;
    }

    public BigDecimal getNutritionInfo5() {
        return nutritionInfo5;
    }

    public void setNutritionInfo5(BigDecimal nutritionInfo5) {
        this.nutritionInfo5 = nutritionInfo5;
    }

    //taxIDs getter/setter
    public String getTaxIDs() {
        return taxIDs;
    }
    public void setTaxIDs(String taxes) {
        this.taxIDs = taxes.replace("\\r", " ");
    }
    
    /**
     * Overridden toString() method for a ModifierModel.
     * @return a {@link String} representation of a ModifierModel.
     */
    @Override
    public String toString () {

        String prepOptsStr = "";
        if (!DataFunctions.isEmptyCollection(prepOptions)) {
            prepOptsStr += "[";
            for (PrepOptionModel prepOption : prepOptions) {
                prepOptsStr += "[";
                if (prepOption.equals(prepOptions.get(prepOptions.size() - 1))) {
                    prepOptsStr += prepOption.toString() + "; ";
                }
                else {
                    prepOptsStr += prepOption.toString() + "]";
                }
            }
            prepOptsStr += "]";
        }

        return String.format("ID: %s, KEYPADID: %s, PREPOPTIONSETID: %s, NAME: %s, DESCRIPTION: %s, PRICE: %s, NUTRITIONINFO1: %s, " +
                "NUTRITIONINFO2: %s, NUTRITIONINFO3: %s, NUTRITIONINFO4: %s, NUTRITIONINFO5: %s, HASNUTRITION: %s, VEGAN: %s, " +
                "VEGETARIAN: %s, GLUTENFREE: %s, HEALTHY: %s, DEFAULTMODIFIER: %s, HASPRINTERMAPPINGS: %s, PREPOPTIONS: %s, " +
                "PREPOPTION: %s, DEFAULTPREPOPTION: %s, TAXIDS: %s",
                Objects.toString((id != null && id > 0 ? id : "N/A"), "N/A"),
                Objects.toString((keypadID != null && keypadID > 0 ? keypadID : "N/A"), "N/A"),
                Objects.toString((prepOptionSetID != null && prepOptionSetID > 0 ? prepOptionSetID : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(name) ? name : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(description) ? description : "N/A"), "N/A"),
                Objects.toString((price != null ? price.toPlainString() : "N/A"), "N/A"),
                Objects.toString((nutritionInfo1 != null ? nutritionInfo1.toPlainString() : "N/A"), "N/A"),
                Objects.toString((nutritionInfo2 != null ? nutritionInfo2.toPlainString() : "N/A"), "N/A"),
                Objects.toString((nutritionInfo3 != null ? nutritionInfo3.toPlainString() : "N/A"), "N/A"),
                Objects.toString((nutritionInfo4 != null ? nutritionInfo4.toPlainString() : "N/A"), "N/A"),
                Objects.toString((nutritionInfo5 != null ? nutritionInfo5.toPlainString() : "N/A"), "N/A"),
                Objects.toString(hasNutrition, "N/A"),
                Objects.toString(vegan, "N/A"),
                Objects.toString(vegetarian, "N/A"),
                Objects.toString(glutenFree, "N/A"),
                Objects.toString(healthy, "N/A"),
                Objects.toString(defaultModifier, "N/A"),
                Objects.toString(hasPrinterMappings, "N/A"),
                Objects.toString((StringFunctions.stringHasContent(prepOptsStr) ? prepOptsStr : "N/A"), "N/A"),
                Objects.toString((prepOption != null ? prepOption : "N/A"), "N/A"),
                Objects.toString((defaultPrepOption != null ? defaultPrepOption : "N/A"), "N/A"),
                Objects.toString((taxIDs != null ? taxIDs : "N/A"), "N/A")
        );
    }

}
