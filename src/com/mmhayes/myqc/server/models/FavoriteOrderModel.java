package com.mmhayes.myqc.server.models;

//mmhayes dependencies

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.CommonAuthResource;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.combo.models.*;
import com.mmhayes.common.combo.models.ComboDetailModel;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.commonMMHFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.MMHResultSet;
import com.mmhayes.myqc.server.api.MyQCCache;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.*;

//javax.ws.rs dependencies
//myqc dependencies
//other dependencies

/* Product Model
 Last Updated (automatically updated by SVN)
 $Author: zdhirschman $: Author of last commit
 $Date: 2020-11-05 17:30:07 -0500 (Thu, 05 Nov 2020) $: Date of last commit
 $Rev: 52418 $: Revision of last commit

 Notes: Model for favorite orders (a.k.a PLUs, Items, etc.)
*/
public class FavoriteOrderModel {
    Integer id = null; //ID
    private Integer productCount = null; // number of products in the order, not including modifiers
    private Integer authenticatedAccountID = null;
    private Long employeeId = null;
    private Integer transactionId = null;
    private Long terminalId = null;
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    private String name = ""; //Name of the order
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String primary = ""; //used to differentiate between favorite products, popular and recent in favorite-line-view
    private boolean active = false;
    private boolean isAvailable = false; //keep track if the favorite order is in the available or unavailable list
    private Timestamp createdDTM = null;
    private Timestamp inactivatedDTM = null;
    List<ProductModel> items = new ArrayList<>();

    private static DataManager dm = new DataManager();
    private commonMMHFunctions commFunc = new commonMMHFunctions();

    //dummy constructor - needed to consume ANY JSON object via REST (Jersey/Jackson style)
    public FavoriteOrderModel() {

    }

    public FavoriteOrderModel(HttpServletRequest request) throws Exception {
       setAuthenticatedAccountID(CommonAuthResource.checkRequestHeaderForMMHayesAuth(request));
    }

    public FavoriteOrderModel(Integer favoriteOrderId) {
        getFavoriteOrderDetails(favoriteOrderId);
    }

    //constructor - takes hashmap that get sets to this models properties
    public FavoriteOrderModel(Integer favoriteOrderID, HttpServletRequest request) throws Exception {
        getFavoriteOrderItems(favoriteOrderID, request);
    }

    //constructor - takes hashmap that get sets to this models properties
    public FavoriteOrderModel(HashMap modelDetailHM, boolean isMyQuickPicks) throws Exception {
        setModelPropertiesMyQuickPicks(modelDetailHM);
    }

    //constructor - takes hashmap that get sets to this models properties
    public FavoriteOrderModel(HashMap modelDetailHM) throws Exception {
        setModelProperties(modelDetailHM);
    }

    public FavoriteOrderModel setModelProperties(HashMap modelDetailHM) {
        this.transactionId = CommonAPI.convertModelDetailToInteger(modelDetailHM.get("transactionId"));
        this.name = CommonAPI.convertModelDetailToString(modelDetailHM.get("name"));
        this.active = true;

        return this;
    }

    //setter for all of the favorite order model's for the My Quick Picks page
    public FavoriteOrderModel setModelPropertiesMyQuickPicks(HashMap modelDetailHM) throws Exception{
        setID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));
        setName(CommonAPI.convertModelDetailToString(modelDetailHM.get("NAME")));
        setActive(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("ACTIVE")));
        setPrimary(CommonAPI.convertModelDetailToString(modelDetailHM.get("PRIMARY")));
        setAvailable(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("AVAILABLE")));
        setProductCount(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PRODUCTCOUNT")));
        return this;
    }

    public HashMap saveFavoriteBeforeTransaction(HashMap body, HttpServletRequest request) throws Exception {

        this.createdDTM = new Timestamp(new Date().getTime());
        this.authenticatedAccountID = CommonAuthResource.checkRequestHeaderForMMHayesAuth(request);
        Integer storeID = CommonAPI.checkRequestHeaderForStoreID(request);
        Integer terminalId = CommonAPI.getUserTerminalID(storeID);
        String name = CommonAPI.convertModelDetailToString(body.get("name"));
        ArrayList<HashMap> products = (ArrayList<HashMap>) body.get("products");

        int favoriteId = dm.parameterizedExecuteNonQuery("data.favorites.createNewFavoriteOrder", new Object[]
                {name, this.authenticatedAccountID, terminalId, this.createdDTM});

        Integer orderItemId;
        for(HashMap product: products) {
            Integer productId = CommonAPI.convertModelDetailToInteger(product.get("productID"));
            BigDecimal quantity = CommonAPI.convertModelDetailToBigDecimal(product.get("quantity"));
            Integer keypadID = CommonAPI.convertModelDetailToInteger(product.get("keypadID"));
            Integer prepOptionID = null;
            Integer prepOptionSetID = null;
            Integer comboID = null;

            HashMap prepOption = (HashMap) product.get("prepOption");
            if(prepOption != null && prepOption.get("id") != null) {
                prepOptionID = Integer.parseInt(prepOption.get("id").toString());
                prepOptionSetID = Integer.parseInt(prepOption.get("prepOptionSetID").toString());
            }

            HashMap comboModel = (HashMap) product.get("comboModel");
            if(comboModel != null && comboModel.get("comboId") != null) {
                comboID = Integer.parseInt(comboModel.get("comboId").toString());

            } else if ( CommonAPI.convertModelDetailToInteger(product.get("pacomboID")) != null ) {
                comboID = CommonAPI.convertModelDetailToInteger(product.get("pacomboID"));
            }

            // Create new FavoriteOrderLineItem
            orderItemId = dm.parameterizedExecuteNonQuery("data.favorites.createNewFavoriteOrderLineItem",
                    new Object[]{productId, favoriteId, quantity, prepOptionID, prepOptionSetID, keypadID, comboID});

            ArrayList<HashMap> modifiers = (ArrayList<HashMap>) product.get("modifiers");

            if(modifiers != null) {
                for(HashMap mod: modifiers) {
                    // Create new QC_FavoriteOrderLineIemMod
                    Integer modId = CommonAPI.convertModelDetailToInteger(mod.get("id"));
                    Integer modKeypadID = CommonAPI.convertModelDetailToInteger(mod.get("keypadID"));
                    Integer modPrepOptionID = null;
                    Integer modPepOptionSetID = null;

                    HashMap modPrepOption = (HashMap) mod.get("prepOption");
                    if(modPrepOption != null && modPrepOption.get("id") != null) {
                        modPrepOptionID = Integer.parseInt(modPrepOption.get("id").toString());
                        modPepOptionSetID = Integer.parseInt(modPrepOption.get("prepOptionSetID").toString());
                    }

                    dm.parameterizedExecuteNonQuery("data.favorites.createNewFavoriteOrderLineItemMod", new Object[]{orderItemId, modId, modPrepOptionID, modPepOptionSetID, modKeypadID});
                }
            }
        }

        HashMap jsonValue = new HashMap();
        jsonValue.put("favoriteOrderId", favoriteId);

        if(body.containsKey("expressOrderID") && body.get("expressOrderID") != null) {
            Integer expressOrderID = CommonAPI.convertModelDetailToInteger(body.get("expressOrderID"));
            MyQCCache.createExpressFavoriteOrderInCache(this.authenticatedAccountID, expressOrderID, name, favoriteId);
        }

        return jsonValue;
    }

    public HashMap saveRecords(HttpServletRequest request) throws Exception {
        HashMap jsonValue = new HashMap();

        ArrayList<HashMap> transactions = dm.parameterizedExecuteQuery(
                "data.favorites.getTransactionByTransactionId", new Object[]{this.transactionId}, true);

        if(transactions != null && transactions.size() > 0) {
            HashMap txn = transactions.get(0);

            this.terminalId = (Long) txn.get("TERMINALID");
            this.createdDTM = new Timestamp(new Date().getTime());
            this.authenticatedAccountID = CommonAuthResource.checkRequestHeaderForMMHayesAuth(request);

            //creates record in QC_FavoriteOrder
            int favoriteId = dm.parameterizedExecuteNonQuery("data.favorites.createNewFavoriteOrder", new Object[]
                    {name, this.authenticatedAccountID, this.terminalId, this.createdDTM});

            Integer orderItemId = null;

            for(HashMap transaction: transactions) {
                int isModifier = (int) transaction.get("ISMODIFIER");
                long productId = (long) transaction.get("PAITEMID");
                BigDecimal quantity = (BigDecimal) transaction.get("QUANTITY");
                Integer keypadID = CommonAPI.convertModelDetailToInteger(transaction.get("PAKEYPADID"));
                Integer prepOptionID = null;
                Integer prepOptionSetID = null;
                Integer comboID = CommonAPI.convertModelDetailToInteger(transaction.get("PACOMBOID"));

                if( !transaction.get("PAPREPOPTIONID").toString().equals("") && !transaction.get("PAPREPOPTIONSETID").toString().equals("") ) {
                    prepOptionID = CommonAPI.convertModelDetailToInteger(transaction.get("PAPREPOPTIONID"));
                    prepOptionSetID = CommonAPI.convertModelDetailToInteger(transaction.get("PAPREPOPTIONSETID"));
                }

                if(isModifier != 1) {
                    // Create new QC_FavoriteOrderLineItem
                    orderItemId = dm.parameterizedExecuteNonQuery("data.favorites.createNewFavoriteOrderLineItem",
                            new Object[]{productId, favoriteId, quantity, prepOptionID, prepOptionSetID, keypadID, comboID});
                } else {
                    // Create new QC_FavoriteOrderLineIemMod
                    this.id = dm.parameterizedExecuteNonQuery("data.favorites.createNewFavoriteOrderLineItemMod", new Object[]{orderItemId, productId, prepOptionID, prepOptionSetID, keypadID});
                }
            }

            jsonValue.put("favoriteOrderId", favoriteId);

            MyQCCache.createExpressFavoriteOrderInCache(this.authenticatedAccountID, this.transactionId, name, favoriteId);
        }

        return jsonValue;
    }

    private FavoriteOrderModel getFavoriteOrderDetails(Integer favoriteOrderId) {
        this.id = favoriteOrderId;
        ArrayList<HashMap> favoriteOrder = dm.parameterizedExecuteQuery("data.favorites.getFavoriteOrderById", new Object[]{favoriteOrderId}, true);
        this.name = (String) favoriteOrder.get(0).get("NAME");
        this.employeeId = (Long) favoriteOrder.get(0).get("EMPLOYEEID");
        this.terminalId = (Long) favoriteOrder.get(0).get("TERMINALID");
        this.active = (boolean) favoriteOrder.get(0).get("ACTIVE");
        this.createdDTM = (Timestamp) favoriteOrder.get(0).get("CREATEDDTM");
        setAuthenticatedAccountID(CommonAPI.convertModelDetailToInteger(favoriteOrder.get(0).get("EMPLOYEEID")));
        try {
            this.inactivatedDTM = (Timestamp) favoriteOrder.get(0).get("INACTIVATEDDTM");
        } catch (ClassCastException e) {
            this.inactivatedDTM = null;
        }
        return this;
    }

    /**
     * Updates the Active, and InactivatedDTM field in the database based on the current favorite order
     */
    public void updateActiveStatus() throws Exception {
        dm.parameterizedExecuteNonQuery("data.favorites.updateFavoriteRecordStatus",
                new Object[]{this.id, this.active, this.inactivatedDTM});

        //check if an express reorder is set to this favorite order, update it's status in the cache if found
        MyQCCache.updateExpressFavoriteOrderInCache(getAuthenticatedAccountID(), this.id, this.active);
    }

    //abstract function for use in ExpressOrderModel
    public ArrayList<HashMap> isFavorite(HttpServletRequest request, long transactionId) throws Exception {
        Integer employeeID =  CommonAuthResource.checkRequestHeaderForMMHayesAuth(request);
        return isFavorite(employeeID, transactionId);
    }

    public ArrayList<HashMap> isFavorite(Integer employeeID, long transactionId) throws Exception {
        return dm.parameterizedExecuteQuery("data.favorites.isFavoriteOrder", new Object[]{transactionId, employeeID}, true);
    }

    public ArrayList<HashMap> isFavorite(HashMap products) throws Exception {
        String sqlQueryParam = "";
        MMHResultSet rs;
        ArrayList<HashMap> ret = new ArrayList<>();
        ArrayList<HashMap> productList = (ArrayList) products.get("products");
        Integer productParams = 0;
        for(int i = 0; i < productList.size(); i++) {
            if( !sqlQueryParam.equals("") ) {
                sqlQueryParam += " UNION ";
            }

            String productID = productList.get(i).get("id").toString();
            BigDecimal quantity = new BigDecimal(productList.get(i).get("quantity").toString());
            String productSql = "(SELECT " + productID + " AS PAPluID, " + quantity + " AS Quantity, NULL as PAPrepOptionID)";

            //check if the product has a valid prep option
            HashMap prepOption = (HashMap) productList.get(i).get("prepOption");
            if(prepOption != null && prepOption.get("id") != null) {
                productSql = "(SELECT " + productID + " AS PAPluID, " + quantity + " AS Quantity, " + prepOption.get("id").toString() +" as PAPrepOptionID)";
            }

            productParams++;

            ArrayList<HashMap> modifiers = (ArrayList<HashMap>) productList.get(i).get("modifiers");

            //check if the product has any modifiers
            if(modifiers != null) {
                for(HashMap mod: modifiers) {
                    String modID = mod.get("id").toString();

                    //check if the modifier has a valid prep option
                    HashMap modPrepOption = (HashMap) mod.get("prepOption");
                    if(modPrepOption != null && modPrepOption.get("id") != null) {
                        productSql += " UNION (SELECT " + modID + " AS PAPluID, " + quantity + " AS Quantity, "+ modPrepOption.get("id").toString() +" as PAPrepOptionID)";
                    } else {
                        productSql += " UNION (SELECT " + modID + " AS PAPluID, " + quantity + " AS Quantity, NULL as PAPrepOptionID)";
                    }
                    
                    productParams++;
                }
            }

            sqlQueryParam += productSql;
        }

        String query =
            "SELECT FavoriteOrderId, Active FROM QC_FavoriteOrder FAV WHERE EmployeeID = {2} AND NOT EXISTS("+
            "({1}) " +
            " EXCEPT " +
            " ((SELECT line.PAPluID, line.Quantity, line.PAPrepOptionID FROM QC_FavoriteOrderLineItem line WHERE line.FavoriteOrderID = FAV.FavoriteOrderID) " +
            " UNION " +
            " ( " +
            " SELECT mod.PAPluID, item.Quantity, mod.PAPrepOptionID FROM QC_FavoriteOrderLineItemMod mod " +
            " INNER JOIN QC_FavoriteOrderLineItem item ON mod.FavoriteOrderLineItemID = item.FavoriteOrderLineItemID" +
            " WHERE item.FavoriteOrderID = FAV.FavoriteOrderID)))" +
            "AND (SELECT COUNT(FavoriteOrderLineItemID) FROM QC_FavoriteOrderLineItem WHERE FavoriteOrderID = FAV.FavoriteOrderID) " +
            " + (SELECT COUNT(FavoriteOrderLineItemModID) FROM QC_FavoriteOrderLineItemMod FAVMOD LEFT JOIN QC_FavoriteOrderLineItem FAVLI ON FAVLI.FavoriteOrderLineItemID = FAVMOD.FavoriteOrderLineItemID WHERE FAVLI.FavoriteOrderID = FAV.FavoriteOrderID )" +
            " = {3}";

        if(products.size() > 0) {
            query = query.replace("{1}", sqlQueryParam);
            query = query.replace("{2}", String.valueOf(this.authenticatedAccountID));
            query = query.replace("{3}", String.valueOf(productParams));
            rs = dm.runSql(query);
            while(rs.resultSet.next()) {
                HashMap favorite = new HashMap();
                Long favoriteOrderId = rs.resultSet.getLong("FavoriteOrderID");
                boolean active = rs.resultSet.getBoolean("Active");
                favorite.put("FAVORITEORDERID", favoriteOrderId);
                favorite.put("ACTIVE", active);
                ret.add(favorite);
            }
        }
        return ret;
    }

    public FavoriteOrderModel getFavoriteOrderItems(Integer favoriteOrderID, HttpServletRequest request) throws  Exception {
        boolean foundDiningOption = false; //flag so we don't have to keep looking for a dining option product if we found one

        //get all items in a single favorite order
        ArrayList<HashMap> favoriteOrderItems = dm.parameterizedExecuteQuery("data.ordering.getFavoriteOrderItemsByID",
                new Object[]{
                        favoriteOrderID
                },
                true
        );

        if(favoriteOrderItems != null && favoriteOrderItems.size() > 0) {
            setID(favoriteOrderID);
            for(HashMap favoriteOrderItem : favoriteOrderItems) {

                //set the name of the favorite order model
                if(getName().equals("") && favoriteOrderItem.get("ORDERNAME") != null && !favoriteOrderItem.get("ORDERNAME").toString().equals("")) {
                    setName(CommonAPI.convertModelDetailToString(favoriteOrderItem.get("ORDERNAME")));
                }

                Integer lineID = CommonAPI.convertModelDetailToInteger(favoriteOrderItem.get("LINEID"));
                Integer modifierID = CommonAPI.convertModelDetailToInteger(favoriteOrderItem.get("MODID"));
                Integer keypadID = CommonAPI.convertModelDetailToInteger(favoriteOrderItem.get("PAKEYPADID"));
                Integer comboID = CommonAPI.convertModelDetailToInteger(favoriteOrderItem.get("PACOMBOID"));
                Integer terminalID = CommonAPI.convertModelDetailToInteger(favoriteOrderItem.get("TERMINALID"));

                if( modifierID != null ) {

                    ProductModel existingProductModel = getExistingProductModel(lineID);

                    if(existingProductModel == null) {
                        existingProductModel = new ProductModel(favoriteOrderItem);

                        existingProductModel.setTransLineItemID(lineID);
                        existingProductModel.setKeypadID(keypadID);

                        updateProductModelPrep(existingProductModel, favoriteOrderItem);
                        updateProductModelModifiers(existingProductModel, favoriteOrderItem);

                        if(!foundDiningOption) {
                            foundDiningOption = checkForDiningOptionProducts(existingProductModel, request);
                        }

                        getItems().add(existingProductModel);
                    } else {
                        updateProductModelModifiers(existingProductModel, favoriteOrderItem);
                    }

                    continue;
                }

                ProductModel productModel = new ProductModel(favoriteOrderItem);

                updateProductModelPrep(productModel, favoriteOrderItem);
                productModel.setTransLineItemID(lineID);
                productModel.setKeypadID(keypadID);

                updateProductModelCombo(productModel, comboID, terminalID);

                if(!foundDiningOption) {
                    foundDiningOption = checkForDiningOptionProducts(productModel, request);
                }

                getItems().add(productModel);
            }
        }
        return this;
    }

    public ProductModel getExistingProductModel(Integer favoriteOrderLineID) {
        for(ProductModel productModel : getItems()) {
            if(productModel.getTransLineItemID().equals(favoriteOrderLineID)) {
                return productModel;
            }
        }
        return null;
    }

    public void updateProductModelCombo(ProductModel productModel, Integer comboID,
                                        Integer terminalID) throws Exception {
        if(comboID == null) {
            return;
        }

        com.mmhayes.common.combo.models.ComboModel comboModel = new com.mmhayes.common.combo.models.ComboModel
                (comboID, terminalID);

        for(ComboDetailModel comboDetailModel : comboModel.getDetails()) {
            if(comboDetailModel.getKeypadDetailPAPluIds().contains(productModel.getID().toString())) {

                productModel.setBasePrice(comboDetailModel.getBasePrice());
                productModel.setComboPrice(comboDetailModel.getComboPrice());
                productModel.setComboDetailId(comboDetailModel.getId());
                productModel.setComboTransLineItemId(comboDetailModel.getComboId());

            }

        }

    }

    public void updateProductModelPrep(ProductModel productModel, HashMap favoriteOrderItem) throws Exception {
        Integer prepID = CommonAPI.convertModelDetailToInteger(favoriteOrderItem.get("PAPREPOPTIONID"));
        Integer prepSetID = CommonAPI.convertModelDetailToInteger(favoriteOrderItem.get("PAPREPOPTIONSETID"));
        boolean prepOptionSetActive = CommonAPI.convertModelDetailToBoolean(favoriteOrderItem.get("PREPSETACTIVE"),false);

        PrepOptionModel prepOptionModel = new PrepOptionModel();

        if( prepID != null ) {
            String prepName = CommonAPI.convertModelDetailToString(favoriteOrderItem.get("PREPNAME"));
            BigDecimal prepPrice = CommonAPI.convertModelDetailToBigDecimal(favoriteOrderItem.get("PREPPRICE"), BigDecimal.ZERO);
            boolean defaultOption = CommonAPI.convertModelDetailToBoolean(favoriteOrderItem.get("DEFAULTOPTION"), false);
            boolean displayDefault = CommonAPI.convertModelDetailToBoolean(favoriteOrderItem.get("DISPLAYDEFAULTMYQC"), false);

            prepOptionModel.setID(prepID);
            prepOptionModel.setPrepOptionSetID(prepSetID);
            prepOptionModel.setName(prepName);
            prepOptionModel.setPrice(prepPrice);
            prepOptionModel.setDefaultOption(defaultOption);
            prepOptionModel.setDisplayDefault(displayDefault);

            productModel.setPrepOptionSetID(prepOptionModel.getPrepOptionSetID());

        //if the product has a prep option set but did not when the order became a favorite, set the prep option as the default option in the prep option set
        } else if(prepSetID != null && prepOptionSetActive) {
           prepOptionModel.getDefaultPrepOptionModel(prepSetID);

        } else {
            return;
        }

        productModel.setPrepOption(prepOptionModel);

        //update price of favorite with prep price
        productModel.setPrice(productModel.getPrice().add(prepOptionModel.getPrice()));
    }

    //create modifier model for modifiers in favorite order
    public void updateProductModelModifiers(ProductModel productModel, HashMap favoriteOrderItem) throws Exception {

        ModifierModel modifierModel = new ModifierModel();

        Integer modifierID = CommonAPI.convertModelDetailToInteger(favoriteOrderItem.get("MODID"));
        String modifierName = CommonAPI.convertModelDetailToString(favoriteOrderItem.get("MODNAME"));
        BigDecimal modifierPrice = CommonAPI.convertModelDetailToBigDecimal(favoriteOrderItem.get("MODPRICE"));
        Integer modifierKeypadID = CommonAPI.convertModelDetailToInteger(favoriteOrderItem.get("PAKEYPADIDMOD"));

        //set modifier model
        modifierModel.setID(modifierID);
        modifierModel.setName(modifierName);
        modifierModel.setPrice(modifierPrice);
        modifierModel.setKeypadID(modifierKeypadID);

        //update price of favorite
        productModel.setPrice(productModel.getPrice().add(modifierPrice));

        //get modifier prep option details
        Integer modifierPrepID = CommonAPI.convertModelDetailToInteger(favoriteOrderItem.get("MODPREPID"));
        Integer modifierPrepSetID = CommonAPI.convertModelDetailToInteger(favoriteOrderItem.get("MODPREPSETID"));

        //set prep option on modifier model
        if( modifierPrepID != null ) {
            PrepOptionModel prepOptionModel = new PrepOptionModel();

            String modifierPrepName = CommonAPI.convertModelDetailToString(favoriteOrderItem.get("MODPREPNAME"));
            BigDecimal modifierPrepPrice = CommonAPI.convertModelDetailToBigDecimal(favoriteOrderItem.get("MODPREPPRICE"), BigDecimal.ZERO);
            boolean modifierDefaultOption = CommonAPI.convertModelDetailToBoolean(favoriteOrderItem.get("MODDEFAULTOPTION"), false);
            boolean modifierDisplayDefault = CommonAPI.convertModelDetailToBoolean(favoriteOrderItem.get("MODDISPLAYDEFAULT"), false);

            prepOptionModel.setID(modifierPrepID);
            prepOptionModel.setPrepOptionSetID(modifierPrepSetID);
            prepOptionModel.setName(modifierPrepName);
            prepOptionModel.setPrice(modifierPrepPrice);
            prepOptionModel.setDefaultOption(modifierDefaultOption);
            prepOptionModel.setDisplayDefault(modifierDisplayDefault);

            modifierModel.setPrepOption(prepOptionModel);
            modifierModel.setPrepOptionSetID(modifierPrepSetID);

            //update price of favorite with mod prep price
            productModel.setPrice(productModel.getPrice().add(prepOptionModel.getPrice()));

        //if the modifier has a prep option set now but didn't when the order became a favorite, set the prep option as the default in the prep option set
        } else if(modifierPrepSetID != null) {
            PrepOptionModel prepOptionModel = new PrepOptionModel();
            prepOptionModel.getDefaultPrepOptionModel(modifierPrepSetID);

            modifierModel.setPrepOption(prepOptionModel);

            //update price of favorite with mod prep price
            productModel.setPrice(productModel.getPrice().add(prepOptionModel.getPrice()));
        }

        List<ModifierModel> modifiers = productModel.getModifiers();
        if(modifiers == null) {
            modifiers = new ArrayList<>();
            modifiers.add(modifierModel);
            productModel.setModifiers(modifiers);
        } else {
            productModel.getModifiers().add(modifierModel);
        }
    }

    public boolean checkForDiningOptionProducts(ProductModel productModel, HttpServletRequest request) throws Exception {

        //retrieve the storeID (MyQCTerminalID) out of the request
        Integer storeID = CommonAPI.checkRequestHeaderForStoreID(request);

        //use the store model to get the Store Keypad Model from the cache to get the number of products that can be popular
        StoreModel storeModel = StoreModel.getStoreModel(storeID, request);
        StoreKeypadModel storeKeypadModel = StoreKeypadModel.getStoreKeypadModel(storeModel);

        //if the store has a dining option keypad and valid dining option products in the keypad, check if any of the products in the favorite order are a dining option product
        if(storeKeypadModel.getDiningOptionKeypadID() != null && storeKeypadModel.getDiningOptionProducts() != null && storeKeypadModel.getDiningOptionProducts().size() > 0) {

            for(HashMap diningOptionProduct : storeKeypadModel.getDiningOptionProducts()) {
                Integer diningOptionID = CommonAPI.convertModelDetailToInteger(diningOptionProduct.get("ID"));

                //if found the dining option product, set flag on product model and break because there can only be one dining option
                if(productModel.getID().equals(diningOptionID)) {
                    productModel.setDiningOptionProduct(true);
                    return true;
                }
            }
            return false; //return false if there is a dining option keypad and we didn't find it
        }

        return true;  //if there isn't a dining option keypad or products then don't keep checking for it
    }

    //update favorite order name
    public Integer updateOrderName(HashMap orderDetailsHM) throws Exception {
        Integer updateResult = 0;
        try {

            String orderID = CommonAPI.convertModelDetailToString(orderDetailsHM.get("ID"));
            String orderName = CommonAPI.convertModelDetailToString(orderDetailsHM.get("NAME"));

            updateResult = dm.parameterizedExecuteNonQuery("data.ordering.updateFavoriteOrderName",
                    new Object[]{
                            orderID,
                            orderName
                    }
            );

            if (updateResult == -1) {
                Logger.logMessage("Could not update favorite order name for favorite order ID: " + getID() + " in FavoriteOrderModel.updateOrderName", Logger.LEVEL.ERROR);
            }

            MyQCCache.updateExpressFavoriteOrderNameInCache(getAuthenticatedAccountID(), Integer.parseInt(orderID), orderName);

        } catch (Exception ex) {
            Logger.logMessage("ERROR: Could not update favorite order name for favorite order ID: " + getID() + " in FavoriteOrderModel.updateOrderName", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }

        return updateResult;
    }

    //getter
    private Integer getAuthenticatedAccountID() {
        return this.authenticatedAccountID;
    }

    //setter
    private void setAuthenticatedAccountID(Integer authenticatedAccountID) {
        this.authenticatedAccountID = authenticatedAccountID;
    }

    //getter
    public Integer getID() {
        return this.id;
    }

    //setter
    public void setID(Integer id) {
        this.id = id;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }

    public String getPrimary() {
        return primary;
    }

    public void setPrimary(String primary) {
        this.primary = primary;
    }

    public List<ProductModel> getItems() {
        return items;
    }

    public void setItems(List<ProductModel> items) {
        this.items = items;
    }

    public Integer getProductCount() {
        return productCount;
    }

    public void setProductCount(Integer productCount) {
        this.productCount = productCount;
    }

    public Timestamp getInactivatedDTM() {
        return inactivatedDTM;
    }

    public void setInactivatedDTM(Timestamp inactivatedDTM) {
        this.inactivatedDTM = inactivatedDTM;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    @Override
    public String toString() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            Logger.logMessage("Error converting FavoriteOrder to JSON", Logger.LEVEL.DEBUG);
            return String.valueOf(this.id);
        }

    }
}

