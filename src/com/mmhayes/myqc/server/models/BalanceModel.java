package com.mmhayes.myqc.server.models;

//mmhayes dependencies
import com.mmhayes.common.dataaccess.*;
import com.mmhayes.common.utils.*;
import com.mmhayes.common.api.json.serializers.*;

//javax.ws.rs dependencies
import com.fasterxml.jackson.databind.annotation.*;

//myqc dependencies
import com.mmhayes.myqc.server.collections.*;

//other dependencies
import java.util.*;
import java.math.BigDecimal;

/* Balance Model
 Last Updated (automatically updated by SVN)
 $Author: jrmitaly $: Author of last commit
 $Date: 2017-07-26 14:28:24 -0400 (Wed, 26 Jul 2017) $: Date of last commit
 $Rev: 22995 $: Revision of last commit

 Notes: Model for Employee Balances by Terminal Group. Terminal group contains terminals in the same spending profile w/ the same purchase limit (aka debit profile)
*/
public class BalanceModel {
    private commonMMHFunctions commFunc = new commonMMHFunctions();
    Integer id = null; //terminal group id (aka purchaseLimitID aka debitProfileID)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String name = "N/A"; //terminal group name
    BigDecimal limit = null; //how much an employee can spend at a particular terminal group
    BigDecimal balance = null; //what the current account balance is (a.k.a how much the employee account owes or is owed) at a particular terminal group
    BigDecimal avail = null; //how much the employee can spend until they hit their limit at a particular terminal group
    List<StoreBalanceModel> stores = null; //collection of revenue centers (stores) balances for a particular terminal group

    //constructor - takes hashmap that get sets to this models properties
    public BalanceModel(HashMap modelDetailHM, StoreBalanceCollection storeBalanceCollection) {
        try {
            setModelProperties(modelDetailHM, storeBalanceCollection);
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
    }

    //setter for all of this model's properties
    public BalanceModel setModelProperties(HashMap modelDetailHM, StoreBalanceCollection storeBalanceCollection) {
        try {
            if (modelDetailHM.get("TERMINALGROUPID") != null) {
                setID(Integer.parseInt(modelDetailHM.get("TERMINALGROUPID").toString()));
            }
            if (modelDetailHM.get("TERMINALGROUPNAME") != null) {
                setName(modelDetailHM.get("TERMINALGROUPNAME").toString());
            }
            if (modelDetailHM.get("TERMINALGROUPLIMIT") != null) {
                setLimit(new BigDecimal(modelDetailHM.get("TERMINALGROUPLIMIT").toString()));
            }
            if (modelDetailHM.get("TERMINALGROUPBALANCE") != null) {
                setBalance(new BigDecimal(modelDetailHM.get("TERMINALGROUPBALANCE").toString()));
            }
            if (modelDetailHM.get("TERMINALGROUPAVAILABLE") != null && modelDetailHM.get("TERMINALGROUPLIMIT") != null) {
                BigDecimal tmpTerminalGroupLimit = new BigDecimal(modelDetailHM.get("TERMINALGROUPLIMIT").toString());
                BigDecimal oneMillion = new BigDecimal(1000000.0000).setScale(4);
                if (tmpTerminalGroupLimit.equals(oneMillion)) { //if the spending limit is "one million dollars", then set as NULL
                    setAvail(null);
                } else {
                    setAvail(new BigDecimal(modelDetailHM.get("TERMINALGROUPAVAILABLE").toString()));
                }
            }
            if (storeBalanceCollection != null) {
                setStores(storeBalanceCollection.getCollection());
            }
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
        return this;
    }

    //getter
    public Integer getID() {
        return this.id;
    }

    //setter
    public void setID(Integer id) {
        this.id = id;
    }

    //getter
    public String getName() {
        return name;
    }

    //setter
    public void setName(String name) {
        this.name = name;
    }

    //getter
    public BigDecimal getLimit() {
        return limit;
    }

    //setter
    public void setLimit(BigDecimal limit) {
        this.limit = limit;
    }

    //getter
    public BigDecimal getBalance() {
        return balance;
    }

    //setter
    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    //getter
    public BigDecimal getAvail() {
        return avail;
    }

    //setter
    public void setAvail(BigDecimal avail) {
        this.avail = avail;
    }

    //getter
    public List<StoreBalanceModel> getStores() {
        return stores;
    }

    //setter
    public void setStores(List<StoreBalanceModel> stores) {
        this.stores = stores;
    }

}
