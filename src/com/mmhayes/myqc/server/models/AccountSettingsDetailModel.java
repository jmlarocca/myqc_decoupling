package com.mmhayes.myqc.server.models;

//mmhayes dependencies

//account settings dependencies

//other dependencies
import javax.servlet.http.HttpServletRequest;
import java.util.*;

import com.mmhayes.common.api.CommonAuthResource;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.myqc.server.collections.AccountSettingsCollection;
import com.mmhayes.common.account.models.AccountModel;

/* Account Funding Detail Model
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2019-01-16 09:48:09 -0500 (Wed, 16 Jan 2019) $: Date of last commit
 $Rev: 36174 $: Revision of last commit

 Notes: Model that contains account group, spending profile and account type collections for Account Settings
*/
public class AccountSettingsDetailModel {
    List<AccountGroupModel> accountGroupOptions = null;
    List<SpendingProfileModel> spendingProfileOptions = null;
    List<AccountTypeModel> accountTypeOptions = null;
    AccountModel accountModel = new AccountModel();
    boolean showSpendingProfileDetails = false;
    private static DataManager dm = new DataManager();

    //constructor - populates all collections
    public AccountSettingsDetailModel(HttpServletRequest request) throws Exception {
        AccountSettingsCollection accountSettingsCollection = new AccountSettingsCollection(request);

        setAccountGroupOptions(accountSettingsCollection.getAccountGroupCollection());
        setSpendingProfileOptions(accountSettingsCollection.getSpendingProfileCollection());
        setAccountTypeOptions(accountSettingsCollection.getAccountTypeCollection());

        setAccountModel(request);
        setShowSpendingProfileDetail(request);
    }

    //constructor - populates all collections
    public AccountSettingsDetailModel(HashMap accountDetailsHM, HttpServletRequest request) throws Exception {
        AccountSettingsCollection accountSettingsCollection = new AccountSettingsCollection(accountDetailsHM);

        setAccountGroupOptions(accountSettingsCollection.getAccountGroupCollection());
        setSpendingProfileOptions(accountSettingsCollection.getSpendingProfileCollection());
        setAccountTypeOptions(accountSettingsCollection.getAccountTypeCollection());

        setAccountModel(request);
        setShowSpendingProfileDetail(request);
    }

    //populate the account model
    public void setAccountModel(HttpServletRequest request) throws Exception {
        //determine the employeeID from the request
        Integer employeeID = CommonAuthResource.checkRequestHeaderForMMHayesAuth(request);

        if(employeeID != null) {
            AccountModel accountModel = new AccountModel(employeeID);
            setAccountModel(accountModel);  //sending back entire account model if other fields are needed in the future
        }
    }

    public void setShowSpendingProfileDetail(HttpServletRequest request) throws Exception {
        //determine the employeeID from the request
        Integer employeeID = CommonAuthResource.checkRequestHeaderForMMHayesAuth(request);

        boolean isPrepaidAccountType = checkForPrepaidStyleAccountType(employeeID); //determine if the account is a prepaid account

        if(!isPrepaidAccountType || ( isPrepaidAccountType && showSpendProfileDecline() )) {
           setShowSpendingProfileDetails(true);
        } else {
            setShowSpendingProfileDetails(false);
        }
    }

    //checks the currently logged in employee (account - same thing) to see if it is of an account type that is of "pre pay" style (pre paid, gift card)
    private Boolean checkForPrepaidStyleAccountType(Integer employeeID) throws Exception {
        Boolean isPrePayStyleAccount = false;
        Object isPrePayStyleAccountObj = dm.parameterizedExecuteScalar("data.myqc.checkForPrepaidStyleAccountType", new Object[] {employeeID});
        isPrePayStyleAccount = Boolean.parseBoolean(isPrePayStyleAccountObj.toString());
        return isPrePayStyleAccount;
    }

    private Boolean showSpendProfileDecline() throws Exception {
        Boolean showSpendProfileDecline = false;

        Object isPrePayStyleAccountObj = dm.parameterizedExecuteScalar("data.myqc.showSpendProfileDecline", new Object[] {});
        showSpendProfileDecline = Boolean.parseBoolean(isPrePayStyleAccountObj.toString());

        return showSpendProfileDecline;
    }

    public List<AccountGroupModel> getAccountGroupOptions() {
        return accountGroupOptions;
    }

    public void setAccountGroupOptions(List<AccountGroupModel> accountGroupOptions) {
        this.accountGroupOptions = accountGroupOptions;
    }

    public List<SpendingProfileModel> getSpendingProfileOptions() {
        return spendingProfileOptions;
    }

    public void setSpendingProfileOptions(List<SpendingProfileModel> spendingProfileOptions) {
        this.spendingProfileOptions = spendingProfileOptions;
    }

    public List<AccountTypeModel> getAccountTypeOptions() {
        return accountTypeOptions;
    }

    public void setAccountTypeOptions(List<AccountTypeModel> accountTypeOptions) {
        this.accountTypeOptions = accountTypeOptions;
    }

    public AccountModel getAccountModel() {
        return accountModel;
    }

    public void setAccountModel(AccountModel accountModel) {
        this.accountModel = accountModel;
    }

    public boolean isShowSpendingProfileDetails() {
        return showSpendingProfileDetails;
    }

    public void setShowSpendingProfileDetails(boolean showSpendingProfileDetails) {
        this.showSpendingProfileDetails = showSpendingProfileDetails;
    }
}
