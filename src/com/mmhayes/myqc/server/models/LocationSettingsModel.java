package com.mmhayes.myqc.server.models;

import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.utils.Logger;

/**
 * Created by mmhayes on 9/25/2020.
 */
import java.util.ArrayList;
import java.util.HashMap;

public class LocationSettingsModel {

    private static DataManager dm = new DataManager();
    private boolean MyQCAllowOnlineOrderLocation;
    private boolean MyQCPromptBeforeOnlineOrder;
    private boolean MyQCAllowOnlineOrderLocationSkip;


    public LocationSettingsModel(){

        try{
            ArrayList<HashMap> locationSettings = dm.parameterizedExecuteQuery("data.globalSettings.getLocationFilterSettings", new Object[]{}, true);

            if(locationSettings != null && locationSettings.size() == 1){
                HashMap locationHM = locationSettings.get(0);

                setMyQCAllowOnlineOrderLocation(CommonAPI.convertModelDetailToBoolean(locationHM.get("MYQCALLOWONLINEORDERLOCATION")));
                setMyQCpromptBeforeOnlineOrder(CommonAPI.convertModelDetailToBoolean(locationHM.get("MYQCPROMPTBEFOREONLINEORDER")));
                setMyQCAllowOnlineOrderLocationSkip(CommonAPI.convertModelDetailToBoolean(locationHM.get("MYQCALLOWONLINEORDERLOCATIONSKIP")));
            }

        }
        catch(Exception e){
            Logger.logMessage("Error getting location settings from globals: ");
            Logger.logException(e);
        }

    }

    public boolean getMyQCAllowOnlineOrderLocation() {
        return MyQCAllowOnlineOrderLocation;
    }

    public void setMyQCAllowOnlineOrderLocation(boolean myQCAllowOnlineOrderLocation) {
        MyQCAllowOnlineOrderLocation = myQCAllowOnlineOrderLocation;
    }

    public boolean getMyQCPromptBeforeOnlineOrder() {
        return MyQCPromptBeforeOnlineOrder;
    }

    public void setMyQCpromptBeforeOnlineOrder(boolean promptBeforeOnlineOrder) {
        MyQCPromptBeforeOnlineOrder = promptBeforeOnlineOrder;
    }

    public boolean getMyQCAllowOnlineOrderLocationSkip() {
        return MyQCAllowOnlineOrderLocationSkip;
    }

    public void setMyQCAllowOnlineOrderLocationSkip(boolean myQCAllowOnlineOrderLocationSkip) {
        MyQCAllowOnlineOrderLocationSkip = myQCAllowOnlineOrderLocationSkip;
    }
}
