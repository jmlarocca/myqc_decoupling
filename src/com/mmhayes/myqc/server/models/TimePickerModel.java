package com.mmhayes.myqc.server.models;

//mmhayes dependencies

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.utils.Logger;
import org.owasp.esapi.ESAPI;

import javax.servlet.http.HttpServletRequest;

import static java.time.temporal.ChronoUnit.MINUTES;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

//myqc dependencies
//other dependencies

/* Time Picker Model
 Last Updated (automatically updated by SVN)
 $Author: ijgerstein $: Author of last commit
 $Date: 2021-04-02 15:10:26 -0400 (Fri, 02 Apr 2021) $: Date of last commit
 $Rev: 54906 $: Revision of last commit

 Notes: Model for Time Picker
*/
public class TimePickerModel {
    private static DataManager dm = new DataManager();
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String orderType = null;
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String currentStoreTime = "";
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String pickupStoreClosing = "";
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String deliveryStoreClosing = "";
    ArrayList<String> pickUpOpenTimes = new ArrayList<>();
    ArrayList<String> pickUpCloseTimes = new ArrayList<>();
    ArrayList<String> deliveryOpenTimes = new ArrayList<>();
    ArrayList<String> deliveryCloseTimes = new ArrayList<>();
    Integer timezoneOffset = null;
    Integer pickupPrepTime = null;
    Integer deliveryPrepTime = null;
    HashMap timePicker = new HashMap();

    public TimePickerModel(StoreModel storeModel, String orderType) {
        try {

            setModelProperties(storeModel, orderType);

        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
    }

    public TimePickerModel(HashMap storeDetailsHM, HttpServletRequest request) {
        try {

            Integer storeID = CommonAPI.convertModelDetailToInteger(storeDetailsHM.get("storeID"));
            String orderType = CommonAPI.convertModelDetailToString(storeDetailsHM.get("orderType"));
            String futureOrderDateStr = CommonAPI.convertModelDetailToString(storeDetailsHM.get("futureOrderDate"));

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
            LocalDate futureOrderDate = LocalDate.parse(futureOrderDateStr, formatter);

            StoreModel storeModel = StoreModel.getStoreModelForFutureOrder(storeID, futureOrderDate, request);
            setModelProperties(storeModel, orderType);

        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
    }

    //setter for all of this model's properties
    public TimePickerModel setModelProperties(StoreModel modelDetailHM, String order) {
        try {

            LocalDateTime currentStoreTime = LocalDateTime.now();

            setTimezoneOffset(CommonAPI.convertModelDetailToInteger(modelDetailHM.getTimezoneOffset()));

            if (getTimezoneOffset() != null && getTimezoneOffset() != 0 ) {
                currentStoreTime = currentStoreTime.plusHours(getTimezoneOffset());
            }

            if(modelDetailHM.getFutureOrderDate() != null) {
                currentStoreTime = modelDetailHM.getCurrentStoreDTM();
            }

            //format time for military - examples: 11:00 (11 AM) or 17:00 (5 PM)
            String currentTime = currentStoreTime.format(DateTimeFormatter.ofPattern("HH:mm:ss"));

            setCurrentStoreTime(currentTime);

            setPickUpOpenTimes(CommonAPI.convertModelDetailToArrayListOfStrings(modelDetailHM.getPickUpOpenTimes()));

            setPickUpCloseTimes(CommonAPI.convertModelDetailToArrayListOfStrings(modelDetailHM.getPickUpCloseTimes()));

            setDeliveryOpenTimes(CommonAPI.convertModelDetailToArrayListOfStrings(modelDetailHM.getDeliveryOpenTimes()));

            setDeliveryCloseTimes(CommonAPI.convertModelDetailToArrayListOfStrings(modelDetailHM.getDeliveryCloseTimes()));

            setPickupPrepTime(CommonAPI.convertModelDetailToInteger(modelDetailHM.getPickUpPrepTime()));

            setDeliveryPrepTime(CommonAPI.convertModelDetailToInteger(modelDetailHM.getDeliveryPrepTime()));

            setPickupStoreClosing(modelDetailHM.getPickupOriginalCloseTime());

            setDeliveryStoreClosing(modelDetailHM.getDeliveryOriginalCloseTime());

            String orderType = determineOrderType(modelDetailHM, order);
            setOrderType(orderType);

            if(CommonAPI.convertModelDetailToBoolean(modelDetailHM.getUsingBuffering())) {
                setupTimePicker(); //using buffering schedules
            } else {
                setupTimePicker(); //using normal pickup or delivery schedules
            }

        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
        return this;
    }

    public String determineOrderType(StoreModel storeModel, String orderType) {

        Boolean allowPickup = CommonAPI.convertModelDetailToBoolean(storeModel.getAllowPickUp());
        Boolean allowDelivery = CommonAPI.convertModelDetailToBoolean(storeModel.getAllowDelivery());

        if(orderType.equals("notChosen") && allowPickup) {
            orderType = "pickup";
            if(getPickUpOpenTimes().size() == 0) {
                orderType = "delivery";
            }
        } else if (orderType.equals("notChosen") && !allowPickup && allowDelivery) {
            orderType = "delivery";
        }

        return orderType;
    }

    public LocalTime adjustForPrep(LocalTime storeTimeMoment, Integer prepTime, LocalTime currentStoreTime) {
        LocalTime now = currentStoreTime;

        //if given store moment (time) is before now (store is open already) then use now
        if(storeTimeMoment.isBefore(now)) {
            storeTimeMoment = now;
        }

        String store = storeTimeMoment.toString();
        LocalTime storeTime = LocalTime.parse(store);
        Integer mins = storeTime.getMinute() + prepTime;
        Integer addHours = 0;

        //round the minutes the user can choose from up to the nearest 15 min interval
        if(mins > 60) {
            Double hour = Math.floor(mins/60);
            addHours = hour.intValue();
            mins -= 60*addHours;
        }

        if( mins > 45 && mins <= 60 ) {
            mins = 0;
            addHours += 1;
        } else if ( mins > 30 && mins <= 45 ) {
            mins = 45;
        } else if ( mins > 15 && mins <= 30 ) {
            mins = 30;
        } else if ( mins > 0 ) {
            mins = 15;
        }

        if(addHours > 0) {
            storeTime = storeTime.plusHours(addHours);
        }
        //string manipulation to the correct time
        String minute = String.valueOf(mins);
        if(mins == 0) {
            minute = "00";
        }
        //can't do storeTime.getHour() because it converts 09:30 to 9:30 which we don't want, have to split string
        String timeSplit[] = (storeTime.toString()).split(":");
        String seconds = "";
        if(timeSplit.length > 2) { //check if the time includes seconds
            seconds = ":" + timeSplit[2];
        }
        String convertTime = timeSplit[0] + ":" + minute + seconds;
        storeTime = LocalTime.parse(convertTime);

        return storeTime;
    }

    public HashMap buildTimeRule(LocalTime openTime, LocalTime closeTime) {
        HashMap timeObj = new HashMap();
        ArrayList<Integer> timeList = new ArrayList<>();

        timeList.add(openTime.getHour());
        timeList.add(openTime.getMinute());
        timeObj.put("from", timeList);

        timeList = new ArrayList<>();

        timeList.add(closeTime.getHour());
        timeList.add(closeTime.getMinute());
        timeObj.put("to", timeList);

        return timeObj;
    }

    public HashMap determineOpenAndCloseTimes(ArrayList<String> openTimes, ArrayList<String> closeTimes, Integer prepTime, String currentTime, String scheduleClosingTime) {

        LocalTime currentStoreTime = LocalTime.parse(currentTime);
        LocalTime maxFutureTime = currentStoreTime;
        LocalTime minFutureTime = LocalTime.parse("23:59");
        LocalTime endOfDay = LocalTime.parse("23:45");
        LocalTime twelveAM = LocalTime.parse("00:00");
        LocalTime openTimeMoment = null;
        LocalTime closeTimeMoment = null;
        LocalTime asap = null;
        ArrayList<HashMap> enableTimes = new ArrayList<>();

        LocalTime scheduleCloseTime = LocalTime.now();
        if( !scheduleClosingTime.equals("") ) {
            scheduleCloseTime = LocalTime.parse(scheduleClosingTime);
        }

        for(int i = 0;  i < openTimes.size(); i++) {

            String opening = CommonAPI.convertToMilitaryTime(openTimes.get(i));
            String closing = CommonAPI.convertToMilitaryTime(closeTimes.get(i));
            if(closing.equals("00:00")) {
                closing = "23:59";
            }

            LocalTime openTime =  LocalTime.parse(opening);
            LocalTime closeTime =  LocalTime.parse(closing);

            openTimeMoment = adjustForPrep(openTime, prepTime, currentStoreTime);
            if (openTimeMoment.isBefore(currentStoreTime)) {
                // Don't let openTimeMoment be after midnight
                openTimeMoment = LocalTime.parse("23:59");
            }

            //get the rounded up or down time of when the schedule originally closes
            LocalTime formattedScheduleCloseTime = (adjustForPrep(scheduleCloseTime, 0, currentStoreTime)).withSecond(0);

            if(openTimeMoment.isBefore(minFutureTime)) {
                minFutureTime = openTimeMoment;
            }

            closeTimeMoment = adjustForPrep(closeTime, prepTime, currentStoreTime);

            //bug with the time picker when sending max times past the last possible interval (11:45 PM), if max time is after 11:45 PM, set to 11:45 PM.
            if (endOfDay.until(closeTimeMoment, MINUTES) >= 0) {
                closeTimeMoment = endOfDay;
            }

            if(closeTime.compareTo(endOfDay) > 0) {
                closeTimeMoment = endOfDay;
            }

            //if the closing time with prep is the same as when the schedule originally closes, set closing time to interval without prep. This would be if the last interval in the schedule was chosen
            if(closeTimeMoment.withSecond(0).compareTo(formattedScheduleCloseTime) == 0) {
                closeTimeMoment = closeTime;
            }

            //the maxFutureTime is when the schedule originally closed
            maxFutureTime = formattedScheduleCloseTime;

            //if intervals at the end of the schedule have been filled, the maxFutureTime will be the end of those intervals
            if(closeTimeMoment.isBefore(maxFutureTime)) {
                maxFutureTime = closeTimeMoment;
            }

            if(closeTimeMoment.toString().equals("00:00") || closeTimeMoment.isBefore(currentStoreTime.withSecond(0)) || maxFutureTime.toString().equals("00:00")) {
                maxFutureTime = endOfDay;
            }

            enableTimes.add(buildTimeRule(openTimeMoment, closeTimeMoment));
        }

        HashMap timeObj = new HashMap();
        timeObj.put("enableTimes", enableTimes);
        timeObj.put("minFutureTime", minFutureTime.toString());
        timeObj.put("maxFutureTime", maxFutureTime.toString());
        timeObj.put("openTimes", openTimes);
        timeObj.put("closeTimes", closeTimes);
        timeObj.put("orderType", getOrderType());
        setOrderType("");

        return timeObj;
    }

    public void setupTimePicker() {

        HashMap timeObj = new HashMap();

        if(getOrderType().equals("pickup")) {

            timeObj = determineOpenAndCloseTimes(getPickUpOpenTimes(), getPickUpCloseTimes(), getPickupPrepTime(), getCurrentStoreTime(), getPickupStoreClosing());

        } else if (getOrderType().equals("delivery")) {

            Integer deliveryPrepTime = getPickupPrepTime() + getDeliveryPrepTime();

            timeObj = determineOpenAndCloseTimes(getDeliveryOpenTimes(), getDeliveryCloseTimes(), deliveryPrepTime, getCurrentStoreTime(), getDeliveryStoreClosing());

         }

        setTimePicker(timeObj);
    }

    public String getCurrentStoreTime() {
        return currentStoreTime;
    }

    public void setCurrentStoreTime(String currentStoreTime) {
        this.currentStoreTime = currentStoreTime;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public ArrayList<String> getDeliveryCloseTimes() {
        return deliveryCloseTimes;
    }

    public void setDeliveryCloseTimes(ArrayList<String> deliveryCloseTimes) {
        this.deliveryCloseTimes = deliveryCloseTimes;
    }

    public ArrayList<String> getPickUpOpenTimes() {
        return pickUpOpenTimes;
    }

    public void setPickUpOpenTimes(ArrayList<String> pickUpOpenTimes) {
        this.pickUpOpenTimes = pickUpOpenTimes;
    }

    public ArrayList<String> getPickUpCloseTimes() {
        return pickUpCloseTimes;
    }

    public void setPickUpCloseTimes(ArrayList<String> pickUpCloseTimes) {
        this.pickUpCloseTimes = pickUpCloseTimes;
    }

    public ArrayList<String> getDeliveryOpenTimes() {
        return deliveryOpenTimes;
    }

    public void setDeliveryOpenTimes(ArrayList<String> deliveryOpenTimes) {
        this.deliveryOpenTimes = deliveryOpenTimes;
    }

    public Integer getDeliveryPrepTime() {
        return deliveryPrepTime;
    }

    public void setDeliveryPrepTime(Integer deliveryPrepTime) {
        this.deliveryPrepTime = deliveryPrepTime;
    }

    public Integer getPickupPrepTime() {
        return pickupPrepTime;
    }

    public void setPickupPrepTime(Integer pickupPrepTime) {
        this.pickupPrepTime = pickupPrepTime;
    }

    public HashMap getTimePicker() {
        return timePicker;
    }

    public void setTimePicker(HashMap timePicker) {
        this.timePicker = timePicker;
    }

    public Integer getTimezoneOffset() {
        return timezoneOffset;
    }

    public void setTimezoneOffset(Integer timezoneOffset) {
        this.timezoneOffset = timezoneOffset;
    }

    public String getPickupStoreClosing() {
        return pickupStoreClosing;
    }

    public void setPickupStoreClosing(String pickupStoreClosing) {
        this.pickupStoreClosing = pickupStoreClosing;
    }

    public String getDeliveryStoreClosing() {
        return deliveryStoreClosing;
    }

    public void setDeliveryStoreClosing(String deliveryStoreClosing) {
        this.deliveryStoreClosing = deliveryStoreClosing;
    }
}
