package com.mmhayes.myqc.server.models;

//mmhayes dependencies

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.utils.StringFunctions;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

//javax.ws.rs dependencies
//myqc dependencies
//other dependencies

/* Prep Option Model
 Last Updated (automatically updated by SVN)
 $Author: jkflanagan $: Author of last commit
 $Date: 2020-12-04 12:01:58 -0500 (Fri, 04 Dec 2020) $: Date of last commit
 $Rev: 53099 $: Revision of last commit

 Notes: Model for prep options
*/
public class PrepOptionModel {
    private static DataManager dm = new DataManager();
    Integer id = null; //ID
    Integer prepOptionSetID = null;
    Integer sortOrder = null;
    BigDecimal price = BigDecimal.ZERO;
    boolean defaultOption = false;
    boolean displayDefault = false;
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String name = "";
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String buttonText = "";
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String prepSetName = "";

    //dummy constructor - needed to consume ANY JSON object via REST (Jersey/Jackson style)
    public PrepOptionModel() {

    }

    //constructor - takes hashmap that get sets to this models properties
    public PrepOptionModel(HashMap modelDetailHM) throws Exception {
        setModelProperties(modelDetailHM);
    }

    public PrepOptionModel(Integer prepOptionID, Integer prepOptionSetID) throws Exception {
        getPrepOptionModel(prepOptionID, prepOptionSetID);
    }

    //setter for all of the favorite order model's for the My Quick Picks page
    public PrepOptionModel setModelProperties(HashMap modelDetailHM) throws Exception{
        setID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAPREPOPTIONID")));
        setPrepOptionSetID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAPREPOPTIONSETID")));
        setSortOrder(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("SORTORDER")));

        setPrice(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("PRICE")));

        setName(CommonAPI.convertModelDetailToString(modelDetailHM.get("NAME")));
        setButtonText(CommonAPI.convertModelDetailToString(modelDetailHM.get("BUTTONTEXT")));
        setPrepSetName(CommonAPI.convertModelDetailToString(modelDetailHM.get("PREPSETNAME")));

        setDefaultOption(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("DEFAULTOPTION")));
        setDisplayDefault(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("DISPLAYDEFAULTMYQC")));

        return this;
    }

    //populates this collection with models
    public PrepOptionModel getPrepOptionModel(Integer prepOptionID, Integer prepOptionSetID) throws Exception {

        //get all models in an array list
        ArrayList<HashMap> prepOptionList = dm.parameterizedExecuteQuery("data.ordering.getPrepOption",
                new Object[]{
                        prepOptionID,
                        prepOptionSetID
                },
                true
        );

        //populate this collection from an array list of hashmaps
        if (prepOptionList != null && prepOptionList.size() > 0) {
            HashMap prepOptionHM = prepOptionList.get(0);
            setModelProperties(prepOptionHM);
        }

        return this;
    }

    //populates this collection with models
    public PrepOptionModel getDefaultPrepOptionModel(Integer prepOptionSetID) throws Exception {

        //get all models in an array list
        ArrayList<HashMap> prepOptionList = dm.parameterizedExecuteQuery("data.ordering.getDefaultPrepOption",
                new Object[]{
                        prepOptionSetID
                },
                true
        );

        //populate this collection from an array list of hashmaps
        if (prepOptionList != null && prepOptionList.size() > 0) {
            HashMap prepOptionHM = prepOptionList.get(0);
            setModelProperties(prepOptionHM);
        }

        return this;
    }

    //getter
    public Integer getID() {
        return this.id;
    }

    //setter
    public void setID(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getButtonText() {
        return buttonText;
    }

    public void setButtonText(String buttonText) {
        this.buttonText = buttonText;
    }

    public Integer getPrepOptionSetID() {
        return prepOptionSetID;
    }

    public void setPrepOptionSetID(Integer prepOptionSetID) {
        this.prepOptionSetID = prepOptionSetID;
    }

    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public boolean isDefaultOption() {
        return defaultOption;
    }

    public void setDefaultOption(boolean defaultOption) {
        this.defaultOption = defaultOption;
    }

    public boolean isDisplayDefault() {
        return displayDefault;
    }

    public void setDisplayDefault(boolean displayDefault) {
        this.displayDefault = displayDefault;
    }

    public String getPrepSetName() {
        return prepSetName;
    }

    public void setPrepSetName(String prepSetName) {
        this.prepSetName = prepSetName;
    }

    /**
     * Overridden toString() method for a PrepOptionModel.
     * @return a {@link String} representation of a PrepOptionModel.
     */
    @Override
    public String toString () {

        return String.format("ID: %s, PREPOPTIONSETID: %s, SORTORDER: %s, PRICE: %s, DEFAULTOPTION: %s, DISPLAYDEFAULT: %s, NAME: %s, BUTTONTEXT: %s, PREPSETNAME: %s",
        Objects.toString((id != null && id > 0 ? id : "N/A"), "N/A"),
        Objects.toString((prepOptionSetID != null && prepOptionSetID > 0 ? prepOptionSetID : "N/A"), "N/A"),
        Objects.toString((sortOrder != null && sortOrder > 0 ? sortOrder : "N/A"), "N/A"),
        Objects.toString((price != null ? price.toPlainString() : "N/A"), "N/A"),
        Objects.toString(defaultOption, "N/A"),
        Objects.toString(displayDefault, "N/A"),
        Objects.toString((StringFunctions.stringHasContent(name) ? name : "N/A"), "N/A"),
        Objects.toString((StringFunctions.stringHasContent(buttonText) ? buttonText : "N/A"), "N/A"),
        Objects.toString((StringFunctions.stringHasContent(prepSetName) ? prepSetName : "N/A"), "N/A"));

    }

}

