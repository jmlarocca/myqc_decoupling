package com.mmhayes.myqc.server.models;

//mmhayes dependencies

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.CommonAuthResource;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;
import com.mmhayes.myqc.server.collections.ModifierCollection;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

//myqc dependencies
//other dependencies

/* Mod Menu Model
 Last Updated (automatically updated by SVN)
 $Author: jkflanagan $: Author of last commit
 $Date: 2020-12-04 12:01:58 -0500 (Fri, 04 Dec 2020) $: Date of last commit
 $Rev: 53099 $: Revision of last commit

 Notes: Model for a "Modifier Menu" (Modifier Keypad)
*/
public class ModMenuModel {
    Integer id = 0; //ID of this "Modifier Menu" (PAKeypadID)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String name = ""; //Name of this "Modifier Menu"
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String description = ""; //Description of this "Modifier Menu"
    Integer min = null; //Minimum products that need to be selected to "advance" through the modifier set
    Integer max = null; //Maximum products that can to be selected to "advance" through the modifier set
    Integer modSetDetailID = null; //ID of this "Modifier Menu" (PAModifierSetDetailID)
    List<ModifierModel> modifiers = new ArrayList<>(); //this Array List represents a ModifierCollection which contains a list of ModifierModels
    boolean showButtonDesc = false; //Should the buttons on the modifier menu show their description
    private static DataManager dm = new DataManager();
    private Integer authenticatedAccountID = null;

    //constructor - takes hashmap that get sets to this models properties
    public ModMenuModel(HashMap modelDetailHM) {
        try {
            setModelProperties(modelDetailHM);
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
    }

    //constructor - takes modDetailID and retrieves a ModMenuModel
    public ModMenuModel(Integer modDetailID, HttpServletRequest request) {
        try {

            //determine the authenticated account ID from the request
            setAuthenticatedAccountID(CommonAuthResource.checkRequestHeaderForMMHayesAuth(request));

            if (getAuthenticatedAccountID() != null) {

                //get all models in an array list
                ArrayList<HashMap> modMenuList = dm.parameterizedExecuteQuery("data.ordering.getModMenu",
                        new Object[]{
                                modDetailID
                        },
                        true
                );

                //populate this collection from an array list of hashmaps
                if (modMenuList != null && modMenuList.size() == 1) {
                    HashMap modelDetailHM = modMenuList.get(0);
                    if (modelDetailHM.get("ID") != null && !modelDetailHM.get("ID").toString().isEmpty()) {
                        setModelProperties(modelDetailHM);
                    } else {
                        Logger.logMessage("ERROR: Could not determine modDetailID in ModMenuModel(modDetailID)", Logger.LEVEL.ERROR);
                    }
                }
            } else {
                Logger.logMessage("ERROR: Could not determine authenticated accountID in ModMenuModel(modDetailID)", Logger.LEVEL.ERROR);
            }
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
    }

    //setter for all of this model's properties
    public ModMenuModel setModelProperties(HashMap modelDetailHM) {
        try {

            setID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));

            setModSetDetailID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("MODSETDETAILID")));

            setName(CommonAPI.convertModelDetailToString(modelDetailHM.get("NAME")));

            setDescription(CommonAPI.convertModelDetailToString(modelDetailHM.get("DESCRIPTION")));

            setMin(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("MIN"), null, true));

            setMax(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("MAX"), null, true));

            setShowButtonDesc(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("SHOWBUTTONDESC")));

        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
        return this;
    }

    //getter
    private Integer getAuthenticatedAccountID() {
        return this.authenticatedAccountID;
    }

    //setter
    private void setAuthenticatedAccountID(Integer authenticatedAccountID) {
        this.authenticatedAccountID = authenticatedAccountID;
    }

    //getter
    public Integer getID() {
        return this.id;
    }

    //setter
    public void setID(Integer id) {
        this.id = id;
    }

    //getter
    public String getName() {
        return name;
    }

    //setter
    public void setName(String name) {
        this.name = name;
    }

    //getter
    public String getDescription() {
        return description;
    }

    //setter
    public void setDescription(String description) {
        this.description = description;
    }

    //getter
    public Integer getMin() {
        return this.min;
    }

    //setter
    public void setMin(Integer min) {
        this.min = min;
    }

    //getter
    public Integer getMax() {
        return this.max;
    }

    //setter
    public void setMax(Integer max) {
        this.max = max;
    }

    //getter
    public List<ModifierModel> getModifiers() {
        return modifiers;
    }

    //setter
    public void setModifiers(List<ModifierModel> modifiers) {
        this.modifiers = modifiers;
    }

    public Integer getModSetDetailID() {
        return modSetDetailID;
    }

    public void setModSetDetailID(Integer modSetDetailID) {
        this.modSetDetailID = modSetDetailID;
    }

    public boolean isShowButtonDesc() {
        return showButtonDesc;
    }

    public void setShowButtonDesc(boolean showButtonDesc) {
        this.showButtonDesc = showButtonDesc;
    }
    
    /**
     * Overridden toString() method for a ModMenuModel.
     * @return a {@link String} representation of a ModMenuModel.
     */
    @Override
    public String toString() {

        String modsStr = "";
        if (!DataFunctions.isEmptyCollection(modifiers)) {
            modsStr += "[";
            for (ModifierModel modifier : modifiers) {
                modsStr += "[";
                if (modifier.equals(modifiers.get(modifiers.size() - 1))) {
                    modsStr += modifier.toString() + "; ";
                }
                else {
                    modsStr += modifier.toString() + "]";
                }
            }
            modsStr += "]";
        }
        
        return String.format("ID: %s, NAME: %s, DESCRIPTION: %s, MIN: %s, MAX: %s, MODSETDETAILID: %s, MODIFIERS: %s, SHOWBUTTONDESC: %s, AUTHENTICATEDACCOUNTID: %s",
                Objects.toString((id != null && id > 0 ? id : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(name) ? name : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(description) ? description : "N/A"), "N/A"),
                Objects.toString((min != null && min > 0 ? min : "N/A"), "N/A"),
                Objects.toString((max != null && max > 0 ? max : "N/A"), "N/A"),
                Objects.toString((modSetDetailID > 0 ? modSetDetailID : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(modsStr) ? modsStr : "N/A"), "N/A"),
                Objects.toString(showButtonDesc, "N/A"),
                Objects.toString((authenticatedAccountID != null && authenticatedAccountID > 0 ? authenticatedAccountID : "N/A"), "N/A"));
                
    }
    
}
