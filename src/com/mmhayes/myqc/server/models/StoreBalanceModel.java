package com.mmhayes.myqc.server.models;

//mmhayes dependencies

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.dataaccess.commonMMHFunctions;
import com.mmhayes.common.utils.Logger;

import java.math.BigDecimal;
import java.util.HashMap;

//myqc dependencies
//other dependencies

/* Store Balance Model
 Last Updated (automatically updated by SVN)
 $Author: ecdyer $: Author of last commit
 $Date: 2017-08-02 08:58:52 -0400 (Wed, 02 Aug 2017) $: Date of last commit
 $Rev: 23136 $: Revision of last commit

 Notes: Model for Employee Balances for a specific revenue center (aka store) in a specific terminal group
*/
public class StoreBalanceModel {
    private commonMMHFunctions commFunc = new commonMMHFunctions();
    Integer id = null; //revenue center ID (aka storeID)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String name = "N/A"; //revenue center name
    BigDecimal balance = null; //what the current account balance is (a.k.a how much the employee account owes or is owed) at this particular revenue center

    //constructor - takes hashmap that get sets to this models properties
    public StoreBalanceModel(HashMap modelDetailHM) {
        try {
            setModelProperties(modelDetailHM);
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
    }

    //setter for all of this model's properties
    public StoreBalanceModel setModelProperties(HashMap modelDetailHM) {
        try {
            if (modelDetailHM.get("REVCENTERID") != null) {
                setID(Integer.parseInt(modelDetailHM.get("REVCENTERID").toString()));
            }
            if (modelDetailHM.get("REVCENTERNAME") != null) {
                setName(modelDetailHM.get("REVCENTERNAME").toString());
            }
            if (modelDetailHM.get("REVCENTERBALANCE") != null) {
                setBalance(new BigDecimal(modelDetailHM.get("REVCENTERBALANCE").toString()));
            }
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
        return this;
    }

    //getter
    public Integer getID() {
        return this.id;
    }

    //setter
    public void setID(Integer id) {
        this.id = id;
    }

    //getter
    public String getName() {
        return name;
    }

    //setter
    public void setName(String name) {
        this.name = name;
    }

    //getter
    public BigDecimal getBalance() {
        return balance;
    }

    //setter
    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }


}
