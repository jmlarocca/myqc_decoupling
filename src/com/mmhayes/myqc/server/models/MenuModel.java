package com.mmhayes.myqc.server.models;

//mmhayes dependencies

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.CommonAuthResource;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.myqc.server.collections.FavoriteCollection;
import com.mmhayes.myqc.server.collections.MenuItemCollection;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

//myqc dependencies
//other dependencies

/* Menu Model
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2020-02-13 17:02:17 -0500 (Thu, 13 Feb 2020) $: Date of last commit
 $Rev: 46191 $: Revision of last commit

 Notes: Model for a "Menu" (Keypad)
*/
public class MenuModel {
    Integer id = null; //ID of this "Menu" (PAKeypadID)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String name = "N/A"; //Name of this "Menu"
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String description = "N/A"; //Description of this "Menu"
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String errorDetails = ""; //holds any error message
    List<MenuItemModel> items = null; //this Array List represents a MenuItemCollection which contains a list of MenuItemModels
    private static DataManager dm = new DataManager();
    private Integer authenticatedAccountID = null;
    private Integer storeID = null;
    private boolean isUpsellMenu = false;


    //constructor - takes hashmap that get sets to this models properties
    public MenuModel(HashMap modelDetailHM, Integer employeeID, Integer storeID) {
        try {
            //set the authenticated account ID
            setAuthenticatedAccountID(employeeID);

            //set store ID
            setStoreID(storeID);

            setModelProperties(modelDetailHM);
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
    }

    //constructor - takes hashmap that get sets to this models properties
    public MenuModel(HashMap modelDetailHM, Integer employeeID, Integer storeID, boolean isUpsell) {
        try {
            //set the authenticated account ID
            setAuthenticatedAccountID(employeeID);

            //set store ID
            setStoreID(storeID);

            setUpsellMenu(isUpsell);

            setModelProperties(modelDetailHM);
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
    }

    //constructor - takes menuID retrieves menu details
    public MenuModel(Integer menuID, HttpServletRequest request) throws  Exception{

        //determine the authenticated account ID from the request
        setAuthenticatedAccountID(CommonAuthResource.checkRequestHeaderForMMHayesAuth(request));

        //determine the store ID from the request
        setStoreID(CommonAPI.checkRequestHeaderForStoreID(request));

        populateCollection(menuID);
    }

    //populates MenuModel collection
    public MenuModel populateCollection(Integer menuID) {
        try {

            if (getAuthenticatedAccountID() != null) {

                //get all models in an array list
                ArrayList<HashMap> menuList = dm.parameterizedExecuteQuery("data.ordering.getMenuHeader",
                        new Object[]{
                                menuID
                        },
                        true
                );

                //populate this collection from an array list of hashmaps
                if (menuList != null && menuList.size() == 1) {
                    HashMap modelDetailHM = menuList.get(0);
                    if (modelDetailHM.get("ID") != null && !modelDetailHM.get("ID").toString().isEmpty()) {
                        setModelProperties(modelDetailHM);
                    } else {
                        Logger.logMessage("ERROR: Could not determine menuID in MenuModel(menuID)", Logger.LEVEL.ERROR);
                    }
                }
            } else {
                Logger.logMessage("ERROR: Could not determine authenticated accountID in MenuModel(menuID)", Logger.LEVEL.ERROR);
            }
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }

        return this;
    }

    //setter for all of this model's properties
    public MenuModel setModelProperties(HashMap modelDetailHM) {
        try {

            setID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));

            setName(CommonAPI.convertModelDetailToString(modelDetailHM.get("NAME")));

            setDescription(CommonAPI.convertModelDetailToString(modelDetailHM.get("DESCRIPTION")));

            if (getID() != null) { //if is a valid ID we can safely create the MenuItemCollection

                //Create MenuItemCollection and set it on this MenuModel
                MenuItemCollection menuItemCollection = new MenuItemCollection(getID(), isUpsellMenu(), getAuthenticatedAccountID(), getStoreID());
                setItems(menuItemCollection.getCollection());

                //Removed this check 11/13/18 - JMD
                //check if the user has available funds to spend and if they can buy at least one product from the store
                //checkAvailableFunds(request);
            }

        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
        return this;
    }

    public void checkAvailableFunds(HttpServletRequest request) {
        try {
            HashMap userBalanceHM = CommonAPI.getUserBalance(request, CommonAPI.convertModelDetailToBigDecimal(0));
            BigDecimal userBalance = CommonAPI.convertModelDetailToBigDecimal(userBalanceHM.get("USERBALANCE"));
            String userBalanceType = CommonAPI.convertModelDetailToString(userBalanceHM.get("USERBALANCETYPE"));
            String userBalanceAmtStr = userBalance.setScale(2, BigDecimal.ROUND_FLOOR).toString();

            Boolean found = false;

            if ( userBalance.compareTo(BigDecimal.ZERO) < 0 ) {
                userBalance = BigDecimal.ZERO;
            }

            if ( getItems() == null || getItems().size() == 0 ) {
                setErrorDetails("No products were available for this menu.");
                return;
            }

            //loop over each item price to see if the user can afford at least ONE item
            for (MenuItemModel item : getItems()) {
                if(item.getPrice().compareTo(userBalance) <= 0) {
                    found = true;
                    break;
                }
            }
            //if at least one item a user can afford is NOT found
            if(!found) {

                if( Double.parseDouble(userBalanceAmtStr) < 0.00) {
                    userBalanceAmtStr = "0.00";
                }

                String errorMsg = "You do not have enough available funds to complete a transaction";
                if ( userBalanceType.equals("store") ) {
                    errorMsg = errorMsg.concat(" at this location. You have $".concat(userBalanceAmtStr).concat(" available to spend in this location."));
                } else if ( userBalanceType.equals("global") ) {
                    errorMsg = errorMsg.concat(". You have $".concat(userBalanceAmtStr).concat(" available to spend."));
                } else if ( userBalanceType.equals("transaction") ) {
                    errorMsg = errorMsg.concat(" at this location today. You have $".concat(userBalanceAmtStr).concat(" available to spend."));
                }
                setErrorDetails(errorMsg);
            }


        } catch (Exception ex) {
            Logger.logException(ex);
        }
    }

    //getter
    public Integer getID() {
        return this.id;
    }

    //setter
    public void setID(Integer id) {
        this.id = id;
    }

    //getter
    public String getName() {
        return name;
    }

    //setter
    public void setName(String name) {
        this.name = name;
    }

    //getter
    public String getDescription() {
        return description;
    }

    //setter
    public void setDescription(String description) {
        this.description = description;
    }

    //getter
    public List<MenuItemModel> getItems() {
        return items;
    }

    //setter
    public void setItems(List<MenuItemModel> items) {
        this.items = items;
    }

    //getter
    private Integer getAuthenticatedAccountID() {
        return this.authenticatedAccountID;
    }

    //setter
    private void setAuthenticatedAccountID(Integer authenticatedAccountID) {
        this.authenticatedAccountID = authenticatedAccountID;
    }

    //getter
    public String getErrorDetails() { return errorDetails; }

    //setter
    public void setErrorDetails(String errorDetails) { this.errorDetails = errorDetails; }

    public Integer getStoreID() {
        return storeID;
    }

    public void setStoreID(Integer storeID) {
        this.storeID = storeID;
    }

    public boolean isUpsellMenu() {
        return isUpsellMenu;
    }

    public void setUpsellMenu(boolean upsellMenu) {
        isUpsellMenu = upsellMenu;
    }

}
