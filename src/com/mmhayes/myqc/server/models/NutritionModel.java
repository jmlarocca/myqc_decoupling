package com.mmhayes.myqc.server.models;

//mmhayes dependencies

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.commonMMHFunctions;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.receiptGen.ReceiptData.NutritionCategory;
import com.mmhayes.common.receiptGen.ReceiptData.NutritionItem;
import com.mmhayes.common.receiptGen.ReceiptData.NutritionTotalConfig;
import com.mmhayes.common.utils.Logger;
import org.owasp.esapi.ESAPI;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.List;

//javax.ws.rs dependencies
//myqc dependencies
//other dependencies

/* Product Model
 Last Updated (automatically updated by SVN)
 $Author: prsmith $: Author of last commit
 $Date: 2021-01-15 14:30:45 -0500 (Fri, 15 Jan 2021) $: Date of last commit
 $Rev: 53810 $: Revision of last commit

 Notes: Model for products (a.k.a PLUs, Items, etc.)
*/
public class NutritionModel {
    Integer id = null; //Product ID (PAPluID)
    private static DataManager dm = new DataManager();
    ArrayList<NutritionCategory> nutritionCategories = null;
    ArrayList<NutritionTotalConfig> nutritionDVInfo = null;
    List<ProductNutritionModel> nutritionItemList = null;
    NutritionItem productNutritionTotal = null;
    ProductNutritionModel productTotal = null;
    List<NutritionItem> allProductNutritionTotals = null;
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String totalNutritionInfo1 = "";
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String totalNutritionInfo2 = "";
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String totalNutritionInfo3 = "";
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String totalNutritionInfo4 = "";
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String totalNutritionInfo5 = "";
    ArrayList<HashMap> calculatedDVTotals = null;
    NutritionItem totalNutritionItem = null;

    protected commonMMHFunctions commDM = new commonMMHFunctions();

    //Product page view
    public NutritionModel(HashMap modelDetailHM, HttpServletRequest request) throws Exception {
        setProductProperties(modelDetailHM);
    }

    //setter for Product
    public NutritionModel setProductProperties(HashMap productInfo) throws Exception{
        setNutritionCategoryNames();
        getProductInfoAndTotals(productInfo, false);

        return this;
    }

    //Cart page view
    public NutritionModel(ArrayList<HashMap> modelDetailHM) throws Exception {
        setCartProperties(modelDetailHM);
    }

    //setter for Cart
    public NutritionModel setCartProperties(ArrayList<HashMap> productInfoList) throws Exception{

        //set the Category names
        setNutritionCategoryNames();

        //set the Daily Value Info
        setNutritionDVInfo();

        //loop over each product and calculate its total nutritional information (for if there are modifiers)
        List<NutritionItem> allProductNutritionItems = new ArrayList<NutritionItem>();
        for(HashMap productInfo : productInfoList) {
            getProductInfoAndTotals(productInfo, true);
            allProductNutritionItems.add(getProductNutritionTotal());
        }

        //set the total nutritional information of ALL products
        setAllProductNutritionTotals(allProductNutritionItems);

        //calculate the Daily Value totals for each category
        calculateDailyValuePercentages();

        //set total product nutritional information
        createTotalNutritionItem();

        return this;
    }

    //get the nutrition category name
    public void setNutritionCategoryNames() throws Exception {
        try {
            ArrayList<HashMap> nutriCategories = dm.parameterizedExecuteQuery("data.ordering.getNutritionCategoryNames", new Object[]{}, true);
            ArrayList<NutritionCategory> nutritionCategories = new ArrayList<NutritionCategory>();

            for (HashMap hm : nutriCategories)
            {
                String shortName = HashMapDataFns.getStringVal(hm, "SHORTNAME");
                String measurementLbl = HashMapDataFns.getStringVal(hm, "MEASUREMENTLBL");
                String name = HashMapDataFns.getStringVal(hm, "NAME");

                NutritionCategory nutriCat = new NutritionCategory(name, shortName, measurementLbl);
                nutritionCategories.add(nutriCat);
            }
            setNutritionCategories(nutritionCategories);
        }  catch (Exception ex) {
            Logger.logMessage("Unable to retrieve Nutrition Category Names in ProductNutritionModel.getAndSetNutritionCategoryNames", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }
    }

    //get the Daily Value information
    public void setNutritionDVInfo() throws Exception {
        try {
            ArrayList<HashMap> nutriTotals = dm.parameterizedExecuteQuery("data.ordering.getNutritionTotals", new Object[]{}, true);
            ArrayList<NutritionTotalConfig> nutritionTotals = new ArrayList<>();

            for (HashMap hm : nutriTotals)
            {
                int nutritionTotalsID = HashMapDataFns.getIntVal(hm, "PANUTRITIONTOTALSID");
                String dvCalorieValLbl = HashMapDataFns.getStringVal(hm, "DVCALORIEVALLBL");
                String nutritonInfo1Total = HashMapDataFns.getStringVal(hm, "NUTRITIONINFO1TOTAL");
                String nutritonInfo2Total = HashMapDataFns.getStringVal(hm, "NUTRITIONINFO2TOTAL");
                String nutritonInfo3Total = HashMapDataFns.getStringVal(hm, "NUTRITIONINFO3TOTAL");
                String nutritonInfo4Total = HashMapDataFns.getStringVal(hm, "NUTRITIONINFO4TOTAL");
                String nutritonInfo5Total = HashMapDataFns.getStringVal(hm, "NUTRITIONINFO5TOTAL");

                NutritionTotalConfig totalConfig = new NutritionTotalConfig();

                totalConfig.setNutritionTotalsID(nutritionTotalsID);
                totalConfig.setDvCalorieValLbl(dvCalorieValLbl);
                totalConfig.setNutritionInfo1Total(nutritonInfo1Total);
                totalConfig.setNutritionInfo2Total(nutritonInfo2Total);
                totalConfig.setNutritionInfo3Total(nutritonInfo3Total);
                totalConfig.setNutritionInfo4Total(nutritonInfo4Total);
                totalConfig.setNutritionInfo5Total(nutritonInfo5Total);

                nutritionTotals.add(totalConfig);
            }
            setNutritionDVInfo(nutritionTotals);
        } catch (Exception ex) {
            Logger.logMessage("Unable to retrieve Nutrition Totals in NutritionModel.getandSetNutritionTotals", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }
    }

    //get the Product Information and determine Totals
    public void getProductInfoAndTotals(HashMap productInfo, Boolean isCartPage) {
        try {
            ArrayList pluIDArrList = createListOfPAPluIDs(productInfo);

            ArrayList<HashMap> productNutritionInfo = setProductNutritionInfo(pluIDArrList, productInfo);

            String quantity = productInfo.get("QUANTITY").toString();
            if(isCartPage) {
                formatCartNutritionInfo(productNutritionInfo, quantity);
            } else {
                formatProductNutritionInfo(productNutritionInfo, quantity);
            }

        } catch (Exception ex) {
            Logger.logMessage("Unable to retrieve Product Nutrition Info in NutritionModel.getProductNutritionInfo", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }
    }

    //creates a list of PAPluIDs to pass into one query to get the product info
    public ArrayList<String> createListOfPAPluIDs(HashMap productInfo) {
        ArrayList<String> pluIDArrList = new ArrayList<>();
        try{
            if(productInfo.containsKey("PAPLUID")) {
                pluIDArrList.add(productInfo.get("PAPLUID").toString());
            }

            if(productInfo.containsKey("MODIFIERINFO")) {
                ArrayList<HashMap> modifierInfoList = CommonAPI.convertModelDetailToArrayListOfHashMaps(productInfo.get("MODIFIERINFO"));

                if(modifierInfoList == null) {
                    Logger.logMessage("Unable to retrieve create list of PAPluIDs NutritionModel.createListOfPAPluIDs", Logger.LEVEL.ERROR);
                    return pluIDArrList;
                }

                for(HashMap modifierHM : modifierInfoList) {
                    pluIDArrList.add(modifierHM.get("id").toString());
                    String modifierName =  ESAPI.encoder().decodeForHTML(modifierHM.get("name").toString());
                    modifierHM.put("name", modifierName);
                }
            }
        } catch (Exception ex) {
            Logger.logMessage("Unable to retrieve create list of PAPluIDs NutritionModel.createListOfPAPluIDs", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }

        return pluIDArrList;
    }

    //gets the nutritional info of each product and modifier in the PluID array list
    public ArrayList<HashMap> setProductNutritionInfo(ArrayList<String> pluIDArrList, HashMap productInfo) {
        ArrayList<HashMap> prodNutritionInfo = new ArrayList<>();
        try {
            if(pluIDArrList.size() > 0) {
                String pluIDList = commDM.ArrayListToCSV(pluIDArrList);
                //if plu ids are empty no reason to ping db
                if(pluIDList.isEmpty()){
                    return new ArrayList<>();
                }
                // Get nutrition data for products in transaction from DB
                prodNutritionInfo = dm.serializeSqlWithColNames("data.ordering.getProductNutritionInfo", new Object[]{pluIDList}, false);

                //update the product names with the names from the given product info if they are different
                if(prodNutritionInfo != null && prodNutritionInfo.size() > 0) {
                    for(HashMap prodInfo: prodNutritionInfo) {
                        if(prodInfo.get("PAPLUID").toString().equals(productInfo.get("PAPLUID").toString())) {
                            if(!prodInfo.get("NAME").toString().equals(productInfo.get("PRODUCTNAME").toString())) {
                                prodInfo.put("NAME", productInfo.get("PRODUCTNAME").toString());
                            }
                        } else if(productInfo.containsKey("MODIFIERINFO")) {
                            ArrayList<HashMap> modifierInfoList = CommonAPI.convertModelDetailToArrayListOfHashMaps(productInfo.get("MODIFIERINFO"));
                            for(HashMap modifierHM : modifierInfoList) {
                                if(prodInfo.get("PAPLUID").toString().equals(modifierHM.get("id").toString())) {
                                    if(!prodInfo.get("NAME").toString().equals(modifierHM.get("name").toString())) {
                                        prodInfo.put("NAME", modifierHM.get("name").toString());
                                    }
                                }
                            }
                        }
                    }
                }
            }

        } catch (Exception ex) {
            Logger.logMessage("Unable to retrieve Product Nutrition Info in NutritionModel.getProductNutritionInfo", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }
        return prodNutritionInfo;
    }

    public void formatProductNutritionInfo(ArrayList<HashMap> productNutritionInfo, String quantity) {
        try {
            List<ProductNutritionModel> nutritionItems = new ArrayList<ProductNutritionModel>();

            ProductNutritionModel totalNutritionItem = new ProductNutritionModel();
            BigDecimal nutritionTotals1 = BigDecimal.ZERO;
            BigDecimal nutritionTotals2 = BigDecimal.ZERO;
            BigDecimal nutritionTotals3 = BigDecimal.ZERO;
            BigDecimal nutritionTotals4 = BigDecimal.ZERO;
            BigDecimal nutritionTotals5 = BigDecimal.ZERO;

            for (HashMap prodNutritionHM : productNutritionInfo) {

                int paPluID = HashMapDataFns.getIntVal(prodNutritionHM, "PAPLUID");
                String itemName = HashMapDataFns.getStringVal(prodNutritionHM, "NAME");
                Boolean isModifier = HashMapDataFns.getBooleanVal(prodNutritionHM, "ISMODIFIER");
                String strProdNutriInfoOne = HashMapDataFns.getStringVal(prodNutritionHM, "NUTRITIONINFO1");
                String strProdNutriInfoTwo = HashMapDataFns.getStringVal(prodNutritionHM, "NUTRITIONINFO2");
                String strProdNutriInfoThree = HashMapDataFns.getStringVal(prodNutritionHM, "NUTRITIONINFO3");
                String strProdNutriInfoFour = HashMapDataFns.getStringVal(prodNutritionHM, "NUTRITIONINFO4");
                String strProdNutriInfoFive = HashMapDataFns.getStringVal(prodNutritionHM, "NUTRITIONINFO5");

                if(!isModifier) {
                    totalNutritionItem.setProductName(itemName);
                    totalNutritionItem.setPluID(paPluID);
                    totalNutritionItem.setQuantity(quantity);
                }

                ProductNutritionModel nutriItem = new ProductNutritionModel();
                setID(paPluID);
                nutriItem.setPluID(paPluID);
                nutriItem.setProductName(itemName);
                nutriItem.setQuantity(quantity);

                // CATEGORY 1
                if (strProdNutriInfoOne.equals("")) {
                    nutriItem.setNutritionInfo1("***");

                } else {
                    BigDecimal prodNutriInfoOne = nutriItem.calculateNutritionTotal(strProdNutriInfoOne);
                    nutriItem.setNutritionInfo1(prodNutriInfoOne.toString());
                    nutritionTotals1 = nutritionTotals1.add(prodNutriInfoOne);
                }

                // CATEGORY 2
                if (strProdNutriInfoTwo.equals(""))  {
                    nutriItem.setNutritionInfo2("***");

                } else {
                    BigDecimal prodNutriInfoTwo = nutriItem.calculateNutritionTotal(strProdNutriInfoTwo);
                    nutriItem.setNutritionInfo2(prodNutriInfoTwo.toString());
                    nutritionTotals2 = nutritionTotals2.add(prodNutriInfoTwo);
                }

                // CATEGORY 3
                if (strProdNutriInfoThree.equals("")) {
                    nutriItem.setNutritionInfo3("***");

                } else {
                    BigDecimal prodNutriInfoThree = nutriItem.calculateNutritionTotal(strProdNutriInfoThree);
                    nutriItem.setNutritionInfo3(prodNutriInfoThree.toString());
                    nutritionTotals3 = nutritionTotals3.add(prodNutriInfoThree);
                }

                // CATEGORY 4
                if (strProdNutriInfoFour.equals("")) {
                    nutriItem.setNutritionInfo4("***");

                } else {
                    BigDecimal prodNutriInfoFour = nutriItem.calculateNutritionTotal(strProdNutriInfoFour);
                    nutriItem.setNutritionInfo4(prodNutriInfoFour.toString());
                    nutritionTotals4 = nutritionTotals4.add(prodNutriInfoFour);
                }

                // CATEGORY 5
                if (strProdNutriInfoFive.equals("")) {
                    nutriItem.setNutritionInfo5("***");

                } else {
                    BigDecimal prodNutriInfoFive = nutriItem.calculateNutritionTotal(strProdNutriInfoFive);
                    nutriItem.setNutritionInfo5(prodNutriInfoFive.toString());
                    nutritionTotals5 = nutritionTotals5.add(prodNutriInfoFive);
                }

                nutritionItems.add(nutriItem);
            }

            totalNutritionItem.setNutritionInfo1(nutritionTotals1.toString());
            if(nutritionTotals1.toString().equals("0")) {
                totalNutritionItem.setNutritionInfo1("***");
            }
            totalNutritionItem.setNutritionInfo2(nutritionTotals2.toString());
            if(nutritionTotals2.toString().equals("0")) {
                totalNutritionItem.setNutritionInfo2("***");
            }
            totalNutritionItem.setNutritionInfo3(nutritionTotals3.toString());
            if(nutritionTotals3.toString().equals("0")) {
                totalNutritionItem.setNutritionInfo3("***");
            }
            totalNutritionItem.setNutritionInfo4(nutritionTotals4.toString());
            if(nutritionTotals4.toString().equals("0")) {
                totalNutritionItem.setNutritionInfo4("***");
            }
            totalNutritionItem.setNutritionInfo5(nutritionTotals5.toString());
            if(nutritionTotals5.toString().equals("0")) {
                totalNutritionItem.setNutritionInfo5("***");
            }

            setProductTotal(totalNutritionItem);

            setNutritionItemList(nutritionItems);

        } catch (Exception ex) {
            Logger.logMessage("Unable to format Product Nutrition Info in NutritionModel.formatProductNutritionInfo", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }
    }

    public void formatCartNutritionInfo(ArrayList<HashMap> productNutritionInfo, String quantity) {
        try {
            NutritionItem totalNutritionItem = new NutritionItem();
            BigDecimal nutritionTotals1 = BigDecimal.ZERO;
            BigDecimal nutritionTotals2 = BigDecimal.ZERO;
            BigDecimal nutritionTotals3 = BigDecimal.ZERO;
            BigDecimal nutritionTotals4 = BigDecimal.ZERO;
            BigDecimal nutritionTotals5 = BigDecimal.ZERO;

            String strProd1 = "", strProd2 = "", strProd3 = "", strProd4 = "", strProd5 = "";

            for (HashMap prodNutritionHM : productNutritionInfo) {

                int paPluID = HashMapDataFns.getIntVal(prodNutritionHM, "PAPLUID");
                String itemName = HashMapDataFns.getStringVal(prodNutritionHM, "NAME");
                Boolean isModifier = HashMapDataFns.getBooleanVal(prodNutritionHM, "ISMODIFIER");
                String strProdNutriInfoOne = HashMapDataFns.getStringVal(prodNutritionHM, "NUTRITIONINFO1");
                String strProdNutriInfoTwo = HashMapDataFns.getStringVal(prodNutritionHM, "NUTRITIONINFO2");
                String strProdNutriInfoThree = HashMapDataFns.getStringVal(prodNutritionHM, "NUTRITIONINFO3");
                String strProdNutriInfoFour = HashMapDataFns.getStringVal(prodNutritionHM, "NUTRITIONINFO4");
                String strProdNutriInfoFive = HashMapDataFns.getStringVal(prodNutritionHM, "NUTRITIONINFO5");

                //Defect 3828 & 4133
                //Problem: Product name in the cart view for nutritional info was showing modifier name instead of actual product name
                //Solution: use the isModifier boolean to make sure the product name isn't overwritten by modifier names.
                if(!isModifier){
                    totalNutritionItem.setProductName(itemName);
                }
                totalNutritionItem.setPluID(paPluID);
                totalNutritionItem.setQuantity(quantity);

                NutritionItem nutriItem = new NutritionItem();
                setID(paPluID);
                nutriItem.setPluID(paPluID);
                nutriItem.setProductName(itemName);
                nutriItem.setQuantity(quantity);

                // CATEGORY 1
                if (strProdNutriInfoOne.equals("")) {
                    nutriItem.setNutritionInfo1(strProdNutriInfoOne);
                    if(strProd1.equals("")) { strProd1 = "***"; }
                } else {
                    BigDecimal prodNutriInfoOne = nutriItem.calculateNutritionTotal(strProdNutriInfoOne);
                    nutriItem.setNutritionInfo1(prodNutriInfoOne.toString());
                    nutritionTotals1 = nutritionTotals1.add(prodNutriInfoOne);
                    strProd1 = strProdNutriInfoOne;
                    if(prodNutriInfoOne.compareTo(BigDecimal.valueOf(9999)) > 0) {
                        this.totalNutritionInfo1 = prodNutriInfoOne.toString();
                    }
                }

                // CATEGORY 2
                if (strProdNutriInfoTwo.equals(""))  {
                    nutriItem.setNutritionInfo2(strProdNutriInfoTwo);
                    if(strProd2.equals("")) { strProd2 = "***"; }
                } else {
                    BigDecimal prodNutriInfoTwo = nutriItem.calculateNutritionTotal(strProdNutriInfoTwo);
                    nutriItem.setNutritionInfo2(prodNutriInfoTwo.toString());
                    nutritionTotals2 = nutritionTotals2.add(prodNutriInfoTwo);
                    strProd2 = strProdNutriInfoTwo;
                    if(prodNutriInfoTwo.compareTo(BigDecimal.valueOf(9999)) > 0) {
                        this.totalNutritionInfo2 = prodNutriInfoTwo.toString();
                    }
                }

                // CATEGORY 3
                if (strProdNutriInfoThree.equals("")) {
                    nutriItem.setNutritionInfo3(strProdNutriInfoThree);
                    if(strProd3.equals("")) { strProd3 = "***"; }
                } else {
                    BigDecimal prodNutriInfoThree = nutriItem.calculateNutritionTotal(strProdNutriInfoThree);
                    nutriItem.setNutritionInfo3(prodNutriInfoThree.toString());
                    nutritionTotals3 = nutritionTotals3.add(prodNutriInfoThree);
                    strProd3 = strProdNutriInfoThree;
                    if(prodNutriInfoThree.compareTo(BigDecimal.valueOf(9999)) > 0) {
                        this.totalNutritionInfo3 = prodNutriInfoThree.toString();
                    }
                }

                // CATEGORY 4
                if (strProdNutriInfoFour.equals("")) {
                    nutriItem.setNutritionInfo4(strProdNutriInfoFour);
                    if(strProd4.equals("")) { strProd4 = "***"; }
                } else {
                    BigDecimal prodNutriInfoFour = nutriItem.calculateNutritionTotal(strProdNutriInfoFour);
                    nutriItem.setNutritionInfo4(prodNutriInfoFour.toString());
                    nutritionTotals4 = nutritionTotals4.add(prodNutriInfoFour);
                    strProd4 = strProdNutriInfoFour;
                    if(prodNutriInfoFour.compareTo(BigDecimal.valueOf(9999)) > 0) {
                        this.totalNutritionInfo4 = prodNutriInfoFour.toString();
                    }
                }

                // CATEGORY 5
                if (strProdNutriInfoFive.equals("")) {
                    nutriItem.setNutritionInfo5(strProdNutriInfoFive);
                    if(strProd5.equals("")) { strProd5 = "***"; }
                } else {
                    BigDecimal prodNutriInfoFive = nutriItem.calculateNutritionTotal(strProdNutriInfoFive);
                    nutriItem.setNutritionInfo5(prodNutriInfoFive.toString());
                    nutritionTotals5 = nutritionTotals5.add(prodNutriInfoFive);
                    strProd5 = strProdNutriInfoFive;
                    if(prodNutriInfoFive.compareTo(BigDecimal.valueOf(9999)) > 0) {
                        this.totalNutritionInfo5 = prodNutriInfoFive.toString();
                    }
                }
            }

            totalNutritionItem.setNutritionInfo1(nutritionTotals1.toString());
            if(strProd1.equals("***")) {
                totalNutritionItem.setNutritionInfo1("");
            }
            totalNutritionItem.setNutritionInfo2(nutritionTotals2.toString());
            if(strProd2.equals("***")) {
                totalNutritionItem.setNutritionInfo2("");
            }
            totalNutritionItem.setNutritionInfo3(nutritionTotals3.toString());
            if(strProd3.equals("***")) {
                totalNutritionItem.setNutritionInfo3("");
            }
            totalNutritionItem.setNutritionInfo4(nutritionTotals4.toString());
            if(strProd4.equals("***")) {
                totalNutritionItem.setNutritionInfo4("");
            }
            totalNutritionItem.setNutritionInfo5(nutritionTotals5.toString());
            if(strProd5.equals("***")) {
                totalNutritionItem.setNutritionInfo5("");
            }

            setProductNutritionTotal(totalNutritionItem);

            calculateTotals(totalNutritionItem);

        } catch (Exception ex) {
            Logger.logMessage("Unable to format Product Nutrition Info for Cart view in NutritionModel.formatCartNutritionInfo", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }
    }


    //calculates the daily value percentage for each category
    public void calculateDailyValuePercentages() {

        List<NutritionTotalConfig> totalConfigList = getNutritionDVInfo();
        BigDecimal nutritionTotal1 = new BigDecimal(getTotalNutritionInfo1());
        BigDecimal nutritionTotal2 = new BigDecimal(getTotalNutritionInfo2());
        BigDecimal nutritionTotal3 = new BigDecimal(getTotalNutritionInfo3());
        BigDecimal nutritionTotal4 = new BigDecimal(getTotalNutritionInfo4());
        BigDecimal nutritionTotal5 = new BigDecimal(getTotalNutritionInfo5());

        ArrayList<HashMap> calculatedDVTotals = new ArrayList<HashMap>();
        Integer index = 1;

        for(NutritionTotalConfig totalConfig: totalConfigList) {

            HashMap dailyValueItem = new HashMap();
            dailyValueItem.put("dvLabel",totalConfig.getDvCalorieValLbl());
            dailyValueItem.put("id",index);

            String strDVTotalsOne = "***";  String strDVTotalsTwo = "***"; String strDVTotalsThree = "***";
            String strDVTotalsFour = "***"; String strDVTotalsFive = "***";

            // CATEGORY 1
            if (totalConfig.getNutritionInfo1Total() != null) {
                BigDecimal nutritionMaxVal = totalConfig.getNutritionInfo1Total();
                BigDecimal dvTotalsOne = nutritionTotal1.divide(nutritionMaxVal, 4, RoundingMode.HALF_UP);
                dvTotalsOne = dvTotalsOne.multiply(new BigDecimal("100"));

                strDVTotalsOne = determineDailyValuePercentage(dvTotalsOne);
            }
            dailyValueItem.put("dvTotal1",strDVTotalsOne);

            // CATEGORY 2
            if (totalConfig.getNutritionInfo2Total() != null)  {
                BigDecimal nutritionMaxVal = totalConfig.getNutritionInfo2Total();
                BigDecimal dvTotalsTwo = nutritionTotal2.divide(nutritionMaxVal, 4, RoundingMode.HALF_UP);
                dvTotalsTwo = dvTotalsTwo.multiply(new BigDecimal("100"));

                strDVTotalsTwo = determineDailyValuePercentage(dvTotalsTwo);
            }
            dailyValueItem.put("dvTotal2",strDVTotalsTwo);

            // CATEGORY 3
            if (totalConfig.getNutritionInfo3Total() != null) {
                BigDecimal nutritionMaxVal = totalConfig.getNutritionInfo3Total();
                BigDecimal dvTotalsThree = nutritionTotal3.divide(nutritionMaxVal, 4, RoundingMode.HALF_UP);
                dvTotalsThree = dvTotalsThree.multiply(new BigDecimal("100"));

                strDVTotalsThree = determineDailyValuePercentage(dvTotalsThree);
            }
            dailyValueItem.put("dvTotal3",strDVTotalsThree);

            // CATEGORY 4
            if (totalConfig.getNutritionInfo4Total() != null) {
                BigDecimal nutritionMaxVal = totalConfig.getNutritionInfo4Total();
                BigDecimal dvTotalsFour = nutritionTotal4.divide(nutritionMaxVal, 4, RoundingMode.HALF_UP);
                dvTotalsFour = dvTotalsFour.multiply(new BigDecimal("100"));

                strDVTotalsFour = determineDailyValuePercentage(dvTotalsFour);
            }
            dailyValueItem.put("dvTotal4",strDVTotalsFour);

            // CATEGORY 5
            if (totalConfig.getNutritionInfo5Total() != null) {
                BigDecimal nutritionMaxVal = totalConfig.getNutritionInfo5Total();
                BigDecimal dvTotalsFive = nutritionTotal5.divide(nutritionMaxVal, 4, RoundingMode.HALF_UP);
                dvTotalsFive = dvTotalsFive.multiply(new BigDecimal("100"));

                strDVTotalsFive = determineDailyValuePercentage(dvTotalsFive);
            }
            dailyValueItem.put("dvTotal5",strDVTotalsFive);
            index++;

            calculatedDVTotals.add(dailyValueItem);
        }
        setCalculatedDVTotals(calculatedDVTotals);
    }

    //performs the calculation to determine the percentage
    private String determineDailyValuePercentage(BigDecimal dvTotalsX) {
        String strDVTotalVal = "***";
        BigDecimal threeDigitMax = new BigDecimal("999");
        BigDecimal fourDigitMax = new BigDecimal("9999");

        if (dvTotalsX.compareTo(threeDigitMax) <= 0)  {
            // If total is <= 999, print it with a % sign
            strDVTotalVal = dvTotalsX.setScale(0, RoundingMode.HALF_UP).toPlainString() + "%";
        }

        else if (dvTotalsX.compareTo(threeDigitMax) == 1 && dvTotalsX.compareTo(fourDigitMax) <= 0)  {
            // If total is > 999 AND total is <= 9999, print it with NO % sign
            strDVTotalVal = dvTotalsX.setScale(0, RoundingMode.HALF_UP).toPlainString();
        }

        else {
            strDVTotalVal = "###";
        }

        return strDVTotalVal;
    }

    //calculate the total nutritional info for each category (look at setters for calcualtions)
    public void calculateTotals(NutritionItem nutritionItem) {
        setTotalNutritionInfo1(nutritionItem.getNutritionInfo1());
        setTotalNutritionInfo2(nutritionItem.getNutritionInfo2());
        setTotalNutritionInfo3(nutritionItem.getNutritionInfo3());
        setTotalNutritionInfo4(nutritionItem.getNutritionInfo4());
        setTotalNutritionInfo5(nutritionItem.getNutritionInfo5());
    }

    //set the Total Product nutritional info (for the 'Totals' row in the Cart nutritional table)
    public void createTotalNutritionItem() {
        NutritionItem totalNutrition = new NutritionItem();
        totalNutrition.setProductName("Totals");
        totalNutrition.setNutritionInfo1(getTotalNutritionInfo1());
        totalNutrition.setNutritionInfo2(getTotalNutritionInfo2());
        totalNutrition.setNutritionInfo3(getTotalNutritionInfo3());
        totalNutrition.setNutritionInfo4(getTotalNutritionInfo4());
        totalNutrition.setNutritionInfo5(getTotalNutritionInfo5());
        setTotalNutritionItem(totalNutrition);
    }

    //getter
    public Integer getID() {
        return this.id;
    }

    //setter
    public void setID(Integer id) {
        this.id = id;
    }

    public ArrayList<NutritionCategory> getNutritionCategories() {
        return nutritionCategories;
    }

    public void setNutritionCategories(ArrayList<NutritionCategory> nutritionCategories) {
        this.nutritionCategories = nutritionCategories;
    }

    public ArrayList<NutritionTotalConfig> getNutritionDVInfo() {
        return nutritionDVInfo;
    }

    public void setNutritionDVInfo(ArrayList<NutritionTotalConfig> nutritionDVInfo) {
        this.nutritionDVInfo = nutritionDVInfo;
    }

    public List<ProductNutritionModel> getNutritionItemList() {
        return nutritionItemList;
    }

    public void setNutritionItemList(List<ProductNutritionModel> nutritionItemList) {
        this.nutritionItemList = nutritionItemList;
    }

    public NutritionItem getProductNutritionTotal() {
        return productNutritionTotal;
    }

    public void setProductNutritionTotal(NutritionItem productNutritionTotal) {
        this.productNutritionTotal = productNutritionTotal;
    }

    public List<NutritionItem> getAllProductNutritionTotals() {
        return allProductNutritionTotals;
    }

    public void setAllProductNutritionTotals(List<NutritionItem> allProductNutritionTotals) {
        this.allProductNutritionTotals = allProductNutritionTotals;
    }


    public String getTotalNutritionInfo1() {
        return totalNutritionInfo1;
    }

    public void setTotalNutritionInfo1(String totalNutritionInfo1) {
        String nutritionInfo1 = getTotalNutritionInfo1();
        if(totalNutritionInfo1.equals("***")) {
            totalNutritionInfo1 = "0";
        }

        if( nutritionInfo1.equals("") || nutritionInfo1.equals("0")) {
            this.totalNutritionInfo1 = new BigDecimal(totalNutritionInfo1).setScale(0, RoundingMode.HALF_UP).toPlainString();

        } else if(totalNutritionInfo1.equals("###") && new Double(nutritionInfo1) > 9999){
            this.totalNutritionInfo1 = new BigDecimal(nutritionInfo1).setScale(0, RoundingMode.HALF_UP).toPlainString();

        } else {
            Double nutriInfoInt = new Double(nutritionInfo1);
            Double givenNutriInfoInt = new Double(totalNutritionInfo1);
            this.totalNutritionInfo1 = new BigDecimal(nutriInfoInt + givenNutriInfoInt).setScale(0, RoundingMode.HALF_UP).toPlainString();
        }
    }

    public String getTotalNutritionInfo2() {
        return totalNutritionInfo2;
    }

    public void setTotalNutritionInfo2(String totalNutritionInfo2) {
        String nutritionInfo2 = getTotalNutritionInfo2();
        if(totalNutritionInfo2.equals("***")) {
            totalNutritionInfo2 = "0";
        }

        if( nutritionInfo2.equals("") || nutritionInfo2.equals("0")) {
            this.totalNutritionInfo2 = new BigDecimal(totalNutritionInfo2).setScale(0, RoundingMode.HALF_UP).toPlainString();

        } else if(totalNutritionInfo2.equals("###") && new Double(nutritionInfo2) > 9999){
            this.totalNutritionInfo2 = new BigDecimal(nutritionInfo2).setScale(0, RoundingMode.HALF_UP).toPlainString();

        } else {
            Double nutriInfoInt = new Double(nutritionInfo2);
            Double givenNutriInfoInt = new Double(totalNutritionInfo2);
            this.totalNutritionInfo2 = new BigDecimal(nutriInfoInt + givenNutriInfoInt).setScale(0, RoundingMode.HALF_UP).toPlainString();
        }
    }

    public String getTotalNutritionInfo3() {
        return totalNutritionInfo3;
    }

    public void setTotalNutritionInfo3(String totalNutritionInfo3) {
        String nutritionInfo3 = getTotalNutritionInfo3();
        if(totalNutritionInfo3.equals("***")) {
            totalNutritionInfo3 = "0";
        }

        if(nutritionInfo3.equals("") || nutritionInfo3.equals("0")) {
            this.totalNutritionInfo3 = new BigDecimal(totalNutritionInfo3).setScale(0, RoundingMode.HALF_UP).toPlainString();

        } else if(totalNutritionInfo3.equals("###") && new Double(nutritionInfo3) > 9999){
            this.totalNutritionInfo3 = new BigDecimal(nutritionInfo3).setScale(0, RoundingMode.HALF_UP).toPlainString();

        } else {
            Double nutriInfoInt = new Double(nutritionInfo3);
            Double givenNutriInfoInt = new Double(totalNutritionInfo3);
            this.totalNutritionInfo3 = new BigDecimal(nutriInfoInt + givenNutriInfoInt).setScale(0, RoundingMode.HALF_UP).toPlainString();
        }
    }

    public String getTotalNutritionInfo4() {
        return totalNutritionInfo4;
    }

    public void setTotalNutritionInfo4(String totalNutritionInfo4) {
        String nutritionInfo4 = getTotalNutritionInfo4();
        if(totalNutritionInfo4.equals("***")) {
            totalNutritionInfo4 = "0";
        }

        if( (nutritionInfo4.equals("") || nutritionInfo4.equals("0")) && !totalNutritionInfo4.equals("###")) {
            this.totalNutritionInfo4 = new BigDecimal(totalNutritionInfo4).setScale(0, RoundingMode.HALF_UP).toPlainString();

        } else if(totalNutritionInfo4.equals("###") && new Double(nutritionInfo4) > 9999){
            this.totalNutritionInfo4 = new BigDecimal(nutritionInfo4).setScale(0, RoundingMode.HALF_UP).toPlainString();

        } else {
            Double nutriInfoInt = new Double(nutritionInfo4);
            Double givenNutriInfoInt = new Double(totalNutritionInfo4);
            this.totalNutritionInfo4 = new BigDecimal(nutriInfoInt + givenNutriInfoInt).setScale(0, RoundingMode.HALF_UP).toPlainString();
        }
    }

    public String getTotalNutritionInfo5() {
        return totalNutritionInfo5;
    }

    public void setTotalNutritionInfo5(String totalNutritionInfo5) {
        String nutritionInfo5 = getTotalNutritionInfo5();
        if(totalNutritionInfo5.equals("***")) {
            totalNutritionInfo5 = "0";
        }

        if( (nutritionInfo5.equals("") || nutritionInfo5.equals("0")) && !totalNutritionInfo5.equals("###")) {
            this.totalNutritionInfo5 = new BigDecimal(totalNutritionInfo5).setScale(0, RoundingMode.HALF_UP).toPlainString();

        } else if(totalNutritionInfo5.equals("###") && new Double(nutritionInfo5) > 9999){
            this.totalNutritionInfo5 = new BigDecimal(nutritionInfo5).setScale(0, RoundingMode.HALF_UP).toPlainString();

        } else {
            Double nutriInfoInt = new Double(nutritionInfo5);
            Double givenNutriInfoInt = new Double(totalNutritionInfo5);
            this.totalNutritionInfo5 = new BigDecimal(nutriInfoInt + givenNutriInfoInt).setScale(0, RoundingMode.HALF_UP).toPlainString();
        }
    }

    public ArrayList<HashMap> getCalculatedDVTotals() {
        return calculatedDVTotals;
    }

    public void setCalculatedDVTotals(ArrayList<HashMap> calculatedDVTotals) {
        this.calculatedDVTotals = calculatedDVTotals;
    }

    public NutritionItem getTotalNutritionItem() {
        return totalNutritionItem;
    }

    public void setTotalNutritionItem(NutritionItem totalNutritionItem) {
        this.totalNutritionItem = totalNutritionItem;
    }

    public ProductNutritionModel getProductTotal() {
        return productTotal;
    }

    public void setProductTotal(ProductNutritionModel productTotal) {
        this.productTotal = productTotal;
    }
}

