package com.mmhayes.myqc.server.models;

//mmhayes dependencies

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.dataaccess.commonMMHFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.myqc.server.collections.AccountSettingsCollection;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;

//other dependencies

/* Spending Profile Model
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2019-05-14 15:32:28 -0400 (Tue, 14 May 2019) $: Date of last commit
 $Rev: 38625 $: Revision of last commit

 Notes: Model for Spending Profiles
*/
public class SpendingProfileModel {
    private commonMMHFunctions commFunc = new commonMMHFunctions();
    private Integer loggedInEmployeeID = null;
    Integer id = null;
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String value = "N/A";
    String price = "";
    Boolean active = null;

    //constructor - takes hashmap that get sets to this models properties
    public SpendingProfileModel(HashMap modelDetailHM) {
        try {
            setModelProperties(modelDetailHM);
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
    }

    //constructor - takes hashmap that get sets to this models properties
    public SpendingProfileModel(HashMap modelDetailHM, Integer employeeID) {
        try {
            setLoggedInEmployeeID(employeeID);

            setModelProperties(modelDetailHM);
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
    }

    //setter for all of this model's properties
    public SpendingProfileModel setModelProperties(HashMap modelDetailHM) {
        try {
            if (modelDetailHM.get("TERMINALPROFILEID") != null) {
                setID(Integer.parseInt(modelDetailHM.get("TERMINALPROFILEID").toString()));
            }
            if (modelDetailHM.get("NAME") != null) {
                setValue(modelDetailHM.get("NAME").toString());
            }
            if (modelDetailHM.get("ACTIVE") != null) {
                setActive(Boolean.valueOf(modelDetailHM.get("ACTIVE").toString()));
            }
            if (modelDetailHM.get("PRICE") != null) {
                setPrice(modelDetailHM.get("PRICE").toString());
                if(!modelDetailHM.get("PRICE").toString().equals("")) {
                    String price = new BigDecimal(modelDetailHM.get("PRICE").toString()).setScale(2, RoundingMode.HALF_UP).toPlainString();
                    setPrice("$"+price);
                }
            }

        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
        return this;
    }

    //getter
    public Integer getID() {
        return this.id;
    }

    //setter
    public void setID(Integer id) {
        this.id = id;
    }

    //getter
    public String getValue() {
        return value;
    }

    //setter
    public void setValue(String value) {
        this.value = value;
    }

    //getter
    public Boolean getActive() {
        return active;
    }

    //setter
    public void setActive(Boolean active) {
        this.active = active;
    }

    //getter
    public String getPrice() {
        return price;
    }

    //setter
    public void setPrice(String price) {
        this.price = price;
    }

    public Integer getLoggedInEmployeeID() {
        return loggedInEmployeeID;
    }

    public void setLoggedInEmployeeID(Integer loggedInEmployeeID) {
        this.loggedInEmployeeID = loggedInEmployeeID;
    }
}
