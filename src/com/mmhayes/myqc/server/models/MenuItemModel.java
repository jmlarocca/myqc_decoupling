package com.mmhayes.myqc.server.models;

//mmhayes dependencies

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.utils.Logger;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

//myqc dependencies
//other dependencies

/* Menu Item Model
 Last Updated (automatically updated by SVN)
 $Author: skaugustine $: Author of last commit
 $Date: 2021-01-12 15:03:36 -0500 (Tue, 12 Jan 2021) $: Date of last commit
 $Rev: 53790 $: Revision of last commit

 Notes: Model for "Menu" items - the details of a menu (the content of a Keypad (Keypad Details) but only Keypads or Products are allowed)
*/
public class MenuItemModel {
    Integer id = null; //MenuItemID (PAKeypadDetailID) - Identifier for this object
    Integer menuID = null; //Menu ID (keypadID) - means the Keypad Button is type 1
    Integer productID = null; //Product ID (PAPluID) - means the Keypad Button is type 2
    Integer comboID = null; //Combo ID (PACombo) - means the Keypad Button is type 36
    Integer modSetDetailID = null; //ID of the FIRST "placeholder" in the "Modifier Menu" - PAModifierSetDetailID off of the QC_PAModifierSetDetail table  - means the Keypad Button is type 2 and autoshowmodset is on
    Integer prepOptionSetID = null; //Prep Option Set ID
    Integer imageShapeID = null; //Image Shape ID
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String name = "N/A"; //Either "product" name or "keypad" name
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String type = "N/A"; //Determines if this MenuItem is a product ("product") or button to another menu ("keypad")
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String image = "N/A"; //Determines if this MenuItem has an image associated with it
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String iconPosition = "N/A"; //Determines the icon position of the image
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String description = "N/A"; //Determines the description
    BigDecimal price = null; //Only valid if the type is "product" - the price of the product
    Boolean shrinkImageToFit = false; //flag that determines if this Store allows pick up orders at all
    Boolean showNameOverlay = true; //flag that determines if this Store allows pick up orders at all
    Boolean showPriceOverlay = true; //flag that determines if this Store allows pick up orders at all
    Boolean healthy = false; //flag that determines if this item is healthy
    Boolean vegetarian = false; //flag that determines if this item is vegetarian
    Boolean vegan = false; //flag that determines if this item is vegan
    Boolean glutenFree = false; //flag that determines if this item is gluten free
    Boolean favorite = false; //flag that determines if this is a favorite product
    Boolean popular = false; //flag that determines if this is a favorite product
    Boolean autoShowModSet = false; //flag that determines if we should show the modifier keypad or not
    Boolean scaleUsed = false; //flag that determines if it is a weighted product or not
    Boolean priceBelowName = false; //flag that determines where the price will be on a menu
    Boolean fontIsBold = true; //flag that determines is the button text is bold or not
    ArrayList<HashMap> nutritionInfo = new ArrayList<>(); //Array of nutrition info for the product

    //Defect 4097: Added department and subdepartment ID data for purchase restriction purposes
    Integer departmentID = null;
    Integer subDeptID = null;

    //constructor - takes hashmap that get sets to this models properties
    public MenuItemModel(HashMap modelDetailHM) {
        try {
            setModelProperties(modelDetailHM);
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
    }

    //setter for all of this model's properties
    public MenuItemModel setModelProperties(HashMap modelDetailHM) {
        try {

            setID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));

            setMenuID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("MENUID")));

            setProductID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PRODUCTID")));

            setModSetDetailID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("MODSETDETAILID")));

            setName(CommonAPI.convertModelDetailToString(modelDetailHM.get("NAME")));

            setType(CommonAPI.convertModelDetailToString(modelDetailHM.get("TYPE")));

            setPrice(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("PRICE")));

            setImage(CommonAPI.convertModelDetailToString(modelDetailHM.get("IMAGE")));

            setIconPosition(CommonAPI.convertModelDetailToString(modelDetailHM.get("ICONPOSITION")));

            setShrinkImageToFit(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("SHRINKIMAGETOFIT")));

            setShowNameOverlay(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("SHOWNAMEOVERLAY")));

            setShowPriceOverlay(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("SHOWPRICEOVERLAY")));

            setHealthy(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("HEALTHY")));

            setVegetarian(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("VEGETARIAN")));

            setVegan(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("VEGAN")));

            setGlutenFree(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("GLUTENFREE")));

            setFavorite(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("FAVORITE")));

            setPopular(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("POPULAR")));

            setAutoShowModSet(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("AUTOSHOWMODSET")));

            setNutritionInfo(modelDetailHM, "NUTRITION1");
            setNutritionInfo(modelDetailHM, "NUTRITION2");
            setNutritionInfo(modelDetailHM, "NUTRITION3");
            setNutritionInfo(modelDetailHM, "NUTRITION4");
            setNutritionInfo(modelDetailHM, "NUTRITION5");

            setPrepOptionSetID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PREPOPTIONSETID")));

            setComboID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("COMBOID")));

            setDescription(CommonAPI.convertModelDetailToString(modelDetailHM.get("DESCRIPTION")));

            setScaleUsed(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("SCALEUSED")));

            setImageShapeID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("IMAGESHAPEID")));

            setPriceBelowName(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("MYQCPRICEBELOWNAME")));

            setFontIsBold(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("FONTISBOLD")));

            setDepartmentID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("DEPARTMENTID")));

            setSubDepartmentID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("SUBDEPARTMENTID")));

        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
        return this;
    }

    private void addNutritionInfo(String info) {
        HashMap nutrition = new HashMap();
        nutrition.put("nutritionInfo", info);
        nutritionInfo.add(nutrition);
    }

    //getter
    public ArrayList<HashMap> getNutritionInfo() {
        return nutritionInfo;
    }

    //setter
    private void setNutritionInfo(HashMap modelDetail, String name ) {
        String value = CommonAPI.convertModelDetailToString(modelDetail.get(name));
        // Only add string that aren't blank
        if(!value.equals("")) {
            addNutritionInfo(value);
        }
    }

    //getter
    public Integer getID() {
        return this.id;
    }

    //setter
    public void setID(Integer id) {
        this.id = id;
    }

    //getter
    public Integer getMenuID() {
        return this.menuID;
    }

    //setter
    public void setMenuID(Integer menuID) {
        this.menuID = menuID;
    }

    //getter
    public Integer getProductID() {
        return this.productID;
    }

    //setter
    public void setProductID(Integer productID) {
        this.productID = productID;
    }

    //getter
    public Integer getModSetDetailID() {
        return this.modSetDetailID;
    }

    //setter
    public void setModSetDetailID(Integer modSetDetailID) {
        this.modSetDetailID = modSetDetailID;
    }

    //getter
    public String getName() {
        return name;
    }

    //setter
    public void setName(String name) {
        this.name = name.replace("\\r", " ");
    }

    //getter
    public String getType() {
        return type;
    }

    //setter
    public void setType(String type) {
        this.type = type;
    }

    //getter
    public BigDecimal getPrice() {
        return price;
    }

    //setter
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    //getter
    public String getImage() {
        return image;
    }

    //setter
    public void setImage(String image) {
        this.image = image;
    }

    //getter
    public String getIconPosition() {
        return iconPosition;
    }

    //setter
    public void setIconPosition(String iconPosition) {
        this.iconPosition = iconPosition;
    }

    public Boolean getShrinkImageToFit() {
        return shrinkImageToFit;
    }

    public void setShrinkImageToFit(Boolean shrinkImageToFit) {
        this.shrinkImageToFit = shrinkImageToFit;
    }

    public Boolean getShowNameOverlay() {
        return showNameOverlay;
    }

    public void setShowNameOverlay(Boolean showNameOverlay) {
        this.showNameOverlay = showNameOverlay;
    }

    public Boolean getShowPriceOverlay() {
        return showPriceOverlay;
    }

    public void setShowPriceOverlay(Boolean showPriceOverlay) {
        this.showPriceOverlay = showPriceOverlay;
    }

    public Boolean getHealthy() {
        return healthy;
    }

    public void setHealthy(Boolean healthy) {
        this.healthy = healthy;
    }

    public Boolean getVegetarian() {
        return vegetarian;
    }

    public void setVegetarian(Boolean vegetarian) {
        this.vegetarian = vegetarian;
    }

    public Boolean getVegan() {
        return vegan;
    }

    public void setVegan(Boolean vegan) {
        this.vegan = vegan;
    }

    public Boolean getGlutenFree() {
        return glutenFree;
    }

    public void setGlutenFree(Boolean glutenFree) {
        this.glutenFree = glutenFree;
    }

    public Boolean getFavorite() {
        return favorite;
    }

    public void setFavorite(Boolean favorite) {
        this.favorite = favorite;
    }

    public Boolean getPopular() {
        return popular;
    }

    public void setPopular(Boolean popular) {
        this.popular = popular;
    }

    public Integer getPrepOptionSetID() {
        return prepOptionSetID;
    }

    public void setPrepOptionSetID(Integer prepOptionSetID) {
        this.prepOptionSetID = prepOptionSetID;
    }

    public Boolean getAutoShowModSet() {
        return autoShowModSet;
    }

    public void setAutoShowModSet(Boolean autoShowModSet) {
        this.autoShowModSet = autoShowModSet;
    }

    public Integer getComboID() {
        return comboID;
    }

    public void setComboID(Integer comboID) {
        this.comboID = comboID;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getScaleUsed() {
        return scaleUsed;
    }

    public void setScaleUsed(Boolean scaleUsed) {
        this.scaleUsed = scaleUsed;
    }

    public Integer getImageShapeID() {
        return imageShapeID;
    }

    public void setImageShapeID(Integer imageShapeID) {
        this.imageShapeID = imageShapeID;
    }

    public Boolean getPriceBelowName() {
        return priceBelowName;
    }

    public void setPriceBelowName(Boolean priceBelowName) {
        this.priceBelowName = priceBelowName;
    }

    public Boolean getFontIsBold() {
        return fontIsBold;
    }

    public void setFontIsBold(Boolean fontIsBold) {
        this.fontIsBold = fontIsBold;
    }

    public Integer getDepartmentID() { return departmentID; }
    public void setDepartmentID(Integer deptID) { this.departmentID = deptID; }

    public Integer getSubDepartmentID() { return subDeptID; }
    public void setSubDepartmentID(Integer subDeptID) { this.subDeptID = subDeptID; }
}
