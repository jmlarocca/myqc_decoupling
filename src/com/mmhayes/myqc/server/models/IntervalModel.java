package com.mmhayes.myqc.server.models;

//mmhayes dependencies

import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.dataaccess.DataManager;

import java.time.LocalTime;
import java.util.HashMap;

//myqc dependencies
//other dependencies

/* Interval Model
Last Updated (automatically updated by SVN)
$Author: gematuszyk $: Author of last commit
$Date: 2019-04-08 16:16:41 -0400 (Mon, 08 Apr 2019) $: Date of last commit
$Rev: 37702 $: Revision of last commit

Notes: Model for buffer intervals */
public class IntervalModel {
    private static DataManager dm = new DataManager();

    //interval related properties
    private String intervalStartTime = "";  //start time of the interval
    private String intervalEndTime = ""; //end time of the interval
    private LocalTime localStartTime = null;  //start time of the interval
    private LocalTime localEndTime = null; //end time of the interval

    //schedule related properties
    private Integer ID = null; //the ID of the interval
    private Integer scheduleID = null; //the ID of the schedule header record
    private Integer scheduleDetailID = null;  //the ID of the schedule detail record
    private String scheduleStartTime = null; //the time when the schedule starts
    private String scheduleEndTime = null; //the time when the schedule starts

    //buffer specific properties
    private Integer pickupTransactionCount = 0; //how many pickup transactions were placed in this interval
    private Integer deliveryTransactionCount = 0; //how many delivery transactions were placed in this interval
    private Integer transactionTotalCount = 0;  //how many pickup and delivery transaction were placed in this interval
    private boolean intervalOpen = true; //is the interval open
    private boolean pickupOpen = true;  //is buffer pickup schedule open
    private boolean deliveryOpen = true; //is buffer delivery schedule open

    //constructor - takes hashmap that get sets to this models properties
    public IntervalModel() throws Exception {

    }

    //constructor - takes hashmap that get sets to this models properties
    public IntervalModel(HashMap scheduleDetailHM) throws Exception {
        setModelProperties(scheduleDetailHM);
    }

    //setter for all of this model's properties
    public IntervalModel setModelProperties(HashMap modelDetailHM) throws Exception {
        setID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));
        setScheduleID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("SCHEDULEID")));
        setScheduleDetailID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("SCHEDULEDETAILID")));
        setScheduleStartTime(CommonAPI.convertModelDetailToString(modelDetailHM.get("SCHEDULESTART")));
        setScheduleEndTime(CommonAPI.convertModelDetailToString(modelDetailHM.get("SCHEDULEEND")));

        setIntervalStartTime(CommonAPI.convertModelDetailToString(modelDetailHM.get("INTERVALSTART")));
        setIntervalEndTime(CommonAPI.convertModelDetailToString(modelDetailHM.get("INTERVALEND")));
        setLocalStartTime(LocalTime.parse(getIntervalStartTime()));
        setLocalEndTime(LocalTime.parse(getIntervalEndTime()));

        setPickupTransactionCount(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("TRANSACTIONSPERINTERVALPICKUP")));
        setDeliveryTransactionCount(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("TRANSACTIONSPERINTERVALDELIVERY")));

        setTransactionTotalCount(getPickupTransactionCount() + getDeliveryTransactionCount());

        return this;
    }

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public Integer getScheduleID() {
        return scheduleID;
    }

    public void setScheduleID(Integer scheduleID) {
        this.scheduleID = scheduleID;
    }

    public Integer getScheduleDetailID() {
        return scheduleDetailID;
    }

    public void setScheduleDetailID(Integer scheduleDetailID) {
        this.scheduleDetailID = scheduleDetailID;
    }

    public String getIntervalStartTime() {
        return intervalStartTime;
    }

    public void setIntervalStartTime(String intervalStartTime) {
        this.intervalStartTime = intervalStartTime;
    }

    public String getIntervalEndTime() {
        return intervalEndTime;
    }

    public void setIntervalEndTime(String intervalEndTime) {
        this.intervalEndTime = intervalEndTime;
    }

    public LocalTime getLocalStartTime() {
        return localStartTime;
    }

    public void setLocalStartTime(LocalTime localStartTime) {
        this.localStartTime = localStartTime;

        setIntervalStartTime(localStartTime.toString());
    }

    public LocalTime getLocalEndTime() {
        return localEndTime;
    }

    public void setLocalEndTime(LocalTime localEndTime) {
        this.localEndTime = localEndTime;

        setIntervalEndTime(localEndTime.toString());
    }

    public Integer getPickupTransactionCount() {
        return pickupTransactionCount;
    }

    public void setPickupTransactionCount(Integer pickupTransactionCount) {
        this.pickupTransactionCount = pickupTransactionCount;
    }

    public Integer getDeliveryTransactionCount() {
        return deliveryTransactionCount;
    }

    public void setDeliveryTransactionCount(Integer deliveryTransactionCount) {
        this.deliveryTransactionCount = deliveryTransactionCount;
    }

    public boolean isIntervalOpen() {
        return intervalOpen;
    }

    public void setIntervalOpen(boolean intervalOpen) {
        this.intervalOpen = intervalOpen;
    }

    public boolean isDeliveryOpen() {
        return deliveryOpen;
    }

    public void setDeliveryOpen(boolean deliveryOpen) {
        this.deliveryOpen = deliveryOpen;
    }

    public boolean isPickupOpen() {
        return pickupOpen;
    }

    public void setPickupOpen(boolean pickupOpen) {
        this.pickupOpen = pickupOpen;
    }

    public Integer getTransactionTotalCount() {
        return transactionTotalCount;
    }

    public void setTransactionTotalCount(Integer transactionTotalCount) {
        this.transactionTotalCount = transactionTotalCount;
    }


    public String getScheduleEndTime() {
        return scheduleEndTime;
    }

    public void setScheduleEndTime(String scheduleEndTime) {
        this.scheduleEndTime = scheduleEndTime;
    }

    public String getScheduleStartTime() {
        return scheduleStartTime;
    }

    public void setScheduleStartTime(String scheduleStartTime) {
        this.scheduleStartTime = scheduleStartTime;
    }

}
