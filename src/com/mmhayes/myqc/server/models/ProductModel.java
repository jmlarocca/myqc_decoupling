package com.mmhayes.myqc.server.models;

//mmhayes dependencies
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mmhayes.common.api.*;
import com.mmhayes.common.dataaccess.*;
import com.mmhayes.common.disclaimer.collections.DisclaimerCollection;
import com.mmhayes.common.disclaimer.models.DisclaimerModel;
import com.mmhayes.common.utils.*;
import com.mmhayes.common.api.json.serializers.*;

//javax.ws.rs dependencies
import com.fasterxml.jackson.databind.annotation.*;

//myqc dependencies
import com.mmhayes.myqc.server.api.MyQCCache;
import com.mmhayes.myqc.server.models.*;
import com.mmhayes.myqc.server.collections.*;

//other dependencies
import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.math.BigDecimal;

/* Product Model
 Last Updated (automatically updated by SVN)
 $Author: ijgerstein $: Author of last commit
 $Date: 2021-09-10 11:50:40 -0400 (Fri, 10 Sep 2021) $: Date of last commit
 $Rev: 57971 $: Revision of last commit

 Notes: Model for products (a.k.a PLUs, Items, etc.)
*/
public class ProductModel {
    Integer id = null; //Product ID (PAPluID)
    Integer modifierSetID = null; //Product Modifier Set ID (PAPluID)
    Integer prepOptionSetID = null; //ID of the prepOptionSet assigned to the product
    Integer subDeptID = null; //ID of the product's subdepartment
    Integer departmentID = null; //ID of the product's department
    Integer complementaryKeypadID = null; //ID of the complementary suggestive keypad associated with the product
    Integer subDeptComplementaryKeypadID = null; //ID of the complementary suggestive keypad associated with the sub department
    Integer subDeptSimilarKeypadID = null; //ID of the similar suggestive keypad associated with the sub department
    Integer keypadID = null;
    Integer modSetDetailID = null; //ID of the FIRST "placeholder" in the "Modifier Menu" - PAModifierSetDetailID off of the QC_PAModifierSetDetail table

    BigDecimal price = null; //Price of the product
    BigDecimal originalPrice = null; //Price of the product
    BigDecimal nutritionInfo1 = null; //Product nutrition info 1
    BigDecimal nutritionInfo2 = null; //Product nutrition info 2
    BigDecimal nutritionInfo3 = null; //Product nutrition info 3
    BigDecimal nutritionInfo4 = null; //Product nutrition info 4
    BigDecimal nutritionInfo5 = null; //Product nutrition info 5
    BigDecimal fixedTareWeight = null; //Fixed Tare Weight for product
    BigDecimal quantity = null; //Product quantity for order
    BigDecimal invCurrent = null;
    BigDecimal taxTotal = null; //Total tax applied on the parent product (for purchase restriction calculations)

    Boolean productInStore = true; //flag that determines if the product is on a keypad in the store
    Boolean scaleUsed = false; //flag that determines if this item is weighted
    Boolean shrinkImageToFit = false; //flag that determines if this image should contain or cover
    Boolean healthy = false; //flag that determines if this item is healthy
    Boolean vegetarian = false; //flag that determines if this item is vegetarian
    Boolean vegan = false; //flag that determines if this item is vegan
    Boolean glutenFree = false; //flag that determines if this item is gluten free
    Boolean hasNutrition = false;
    Boolean priceOpen = false; // flag that determines open pricing

    boolean isInventoryItem = false; //keep track of if invItem - for submitTransaction
    Boolean diningOptionProduct = false; //flag that determines if this item is gluten free
    Boolean hasPrinterMappings = true; //flag that determines if this item has Kitchen Printer Mappings
    Boolean autoShowModSet = false; //flag that determines if we will being them to modifier menu or not

    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String name = ""; //Name of the product
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String description = ""; //Description of the product
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String image = ""; //for setting the image in the button view
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String productImage = ""; //for setting the image in the product view
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String taxIDs = null; //comma separated list of mapped tax IDs
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String discountIDs = null; //comma separated list of mapped discount IDs
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String tareName = ""; // Tare assigned to product
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String message = ""; //for reordering or general error messaging to front end

    PrepOptionModel prepOption = null; //model that holds prep option details for products
    List<ModifierModel> modifiers = null; //this Array List represents a ModifierCollection which contains a list of ModifierModels
    List<ModMenuModel> modifierMenus = new ArrayList<>(); //this Array List represents a ModifierCollection which contains a list of ModifierModels
    HashMap<Integer, List<PrepOptionModel>> loadedPrepOptions = new HashMap<Integer, List<PrepOptionModel>>();
    List<DisclaimerModel> disclaimers = new ArrayList<>();

    private Integer transLineItemID = null;
    private Integer comboTransLineItemId = null;
    private Integer comboDetailId = null;
    private Boolean comboActive = false;
    private String comboName = null;
    private BigDecimal basePrice = null;
    private BigDecimal comboPrice = null;
    private BigDecimal upcharge = null;

    //caching related properties
    private LocalDateTime lastUpdateDTM = null; //when the product was last updated
    private LocalDateTime lastUpdateCheck = null; //when the product was last checked to see if it has been updated

    private static DataManager dm = new DataManager();
    private commonMMHFunctions commFunc = new commonMMHFunctions();
    private Integer authenticatedAccountID = null;

    //dummy constructor - needed to consume ANY JSON object via REST (Jersey/Jackson style)
    public ProductModel() {

    }

    //constructor - takes hashmap that get sets to this models properties
    public ProductModel(HashMap modelDetailHM) throws Exception {
        setModelProperties(modelDetailHM);
    }

    //constructor - takes productID and retrieves a ProductModel
    public ProductModel(Integer productID) throws Exception {
        getAndSetProductDetailsWithoutStore(productID);

        MyQCCache.addProductToCache( this );
    }

    //constructor - takes productID and storeID and retrieves a ProductModel
    public ProductModel(Integer productID, Integer storeID) throws Exception {
        getAndSetProductDetails(productID, storeID);

        MyQCCache.addProductToCache( this );
    }

    //constructor - takes productID and storeID and retrieves a ProductModel, optionally can choose to not cache product.
    public ProductModel(Integer productID, Integer storeID, Boolean addToCache) throws Exception{
        getAndSetProductDetails(productID, storeID);

        if(addToCache){
            MyQCCache.addProductToCache( this );
        }
    }

    //setter for all of this model's properties
    public ProductModel setModelProperties(HashMap modelDetailHM) throws Exception{
        setID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));
        setModifierSetID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("MODIFIERSETID")));
        setPrepOptionSetID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAPREPOPTIONSETID")));
        setComplementaryKeypadID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PRODCOMPLEMENTARYKEYPPADID")));
        setSubDeptComplementaryKeypadID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("SUBCOMPLEMENTARYKEYPADID")));
        setSubDeptSimilarKeypadID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("SUBSIMILARKEYPADID")));
        setSubDeptID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PASUBDEPTID")));
        setDepartmentID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PADEPARTMENTID")));
        setModSetDetailID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("MODSETDETAILID")));

        setPrice(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("PRICE")));
        setOriginalPrice(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("ORIGINALPRICE")));
        setQuantity(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("QUANTITY")));
        setInvCurrent(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("INVCURRENT")));
        setFixedTareWeight(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("FIXEDTAREWEIGHT")));
        setNutritionInfo1(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("NUTRITIONINFO1")));
        setNutritionInfo2(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("NUTRITIONINFO2")));
        setNutritionInfo3(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("NUTRITIONINFO3")));
        setNutritionInfo4(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("NUTRITIONINFO4")));
        setNutritionInfo5(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("NUTRITIONINFO5")));

        setName(CommonAPI.convertModelDetailToString(modelDetailHM.get("NAME")));
        setDescription(CommonAPI.convertModelDetailToString(modelDetailHM.get("DESCRIPTION")));
        setImage(CommonAPI.convertModelDetailToString(modelDetailHM.get("IMAGE")));
        setProductImage(CommonAPI.convertModelDetailToString(modelDetailHM.get("PRODUCTIMAGE")));
        setTaxIDs(CommonAPI.convertModelDetailToString(modelDetailHM.get("TAXIDS")));
        setDiscountIDs(CommonAPI.convertModelDetailToString(modelDetailHM.get("DISCOUNTIDS")));
        setTareName(CommonAPI.convertModelDetailToString(modelDetailHM.get("TARENAME")));

        setShrinkImageToFit(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("SHRINKIMAGETOFIT")));
        setHasNutrition(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("HASNUTRITION")));
        setHealthy(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("HEALTHY")));
        setVegetarian(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("VEGETARIAN")));
        setVegan(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("VEGAN")));
        setGlutenFree(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("GLUTENFREE")));
        setScaleUsed(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("SCALEUSED")));
        setAutoShowModSet(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("AUTOSHOWMODSET")));
        setIsInventoryItem(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("INVENTORYITEM"), false));
        setHasPrinterMappings(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("HASPRINTERMAPPINGS"), true));

        if(getNutritionInfo1() != null || getNutritionInfo2() != null || getNutritionInfo3() != null || getNutritionInfo4() != null || getNutritionInfo5() != null) {
            setHasNutrition(true);
        }

        if(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("PRICEOPEN"))) {
            setPriceOpen(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("PRICEOPEN")));
        } else {
            setPriceOpen(false);
        }

        //specify in and out formats for formatting modelDetailHM.get("LASTUPDATEDTM")
        String dateTimeInFormat = "yyyy-MM-dd HH:mm:ss.S";
        String dateTimeOutFormat = "M/dd/yyyy h:mm:ss a"; //old way was - M/dd/yyyy H:mm:ss - jrmitaly 6/23/2015

        //format modelDetailHM.get("TRANSDATE") and set to dateTimeStr
        String dateTimeStr = commFunc.formatDateTime(modelDetailHM.get("LASTUPDATEDTM").toString(),dateTimeInFormat,dateTimeOutFormat);

        //parse dateTimeStr to LocalDateTime and set that to this.dateTime
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(dateTimeOutFormat);
        setLastUpdateDTM( LocalDateTime.parse(dateTimeStr, dateTimeFormatter) );

        setLastUpdateCheck( LocalDateTime.now() );

        return this;
    }

    public ProductModel(ProductModel existingModel) throws Exception {
        setID(existingModel.getID());
        setQuantity(existingModel.getQuantity());
        setName(existingModel.getName());
        setTaxIDs(existingModel.getTaxIDs());
        setDiscountIDs(existingModel.getDiscountIDs());
        setDescription(existingModel.getDescription());
        setPrice(existingModel.getPrice());
        setOriginalPrice(existingModel.getOriginalPrice());
        setModSetDetailID(existingModel.getModSetDetailID());
        setIsInventoryItem(existingModel.getIsInventoryItem());
        setInvCurrent(existingModel.getInvCurrent());
        setHealthy(existingModel.getHealthy());
        setVegetarian(existingModel.getVegetarian());
        setVegan(existingModel.getVegan());
        setGlutenFree(existingModel.getGlutenFree());
        setScaleUsed(existingModel.getScaleUsed());
        setFixedTareWeight(existingModel.getFixedTareWeight());
        setTareName(existingModel.getTareName());
    }

    public ProductModel setModelPropertiesNotCached(HashMap modelDetailHM) throws Exception {
        setModifierSetID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("MODIFIERSETID")));
        setDepartmentID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PADEPARTMENTID")));
        setDepartmentID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PADEPARTMENTID")));
        setSubDeptComplementaryKeypadID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("SUBCOMPLEMENTARYKEYPADID")));
        setSubDeptSimilarKeypadID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("SUBSIMILARKEYPADID")));
        setFixedTareWeight(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("FIXEDTAREWEIGHT")));
        setTareName(CommonAPI.convertModelDetailToString(modelDetailHM.get("TARENAME")));
        setHasPrinterMappings(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("HASPRINTERMAPPINGS"), true));
        setInvCurrent(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("INVCURRENT")));

        //specify in and out formats for formatting modelDetailHM.get("LASTUPDATEDTM")
        String dateTimeInFormat = "yyyy-MM-dd HH:mm:ss.S";
        String dateTimeOutFormat = "M/dd/yyyy h:mm:ss a"; //old way was - M/dd/yyyy H:mm:ss - jrmitaly 6/23/2015

        //format modelDetailHM.get("TRANSDATE") and set to dateTimeStr
        String dateTimeStr = commFunc.formatDateTime(modelDetailHM.get("LASTUPDATEDTM").toString(),dateTimeInFormat,dateTimeOutFormat);

        //parse dateTimeStr to LocalDateTime and set that to this.dateTime
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(dateTimeOutFormat);
        setLastUpdateDTM( LocalDateTime.parse(dateTimeStr, dateTimeFormatter) );

        return this;
    }

    public static ProductModel getProductModel( Integer productID, HttpServletRequest request ) throws Exception {

        //retrieve the storeID (MyQCTerminalID) out of the request
        Integer storeID = CommonAPI.checkRequestHeaderForStoreID(request);

        // /if product is in cache, load from cache
        if ( MyQCCache.checkCacheForProduct(productID) ) {
            ProductModel productModel = MyQCCache.getProductFromCache(productID);
            return getProductDetailsNotCached(productID, storeID, productModel);
        }

        return new ProductModel( productID, storeID );
    }

    public static ProductModel getProductModel( Integer productID, Integer storeID ) throws Exception {
        // /if product is in cache, load from cache
        if ( MyQCCache.checkCacheForProduct(productID) ) {
            ProductModel productModel = MyQCCache.getProductFromCache(productID);
            return getProductDetailsNotCached(productID, storeID, productModel);
        }

        return new ProductModel( productID, storeID );
    }

    public static ProductModel getProductModel( Integer productID ) throws Exception {

        return new ProductModel( productID );
    }

    private void getAndSetProductDetails(Integer productID, Integer storeID) throws Exception {
        //get all models in an array list
        ArrayList<HashMap> productList = dm.parameterizedExecuteQuery("data.product.getProduct",
                new Object[]{
                        productID,
                        storeID
                },
                true
        );

        //populate this collection from an array list of hashmaps
        if (productList != null && productList.size() == 1) {
            HashMap modelDetailHM = productList.get(0);
            if (modelDetailHM.get("ID") != null && !modelDetailHM.get("ID").toString().isEmpty()) {
                setModelProperties(modelDetailHM);

                getProductModifierDetails(storeID);
                getAndSetDisclaimerDetails(productID);
            } else {
                Logger.logMessage("ERROR: Could not determine productID in ProductModel(productID)", Logger.LEVEL.ERROR);
            }


        } else {
            this.setID(0);
        }
    }

    public static ProductModel getProductDetailsNotCached(Integer productID, Integer storeID, ProductModel productModel) throws Exception {
        //get all models in an array list
        ArrayList<HashMap> productList = dm.parameterizedExecuteQuery("data.product.getProductDetailsNotCached",
                new Object[]{
                        productID,
                        storeID
                },
                true
        );

        //populate this collection from an array list of hashmaps
        if (productList != null && productList.size() == 1) {
            HashMap modelDetailHM = productList.get(0);
            if (modelDetailHM.get("ID") != null && !modelDetailHM.get("ID").toString().isEmpty()) {
                productModel.setModelPropertiesNotCached(modelDetailHM);

                productModel.getProductModifierDetails(storeID);
                productModel.getAndSetDisclaimerDetails(productID);
            } else {
                Logger.logMessage("ERROR: Could not determine productID in ProductModel(productID)", Logger.LEVEL.ERROR);
            }


        } else {
            productModel.setID(0);
        }

        return productModel;
    }

    private void getAndSetProductDetailsWithoutStore(Integer productID) throws Exception {
        //get all models in an array list
        ArrayList<HashMap> productList = dm.parameterizedExecuteQuery("data.product.getProductWithoutStore",
                new Object[]{
                        productID
                },
                true
        );

        //populate this collection from an array list of hashmaps
        if (productList != null && productList.size() == 1) {
            HashMap modelDetailHM = productList.get(0);
            if (modelDetailHM.get("ID") != null && !modelDetailHM.get("ID").toString().isEmpty()) {
                setModelProperties(modelDetailHM);
            } else {
                Logger.logMessage("ERROR: Could not determine productID in ProductModel(productID)", Logger.LEVEL.ERROR);
            }
        }
    }

    //updates prices of all modifiers by creating a collection then updates price of product based on modifiers
    public void updatePricesBasedOnModifiers(ArrayList<ModifierModel> passedInProductModifiers, HttpServletRequest request) {
        try {

            //updates prices of all modifiers by creating a new collection and setting it to this model
            Integer modifierSetID = this.getModifierSetID();
            ModifierCollection modifierCollection = new ModifierCollection(passedInProductModifiers, request, modifierSetID);
            setModifiers(modifierCollection.getCollection());

            //updates the price of the product by iterating through all the modifiers and adjusting the price accordingly
            updateProductPriceBasedOnModifiers();

        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
    }

    //updates the price of the product by iterating through all the modifiers and adjusting the price accordingly
    public void updateProductPriceBasedOnModifiers() {
        try {

            for (ModifierModel modifierModel : getModifiers()) {
                if (modifierModel.getID() != null && !modifierModel.getID().toString().isEmpty()) {

                    //if this modifier really has a price
                    if (modifierModel.getPrice() != null && !modifierModel.getID().toString().isEmpty()) {
                        //expanded out to show what is going on... valid one liner... this.getPrice().add(modifierModel.getPrice()
                        BigDecimal currentProductPrice = this.getPrice();
                        BigDecimal newProductPrice = currentProductPrice.add(modifierModel.getPrice());

                        //if there is a prep option set on the modifier model, add the prep option price to total product price
                        if(modifierModel.getPrepOption() != null) {
                            if(modifierModel.getPrepOption().getPrice().compareTo(BigDecimal.ZERO) > 0) {
                                newProductPrice = newProductPrice.add(modifierModel.getPrepOption().getPrice());
                            }
                        }

                        setPrice(newProductPrice);
                    }

                    if(modifierModel != null && modifierModel.getHasNutrition() && !getHasNutrition()) {
                        setHasNutrition(true);
                    }

                } else {
                    Logger.logMessage("ERROR: Could not determine Modifier Model in ProductModel.updateProductPriceBasedOnModifiers()", Logger.LEVEL.ERROR);
                }
            }

        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
    }

    //constructor - takes productID and retrieves a ProductModel
    public ProductModel(String PLUCode, HttpServletRequest request) throws Exception {
        //determine the authenticated account ID from the request
        setAuthenticatedAccountID(CommonAuthResource.checkRequestHeaderForMMHayesAuth(request));

        if (getAuthenticatedAccountID() != null) {
            //retrieve the storeID (MyQCTerminalID) out of the request
            Integer storeID = CommonAPI.checkRequestHeaderForStoreID(request);

            //get all models in an array list
            ArrayList<HashMap> productList = dm.parameterizedExecuteQuery("data.ordering.getProductByPLUCode",
                    new Object[]{
                            PLUCode,
                            storeID
                    },
                    true
            );

            //populate this collection from an array list of hashmaps
            if (productList != null && productList.size() == 1) {
                HashMap modelDetailHM = productList.get(0);
                if (modelDetailHM.get("ID") != null && !modelDetailHM.get("ID").toString().isEmpty()) {
                    setModelProperties(modelDetailHM);
                    Integer productID = CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID"));

                    getProductModifierDetails(storeID);
                    getAndSetDisclaimerDetails(productID);
                } else {
                    Logger.logMessage("ERROR: Could not determine productID in ProductModel(productID)", Logger.LEVEL.ERROR);
                }


            } else {
                this.setID(0);
            }
        } else {
            Logger.logMessage("ERROR: Could not determine authenticated accountID in ProductModel(plucode)", Logger.LEVEL.ERROR);
        }
    }

    private void determineIfProductInStore(Integer storeID) throws Exception {
        StoreModel storeModel = StoreModel.getStoreModel(storeID, getAuthenticatedAccountID());

        StoreKeypadModel storeKeypadModel = new StoreKeypadModel();

        //if store keypad model is not in cache or is not up-to-date
        if ( MyQCCache.checkCacheForStoreKeypad(storeID)) {
            storeKeypadModel = MyQCCache.getStoreKeypadFromCache( storeID );
        } else {
            storeKeypadModel = new StoreKeypadModel( storeModel );
        }

        ArrayList<String> productsInStore = storeKeypadModel.getProductsInStoreWithDiningOptionsList();

        boolean productFound = false;
        for(String productID : productsInStore) {
            if(productID.equals(this.getID().toString())) {
                productFound = true;
                break;
            }
        }

        if(!productFound) {
            setProductInStore(false);
        }
    }

    /** PRODUCT MODEL NEW FUNCTIONS **/

    private void getAndSetDisclaimerDetails(Integer productID){
        try{
            DisclaimerCollection productDisclaimers = new DisclaimerCollection();
            DisclaimerCollection disclaimers = new DisclaimerCollection();

            productDisclaimers.getAllDisclaimersByProductId(productID);

            //Add the product disclaimers
            for (DisclaimerModel productDisclaimer : productDisclaimers.getCollection()) {
                disclaimers.getCollection().add(productDisclaimer);
            }


            if(disclaimers.getCollection().size() > 0){
                setDisclaimers(disclaimers.getCollection());
            }
        }
        catch(Exception e){
            Logger.logException(e);
        }
    }

    // Get the Modifier Menu and Modifier Models for the Product
    private void getProductModifierDetails(Integer storeID) throws Exception {

        if(getModifierSetID() == null) {
            return;
        }

        //get all models in an array list
        ArrayList<HashMap> modifierMenuList = dm.parameterizedExecuteQuery("data.product.getModifierDetails",
                new Object[]{
                        getModifierSetID(),
                        storeID
                },
                true
        );

        //populate this collection from an array list of hashmaps
        if (modifierMenuList != null && modifierMenuList.size() > 0) {
            populateModifierMenus(modifierMenuList);
        }
    }

    public void populateModifierMenus(ArrayList<HashMap> modifierMenuList) throws Exception {
        Integer currentModKeypadID = null;
        ModMenuModel currentModMenuModel = null;
        List<Integer> modifierMenuIDs = new ArrayList<>();

        for(HashMap modifierMenuHM : modifierMenuList) {
            Integer modKeypadID = CommonAPI.convertModelDetailToInteger(modifierMenuHM.get("ID"));
            modifierMenuIDs.add(modKeypadID);

            modifierMenuHM.put("KEYPADID", modKeypadID);
            // If new Modifier Menu Model
            if(currentModKeypadID == null || !currentModKeypadID.equals(modKeypadID)) {
                if(currentModMenuModel != null) {
                    getModifierMenus().add(currentModMenuModel);
                }

                // Create new Modifier Menu Model
                ModMenuModel modMenuModel = new ModMenuModel(modifierMenuHM);

                ModifierModel modifierModel = new ModifierModel(modifierMenuHM);
                setModifierPrepOption(modifierModel);

                // Add to Modifier Menu Model
                modMenuModel.getModifiers().add(modifierModel);

                // Update current Modifier Menu Model in progress
                currentModKeypadID = modKeypadID;
                currentModMenuModel = modMenuModel;

                // If this modifier is part of the current Modifier Menu Model
            } else {

                ModifierModel modifierModel = new ModifierModel(modifierMenuHM);
                setModifierPrepOption(modifierModel);

                // Add to Modifier Menu Model
                currentModMenuModel.getModifiers().add(modifierModel);
            }
        }

        // Add Modifier Menus to product
        getModifierMenus().add(currentModMenuModel);

        // Remove any modifier menus no longer set
        for(int i = 0; i < this.getModifierMenus().size(); i++){
            Integer modifierMenuID = this.getModifierMenus().get(i).getID();

            if(!modifierMenuIDs.contains(modifierMenuID)){
                getModifierMenus().remove(i);
            }
        }
    }

    // Set prep options on modifier model
    public void setModifierPrepOption(ModifierModel modifierModel) throws Exception {
        if(modifierModel.getPrepOptionSetID() == null) {
            return;
        }

        List<PrepOptionModel> prepOptions = getLoadedPrepOptions(modifierModel.getPrepOptionSetID());
        modifierModel.setPrepOptions(prepOptions);
    }

    // Check if this prep option set has already been loaded
    public List<PrepOptionModel> getLoadedPrepOptions(Integer prepOptionSetID) throws Exception {

        if(getLoadedPrepOptions().containsKey(prepOptionSetID)) {
            return getLoadedPrepOptions().get(prepOptionSetID);
        }

        PrepOptionCollection prepOptionCollection = new PrepOptionCollection(prepOptionSetID);

        HashMap<Integer, List<PrepOptionModel>> loadedPrepOptions = new HashMap<Integer, List<PrepOptionModel>>();
        loadedPrepOptions.put(prepOptionSetID, prepOptionCollection.getCollection());
        setLoadedPrepOptions(loadedPrepOptions);

        return prepOptionCollection.getCollection();
    }

    //checks against the value of LastUpdateDTM to see if the model has been updated since last being cached
    public boolean checkForUpdates() throws Exception {
        boolean needsUpdate = true;

        Object result = dm.parameterizedExecuteScalar("data.product.checkProductForUpdates",
                new Object[]{
                        getID(),
                        getLastUpdateDTM().plusSeconds(1).toLocalTime().toString()
                }
        );

        //schedule needs to be updated if there is no lastUpdateDTM or if the result is 1
        needsUpdate = ( result == null || result.toString().equals("1") );

        return needsUpdate;
    }

    //getter
    private Integer getAuthenticatedAccountID() {
        return this.authenticatedAccountID;
    }

    //setter
    private void setAuthenticatedAccountID(Integer authenticatedAccountID) {
        this.authenticatedAccountID = authenticatedAccountID;
    }

    //getter
    public Integer getID() {
        return this.id;
    }

    //setter
    public void setID(Integer id) {
        this.id = id;
    }

    //getter
    public BigDecimal getQuantity() {
        return this.quantity;
    }

    //setter
    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    //getter
    public BigDecimal getTaxTotal() {
        return this.taxTotal;
    }

    //setter
    public void setTaxTotal(BigDecimal taxTotal) {
        this.taxTotal = taxTotal;
    }

    //getter
    public String getTaxIDs() {
        return taxIDs;
    }

    //setter
    public void setTaxIDs(String taxIDs) {
        if ( taxIDs == null ) {
            this.taxIDs = null;
            return;
        }

        LinkedHashSet taxes = new LinkedHashSet(Arrays.asList(taxIDs.replaceAll("\\s", "").split(",")));

        this.taxIDs = taxes.toString().replaceAll("\\[|\\]","");
    }

    //getter
    public String getDiscountIDs() {
        return discountIDs;
    }

    //setter
    public void setDiscountIDs(String discountIDs) {
        if ( discountIDs == null ) {
            this.discountIDs = null;
            return;
        }

        LinkedHashSet discounts = new LinkedHashSet(Arrays.asList(discountIDs.replaceAll("\\s", "").split(",")));

        this.discountIDs = discounts.toString().replaceAll("\\[|\\]","");
    }

    //getter
    public String getName() {
        return name;
    }

    //setter
    public void setName(String name) {
        this.name = name.replace("\\r", " ");
    }

    //getter
    public String getDescription() {
        return description;
    }

    //setter
    public void setDescription(String description) {
        this.description = description;
    }

    //getter
    public boolean getIsInventoryItem() {
        return isInventoryItem;
    }

    //setter
    public void setIsInventoryItem(boolean isInventoryItem) {
        this.isInventoryItem = isInventoryItem;
    }

    //getter
    public BigDecimal getPrice() {
        return price;
    }

    //setter
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    //getter
    public BigDecimal getOriginalPrice() {
        return originalPrice;
    }

    //setter
    public void setOriginalPrice(BigDecimal originalPrice) {
        this.originalPrice = originalPrice;
    }

    // getter
    public boolean getPriceOpen() { return this.priceOpen; }

    //setter
    private void setPriceOpen(boolean priceOpen) { this.priceOpen = priceOpen; }

    //getter
    public Integer getModSetDetailID() {
        return this.modSetDetailID;
    }

    //setter
    public void setModSetDetailID(Integer modSetDetailID) {
        this.modSetDetailID = modSetDetailID;
    }

    //getter
    public List<ModifierModel> getModifiers() {
        return modifiers;
    }

    //setter
    public void setModifiers(List<ModifierModel> modifiers) {
        this.modifiers = modifiers;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public BigDecimal getInvCurrent() {
        return invCurrent;
    }

    public void setInvCurrent(BigDecimal invCurrent) {
        this.invCurrent = invCurrent;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getProductImage() { return productImage; }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public Boolean getShrinkImageToFit() {
        return shrinkImageToFit;
    }

    public void setShrinkImageToFit(Boolean shrinkImageToFit) {
        this.shrinkImageToFit = shrinkImageToFit;
    }

    public Boolean getHasNutrition() {
        return hasNutrition;
    }

    public void setHasNutrition(Boolean hasNutrition) {
        this.hasNutrition = hasNutrition;
    }

    public Integer getKeypadID() {
        return keypadID;
    }

    public void setKeypadID(Integer keypadID) {
        this.keypadID = keypadID;
    }

    public Boolean getHealthy() {
        return healthy;
    }

    public void setHealthy(Boolean healthy) {
        this.healthy = healthy;
    }

    public Boolean getVegetarian() {
        return vegetarian;
    }

    public void setVegetarian(Boolean vegetarian) {
        this.vegetarian = vegetarian;
    }

    public Boolean getVegan() {
        return vegan;
    }

    public void setVegan(Boolean vegan) {
        this.vegan = vegan;
    }

    public Boolean getGlutenFree() {
        return glutenFree;
    }

    public void setGlutenFree(Boolean glutenFree) {
        this.glutenFree = glutenFree;
    }

    public Boolean getDiningOptionProduct() {
        return diningOptionProduct;
    }

    public void setDiningOptionProduct(Boolean diningOptionProduct) {
        this.diningOptionProduct = diningOptionProduct;
    }

    public Boolean getScaleUsed() {
        return scaleUsed;
    }

    public void setScaleUsed(Boolean scaleUsed) {
        this.scaleUsed = scaleUsed;
    }

    public PrepOptionModel getPrepOption() {
        return prepOption;
    }

    public void setPrepOption(PrepOptionModel prepOption) {
        this.prepOption = prepOption;
    }

    public Boolean getAutoShowModSet() {
        return autoShowModSet;
    }

    public void setAutoShowModSet(Boolean autoShowModSet) {
        this.autoShowModSet = autoShowModSet;
    }

    @JsonIgnore
    public Integer getTransLineItemID() {
        return transLineItemID;
    }

    public void setTransLineItemID(Integer transLineItemID) {
        this.transLineItemID = transLineItemID;
    }

    public Integer getPrepOptionSetID() {
        return prepOptionSetID;
    }

    public void setPrepOptionSetID(Integer prepOptionSetID) {
        this.prepOptionSetID = prepOptionSetID;
    }

    public Integer getComplementaryKeypadID() {
        return complementaryKeypadID;
    }

    public void setComplementaryKeypadID(Integer complementaryKeypadID) {
        this.complementaryKeypadID = complementaryKeypadID;
    }

    public Integer getSubDeptComplementaryKeypadID() {
        return subDeptComplementaryKeypadID;
    }

    public void setSubDeptComplementaryKeypadID(Integer subDeptComplementaryKeypadID) {
        this.subDeptComplementaryKeypadID = subDeptComplementaryKeypadID;
    }

    public Integer getSubDeptSimilarKeypadID() {
        return subDeptSimilarKeypadID;
    }

    public void setSubDeptSimilarKeypadID(Integer subDeptSimilarKeypadID) {
        this.subDeptSimilarKeypadID = subDeptSimilarKeypadID;
    }

    public Integer getComboTransLineItemId() {
        return comboTransLineItemId;
    }

    public void setComboTransLineItemId(Integer comboTransLineItemId) {
        this.comboTransLineItemId = comboTransLineItemId;
    }

    public Integer getComboDetailId() {
        return comboDetailId;
    }

    public void setComboDetailId(Integer comboDetailId) {
        this.comboDetailId = comboDetailId;
    }

    public BigDecimal getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(BigDecimal basePrice) {
        this.basePrice = basePrice;
    }

    public BigDecimal getComboPrice() {
        return comboPrice;
    }

    public void setComboPrice(BigDecimal comboPrice) {
        this.comboPrice = comboPrice;
    }

    public BigDecimal getUpcharge() {
        return upcharge;
    }

    public void setUpcharge(BigDecimal upcharge) {
        this.upcharge = upcharge;
    }

    public Integer getSubDeptID() {
        return subDeptID;
    }

    public void setSubDeptID(Integer subDeptID) {
        this.subDeptID = subDeptID;
    }

    public Integer getDepartmentID() {
        return departmentID;
    }

    public void setDepartmentID(Integer departmentID) {
        this.departmentID = departmentID;
    }

    public BigDecimal getFixedTareWeight(){ return this.fixedTareWeight; }

    public void setFixedTareWeight(BigDecimal fixedTareWeight){ this.fixedTareWeight = fixedTareWeight; }

    public String getTareName(){ return this.tareName; }

    public void setTareName(String tareName){ this.tareName = tareName; }

    public Boolean getHasPrinterMappings() {
        return hasPrinterMappings;
    }

    public void setHasPrinterMappings(Boolean hasPrinterMappings) {
        this.hasPrinterMappings = hasPrinterMappings;
    }

    public Boolean getProductInStore() {
        return productInStore;
    }

    public void setProductInStore(Boolean productInStore) {
        this.productInStore = productInStore;
    }

    public LocalDateTime getLastUpdateDTM() {
        return lastUpdateDTM;
    }

    public void setLastUpdateDTM(LocalDateTime lastUpdateDTM) {
        this.lastUpdateDTM = lastUpdateDTM;
    }

    public LocalDateTime getLastUpdateCheck() {
        return lastUpdateCheck;
    }

    public void setLastUpdateCheck(LocalDateTime lastUpdateCheck) {
        this.lastUpdateCheck = lastUpdateCheck;
    }

    public BigDecimal getNutritionInfo1() {
        return nutritionInfo1;
    }

    public void setNutritionInfo1(BigDecimal nutritionInfo1) {
        this.nutritionInfo1 = nutritionInfo1;
    }

    public BigDecimal getNutritionInfo2() {
        return nutritionInfo2;
    }

    public void setNutritionInfo2(BigDecimal nutritionInfo2) {
        this.nutritionInfo2 = nutritionInfo2;
    }

    public BigDecimal getNutritionInfo3() {
        return nutritionInfo3;
    }

    public void setNutritionInfo3(BigDecimal nutritionInfo3) {
        this.nutritionInfo3 = nutritionInfo3;
    }

    public BigDecimal getNutritionInfo4() {
        return nutritionInfo4;
    }

    public void setNutritionInfo4(BigDecimal nutritionInfo4) {
        this.nutritionInfo4 = nutritionInfo4;
    }

    public BigDecimal getNutritionInfo5() {
        return nutritionInfo5;
    }

    public void setNutritionInfo5(BigDecimal nutritionInfo5) {
        this.nutritionInfo5 = nutritionInfo5;
    }

    public Integer getModifierSetID() {
        return modifierSetID;
    }

    public void setModifierSetID(Integer modifierSetID) {
        this.modifierSetID = modifierSetID;
    }

    public List<ModMenuModel> getModifierMenus() {
        return modifierMenus;
    }

    public void setModifierMenus(List<ModMenuModel> modifierMenus) {
        this.modifierMenus = modifierMenus;
    }

    @JsonIgnore
    public HashMap<Integer, List<PrepOptionModel>> getLoadedPrepOptions() {
        return loadedPrepOptions;
    }

    public void setLoadedPrepOptions(HashMap<Integer, List<PrepOptionModel>> loadedPrepOptions) {
        this.loadedPrepOptions = loadedPrepOptions;
    }

    public List<DisclaimerModel> getDisclaimers() {
        return disclaimers;
    }

    public void setDisclaimers(List<DisclaimerModel> disclaimers) {
        this.disclaimers = disclaimers;
    }

    public Boolean getComboActive() {
        return comboActive;
    }

    public void setComboActive(Boolean comboActive) {
        this.comboActive = comboActive;
    }

    public String getComboName() {
        return comboName;
    }

    public void setComboName(String comboName) {
        this.comboName = comboName;
    }
    
    /**
     * Overridden toString() method for a ProductModel.
     * @return a {@link String} representation of a ProductModel.
     */
    @Override
    public String toString () {

        String modsStr = "";
        if (!DataFunctions.isEmptyCollection(modifiers)) {
            modsStr += "[";
            for (ModifierModel modifier : modifiers) {
                modsStr += "[";
                if (modifier.equals(modifiers.get(modifiers.size() - 1))) {
                    modsStr += modifier.toString() + "; ";
                }
                else {
                    modsStr += modifier.toString() + "]";
                }
            }
            modsStr += "]";
        }

        String modMenusStr = "";
        if (!DataFunctions.isEmptyCollection(modifierMenus)) {
            modMenusStr += "[";
            for (ModMenuModel modMenu : modifierMenus) {
                modMenusStr += "[";
                if (modMenu.equals(modifierMenus.get(modifierMenus.size() - 1))) {
                    modMenusStr += modMenu.toString() + "; ";
                }
                else {
                    modMenusStr += modMenu.toString() + "]";
                }
            }
            modMenusStr += "]";
        }

        String loadedPrepOptsStr = "";
        if (!DataFunctions.isEmptyMap(loadedPrepOptions)) {
            loadedPrepOptsStr += "[";
            for (Map.Entry<Integer, List<PrepOptionModel>> entry : loadedPrepOptions.entrySet()) {
                if (!DataFunctions.isEmptyCollection(entry.getValue())) {
                    loadedPrepOptsStr += "[" + entry.getKey() + ": ";
                    for (PrepOptionModel prepOption : entry.getValue()) {
                        loadedPrepOptsStr += "[";
                        if (prepOption.equals(entry.getValue().get(entry.getValue().size() - 1))) {
                            loadedPrepOptsStr += prepOption.toString() + "; ";
                        }
                        else {
                            loadedPrepOptsStr += prepOption.toString() + "]";
                        }
                    }
                    loadedPrepOptsStr += "]";
                }
            }
            loadedPrepOptsStr += "]";
        }
        
        String disclaimersStr = "";
        if (!DataFunctions.isEmptyCollection(disclaimers)) {
            disclaimersStr += "[";
            for (DisclaimerModel disclaimer : disclaimers) {
                disclaimersStr += "[";
                if (disclaimer.equals(disclaimers.get(disclaimers.size() - 1))) {
                    disclaimersStr += disclaimer.toString() + "; ";
                }
                else {
                    disclaimersStr += disclaimer.toString() + "]";
                }
            }
            disclaimersStr += "]";
        }

        return String.format("ID: %s, MODIFIERSETID: %s, PREPOPTIONSETID: %s, SUBDEPTID: %s, DEPARTMENTID: %s, COMPLEMENTARYKEYPADID: %s, " +
                "SUBDEPTCOMPLEMENTARYKEYPADID: %s, SUBDEPTSIMILARKEYPADID: %s, KEYPADID: %s, MODSETDETAILID: %s, AUTHENTICATEDACCOUNTID: %s, " +
                "TRANSLINEITEMID: %s, COMBOTRANSLINEITEMID: %s, COMBODETAILID: %s, PRICE:%s, ORIGINALPRICE: %s, NUTRITIONINFO1: %s, NUTRITIONINFO2: %s, " +
                "NUTRITIONINFO3: %s, NUTRITIONINFO4: %s, NUTRITIONINFO5: %s, FIXEDTAREWEIGHT: %s, QUANTITY: %s, INVCURRENT: %s, BASEPRICE: %s, " +
                "COMBOPRICE: %s, UPCHARGE: %s, PRODUCTINSTORE: %s, SCALEUSED: %s, SHRINKIMAGETOFIT: %s, HEALTHY: %s, VEGETARIAN: %s, " +
                "VEGAN: %s, GLUTENFREE: %s, HASNUTRITION: %s, ISINVENTORYITEM: %s, DININGOPTIONPRODUCT: %s, HASPRINTERMAPPINGS: %s, " +
                "AUTOSHOWMODSET: %s, COMBOACTIVE: %s, NAME:%s, DESCRIPTION: %s, IMAGE:%s, TAXIDS: %s, DISCOUNTIDS: %s, TARENAME: %s, " +
                "MESSAGE: %s, COMBONAME: %s, LASTUPDATEDTM: %s, LASTUPDATECHECK: %s, PREPOPTION: %s, MODIFIERS: %s, MODIFIERMENUS: %s, " +
                "LOADEDPREPOPTIONS: %s, DISCLAIMERS: %s",
                Objects.toString((id != null && id > 0 ? id : "N/A"), "N/A"),
                Objects.toString((modifierSetID != null && modifierSetID > 0 ? modifierSetID : "N/A"), "N/A"),
                Objects.toString((prepOptionSetID != null && prepOptionSetID > 0 ? prepOptionSetID : "N/A"), "N/A"),
                Objects.toString((subDeptID != null && subDeptID > 0 ? subDeptID : "N/A"), "N/A"),
                Objects.toString((departmentID != null && departmentID > 0 ? departmentID : "N/A"), "N/A"),
                Objects.toString((complementaryKeypadID != null && complementaryKeypadID > 0 ? complementaryKeypadID : "N/A"), "N/A"),
                Objects.toString((subDeptComplementaryKeypadID != null && subDeptComplementaryKeypadID > 0 ? subDeptComplementaryKeypadID : "N/A"), "N/A"),
                Objects.toString((subDeptSimilarKeypadID != null && subDeptSimilarKeypadID > 0 ? subDeptSimilarKeypadID : "N/A"), "N/A"),
                Objects.toString((keypadID != null && keypadID > 0 ? keypadID : "N/A"), "N/A"),
                Objects.toString((modSetDetailID != null && modSetDetailID > 0 ? modSetDetailID : "N/A"), "N/A"),
                Objects.toString((authenticatedAccountID != null && authenticatedAccountID > 0 ? authenticatedAccountID : "N/A"), "N/A"),
                Objects.toString((transLineItemID != null && transLineItemID > 0 ? transLineItemID : "N/A"), "N/A"),
                Objects.toString((comboTransLineItemId != null && comboTransLineItemId > 0 ? comboTransLineItemId : "N/A"), "N/A"),
                Objects.toString((comboDetailId != null && comboDetailId > 0 ? comboDetailId : "N/A"), "N/A"),
                Objects.toString((price != null ? price.toPlainString() : "N/A"), "N/A"),
                Objects.toString((originalPrice != null ? originalPrice.toPlainString() : "N/A"), "N/A"),
                Objects.toString((nutritionInfo1 != null ? nutritionInfo1.toPlainString() : "N/A"), "N/A"),
                Objects.toString((nutritionInfo2 != null ? nutritionInfo2.toPlainString() : "N/A"), "N/A"),
                Objects.toString((nutritionInfo3 != null ? nutritionInfo3.toPlainString() : "N/A"), "N/A"),
                Objects.toString((nutritionInfo4 != null ? nutritionInfo4.toPlainString() : "N/A"), "N/A"),
                Objects.toString((nutritionInfo5 != null ? nutritionInfo5.toPlainString() : "N/A"), "N/A"),
                Objects.toString((fixedTareWeight != null ? fixedTareWeight.toPlainString() : "N/A"), "N/A"),
                Objects.toString((quantity != null ? quantity.toPlainString() : "N/A"), "N/A"),
                Objects.toString((invCurrent != null ? invCurrent.toPlainString() : "N/A"), "N/A"),
                Objects.toString((basePrice != null ? basePrice.toPlainString() : "N/A"), "N/A"),
                Objects.toString((comboPrice != null ? comboPrice.toPlainString() : "N/A"), "N/A"),
                Objects.toString((upcharge != null ? upcharge.toPlainString() : "N/A"), "N/A"),
                Objects.toString(productInStore, "N/A"),
                Objects.toString(scaleUsed, "N/A"),
                Objects.toString(shrinkImageToFit, "N/A"),
                Objects.toString(healthy, "N/A"),
                Objects.toString(vegetarian, "N/A"),
                Objects.toString(vegan, "N/A"),
                Objects.toString(glutenFree, "N/A"),
                Objects.toString(hasNutrition, "N/A"),
                Objects.toString(isInventoryItem, "N/A"),
                Objects.toString(diningOptionProduct, "N/A"),
                Objects.toString(hasPrinterMappings, "N/A"),
                Objects.toString(autoShowModSet, "N/A"),
                Objects.toString(comboActive, "N/A"),
                Objects.toString((StringFunctions.stringHasContent(name) ? name : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(description) ? description : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(image) ? image : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(taxIDs) ? taxIDs : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(discountIDs) ? discountIDs : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(tareName) ? tareName : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(message) ? message : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(comboName) ? comboName : "N/A"), "N/A"),
                Objects.toString((lastUpdateDTM != null ? lastUpdateDTM.toString() : "N/A"), "N/A"),
                Objects.toString((lastUpdateCheck != null ? lastUpdateCheck.toString() : "N/A"), "N/A"),
                Objects.toString((prepOption != null ? prepOption.toString() : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(modsStr) ? modsStr : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(modMenusStr) ? modMenusStr : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(loadedPrepOptsStr) ? loadedPrepOptsStr : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(disclaimersStr) ? disclaimersStr : "N/A"), "N/A"));

    }
    
}

