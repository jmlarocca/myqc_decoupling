package com.mmhayes.myqc.server.models;

//mmhayes dependencies

import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.utils.Logger;

import java.util.*;

//javax.ws.rs dependencies
//myqc dependencies
//other dependencies

/* Express Order Item Model
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2019-03-13 17:18:12 -0400 (Wed, 13 Mar 2019) $: Date of last commit
 $Rev: 37153 $: Revision of last commit

 Notes: Model for express order items (a.k.a products and products with modifiers)
*/
public class ComboModel {
    private Integer id = null;
    private Integer paDiscountId = null;

    private String name = "";

    private List<ComboDetailModel> comboDetails = new ArrayList<ComboDetailModel>();
    private List<Integer> comboDetailKeypadIds = new ArrayList<>();
    static DataManager dm = new DataManager();

    public ComboModel() {

    }

    public ComboModel(Integer comboId) throws Exception {

        ArrayList<HashMap> comboList = dm.parameterizedExecuteQuery("data.combo.getCombDetails",
            new Object[]{
                comboId
            },
            true
        );

        if ( comboList == null || comboList.size() == 0 ) {
            Logger.logMessage("Could not retrieve combo details for combo ID:" + comboId, Logger.LEVEL.TRACE);
            return;
        }

        setModelProperties( comboList.get(0) );

        for(HashMap comboDetail : comboList) {
            ComboDetailModel comboDetailModel = new ComboDetailModel(comboDetail);
            getComboDetails().add(comboDetailModel);
            getComboDetailKeypadIds().add(comboDetailModel.getKeypadId());
        }
    }

    //constructor - takes hashmap that get sets to this models properties
    public ComboModel(HashMap modelDetailHM) throws Exception {
        setModelProperties(modelDetailHM);
    }

    //setter for all of the favorite order model's for the My Quick Picks page
    public ComboModel setModelProperties(HashMap modelDetailHM) throws Exception{
        setID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PACOMBOID")));
        setName(CommonAPI.convertModelDetailToString(modelDetailHM.get("NAME")));
        setPaDiscountId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PADISCOUNTID")));

        return this;
    }

    //getter
    public Integer getID() {
        return this.id;
    }

    //setter
    public void setID(Integer id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name.replace("\\r", " ");
    }

    public Integer getPaDiscountId() {
        return paDiscountId;
    }

    public void setPaDiscountId(Integer paDiscountId) {
        this.paDiscountId = paDiscountId;
    }

    public List<ComboDetailModel> getComboDetails() {
        return comboDetails;
    }

    public void setComboDetails(List<ComboDetailModel> comboDetails) {
        this.comboDetails = comboDetails;
    }

    public List<Integer> getComboDetailKeypadIds() {
        return comboDetailKeypadIds;
    }

    public void setComboDetailKeypadIds(List<Integer> comboDetailKeypadIds) {
        this.comboDetailKeypadIds = comboDetailKeypadIds;
    }

}

