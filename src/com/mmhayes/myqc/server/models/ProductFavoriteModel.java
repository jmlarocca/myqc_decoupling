package com.mmhayes.myqc.server.models;

//mmhayes dependencies

import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.utils.Logger;

import java.util.*;

//javax.ws.rs dependencies
//myqc dependencies
//other dependencies

/* Product Model
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2020-10-01 14:53:57 -0400 (Thu, 01 Oct 2020) $: Date of last commit
 $Rev: 51599 $: Revision of last commit

 Notes: Model for products (a.k.a PLUs, Items, etc.)
*/
public class ProductFavoriteModel {
    Integer id = null; //Product ID (PAPluID)
    Integer productID = null; //Product ID (PAPluID)
    Integer terminalID = null;
    Integer storeID = null;
    Integer orderingFavoriteID = null;
    Integer keypadID = null;
    Integer prepOptionSetID = null;
    List<ModifierModel> modifiers = new ArrayList<>(); //this Array List represents a ModifierCollection which contains a list of ModifierModels
    PrepOptionModel prepOption = null; //model that holds prep option details for products
    boolean active = false;

    private static DataManager dm = new DataManager();
    private Integer authenticatedAccountID = null;

    //dummy constructor - needed to consume ANY JSON object via REST (Jersey/Jackson style)
    public ProductFavoriteModel() {

    }

    //dummy constructor - needed to consume ANY JSON object via REST (Jersey/Jackson style)
    public ProductFavoriteModel(HashMap modelDetailHM) {
        setModelProperties(modelDetailHM);
    }

    //setter for all of this model's properties
    public ProductFavoriteModel setModelProperties(HashMap modelDetailHM) {
        try {

            setID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ORDERINGFAVORITEID")));

            setOrderingFavoriteID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ORDERINGFAVORITEID")));

            setProductID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAPLUID")));

            setKeypadID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAKEYPADID")));

            setActive(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("ACTIVE")));

            setPrepOptionSetID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAPREPOPTIONSETID")));

            Integer prepOptionID = CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAPREPOPTIONID"));

            if(prepOptionID != null) {
                PrepOptionModel prepOptionModel = new PrepOptionModel();
                prepOptionModel.setID(prepOptionID);
                setPrepOption(prepOptionModel);
            }

        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
        return this;
    }

    //getter
    private Integer getAuthenticatedAccountID() {
        return this.authenticatedAccountID;
    }

    //setter
    private void setAuthenticatedAccountID(Integer authenticatedAccountID) {
        this.authenticatedAccountID = authenticatedAccountID;
    }

    //getter
    public Integer getID() {
        return this.id;
    }

    //setter
    public void setID(Integer id) {
        this.id = id;
    }

    //getter
    public List<ModifierModel> getModifiers() {
        return modifiers;
    }

    //setter
    public void setModifiers(List<ModifierModel> modifiers) {
        if(modifiers == null) {
            modifiers = new ArrayList<>();
        }

        this.modifiers = modifiers;
    }

    public PrepOptionModel getPrepOption() {
        return prepOption;
    }

    public void setPrepOption(PrepOptionModel prepOption) {
        this.prepOption = prepOption;
    }

    public Integer getStoreID() {
        return storeID;
    }

    public void setStoreID(Integer storeID) {
        this.storeID = storeID;
    }

    public Integer getOrderingFavoriteID() {
        return orderingFavoriteID;
    }

    public void setOrderingFavoriteID(Integer orderingFavoriteID) {
        this.orderingFavoriteID = orderingFavoriteID;
    }

    public Integer getTerminalID() {
        return terminalID;
    }

    public void setTerminalID(Integer terminalID) {
        this.terminalID = terminalID;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Integer getKeypadID() {
        return keypadID;
    }

    public void setKeypadID(Integer keypadID) {
        this.keypadID = keypadID;
    }

    public Integer getPrepOptionSetID() {
        return prepOptionSetID;
    }

    public void setPrepOptionSetID(Integer prepOptionSetID) {
        this.prepOptionSetID = prepOptionSetID;
    }

    public Integer getProductID() {
        return productID;
    }

    public void setProductID(Integer productID) {
        this.productID = productID;
    }
}

