package com.mmhayes.myqc.server.models;

//mmhayes dependencies

import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.myqc.server.collections.IntervalCollection;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.*;

//myqc dependencies
//other dependencies

/* Schedule Detail Model
Last Updated (automatically updated by SVN)
$Author: gematuszyk $: Author of last commit
$Date: 2019-04-10 13:39:03 -0400 (Wed, 10 Apr 2019) $: Date of last commit
$Rev: 37794 $: Revision of last commit

Notes: Model for ordering buffer schedules*/
public class ScheduleDetailModel {
    private static DataManager dm = new DataManager();

    private Integer ID = null; //the ID of the schedule detail record
    private Integer scheduleID = null; //the ID of the schedule header record
    private Integer scheduleTypeID = null; //the typeID of the schedule

    private String scheduleStartTime = "";
    private String scheduleEndTime = "";

    private Boolean onSun = false;
    private Boolean onMon = false;
    private Boolean onTue = false;
    private Boolean onWed = false;
    private Boolean onThu = false;
    private Boolean onFri = false;
    private Boolean onSat = false;

    //buffering specific properties
    private Integer maxTotalOrders = null;
    private Integer maxDeliveryOrders = null;
    private Integer maxPickupOrders = null;
    private Integer maxTotalOrdersPer15Min = null;
    private Integer maxDeliveryOrdersPer15Min = null;
    private Integer maxPickupOrdersPer15Min = null;
    private Boolean scheduleOpen = false;
    private Boolean deliveryScheduleOpen = false;
    private Boolean pickupScheduleOpen = false;

    //constructor - takes hashmap that get sets to this models properties
    public ScheduleDetailModel(HashMap scheduleDetailHM) throws Exception {
        setModelProperties(scheduleDetailHM);
    }

    public boolean checkDayOfWeek(String dayOfWeek) {
        boolean isOpen = false;

        switch (dayOfWeek) {
            case "mon":
                isOpen = getOnMon();
                break;
            case "tue":
                isOpen = getOnTue();
                break;
            case "wed":
                isOpen = getOnWed();
                break;
            case "thu":
                isOpen = getOnThu();
                break;
            case "fri":
                isOpen = getOnFri();
                break;
            case "sat":
                isOpen = getOnSat();
                break;
            case "sun":
                isOpen = getOnSun();
                break;
        }

        return isOpen;
    }

    //setter for all of this model's properties
    public ScheduleDetailModel setModelProperties(HashMap modelDetailHM) throws Exception {
        setID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));
        setScheduleID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("SCHEDULEID")));
        setScheduleTypeID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("SCHEDULETYPEID")));

        setScheduleStartTime(CommonAPI.convertModelDetailToString(modelDetailHM.get("STARTTIME")));
        setScheduleEndTime(CommonAPI.convertModelDetailToString(modelDetailHM.get("ENDTIME")));

        setOnSun(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("ONSUN"), false));
        setOnMon(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("ONMON"), false));
        setOnTue(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("ONTUE"), false));
        setOnWed(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("ONWED"), false));
        setOnThu(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("ONTHU"), false));
        setOnFri(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("ONFRI"), false));
        setOnSat(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("ONSAT"), false));

        if (!getScheduleTypeID().equals(ScheduleModel.ORDER_BUFFERING_SCHEDULE)) {
            return this;
        }

        setMaxTotalOrders(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("MAXTOTALORDERS")));
        setMaxDeliveryOrders(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("MAXDELIVERYORDERS")));
        setMaxPickupOrders(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("MAXPICKUPORDERS")));
        setMaxTotalOrdersPer15Min(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("MAXTOTALORDERSPER15MIN")));
        setMaxDeliveryOrdersPer15Min(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("MAXDELIVERYORDERSPER15MIN")));
        setMaxPickupOrdersPer15Min(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("MAXPICKUPORDERSPER15MIN")));

        return this;
    }

    public void determineBufferScheduleOpen(IntervalCollection bufferIntervals) throws Exception {
        Integer foundTransactionTotal = 0;
        Integer foundPickupTransactionTotal = 0;
        Integer foundDeliveryTransactionTotal = 0;

        this.setScheduleOpen(true);
        this.setPickupScheduleOpen(true);
        this.setDeliveryScheduleOpen(true);

        for(IntervalModel bufferInterval : bufferIntervals.getCollection()) {

            if( !bufferInterval.getScheduleDetailID().equals(getID()) ) {
                continue;
            }

            foundTransactionTotal += bufferInterval.getTransactionTotalCount();
            foundPickupTransactionTotal += bufferInterval.getPickupTransactionCount();
            foundDeliveryTransactionTotal += bufferInterval.getDeliveryTransactionCount();

            //check that the schedule detail hasn't reached the max total orders, max pickup orders or max delivery orders count
            if(foundTransactionTotal >= this.getMaxTotalOrders() || (foundPickupTransactionTotal >= this.getMaxPickupOrders() && foundDeliveryTransactionTotal >= this.getMaxDeliveryOrders())) {
                this.setScheduleOpen(false);
                this.setPickupScheduleOpen(false);
                this.setDeliveryScheduleOpen(false);
                break;

            } else if (foundPickupTransactionTotal >= this.getMaxPickupOrders()) {
                this.setPickupScheduleOpen(false);

            } else if (foundDeliveryTransactionTotal >= this.getMaxDeliveryOrders()) {
                this.setDeliveryScheduleOpen(false);
            }

            //check that the interval hasn't reached the max total orders per 15, max pickup orders per 15 or max delivery orders per 15 count
            if(bufferInterval.getTransactionTotalCount() >= this.getMaxTotalOrdersPer15Min() || (bufferInterval.getPickupTransactionCount() >= this.getMaxPickupOrdersPer15Min() && bufferInterval.getDeliveryTransactionCount() >= this.getMaxDeliveryOrdersPer15Min())) {
                bufferInterval.setIntervalOpen(false);
                bufferInterval.setPickupOpen(false);
                bufferInterval.setDeliveryOpen(false);

            } else if (bufferInterval.getPickupTransactionCount() >= this.getMaxPickupOrdersPer15Min()) {
                bufferInterval.setPickupOpen(false);

            } else if (bufferInterval.getDeliveryTransactionCount() >= this.getMaxDeliveryOrdersPer15Min()) {
                bufferInterval.setDeliveryOpen(false);
            }
        }

        //determine if all the intervals need to be closed based on the total transaction counts
        if( !this.getScheduleOpen() ) {
            closeIntervals(bufferIntervals, 0);

        } else if ( !this.getPickupScheduleOpen() ) {
            closeIntervals(bufferIntervals, 1);

        } else if ( !this.getDeliveryScheduleOpen() ) {
            closeIntervals(bufferIntervals, 2);
        }
    }

    //loop over the buffer schedule intervals and close them based on whether the entire schedule is closed, just pickup or just delivery
    public void closeIntervals(IntervalCollection bufferIntervals, Integer closeType) throws Exception {

        for(IntervalModel bufferInterval : bufferIntervals.getCollection()) {

            if( !bufferInterval.getScheduleDetailID().equals(getID()) ) {
                continue;
            }

            if(closeType.equals(0)) {
                bufferInterval.setIntervalOpen(false);
                bufferInterval.setPickupOpen(false);
                bufferInterval.setDeliveryOpen(false);

            } else if (closeType.equals(1)) {
                bufferInterval.setPickupOpen(false);

            } else if (closeType.equals(2)) {
                bufferInterval.setDeliveryOpen(false);
            }
        }
    }

    public String getScheduleStartTime() {
        return scheduleStartTime;
    }

    public void setScheduleStartTime(String scheduleStartTime) {
        this.scheduleStartTime = scheduleStartTime;
    }

    public String getScheduleEndTime() {
        return scheduleEndTime;
    }

    public void setScheduleEndTime(String scheduleEndTime) {
        this.scheduleEndTime = scheduleEndTime;
    }

    public Integer getMaxTotalOrders() {
        return maxTotalOrders;
    }

    public void setMaxTotalOrders(Integer maxTotalOrders) {
        this.maxTotalOrders = maxTotalOrders;
    }

    public Integer getMaxDeliveryOrders() {
        return maxDeliveryOrders;
    }

    public void setMaxDeliveryOrders(Integer maxDeliveryOrders) {
        this.maxDeliveryOrders = maxDeliveryOrders;
    }

    public Integer getMaxPickupOrders() {
        return maxPickupOrders;
    }

    public void setMaxPickupOrders(Integer maxPickupOrders) {
        this.maxPickupOrders = maxPickupOrders;
    }

    public Integer getMaxTotalOrdersPer15Min() {
        return maxTotalOrdersPer15Min;
    }

    public void setMaxTotalOrdersPer15Min(Integer maxTotalOrdersPer15Min) {
        this.maxTotalOrdersPer15Min = maxTotalOrdersPer15Min;
    }

    public Integer getMaxDeliveryOrdersPer15Min() {
        return maxDeliveryOrdersPer15Min;
    }

    public void setMaxDeliveryOrdersPer15Min(Integer maxDeliveryOrdersPer15Min) {
        this.maxDeliveryOrdersPer15Min = maxDeliveryOrdersPer15Min;
    }

    public Integer getMaxPickupOrdersPer15Min() {
        return maxPickupOrdersPer15Min;
    }

    public void setMaxPickupOrdersPer15Min(Integer maxPickupOrdersPer15Min) {
        this.maxPickupOrdersPer15Min = maxPickupOrdersPer15Min;
    }

    public Boolean getScheduleOpen() {
        return scheduleOpen;
    }

    public void setScheduleOpen(Boolean scheduleOpen) {
        this.scheduleOpen = scheduleOpen;
    }


    public Integer getScheduleID() {
        return scheduleID;
    }

    public void setScheduleID(Integer scheduleID) {
        this.scheduleID = scheduleID;
    }

    public Boolean getDeliveryScheduleOpen() {
        return deliveryScheduleOpen;
    }

    public void setDeliveryScheduleOpen(Boolean deliveryScheduleOpen) {
        this.deliveryScheduleOpen = deliveryScheduleOpen;
    }

    public Boolean getPickupScheduleOpen() {
        return pickupScheduleOpen;
    }

    public void setPickupScheduleOpen(Boolean pickupScheduleOpen) {
        this.pickupScheduleOpen = pickupScheduleOpen;
    }

    public Boolean getOnSun() {
        return onSun;
    }

    public void setOnSun(Boolean onSun) {
        this.onSun = onSun;
    }

    public Boolean getOnMon() {
        return onMon;
    }

    public void setOnMon(Boolean onMon) {
        this.onMon = onMon;
    }

    public Boolean getOnTue() {
        return onTue;
    }

    public void setOnTue(Boolean onTue) {
        this.onTue = onTue;
    }

    public Boolean getOnWed() {
        return onWed;
    }

    public void setOnWed(Boolean onWed) {
        this.onWed = onWed;
    }

    public Boolean getOnThu() {
        return onThu;
    }

    public void setOnThu(Boolean onThu) {
        this.onThu = onThu;
    }

    public Boolean getOnFri() {
        return onFri;
    }

    public void setOnFri(Boolean onFri) {
        this.onFri = onFri;
    }

    public Boolean getOnSat() {
        return onSat;
    }

    public void setOnSat(Boolean onSat) {
        this.onSat = onSat;
    }

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public Integer getScheduleTypeID() {
        return scheduleTypeID;
    }

    public void setScheduleTypeID(Integer scheduleTypeID) {
        this.scheduleTypeID = scheduleTypeID;
    }
}
