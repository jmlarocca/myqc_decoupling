package com.mmhayes.myqc.server.models;

//mmhayes dependencies

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.CommonAuthResource;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.commonMMHFunctions;
import com.mmhayes.common.disclaimer.collections.DisclaimerCollection;
import com.mmhayes.common.disclaimer.models.DisclaimerModel;
import com.mmhayes.common.purchase_restriction.collections.PurchaseRestrictionProfileCollection;
import com.mmhayes.common.purchase_restriction.models.PurchaseRestrictionProfileDetailModel;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.myqc.server.api.MyQCCache;
import com.mmhayes.myqc.server.collections.IntervalCollection;
import com.mmhayes.myqc.server.collections.ScheduleDetailCollection;
import com.mmhayes.myqc.server.collections.SuggestiveSellingCollection;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

//myqc dependencies
//other dependencies

/* Store Model
 Last Updated (automatically updated by SVN)
 $Author: ijgerstein $: Author of last commit
 $Date: 2021-09-14 10:02:51 -0400 (Tue, 14 Sep 2021) $: Date of last commit
 $Rev: 58054 $: Revision of last commit

 Notes: Model for online ordering "Stores" (virtual terminals called "MyQCTerminals")
*/
public class StoreModel {
    private Integer id = null; //ID of this "Store" as a virtual terminal in the QC_MyQCTerminal table (MyQCTerminalID)
    private Integer terminalID = null; //ID of the terminal in the QC_Terminals table (TerminalID)
    private Integer authenticatedAccountID = null;

    //general store properties
    private Integer homeKeypadID = null; //PAHomeKeypadIDDefault from QC_Terminals - the "home" keypad id for this Store
    private Integer diningOptionKeypadID = null; //DiningOptionsPAKeypadID from QC_Terminals - the dining option keypad id for this Store
    private Integer reAuthTypeID = 1; //for reauthentication on purchase
    private Integer maxMinsLastOnlineCheck = null; //how long (in minutes) the store will be allowed to accept orders since the last online order check
    private Integer maxNumDaysForFutureOrders = null; //how many days in the future the store will allow future orders to be placed
    private BigDecimal maxOverdraftAmount = new BigDecimal(0); //the max amount the account can be overdrafted to pay for the order
    BigDecimal percentOfPopularProducts = BigDecimal.valueOf(25); //the percent of popular products based on all the products in the store, defaults to 25
    private boolean ignoreInventoryCounts = false;
    private boolean requirePhoneNum = false; //flag that determines if this Store requires a phone number
    private boolean usesSMSNotifications = false; //flag that determines if this Store uses SMS Notifications
    private boolean usesEatInTakeOut = false; //flag that determines if this Store uses Eat In or Take Out feature
    private boolean isPrinterHostOnline = false; //flag that determines if the printer host is offline
    private boolean enableFavorites = false; //flag that determines the store can you favorites
    private boolean usesCreditCardAsTender = false; //flag that determines if this Store uses Credit Card as a Tender
    private boolean chargeAtPurchaseTime = true; //flag that determines if this store will charge the user when they place the order or when they pick it up - sale vs open transaction
    private boolean allowsVouchers = false; //flag that determines if vouchers are available for this store
    private boolean allowItemScan = false; //flag that determines if this Store will allow items to be scanned or not
    private boolean productScanAddToCart = true; //flag that determines if scanned items will be added directly to the store
    private boolean useFundingTerminal = false; //merchant account information on terminal
    private boolean useOnlineOrderingFunding = false; //merchant account information on terminal
    private List<DisclaimerModel> disclaimers = new ArrayList<>(); //Contains a list of the disclaimers set for the store

    //show order detail and pickup/delivery time fields
    private boolean forceASAP = false; //flag that determines if the store will allow them to pick a pickup/delivery time
    private boolean showPhoneNumberInput = true; //flag that determines if the phone input should show on Order Details page
    private boolean showCommentInput = true; //flag that determines if the comment input should show on Order Details page
    private boolean showDeliveryInput = true; //flag that determines if the delivery location input should show on Order Details page
    private boolean showWaitTime = true; //flag that determines if the wait time should show on the store page
    private boolean showOrderTypeAndTimeMsg = true; //flag that determines if the order type and time should show on Order Details, Review, Confirmation Message and Receipt pages
    private boolean showPickupDeliveryLocationMsg = true; //flag that determines if the pickup/delivery location should show on Confirmation Message page

    //header properties
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    private String header1 = ""; //information for the Store off the virtual Terminal Record for displaying the store name, hours, etc.
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    private String header2 = ""; //information for the Store off the virtual Terminal Record for displaying the store name, hours, etc.
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    private String header3 = ""; //information for the Store off the virtual Terminal Record for displaying the store name, hours, etc.
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    private String header4 = ""; //information for the Store off the virtual Terminal Record for displaying the store name, hours, etc.
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    private String header5 = ""; //information for the Store off the virtual Terminal Record for displaying the store name, hours, etc.
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    private String venueCategory = ""; //category of the Revenue Center which the Store (the virtual terminal) is part of
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    private String pickupWaitTime = ""; //the estimated wait time for the earliest pickup order
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    private String deliveryWaitTime = ""; //the estimated wait time for the earliest pickup order
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    private String openTxnPopupMessage = ""; //warning the user they are doing an open txn
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    private String upsellPageMessage = ""; //header message on suggestions page
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    private String iconGrayscalePercentage = ""; //grayscale percentage for icons

    //suggestive selling related properties
    private Integer maxNumUpsellKeypads = null; //how many suggestions to make when adding product to the cart or checkout
    private Integer upsellProfileID = null; //the upsell profile ID associated with the terminal, hold suggestive selling keypads
    private List<MenuModel> upsellProfileKeypads = new ArrayList<>();

    //purchase restriction profile related properties
    private Integer purchaseRestrictionProfileID = null; //the purchase restriction profile ID associated with the employee's spending profile and purchase limit
    private List<PurchaseRestrictionProfileDetailModel> purchaseRestrictionProfileDetails = new ArrayList<>(); //purchase restriction profile details based on product, subdept or dept

    //store time related properties
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    private String currentStoreTime = ""; //holds the String representation of the current time
    private LocalDateTime currentStoreDTM = null; //holds the LocalDateTime representation of the current time
    private Integer timezoneOffset = 0; //for when the terminal and server are in different time zones
    private LocalDate futureOrderDate = null; //the future order date the user selected for when they want to place an order

    //pickup related properties
    private Integer numMinutesUntilStoreClose = null;
    private Boolean allowPickUp = false; //flag that determines if this Store allows pick up orders at all
    private Integer pickUpScheduleID = null; //the schedule that determines when the kitchen is making pickup orders
    private Integer pickUpOrderingScheduleID = null; //the schedule that determines when the store is accepting new pickup orders
    private Integer pickUpPrepTime = 0; //treat as minutes - time it takes for the Store to prepare pick up orders
    private ArrayList<String> pickUpOpenTimes = new ArrayList<>(); //when the Store IS willing to have users pick up orders today (as in "now")
    private ArrayList<String> pickUpCloseTimes = new ArrayList<>(); //when the Store IS willing to have users pick up orders today (as in "now")
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    private String pickUpOrderingOpenTime = ""; //when the Store IS willing to receive pick up orders today (as in "now")
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    private String pickUpOrderingCloseTime = ""; //when the Store IS NOT willing to receive pick up orders today (as in "now")

    //delivery related properties
    private Boolean allowDelivery = false; //flag that determines if this Store allows delivery orders at all
    private Integer deliveryScheduleID = null; //the schedule that determines when the kitchen is making delivery orders
    private Integer deliveryOrderingScheduleID = null; //the schedule that determines when the store is accepting new delivery orders
    private Integer deliveryPrepTime = 0; //treat as minutes - time it takes for the Store to prepare and deliver a delivery order
    private BigDecimal deliveryMinimum = new BigDecimal(0); //treat as money - check against this amount for delivery orders before allowing to be placed
    private ArrayList<String> deliveryOpenTimes = new ArrayList<>(); //when the Store IS willing to deliver orders today (as in "now")
    private ArrayList<String> deliveryCloseTimes = new ArrayList<>(); //when the Store IS willing to deliver orders today (as in "now")
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    private String deliveryOrderingOpenTime = ""; //when the Store IS willing to receive delivery orders today (as in "now")
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    private String deliveryOrderingCloseTime = ""; //when the Store IS NOT willing to receive delivery orders today (as in "now")

    //buffering related properties
    private Boolean usingBuffering = false; //flag that determines if this Store's buffering schedule is active
    private Integer bufferScheduleID = null; //the schedule that limits the amount of orders that can be placed in a given timeframe
    private ArrayList<String> bufferingOpenTimes = new ArrayList<>();
    private ArrayList<String> bufferingCloseTimes = new ArrayList<>();
    private List<IntervalModel> bufferIntervalList = new ArrayList<>();
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    private String pickupOriginalCloseTime = ""; //when the Store was originally closing for pickup orders
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    private String deliveryOriginalCloseTime = ""; //when the Store was originally closing for delivery orders

    //caching related properties
    private LocalDateTime lastUpdateDTM = null; //when the store was last updated
    private LocalDateTime lastUpdateCheck = null; //when the store was last checked to see if it has been updated
    private String tokenizedFileName = null;
    private Integer imagePosition = 1;

    private static DataManager dm = new DataManager();
    private commonMMHFunctions commFunc = new commonMMHFunctions();

    //constructor - takes hashmap that get sets to this models properties
    public StoreModel(HashMap modelDetailHM, Integer authenticatedAccountID) {
        try {
            setAuthenticatedAccountID( authenticatedAccountID );

            setModelProperties( modelDetailHM, false );

            MyQCCache.addStoreToCache( this );
        } catch (Exception ex) {
            Logger.logMessage("Could not create StoreModel in hashmap constructor", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }
    }

    public StoreModel(Integer storeID, HttpServletRequest request) throws Exception {

        //determine the authenticated account ID from the request
        setAuthenticatedAccountID( CommonAuthResource.checkRequestHeaderForMMHayesAuth(request) );

        setStoreDetails(storeID, getAuthenticatedAccountID());
    }

    //constructor - gets the store's details by ID and populates model properties, sets the future order date for the store's current time
    public StoreModel(Integer storeID, LocalDate futureOrderDate, HttpServletRequest request) throws Exception {

        //determine the authenticated account ID from the request
        setAuthenticatedAccountID( CommonAuthResource.checkRequestHeaderForMMHayesAuth(request) );

        //set the future order date
        if(futureOrderDate != null) {
            setFutureOrderDate(futureOrderDate);
        }

        setStoreDetails(storeID, getAuthenticatedAccountID());
    }

    public StoreModel(Integer storeID, Integer employeeID) {
        setAuthenticatedAccountID(employeeID);

        setStoreDetails(storeID, employeeID);
    }

    public static StoreModel getStoreModel( Integer storeId, Integer employeeID ) throws Exception {
        //if store is in cache, load from cache
        if ( MyQCCache.checkCacheForStore( storeId ) ) {
            return MyQCCache.getStoreFromCache( storeId );
        }

        return new StoreModel( storeId, employeeID );
    }

    //constructor - takes hashmap and sets to this models properties, sets the future order date for the store's current time
    public StoreModel(HashMap modelDetailHM, Integer authenticatedAccountID, String futureOrderDate) throws Exception {

        setAuthenticatedAccountID(authenticatedAccountID);

        //set the future order date
        setFutureOrderDateByStr(futureOrderDate);

        setModelProperties( modelDetailHM, true );

        MyQCCache.addStoreToCache( this );
    }

    public void setStoreDetails(Integer storeID, Integer employeeID) {
        try {

            //get all models in an array list
            ArrayList<HashMap> storeList = dm.parameterizedExecuteQuery("data.ordering.getStore",
                    new Object[]{
                            storeID,
                            employeeID
                    },
                    true
            );

            //populate this collection from an array list of hashmaps
            if ( storeList == null || storeList.size() == 0 || storeList.get(0) == null ) {
                Logger.logMessage("Could not find store record for ID: " + storeID, Logger.LEVEL.ERROR);
                return;
            }

            HashMap modelDetailHM = storeList.get(0);

            //determine that response is valid
            if ( modelDetailHM.get("ID") == null || modelDetailHM.get("ID").toString().isEmpty() ) {
                Logger.logMessage("Could not determine storeID in StoreModel constructor by ID", Logger.LEVEL.ERROR);
                return;
            }

            setModelProperties( modelDetailHM, true );

            //add newly created model to store cache
            MyQCCache.addStoreToCache( this );
        } catch (Exception ex) {
            Logger.logMessage("Could not create StoreModel in ID constructor", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }
    }

    public static StoreModel getStoreModel( Integer storeId, HttpServletRequest request ) throws Exception {
        //if store is in cache, load from cache
        if ( MyQCCache.checkCacheForStore( storeId ) ) {
            return MyQCCache.getStoreFromCache( storeId );
        }

        return new StoreModel( storeId, request );
    }

    public static StoreModel getStoreModelForFutureOrder( Integer storeId, LocalDate futureOrderDate, HttpServletRequest request ) throws Exception {
        //if store is in cache, load from cache
        if ( MyQCCache.checkCacheForStore( storeId ) ) {
            return MyQCCache.getStoreFromCache( storeId, futureOrderDate );
        }

        return new StoreModel( storeId, futureOrderDate, request );
    }

    //setter for all of this model's properties
    public StoreModel setModelProperties(HashMap modelDetailHM, boolean withMenuItems) {
        try {
            setID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));
            setTerminalID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("TERMINALID")));
            setHomeKeypadID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("HOMEKEYPADID")));
            setDiningOptionKeypadID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("DININGOPTIONKEYPADID")));
            setPercentOfPopularProducts(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("PERCENTOFPOPULARPRODUCTS")));

            setHeader1(CommonAPI.convertModelDetailToString(modelDetailHM.get("HEADER1")));
            setHeader2(CommonAPI.convertModelDetailToString(modelDetailHM.get("HEADER2")));
            setHeader3(CommonAPI.convertModelDetailToString(modelDetailHM.get("HEADER3")));
            setHeader4(CommonAPI.convertModelDetailToString(modelDetailHM.get("HEADER4")));
            setHeader5(CommonAPI.convertModelDetailToString(modelDetailHM.get("HEADER5")));
            setVenueCategory(CommonAPI.convertModelDetailToString(modelDetailHM.get("VENUECATEGORY")));

            setPickUpPrepTime(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PICKUPPREPTIME")));
            setDeliveryPrepTime(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("DELIVERYPREPTIME")));
            setTimezoneOffset(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("TIMEDIFFERENCE"), 0, true));
            setReAuthTypeID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("REAUTHTYPEID")));
            setMaxMinsLastOnlineCheck(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("MAXMINSLASTONLINECHECK")));
            setMaxNumDaysForFutureOrders(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("MAXNUMDAYSFORFUTUREORDERS")));
            setMaxNumUpsellKeypads(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAMAXNUMUPSELLKEYPADS")));
            setUpsellProfileID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAUPSELLPROFILEID")));
            setPurchaseRestrictionProfileID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("RESTRICTIONPROFILEID")));
            setUpsellPageMessage(CommonAPI.convertModelDetailToString(modelDetailHM.get("PAUPSELLMESSAGE")));

            setAllowPickUp(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("ALLOWPICKUP"), false));
            setAllowDelivery(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("ALLOWDELIVERY"), false));
            setIgnoreInventoryCounts(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("IGNOREINVENTORYCOUNTS"), false));
            setRequirePhoneNum(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("REQUIREPHONENUM")));
            setUsesSMSNotifications(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("USESSMSNOTIFICATIONS")));
            setEnableFavorites(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("ENABLEFAVORITES")));
            setUsesEatInTakeOut(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("USESEATINTAKEOUT")));
            setUsesCreditCardAsTender(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("USESCREDITCARDASTENDER")));
            setChargeAtPurchaseTime(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("CHARGEATPURCHASETIME")));
            setAllowsVouchers(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("ALLOWSVOUCHERS")));
            setOpenTxnPopupMessage(CommonAPI.convertModelDetailToString(modelDetailHM.get("OPENTXNPOPUPMESSAGE")));
            setIconGrayscalePercentage(CommonAPI.convertModelDetailToString(modelDetailHM.get("ICONGRAYSCALEPERCENT")));
            setAllowItemScan(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("ALLOWITEMSCAN")));
            setProductScanAddToCart(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("PRODUCTSCANADDTOCART")));

            setMaxOverdraftAmount(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("MAXOVERDRAFTAMOUNT")));
            setDeliveryMinimum(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("DELIVERYMINIMUM")));

            setNumMinutesUntilStoreClose(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("NUMMINUTESUNTILSTORECLOSES")));
            setPickUpScheduleID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PICKUPSCHEDULEID")));
            setPickUpOrderingScheduleID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PICKUPORDERINGSCHEDULEID")));
            setDeliveryScheduleID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("DELIVERYSCHEDULEID")));
            setDeliveryOrderingScheduleID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("DELIVERYORDERINGSCHEDULEID")));
            setBufferScheduleID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ORDERINGBUFFERSCHEDULEID")));

            setTokenizedFileName(CommonAPI.convertModelDetailToString(modelDetailHM.get("TOKENIZEDFILENAME")));
            setImagePosition(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("IMAGEPOSITION")));

            setForceASAP(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("FORCEASAP")));
            setShowPhoneNumberInput(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("SHOWPHONENUMBERINPUT")));
            setShowCommentInput(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("SHOWCOMMENTINPUT")));
            setShowDeliveryInput(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("SHOWDELIVERYINPUT")));
            setShowWaitTime(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("SHOWWAITTIME")));
            setShowOrderTypeAndTimeMsg(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("SHOWORDERTYPEANDTIMEMSG")));
            setShowPickupDeliveryLocationMsg(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("SHOWPICKUPDELIVERYLOCATIONMSG")));

            setUseFundingTerminal(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("USEFUNDINGTERMINAL")));
            setUseOnlineOrderingFunding(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("USEONLINEORDERINGFUNDING")));

            //get store disclaimers
            getAndSetStoreDisclaimers();

            //specify in and out formats for formatting modelDetailHM.get("LASTUPDATEDTM")
            String dateTimeInFormat = "yyyy-MM-dd HH:mm:ss.S";
            String dateTimeOutFormat = "M/dd/yyyy h:mm:ss a"; //old way was - M/dd/yyyy H:mm:ss - jrmitaly 6/23/2015

            //format modelDetailHM.get("TRANSDATE") and set to dateTimeStr
            String dateTimeStr = commFunc.formatDateTime(modelDetailHM.get("LASTUPDATEDTM").toString(),dateTimeInFormat,dateTimeOutFormat);

            //parse dateTimeStr to LocalDateTime and set that to this.dateTime
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(dateTimeOutFormat);
            setLastUpdateDTM( LocalDateTime.parse(dateTimeStr, dateTimeFormatter) );

            //sets CurrentStoreTime, CurrentStoreDTM considering the timezone offset
            setCurrentTime();

            setLastUpdateCheck( LocalDateTime.now() );

            //store is not set to allow any orders, return
            if ( !getAllowPickUp() && !getAllowDelivery() ) {
                return this;
            }

            if(withMenuItems) {
            //set suggestive selling keypads if there is an upsell profile ID and the max number of upsell keypads is greater than 0
            if(getMaxNumUpsellKeypads() != null && getMaxNumUpsellKeypads() > 0 && getUpsellProfileID() != null) {

                //Add StoreKeypadModel to cache or else gets stuck in infinite loop
                if ( !MyQCCache.checkCacheForStoreKeypad(getID()) ) {
                    new StoreKeypadModel( this );
                }

                SuggestiveSellingCollection suggestiveSellingCollection = new SuggestiveSellingCollection(getUpsellProfileID(), getID(), getAuthenticatedAccountID());
                setUpsellProfileKeypads(suggestiveSellingCollection.getCollection());

                // Add upsell profile products to StoreKeypadModel
                if ( MyQCCache.checkCacheForStoreKeypad(getID()) ) {
                    StoreKeypadModel storeKeypadModel = MyQCCache.getStoreKeypadFromCache(getID());

                    // If the StoreKeypadModel doesn't have upsell keypads but there are upsell keypads, add to StoreKeypadModel
                    if(storeKeypadModel.getUpsellProfileKeypads().size() == 0 && suggestiveSellingCollection.getCollection().size() > 0) {
                        storeKeypadModel.setUpsellProfileKeypads(suggestiveSellingCollection.getCollection());
                        storeKeypadModel.setUpsellProfileKeypadProducts();
                        MyQCCache.getKeypadCache().put(getID(), storeKeypadModel);
                    }
                }
            }
            }

            //set purchase restriction profile details if there is a restriction on the terminal for the user's spending profile
            if(getPurchaseRestrictionProfileID() != null) {
                PurchaseRestrictionProfileCollection purchaseRestrictionProfileCollection = new PurchaseRestrictionProfileCollection(getPurchaseRestrictionProfileID());
                setPurchaseRestrictionProfileDetails(purchaseRestrictionProfileCollection.getCollection());
            }

            //determines and sets the stores ordering availability (open and close times for pick up and delivery) for this StoreModel instance
            determineAndSetStoreOrderingAvailability();

            //if not using buffering (no buffer schedule or the store is closed), skip all buffering checks and logic
            if ( getBufferScheduleID() == null || ( ( getPickUpOrderingOpenTime() == null || getPickUpOrderingOpenTime().equals("") ) && ( getDeliveryOrderingOpenTime() == null || getDeliveryOrderingOpenTime().equals("") ) ) ) {
                setWaitTimes();
                return this;
            }

            setBuffering();

            setWaitTimes();
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }

        return this;
    }

    //*************** START PICKUP AND DELIVERY SCHEDULE FUNCTIONS ********************//

    //determines and sets the stores ordering availability (open and close times for pick up and delivery) for this StoreModel instance
    public void determineAndSetStoreOrderingAvailability() {
        try {
            //return if the printer host is offline, so the stores are seen as offline as well
            if ( !isPrinterHostOnline( getCurrentStoreDTM() ) ) {
                Logger.logMessage("Printer Host is OFFLINE for Online Ordering Terminal: " + getID() + " - " + getHeader1(), Logger.LEVEL.DEBUG);
                clearSchedules();
                return;
            }

            Logger.logMessage("Printer Host is ONLINE for Online Ordering Terminal: " + getID() + " - " + getHeader1(), Logger.LEVEL.DEBUG);

            //return if the max number of days for future ordering is not encompassed by the future date
            if( !maxFutureOrderDaysValid()) {
                clearSchedules();
                return;
            }

            //apply pickup schedules to store
            if ( getAllowPickUp() ) {
                //get the pickup prep schedule
                ScheduleModel pickUpPrepSchedule = ScheduleModel.getScheduleModel( getPickUpScheduleID(), getCurrentStoreDTM() );

                //apply to store
                applyPrepSchedule( pickUpPrepSchedule, true, false );

                //get the pickup ordering schedule
                ScheduleModel pickUpOrderingSchedule = ScheduleModel.getScheduleModel( getPickUpOrderingScheduleID(), getCurrentStoreDTM() );

                //apply to store
                applyOrderingSchedule(pickUpOrderingSchedule, true);
            }

            //apply delivery schedules to store
            if ( getAllowDelivery() ) {
                //get the delivery prep schedule
                ScheduleModel deliveryPrepSchedule = ScheduleModel.getScheduleModel( getDeliveryScheduleID(), getCurrentStoreDTM() );

                //apply to store
                applyPrepSchedule( deliveryPrepSchedule, false, false );

                //get the delivery ordering schedule
                ScheduleModel deliveryOrderingSchedule = ScheduleModel.getScheduleModel( getDeliveryOrderingScheduleID(), getCurrentStoreDTM() );

                //apply to store
                applyOrderingSchedule( deliveryOrderingSchedule, false );
            }
        } catch (Exception ex) {
            Logger.logException(ex);
        }

        if(getFutureOrderDate() != null) {
            updateFutureOrderTime();
        }
    }

    //determine and set the prep open and close times for the given order type
    private void applyPrepSchedule(ScheduleModel scheduleModel, boolean pickUp, boolean buffer) throws Exception {
        ScheduleDetailCollection scheduleDetails = scheduleModel.getScheduleDetails();

        HashMap<String, ArrayList<String>> prepTimes = scheduleDetails.getOpenCloseTimes(getCurrentStoreDTM());

        ArrayList<String> openTimes = prepTimes.get("OPENTIMES");
        ArrayList<String> closeTimes = prepTimes.get("CLOSETIMES");

        if ( pickUp ) {
            setPickUpOpenTimes(openTimes);
            setPickUpCloseTimes(closeTimes);
            if(closeTimes.size() > 0) {
                setPickupOriginalCloseTime(closeTimes.get(closeTimes.size()-1));
            }
        } else if (buffer) {
            setBufferingOpenTimes(openTimes);
            setBufferingCloseTimes(closeTimes);
        } else {
            setDeliveryOpenTimes(openTimes);
            setDeliveryCloseTimes(closeTimes);
            if(closeTimes.size() > 0) {
                setDeliveryOriginalCloseTime(closeTimes.get(closeTimes.size()-1));
            }
        }
    }

    //determine and set the ordering open and close times for the given order type
    private void applyOrderingSchedule(ScheduleModel scheduleModel, boolean pickUp) throws Exception {
        ScheduleDetailCollection scheduleDetails = scheduleModel.getScheduleDetails();

        HashMap orderingTimes = scheduleDetails.getOrderingTimes( getCurrentStoreDTM() );

        String orderingOpenTime = CommonAPI.convertModelDetailToString(orderingTimes.get("OPENTIME"));
        String orderingCloseTime = CommonAPI.convertModelDetailToString(orderingTimes.get("CLOSETIME"));

        if(getFutureOrderDate() != null) {
            orderingOpenTime = "00:15";
            orderingCloseTime = "23:59";
        }

        if ( pickUp ) {
            setPickUpOrderingOpenTime(orderingOpenTime);
            setPickUpOrderingCloseTime(orderingCloseTime);
        } else {
            setDeliveryOrderingOpenTime(orderingOpenTime);
            setDeliveryOrderingCloseTime(orderingCloseTime);
        }
    }


    //resets all schedule related information in order to display the store as closed and prevent orders
    private void clearSchedules() {
        setPickUpOrderingOpenTime("");
        setPickUpOrderingCloseTime("");
        setPickUpOpenTimes(new ArrayList<>());
        setPickUpCloseTimes(new ArrayList<>());

        setDeliveryOrderingOpenTime("");
        setDeliveryOrderingCloseTime("");
        setDeliveryOpenTimes(new ArrayList<>());
        setDeliveryCloseTimes(new ArrayList<>());
    }

    // Update the time of the future order to be the earlier schedule's opening time, this will hopefully pickup any rotations for this day
    public void updateFutureOrderTime() {
        LocalTime pickupOpenTime = null;
        LocalTime deliveryOpenTime = null;

        String pickupOpenOrdering = "";
        String deliveryOpenOrdering = "";

        if(getPickUpOpenTimes().size() > 0) {
            pickupOpenOrdering = getPickUpOpenTimes().get(0);
        }

        if(getDeliveryOpenTimes().size() > 0) {
            deliveryOpenOrdering = getDeliveryOpenTimes().get(0);
        }

        if(pickupOpenOrdering.contains("AM") || pickupOpenOrdering.contains("PM")) {
            pickupOpenOrdering = CommonAPI.convertToMilitaryTime(pickupOpenOrdering);
        }

        if(deliveryOpenOrdering.contains("AM") || deliveryOpenOrdering.contains("PM")) {
            deliveryOpenOrdering = CommonAPI.convertToMilitaryTime(deliveryOpenOrdering);
        }

        if( !pickupOpenOrdering.equals("") ) {
            pickupOpenTime = LocalTime.parse(pickupOpenOrdering);
        }

        if( !deliveryOpenOrdering.equals("") ) {
            deliveryOpenTime = LocalTime.parse(deliveryOpenOrdering);
        }

        Integer hour = 0;
        Integer minute = 15;

        if(pickupOpenTime != null && deliveryOpenTime != null && deliveryOpenTime.isBefore(pickupOpenTime)) {
            hour = deliveryOpenTime.getHour();
            minute = deliveryOpenTime.getMinute();

        } else if(pickupOpenTime != null && deliveryOpenTime != null) {
            hour = pickupOpenTime.getHour();
            minute = pickupOpenTime.getMinute();

        } else if (pickupOpenTime != null) {
            hour = pickupOpenTime.getHour();
            minute = pickupOpenTime.getMinute();

        } else if (deliveryOpenTime != null) {
            hour = deliveryOpenTime.getHour();
            minute = deliveryOpenTime.getMinute();
        }

        if(hour == 0 && minute < 15) {
            minute = 15;
        }

        LocalDateTime currentTime = getCurrentStoreDTM();
        currentTime = currentTime.withHour(hour);
        currentTime = currentTime.withMinute(minute);

        this.setCurrentStoreDTM( currentTime );

        this.setCurrentStoreTime( currentTime.toLocalTime().toString() );

    }

    //*************** END PICKUP AND DELIVERY SCHEDULE FUNCTIONS ********************//

    //*************** START UPDATE CHECK FUNCTIONS ********************//

    //check if the time passed since the lastOnlineOrderCheck for the Printer Host using this terminal is more than the MyQC Terminal's MaxMinsLastOnlineCheck value
    public boolean isPrinterHostOnline(LocalDateTime currentStoreTime) {
        boolean isOnline = true;

        if(this.getMaxMinsLastOnlineCheck() == null) {
            setPrinterHostOnline(isOnline);
            return isOnline;

        } else {

            // If future order date, look at the real store time right now
            if(getFutureOrderDate() != null) {
                currentStoreTime = LocalDateTime.now();

                if ( getTimezoneOffset() != null && getTimezoneOffset() != 0 ) {
                    currentStoreTime = currentStoreTime.plusHours( getTimezoneOffset() );
                }
            }

            //get the lastOnlineOrderCheck from the printer host for the My_QCTerminal
            ArrayList<HashMap> lastOnlineOrderCheckList = dm.serializeSqlWithColNames("data.ordering.getStoreLastOnlineOrderCheck",
                    new Object[]{
                            getID()
                    },
                    false,
                    false
            );

            if(lastOnlineOrderCheckList != null && lastOnlineOrderCheckList.size() > 0) {
                HashMap lastOnlineOrderCheckHM = lastOnlineOrderCheckList.get(0);
                if(lastOnlineOrderCheckHM.get("LASTONLINEORDERCHECK") != null && !lastOnlineOrderCheckHM.get("LASTONLINEORDERCHECK").toString().equals("")) {

                    String lastOnlineOrderCheck = lastOnlineOrderCheckHM.get("LASTONLINEORDERCHECK").toString();
                    lastOnlineOrderCheck = lastOnlineOrderCheck.replace(" ", "T");

                    LocalDateTime lastOnlineOrderCheckTime = LocalDateTime.parse(lastOnlineOrderCheck);

                    Integer timeDifference = currentStoreTime.compareTo(lastOnlineOrderCheckTime);

                    if(timeDifference > 0) { //if now is after lastOnlineOrderCheck

                        //get the number of seconds that have passed since the lastOnlineOrderCheck time
                        long seconds = lastOnlineOrderCheckTime.until(currentStoreTime, ChronoUnit.SECONDS);

                        //convert that into a float number to get the most accurate number of minutes
                        float minutes = ((float)seconds / (float)60);

                        //if the number of minutes passed is more than the maxMinsLastOnlineCheck for the store, store isn't available
                        if(minutes > this.getMaxMinsLastOnlineCheck()) {
                            isOnline = false;
                        }
                    }
                }
            }
        }

        setPrinterHostOnline(isOnline);
        return isOnline;
    }

    //checks against the value of LastUpdateDTM to see if the model has been updated since last being cached
    public boolean checkForUpdates() throws Exception {
        boolean needsUpdate = true;

        Object result = dm.parameterizedExecuteScalar("data.ordering.checkStoreForUpdates",
                new Object[]{
                        getID(),
                        getLastUpdateDTM().toLocalDate().toString() + " " + getLastUpdateDTM().plusSeconds(1).toLocalTime().toString()
                }
        );

        //store needs to be updated if there is no lastUpdateDTM or if the result is 1
        needsUpdate = ( result == null || result.toString().equals("1") );

        return needsUpdate;
    }

    public boolean maxFutureOrderDaysValid() throws Exception {
        if(getFutureOrderDate() == null) {
            return true;
        }

        //get difference between today's date and the future date
        LocalDate today = LocalDate.now();
        long daysBetween = ChronoUnit.DAYS.between(today, getFutureOrderDate());

        //check the number of days between the two days is not greater than the max
        return daysBetween <= getMaxNumDaysForFutureOrders();
    }

    //*************** END UPDATE CHECK FUNCTIONS ********************//

    //*************** START WAIT TIME FUNCTIONS ********************//

    //set the wait time as the number of minutes between now and the next available interval
    public void setWaitTimes() {
        if(!isShowWaitTime()) {
            setPickupWaitTime("");
            setDeliveryWaitTime("");
            return;
        }

        if(getAllowPickUp()) {

            //get the number of minutes between now and the earliest pickup order
            LocalTime pickupASAP = determineASAPTime(getPickUpOpenTimes(), getPickUpCloseTimes(), getPickUpPrepTime());

            if(pickupASAP != null) {
                pickupASAP = pickupASAP.plusMinutes(1);
                String pickupWaitTime = formatWaitTime(pickupASAP);

                //if using pickup and delivery show as "Pickup Time: 25 - 35 minutes", if just using pickup show as 25 - 30 minutes or is no wait time show as blank
                String pickupWaitTimeStr = pickupWaitTime.equals("") ? pickupWaitTime : "Next Pickup Time: " + pickupWaitTime;
                setPickupWaitTime(pickupWaitTimeStr);
            }
        }

        if(getAllowDelivery()) {
            Integer deliveryPrepTime = getPickUpPrepTime() + getDeliveryPrepTime();

            //get the number of minutes between now and the earliest delivery order
            LocalTime deliveryASAP = determineASAPTime(getDeliveryOpenTimes(), getDeliveryCloseTimes(), deliveryPrepTime);

            if(deliveryASAP != null) {
                deliveryASAP = deliveryASAP.plusMinutes(1);
                String deliveryWaitTime = formatWaitTime(deliveryASAP);

                //if using pickup and delivery show as "Delivery Time: 25 - 35 minutes", if just using delivery show as 25 - 30 minutes or if not wait time show as blank
                String deliveryWaitTimeStr = deliveryWaitTime.equals("") ? deliveryWaitTime : "Next Delivery Time: " + deliveryWaitTime;
                setDeliveryWaitTime(deliveryWaitTimeStr);
            }
        }
    }

    //determine what the soonest possible order time is based on the schedule open and close time
    public LocalTime determineASAPTime(ArrayList<String> storeOpenTimes, ArrayList<String> storeCloseTimes, Integer prepTime) {
        LocalTime ASAP = LocalTime.parse(getCurrentStoreTime());

        for(int i=0; i<storeOpenTimes.size(); i++) {
            LocalTime readyTime = LocalTime.now();
            if(i >= storeOpenTimes.size() || i >= storeCloseTimes.size() || storeOpenTimes.get(i) == null || storeCloseTimes.get(i) == null) {
                return null;
            }
            LocalTime storeOpen = LocalTime.parse(CommonAPI.convertToMilitaryTime(storeOpenTimes.get(i)));
            LocalTime storeClose = LocalTime.parse(CommonAPI.convertToMilitaryTime(storeCloseTimes.get(i)));

            if(ASAP.isBefore(storeOpen)) {
                readyTime = storeOpen.plusMinutes(prepTime);
            } else {
                LocalTime time = ASAP;
                if (time.plusMinutes(prepTime).isBefore(time)) {
                    ASAP = LocalTime.parse("23:59");
                } else {
                    ASAP = ASAP.plusMinutes(prepTime);
                }
                readyTime = ASAP;
            }

            LocalTime orderReady = readyTime;

            LocalTime closeTime = storeClose;
            if(closeTime.plusMinutes(prepTime).getHour() == 0) {
                storeClose = LocalTime.parse("23:59");
            } else {
                storeClose = storeClose.plusMinutes(prepTime);
            }

            //if the prep time is 0 then the storeOpen and orderReady times will be the same, shouldn't be configured this way but still need to handle this scenario
            if(prepTime == 0 && storeOpen.compareTo(orderReady) == 0 && (storeClose.isAfter(orderReady) || storeClose.compareTo(orderReady) == 0) ) {
                return readyTime;
            }

            if(storeOpen.isBefore(orderReady) && (storeClose.isAfter(orderReady) || storeClose.compareTo(orderReady) == 0) ) {
                return readyTime;
            }
        }

        return null;
    }

    //formats the wait time so it shown as "10 - 20 minutes" or "1 hour 5 minutes - 1 hour 15 minutes"
    public String formatWaitTime(LocalTime ASAP) {
        LocalTime now = LocalTime.parse(getCurrentStoreTime());

        //add one minutes to the current time to make up for ordering time
        now = now.plusMinutes(1);

        long waitTimeMinutes = now.until(ASAP, ChronoUnit.MINUTES);

        if (waitTimeMinutes < 0) {
            return "";
        }

        long waitTimeStart = 1;
        long waitTimeEnd = waitTimeMinutes + 5;

        if( (waitTimeMinutes - 5) > 0) {
            waitTimeStart = waitTimeMinutes - 5;
        }

        try {

            long waitTimeStartHours = waitTimeStart / 60;
            long waitTimeStartMins = waitTimeStart % 60;

            long waitTimeEndHours = waitTimeEnd / 60;
            long waitTimeEndMins = waitTimeEnd % 60;

            String waitTimeStartStr = "";
            if(waitTimeStartHours == 1) {
                waitTimeStartStr = waitTimeStartHours + " hr " + waitTimeStartMins + " mins " + " - ";

            } else if(waitTimeStartHours > 1) {
                waitTimeStartStr = waitTimeStartHours + " hrs " + waitTimeStartMins + " mins " + " - ";

            } else {
                waitTimeStartStr = waitTimeStart + " - ";
            }

            String waitTimeEndStr = "";
            if(waitTimeEndHours == 1) {
                waitTimeEndStr = waitTimeEndHours + " hr " + waitTimeEndMins + " mins ";

            } else if(waitTimeEndHours > 1) {
                waitTimeEndStr = waitTimeEndHours + " hrs " + waitTimeEndMins + " mins ";

            } else {
                waitTimeEndStr = waitTimeEnd + " mins";
            }

            return waitTimeStartStr + waitTimeEndStr;

        } catch (Exception ex) {
            Logger.logMessage("Issue formatting wait time into hours and minutes.", Logger.LEVEL.DEBUG);
            return waitTimeStart + " - " + waitTimeEnd + " minutes";
        }
    }

    //*************** END WAIT TIME FUNCTIONS ********************//

    //*************** START BUFFERING FUNCTIONS ********************//

    public void setBuffering() throws Exception {
        //get buffering schedule model
        ScheduleModel bufferSchedule = ScheduleModel.getScheduleModel( getBufferScheduleID(), getCurrentStoreDTM() );

        //check if the buffer schedule model is valid
        if ( bufferSchedule == null || bufferSchedule.getID() == null ) {
            Logger.logMessage("Buffering Schedule " + getBufferScheduleID() + " could not be loaded properly", Logger.LEVEL.DEBUG);
            setUsingBuffering( false );
            return;
        }

        //check the buffer schedule is active
        if ( bufferSchedule.getActive().equals( false ) ) {
            Logger.logMessage("Buffering Schedule " + getBufferScheduleID() + " is not active, skipping buffer checks", Logger.LEVEL.DEBUG);
            setUsingBuffering( false );
            return;
        }

        //check the buffer schedule details
        if ( bufferSchedule.getScheduleDetails() == null || bufferSchedule.getScheduleDetails().getCollection() == null || bufferSchedule.getScheduleDetails().getCollection().size() == 0 ) {
            Logger.logMessage("Buffering Schedule " + getBufferScheduleID() + " has no valid schedule details", Logger.LEVEL.DEBUG);
            setUsingBuffering(false);
            return;
        }

        setUsingBuffering( true );

        //reset buffer interval list
        setBufferIntervalList( new ArrayList<>() );

        //set buffering open and close times on storeModel
        applyPrepSchedule( bufferSchedule, false, true );

        IntervalCollection bufferIntervals = new IntervalCollection();

        //create list of intervals from each buffer schedule detail's start and end time
        bufferIntervals.createBufferIntervalList(bufferSchedule);

        //update the buffer intervals with the number of transactions made in each interval
        bufferIntervals.updateIntervalCounts(this);

        //determine if the intervals should be open based on the schedule detail's max total orders properties
        for(ScheduleDetailModel bufferScheduleDetailModel : bufferSchedule.getScheduleDetails().getCollection()) {
            bufferScheduleDetailModel.determineBufferScheduleOpen(bufferIntervals);
        }

        setBufferIntervalList(bufferIntervals.getCollection());

        //once we have a final buffer list of open and closed interval, use that to build final pickup and delivery lists
        buildScheduleIntervalLists();
    }

    //creates a list of intervals from the pickup or delivery schedule, updates intervals with buffering details
    public void buildScheduleIntervalLists() throws Exception {

        IntervalCollection pickupIntervals = new IntervalCollection();
        IntervalCollection deliveryIntervals = new IntervalCollection();

        //create list of pickup intervals from open and close times
        if(getAllowPickUp()) {
            pickupIntervals.createIntervalList(getPickUpOpenTimes(), getPickUpCloseTimes());

            //update lists with buffering interval details
            pickupIntervals.updateScheduleFromBufferIntervals(getBufferIntervalList(), 3);

            createOpenAndCloseTimes(pickupIntervals, 3);
        }

        //create list of delivery intervals from open and close times
        if(getAllowDelivery()) {
            deliveryIntervals.createIntervalList(getDeliveryOpenTimes(), getDeliveryCloseTimes());

            //update lists with buffering interval details
            deliveryIntervals.updateScheduleFromBufferIntervals(getBufferIntervalList(), 2);

            createOpenAndCloseTimes(deliveryIntervals, 2);
        }
    }

    public void createOpenAndCloseTimes(IntervalCollection intervalCollection, Integer orderType) throws Exception {
        ArrayList<String> openTimes = new ArrayList<>();
        ArrayList<String> closeTimes = new ArrayList<>();

        Integer index = 0;
        IntervalModel previousOpenInterval = new IntervalModel();
        for(IntervalModel intervalModel : intervalCollection.getCollection()) {

            //check if we need to add an open time
            if(openTimes.size() == closeTimes.size()) {

                //check if the interval is open, if it is not then look at the next interval
                if( isBufferIntervalOpen(intervalModel, orderType) )  {
                    openTimes.add(intervalModel.getIntervalStartTime());
                    previousOpenInterval = intervalModel;

                    //if this interval is also the last interval then add a closing time
                    if(index.equals(intervalCollection.getCollection().size() - 1)) {
                        closeTimes.add(intervalModel.getIntervalEndTime());
                    }
                }

                //if this is the last interval in the list
            } else if(index.equals(intervalCollection.getCollection().size() - 1)) {
                if( isBufferIntervalOpen(intervalModel, orderType) ) {
                    closeTimes.add(intervalModel.getIntervalEndTime());
                } else {
                    closeTimes.add(intervalModel.getIntervalStartTime());
                }

                //if there is a gap in the schedule the previous interval's end time won't equal the next interval's start time
            } else if ( !(previousOpenInterval.getIntervalEndTime().equals(intervalModel.getIntervalStartTime())) ) {
                closeTimes.add(previousOpenInterval.getIntervalStartTime());

                if ( isBufferIntervalOpen(intervalModel, orderType) ) {
                    openTimes.add(intervalModel.getIntervalStartTime());
                    previousOpenInterval = intervalModel;
                }

            } else if ( !isBufferIntervalOpen(intervalModel, orderType) ) {
                closeTimes.add(previousOpenInterval.getIntervalStartTime());

            } else {
                previousOpenInterval = intervalModel;
            }

            index++;
        }

        validateOpenAndCloseTimes(openTimes, closeTimes, orderType);
    }

    //check that the time right now is before the schedule closing times
    public void validateOpenAndCloseTimes(ArrayList<String> openTimes, ArrayList<String> closeTimes, Integer orderType) {
        ArrayList<String> validOpenTimes = new ArrayList<>();
        ArrayList<String> validCloseTimes = new ArrayList<>();

        //loop over each open and close time
        for(int i=0; i<openTimes.size(); i++) {
            LocalTime currentStoreTime = LocalTime.parse(getCurrentStoreTime());
            LocalTime closeTime = LocalTime.parse(closeTimes.get(i));

            //if the current store time is after the schedule close time then the schedule is not open
            if(!(currentStoreTime.isAfter(closeTime)) || closeTime.compareTo(LocalTime.parse("00:00")) == 0) {
                validOpenTimes.add(openTimes.get(i));
                if (closeTimes.get(i).equals("00:00")) {
                    validCloseTimes.add("23:59");
                } else {
                    validCloseTimes.add(closeTimes.get(i));
                }
            }
        }

        if(orderType.equals(3)) {
            setPickUpOpenTimes(validOpenTimes);
            setPickUpCloseTimes(validCloseTimes);
        } else {
            setDeliveryOpenTimes(validOpenTimes);
            setDeliveryCloseTimes(validCloseTimes);
        }
    }

    //check if the interval is open AND the order type is pickup and interval is open for pickup OR the order type is delivery and the interval is open for delivery
    public boolean isBufferIntervalOpen(IntervalModel intervalModel, Integer orderType) {

        return intervalModel.isIntervalOpen() && ((orderType.equals(3) && intervalModel.isPickupOpen()) || (orderType.equals(2) && intervalModel.isDeliveryOpen()));
    }

    //takes the time the order will be picked up or delivered and returns the time the order start to be prepared
    public LocalDateTime setPreparingOrderStartTime(String readyByTime, LocalDateTime orderSubmittedDateTime, LocalDate futureOrderDate, Integer prepTime) {
        LocalTime orderTime = LocalTime.parse(readyByTime);
        LocalDateTime estimateOrderTime = orderSubmittedDateTime;

        if(futureOrderDate != null) {
            estimateOrderTime = estimateOrderTime.withDayOfMonth(getFutureOrderDate().getDayOfMonth());
            estimateOrderTime = estimateOrderTime.withDayOfYear(getFutureOrderDate().getDayOfYear());
        }

        estimateOrderTime = estimateOrderTime.withHour(orderTime.getHour());
        estimateOrderTime = estimateOrderTime.withMinute(orderTime.getMinute());
        estimateOrderTime = estimateOrderTime.withSecond(orderTime.getSecond());

        //subtract the prep time from the ready time
        orderTime = orderTime.minusMinutes(prepTime);

        estimateOrderTime = estimateOrderTime.minusMinutes(prepTime);

        return estimateOrderTime;
    }

    //checks that the interval is valid for the preparing start time
    public Boolean checkInterval(LocalDateTime startTime, String orderType) {
        LocalTime orderTime = startTime.toLocalTime();

        //if the order wasn't placed during a buffer schedule then it's valid
        if( !isTransactionInBufferSchedule(orderTime) ) {
            return true;
        }

        //if no transactions have been made in the buffer intervals, all intervals are considered open
        if(getBufferIntervalList().size() == 0) {
            return true;
        }

        //if the order was placed in a buffer schedule check if the buffer pickup interval is open
        if(orderType.equals("pickup") && isIntervalOpen(orderTime, 3)) {
            return true;
        }

        //if the order was placed in a buffer schedule check if the buffer delivery interval is open
        if(orderType.equals("delivery") && isIntervalOpen(orderTime, 2)) {
            return true;
        }

        return false;
    }

    public boolean isIntervalOpen(LocalTime orderTime, Integer orderType) {
        boolean isIntervalValid = false;

        for(IntervalModel bufferIntervalModel : getBufferIntervalList()) {

            LocalTime bufferStartTime = bufferIntervalModel.getLocalStartTime();
            LocalTime bufferEndTime = bufferIntervalModel.getLocalEndTime();
            LocalTime scheduleStart = LocalTime.parse(bufferIntervalModel.getScheduleStartTime());
            LocalTime scheduleEnd = LocalTime.parse(bufferIntervalModel.getScheduleEndTime());

            boolean isBetweenInterval = orderTime.isAfter(bufferStartTime) && orderTime.isBefore(bufferEndTime);
            boolean isStartOfInterval = orderTime.compareTo(bufferStartTime) == 0;
            boolean isStartOrEndOfSchedule = (orderTime.compareTo(bufferStartTime) == 0 && bufferEndTime.compareTo(scheduleEnd) == 0) ||  (orderTime.compareTo(bufferStartTime) == 0 && bufferStartTime.compareTo(scheduleStart) == 0);

            boolean bufferOpen = isBufferIntervalOpen(bufferIntervalModel, orderType);

            //check if the transaction time is between the interval's start and end and the interval is open
            if(isBetweenInterval && bufferOpen) {
                isIntervalValid = true;
                break;

                //otherwise check if the transaction time is the same as the interval's start time and the interval is open
            } else if (isStartOfInterval && bufferOpen) {
                isIntervalValid = true;
                break;

                //lastly check if the transaction time is the same as the interval start or end time and if the interval start or end time is the same as the schedule start or end time
            } else if (isStartOrEndOfSchedule && bufferOpen) {
                isIntervalValid = true;
                break;
            }
        }

        return isIntervalValid;
    }

    public boolean isTransactionInBufferSchedule(LocalTime orderTime) {

        for(int i=0; i<getBufferingOpenTimes().size(); i++) {
            LocalTime bufferScheduleStart = LocalTime.parse(getBufferingOpenTimes().get(i));
            LocalTime bufferScheduleEnd = LocalTime.parse(getBufferingCloseTimes().get(i));

            if( (orderTime.isAfter(bufferScheduleStart) && orderTime.isBefore(bufferScheduleEnd)) || orderTime.compareTo(bufferScheduleStart)==0 ||  orderTime.compareTo(bufferScheduleEnd) == 0) {
                return true;
            }
        }
        return false;
    }

    //*************** END BUFFERING FUNCTIONS ********************//

    //*************** START DISCLAIMER FUNCTIONS ********************//

    public void getAndSetStoreDisclaimers(){
        try{
            Integer storeID = getID();
            DisclaimerCollection storeDisclaimers = new DisclaimerCollection();

            storeDisclaimers.getAllDisclaimersByStoreId(storeID);

            // add the store disclaimers
            for (DisclaimerModel storeDisclaimer : storeDisclaimers.getCollection()) {
                getDisclaimers().add(storeDisclaimer);
            }

        }
        catch(Exception e){
            Logger.logException(e);
        }
    }

    //*************** END DISCLAIMER FUNCTIONS ********************//

    //getter
    public Integer getID() {
        return this.id;
    }

    //setter
    public void setID(Integer id) {
        this.id = id;
    }

    //getter
    public Integer getHomeKeypadID() {
        return this.homeKeypadID;
    }

    //setter
    public void setHomeKeypadID(Integer homeKeypadID) {
        this.homeKeypadID = homeKeypadID;
    }

    //getter
    public String getHeader1() {
        return header1;
    }

    //setter
    public void setHeader1(String header1) {
        this.header1 = header1;
    }

    //getter
    public String getHeader2() {
        return header2;
    }

    //setter
    public void setHeader2(String header2) {
        this.header2 = header2;
    }

    //getter
    public String getHeader3() {
        return header3;
    }

    //setter
    public void setHeader3(String header3) {
        this.header3 = header3;
    }

    //getter
    public String getHeader4() {
        return header4;
    }

    //setter
    public void setHeader4(String header4) {
        this.header4 = header4;
    }

    //getter
    public String getHeader5() {
        return header5;
    }

    //setter
    public void setHeader5(String header5) {
        this.header5 = header5;
    }

    //getter
    public String getVenueCategory() {
        return venueCategory;
    }

    //setter
    public void setVenueCategory(String venueCategory) {
        this.venueCategory = venueCategory;
    }

    //getter
    public ArrayList<String> getPickUpOpenTimes() {
        return pickUpOpenTimes;
    }

    //adder
    private void addPickUpOpenTime(String pickUpOpenTime) {
        if (!pickUpOpenTime.isEmpty()) {
            pickUpOpenTime = LocalTime.parse(pickUpOpenTime).format(DateTimeFormatter.ofPattern("hh:mm a"));
        }

        if(this.pickUpOpenTimes.contains(pickUpOpenTime)) {
            return;
        }

        this.pickUpOpenTimes.add(pickUpOpenTime);
    }

    private void resetPickUpOpenTimes() {
        this.pickUpOpenTimes = new ArrayList<String>();
    }

    //setter
    public void setPickUpOpenTimes(ArrayList<String> pickUpOpenTimes) {
        resetPickUpOpenTimes();

        for ( String openTime : pickUpOpenTimes ) {
            addPickUpOpenTime(openTime);
        }
    }

    //getter
    public ArrayList<String> getPickUpCloseTimes() {
        return pickUpCloseTimes;
    }

    //adder
    private void addPickUpCloseTime(String pickUpCloseTime) {
        if (!pickUpCloseTime.isEmpty()) {
            pickUpCloseTime = LocalTime.parse(pickUpCloseTime).format(DateTimeFormatter.ofPattern("hh:mm a"));
        }

        if(this.pickUpCloseTimes.contains(pickUpCloseTime)) {
            return;
        }

        this.pickUpCloseTimes.add(pickUpCloseTime);
    }

    private void resetPickUpCloseTimes() {
        this.pickUpCloseTimes = new ArrayList<String>();
    }

    //setter
    public void setPickUpCloseTimes(ArrayList<String> pickUpCloseTimes) {
        resetPickUpCloseTimes();

        for ( String closeTime : pickUpCloseTimes ) {
            addPickUpCloseTime(closeTime);
        }
    }


    //getter
    public String getPickUpOrderingOpenTime() {
        return pickUpOrderingOpenTime;
    }

    //setter
    public void setPickUpOrderingOpenTime(String pickUpOrderingOpenTime) {
        Integer prepTime = 0; //prep time does not effect open times
        if (!pickUpOrderingOpenTime.isEmpty()) {
            pickUpOrderingOpenTime = LocalTime.parse(pickUpOrderingOpenTime).minusMinutes(prepTime).format(DateTimeFormatter.ofPattern("hh:mm a"));
        }
        this.pickUpOrderingOpenTime = pickUpOrderingOpenTime;
    }

    //getter
    public String getPickUpOrderingCloseTime() {
        return pickUpOrderingCloseTime;
    }

    //setter
    public void setPickUpOrderingCloseTime(String pickUpOrderingCloseTime) {
        if (!pickUpOrderingCloseTime.isEmpty()) {
            pickUpOrderingCloseTime = LocalTime.parse(pickUpOrderingCloseTime).format(DateTimeFormatter.ofPattern("hh:mm a"));
        }
        this.pickUpOrderingCloseTime = pickUpOrderingCloseTime;
    }

    //getter
    public Integer getPickUpPrepTime() {
        return this.pickUpPrepTime;
    }

    //setter
    public void setPickUpPrepTime(Integer pickUpPrepTime) {
        this.pickUpPrepTime = pickUpPrepTime;
    }

    //getter
    public ArrayList<String> getDeliveryOpenTimes() {
        return deliveryOpenTimes;
    }

    private void addDeliveryOpenTime(String deliveryOpenTime) {
        if (!deliveryOpenTime.isEmpty()) {
            deliveryOpenTime = LocalTime.parse(deliveryOpenTime).format(DateTimeFormatter.ofPattern("hh:mm a"));
        }
        this.deliveryOpenTimes.add(deliveryOpenTime);
    }

    private void resetDeliveryOpenTimes() {
        this.deliveryOpenTimes = new ArrayList<String>();
    }

    //setter
    public void setDeliveryOpenTimes(ArrayList<String> deliveryOpenTimes) {
        resetDeliveryOpenTimes();

        for (String openTime : deliveryOpenTimes ) {
            addDeliveryOpenTime(openTime);
        }
    }

    //getter
    public ArrayList<String> getDeliveryCloseTimes() {
        return deliveryCloseTimes;
    }

    public void addDeliveryCloseTime(String deliveryCloseTime) {
        if (!deliveryCloseTime.isEmpty()) {
            deliveryCloseTime = LocalTime.parse(deliveryCloseTime).format(DateTimeFormatter.ofPattern("hh:mm a"));
        }
        this.deliveryCloseTimes.add(deliveryCloseTime);
    }

    private void resetDeliveryCloseTimes() {
        this.deliveryCloseTimes = new ArrayList<String>();
    }

    //setter
    public void setDeliveryCloseTimes(ArrayList<String> deliveryCloseTimes) {
        resetDeliveryCloseTimes();

        for (String closeTime : deliveryCloseTimes ) {
            addDeliveryCloseTime(closeTime);
        }
    }

    //getter
    public String getDeliveryOrderingOpenTime() {
        return deliveryOrderingOpenTime;
    }

    //setter
    public void setDeliveryOrderingOpenTime(String deliveryOrderingOpenTime) {
        Integer prepTime = 0; //prep time does not effect open times
        if (!deliveryOrderingOpenTime.isEmpty()) {
            deliveryOrderingOpenTime = LocalTime.parse(deliveryOrderingOpenTime).minusMinutes(prepTime).format(DateTimeFormatter.ofPattern("hh:mm a"));
        }
        this.deliveryOrderingOpenTime = deliveryOrderingOpenTime;
    }

    //getter
    public String getDeliveryOrderingCloseTime() {
        return deliveryOrderingCloseTime;
    }

    //setter
    public void setDeliveryOrderingCloseTime(String deliveryOrderingCloseTime) {
        if (!deliveryOrderingCloseTime.isEmpty()) {
            deliveryOrderingCloseTime = LocalTime.parse(deliveryOrderingCloseTime).format(DateTimeFormatter.ofPattern("hh:mm a"));
        }
        this.deliveryOrderingCloseTime = deliveryOrderingCloseTime;
    }

    //getter
    public Integer getDeliveryPrepTime() {
        return this.deliveryPrepTime;
    }

    //setter
    public void setDeliveryPrepTime(Integer deliveryPrepTime) {
        this.deliveryPrepTime = deliveryPrepTime;
    }

    //getter
    public Boolean getAllowPickUp() {
        return this.allowPickUp;
    }

    //setter
    public void setAllowPickUp(Boolean allowPickUp) {
        this.allowPickUp = allowPickUp;
    }

    //getter
    public Boolean getAllowDelivery() {
        return this.allowDelivery;
    }

    //setter
    public void setAllowDelivery(Boolean allowDelivery) {
        this.allowDelivery = allowDelivery;
    }

    //getter
    public BigDecimal getDeliveryMinimum() { return deliveryMinimum; }

    //setter
    public void setDeliveryMinimum(BigDecimal deliveryMinimum) { this.deliveryMinimum = deliveryMinimum; }

    //getter
    private Integer getAuthenticatedAccountID() {
        return this.authenticatedAccountID;
    }

    //setter
    private void setAuthenticatedAccountID(Integer authenticatedAccountID) {
        this.authenticatedAccountID = authenticatedAccountID;
    }

    //getter
    public Integer getReAuthTypeID() {
        return this.reAuthTypeID;
    }

    //setter
    public void setReAuthTypeID(Integer reAuthTypeID) {
        this.reAuthTypeID = reAuthTypeID;
    }

    //getter
    public Integer getTimezoneOffset() {
        return timezoneOffset;
    }

    //setter
    public void setTimezoneOffset(Integer timezoneOffset) {
        this.timezoneOffset = timezoneOffset;
    }

    public String getCurrentStoreTime() {
        return currentStoreTime;
    }

    public void setCurrentStoreTime(String currentStoreTime) {
        this.currentStoreTime = currentStoreTime;
    }

    public LocalDateTime getCurrentStoreDTM() {
        return currentStoreDTM;
    }

    public void setCurrentStoreDTM(LocalDateTime currentStoreDTM) {
        this.currentStoreDTM = currentStoreDTM;
    }

    public void setCurrentTime() throws Exception {
        LocalDateTime currentTime = LocalDateTime.now();

        if ( getTimezoneOffset() != null && getTimezoneOffset() != 0 ) {
            currentTime = currentTime.plusHours( getTimezoneOffset() );
        }

        if( getFutureOrderDate() != null ) {
            currentTime = currentTime.withDayOfMonth(getFutureOrderDate().get(ChronoField.DAY_OF_MONTH));
            currentTime = currentTime.withMonth(getFutureOrderDate().get(ChronoField.MONTH_OF_YEAR));
            currentTime = currentTime.withYear(getFutureOrderDate().get(ChronoField.YEAR));
            currentTime = currentTime.withHour(0);
            currentTime = currentTime.withMinute(15);
        }

        this.setCurrentStoreDTM( currentTime );

        this.setCurrentStoreTime( currentTime.toLocalTime().toString() );
    }

    //getter
    public boolean getIgnoreInventoryCounts() {
        return ignoreInventoryCounts;
    }

    //setter
    public void setIgnoreInventoryCounts(boolean ignoreInventoryCounts) {
        this.ignoreInventoryCounts = ignoreInventoryCounts;
    }

    public Boolean getUsingBuffering() {
        return usingBuffering;
    }

    public void setUsingBuffering(Boolean usingBuffering) {
        this.usingBuffering = usingBuffering;
    }

    public ArrayList<String> getBufferingOpenTimes() {
        return bufferingOpenTimes;
    }

    public void setBufferingOpenTimes(ArrayList<String> bufferingOpenTimes) {
        this.bufferingOpenTimes = bufferingOpenTimes;
    }

    public ArrayList<String> getBufferingCloseTimes() {
        return bufferingCloseTimes;
    }

    public void setBufferingCloseTimes(ArrayList<String> bufferingCloseTimes) {
        this.bufferingCloseTimes = bufferingCloseTimes;
    }

    public boolean isRequirePhoneNum() {
        return requirePhoneNum;
    }

    public void setRequirePhoneNum(boolean requirePhoneNum) {
        this.requirePhoneNum = requirePhoneNum;
    }

    public boolean isUsesSMSNotifications() {
        return usesSMSNotifications;
    }

    public void setUsesSMSNotifications(boolean usesSMSNotifications) {
        this.usesSMSNotifications = usesSMSNotifications;
    }

    public boolean isUsesEatInTakeOut() {
        return usesEatInTakeOut;
    }

    public void setUsesEatInTakeOut(boolean usesEatInTakeOut) {
        this.usesEatInTakeOut = usesEatInTakeOut;
    }

    public boolean isUsesCreditCardAsTender() {
        return usesCreditCardAsTender;
    }

    public void setUsesCreditCardAsTender(boolean usesCreditCardAsTender) {
        this.usesCreditCardAsTender = usesCreditCardAsTender;
    }

    public Integer getMaxMinsLastOnlineCheck() {
        return maxMinsLastOnlineCheck;
    }

    public void setMaxMinsLastOnlineCheck(Integer maxMinsLastOnlineCheck) {
        this.maxMinsLastOnlineCheck = maxMinsLastOnlineCheck;
    }

    @JsonIgnore
    public boolean isPrinterHostOnline() {
        return isPrinterHostOnline;
    }

    public void setPrinterHostOnline(boolean printerHostOnline) {
        isPrinterHostOnline = printerHostOnline;
    }

    public boolean isEnableFavorites() {
        return enableFavorites;
    }

    public void setEnableFavorites(boolean enableFavorites) {
        this.enableFavorites = enableFavorites;
    }

    public Integer getPickUpScheduleID() {
        return pickUpScheduleID;
    }

    @JsonIgnore
    public void setPickUpScheduleID(Integer pickUpScheduleID) {
        this.pickUpScheduleID = pickUpScheduleID;
    }

    public Integer getPickUpOrderingScheduleID() {
        return pickUpOrderingScheduleID;
    }

    public void setPickUpOrderingScheduleID(Integer pickUpOrderingScheduleID) {
        this.pickUpOrderingScheduleID = pickUpOrderingScheduleID;
    }

    public Integer getDeliveryScheduleID() {
        return deliveryScheduleID;
    }

    @JsonIgnore
    public void setDeliveryScheduleID(Integer deliveryScheduleID) {
        this.deliveryScheduleID = deliveryScheduleID;
    }

    public Integer getDeliveryOrderingScheduleID() {
        return deliveryOrderingScheduleID;
    }

    @JsonIgnore
    public void setDeliveryOrderingScheduleID(Integer deliveryOrderingScheduleID) {
        this.deliveryOrderingScheduleID = deliveryOrderingScheduleID;
    }

    public Integer getBufferScheduleID() {
        return bufferScheduleID;
    }

    @JsonIgnore
    public void setBufferScheduleID(Integer bufferScheduleID) {
        this.bufferScheduleID = bufferScheduleID;
    }

    public LocalDateTime getLastUpdateDTM() {
        return lastUpdateDTM;
    }

    public void setLastUpdateDTM(LocalDateTime lastUpdateDTM) {
        this.lastUpdateDTM = lastUpdateDTM;
    }

    public LocalDateTime getLastUpdateCheck() {
        return lastUpdateCheck;
    }

    public void setLastUpdateCheck(LocalDateTime lastUpdateCheck) {
        this.lastUpdateCheck = lastUpdateCheck;
    }

    public String getPickupWaitTime() {
        return pickupWaitTime;
    }

    public void setPickupWaitTime(String pickupWaitTime) {
        this.pickupWaitTime = pickupWaitTime;
    }

    public String getDeliveryWaitTime() {
        return deliveryWaitTime;
    }

    public void setDeliveryWaitTime(String deliveryWaitTime) {
        this.deliveryWaitTime = deliveryWaitTime;
    }

    public Integer getDiningOptionKeypadID() {
        return diningOptionKeypadID;
    }

    public boolean getUseFundingTerminal() { return useFundingTerminal;}

    public boolean getUseOnlineOrderingFunding() {return useOnlineOrderingFunding;}

    @JsonIgnore
    public void setDiningOptionKeypadID(Integer diningOptionKeypadID) {
        this.diningOptionKeypadID = diningOptionKeypadID;
    }

    public BigDecimal getPercentOfPopularProducts() {
        return percentOfPopularProducts;
    }

    @JsonIgnore
    public void setPercentOfPopularProducts(BigDecimal percentOfPopularProducts) {
        this.percentOfPopularProducts = percentOfPopularProducts;
    }

    public Integer getNumMinutesUntilStoreClose() {
        return numMinutesUntilStoreClose;
    }

    public void setNumMinutesUntilStoreClose(Integer numMinutesUntilStoreClose) {
        this.numMinutesUntilStoreClose = numMinutesUntilStoreClose;
    }

    public List<IntervalModel> getBufferIntervalList() {
        return bufferIntervalList;
    }

    public void setBufferIntervalList(List<IntervalModel> bufferIntervalList) {
        this.bufferIntervalList = bufferIntervalList;
    }

    public Integer getMaxNumDaysForFutureOrders() {
        return maxNumDaysForFutureOrders;
    }

    public void setMaxNumDaysForFutureOrders(Integer maxNumDaysForFutureOrders) {
        this.maxNumDaysForFutureOrders = maxNumDaysForFutureOrders;
    }

    public LocalDate getFutureOrderDate() {
        return futureOrderDate;
    }

    public void setFutureOrderDate(LocalDate futureOrderDate) {
        this.futureOrderDate = futureOrderDate;
    }

    public void setFutureOrderDateByStr(String futureOrderDate) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM-dd-yyyy");
        this.futureOrderDate = LocalDate.parse(futureOrderDate, formatter);
    }

    public boolean isChargeAtPurchaseTime() {
        return chargeAtPurchaseTime;
    }

    public void setChargeAtPurchaseTime(boolean chargeAtPurchaseTime) {
        this.chargeAtPurchaseTime = chargeAtPurchaseTime;
    }

    public boolean getAllowsVouchers() {
        return allowsVouchers;
    }

    public void setAllowsVouchers(boolean allowsVouchers) {
        this.allowsVouchers = allowsVouchers;
    }

    public String getTokenizedFileName() {
        return tokenizedFileName;
    }

    public void setTokenizedFileName(String tokenizedFileName) {
        this.tokenizedFileName = tokenizedFileName;
    }

    public Integer getImagePosition() {
        return imagePosition;
    }

    public void setImagePosition(Integer imagePosition) {
        this.imagePosition = imagePosition;
    }

    public Integer getMaxNumUpsellKeypads() {
        return maxNumUpsellKeypads;
    }

    public void setMaxNumUpsellKeypads(Integer maxNumUpsellKeypads) {
        this.maxNumUpsellKeypads = maxNumUpsellKeypads;
    }

    public Integer getUpsellProfileID() {
        return upsellProfileID;
    }

    public void setUpsellProfileID(Integer upsellProfileID) {
        this.upsellProfileID = upsellProfileID;
    }

    public List<MenuModel> getUpsellProfileKeypads() {
        return upsellProfileKeypads;
    }

    public void setUpsellProfileKeypads(List<MenuModel> upsellProfileKeypads) {
        this.upsellProfileKeypads = upsellProfileKeypads;
    }

    public BigDecimal getMaxOverdraftAmount() {
        return maxOverdraftAmount;
    }

    public void setMaxOverdraftAmount(BigDecimal maxOverdraftAmount) {
        this.maxOverdraftAmount = maxOverdraftAmount;
    }

    public String getPickupOriginalCloseTime() {
        return pickupOriginalCloseTime;
    }

    public void setPickupOriginalCloseTime(String pickupOriginalCloseTime) {
        this.pickupOriginalCloseTime = pickupOriginalCloseTime;
    }

    public String getDeliveryOriginalCloseTime() {
        return deliveryOriginalCloseTime;
    }

    public void setDeliveryOriginalCloseTime(String deliveryOriginalCloseTime) {
        this.deliveryOriginalCloseTime = deliveryOriginalCloseTime;
    }

    public Integer getPurchaseRestrictionProfileID() {
        return purchaseRestrictionProfileID;
    }

    public void setPurchaseRestrictionProfileID(Integer purchaseRestrictionProfileID) {
        this.purchaseRestrictionProfileID = purchaseRestrictionProfileID;
    }

    public List<PurchaseRestrictionProfileDetailModel> getPurchaseRestrictionProfileDetails() {
        return purchaseRestrictionProfileDetails;
    }

    public void setPurchaseRestrictionProfileDetails(List<PurchaseRestrictionProfileDetailModel> purchaseRestrictionProfileDetails) {
        this.purchaseRestrictionProfileDetails = purchaseRestrictionProfileDetails;
    }

    public String getOpenTxnPopupMessage() {
        return openTxnPopupMessage;
    }

    public void setOpenTxnPopupMessage(String openTxnPopupMessage) {
        this.openTxnPopupMessage = openTxnPopupMessage;
    }

    public String getUpsellPageMessage() {
        return upsellPageMessage;
    }

    public void setUpsellPageMessage(String upsellPageMessage) {
        this.upsellPageMessage = upsellPageMessage;
    }

    public String getIconGrayscalePercentage() {
        return iconGrayscalePercentage;
    }

    public void setIconGrayscalePercentage(String iconGrayscalePercentage) {
        this.iconGrayscalePercentage = iconGrayscalePercentage;
    }

    public boolean isAllowItemScan() {
        return allowItemScan;
    }

    public void setAllowItemScan(boolean allowItemScan) {
        this.allowItemScan = allowItemScan;
    }

    public boolean isProductScanAddToCart() {
        return productScanAddToCart;
    }

    public void setProductScanAddToCart(boolean productScanAddToCart) {
        this.productScanAddToCart = productScanAddToCart;
    }

    public Integer getTerminalID() {
        return terminalID;
    }

    public void setTerminalID(Integer terminalID) {
        this.terminalID = terminalID;
    }

    public boolean isShowPhoneNumberInput() {
        return showPhoneNumberInput;
    }

    public void setShowPhoneNumberInput(boolean showPhoneNumberInput) {
        this.showPhoneNumberInput = showPhoneNumberInput;
    }

    public boolean isShowCommentInput() {
        return showCommentInput;
    }

    public void setShowCommentInput(boolean showCommentInput) {
        this.showCommentInput = showCommentInput;
    }

    public boolean isShowDeliveryInput() {
        return showDeliveryInput;
    }

    public void setShowDeliveryInput(boolean showDeliveryInput) {
        this.showDeliveryInput = showDeliveryInput;
    }

    public boolean isShowWaitTime() {
        return showWaitTime;
    }

    public void setShowWaitTime(boolean showWaitTime) {
        this.showWaitTime = showWaitTime;
    }

    public boolean isShowOrderTypeAndTimeMsg() {
        return showOrderTypeAndTimeMsg;
    }

    public void setShowOrderTypeAndTimeMsg(boolean showOrderTypeAndTimeMsg) {
        this.showOrderTypeAndTimeMsg = showOrderTypeAndTimeMsg;
    }

    public boolean isShowPickupDeliveryLocationMsg() {
        return showPickupDeliveryLocationMsg;
    }

    public void setShowPickupDeliveryLocationMsg(boolean showPickupDeliveryLocationMsg) {
        this.showPickupDeliveryLocationMsg = showPickupDeliveryLocationMsg;
    }

    public boolean isForceASAP() {
        return forceASAP;
    }

    public void setForceASAP(boolean forceASAP) {
        this.forceASAP = forceASAP;
    }

    public void setUseFundingTerminal(boolean useFundingTerminal) {
        this.useFundingTerminal = useFundingTerminal;
    }

    public void setUseOnlineOrderingFunding(boolean useOnlineOrderingFunding) {
        this.useOnlineOrderingFunding = useOnlineOrderingFunding;
    }


    public List<DisclaimerModel> getDisclaimers() {
        return disclaimers;
    }

    public void setDisclaimers(List<DisclaimerModel> disclaimers) {
        this.disclaimers = disclaimers;
    }

}
