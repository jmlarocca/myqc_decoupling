package com.mmhayes.myqc.server.models;

//mmhayes dependencies

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.CommonAuthResource;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.commonMMHFunctions;
import com.mmhayes.common.utils.Logger;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.*;

//javax.ws.rs dependencies
//myqc dependencies
//other dependencies

/* Product Model
 Last Updated (automatically updated by SVN)
 $Author: zdhirschman $: Author of last commit
 $Date: 2020-11-12 11:34:53 -0500 (Thu, 12 Nov 2020) $: Date of last commit
 $Rev: 52754 $: Revision of last commit

 Notes: Model for products (a.k.a PLUs, Items, etc.)
*/
public class FavoriteModel {
    Integer id = null; //ID
    Integer orderingFavoriteID = null; //ID
    Integer productID = null; //Product ID (PAPluID)
    Integer productCount = null;
    Integer popularModifierID = null;
    Integer popularModifierDetailID = null;
    Integer modifierSetID = null;
    Integer modifierSetDetailID = null;
    Integer keypadID = null;
    Integer prepOptionSetID = null;
    Integer imageShapeID = null; //Image Shape ID
    BigDecimal price = null; //Price of the product
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String terminalID = null; //Terminal order was made at it
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String name = ""; //Name of the product
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String inactivatedReason = null; //Name of the product
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String modifierNames = "";
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String primary = "";
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String image = "";
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String unavailableModifiers = "";
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String unavailableModPrepOptions = "";
    Boolean unavailablePrepOption = false; //flag that determines if the prep option on the parent product is available
    ArrayList<String> modifierList = new ArrayList<>();
    List<ModifierModel> modifiers = new ArrayList<>(); //this Array List represents a ModifierCollection which contains a list of ModifierModels
    private static DataManager dm = new DataManager();
    private Integer authenticatedAccountID = null;
    boolean isAvailable = false; //keep track of if the favorite product is still available
    Boolean active = false;
    Boolean favorite = false;
    Boolean popular = false;
    Boolean recent = false;
    Boolean healthy = false; //flag that determines if this item is healthy
    Boolean vegetarian = false; //flag that determines if this item is vegetarian
    Boolean vegan = false; //flag that determines if this item is vegan
    Boolean glutenFree = false; //flag that determines if this item is gluten free
    Boolean autoShowModSet = false; //flag that determines if the modifier set should be automatically shown
    ProductFavoriteModel productFavoriteModel = null;
    PrepOptionModel prepOption = null;
    PrepOptionModel defaultPrepOption = null;
    ArrayList<HashMap> nutritionInfo = new ArrayList<>(); //Array of nutrition info for the product

    protected commonMMHFunctions commDM = new commonMMHFunctions();

    //dummy constructor - needed to consume ANY JSON object via REST (Jersey/Jackson style)
    public FavoriteModel() {

    }

    //constructor - takes hashmap that get sets to this models properties
    public FavoriteModel(HashMap modelDetailHM) throws Exception {
        setModelProperties(modelDetailHM);
    }

    //setter for all of this model's properties
    public FavoriteModel setModelProperties(HashMap modelDetailHM) throws Exception{
        setID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));
        setProductID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PRODUCTID")));
        setName(CommonAPI.convertModelDetailToString(modelDetailHM.get("NAME")));
        setPrice(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("PRICE")));
        setFavorite(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("FAVORITE")));
        setRecent(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("RECENT")));
        setPopular(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("POPULAR")));
        setProductCount(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PRODUCTCOUNT")));
        setPrimary(CommonAPI.convertModelDetailToString(modelDetailHM.get("PRIMARY")));
        setImage(CommonAPI.convertModelDetailToString(modelDetailHM.get("IMAGE")));
        setKeypadID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("KEYPADID")));
        setHealthy(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("HEALTHY")));
        setVegetarian(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("VEGETARIAN")));
        setVegan(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("VEGAN")));
        setGlutenFree(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("GLUTENFREE")));

        setNutritionInfo(modelDetailHM, "NUTRITION1");
        setNutritionInfo(modelDetailHM, "NUTRITION2");
        setNutritionInfo(modelDetailHM, "NUTRITION3");
        setNutritionInfo(modelDetailHM, "NUTRITION4");
        setNutritionInfo(modelDetailHM, "NUTRITION5");

        if(modelDetailHM.containsKey("AVAILABLE") && !modelDetailHM.get("AVAILABLE").toString().equals("")) {
            setAvailable(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("AVAILABLE")));
        }

        if(modelDetailHM.containsKey("POPULARMODIFIER") && !modelDetailHM.get("POPULARMODIFIER").toString().equals("")) {
            setPopularModifierID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("POPULARMODIFIER")));
        }

        if(modelDetailHM.containsKey("POPULARMODIFIERDETAILID") && !modelDetailHM.get("POPULARMODIFIERDETAILID").toString().equals("")) {
            setPopularModifierDetailID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("POPULARMODIFIERDETAILID")));
        }

        if(modelDetailHM.containsKey("MODIFIERSETID") && !modelDetailHM.get("MODIFIERSETID").toString().equals("")) {
            setModifierSetID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("MODIFIERSETID")));
        }

        if(modelDetailHM.containsKey("MODSETDETAILID") && !modelDetailHM.get("MODSETDETAILID").toString().equals("")) {
            setModifierSetDetailID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("MODSETDETAILID")));
        }

        if(modelDetailHM.containsKey("AUTOSHOWMODSET") && !modelDetailHM.get("AUTOSHOWMODSET").toString().equals("")) {
            setAutoShowModSet(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("AUTOSHOWMODSET"), true));
        }

        if(modelDetailHM.containsKey("PAPREPOPTIONID") && !modelDetailHM.get("PAPREPOPTIONID").equals("")) {
            setPrepOption(modelDetailHM);
        }

        if(modelDetailHM.containsKey("PREPOPTIONNOTAVAILABLE") && modelDetailHM.get("PREPOPTIONNOTAVAILABLE").equals("true")){
            setUnavailablePrepOption(true);
        }

        if(modelDetailHM.containsKey("PAPREPOPTIONSETID") && !modelDetailHM.get("PAPREPOPTIONSETID").equals("")) {
            setPrepOptionSetID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAPREPOPTIONSETID")));
        }

        if(modelDetailHM.containsKey("PAPREPOPTIONSETID") && !modelDetailHM.get("PAPREPOPTIONSETID").equals("") && modelDetailHM.containsKey("PAPREPOPTIONID") && modelDetailHM.get("PAPREPOPTIONID").equals("") && (modelDetailHM.containsKey("PREPSETACTIVE") && (!modelDetailHM.get("PREPSETACTIVE").equals("") || !modelDetailHM.get("PREPSETACTIVE").equals("0")))) {
            PrepOptionModel prepOptionModel = new PrepOptionModel();
            prepOptionModel.getDefaultPrepOptionModel(getPrepOptionSetID());
            setDefaultPrepOption(prepOptionModel);
        }

        //if it's a product in the popular list, there is a prep option set on the product, add the default prep option for when they add the product to the cart
        if(getPrimary().equals("popular") && getPrepOptionSetID() != null) {
            PrepOptionModel prepOptionModel = new PrepOptionModel();
            prepOptionModel.getDefaultPrepOptionModel(getPrepOptionSetID());
            setDefaultPrepOption(prepOptionModel);
        }

        if(modelDetailHM.containsKey("IMAGESHAPEID") && !modelDetailHM.get("IMAGESHAPEID").equals("")) {
            setImageShapeID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("IMAGESHAPEID")));
        }

        return this;
    }

    public void setModifierModel(HashMap modelDetailHM) throws Exception {
        ModifierModel modifierModel = new ModifierModel();

        Integer modifierID = CommonAPI.convertModelDetailToInteger(modelDetailHM.get("MODID"));
        Integer modifierKeypadID = CommonAPI.convertModelDetailToInteger(modelDetailHM.get("MODKEYPADID"));
        String modifierName = CommonAPI.convertModelDetailToString(modelDetailHM.get("MODNAME"));
        BigDecimal modifierPrice = CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("MODPRICE"));
        boolean modifierAvailable = CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("MODIFIERAVAILABLE"));

        Integer modifierPrepID = CommonAPI.convertModelDetailToInteger(modelDetailHM.get("MODPREPID"));
        Integer modifierPrepSetID = CommonAPI.convertModelDetailToInteger(modelDetailHM.get("MODPREPSETID"));
        String modifierPrepName = CommonAPI.convertModelDetailToString(modelDetailHM.get("MODPREPNAME"));
        BigDecimal modifierPrepPrice = CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("MODPREPPRICE"), BigDecimal.ZERO);
        boolean modifierDefaultOption = CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("MODDEFAULTOPTION"), false);
        boolean modifierDisplayDefault = CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("MODDISPLAYDEFAULT"), false);
        boolean modifierPrepNotAvailable = CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("MODPREPRODNOTAVAILABLE"));

        //add the unavailable modifier to the string list of unavailable modifier IDs
        if( !modifierAvailable ) {
            String unavailableModifiers = getUnavailableModifiers();

            if(unavailableModifiers.equals("")) {
                setUnavailableModifiers(modifierID.toString());
            } else {
                List<String> unavailableModList = new ArrayList<String>(Arrays.asList(unavailableModifiers.split(",")));
                unavailableModList.add(modifierID.toString());
                setUnavailableModifiers(String.join(",", unavailableModList));
            }
        }

        //add the unavailable prep option to the string list of unavailable modifier IDs
        if( modifierPrepNotAvailable ) {
            String unavailableModifierPreps = getUnavailableModPrepOptions();

            if(unavailableModifierPreps.equals("")) {
                setUnavailableModPrepOptions(modifierPrepID.toString());
            } else {
                List<String> unavailableModPrepList = new ArrayList<String>(Arrays.asList(unavailableModifierPreps.split(",")));

                unavailableModPrepList.add(modifierPrepID.toString());
                setUnavailableModPrepOptions(String.join(",", unavailableModPrepList));
            }
        }

        //set modifier model
        modifierModel.setID(modifierID);
        modifierModel.setKeypadID(modifierKeypadID);
        modifierModel.setName(modifierName);
        modifierModel.setPrice(modifierPrice);

        //update price of favorite
        setPrice(getPrice().add(modifierPrice));

        //set prep option on modifier model
        if( modifierPrepID != null ) {
            PrepOptionModel prepOptionModel = new PrepOptionModel();

            prepOptionModel.setID(modifierPrepID);
            prepOptionModel.setPrepOptionSetID(modifierPrepSetID);
            prepOptionModel.setName(modifierPrepName);
            prepOptionModel.setPrice(modifierPrepPrice);
            prepOptionModel.setDefaultOption(modifierDefaultOption);
            prepOptionModel.setDisplayDefault(modifierDisplayDefault);

            modifierModel.setPrepOption(prepOptionModel);
            modifierModel.setPrepOptionSetID(modifierPrepSetID);

            //update price of favorite with mod prep price
            setPrice(getPrice().add(modifierPrepPrice));

        //if the modifier became a favorite without a prep option but then a prep option set was added, find the set's default prep option and set it on the modifier
        } else if ( modifierPrepSetID != null ) {
            PrepOptionModel prepOptionModel = new PrepOptionModel();
            prepOptionModel.getDefaultPrepOptionModel(modifierPrepSetID);

            modifierModel.setDefaultPrepOption(prepOptionModel);
            modifierModel.setPrepOptionSetID(modifierPrepSetID);

            //update price of favorite with mod prep price
            setPrice(getPrice().add(prepOptionModel.getPrice()));
        }

        getModifiers().add(modifierModel);
    }

    public void setPrepOption(HashMap modelDetailHM) {

        PrepOptionModel prepOptionModel = new PrepOptionModel();

        Integer prepOptionID = CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAPREPOPTIONID"));
        Integer prepOptionSetID = CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAPREPOPTIONSETID"));
        String prepName = CommonAPI.convertModelDetailToString(modelDetailHM.get("PREPNAME"));
        BigDecimal prepPrice = CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("PREPPRICE"), BigDecimal.ZERO);
        boolean defaultOption = CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("DEFAULTOPTION"), false);
        boolean displayDefault = CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("DISPLAYDEFAULTMYQC"), false);

        prepOptionModel.setID(prepOptionID);
        prepOptionModel.setPrepOptionSetID(prepOptionSetID);
        prepOptionModel.setName(prepName);
        prepOptionModel.setPrice(prepPrice);
        prepOptionModel.setDefaultOption(defaultOption);
        prepOptionModel.setDisplayDefault(displayDefault);

        //update price of favorite with prep price
        setPrice(getPrice().add(prepPrice));

        setPrepOption(prepOptionModel);
    }

    //constructor - takes hashmap that get sets to this models properties
    public FavoriteModel(HttpServletRequest request, ProductFavoriteModel productFavoriteModel) throws Exception {
        setAuthenticatedAccountID(CommonAuthResource.checkRequestHeaderForMMHayesAuth(request));
        setProductFavoriteModel(productFavoriteModel);
    }

    //constructor - takes hashmap that get sets to this models properties
    public FavoriteModel(ProductFavoriteModel productFavoriteModel, HttpServletRequest request) throws Exception {
        setAuthenticatedAccountID(CommonAuthResource.checkRequestHeaderForMMHayesAuth(request));

        String myQCTerminalID = CommonAPI.checkRequestHeaderForStoreID(request).toString();
        String terminalID = getTerminalFromMyQCTerminalID(myQCTerminalID);
        setTerminalID(terminalID);

        productFavoriteModel.setTerminalID(Integer.parseInt(getTerminalID()));
        productFavoriteModel.setStoreID(Integer.parseInt(myQCTerminalID));
    }

    public ProductFavoriteModel determineIfFavoriteIsProduct(ProductFavoriteModel productFavoriteModel) {
        ArrayList<HashMap> foundFavoriteList = new ArrayList<>();
        boolean modifiersHavePrepOptions = modifiersHavePrepOption(productFavoriteModel);

        //reset fields before checking if favorite product
        productFavoriteModel.setOrderingFavoriteID(null);
        productFavoriteModel.setActive(false);


        //product with no mods or prep
        if(productFavoriteModel.getModifiers().size() == 0 && productFavoriteModel.getPrepOption() == null) {
            foundFavoriteList = isFavoriteNoModsNoPrep(productFavoriteModel);

        //product with no mods and prep
        } else if(productFavoriteModel.getModifiers().size() == 0 && productFavoriteModel.getPrepOption() != null) {
            foundFavoriteList = isFavoriteNoModsWithPrep(productFavoriteModel);

        //product with mods and no prep
        } else if(productFavoriteModel.getModifiers().size() > 0 && productFavoriteModel.getPrepOption() == null && !modifiersHavePrepOptions) {
            foundFavoriteList = isFavoriteWithModsNoPrep(productFavoriteModel);

        //product with mods and prep
        } else if(productFavoriteModel.getModifiers().size() > 0 && productFavoriteModel.getPrepOption() != null && !modifiersHavePrepOptions) {
            foundFavoriteList = isFavoriteWithModsWithPrep(productFavoriteModel);

        //product with mods and prep mods no prep
        } else if(productFavoriteModel.getModifiers().size() > 0 && productFavoriteModel.getPrepOption() == null && modifiersHavePrepOptions) {
            foundFavoriteList = isFavoriteWithModsWithModsPrepNoPrep(productFavoriteModel);

        //product with mods and prep mods and prep
        } else if(productFavoriteModel.getModifiers().size() > 0 && productFavoriteModel.getPrepOption() != null && modifiersHavePrepOptions) {
            foundFavoriteList = isFavoriteWithModsWithModsPrepWithPrep(productFavoriteModel);
        }

        //check if more than one favorite was found, need to remove duplicates
        if(foundFavoriteList != null && foundFavoriteList.size() > 0) {
            HashMap favoriteHM = foundFavoriteList.get(0);
            boolean activeStatus = CommonAPI.convertModelDetailToBoolean(favoriteHM.get("ACTIVE"));
            Integer orderingFavoriteID = CommonAPI.convertModelDetailToInteger(favoriteHM.get("ID"));

            productFavoriteModel.setOrderingFavoriteID(orderingFavoriteID);
            productFavoriteModel.setActive(activeStatus);

            //if duplicate favorites are found, remove all duplicates except for one
            if(foundFavoriteList.size() > 1) {
                Logger.logMessage("ERROR: Duplicate favorite records found in QC_OrderingFavorite for EmployeeID: " + getAuthenticatedAccountID() +", TerminalID: " + getTerminalID() + " and ProductID: " + getProductID() , Logger.LEVEL.ERROR);

                foundFavoriteList = new ArrayList<>(foundFavoriteList.subList(1, foundFavoriteList.size()));
                removeDuplicateFavorites(foundFavoriteList);
            }
        }

        return productFavoriteModel;
    }

    public boolean modifiersHavePrepOption(ProductFavoriteModel productFavoriteModel) {
        for(ModifierModel modifierModel : productFavoriteModel.getModifiers()) {
            if(modifierModel.getPrepOption() != null) {
                return true;
            }
        }
        return false;
    }

    public ArrayList<HashMap> isFavoriteNoModsNoPrep(ProductFavoriteModel productFavoriteModel) {
        ArrayList<HashMap> orderingFavoriteDetails = dm.parameterizedExecuteQuery("data.favorite.isProductFavorite",
            new Object[]{
                    getTerminalID(),
                    getAuthenticatedAccountID(),
                    productFavoriteModel.getID()
            }, true
        );

        return orderingFavoriteDetails;
    }

    public ArrayList<HashMap> isFavoriteNoModsWithPrep(ProductFavoriteModel productFavoriteModel) {
        ArrayList<HashMap> orderingFavoriteDetails =  dm.parameterizedExecuteQuery("data.favorite.isProductWithPrepFavorite",
                new Object[]{
                        getTerminalID(),
                        getAuthenticatedAccountID(),
                        productFavoriteModel.getID(),
                        productFavoriteModel.getPrepOption().getID()
                }, true
        );

        return orderingFavoriteDetails;
    }

    public ArrayList<HashMap> isFavoriteWithModsNoPrep(ProductFavoriteModel productFavoriteModel) {
        ArrayList<String> modifierListArr = new ArrayList<>();
        for(ModifierModel modifierModel : productFavoriteModel.getModifiers()) {
            modifierListArr.add(modifierModel.getID().toString());
        }

        String modifierListStr = String.join(",", modifierListArr);

        ArrayList<HashMap> orderingFavoriteDetails = dm.parameterizedExecuteQuery("data.favorite.isProductWithModsFavorite",
                new Object[]{
                        getTerminalID(),
                        getAuthenticatedAccountID(),
                        productFavoriteModel.getID(),
                        modifierListStr,
                        productFavoriteModel.getModifiers().size()
                }, true
        );

        return orderingFavoriteDetails;
    }

    public ArrayList<HashMap> isFavoriteWithModsWithPrep(ProductFavoriteModel productFavoriteModel) {
        ArrayList<String> modifierListArr = new ArrayList<>();
        for(ModifierModel modifierModel : productFavoriteModel.getModifiers()) {
            modifierListArr.add(modifierModel.getID().toString());
        }

        String modifierListStr = String.join(",", modifierListArr);

        ArrayList<HashMap> orderingFavoriteDetails = dm.parameterizedExecuteQuery("data.favorite.isProductWithPrepAndModsFavorite",
                new Object[]{
                        getTerminalID(),
                        getAuthenticatedAccountID(),
                        productFavoriteModel.getID(),
                        productFavoriteModel.getPrepOption().getID(),
                        modifierListStr,
                        productFavoriteModel.getModifiers().size()
                }, true
        );

        return orderingFavoriteDetails;
    }

    public ArrayList<HashMap> isFavoriteWithModsWithModsPrepNoPrep(ProductFavoriteModel productFavoriteModel) {
        String modsWithPrepSQL = buildCustomModifierPrepSQL(productFavoriteModel);

        ArrayList<HashMap> orderingFavoriteDetails = dm.serializeSqlWithColNames("data.favorite.isProductWithModsAndPrepModsFavorite",
                new Object[]{
                        getTerminalID(),
                        getAuthenticatedAccountID(),
                        productFavoriteModel.getID(),
                        modsWithPrepSQL,
                        productFavoriteModel.getModifiers().size()
                }, false
        );

        return orderingFavoriteDetails;

    }

    public ArrayList<HashMap> isFavoriteWithModsWithModsPrepWithPrep(ProductFavoriteModel productFavoriteModel) {
        String modsWithPrepSQL = buildCustomModifierPrepSQL(productFavoriteModel);

        ArrayList<HashMap> orderingFavoriteDetails = dm.serializeSqlWithColNames("data.favorite.isProductWithPrepAndModsAndPrepModsFavorite",
                new Object[]{
                        getTerminalID(),
                        getAuthenticatedAccountID(),
                        productFavoriteModel.getID(),
                        productFavoriteModel.getPrepOption().getID(),
                        modsWithPrepSQL,
                        productFavoriteModel.getModifiers().size()
                }, false
        );

        return orderingFavoriteDetails;
    }

    //remove duplicate occurrences of the same exact favorite product and modifiers
    public void removeDuplicateFavorites(ArrayList<HashMap> duplicateFavoriteList) {
        try {

            for(HashMap duplicateFavorite : duplicateFavoriteList) {

                if(duplicateFavorite.get("ID") != null && !duplicateFavorite.get("ID").toString().equals("")) {

                    String orderingFavoriteID = duplicateFavorite.get("ID").toString();

                    //remove ordering favorite mods for the orderingFavoriteID
                    Integer updateResult = dm.parameterizedExecuteNonQuery("data.ordering.removeDuplicateFavoriteModifiers",
                            new Object[]{
                                    orderingFavoriteID
                            }
                    );

                    if (updateResult == -1) {
                        Logger.logMessage("Could not remove duplicate favorite modifiers in FavoriteModel.removeDuplicateFavorites", Logger.LEVEL.ERROR);
                    }

                    //remove ordering favorite product for the orderingFavoriteID
                    Integer removeResult = dm.parameterizedExecuteNonQuery("data.ordering.removeDuplicateFavorite",
                            new Object[]{
                                    orderingFavoriteID
                            }
                    );

                    if (removeResult == -1) {
                        Logger.logMessage("Could not remove duplicate favorite in FavoriteModel.removeDuplicateFavorites", Logger.LEVEL.ERROR);
                    }
                }
            }

        } catch (Exception ex) {
            Logger.logMessage("ERROR: Could not determine if product is already a favorite or not.", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }
    }

    //inactivate the favorite record, returns 1 if successful
    public static Integer inactivateFavorite(Integer orderingFavoriteID) {
        Integer inactivatedResult = null;
        try {

            String inactivateReason = "Account inactivated their favorite";
            Date inactivatedDTM = new Date();

            inactivatedResult = dm.parameterizedExecuteNonQuery("data.ordering.inactivateFavorite",
                    new Object[]{
                            orderingFavoriteID,
                            false,
                            inactivatedDTM,
                            inactivateReason,
                    }
            );

            if (inactivatedResult != 1) {
                Logger.logMessage("Could not inactivate product in FavoriteModel.inactivateFavorite for ordering favorite ID: " +orderingFavoriteID, Logger.LEVEL.ERROR);
            }

        } catch (Exception ex) {
            Logger.logMessage("ERROR: Could not inactivate product in FavoriteModel.inactivateFavorite.", Logger.LEVEL.ERROR);
        }

        return inactivatedResult;
    }

    //creates a new favorite record or updates an existing favorite record from inactive to active, returns record's orderingFavoriteID if successful
    public Integer activateFavorite() {
        Integer orderingFavoriteID = null;
        try {

            //if the favorite model has an ordering favorite ID then it already exists, update to active
            if(getProductFavoriteModel().getOrderingFavoriteID() != null) {
                orderingFavoriteID = updateExistingFavorite();

            } else { //creates a new favorite record
                orderingFavoriteID = createProductFavoriteRecord();
            }


        } catch (Exception ex) {
            Logger.logMessage("ERROR: Could not inactivate product in FavoriteModel.inactivateFavorite.", Logger.LEVEL.ERROR);
        }

        return orderingFavoriteID;
    }

    //updates an existing favorite record, changing it from inactive to active
    public Integer updateExistingFavorite() {
        Integer orderingFavoriteID = null;
        Date inactivatedDTM = null;
        try {

            Integer activateResult = dm.parameterizedExecuteNonQuery("data.ordering.activateFavorite",
                    new Object[]{
                            getProductFavoriteModel().getOrderingFavoriteID(),
                            getProductFavoriteModel().isActive(),
                            inactivatedDTM,
                            null,
                    }
            );

            if (activateResult != 1) {
                Logger.logMessage("Could not activate product in FavoriteModel.updateExistingFavorite for ordering favorite ID: " + getID(), Logger.LEVEL.ERROR);
                return orderingFavoriteID;  //will return null
            }

            orderingFavoriteID = getProductFavoriteModel().getOrderingFavoriteID(); //returns orderingFavoriteID

        } catch (Exception ex) {
            Logger.logMessage("ERROR: Could not update the active status of the favorite product in FavoriteModel.updateExistingFavorite.", Logger.LEVEL.ERROR);
        }

        return orderingFavoriteID;
    }

    //creates a new record for this product with the exact modifiers, returns record's new orderingFavoriteID if successful
    public Integer createProductFavoriteRecord() {
        Integer orderingFavoriteID = null;

        try {

            Integer prepOptionID = null;
            Integer prepOptionSetID = null;
            if(getProductFavoriteModel().getPrepOption() != null) {
                prepOptionID = getProductFavoriteModel().getPrepOption().getID();
                prepOptionSetID = getProductFavoriteModel().getPrepOptionSetID();
            }

            //first creates record in QC_OrderingFavorite
            Integer insertResult = dm.parameterizedExecuteNonQuery("data.ordering.createProductFavoriteRecord",
                    new Object[]{
                            getProductFavoriteModel().getTerminalID(),
                            getAuthenticatedAccountID(),
                            getProductFavoriteModel().getID(),
                            getProductFavoriteModel().getKeypadID(),
                            prepOptionSetID,
                            prepOptionID
                    }
            );

            if (!(insertResult > 0)) {
                Logger.logMessage("Could not insert into QC_OrderingFavorite in FavoriteModel.createProductFavoriteRecord", Logger.LEVEL.ERROR);
                return orderingFavoriteID; //returns null if unsuccessful
            }

            //if successful set orderingFavoriteID as ID returned from insert
            orderingFavoriteID = insertResult;

            //then creates record in QC_OrderingFavoriteMod if there are modifiers
            if(getProductFavoriteModel().getModifiers().size() > 0) {

                for(ModifierModel modifierModel : getProductFavoriteModel().getModifiers()) {

                    Integer modPrepOptionID = null;
                    Integer modPrepOptionSetID = null;
                    if(modifierModel.getPrepOption() != null) {
                        modPrepOptionID = modifierModel.getPrepOption().getID();
                        modPrepOptionSetID = modifierModel.getPrepOption().getPrepOptionSetID();
                    }

                    Integer insertResultMods = dm.parameterizedExecuteNonQuery("data.ordering.createModifierFavoriteRecord",
                            new Object[]{
                                    orderingFavoriteID,
                                    modifierModel.getID(),
                                    modifierModel.getKeypadID(),
                                    modPrepOptionSetID,
                                    modPrepOptionID
                            }
                    );

                    if (insertResultMods != 1) {
                        Logger.logMessage("Could not insert into QC_OrderingFavoriteMods in FavoriteModel.createProductFavoriteRecord", Logger.LEVEL.ERROR);
                        orderingFavoriteID = null;  //returns null if unsuccessful
                    }

                }
            }
        } catch (Exception ex) {
            Logger.logException(ex);
            Logger.logMessage("ERROR: Could not create a new product favorite record in FavoriteModel.createProductFavoriteRecord.", Logger.LEVEL.ERROR);
        }

        return orderingFavoriteID;
    }

    //if the product doesn't even have prep options or modifiers, don't call this
    public ArrayList<HashMap> isProductAlreadyFavorite() {
        ArrayList<HashMap> alreadyFavoriteList = new ArrayList<>();

        if(getProductFavoriteModel().getModifiers().size() == 0) {

            //if product doesn't have a prep option check if there's already a favorite product with a prep option
            String productSQL = "fav.PAPrepOptionID IS NOT NULL";
            if(getProductFavoriteModel().getPrepOption() != null) {
                productSQL = "(fav.PAPrepOptionID != " + getProductFavoriteModel().getPrepOption().getID() + " OR fav.PAPrepOptionID IS NULL)";
            }

            alreadyFavoriteList =  dm.serializeSqlWithColNames("data.favorite.isProductAlreadyFavorite",
                    new Object[]{
                            getProductFavoriteModel().getTerminalID(),
                            getAuthenticatedAccountID(),
                            getProductFavoriteModel().getID(),
                            productSQL
                    }, false
            );


        //if there is not a prepOptionSetID then the product itself does not allow prep options
        } else {

            String productPrepSQL = "fav.PAPrepOptionID IS NULL";
            if(getProductFavoriteModel().getPrepOption() != null) {
                productPrepSQL = "fav.PAPrepOptionID = " + getProductFavoriteModel().getPrepOption().getID();
            }

            String modsWithPrepSQL = buildCustomModifierPrepSQL(getProductFavoriteModel());

            alreadyFavoriteList =  dm.serializeSqlWithColNames("data.favorite.isProductWithModsAlreadyFavorite",
                    new Object[]{
                            getProductFavoriteModel().getTerminalID(),
                            getAuthenticatedAccountID(),
                            getProductFavoriteModel().getID(),
                            productPrepSQL,
                            modsWithPrepSQL,
                            getProductFavoriteModel().getModifiers().size()
                    }, false
            );

        }

        return alreadyFavoriteList;
    }

    public String buildCustomModifierPrepSQL(ProductFavoriteModel productFavoriteModel) {
        String modsWithPrepSQL = "AND (";

        Integer index = 0;
        for(ModifierModel modifierModel : productFavoriteModel.getModifiers()) {
            if(index > 0) {
                modsWithPrepSQL += " OR ";
            }

            if(modifierModel.getPrepOption() != null) {
                modsWithPrepSQL += "(mods.PAPluID = "+modifierModel.getID()+" AND mods.PAPrepOptionID = "+modifierModel.getPrepOption().getID()+")";
            } else {
                modsWithPrepSQL += "(mods.PAPluID = "+modifierModel.getID()+" AND mods.PAPrepOptionID IS NULL)";
            }

            index ++;
        }

        modsWithPrepSQL += ")";

        return modsWithPrepSQL;
    }

    //get the terminal id from the store ID (My QC Terminal ID)
    public static String getTerminalFromMyQCTerminalID(String myQCTerminalID) {
        String terminalID = "";
        try {

            ArrayList<HashMap> terminalList =  dm.serializeSqlWithColNames("data.ordering.getTerminalFromMyQCTerminalID", new Object[]{myQCTerminalID}, false);

            if (terminalList != null && terminalList.size() == 1) {
                HashMap detailHM = terminalList.get(0);
                if (detailHM.get("TERMINALID") != null && !detailHM.get("TERMINALID").toString().isEmpty()) {
                    terminalID = detailHM.get("TERMINALID").toString();
                }
            }

        } catch (Exception ex) {
            Logger.logMessage("ERROR: Could not determine terminal id from MyQCTerminalID FavoriteModel.getTerminalFromMyQCTerminalID.", Logger.LEVEL.ERROR);
        }
        return terminalID;
    }

    //getter
    private Integer getAuthenticatedAccountID() {
        return this.authenticatedAccountID;
    }

    //setter
    private void setAuthenticatedAccountID(Integer authenticatedAccountID) {
        this.authenticatedAccountID = authenticatedAccountID;
    }

    //getter
    public Integer getID() {
        return this.id;
    }

    //setter
    public void setID(Integer id) {
        this.id = id;
    }

    //getter
    public List<ModifierModel> getModifiers() {
        return modifiers;
    }

    //setter
    public void setModifiers(List<ModifierModel> modifiers) {
        this.modifiers = modifiers;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getInactivatedReason() {
        return inactivatedReason;
    }

    public void setInactivatedReason(String inactivatedReason) {
        this.inactivatedReason = inactivatedReason;
    }

    public Integer getProductID() {
        return productID;
    }

    public void setProductID(Integer productID) {
        this.productID = productID;
    }

    public String getTerminalID() {
        return terminalID;
    }

    public void setTerminalID(String terminalID) {
        this.terminalID = terminalID;
    }

    public ArrayList<String> getModifierList() {
        return modifierList;
    }

    public void setModifierList(ArrayList<String> modifierList) {
        this.modifierList = modifierList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name.replace("\\r", " ");
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }

    public String getModifierNames() {
        return modifierNames;
    }

    public void setModifierNames(String modifierNames) {
        this.modifierNames = modifierNames;
    }

    public Boolean getFavorite() {
        return favorite;
    }

    public void setFavorite(Boolean favorite) {
        this.favorite = favorite;
    }

    public Boolean getPopular() {
        return popular;
    }

    public void setPopular(Boolean popular) {
        this.popular = popular;
    }

    public Boolean getRecent() {
        return recent;
    }

    public void setRecent(Boolean recent) {
        this.recent = recent;
    }

    public Integer getProductCount() {
        return productCount;
    }

    public void setProductCount(Integer productCount) {
        this.productCount = productCount;
    }

    public String getPrimary() {
        return primary;
    }

    public void setPrimary(String primary) {
        this.primary = primary;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUnavailableModifiers() {
        return unavailableModifiers;
    }

    public void setUnavailableModifiers(String unavailableModifiers) {
        this.unavailableModifiers = unavailableModifiers;
    }

    public Integer getPopularModifierID() {
        return popularModifierID;
    }

    public void setPopularModifierID(Integer popularModifierID) {
        this.popularModifierID = popularModifierID;
    }

    public Integer getModifierSetID() {
        return modifierSetID;
    }

    public void setModifierSetID(Integer modifierSetID) {
        this.modifierSetID = modifierSetID;
    }

    public Integer getModifierSetDetailID() {
        return modifierSetDetailID;
    }

    public void setModifierSetDetailID(Integer modifierSetDetailID) {
        this.modifierSetDetailID = modifierSetDetailID;
    }

    public Integer getPopularModifierDetailID() {
        return popularModifierDetailID;
    }

    public void setPopularModifierDetailID(Integer popularModifierDetailID) {
        this.popularModifierDetailID = popularModifierDetailID;
    }

    public Integer getKeypadID() {
        return keypadID;
    }

    public void setKeypadID(Integer keypadID) {
        this.keypadID = keypadID;
    }

    public Integer getOrderingFavoriteID() {
        return orderingFavoriteID;
    }

    public void setOrderingFavoriteID(Integer orderingFavoriteID) {
        this.orderingFavoriteID = orderingFavoriteID;
    }

    public Boolean getHealthy() {
        return healthy;
    }

    public void setHealthy(Boolean healthy) {
        this.healthy = healthy;
    }

    public Boolean getVegetarian() {
        return vegetarian;
    }

    public void setVegetarian(Boolean vegetarian) {
        this.vegetarian = vegetarian;
    }

    public Boolean getVegan() {
        return vegan;
    }

    public void setVegan(Boolean vegan) {
        this.vegan = vegan;
    }

    public Boolean getGlutenFree() {
        return glutenFree;
    }

    public void setGlutenFree(Boolean glutenFree) {
        this.glutenFree = glutenFree;
    }

    public ProductFavoriteModel getProductFavoriteModel() {
        return productFavoriteModel;
    }

    public void setProductFavoriteModel(ProductFavoriteModel productFavoriteModel) {
        this.productFavoriteModel = productFavoriteModel;
    }

    public PrepOptionModel getPrepOption() {
        return prepOption;
    }

    public void setPrepOption(PrepOptionModel prepOption) {
        this.prepOption = prepOption;
    }

    public String getUnavailableModPrepOptions() {
        return unavailableModPrepOptions;
    }

    public void setUnavailableModPrepOptions(String unavailableModPrepOptions) {
        this.unavailableModPrepOptions = unavailableModPrepOptions;
    }

    public Boolean getAutoShowModSet() {
        return autoShowModSet;
    }

    public void setAutoShowModSet(Boolean autoShowModSet) {
        this.autoShowModSet = autoShowModSet;
    }

    public Integer getPrepOptionSetID() {
        return prepOptionSetID;
    }

    public void setPrepOptionSetID(Integer prepOptionSetID) {
        this.prepOptionSetID = prepOptionSetID;
    }

    public PrepOptionModel getDefaultPrepOption() {
        return defaultPrepOption;
    }

    public void setDefaultPrepOption(PrepOptionModel defaultPrepOption) {
        this.defaultPrepOption = defaultPrepOption;
    }

    private void addNutritionInfo(String info) {
        HashMap nutrition = new HashMap();
        nutrition.put("nutritionInfo", info);
        nutritionInfo.add(nutrition);
    }

    //getter
    public ArrayList<HashMap> getNutritionInfo() {
        return nutritionInfo;
    }

    //setter
    private void setNutritionInfo(HashMap modelDetail, String name ) {
        String value = CommonAPI.convertModelDetailToString(modelDetail.get(name));
        // Only add string that aren't blank
        if(!value.equals("")) {
            addNutritionInfo(value);
        }
    }

    public Integer getImageShapeID() {
        return imageShapeID;
    }

    public void setImageShapeID(Integer imageShapeID) {
        this.imageShapeID = imageShapeID;
    }


    public Boolean getUnavailablePrepOption() {
        return unavailablePrepOption;
    }

    public void setUnavailablePrepOption(Boolean unavailablePrepOption) {
        this.unavailablePrepOption = unavailablePrepOption;
    }

}


