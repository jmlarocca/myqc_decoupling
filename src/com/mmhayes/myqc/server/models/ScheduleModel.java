package com.mmhayes.myqc.server.models;

//mmhayes dependencies

import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.commonMMHFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.myqc.server.api.MyQCCache;
import com.mmhayes.myqc.server.collections.ScheduleDetailCollection;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;

//myqc dependencies
//other dependencies

/* Schedule Detail Model
Last Updated (automatically updated by SVN)
$Author: gematuszyk $: Author of last commit
$Date: 2019-04-08 16:16:41 -0400 (Mon, 08 Apr 2019) $: Date of last commit
$Rev: 37702 $: Revision of last commit

Notes: Model for ordering buffer schedules*/
public class ScheduleModel {
    private static DataManager dm = new DataManager();
    private commonMMHFunctions commFunc = new commonMMHFunctions();
    private Integer ID = null; //the ID of the schedule
    private Integer scheduleTypeID = null; //the typeID of the schedule
    private Boolean active = false; //active flag for the schedule
    private String name = null; //name of the schedule
    private ScheduleDetailCollection scheduleDetails = new ScheduleDetailCollection();

    //caching related properties
    private LocalDateTime lastUpdateDTM = null; //when the store was last updated
    private LocalDateTime lastUpdateCheck = null; //when the store was last checked to see if it has been updated

    public final static Integer DELIVERY_ORDERING_SCHEDULE = 1;
    public final static Integer DELIVERY_PREP_SCHEDULE = 2;
    public final static Integer PICKUP_PREP_SCHEDULE = 3;
    public final static Integer PICKUP_ORDERING_SCHEDULE = 4;
    public final static Integer ORDER_BUFFERING_SCHEDULE = 6;

    //constructor - takes hashmap that get sets to this models properties
    public ScheduleModel(Integer scheduleID, LocalDateTime referenceTime) throws Exception {
        try {
            //get all models in an array list
            ArrayList<HashMap> scheduleList = dm.parameterizedExecuteQuery("data.ordering.getSchedule",
                new Object[]{
                    scheduleID
                },
                true
            );

            //populate this collection from an array list of hashmaps
            if ( scheduleList == null || scheduleList.size() == 0 || scheduleList.get(0) == null ) {
                Logger.logMessage("Could not find schedule record for ID: " + scheduleID, Logger.LEVEL.ERROR);
                return;
            }

            HashMap modelDetailHM = scheduleList.get(0);

            //determine that response is valid
            if ( modelDetailHM.get("ID") == null || modelDetailHM.get("ID").toString().isEmpty() ) {
                Logger.logMessage("Could not determine scheduleID in ScheduleModel constructor by ID", Logger.LEVEL.ERROR);
                return;
            }

            setModelProperties( modelDetailHM, referenceTime );

            //add newly created model to schedule cache
            MyQCCache.addScheduleToCache( this );

        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
    }

    public static ScheduleModel getScheduleModel( Integer scheduleID, LocalDateTime referenceTime ) throws Exception {
        //if schedule is in cache, load from cache
        if ( MyQCCache.checkCacheForSchedule(scheduleID) ) {
            return MyQCCache.getScheduleFromCache(scheduleID);
        }

        return new ScheduleModel( scheduleID, referenceTime );
    }

    private ScheduleModel setModelProperties(HashMap modelDetailHM, LocalDateTime referenceTime) throws Exception {
        setID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));
        setName(CommonAPI.convertModelDetailToString(modelDetailHM.get("NAME")));
        setActive(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("ACTIVE"), false));
        setScheduleTypeID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("SCHEDULETYPEID")));

        //specify in and out formats for formatting modelDetailHM.get("LASTUPDATEDTM")
        String dateTimeInFormat = "yyyy-MM-dd HH:mm:ss.S";
        String dateTimeOutFormat = "M/dd/yyyy h:mm:ss a"; //old way was - M/dd/yyyy H:mm:ss - jrmitaly 6/23/2015

        //format modelDetailHM.get("TRANSDATE") and set to dateTimeStr
        String dateTimeStr = commFunc.formatDateTime(modelDetailHM.get("LASTUPDATEDTM").toString(),dateTimeInFormat,dateTimeOutFormat);

        //parse dateTimeStr to LocalDateTime and set that to this.dateTime
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(dateTimeOutFormat);
        setLastUpdateDTM( LocalDateTime.parse(dateTimeStr, dateTimeFormatter) );

        setLastUpdateCheck(LocalDateTime.now());

        //schedule is not active, no use in retrieving details
        if ( !getActive() ) {
            return this;
        }

        populateDetails(referenceTime);

        return this;
    }

    //instantiates, builds and sets a new ScheduleDetailCollection
    private void populateDetails( LocalDateTime referenceTime ) throws Exception {
        setScheduleDetails( new ScheduleDetailCollection( getID(), getScheduleTypeID(), referenceTime ) );
    }

    //checks against the value of LastUpdateDTM to see if the model has been updated since last being cached
    public boolean checkForUpdates() throws Exception {
        boolean needsUpdate = true;

        Object result = dm.parameterizedExecuteScalar("data.ordering.checkScheduleForUpdates",
            new Object[]{
                getID(),
                getLastUpdateDTM().toLocalDate().toString()+" "+getLastUpdateDTM().plusSeconds(1).toLocalTime().toString()
            }
        );

        //schedule needs to be updated if there is no lastUpdateDTM or if the result is 1
        needsUpdate = ( result == null || result.toString().equals("1") );

        return needsUpdate;
    }

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public Integer getScheduleTypeID() {
        return scheduleTypeID;
    }

    public void setScheduleTypeID(Integer scheduleTypeID) {
        this.scheduleTypeID = scheduleTypeID;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ScheduleDetailCollection getScheduleDetails() {
        return scheduleDetails;
    }

    public void setScheduleDetails(ScheduleDetailCollection scheduleDetails) {
        this.scheduleDetails = scheduleDetails;
    }

    public LocalDateTime getLastUpdateDTM() {
        return lastUpdateDTM;
    }

    public void setLastUpdateDTM(LocalDateTime lastUpdateDTM) {
        this.lastUpdateDTM = lastUpdateDTM;
    }

    public LocalDateTime getLastUpdateCheck() {
        return lastUpdateCheck;
    }

    public void setLastUpdateCheck(LocalDateTime lastUpdateCheck) {
        this.lastUpdateCheck = lastUpdateCheck;
    }
}
