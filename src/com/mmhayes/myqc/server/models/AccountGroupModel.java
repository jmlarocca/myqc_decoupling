package com.mmhayes.myqc.server.models;

//mmhayes dependencies

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.dataaccess.commonMMHFunctions;
import com.mmhayes.common.utils.Logger;

import java.util.HashMap;

//other dependencies

/* Account Group Model
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2017-08-02 08:58:52 -0400 (Wed, 02 Aug 2017) $: Date of last commit
 $Rev: 23136 $: Revision of last commit

 Notes: Model for Account Groups
*/
public class AccountGroupModel {
    private commonMMHFunctions commFunc = new commonMMHFunctions();
    Integer id = null;
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String value = "N/A";
    Boolean active = false; //set to true on the account group of the logged in employee

    //constructor - takes hashmap that get sets to this models properties
    public AccountGroupModel(HashMap modelDetailHM) {
        try {
            setModelProperties(modelDetailHM);
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
    }

    //setter for all of this model's properties
    public AccountGroupModel setModelProperties(HashMap modelDetailHM) {
        try {
            if (modelDetailHM.get("ID") != null) {
                setID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));
            }
            if (modelDetailHM.get("NAME") != null) {
                setValue(CommonAPI.convertModelDetailToString(modelDetailHM.get("NAME")));
            }
            if(modelDetailHM.get("ACTIVE") != null) {
                setActive(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("ACTIVE")));
            }

        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
        return this;
    }

    //getter
    public Integer getID() {
        return this.id;
    }

    //setter
    public void setID(Integer id) {
        this.id = id;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    //getter
    public String getValue() {
        return value;
    }

    //setter
    public void setValue(String value) {
        this.value = value;
    }
}
