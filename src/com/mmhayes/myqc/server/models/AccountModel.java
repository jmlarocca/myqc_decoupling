package com.mmhayes.myqc.server.models;

//mmhayes dependencies

import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.CommonAuthResource;
import com.mmhayes.common.api.Validation;
import com.mmhayes.common.api.errorhandling.exceptions.AccountCreationException;
import com.mmhayes.common.api.errorhandling.exceptions.InvalidAuthException;
import com.mmhayes.common.api.errorhandling.exceptions.MissingDataException;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.commonMMHFunctions;
import com.mmhayes.common.utils.BCrypt;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.myqc.server.api.InstanceClient;
import org.glassfish.jersey.internal.util.collection.MultivaluedStringMap;
import org.omg.CORBA.OBJECT_NOT_EXIST;
import org.owasp.esapi.ESAPI;

import javax.servlet.http.HttpServletRequest;
import java.util.*;


/* Account Model
 Last Updated (automatically updated by SVN)
 $Author: mjbatko $: Author of last commit
 $Date: 2021-07-19 12:28:15 -0400 (Mon, 19 Jul 2021) $: Date of last commit
 $Rev: 56528 $: Revision of last commit

 Notes: Functions for Creating an Account, checking for duplicate username/emails in the cloud and sending a request to the gateway to create a username account
*/
public class AccountModel {
    private static DataManager dm = new DataManager();
    private static commonMMHFunctions comm = new commonMMHFunctions(); //the usual common mmh functions

    //creates a record in QC_Employees and QC_EmployeeChanges and updates the LastEmpID in the global settings
    //if email in cloud account is created then it sends an invite
    //if username in cloud account is created then calls the gateway '/new' endpoint to create a record in MMH_GatewayUser
    public static HashMap<String, String> createAccountInMyQC(HashMap accountInfo, HttpServletRequest request)  {
        HashMap<String, String> result = new HashMap<>();
        String employeeID = "", username = null, email = null, password = null;
        String name = "";
        Boolean isGatewayUsernameAcct = false, isGatewayEmailAcct = false, isGroundUsernameAcct = false;

        try {

            Boolean isAllowAccountCreationOn = getAccountCreationFlag(CommonAPI.convertModelDetailToInteger(accountInfo.get("accountGroup")));
            if(isAllowAccountCreationOn) {

                // no licenses for account creation, or an account type that should not have account creation available
                // Guest
                if(!guestLicensesAreAvailable() && accountInfo.get("accountType").equals(5)) {
                    throw new AccountCreationException(5);
                // Payroll Deduct or Prepaid
                } else if(!employeeLicensesAreAvailable() && ( accountInfo.get("accountType").equals(1) || accountInfo.get("accountType").equals(2)) ) {
                    throw new AccountCreationException(Integer.parseInt(accountInfo.get("accountType").toString()));
                // shouldn't even be an issue but if they manage to get any other account group, throw an account creation exception with generic message
                } else if(!(accountInfo.get("accountType").equals(1) || accountInfo.get("accountType").equals(2) || accountInfo.get("accountType").equals(5))) {
                    throw new AccountCreationException(Integer.parseInt(accountInfo.get("accountType").toString()));
                }

                //determine the next employeeID from the QC_Globals table and add to hash map
                Integer lastEmpID = generateNextEmployeeID();//getLastEmployeeID();
                if(lastEmpID != null && lastEmpID > 0){
                    employeeID = lastEmpID.toString();
                } else {
                    Logger.logMessage("ERROR: Could not determine Last Employee ID in createAccountInMyQC for account creation. Failure in SP_QC_GetNextEmployeeID.", Logger.LEVEL.ERROR);
                    result.put("status", "failure");
                    result.put("errorMessage", "Error creating new ID for account. Please try again.");
                    return result;
                }

                //check for a valid password and encode the password
                if (accountInfo.get("password") != null && !accountInfo.get("password").equals("")) {
                    String plainTextPassword = accountInfo.get("password").toString();
                    if(CommonAPI.validatePassword(plainTextPassword)){
                         password = commonMMHFunctions.hashPassword(plainTextPassword);
                    }
                }

                Boolean promptForName = getPromptForNameFlag(CommonAPI.convertModelDetailToInteger(accountInfo.get("accountGroup")));

                if(promptForName) {
                    String firstName = CommonAPI.convertModelDetailToString(accountInfo.get("firstName")).trim();
                    String  middleInitial = CommonAPI.convertModelDetailToString(accountInfo.get("middleInitial")).trim();
                    String lastName = CommonAPI.convertModelDetailToString(accountInfo.get("lastName")).trim();

                    if(firstName.equals("")) {
                        result.put("status", "failure");
                        result.put("errorMessage", "Please provide a valid First Name.");
                        return result;
                    }

                    if(lastName.equals("")) {
                        result.put("status", "failure");
                        result.put("errorMessage", "Please provide a valid Last Name.");
                        return result;
                    }

                    name = lastName + ", " + firstName + " " + middleInitial;
                } else {
                    String nameField = CommonAPI.convertModelDetailToString(accountInfo.get("name"));
                    if(!nameField.equals("")) {
                        name = nameField;

                    //shouldn't get here but just in case, set the name as the username or email
                    } else if (!CommonAPI.convertModelDetailToString(accountInfo.get("username")).equals("")) {
                        name = username;
                    } else if (!CommonAPI.convertModelDetailToString(accountInfo.get("email")).equals("")) {
                        name = email;
                    } else {
                        result.put("status", "failure");
                        result.put("errorMessage", "Could not set name, please provide a name, username, or email.");
                    }
                }

                //set account details to be
                Integer payrollGroupID = Integer.parseInt(accountInfo.get("accountGroup").toString());
                Integer terminalProfileID = Integer.parseInt(accountInfo.get("spendingProfile").toString());
                Integer accountTypeID = Integer.parseInt(accountInfo.get("accountType").toString());
                String employeeNumber = accountInfo.get("accountNumber").toString();
                String badge = accountInfo.get("badgeNumber").toString();
                String AIP = accountInfo.get("accountStatus").toString();
                String inviteStatus = "1";

                //if email account in the cloud, keep UserAcctLogin and UserAcctPass as NULL
                if (!CommonAPI.getOnGround() && !accountInfo.get("email").equals("")){
                    email =  accountInfo.get("email").toString().toLowerCase();
                    isGatewayEmailAcct = true;

                    //double check that it is not a duplicate email before continuing
                    Validation requestValidation = new Validation();
                    requestValidation.setLoginName(email);
                    HashMap emailInfo = findAvailableEmailCloud(requestValidation, request);
                    if(emailInfo.get("isDuplicateInstance").toString().equals("true") || emailInfo.get("isDuplicateCloud").toString().equals("true")) {
                        result.put("status", "failure");
                        result.put("errorMessage", "You have already created an account with this email.");
                        return result;
                    }

                //if username account in the cloud, keep UserAcctLogin and UserAcctPass as NULL
                } else if(!CommonAPI.getOnGround() && !accountInfo.get("username").equals("")) {
                    password = null;
                    inviteStatus = "6";
                    isGatewayUsernameAcct = true;
                    username = accountInfo.get("username").toString();

                    //double check that it is not a duplicate username before continuing
                    Validation requestValidation = new Validation();
                    requestValidation.setLoginName(username);
                    HashMap resultHM = findAvailableLoginNameCloud(requestValidation, request);
                    boolean isDuplicate = Boolean.valueOf(resultHM.get("isDuplicate").toString());
                    if(isDuplicate) {
                        result.put("status", "failure");
                        result.put("errorMessage", "You have already created an account with this username.");
                        return result;
                    }

                    //if username account on ground set UserAcctLogin to inputted username
                } else if (CommonAPI.getOnGround() && !accountInfo.get("username").equals("")){
                    username = accountInfo.get("username").toString();
                    inviteStatus = "6";

                    //double check that it is not a duplicate username before continuing
                    String loginName = AccountModel.findAvailableLoginNameGround(username);
                    if(!loginName.equals(username)) {
                        result.put("status", "failure");
                        result.put("errorMessage", "You have already created an account with this username.");
                        return result;
                    }

                    //if allow email prompt is on and there's a value, set this field on ground
                    if(!accountInfo.get("email").equals("")) {
                        email = accountInfo.get("email").toString().toLowerCase();
                    }
                    isGroundUsernameAcct = true;
                }

                //prepare sql parameters and then execute insert SQL
                Integer insertResult = dm.parameterizedExecuteNonQuery("data.accounts.createAccount",
                        new Object[]{
                                employeeID,
                                name,
                                AIP,
                                username,
                                password,
                                email,
                                payrollGroupID,
                                terminalProfileID,
                                accountTypeID,
                                employeeNumber,
                                badge,
                                inviteStatus,
                                "1"
                        }
                );

                //if the insert was a success
                if (insertResult != 1) {
                    Logger.logMessage("Could not execute insert SQL in AccountModel.createAccountInMyQC", Logger.LEVEL.ERROR);
                    throw new MissingDataException("Could not execute insert SQL for account creation");
                }  else {

                    //update the global employeeID
//                    if(dm.serializeUpdate("data.accounts.setGlobalEmpID", new Object[]{employeeID})==-1){
//                        Logger.logMessage("ERROR: Unable to update global employee ID", Logger.LEVEL.ERROR);
//                    }

                    //create new account record in QC_EmployeeChanges
                    String recordField = "Status";
                    if (!CommonAPI.recordAccountChange(employeeID, recordField, "New Activation", "Active", commonMMHFunctions.checkSession(request))) {
                        Logger.logMessage("ERROR: Error recording account change in createAccountInMyQC for field: "+recordField, Logger.LEVEL.ERROR);
                    }

                    //set status to success for username account on the ground
                    if(isGroundUsernameAcct) {
                        result.put("status", "success");
                        result.put("username", username);
                    }

                    //send request to gateway to create username username account, if successful set status and username
                    if(isGatewayUsernameAcct) {
                        Validation requestValidation = createGatewayUsernameAcct(accountInfo, employeeID, request);
                        if(requestValidation.getIsValid()) {
                            result.put("status", "success");
                            result.put("username", requestValidation.getLoginName());
                        } else {
                            Logger.logMessage("ERROR: Error creating username gateway account in AccountModel.createAccountInMyQC", Logger.LEVEL.ERROR);
                        }
                    }

                    //turn ON AutoInviteCloud flag in QC_Employees so email is sent to newly created account for Gateway login
                    if(isGatewayEmailAcct) {
                        if(AccountModel.sendInviteForGatewayEmailAcct(employeeID)) {
                            result.put("status", "success");
                            result.put("email", accountInfo.get("email").toString().toLowerCase());
                        } else {
                            Logger.logMessage(" Error creating email gateway account in AccountModel.createAccountInMyQC", Logger.LEVEL.ERROR);
                        }
                    }
                }
            } else {
                Logger.logMessage("Allow Account Creation flag is off, cannot create account in AccountModel.createAccountInMyQC", Logger.LEVEL.ERROR);
            }
        } catch (AccountCreationException acctCreationException) {
            result.put("status", "failure");
            result.put("errorMessage", acctCreationException.getErrorCode() + ": " + acctCreationException.getDetails());
            Logger.logMessage("Error occurred while creating account: ", Logger.LEVEL.ERROR);
            Logger.logException(acctCreationException);
        } catch (Exception ex) {
            Logger.logException(ex);
            Logger.logMessage("Error in AccountModel.createAccountInMyQC", Logger.LEVEL.ERROR);
        }

        return result;
    }

    //check if the allow account creation flag is on
    public static Boolean getAccountCreationFlag(int accountGroupId) throws Exception {
        Boolean isAccountCreationOn = false;
        try {

            isAccountCreationOn = Boolean.parseBoolean(dm.parameterizedExecuteScalar("data.accounts.getAccountCreationFlagByAcctGroup", new Object[]{ accountGroupId }).toString());

        } catch (Exception ex) {
            Logger.logMessage("Exception caught in AccountModel.getAccountCreationFlag()", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }
        return isAccountCreationOn;
    }

    //check if the allow person account creation flag is on
    public static Boolean getPersonAccountCreationFlag() throws Exception {
        Boolean isPersonAccountCreationOn = false;
        try {

            isPersonAccountCreationOn = Boolean.parseBoolean(dm.parameterizedExecuteScalar("data.accounts.getPersonAccountCreationFlag", new Object[]{}).toString());

        } catch (Exception ex) {
            Logger.logMessage("Exception caught in AccountModel.getPersonAccountCreationFlag()", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }
        return isPersonAccountCreationOn;
    }

    public static Boolean getPersonAccountCreationFlag(int accessCodeID) {
        try {
            // query to check for the field IsPersonCode in the row with accessCodeID
            ArrayList<HashMap> personAcctActive = dm.parameterizedExecuteQuery("data.accounts.getPersonAccountCreationFlag_AccessCode", new Object[] { accessCodeID }, true);
            if(personAcctActive == null || personAcctActive.size() == 0) {
                Logger.logMessage("No person account access code found", Logger.LEVEL.TRACE);
                return false;
            }

            boolean active = CommonAPI.convertModelDetailToBoolean(personAcctActive.get(0).get("ACTIVE"));
            boolean isPersonAccount = CommonAPI.convertModelDetailToBoolean(personAcctActive.get(0).get("ISPERSONCODE"));
            if(!active && isPersonAccount) {
                Logger.logMessage("Found person account access code with ID " + accessCodeID + " , however it is inactive", Logger.LEVEL.TRACE);
                return false;
            }
            if(!isPersonAccount) {
                Logger.logMessage("Found the access code with ID " + accessCodeID + " but it is not used for person account creation", Logger.LEVEL.TRACE);
                return false;
            }

            // found an active access code with person acct creation enabled
            return true;

        } catch (Exception ex) {
            Logger.logException(ex);
            Logger.logMessage("Experienced an error getting person account creation settings for access code with ID " + accessCodeID, Logger.LEVEL.ERROR);
        }
        return false;
    }

    //check if the prompt for name flag is on
    public static Boolean getPromptForNameFlag(Integer accountGroupId) throws Exception {
        Boolean isPersonAccountCreationOn = false;
        try {
            if(accountGroupId == null) {
                return false;
            }
            isPersonAccountCreationOn = Boolean.parseBoolean(dm.parameterizedExecuteScalar("data.accounts.getPromptForNameFlagByAcctGroup", new Object[]{
                accountGroupId
            }).toString());

        } catch (Exception ex) {
            Logger.logMessage("Exception caught in AccountModel.getPromptForNameFlag()", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }
        return isPersonAccountCreationOn;
    }

    //checks the database for duplicate username or email (depends on the isEmail flag)
    public static HashMap checkDuplicateAccountNames(HashMap accountNameInfo, HttpServletRequest request) {
        HashMap accountInfo = new HashMap();
        Boolean isDuplicate = false;
        String loginName = "";
        Validation requestValidation = new Validation();
        String username = "", email = "";

        try {

            if (accountNameInfo != null) {
                if (accountNameInfo.get("username") != null && !accountNameInfo.get("username").equals("")) {
                    username = accountNameInfo.get("username").toString();
                }
                if (accountNameInfo.get("email") != null && !accountNameInfo.get("email").equals("")) {
                    email = accountNameInfo.get("email").toString().toLowerCase();
                }
            }

            //if email account in the cloud check if this email is available in the cloud
            if(!email.equals("") && !CommonAPI.getOnGround()) {
                requestValidation.setLoginName(email);
                HashMap emailInfo = findAvailableEmailCloud(requestValidation, request);

                //set necessary hashmap properties to be passed back to front end
                accountInfo.put("isDuplicateInstance", emailInfo.get("isDuplicateInstance"));
                accountInfo.put("isDuplicateCloud", emailInfo.get("isDuplicateCloud"));
                accountInfo.put("inviteStatus", emailInfo.get("inviteStatus"));
                accountInfo.put("employeeID", emailInfo.get("employeeID"));
                accountInfo.put("email", email);

            //if email address and on the ground (email is not main identifier) check for duplicate EmailAddress1
            } else if(!email.equals("") && CommonAPI.getOnGround()) {
                isDuplicate = AccountModel.findAvailableEmailGround(email);
                accountInfo.put("email", email);

            } else{
                //if username account on the ground check if username is available in QC_Employees
                if(!username.equals("") && CommonAPI.getOnGround()) { //username and On Ground
                    loginName = AccountModel.findAvailableLoginNameGround(username);

                    if(!loginName.equals(username)) {
                        isDuplicate = true;
                        accountInfo.put("username", loginName);
                    }
                    //if username account in the cloud check if username with instanceAlias is available in cloud
                } else if(!username.equals("") && !CommonAPI.getOnGround()) { //username and In Cloud
                    requestValidation.setLoginName(username);
                    HashMap resultHM = findAvailableLoginNameCloud(requestValidation, request);
                    isDuplicate = Boolean.valueOf(resultHM.get("isDuplicate").toString());
                    if(isDuplicate) {
                        accountInfo.put("username", resultHM.get("username").toString());
                    }
                }
            }

        } catch (Exception ex) {
            Logger.logException(ex);
            Logger.logMessage("Error in AccountModel.checkDuplicateUsername", Logger.LEVEL.ERROR);
        }

        //set boolean flags if duplicate name was found and if email was used
        accountInfo.put("isDuplicate", isDuplicate);

        return accountInfo;
    }

    //checks if the loginName (username on ground) passed in is valid for use (if not, adds a number and keeps incrementing until it finds a good one)
    public static String findAvailableLoginNameGround(String username) {
        String result = "";
        String baseLoginName = username;

        try {
            Boolean keepTrying = true;
            Integer tryCount = 0;
            while (keepTrying) { //iterate through and keep incrementing the baseLoginName until a gateway loginName is found that is not being used (gives up after 100 tries)

                result = dm.getSingleField("data.accounts.checkLoginIDInUse",new Object[]{baseLoginName},null,"",false,true).toString();
                if (result != null && !result.isEmpty() && !result.equals("0")) {

                    //if username is in user, increment number at end of username and try again....
                    if (Integer.parseInt(result) > 0) {
                        tryCount++;
                        baseLoginName = baseLoginName+tryCount.toString();
                        Logger.logMessage("WARNING: Duplicate username found in AccountModel.findAvailableLoginNameGround - trying next Username: "+baseLoginName, Logger.LEVEL.WARNING);
                        if (tryCount > 100) {
                            Logger.logMessage("ERROR: Could NOT find valid username in AccountModel.findAvailableLoginNameGround - tried 100 times.. giving up...", Logger.LEVEL.ERROR);
                            keepTrying = false;
                        }
                    }
                } else {  //username is not in use
                    keepTrying = false;
                }
            }
        } catch (Exception ex) {
            Logger.logException(ex);
            Logger.logMessage("Error in AccountModel.findAvailableLoginNameGround", Logger.LEVEL.ERROR);
        }
        return baseLoginName;
    }

    //checks if the loginName (username) passed is valid in the cloud for use (if not, adds a number and keeps incrementing until it finds a good one)
    private static HashMap findAvailableLoginNameCloud(Validation requestValidation, HttpServletRequest request) {
        HashMap result = new HashMap();
        String validLoginName = null;
        Boolean keepTrying = true;
        Boolean isDuplicate = false;
        try {
            //determine the instance's alias from a gateway
            InstanceClient instanceClient = new InstanceClient();
            requestValidation = instanceClient.getInstanceDetails(requestValidation, request);

            String baseLoginName = requestValidation.getLoginName().split("@")[0]; //only take the "base" of the login name (without the "@someAlias")
            String instanceAlias = requestValidation.getInstanceAlias();
            requestValidation.setLoginName(baseLoginName+"@"+instanceAlias);
            result.put("username", requestValidation.getLoginName());

            Integer tryCount = 0;
            while (keepTrying) { //iterate through and keep incrementing the baseLoginName until a gateway loginName is found that is not being used (gives up after 100 tries)

                //sends request to the gateway to determine if THIS loginName is NOT in use
                requestValidation = instanceClient.checkIfGatewayUserExists(requestValidation, request);

                //if not valid, then the loginName is NOT in use
                if (!requestValidation.getIsValid()) {
                    keepTrying = false;

                //the loginName IS in use - try again...
                } else {
                    tryCount++;
                    isDuplicate = true;
                    requestValidation.setLoginName(baseLoginName+tryCount.toString()+"@"+instanceAlias);
                    Logger.logMessage("WARNING: Duplicate username found in GatewayUser database in AccountModel.findAvailableLoginNameCloud - trying next loginName: "+baseLoginName, Logger.LEVEL.WARNING);
                    if (tryCount > 100) {
                        Logger.logMessage("ERROR: Could NOT find valid username in AccountModel.findAvailableLoginNameCloud - tried 100 times.. giving up...", Logger.LEVEL.ERROR);
                        keepTrying = false;
                    }
                }
            }
            result.put("isDuplicate", isDuplicate);
        } catch (Exception ex) {
            Logger.logException(ex);
            requestValidation.reset();
            requestValidation.setDetails("An error has occurred. Please contact MMHayes support if the error continues.");
            requestValidation.handleGenericException(ex, "AccountModel.findAvailableLoginNameCloud");
            return null;
        }
        return result;  //return the username and if it was a duplicate
    }

    //checks if the email passed in is valid for use
    public static Boolean findAvailableEmailGround(String email) {
        String result = "";
        Boolean isDuplicate = true;
        try {

            //check if the email address is in use in QC_Employees.EmailAddress1
            result = dm.getSingleField("data.accounts.checkEmailAddressInUseGround",new Object[]{email},null,"",false,true).toString();

            //if email address found keep isDuplicate as true
            if (result != null && !result.isEmpty() && !result.equals("0")) {
                Logger.logMessage("WARNING: Duplicate email address found in AccountModel.findAvailableEmailGround", Logger.LEVEL.WARNING);

            //email is not in use, set isDuplicate to false
            } else {
                isDuplicate = false;
            }

        } catch (Exception ex) {
            Logger.logException(ex);
            Logger.logMessage("Error in AccountModel.findAvailableEmailGround", Logger.LEVEL.ERROR);
        }
        return isDuplicate;
    }

    //checks if the loginName (email) passed is valid in the cloud for use (if not, adds a number and keeps incrementing until it finds a good one)
    private static HashMap findAvailableEmailCloud(Validation requestValidation, HttpServletRequest request) {
        HashMap emailResultHM = new HashMap();
        Boolean isDuplicateInstance = true;
        Boolean isDuplicateCloud = true;
        ArrayList<HashMap> result = new ArrayList<HashMap>();

        try {
            String emailTemp = requestValidation.getLoginName();

            //check if the email address is in use in the instance database
            result = dm.serializeSqlLinkedHashmap("data.accounts.checkEmailAddressInUse",new Object[]{emailTemp});

            //if duplicate is NOT found in instance db
            if (result == null || result.size() == 0 || result.get(0) == null) {
                isDuplicateInstance = false;
                // D-4444, every email was considered a duplicate because isDuplicateCloud was never reassigned
                isDuplicateCloud = false;

            //if duplicate is found in the instance db
            } else {
                HashMap accountInfoHM = result.get(0);
                String inviteStatus = accountInfoHM.get("INVITESTATUSID").toString();
                String employeeID = accountInfoHM.get("EMPLOYEEID").toString();

                //set the invite status and employeeID if they need another invite sent to them in the front end
                emailResultHM.put("inviteStatus", inviteStatus);
                emailResultHM.put("employeeID", employeeID);

                Logger.logMessage("WARNING: Duplicate email found in instance database in AccountModel.findAvailableEmailCloud", Logger.LEVEL.WARNING);
            }

            emailResultHM.put("isDuplicateInstance", isDuplicateInstance);

            //sends request to the gateway to determine if THIS email is NOT in use
            InstanceClient instanceClient = new InstanceClient();
            requestValidation = instanceClient.checkIfGatewayUserExists(requestValidation, request);

            if (!requestValidation.getIsValid()) { //if not valid, then the email is NOT in use
                isDuplicateCloud = false;
            } else { //the email IS in use - try again...
                Logger.logMessage("WARNING: Duplicate email found in gateway database in AccountModel.findAvailableEmailCloud", Logger.LEVEL.WARNING);
            }
            emailResultHM.put("isDuplicateCloud", isDuplicateCloud);
            if(!isDuplicateInstance) {
                emailResultHM.put("inviteStatus", 4);
            }

        } catch (Exception ex) {
            Logger.logException(ex);
            requestValidation.reset();
            requestValidation.setDetails("An error has occurred. Please contact MMHayes support if the error continues.");
            requestValidation.handleGenericException(ex, "AccountModel.findAvailableEmailCloud");
            return null;
        }
        return emailResultHM;
    }

    //sends request to Gateway to create a username account in the cloud
    public static Validation createGatewayUsernameAcct(HashMap accountInfo, String employeeID, HttpServletRequest request) {
        //create a new validation object (to respond to the client with)
        Validation requestValidation = new Validation();
        try {
            //get login name for cloud
            String cloudLoginName = accountInfo.get("username").toString();

            //hash the password
            String newSalt = BCrypt.GenerateSalt(CommonAPI.getWorkFactor());
            String cloudLoginPassword = BCrypt.HashPassword(accountInfo.get("password").toString(), newSalt);
            Integer instanceUserID = Integer.parseInt(employeeID) * -1;

            InstanceClient instanceClient = new InstanceClient();

            //set the instance details (alias, instance id...)
            requestValidation = instanceClient.getInstanceDetails(requestValidation, request);

            //set requestValidation object properties
            requestValidation.setLoginName(cloudLoginName);
            requestValidation.setLoginPassword(cloudLoginPassword);
            requestValidation.setInstanceUserID(instanceUserID);
            requestValidation.setInstanceUserTypeID(1);

            //send request to gateway to create an account
            requestValidation = instanceClient.createGatewayAccount(requestValidation, request);

        } catch (Exception ex) {
            Logger.logMessage("ERROR: Error creating account for Gateway in AccountModel.createGatewayUsernameAcct", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }

        return requestValidation;
    }

    //gets the last employeeID from the Global Setting - LastEmpID
    public static Integer getLastEmployeeID() {
        Integer lastEmpID = null;
        try {
            lastEmpID = Integer.parseInt(dm.getSingleField("data.accounts.getLastEmployeeID", new Object[]{}).toString());
        } catch (Exception ex) {
            Logger.logMessage("Unable to retrieve Last Employee ID from QC_Globals", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }
        return lastEmpID;
    }

    public static Integer generateNextEmployeeID () {
        try {
            Object nextEmployeeIDObj = dm.getSingleField("data.accounts.generateNextEmpID", new Object[]{});
            if (nextEmployeeIDObj != null) {
                String nextEmployeeIDStr = nextEmployeeIDObj.toString();
                if (nextEmployeeIDStr.trim().length() > 0) {
                    return Integer.parseInt(nextEmployeeIDStr);
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was an error calling the stored procedure SP_QC_GetNextEmployeeID", Logger.LEVEL.ERROR);
        }
        return null;
    }

    //turns the AutoCloudInvite flag ON for the employeeID so it will be picked up by the executor and the email invite will be sent
    public static Boolean sendInviteForGatewayEmailAcct(String employeeID) {
        Boolean isValid = false;
        try {
            //update the global employeeID
            Integer result = dm.serializeUpdate("data.account.sendInviteForGatewayEmailAcct", new Object[]{employeeID});
            if(result != -1){
                isValid = true;
            } else {
                Logger.logMessage("ERROR: Unable to turn on Auto Cloud Invite flag for employee ID: " +employeeID, Logger.LEVEL.ERROR);
            }

        } catch (Exception ex) {
            Logger.logMessage("ERROR: Error sending invite to Gateway Email Account in AccountModel.sendInviteForGatewayEmailAcct", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }

        return isValid;
    }

    //determines the account status based on if the TOS is required, then checks if there are available licenses
    public static HashMap determineAccountStatusAndLicense(HashMap accountTypeInfo) {
        HashMap result = new HashMap();
        HashMap accountWithInfo = new HashMap();
        Boolean validLicenses = false;
        Integer accountGroup = null, spendingProfile = null, accountType = null;

        try {

            if (accountTypeInfo != null) {
                if (accountTypeInfo.get("accountGroupID") != null && !accountTypeInfo.get("accountGroupID").equals("")) {
                    accountGroup = Integer.parseInt(accountTypeInfo.get("accountGroupID").toString());
                }
                if (accountTypeInfo.get("spendingProfileID") != null && !accountTypeInfo.get("spendingProfileID").equals("")) {
                    spendingProfile = Integer.parseInt(accountTypeInfo.get("spendingProfileID").toString());
                }
                if (accountTypeInfo.get("accountTypeID") != null && !accountTypeInfo.get("accountTypeID").equals("")) {
                    accountType = Integer.parseInt(accountTypeInfo.get("accountTypeID").toString());
                }
            }

            //determine if user requires TOS
            Boolean isTOSRequired = comm.checkIfTOSIsRequired(accountGroup, spendingProfile);

            //set the account status based on if TOS is required
            if(isTOSRequired) {
                accountWithInfo.put("ACCOUNTSTATUS", "W");
            } else {
                accountWithInfo.put("ACCOUNTSTATUS", "A");
            }

            //set accountID to -1 for new account and accountTypeID is based off of payrollGroupingID
            accountWithInfo.put("ACCOUNTID", -1);
            accountWithInfo.put("ACCOUNTTYPEID", accountType);

            ArrayList<HashMap> accountsWithInfoForLicenseAvailability = new ArrayList<HashMap>();
            accountsWithInfoForLicenseAvailability.add(accountWithInfo);

            //check if there are available licenses
            if(comm.checkAccountLicenseAvailability(accountsWithInfoForLicenseAvailability)) {
                validLicenses = true;
                result.put("accountStatus", accountWithInfo.get("ACCOUNTSTATUS"));
            } else {
                Logger.logMessage("WARNING: Insufficient licenses AccountModel.checkAccountLicenseAvailability", Logger.LEVEL.WARNING);
            }

            //set values to be displayed in front end
            result.put("validLicenses", validLicenses);

        } catch (Exception ex) {
            Logger.logMessage("ERROR: In AccountModel.determineAccountStatusAndLicense ", Logger.LEVEL.ERROR);
        }

        return result;
    }

    // Gets the latest TOS that was accepted by this user
    public static HashMap getLastAcceptedTOS(HttpServletRequest request) {
        try {
            Integer employeeID = CommonAuthResource.checkRequestHeaderForMMHayesAuth(request);
            ArrayList<HashMap> TOS = dm.parameterizedExecuteQuery("data.accountSettings.getLastAcceptedTOS",
                    new Object[]{employeeID}, true);
            if (TOS != null && TOS.size() > 0) {
                return TOS.get(0);
            }
        } catch (Exception ex) {
            Logger.logException(ex);
        }
        return null;
    }

    //checks if the account number or badge number is in use in QC_Employees (based on the isAcctNum flag)
    public static HashMap checkDuplicateAccountNumber(HashMap accountInfo) {
        String checkResult = ""; String numberTemp = "";
        Boolean isDuplicate = false, keepTrying = true, isAutoGenerated = false, autoGenerateBoth = false;
        HashMap result = new HashMap();
        Integer numLength = null;
        String acctNumber = "", badgeNumber = "";

        try{

            if (accountInfo != null) {
                if (accountInfo.get("accountNumber") != null && !accountInfo.get("accountNumber").equals("")) {
                    acctNumber = accountInfo.get("accountNumber").toString();
                    numLength = acctNumber.length();
                    numberTemp = acctNumber;
                }
                if (accountInfo.get("badgeNumber") != null && !accountInfo.get("badgeNumber").equals("")) {
                    badgeNumber = accountInfo.get("badgeNumber").toString();
                    numLength = badgeNumber.length();
                    numberTemp = badgeNumber;
                }
                if (accountInfo.get("isAutoGenerated") != null && !accountInfo.get("isAutoGenerated").equals("")) {
                    isAutoGenerated = Boolean.valueOf(accountInfo.get("isAutoGenerated").toString());
                }
                if (accountInfo.get("autoGenerateBoth") != null && !accountInfo.get("autoGenerateBoth").equals("")) {
                    autoGenerateBoth = Boolean.valueOf(accountInfo.get("autoGenerateBoth").toString());
                }
            }

            Integer tryCount = 0;
            while (keepTrying) {
                if(!acctNumber.equals("")) { //if checking account number
                    checkResult = dm.getSingleField("data.accounts.checkAccountNumberInUse",new Object[]{numberTemp},null,"",false,true).toString();
                } else if (!badgeNumber.equals("")){  //if checking badge number
                    checkResult = dm.getSingleField("data.accounts.checkBadgeNumberInUse",new Object[]{numberTemp},null,"",false,true).toString();
                }

                if (checkResult != null && !checkResult.isEmpty()) {

                    //if duplicate number is in user, create a new random number and try again...
                    if (Integer.parseInt(checkResult) > 0) {
                        isDuplicate = true;
                        keepTrying = false;
                        if(isAutoGenerated) {   //if auto generated number then create a new one and check again
                            keepTrying = true;
                            tryCount++;
                            numberTemp = createRandomNumber(numLength);
                            Logger.logMessage("WARNING: Could NOT find valid account (badge) Number in AccountModel.checkDuplicateAcctNumber - trying next accountNumber", Logger.LEVEL.WARNING);
                            if (tryCount > 100) {
                                Logger.logMessage("ERROR: Could NOT find valid account (badge) Number in AccountModel.checkDuplicateAcctNumber - tried 100 times.. giving up...", Logger.LEVEL.ERROR);
                                keepTrying = false;
                            }
                        }
                    } else { //the account number is not un use
                        keepTrying = false;
                        if(autoGenerateBoth) { //if both account number and badge number are auto generated, have to also check if generated badge number is duplicate
                            keepTrying = true;
                            autoGenerateBoth = false;
                            result.put("newAcctNum", numberTemp);
                            badgeNumber = acctNumber;
                            acctNumber = "";
                        }
                    }
                }
            }
            if(!acctNumber.equals("")) { //if checking account number
                result.put("newAcctNum", numberTemp);
            } else {  //if checking badge number
                result.put("newBadgeNum", numberTemp);
            }
            result.put("isDuplicate", isDuplicate);
            result.put("isAutoGenerated", isAutoGenerated);

        } catch (Exception ex) {
            Logger.logMessage("ERROR: In AccountModel.checkDuplicateAcctNumber ", Logger.LEVEL.ERROR);
        }
        return result;
    }

    //check the given password again the person account password in the global settings
    public static boolean getPersonAccountPassword(HashMap passwordHM) {
        boolean isValid = false;
        String givenPassword = "";
        try {

            if(!passwordHM.get("personPassword").toString().equals("")) {
                givenPassword = passwordHM.get("personPassword").toString();
            } else {
                throw new MissingDataException();
            }

            String personAccountPassword = dm.parameterizedExecuteScalar("data.accounts.getPersonAccountPassword", new Object[]{}).toString();

            if(givenPassword.equals(personAccountPassword)) {
                isValid = true;
            }

        } catch (Exception ex) {
            Logger.logMessage("ERROR: In AccountModel.getPersonAccountPassword ", Logger.LEVEL.ERROR);
        }
        return isValid;
    }

    public static HashMap<String, String> createPersonAccountInMyQC(HashMap accountInfo, HttpServletRequest request)  {
        HashMap<String, String> result = new HashMap<>();
        String email = "";
        String name = "";
        String lowBalanceThreshold = "";
        try {

            int accessCodeID = CommonAPI.convertModelDetailToInteger(accountInfo.get("accessCodeID"));
            Boolean isAllowPersonAccountCreationOn = getPersonAccountCreationFlag(accessCodeID);

            if(!isAllowPersonAccountCreationOn || CommonAPI.getOnGround()) {
                Logger.logMessage("Error: MyQCAllowPersonAccountCreation flag is off, cannot create Person Account in AccountModel.createPersonAccountInMyQC", Logger.LEVEL.ERROR);
                throw new InvalidAuthException();

            } else {

                email = accountInfo.get("email").toString().toLowerCase();
                lowBalanceThreshold = accountInfo.get("lowBalanceThreshold").toString();

                String firstName = CommonAPI.convertModelDetailToString(accountInfo.get("firstName")).trim();
                String middleInitial = CommonAPI.convertModelDetailToString(accountInfo.get("middleInitial")).trim();
                String lastName = CommonAPI.convertModelDetailToString(accountInfo.get("lastName")).trim();

                if(firstName.equals("")) {
                    result.put("status", "failure");
                    result.put("errorMessage", "Please provide a valid First Name.");
                    Logger.logMessage("Could not create person account due to missing first name", Logger.LEVEL.TRACE);
                    return result;
                }

                if(lastName.equals("")) {
                    result.put("status", "failure");
                    result.put("errorMessage", "Please provide a valid Last Name.");
                    Logger.logMessage("Could not create person account due to missing last name", Logger.LEVEL.TRACE);
                    return result;
                }

                name = lastName + ", " + firstName + " " + middleInitial;

                //prepare sql parameters and then execute insert SQL
                Integer personID = dm.parameterizedExecuteNonQuery("data.accounts.createPersonAccount",
                        new Object[]{
                                name,
                                email,
                                lowBalanceThreshold,
                                "1"
                        }
                );

                //return newly created personID if insert was a success
                if (personID <= 0) {
                    Logger.logMessage("Could not execute insert SQL in AccountModel.createPersonAccountInMyQC", Logger.LEVEL.ERROR);
                    throw new MissingDataException("Could not execute insert SQL for person account creation");
                }  else {

                    Logger.logMessage("Successfully created person account as personID: "+personID+" - in AccountModel.createPersonAccountInMyQC", Logger.LEVEL.TRACE);

                    Validation requestValidation = createGatewayPersonAcct(accountInfo, personID, request);

                    if(requestValidation.getIsValid()) {
                        result.put("status", "success");
                        result.put("email", email);
                    } else {
                        Logger.logMessage("ERROR: Error creating person gateway account in AccountModel.createPersonAccountInMyQC", Logger.LEVEL.ERROR);
                    }

                }
            }

        } catch (Exception ex) {
            Logger.logMessage("Error in AccountModel.createPersonAccountInMyQC", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }
        return result;
    }

    //sends request to Gateway to create a person account in the cloud
    public static Validation createGatewayPersonAcct(HashMap accountInfo, Integer personID, HttpServletRequest request) {
        //create a new validation object (to respond to the client with)
        Validation requestValidation = new Validation();
        Logger.logMessage("Creating account for user in AccountModel.createGatewayPersonAcct", Logger.LEVEL.TRACE);
        try {
            //get login name for cloud
            String cloudEmail = accountInfo.get("email").toString().toLowerCase();

            //hash the password
            String newSalt = BCrypt.GenerateSalt(CommonAPI.getWorkFactor());
            String cloudLoginPassword = BCrypt.HashPassword(accountInfo.get("password").toString(), newSalt);

            InstanceClient instanceClient = new InstanceClient();

            //set the instance details (alias, instance id...)
            requestValidation = instanceClient.getInstanceDetails(requestValidation, request);

            //set requestValidation object properties
            ArrayList<HashMap> emailList = new ArrayList<>();
            HashMap emailHM = new HashMap();

            emailHM.put("emailAddress", cloudEmail);
            emailList.add(emailHM);

            requestValidation.setData(emailList);
            requestValidation.setLoginPassword(cloudLoginPassword);
            requestValidation.setLoginName(cloudEmail);
            requestValidation.setInstanceUserID(personID);
            requestValidation.setInstanceUserTypeID(3);

            //send request to gateway to create an account
            requestValidation = instanceClient.createGatewayAccount(requestValidation, request);

        } catch (Exception ex) {
            Logger.logMessage("ERROR: Error creating account for Gateway in AccountModel.createGatewayUsernameAcct", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }

        Logger.logMessage("Request to gateway was successful; person account created", Logger.LEVEL.TRACE);
        return requestValidation;
    }

    public static String createRandomNumber(Integer numLength) {
        StringBuilder str = new StringBuilder();
        Random rand = new Random();
        for(int i=0;i<numLength;i++) {
            str.append(rand.nextInt(9));
        }
        return str.toString();
    }


    public static Validation createGatewayPersonAccount(HashMap accountDetails, HttpServletRequest request) throws Exception {
        Validation requestValidation = new Validation();
        try {

            Integer personID = 0;
            if(accountDetails.containsKey("personID") && !accountDetails.get("personID").toString().equals("")) {
                personID = CommonAPI.convertModelDetailToInteger(accountDetails.get("personID").toString());
            }

            Logger.logMessage("Successfully hit create/person endpoint in AccountModel.createGatewayPersonAccount for person ID: " + personID.toString(), Logger.LEVEL.DEBUG);

            String email = "";
            if(accountDetails.containsKey("email") && !accountDetails.get("email").toString().equals("")) {
                email = ESAPI.encoder().decodeForHTML(accountDetails.get("email").toString());
            }

            String password = "";
            if(accountDetails.containsKey("password") && !accountDetails.get("password").toString().equals("")) {
                password = ESAPI.encoder().decodeForHTML(accountDetails.get("password").toString());
            }

            if(personID == 0 || email.equals("") || password.equals("")) {
                Logger.logMessage("ERROR: Error missing account information to create person account in AccountModel.createGatewayPersonAccount", Logger.LEVEL.ERROR);
                return requestValidation;
            }

            accountDetails.put("email", email);
            accountDetails.put("password",  password);

            requestValidation = createGatewayPersonAcct(accountDetails, personID, request);

        } catch (Exception ex) {
            Logger.logMessage("ERROR: Error creating account for Gateway in AccountModel.createGatewayPersonAccount", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }

        return requestValidation;
    }

    // Registers a device in the database if not already registered
    public static boolean registerDevice(HashMap registrationInfo, HttpServletRequest request) {
        Integer employeeID;
        try {
            employeeID = CommonAuthResource.checkRequestHeaderForMMHayesAuth(request);
        } catch (Exception ex) {
            Logger.logMessage("ERROR: Could not get the employeeID of the logged in user", Logger.LEVEL.ERROR);
            Logger.logException(ex);
            return false;
        }
        String token = registrationInfo.get("token").toString();
        Integer deviceTypeID = Integer.parseInt(registrationInfo.get("deviceTypeID").toString());

        // Deactivate rows with the same token for different employees
        deactivateDeviceRegistration(employeeID, token);

        // Check whether a registration already exists with the same token and employee
        if (checkDeviceRegistration(employeeID, token) == 0) {
            // Insert a new row
            return createDeviceRegistration(employeeID, token, deviceTypeID);
        } else {
            // Activate the existing registration
            return activateDeviceRegistration(employeeID, token);
        }
    }

    // Inserts a new row for the device registration
    private static boolean createDeviceRegistration(Integer employeeID, String token, Integer deviceTypeID) {
        int insertResult = dm.parameterizedExecuteNonQuery("data.accounts.createDeviceRegistration",
            new Object[] {
                    employeeID,
                    token,
                    deviceTypeID
            }
        );
        if (insertResult != 1) {
            Logger.logMessage("ERROR: Could not execute insert SQL in AccountModel.createDeviceRegistration", Logger.LEVEL.ERROR);
            return false;
        }
        Logger.logMessage("Successfully executed insert SQL in AccountModel.createDeviceRegistration", Logger.LEVEL.TRACE);
        return true;
    }

    // Counts the number of rows with the given device token and employeeID
    private static int checkDeviceRegistration(Integer employeeID, String token) {
        Object countObj = dm.parameterizedExecuteScalar("data.accounts.checkDeviceRegistration",
            new Object[] {
                    employeeID,
                    token
            }
        );
        if (countObj == null) {
            Logger.logMessage("ERROR: Could not execute query in AccountModel.checkDeviceRegistration", Logger.LEVEL.ERROR);
            return -1;
        }
        int count = Integer.parseInt(countObj.toString());
        Logger.logMessage("Successfully executed query in AccountModel.checkDeviceRegistration: " +
                count + " row(s) counted", Logger.LEVEL.TRACE);
        return count;
    }

    // Activates rows with the same device token and employeeID
    private static boolean activateDeviceRegistration(Integer employeeID, String token) {
        int updateResult = dm.parameterizedExecuteNonQuery("data.accounts.activateDeviceRegistration",
            new Object[] {
                    employeeID,
                    token
            }
        );
        if (updateResult < 0) {
            Logger.logMessage("ERROR: Could not execute update SQL in AccountModel.activateDeviceRegistration", Logger.LEVEL.ERROR);
            return false;
        }
        Logger.logMessage("Successfully executed update SQL in AccountModel.activateDeviceRegistration: " +
                updateResult + " row(s) updated", Logger.LEVEL.TRACE);
        return true;
    }

    // Deactivates rows with the same device token and different employeeID
    private static void deactivateDeviceRegistration(Integer employeeID, String token) {
        int updateResult = dm.parameterizedExecuteNonQuery("data.accounts.deactivateDeviceRegistration",
            new Object[] {
                    employeeID,
                    token
            }
        );
        if (updateResult < 0) {
            Logger.logMessage("ERROR: Could not execute update SQL in AccountModel.deactivateDeviceRegistration", Logger.LEVEL.ERROR);
        } else {
            Logger.logMessage("Successfully executed update SQL in AccountModel.deactivateDeviceRegistration: " +
                    updateResult + " row(s) updated", Logger.LEVEL.TRACE);
        }
    }

    public static Boolean guestLicensesAreAvailable() {
        try {
            return CommonAPI.convertModelDetailToBoolean( ((HashMap) dm.parameterizedExecuteQuery("data.accounts.guestLicensesAvailable", new Object[]{}, true).get(0)).get("GUESTACCOUNTSALLOWED"));
        } catch(Exception e) {
            Logger.logMessage("There was an error trying to determine if any guest account licenses are still available.", Logger.LEVEL.ERROR);
            Logger.logException(e);
        }

        return false;
    }

    public static Boolean employeeLicensesAreAvailable() {
        try {
            return CommonAPI.convertModelDetailToBoolean( ((HashMap) dm.parameterizedExecuteQuery("data.accounts.employeeLicensesAvailable", new Object[]{}, true).get(0)).get("EMPLOYEEACCOUNTSAREALLOWED"));
        } catch(Exception e) {
            Logger.logMessage("There was an error trying to determine if any employee account licenses are still available.", Logger.LEVEL.ERROR);
            Logger.logException(e);
        }

        return false;
    }

}
