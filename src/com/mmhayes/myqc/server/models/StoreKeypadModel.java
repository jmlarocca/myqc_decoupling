package com.mmhayes.myqc.server.models;

//mmhayes dependencies

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.CommonAuthResource;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.commonMMHFunctions;
import com.mmhayes.common.rotations.RotationHandler;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.myqc.server.api.MyQCCache;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

//myqc dependencies
//other dependencies

/* Store Keypad Model
Last Updated (automatically updated by SVN)
$Author: ijgerstein $: Author of last commit
$Date: 2021-09-28 10:43:14 -0400 (Tue, 28 Sep 2021) $: Date of last commit
$Rev: 58300 $: Revision of last commit

Notes: Model for Keypads in Store*/
public class StoreKeypadModel {
    private static DataManager dm = new DataManager();
    private commonMMHFunctions commFunc = new commonMMHFunctions();
    private Integer ID = null; //the ID of the store
    private Integer homeKeypadID = null; //the keypad ID of the home keypad for the store
    private Integer diningOptionKeypadID = null; //the keypad ID of the dining option keypad for the store
    private LocalDateTime currentStoreDTM = null; //the current datetime of the store, used for determining rotations
    private Integer popularProductsCount = 0; //the number of products that are considered popular from transactions in the last 3 months, based on store's percent of popular products field
    boolean popularProductsNeedsUpdate = false;
    ArrayList<String> keypadsInStoreList = new ArrayList<>();
    ArrayList<HashMap> rotationMappings = new ArrayList<>();
    ArrayList<String> productsInStoreList = new ArrayList<>();
    ArrayList<String> popularProductsList = new ArrayList<>();
    ArrayList<String> productsInStoreWithDiningOptionsList = new ArrayList<>();
    ArrayList<HashMap> diningOptionProducts = new ArrayList<>();
    ArrayList<String> combosInStoreList = new ArrayList<>();
    List<MenuModel> upsellProfileKeypads = new ArrayList<>();
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String keypadsInStore = "";
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String productsInStore = "";
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String productsInStoreWithDiningOptions = "";
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String popularProducts = "";
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String combosInStore = "";

    //caching related properties
    private LocalDateTime lastUpdateDTM = null; //when the store was last updated
    private LocalDateTime lastUpdateCheck = null; //when the store was last checked to see if it has been updated
    private LocalDateTime lastCacheUpdate = null;
    private LocalDateTime lastPopularUpdateCheck = null;

    public StoreKeypadModel() {

    }

    //constructor - takes hashmap that get sets to this models properties
    public StoreKeypadModel(StoreModel storeModel) throws Exception {
        try {

            setID(storeModel.getID());  //set the ID as the store ID
            setHomeKeypadID(storeModel.getHomeKeypadID());  //set the home keypad ID for the store
            setDiningOptionKeypadID(storeModel.getDiningOptionKeypadID()); //set the dining option keypad ID for the store
            setCurrentStoreDTM(storeModel.getCurrentStoreDTM()); //set the current datetime of the store
            setUpsellProfileKeypads(storeModel.getUpsellProfileKeypads()); //set upsell profile keypads for the store

            //set the list of all the keypads and products in the store
            determineAllProductsAndKeypadsInStore();

            //determine the number of products that can be popular based on all the products in the store
            determinePopularProductsCount(storeModel.getPercentOfPopularProducts());

            //get the most recent lastUpdateDTM for all the keypads in the store
            ArrayList<HashMap> lastUpdateList = dm.parameterizedExecuteQuery("data.ordering.getStoreKeypadRecentUpdate",
                    new Object[]{
                            getKeypadsInStore()
                    },
                    true
            );

            //populate this collection from an array list of hashmaps
            if ( lastUpdateList == null || lastUpdateList.size() == 0 || lastUpdateList.get(0) == null ) {
                Logger.logMessage("Could not find last update time for keypads in store: " + storeModel.getID(), Logger.LEVEL.ERROR);
                return;
            }

            HashMap modelDetailHM = lastUpdateList.get(0);

            //determine that response is valid
            if ( modelDetailHM.get("LASTUPDATEDTM") == null || modelDetailHM.get("LASTUPDATEDTM").toString().isEmpty() ) {
                Logger.logMessage("Could not determine last update time for keypads in StoreKeypadModel constructor by store ID", Logger.LEVEL.ERROR);
                return;
            }

            setModelProperties( modelDetailHM);

            //add newly created model to schedule cache
            MyQCCache.addStoreKeypadToCache(this);

        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
    }

    public static StoreKeypadModel getStoreKeypadModel( StoreModel storeModel ) throws Exception {
        Integer storeID = storeModel.getID();

        //if schedule is in cache, load from cache
        if ( !MyQCCache.checkIfStoreKeypadCacheNeedsUpdate(storeID) ) {
            return MyQCCache.getStoreKeypadFromCache(storeID);
        }

        return new StoreKeypadModel( storeModel );
    }

    public static StoreKeypadModel getStoreKeypadModel( Integer storeID ) throws Exception {

        //if schedule is in cache, load from cache
        if ( MyQCCache.checkCacheForStoreKeypad(storeID) ) {
            return MyQCCache.getStoreKeypadFromCache(storeID);
        }

        return null;
    }

    //return just the list of all the products in the store for /favorite/all/products endpoint
    public static ArrayList<String> getProductsInStoreKeypadModel(HttpServletRequest request) throws Exception {
        Integer storeID = CommonAPI.checkRequestHeaderForStoreID(request);
        StoreModel storeModel = StoreModel.getStoreModel(storeID, request);

        StoreKeypadModel storeKeypadModel = StoreKeypadModel.getStoreKeypadModel(storeModel);
        return storeKeypadModel.getProductsInStoreList();
    }

    private StoreKeypadModel setModelProperties(HashMap modelDetailHM) throws Exception {
        //specify in and out formats for formatting modelDetailHM.get("LASTUPDATEDTM")
        String dateTimeInFormat = "yyyy-MM-dd HH:mm:ss.S";
        String dateTimeOutFormat = "M/dd/yyyy h:mm:ss a"; //old way was - M/dd/yyyy H:mm:ss - jrmitaly 6/23/2015

        //format modelDetailHM.get("TRANSDATE") and set to dateTimeStr
        String dateTimeStr = commFunc.formatDateTime(modelDetailHM.get("LASTUPDATEDTM").toString(),dateTimeInFormat,dateTimeOutFormat);

        //parse dateTimeStr to LocalDateTime and set that to this.dateTime
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(dateTimeOutFormat);
        setLastUpdateDTM( LocalDateTime.parse(dateTimeStr, dateTimeFormatter) );

        setLastUpdateCheck(LocalDateTime.now());
        setLastCacheUpdate(LocalDateTime.now());

        return this;
    }

    //checks against the value of LastUpdateDTM to see if the model has been updated since last being cached
    public boolean checkForUpdates() throws Exception {
        boolean needsUpdate = true;

        Object result = dm.parameterizedExecuteScalar("data.ordering.checkStoreKeypadsForUpdates",
            new Object[]{
                getKeypadsInStore(),
                getLastUpdateDTM().toLocalDate().toString()+" "+getLastUpdateDTM().plusSeconds(1).toLocalTime().toString()
            }
        );

        //keypads needs to be updated if there is no lastUpdateDTM or if the result is 1
        needsUpdate = ( result == null || result.toString().equals("1") );

        return needsUpdate;
    }

    public void determinePopularProductsCount(BigDecimal percentOfPopularPAPluIDs) {

        if ( percentOfPopularPAPluIDs != null) {
            BigDecimal percentOfPopularProducts = (percentOfPopularPAPluIDs).divide(BigDecimal.valueOf(100));

            //get number of products in the store
            BigDecimal productsInStore = BigDecimal.valueOf(getProductsInStoreList().size());

            //get the number of products in the store that can be popular
            BigDecimal popularProductsCount = productsInStore.multiply(percentOfPopularProducts).setScale(0, BigDecimal.ROUND_HALF_UP);

            setPopularProductsCount(popularProductsCount.intValueExact());

            if(checkPopularProductsNeedsReload()) {
                determinePopularProductList();
            }
        }
    }

    public boolean checkPopularProductsNeedsReload() {
        boolean check = false;

        //if loading storeKeypadModel for the first time, return true that it needs to be reloaded
        if(getLastPopularUpdateCheck() == null) {
            return true;
        }

        LocalDateTime now = LocalDateTime.now();

        //if it's been 15 minutes since the popular products list was last reloaded
        if ( now.isAfter(getLastPopularUpdateCheck().plusSeconds(10)) ) {
            Logger.logMessage("Store" + getID() + " needs to be checked for updates to the list of popular products in cache", Logger.LEVEL.DEBUG);

            //if a transaction was made, need to update popular products list
            if ( isPopularProductsNeedsUpdate() ) {
                Logger.logMessage("Store" + getID() + " needs to update popular products list, returning failed cache check", Logger.LEVEL.DEBUG);
                return true;
            } else {
                Logger.logMessage("Store" + getID() + " does not need to update popular products list, returning successful cache check", Logger.LEVEL.DEBUG);
                setLastPopularUpdateCheck( now );
            }
        }

        return check;
    }

    public void determinePopularProductList() {

        //get the list of popular products on the store
        ArrayList<HashMap> popularProductList = dm.parameterizedExecuteQuery("data.ordering.getPopularProductsByStore",
                new Object[]{
                        getID(),
                        getPopularProductsCount(),
                        getProductsInStore()
                },
                true
        );

        ArrayList<String> popularProducts = new ArrayList<>();
        for(HashMap popularHM : popularProductList) {
            String productID = CommonAPI.convertModelDetailToString(popularHM.get("PAITEMID"));
            if( !popularProductList.contains(productID) ) {
                popularProducts.add(productID);
            }
        }

        String popularProductsStr = String.join(",", popularProducts);

        setPopularProductsList(popularProducts);
        setPopularProducts(popularProductsStr);

        setLastPopularUpdateCheck(LocalDateTime.now());
    }

    public void determineAllProductsAndKeypadsInStore() {
        ArrayList<String> keypadList = new ArrayList<>();
        try {
            keypadList.add( getHomeKeypadID().toString() );

            keypadList = getAllKeypadIDsInStore( getHomeKeypadID().toString(), keypadList );

            setKeypadsInStoreList(keypadList);
            setKeypadsInStore();

            getAllProductsByKeypad( keypadList );

            getAllProductsByCombos();

            if(getDiningOptionKeypadID() != null) {
                setDiningOptionKeypadProducts();
            }

            if(getUpsellProfileKeypads().size() > 0) {
                setUpsellProfileKeypadProducts();
            }

        } catch (Exception ex) {
            Logger.logMessage("ERROR: Could not determine all products in current keypad in FavoriteCollection.getAllProductsByStore", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }
    }

    //get all the keypad ids that are active in the current store
    public ArrayList<String> getAllKeypadIDsInStore(String keypadID, ArrayList<String> keypadList) {
        ArrayList<HashMap> innerKeypads = new ArrayList<>();
        try {
            innerKeypads = dm.serializeSqlWithColNames("data.ordering.getAllKeypadsByKeypadID",
                    new Object[]{
                            keypadID,
                            getID()
                    },
                    false,
                    false
            );

            if ( innerKeypads != null && innerKeypads.size() > 0 ) {
                for ( HashMap keypadHM : innerKeypads ) {
                    if ( keypadHM.get("KEYPADID") != null && !keypadHM.get("KEYPADID").toString().equals("") ) {
                        String innerKeypadID = keypadHM.get("KEYPADID").toString();

                        // If this keypad is a rotation controlled keypad, replace it with the current rotation
                        String rotationKeypadID = RotationHandler.checkForRotatingKeypad(getID(), Integer.parseInt(innerKeypadID), getCurrentStoreDTM(), true).toString();
                        if (!innerKeypadID.equals(rotationKeypadID)) {
                            HashMap<String, String> rotationMapping = new HashMap<>();
                            rotationMapping.put("BASEKEYPADID", innerKeypadID);
                            rotationMapping.put("ROTATIONKEYPADID", rotationKeypadID);
                            getRotationMappings().add(rotationMapping);
                            innerKeypadID = rotationKeypadID;
                        }

                        if ( keypadList.contains(innerKeypadID) ) {
                            continue;
                        }

                        keypadList.add(innerKeypadID);

                        getAllKeypadIDsInStore(innerKeypadID, keypadList);
                    }
                }
            }
        } catch (Exception ex) {
            Logger.logMessage("ERROR: Could not determine all products in current keypad in FavoriteCollection.getAllKeypadIDsInStore", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }
        return keypadList;
    }

    // Check for whether any rotating keypads have changed
    public boolean checkForRotationUpdates(LocalDateTime storeDTM) {
        try {
            for (HashMap rotationMapping : getRotationMappings()) {
                String baseKeypadID = rotationMapping.get("BASEKEYPADID").toString();
                String rotationKeypadID = rotationMapping.get("ROTATIONKEYPADID").toString();
                String updatedRotationID = RotationHandler.checkForRotatingKeypad(getID(), Integer.parseInt(baseKeypadID), storeDTM, true).toString();
                if (!rotationKeypadID.equals(updatedRotationID)) {
                    return true;
                }
            }
            return false;
        } catch (Exception ex) {
            Logger.logMessage("ERROR: Could not determine rotation updates in StoreKeypadModel.checkForRotationUpdates", Logger.LEVEL.ERROR);
            Logger.logException(ex);
            return true;
        }
    }

    //get all the keypad ids that are active in the current store
    public ArrayList<String> getAllComboKeypadIDsInStore(String keypadID, ArrayList<String> keypadList) {
        ArrayList<HashMap> innerKeypads = new ArrayList<>();
        try {
            innerKeypads = dm.serializeSqlWithColNames("data.ordering.getAllKeypadsByKeypadID",
                    new Object[]{
                            keypadID,
                            getID()
                    },
                    false,
                    false
            );

            if ( innerKeypads != null && innerKeypads.size() > 0 ) {
                for ( HashMap keypadHM : innerKeypads ) {
                    if ( keypadHM.get("KEYPADID") != null && !keypadHM.get("KEYPADID").toString().equals("") ) {
                        String innerKeypadID = keypadHM.get("KEYPADID").toString();

                        if ( keypadList.contains(innerKeypadID) ) {
                            continue;
                        }

                        keypadList.add(innerKeypadID);

                        getAllKeypadIDsInStore(innerKeypadID, keypadList);
                    }
                }
            }
        } catch (Exception ex) {
            Logger.logMessage("ERROR: Could not determine all products in current keypad in FavoriteCollection.getAllKeypadIDsInStore", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }
        return keypadList;
    }

    //go through each keypad root until all the products have been accounted for
    public void getAllProductsByKeypad(ArrayList<String> keypadList) {
        String keypadListStr = "";
        if(keypadList.size() > 0) {
            keypadListStr = String.join(",", keypadList);
        }

        try {
            ArrayList<HashMap> buttons = dm.serializeSqlWithColNames("data.ordering.getAllProductsByKeypadID",
                    new Object[]{
                            keypadListStr,
                            getID()
                    },
                    false,
                    false
            );

            if(buttons != null && buttons.size() > 0) {
                for(HashMap button : buttons) {
                    if(button.get("PRODUCTID") != null && !button.get("PRODUCTID").toString().equals("")) {
                        String productID = button.get("PRODUCTID").toString();

                        if ( getProductsInStoreList().contains(productID) ) {
                            continue;
                        }

                        addProductToProductList(productID);
                    } else if ( button.get("COMBOID") != null && !button.get("COMBOID").toString().equals("") ) {
                        String comboID = button.get("COMBOID").toString();

                        if ( getCombosInStoreList().contains(comboID) ) {
                            continue;
                        }

                        addComboToComboList(comboID);
                    }
                }
            }

        } catch (Exception ex) {
            Logger.logMessage("ERROR: Could not determine all products in current keypad in FavoriteCollection.getAllProductsByKeypad", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }
    }

    //go through each keypad root until all the products have been accounted for
    public void getAllProductsByCombos() {
        if(getCombosInStore().equals("")) {
            return;
        }

        try {
            ArrayList<HashMap> buttons = dm.parameterizedExecuteQuery("data.ordering.getAllProductsByComboID",
                    new Object[]{
                            getCombosInStore(),
                            getID()
                    },
                    true
            );

            if(buttons != null && buttons.size() > 0) {
                for(HashMap button : buttons) {
                    if(button.get("PRODUCTID") != null && !button.get("PRODUCTID").toString().equals("")) {
                        String productID = button.get("PRODUCTID").toString();

                        if ( getProductsInStoreList().contains(productID) ) {
                            continue;
                        }

                        addProductToProductList(productID);
                    }
                }
            }

        } catch (Exception ex) {
            Logger.logMessage("ERROR: Could not determine all products in current keypad in FavoriteCollection.getAllProductsByKeypad", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }
    }

    public void setDiningOptionKeypadProducts() {
        try {

            ArrayList<HashMap> diningOptionsList = dm.parameterizedExecuteQuery("data.ordering.getDiningOptions",
                    new Object[]{
                            getID()
                    },
                    true
            );

            setDiningOptionProducts(diningOptionsList);

            if(diningOptionsList != null && diningOptionsList.size() > 0) {
                for(HashMap diningOption : diningOptionsList) {
                    String diningOptionID = CommonAPI.convertModelDetailToString(diningOption.get("ID"));

                    if(!getProductsInStoreWithDiningOptionsList().contains(diningOptionID)) {
                        addProductToProductListWithDiningOption(diningOptionID);
                    }
                }

                //the product list updates when the dining option list updates for some reason - this sets the product list back to it's string list
                ArrayList<String> productList = new ArrayList<String>(Arrays.asList(getProductsInStore().split(",")));
                setProductsInStoreList(productList);
            }

        } catch (Exception ex) {
            Logger.logMessage("ERROR: Could not determine dining option products in store for store ID: "+getID()+" in FavoriteCollection.setDiningOptionKeypadProducts", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }
    }

    // Add products from the suggestive selling keypads to the list of all products in store
    public void setUpsellProfileKeypadProducts() {

        for(MenuModel upsellProfileKeypad : getUpsellProfileKeypads()) {
            for(MenuItemModel item : upsellProfileKeypad.getItems()) {
                if(item.getProductID() == null) {
                    continue;
                }
                String productID = item.getProductID().toString();

                if(!getProductsInStoreWithDiningOptionsList().contains(productID)) {
                    addProductToProductListWithDiningOption(productID);
                }
            }
        }

    }

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public LocalDateTime getLastUpdateDTM() {
        return lastUpdateDTM;
    }

    public void setLastUpdateDTM(LocalDateTime lastUpdateDTM) {
        this.lastUpdateDTM = lastUpdateDTM;
    }

    public LocalDateTime getLastUpdateCheck() {
        return lastUpdateCheck;
    }

    public void setLastUpdateCheck(LocalDateTime lastUpdateCheck) {
        this.lastUpdateCheck = lastUpdateCheck;
    }

    public LocalDateTime getLastCacheUpdate() {
        return lastCacheUpdate;
    }

    public void setLastCacheUpdate(LocalDateTime lastCacheUpdate) {
        this.lastCacheUpdate = lastCacheUpdate;
    }

    public ArrayList<String> getKeypadsInStoreList() {
        return keypadsInStoreList;
    }

    public void setKeypadsInStoreList(ArrayList<String> keypadsInStoreList) {
        this.keypadsInStoreList = keypadsInStoreList;
    }

    public ArrayList<HashMap> getRotationMappings() {
        return rotationMappings;
    }

    public void setRotationMappings(ArrayList<HashMap> rotationMappings) {
        this.rotationMappings = rotationMappings;
    }

    public ArrayList<String> getProductsInStoreList() {
        return productsInStoreList;
    }

    public void setProductsInStoreList(ArrayList<String> productsInStoreList) {
        this.productsInStoreList = productsInStoreList;
    }

    public void addProductToProductList(String productID) {
        ArrayList<String> allProductsInStoreList = this.getProductsInStoreList();

        allProductsInStoreList.add(productID);

        setProductsInStoreList(allProductsInStoreList);
        setProductsInStoreWithDiningOptionsList(allProductsInStoreList);
        setProductsInStore();
        setProductsInStoreWithDiningOptions();
    }

    public String getKeypadsInStore() {
        return keypadsInStore;
    }

    public void setKeypadsInStore() {
        this.keypadsInStore = String.join(",", getKeypadsInStoreList());
    }

    public String getProductsInStore() {
        return productsInStore;
    }

    public void setProductsInStore() {
        this.productsInStore = String.join(",", getProductsInStoreList());
    }

    public Integer getHomeKeypadID() {
        return homeKeypadID;
    }

    public void setHomeKeypadID(Integer homeKeypadID) {
        this.homeKeypadID = homeKeypadID;
    }

    public Integer getDiningOptionKeypadID() {
        return diningOptionKeypadID;
    }

    public void setDiningOptionKeypadID(Integer diningOptionKeypadID) {
        this.diningOptionKeypadID = diningOptionKeypadID;
    }

    public LocalDateTime getCurrentStoreDTM() {
        return currentStoreDTM;
    }

    public void setCurrentStoreDTM(LocalDateTime currentStoreDTM) {
        this.currentStoreDTM = currentStoreDTM;
    }

    public Integer getPopularProductsCount() {
        return popularProductsCount;
    }

    public void setPopularProductsCount(Integer popularProductsCount) {
        this.popularProductsCount = popularProductsCount;
    }

    public ArrayList<HashMap> getDiningOptionProducts() {
        return diningOptionProducts;
    }

    public void addProductToProductListWithDiningOption(String productID) {
        ArrayList<String> allProductsInStoreWithDiningOptionsList = this.getProductsInStoreWithDiningOptionsList();

        allProductsInStoreWithDiningOptionsList.add(productID);

        setProductsInStoreWithDiningOptionsList(allProductsInStoreWithDiningOptionsList);
        setProductsInStoreWithDiningOptions();
    }

    public void setDiningOptionProducts(ArrayList<HashMap> diningOptionProducts) {
        this.diningOptionProducts = diningOptionProducts;
    }

    public ArrayList<String> getProductsInStoreWithDiningOptionsList() {
        return productsInStoreWithDiningOptionsList;
    }

    public void setProductsInStoreWithDiningOptionsList(ArrayList<String> productsInStoreWithDiningOptionsList) {
        this.productsInStoreWithDiningOptionsList = productsInStoreWithDiningOptionsList;
    }

    public String getProductsInStoreWithDiningOptions() {
        return productsInStoreWithDiningOptions;
    }

    public void setProductsInStoreWithDiningOptions() {
        this.productsInStoreWithDiningOptions = String.join(",", getProductsInStoreWithDiningOptionsList());
    }

    public String getPopularProducts() {
        if(checkPopularProductsNeedsReload()) {
            determinePopularProductList();
        }

        return popularProducts;
    }

    public void setPopularProducts(String popularProducts) {
        this.popularProducts = popularProducts;
    }

    public ArrayList<String> getPopularProductsList() {
        return popularProductsList;
    }

    public void setPopularProductsList(ArrayList<String> popularProductsList) {
        this.popularProductsList = popularProductsList;
    }

    public LocalDateTime getLastPopularUpdateCheck() {
        return lastPopularUpdateCheck;
    }

    public void setLastPopularUpdateCheck(LocalDateTime lastPopularUpdateCheck) {
        this.lastPopularUpdateCheck = lastPopularUpdateCheck;
    }

    public boolean isPopularProductsNeedsUpdate() {
        return popularProductsNeedsUpdate;
    }

    public void setPopularProductsNeedsUpdate(boolean popularProductsNeedsUpdate) {
        this.popularProductsNeedsUpdate = popularProductsNeedsUpdate;
    }

    public void addComboToComboList(String comboID) {
        ArrayList<String> allCombosInStoreList = this.getCombosInStoreList();

        allCombosInStoreList.add(comboID);

        setCombosInStoreList(allCombosInStoreList);
        setCombosInStore();
    }

    public ArrayList<String> getCombosInStoreList() {
        return combosInStoreList;
    }

    public void setCombosInStoreList(ArrayList<String> combosInStoreList) {
        this.combosInStoreList = combosInStoreList;
    }

    public String getCombosInStore() {
        return combosInStore;
    }

    public void setCombosInStore() {
        this.combosInStore = String.join(",", getCombosInStoreList());
    }

    public List<MenuModel> getUpsellProfileKeypads() {
        return upsellProfileKeypads;
    }

    public void setUpsellProfileKeypads(List<MenuModel> upsellProfileKeypads) {
        this.upsellProfileKeypads = upsellProfileKeypads;
    }
}
