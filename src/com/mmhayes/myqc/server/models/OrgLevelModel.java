package com.mmhayes.myqc.server.models;

import com.mmhayes.common.api.CommonAPI;

import java.util.HashMap;

/**
 * Created by mmhayes on 9/25/2020.
 */
public class OrgLevelModel {

    private int OrgLevelPrimaryId;
    private String Name;
    private Boolean PickupAvailable;
    private Boolean DeliveryAvailable;

    public OrgLevelModel(HashMap orgLevelHM) {
        setOrgLevelPrimaryId(CommonAPI.convertModelDetailToInteger(orgLevelHM.get("ORGLEVELPRIMARYID")));
        setName(CommonAPI.convertModelDetailToString(orgLevelHM.get("NAME")));
        setPickupAvailable(CommonAPI.convertModelDetailToBoolean(orgLevelHM.get("PICKUPAVAILABLE")));
        setDeliveryAvailable(CommonAPI.convertModelDetailToBoolean(orgLevelHM.get("DELIVERYAVAILABLE")));
    }

    public int getOrgLevelPrimaryId() {
        return OrgLevelPrimaryId;
    }

    public void setOrgLevelPrimaryId(int orgLevelPrimaryId) {
        OrgLevelPrimaryId = orgLevelPrimaryId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public Boolean getPickupAvailable() {
        return PickupAvailable;
    }

    public void setPickupAvailable(Boolean pickupAvailable) {
        PickupAvailable = pickupAvailable;
    }

    public Boolean getDeliveryAvailable() {
        return DeliveryAvailable;
    }

    public void setDeliveryAvailable(Boolean deliveryAvailable) {
        DeliveryAvailable = deliveryAvailable;
    }

}
