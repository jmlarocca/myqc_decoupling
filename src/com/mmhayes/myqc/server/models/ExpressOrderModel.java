package com.mmhayes.myqc.server.models;

//mmhayes dependencies

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.commonMMHFunctions;
import com.mmhayes.common.utils.Logger;

import java.math.BigDecimal;
import java.util.*;

//javax.ws.rs dependencies
//myqc dependencies
//other dependencies

/* Expres Order Model
 Last Updated (automatically updated by SVN)
 $Author: ijgerstein $: Author of last commit
 $Date: 2021-08-19 09:58:44 -0400 (Thu, 19 Aug 2021) $: Date of last commit
 $Rev: 57450 $: Revision of last commit

 Notes: Model for express orders (a.k.a PLUs, Items, etc.)
*/
public class ExpressOrderModel {
    Integer id = null; //ID
    private Integer terminalId = null;
    private Integer storeID = null;
    private Integer tenderTypeID = null;
    private Integer favoriteOrderID = null;
    private BigDecimal total = BigDecimal.ZERO;
    private boolean usingDiningOptions = false;
    private boolean usingCreditCardAsTender = false;
    private boolean favoriteOrderActive = false;
    private boolean enableFavorites = true;
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String storeName = "";
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String phone = "";
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String orderType = "";
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String favoriteOrderName = "";
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String comments = "";
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String location = "";
    List<ExpressOrderItemModel> itemList = new ArrayList<>();

    private Integer authenticatedAccountID = null;
    private static DataManager dm = new DataManager();
    private commonMMHFunctions commFunc = new commonMMHFunctions();

    //dummy constructor - needed to consume ANY JSON object via REST (Jersey/Jackson style)
    public ExpressOrderModel() {

    }

    //constructor - takes hashmap that get sets to this models properties
    public ExpressOrderModel(HashMap modelDetailHM, Integer employeeID) throws Exception {
        setAuthenticatedAccountID(employeeID);

        setModelProperties(modelDetailHM);
    }

    //setter for all of the favorite order model's for the My Quick Picks page
    public ExpressOrderModel setModelProperties(HashMap modelDetailHM) throws Exception{
        setID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PATRANSID")));
        setTerminalId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("TERMINALID")));
        setStoreID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("STOREID")));
        setStoreName(CommonAPI.convertModelDetailToString(modelDetailHM.get("STORENAME")));
        setPhone(CommonAPI.convertModelDetailToString(modelDetailHM.get("PHONE")));
        setTenderTypeID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("TENDERTYPEID")));
        setUsingDiningOptions(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("PROMPTFORDININGOPTIONS")));
        setEnableFavorites(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("ENABLEFAVORITES")));
        setComments(CommonAPI.convertModelDetailToString(modelDetailHM.get("COMMENTS")));
        setLocation(CommonAPI.convertModelDetailToString(modelDetailHM.get("LOCATION")));

        if(isEnableFavorites()) {
            FavoriteOrderModel favoriteOrderModel = new FavoriteOrderModel();
            ArrayList<HashMap> favoriteOrder = favoriteOrderModel.isFavorite(getAuthenticatedAccountID(), getID());
            if(favoriteOrder != null && favoriteOrder.size() > 0 && favoriteOrder.get(0).get("FAVORITEORDERID") != "") {
                HashMap favoriteOrderHM = favoriteOrder.get(0);
                Integer favoriteOrderID = CommonAPI.convertModelDetailToInteger(favoriteOrderHM.get("FAVORITEORDERID"));
                boolean active = CommonAPI.convertModelDetailToBoolean(favoriteOrderHM.get("ACTIVE"));
                String name = CommonAPI.convertModelDetailToString(favoriteOrderHM.get("NAME"));

                setFavoriteOrderID(favoriteOrderID);
                setFavoriteOrderName(name);
                setFavoriteOrderActive(active);
            }
        }

        Integer paOrderTypeID = CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAORDERTYPEID"));
        if(paOrderTypeID != null && paOrderTypeID == 3) {
            setOrderType("pickup");
        } else if(paOrderTypeID != null && paOrderTypeID == 2) {
            setOrderType("delivery");
            setUsingDiningOptions(false);
        }

        if(getTenderTypeID() != null && getTenderTypeID().equals(2)) {
            setUsingCreditCardAsTender(true);
        }

        if(modelDetailHM.containsKey("ID")) { //product ID
            setExpressOrderItems(modelDetailHM);
        }

        return this;
    }

    public void setExpressOrderItems(HashMap modelDetailHM) throws Exception {

        if(modelDetailHM.get("ISMODIFIER") != null && modelDetailHM.get("ISMODIFIER").toString().equals("1")) {
            Integer parentLineItemID = CommonAPI.convertModelDetailToInteger(modelDetailHM.get("LINEID"));
            ExpressOrderItemModel expressOrderItemModel = getItemModelByTransLineID(parentLineItemID);

            if(expressOrderItemModel != null) {
                expressOrderItemModel.updateModelModifiers(modelDetailHM);
            }

            updateOrderTotalBasedOnItems();
        } else {
            getItemList().add(new ExpressOrderItemModel(modelDetailHM));

            updateOrderTotalBasedOnItems();
        }
    }

    public ExpressOrderItemModel getItemModelByTransLineID(Integer parentLineItemID) {
        for(ExpressOrderItemModel expressOrderItemModel : getItemList()) {
            if(expressOrderItemModel.getLineID().equals(parentLineItemID)) {
                return expressOrderItemModel;
            }
        }
        return null;
    }

    //updates the total of the order by iterating through all the items and adjusting the price accordingly
    public void updateOrderTotalBasedOnItems() {
        try {

            this.setTotal(BigDecimal.ZERO); //reset to zero every time we add a new product

            for (ExpressOrderItemModel expressOrderItemModel : getItemList()) {
                if (expressOrderItemModel.getID() != null && !expressOrderItemModel.getID().toString().isEmpty()) {

                    //if this item really has a price
                    if (expressOrderItemModel.getPrice() != null && !expressOrderItemModel.getID().toString().isEmpty()) {
                        //expanded out to show what is going on... valid one liner... this.getPrice().add(modifierModel.getPrice()
                        BigDecimal currentTotal = this.getTotal();
                        BigDecimal newTotal = currentTotal.add(expressOrderItemModel.getPrice());
                        setTotal(newTotal);
                    }

                } else {
                    Logger.logMessage("ERROR: Could not determine order total in ExpressOrderModel.updateTotalPriceBasedOnItems()", Logger.LEVEL.ERROR);
                }
            }

        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
    }

    public String buildComparisonString() {
        String expressOrderString = "";

        //sort temporary copy of ItemList by productID and quantity
        List<ExpressOrderItemModel> expressOrderItemModels = new ArrayList<ExpressOrderItemModel>( this.getItemList() );
        Collections.sort( expressOrderItemModels, new Comparator<ExpressOrderItemModel>() {
            public int compare(ExpressOrderItemModel o1, ExpressOrderItemModel o2) {
                try {
                    return o1.getProductID() < o2.getProductID() ? -1 : o1.getProductID() > o2.getProductID() ? 1 : quantitySort(o1, o2);
                } catch (Exception ex) {
                    return -1;
                }
            }

            public int quantitySort(ExpressOrderItemModel o1, ExpressOrderItemModel o2) {
                try {
                    return o1.getQuantity().compareTo(o2.getQuantity()) == -1 ? -1 : o1.getQuantity().compareTo(o2.getQuantity()) == 1 ? 1 : modSort(o1, o2);
                } catch (Exception ex) {
                    return -1;
                }

            }

            public int modSort(ExpressOrderItemModel o1, ExpressOrderItemModel o2) {
                try {
                    List<Integer> o1Mods = o1.getSortedModifierIDs();
                    List<Integer> o2Mods = o2.getSortedModifierIDs();

                    if ( o1Mods.size() == 0 && o2Mods.size() == 0 ) {
                        return 0;
                    }

                    if ( o1Mods.size() > o2Mods.size() ) {
                        return 1;
                    } else if ( o1Mods.size() < o2Mods.size() ) {
                        return -1;
                    }

                    for ( int i = 0; i < o1Mods.size(); i++ ) {
                        if ( o1Mods.get( i ).equals( o2Mods.get( i ) ) ) {
                            continue;
                        }

                        if ( o1Mods.get( i ) > o2Mods.get( i ) ) {
                            return 1;
                        } else {
                            return -1;
                        }
                    }

                    return 0;
                } catch (Exception ex) {
                    return -1;
                }
            }
        });

        for ( ExpressOrderItemModel expressOrderItemModel : expressOrderItemModels ) {
            expressOrderString = expressOrderString.concat( expressOrderItemModel.getProductID().toString() );
            expressOrderString = expressOrderString.concat( expressOrderItemModel.getQuantity().toString() );

            if( expressOrderItemModel.getPrepOption() != null ) {
                expressOrderString = expressOrderString.concat( expressOrderItemModel.getPrepOption().getID().toString() );
            }

            if( expressOrderItemModel.getPAComboID() != null ) {
                expressOrderString = expressOrderString.concat( expressOrderItemModel.getPAComboID().toString() );
            }

            if ( expressOrderItemModel.getModifiers() != null && expressOrderItemModel.getModifiers().size() > 0 ) {
                for ( ModifierModel modifierModel : expressOrderItemModel.getSortedModifierModels() ) {
                    expressOrderString = expressOrderString.concat( modifierModel.getID().toString() );

                    if( modifierModel.getPrepOption() != null ) {
                        expressOrderString = expressOrderString.concat( modifierModel.getPrepOption().getID().toString() );
                    }
                }
            }
        }

        return expressOrderString;
    }

    //getter
    public Integer getID() {
        return this.id;
    }

    //setter
    public void setID(Integer id) {
        this.id = id;
    }


    public Integer getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(Integer terminalId) {
        this.terminalId = terminalId;
    }

    public Integer getStoreID() {
        return storeID;
    }

    public void setStoreID(Integer storeID) {
        this.storeID = storeID;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public List<ExpressOrderItemModel> getItemList() {
        return itemList;
    }

    public void setItemList(List<ExpressOrderItemModel> itemList) {
        this.itemList = itemList;
    }

    public Integer getTenderTypeID() {
        return tenderTypeID;
    }

    public void setTenderTypeID(Integer tenderTypeID) {
        this.tenderTypeID = tenderTypeID;
    }

    public boolean isUsingDiningOptions() {
        return usingDiningOptions;
    }

    public void setUsingDiningOptions(boolean usingDiningOptions) {
        this.usingDiningOptions = usingDiningOptions;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean isUsingCreditCardAsTender() {
        return usingCreditCardAsTender;
    }

    public void setUsingCreditCardAsTender(boolean usingCreditCardAsTender) {
        this.usingCreditCardAsTender = usingCreditCardAsTender;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public Integer getFavoriteOrderID() {
        return favoriteOrderID;
    }

    public void setFavoriteOrderID(Integer favoriteOrderID) {
        this.favoriteOrderID = favoriteOrderID;
    }

    public String getFavoriteOrderName() {
        return favoriteOrderName;
    }

    public void setFavoriteOrderName(String favoriteOrderName) {
        this.favoriteOrderName = favoriteOrderName;
    }

    public boolean isFavoriteOrderActive() {
        return favoriteOrderActive;
    }

    public void setFavoriteOrderActive(boolean favoriteOrderActive) {
        this.favoriteOrderActive = favoriteOrderActive;
    }

    @JsonIgnore
    public Integer getAuthenticatedAccountID() {
        return authenticatedAccountID;
    }

    public void setAuthenticatedAccountID(Integer authenticatedAccountID) {
        this.authenticatedAccountID = authenticatedAccountID;
    }

    public boolean isEnableFavorites() {
        return enableFavorites;
    }

    public void setEnableFavorites(boolean enableFavorites) {
        this.enableFavorites = enableFavorites;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

}

