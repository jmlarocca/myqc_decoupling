package com.mmhayes.myqc.server.models;

//mmhayes dependencies

import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.utils.Logger;

import java.math.BigDecimal;
import java.util.*;

//javax.ws.rs dependencies
//myqc dependencies
//other dependencies

/* Express Order Item Model
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2019-03-13 17:18:12 -0400 (Wed, 13 Mar 2019) $: Date of last commit
 $Rev: 37153 $: Revision of last commit

 Notes: Model for express order items (a.k.a products and products with modifiers)
*/
public class ComboDetailModel {
    private Integer id = null;
    private Integer comboId = null;
    private Integer basePricePAPluId = null;
    private Integer keypadId = null;

    private BigDecimal basePrice = null;
    private BigDecimal comboPrice = null;

    private List<String> keypadDetailPAPluIds = new ArrayList<>();

    private Integer authenticatedAccountID = null;
    private Integer storeID = null;
    static DataManager dm = new DataManager();

    //dummy constructor - needed to consume ANY JSON object via REST (Jersey/Jackson style)
    public ComboDetailModel() {

    }

    //constructor - takes hashmap that get sets to this models properties
    public ComboDetailModel(HashMap modelDetailHM) throws Exception {
        setModelProperties(modelDetailHM);
    }

    //setter for all of the favorite order model's for the My Quick Picks page
    public ComboDetailModel setModelProperties(HashMap modelDetailHM) throws Exception{
        setID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PACOMBODETAILID")));
        setComboId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PACOMBOID")));
        setBasePricePAPluId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("BASEPRICEPAPLUID")));
        setKeypadId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAKEYPADID")));

        setBasePrice(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("BASEPRICE")));
        setComboPrice(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("COMBOPRICE")));

        if(getKeypadId() != null) {
            getKeypadProducts();
        }

        return this;
    }

    public void getKeypadProducts() throws Exception {

        ArrayList<HashMap> products = dm.parameterizedExecuteQuery("data.combo.getComboKeypadItems",
                new Object[]{
                        getKeypadId()
                },
                true
        );

        if ( products == null || products.size() == 0 ) {
            Logger.logMessage("Could not retrieve combo keypad products for keypad ID:" + getKeypadId() + " and combo detail ID: " + getID(), Logger.LEVEL.TRACE);
            return;
        }

        for(HashMap product : products) {
            if(product.get("PRODUCTID") != null && !product.get("PRODUCTID").toString().equals("")) {
                String productID = CommonAPI.convertModelDetailToString(product.get("PRODUCTID"));

                if(!getKeypadDetailPAPluIds().contains(productID)) {
                    getKeypadDetailPAPluIds().add(productID);
                }
            }
        }
    }

    //getter
    public Integer getID() {
        return this.id;
    }

    //setter
    public void setID(Integer id) {
        this.id = id;
    }

    public Integer getComboId() {
        return comboId;
    }

    public void setComboId(Integer comboId) {
        this.comboId = comboId;
    }

    public Integer getBasePricePAPluId() {
        return basePricePAPluId;
    }

    public void setBasePricePAPluId(Integer basePricePAPluId) {
        this.basePricePAPluId = basePricePAPluId;
    }

    public Integer getKeypadId() {
        return keypadId;
    }

    public void setKeypadId(Integer keypadId) {
        this.keypadId = keypadId;
    }

    public BigDecimal getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(BigDecimal basePrice) {
        this.basePrice = basePrice;
    }

    public BigDecimal getComboPrice() {
        return comboPrice;
    }

    public void setComboPrice(BigDecimal comboPrice) {
        this.comboPrice = comboPrice;
    }

    public List<String> getKeypadDetailPAPluIds() {
        return keypadDetailPAPluIds;
    }

    public void setKeypadDetailPAPluIds(List<String> keypadDetailPAPluIds) {
        this.keypadDetailPAPluIds = keypadDetailPAPluIds;
    }

    public Integer getAuthenticatedAccountID() {
        return authenticatedAccountID;
    }

    public void setAuthenticatedAccountID(Integer authenticatedAccountID) {
        this.authenticatedAccountID = authenticatedAccountID;
    }

    public Integer getStoreID() {
        return storeID;
    }

    public void setStoreID(Integer storeID) {
        this.storeID = storeID;
    }

}

