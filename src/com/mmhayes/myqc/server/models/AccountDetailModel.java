package com.mmhayes.myqc.server.models;

//mmhayes dependencies

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.dataaccess.commonMMHFunctions;
import com.mmhayes.common.utils.Logger;

import java.util.ArrayList;
import java.util.List;

/* Account Detail Model
 Last Updated (automatically updated by SVN)
 $Author: mjbatko $: Author of last commit
 $Date: 2021-02-11 10:37:47 -0500 (Thu, 11 Feb 2021) $: Date of last commit
 $Rev: 54203 $: Revision of last commit

 Notes: Model for Employee Account Details
*/
public class AccountDetailModel {
    private commonMMHFunctions commFunc = new commonMMHFunctions();
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String id = "N/A";
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String label = "N/A";
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String value = "N/A";
    List<FAQModel> faqs = new ArrayList<>();

    //constructor - takes hashmap that get sets to this models properties
    public AccountDetailModel(String modelDetailKey, String modelDetailValue) {
        try {
            setModelProperties(modelDetailKey, modelDetailValue);
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
    }

    //constructor - takes hashmap that get sets to this models properties
    public AccountDetailModel(String modelDetailKey, List<FAQModel> faqs) {
        try {

            if (modelDetailKey.equals("FAQS") && faqs.size() > 0) {
                setID("faqs");
                setLabel("faqs");
                setFaqs(faqs);
            }

        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
    }

    //setter for all of this model's properties
    public AccountDetailModel setModelProperties(String modelDetailKey, String modelDetailValue) {
        try {

            //check if the model detail key equals -> NAME
            if (modelDetailKey.equals("NAME") && modelDetailValue != null) {
                setID("name");
                setLabel("Name");
                setValue(modelDetailValue);
                return this; //return now, no need to continue checking
            }

            //check if the model detail key equals -> EMPLOYEENUMBER
            if (modelDetailKey.equals("EMPLOYEENUMBER") && modelDetailValue != null) {
                setID("employee-number");
                setLabel("Employee #");
                setValue(modelDetailValue);
                return this; //return now, no need to continue checking
            }

            //check if the model detail key equals -> CONTACT
            if (modelDetailKey.equals("CONTACT") && modelDetailValue != null) {
                setID("contact-name");
                setLabel("Contact Name");
                setValue(modelDetailValue);
                return this; //return now, no need to continue checking
            }

            //check if the model detail key equals -> EMAILADDRESS1
            if (modelDetailKey.equals("EMAILADDRESS1") && modelDetailValue != null) {
                setID("email");
                setLabel("E-mail");
                setValue(modelDetailValue);
                return this; //return now, no need to continue checking
            }

            //check if the model detail key equals -> PHONE
            if (modelDetailKey.equals("PHONE") && modelDetailValue != null) {
                setID("phone-number");
                setLabel("Phone Number");
                setValue(modelDetailValue);
                return this; //return now, no need to continue checking
            }

            //check if the model detail key equals -> MOBILEPHONE
            if (modelDetailKey.equals("MOBILEPHONE") && modelDetailValue != null) {
                setID("mobile-phone");
                setLabel("Mobile Phone");
                setValue(modelDetailValue);
                return this; //return now, no need to continue checking
            }

            //check if the model detail key equals -> ADDRESS1
            if (modelDetailKey.equals("ADDRESS1") && modelDetailValue != null) {
                setID("address-one");
                setLabel("Address One");
                setValue(modelDetailValue);
                return this; //return now, no need to continue checking
            }

            //check if the model detail key equals -> ADDRESS2
            if (modelDetailKey.equals("ADDRESS2") && modelDetailValue != null) {
                setID("address-two");
                setLabel("Address Two");
                setValue(modelDetailValue);
                return this; //return now, no need to continue checking
            }

            //check if the model detail key equals -> CITY
            if (modelDetailKey.equals("CITY") && modelDetailValue != null) {
                setID("city");
                setLabel("City");
                setValue(modelDetailValue);
                return this; //return now, no need to continue checking
            }

            //check if the model detail key equals -> STATE
            if (modelDetailKey.equals("STATE") && modelDetailValue != null) {
                setID("state");
                setLabel("State");
                setValue(modelDetailValue);
                return this; //return now, no need to continue checking
            }

            //check if the model detail key equals -> ZIPCODE
            if (modelDetailKey.equals("ZIPCODE") && modelDetailValue != null) {
                setID("zip-code");
                setLabel("Zip Code");
                setValue(modelDetailValue);
                return this; //return now, no need to continue checking
            }

            //check if the model detail key equals -> SPENDINGPROFILE
            if (modelDetailKey.equals("SPENDINGPROFILE") && modelDetailValue != null) {
                setID("spending-profile");
                setLabel("Spending Profile");
                setValue(modelDetailValue);
                return this; //return now, no need to continue checking
            }

            //check if the model detail key equals -> ACCOUNTGROUP
            if (modelDetailKey.equals("ACCOUNTGROUP") && modelDetailValue != null) {
                setID("account-group");
                setLabel("Account Group");
                setValue(modelDetailValue);
                return this; //return now, no need to continue checking
            }

            //check if the model detail key equals -> ACCOUNTSTATUS
            if (modelDetailKey.equals("ACCOUNTSTATUS") && modelDetailValue != null) {
                setID("account-status");
                setLabel("Account Status");
                setValue(modelDetailValue);
                return this; //return now, no need to continue checking
            }

            //check if the model detail key equals -> USERACCTACTIVE
            if (modelDetailKey.equals("USERACCTACTIVE") && modelDetailValue != null) {
                setID("account-active");
                setLabel("Account Active");
                setValue(modelDetailValue);
                return this; //return now, no need to continue checking
            }

            //check if the model detail key equals -> ACCOUNTTYPEID
            if (modelDetailKey.equals("ACCOUNTTYPEID") && modelDetailValue != null) {
                setID("account-type-id");
                setLabel("Account Type ID");
                setValue(modelDetailValue);
                return this; //return now, no need to continue checking
            }

            //check if the model detail key equals -> ACCOUNTTYPENAME
            if (modelDetailKey.equals("ACCOUNTTYPENAME") && modelDetailValue != null) {
                setID("account-type-name");
                setLabel("Account Type Name");
                setValue(modelDetailValue);
                return this; //return now, no need to continue checking
            }

            //check if the model detail key equals -> ACCOUNTBADGE
            if (modelDetailKey.equals("ACCOUNTBADGE") && modelDetailValue != null) {
                setID("account-badge");
                setLabel("Account Badge");
                setValue(modelDetailValue);
                return this; //return now, no need to continue checking
            }

            //check if the model detail key equals -> AUTHENTICATIONTYPEID
            if (modelDetailKey.equals("AUTHENTICATIONTYPEID") && modelDetailValue != null) {
                setID("account-authtype");
                setLabel("Account Authentication Type");
                setValue(modelDetailValue);
                return this; //return now, no need to continue checking
            }

            //check if the model detail key equals -> showQCAuthLoginLinkMYQC
            if (modelDetailKey.equals("SHOWQCAUTHLOGINLINKMYQC") && modelDetailValue != null) {
                setID("account-showqcloginlink");
                setLabel("Show QC Auth Login Link MyQC");
                setValue(modelDetailValue);
                return this; //return now, no need to continue checking
            }

            //check if the model detail key equals -> FORCECHANGE
            if (modelDetailKey.equals("FORCECHANGE") && modelDetailValue != null) {
                setID("force-change");
                setLabel("Force Change");
                setValue(modelDetailValue);
                return this; //return now, no need to continue checking
            }

            //check if the model detail key equals -> BADGENUMPREFIX
            if (modelDetailKey.equals("BADGENUMPREFIX") && modelDetailValue != null) {
                setID("badgeNumPrefix");
                setLabel("badgeNumPrefix");
                setValue(modelDetailValue);
                return this; //return now, no need to continue checking
            }

            //check if the model detail key equals -> BADGENUMSUFFIX
            if (modelDetailKey.equals("BADGENUMSUFFIX") && modelDetailValue != null) {
                setID("badgeNumSuffix");
                setLabel("badgeNumSuffix");
                setValue(modelDetailValue);
                return this; //return now, no need to continue checking
            }

            //check if the model detail key equals -> QRCODELENGTH
            if (modelDetailKey.equals("QRCODELENGTH") && modelDetailValue != null) {
                setID("qrCodeLength");
                setLabel("qrCodeLength");
                setValue(modelDetailValue);
                return this; //return now, no need to continue checking
            }

            //check if the model detail key equals -> EMPLOYEELOWBALANCETHRESHOLD
            if (modelDetailKey.equals("EMPLOYEELOWBALANCETHRESHOLD") && modelDetailValue != null) {
                setID("employeeLowBalanceThreshold");
                setLabel("employeeLowBalanceThreshold");
                setValue(modelDetailValue);
                return this; //return now, no need to continue checking
            }

            if (modelDetailKey.equals("ORGLEVELPRIMARYID") && modelDetailValue != null) {
                setID("orgLevelPrimaryId");
                setLabel("Org Level");
                setValue(modelDetailValue);
                return this; //return now, no need to continue checking
            }

            if (modelDetailKey.equals("ORGLEVELPRIMARYNAME")){
                setID("orgLevelName");
                setValue(modelDetailValue);
                setLabel("Location");
                return this;
            }

        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
        return this;
    }

    //getter
    public String getID() {
        return this.id;
    }

    //setter
    public void setID(String id) {
        this.id = id;
    }

    //getter
    public String getLabel() {
        return this.label;
    }

    //setter
    public void setLabel(String label) {
        this.label = label;
    }

    //getter
    public String getValue() {
        return this.value;
    }

    //setter
    public void setValue(String value) {
        if (value.equals("")) {
            value = "N/A";
        }
        this.value = value;
    }

    public List<FAQModel> getFaqs() {
        return faqs;
    }

    public void setFaqs(List<FAQModel> faqs) {
        this.faqs = faqs;
    }

}
