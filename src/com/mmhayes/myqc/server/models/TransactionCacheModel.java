package com.mmhayes.myqc.server.models;

//mmhayes dependencies

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.utils.Logger;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;

//myqc dependencies
//other dependencies

/* Transaction Cache Model
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2019-11-26 11:26:47 -0500 (Tue, 26 Nov 2019) $: Date of last commit
 $Rev: 44044 $: Revision of last commit

 Notes: Model for Transaction Cache items
 Keypads or Products are allowed)
*/
public class TransactionCacheModel {
    Integer id = null; // PATransactionId
    LocalDateTime timeCreated = null;
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String orderNumber = "";
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String timeStamp = "";

    //constructor - takes hashmap that get sets to this models properties
    public TransactionCacheModel() {}

    //getter
    public Integer getID() {
        return this.id;
    }

    //setter
    public void setID(Integer id) {
        this.id = id;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public LocalDateTime getTimeCreated() {
        return timeCreated;
    }

    public void setTimeCreated(LocalDateTime timeCreated) {
        this.timeCreated = timeCreated;
    }
}
